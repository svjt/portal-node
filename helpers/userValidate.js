const Joi = require('joi');
const common        = require('../controllers/common');
const Config   = require('../configuration/config'); 
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
//s3 bucket
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require("multer");
const uuidv4        = require('uuid/v4');
const userModel = require('../models/user');
const agentModel = require('../models/agents');
var path = require("path");
const bcrypt   = require('bcryptjs');

const s3Config = new AWS.S3({
  accessKeyId: Config.aws.accessKeyId,
  secretAccessKey: Config.aws.secretAccessKey,
  Bucket: Config.aws.bucketName
});

var storage_client = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'public/profile_pic')
  },
  filename: function(req, file, callback) {
    var extention = path.extname(file.originalname);
    var new_file_name = (Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    callback(null, new_file_name);
  }
});

var multerS3Config = multerS3({
  s3: s3Config,
  bucket: Config.aws.bucketName,
  metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    var extention  = path.extname(file.originalname);
    var new_file_name = 'profile_pic/'+(Math.floor(Date.now() / 1000))+uuidv4()+extention;
    cb(null, new_file_name)
  }
});

var multerS3ConfigQA = multerS3({
  s3: s3Config,
  bucket: Config.aws.bucketName,
  metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    var extention  = path.extname(file.originalname);
    var new_file_name = 'qa_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    cb(null, new_file_name)
  }
});

module.exports = {
  validateBody: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema,{abortEarly: false});
      if (result.error) {
        let err_msg = {};
        for (let counter in result.error.details) {

          let k   = result.error.details[counter].context.key;
          let val = result.error.details[counter].message;
          err_msg[k] = val;
        }
        let return_err = {status:2,errors:err_msg};
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['body'] = result.value;
      next();
    }
  },
  validateParam: (schema) => {
    return (req, res, next) => {

      const result = Joi.validate(req.params, schema);
      if (result.error) {
  
        let return_err = {status:2,errors:"Invalid argument"};
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['params'] = result.value;
      next();
    }
  },
  schemas: {
    authSchema: Joi.object().keys({
      username: Joi.string().required().min(4).max(80),
      password: Joi.string().required().min(4).max(20)
    }),
    passSchema: Joi.object().keys({
      ex_password: Joi.string().required().min(4).max(12),
      new_password: Joi.string().required().min(4).max(12)
    }),
    add_user: Joi.object().keys({
      email: Joi.string().required().email().max(80),
      //No whitespace allowed
      first_name: Joi.string().min(1).max(50).required().regex(/^([A-Za-z]( )?)+$/, 'first name').error(() => {
        return {
          message: 'Please enter text in English or remove extra whitespace.',
        };
      }),
      //No whitespace allowed
      last_name: Joi.string().min(1).max(50).required().regex(/^([A-Za-z]( )?)+$/, 'last name').error(() => {
        return {
          message: 'Please enter text in English or remove extra whitespace.',
        };
      }),
      //Only numbers allowed
      mobile: Joi.string().required().max(20).regex(/^[0-9-]+$/),
      company_name : Joi.string().required().max(150).regex(/^[A-Za-z\s\d\.\'\"\,\-\(\)\&]+$/, 'company name').error(() => {
        return {
          message: 'Alphabets(A-Z), Digits(0-9), space & some special characters (\',",-,(,),/,&,. and \,) are allowed.',
        };
      }),
      country_id : Joi.number().required(),
      language_code : Joi.string().required()

    }),
    contact_me: Joi.object().keys({
      email: Joi.string().required().email().max(80),
      name: Joi.string().min(1).max(50).required(),
      mobile: Joi.string().max(20).allow('').allow(null).regex(/^[^.\/]*$/, 'mobile'),
      message : Joi.string().required().max(250),
      company_name : Joi.string().allow('').allow(null),
      country_id : Joi.number().required(),
      product_id:  Joi.number().required()
    }),
    forgotPassword: Joi.object().keys({
      email: Joi.string().required().email().max(80),
    }),
    resetPassword: Joi.object().keys({
      token : Joi.string().required().min(1),
      password : Joi.string().required().min(4).max(20)
    }),
    token_chck: Joi.object().keys({
      token : Joi.string().required().min(1),
    }),
    setPassword: Joi.object().keys({
      token : Joi.string().required().min(1),
      password : Joi.string().required().min(4).max(20)
    }),
    notification: Joi.object().keys({
      company_id : Joi.number().required()
    }),
    
    update_self_customer: Joi.object().keys({
      current_password: Joi.string().optional().min(4).max(20).allow('').allow(null),
      password : Joi.string().optional().min(4).max(20).allow('').allow(null),
      // first_name: Joi.string().required().min(1).regex(/^[A-Za-z\s]*$/, 'alphabet and space').max(30),
      // last_name: Joi.string().required().min(1).regex(/^[A-Za-z\s]*$/, 'alphabet and space').max(30),
      phone_no: Joi.string().required().max(20),
      country_id : Joi.number().required(),
      dnd: Joi.string().required().regex(/^[1|2]$/, 'status for do not disturb.'),
      file: Joi.optional(),
      language_code : Joi.string().required()
      
    }),
    update_pic: Joi.object().keys({
      file: Joi.required()
    }),
    update_notification: Joi.object().keys({
      notification_id: Joi.number().required(),
      task_id: Joi.number().required()
    }),
    setToken : Joi.object().keys({
      shadow_token: Joi.string().required().min(1)
    }),
    qa_form: Joi.object().keys({
      customer_name: Joi.string().required(),
      batch_number: Joi.string().required(),
      lot_no: Joi.string().required(),
      file: Joi.optional(),
    }),
    validate_customer_rejection: Joi.object().keys({
      reason_of_rejection: Joi.string().required().min(2).max(200)
    }),
  },
  schema_posts:{
   post_update_self: async (req, res, next) => {
      try{
         //file upload
            var upload = multer({
              storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
                fileFilter: function(req, file, callback) {
                  var ext = path.extname(file.originalname).toLowerCase();
                  //console.log("file extension", ext);

                  if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {

                    callback("Only files with the following extensions are allowed: jpeg jpg png", null)
                  }else{
                    callback(null, true)
                  }
                },
                limits: { fileSize: Config.maxFileUploadSize }
              }).single('file');

              upload(req, res, async function(err) {
                if (err) {
                  if (err.code == 'LIMIT_FILE_SIZE') {
                    res.status(400).json({
                      status:2,
                      errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                      ).end()
                  }else {
                    res.status(400).json({
                      status:2,
                      errors: {message : err}}
                      ).end()
                  }
                }else{
                  //VALIDATE USING JOI
                  var result = Joi.validate(req.body, module.exports.schemas.update_self_customer,{abortEarly:false});
                  // if(req.user.role == 1)
                  // {
                  //   result = Joi.validate(req.body, module.exports.schemas.update_self_customer,{abortEarly:false});
                  // }
                  // else
                  // {
                  //   result = Joi.validate(req.body, module.exports.schemas.update_self_agent,{abortEarly:false});
                  // }
                  
                  if (result.error && !(req.file)) {
                    let err_msg = [];
                      // var rs = result.error.details.map((value,index)=>{
                      //   let err_key = value.context.key
                      //   let err_val = value.message

                      //   return { err_key : err_val }
                      // })

                      for (let counter in result.error.details) {

                        let k   = result.error.details[counter].context.key;
                        let val = result.error.details[counter].message;
                        err_msg[k] = val;
                        // console.log("TYPE",typeof err_key);
                        err_msg.push({[k] : val});
                      }
                     // console.log(err_msg);

                      let return_err = {status:2,errors: Object.assign(...err_msg)};
                      res.status(400).json(return_err).end();
                  }else{
                    if(Object.keys(req.body).length > 0)
                    {
                      const {current_password} = req.body;
                      if(Object.prototype.hasOwnProperty.call(req.body, 'current_password') &&  entities.encode(current_password) != "")
                      {
                        if(req.user.role == 1)
                        {
                            await userModel.get_pass(req.user.customer_id)
                            .then(async function(data){
                            await bcrypt.compare(entities.encode(current_password),data[0].password)
                            .then(async function(data){
                              if(!data)
                              {
                              return res.status(400).json({
                                    status  : 2,
                                    errors : {current_password : "Password does not match with the existing password"} 
                                  })
                              }
                              else
                              {
                                req.body.file_name = (req.file) ? req.file : null;
                                next();
                              }
                            }).catch(err=> {
                              common.logError(err);
                              res.status(400).json({
                                status  : 3,
                                message : Config.errorText.value
                              }).end();
                            })
                            
                          }).catch(err=> {
                            common.logError(err);
                            res.status(400).json({
                              status  : 3,
                              message : Config.errorText.value
                            }).end();
                          })
                        }
                        else
                        {
                          await agentModel.get_pass(req.user.customer_id)
                          .then(async function(data){
                          await bcrypt.compare(entities.encode(current_password),data[0].password)
                          .then(async function(data){
                            if(!data)
                            {
                            return res.status(400).json({
                                  status  : 2,
                                  errors : {current_password : "Password does not match with the existing password"}
                                })
                            }
                            else
                            {
                              req.body.file_name = (req.file) ? req.file : null;
                              next();
                            }
                          }).catch(err=> {
                            common.logError(err);
                            res.status(400).json({
                              status  : 3,
                              message : Config.errorText.value
                            }).end();
                          })
                           
                        }).catch(err=> {
                          common.logError(err);
                          res.status(400).json({
                            status  : 3,
                            message : Config.errorText.value
                          }).end();
                        })
                        }
                          
                      }
                      else
                      {
                        req.body.file_name = (req.file) ? req.file : null;
                        next();
                      }
                      
                    }
                    else
                    {
                      req.body.file_name = (req.file) ? req.file : null;
                      next();
                    }
                  
              }
            }
            })
        

        
      }catch(err){
        common.logError(err);
        res.status(400).json({
          status  : 3,
          message : Config.errorText.value
        }).end();
      }
    

  },
  post_update_pic: async (req, res, next) => {
    try{
       //file upload
          var upload = multer({
            storage: Config.environment == 'local' ? storage_client :  multerS3Config,//bucket storage will be there if env production and qa
              fileFilter: function(req, file, callback) {
                var ext = path.extname(file.originalname).toLowerCase();
                //console.log("file extension", ext);

                if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg' && ext !== '.gif') {

                  callback("Only files with the following extensions are allowed: jpeg jpg png gif", null)
                }
                else
                {
                  callback(null, true)
                }
                
              },
              limits: { fileSize: Config.maxFileUploadSize }
            }).single('file');

            upload(req, res, async function(err) {
              if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                  res.status(400).json({
                    status:2,
                    errors: {profile_pic: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                    ).end()
                }else {
                  res.status(400).json({
                    status:2,
                    errors: {profile_pic : err}}
                    ).end()
                }
              }else{
                //VALIDATE USING JOI
                //console.log(req.file);
                //console.log(req);
                const result = Joi.validate(req.body, module.exports.schemas.update_pic,{abortEarly:false});
                if (result.error && !(req.file)) {
                    let err_msg = {};
                    for (let counter in result.error.details) {

                      let k   = result.error.details[counter].context.key;
                      let val = result.error.details[counter].message;
                      err_msg[k] = val;
                    }
                    let return_err = {status:2,errors:err_msg};
                    res.status(400).json(return_err).end();
                }
                else
                {
                  req.body.file_name = (req.file) ? req.file : null;
                  if(process.env.NODE_ENV == 'production')
                  {
                    req.body.file_name.filename = req.file.key;
                  }

                  next();
                }
          }
          })
    }catch(err){
      common.logError(err);
      res.status(400).json({
        status  : 3,
        message : Config.errorText.value 
      }).end();
    }
  },
  // findByEmail
  login :  async (req, res, next) => {
    try{
      let return_err = {status:4,errors:{expired : 'Password has expired.'}};
      const {username} =req.value.body;
      //console.log('username',username);
      const user = await userModel.findByEmail(entities.encode(username));
      //console.log('user',user);
      const agent = await agentModel.findByEmail(entities.encode(username));
      if (user.length>0 && user[0].activated == 1 &&  (user[0].password == null || user[0].password == '')) {
        res.status(400).json(return_err).end();
      }
      else if (agent.length>0 && agent[0].activated == 1 &&  (agent[0].password == null || agent[0].password == '')) {
        res.status(400).json(return_err).end();
      }
      else{
        next();
      }
    }
    catch(err){
      common.logError(err);
      res.status(400).json({
        status  : 3,
        message : Config.errorText.value
      }).end();
    }
  },
  // findByEmail
  login_qa :  async (req, res, next) => {
    try{
      let return_err = {status:4,errors:{expired : 'Password has expired.'}};
      const {username} =req.value.body;
      const user = await userModel.findByEmailQA(entities.encode(username));
      if (user.length>0 && (user[0].password == null || user[0].password == '')) {
        res.status(400).json(return_err).end();
      }
      else{
        next();
      }
    }
    catch(err){
      common.logError(err);
      res.status(400).json({
        status  : 3,
        message : Config.errorText.value
      }).end();
    }
  },
  qa_form: async (req, res, next) => {
    //console.log('task valdiate helper',process.env.NODE_ENV);
    try {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3ConfigQA,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);
  
          if (ext !== '.pdf') {
  
            callback("Only files with the following extensions are allowed: pdf", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);
  
      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {file_name: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {file_name : err}}
              ).end()
          }
        } else {
          let result = Joi.validate(req.body, module.exports.schemas.qa_form,{abortEarly:false});
          if (result.error) {
            let err_msg = {};
            for (let counter in result.error.details) {
  
              let k   = result.error.details[counter].context.key;
              let val = result.error.details[counter].message;
              err_msg[k] = val;
            }
            let return_err = {status:2,errors:err_msg};
            res.status(400).json(return_err).end();
          }else{
            if(process.env.NODE_ENV == 'production')
            {
              if(req.files && req.files.length > 0)
              {
                for (let index = 0; index < req.files.length; index++) {
                  console.log(req.files[index].key,req.files[index].buffer);
                  req.files[index].filename = req.files[index].key;
                }
              }
            }
            next();
          }
        }         
      });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  }

}
}