const db =require('../configuration/dbConn');
const sapModel = require('../models/sap');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const common = require('../controllers/common');


module.exports = {	
	
	products_details :async (req, res, next)=> {
		var product_id = req.query.id;
		let err_count  = 0;

		if(product_id && product_id != ''){

			let check_product = await sapModel.get_products_details(product_id);
			if(check_product){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},
}

