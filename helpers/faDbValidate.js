const db = require('../configuration/dbConn');
const faModel = require('../models/fa');
const validateModel = require('../models/validate');
const agentModel    = require('../models/agents');
const userModel     = require("../models/user");
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const common = require('../controllers/common');


module.exports = {

	task_enquiry: async (req, res, next) => {
		var enquiry_id = req.body.id;
		let err_count = 0;

		if (enquiry_id && enquiry_id != '') {

			let check_enquiry = await faModel.get_task_enquiry_details(enquiry_id);
			if (check_enquiry) {
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');
			} else {
				err_count++;
			}

		}

		if (err_count == 0) {
			next();
		} else {
			let return_err = { 'status': 5, 'errors': 'Task enquiry not found' };
			return res.status(400).json(return_err);
		}

	},

	check_task_enquiry_details: async (req, res, next) => {
		var enquiry_id = req.params.id;
		let customer_id = req.user.customer_id;
		let err_count = 0;

		if(enquiry_id > 0 && customer_id > 0){
			let check_enquiry = await faModel.check_task_enquiry_details(enquiry_id,customer_id);
			if (check_enquiry) {
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');
			} else {
				err_count++;
			}
		}else{
			err_count++;
		}

		if (err_count == 0) {
			next();
		} else {
			let return_err = { 'status': 5, 'errors': 'Task enquiry not found' };
			return res.status(400).json(return_err);
		}

	},

	get_excipients: async (req, res, next) => {
		var ndc_code = req.body.ndc_code;
		let err_count = 0;

		if (ndc_code && ndc_code != '') {

			let check_enquiry = await faModel.ndc_code_exist(ndc_code);
			if (check_enquiry.success) {
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');
			} else {
				err_count++;
			}

		}

		if (err_count == 0) {
			next();
		} else {
			let return_err = { 'status': 5, 'errors': 'Task enquiry not found' };
			return res.status(200).json(return_err);
		}

	},

	request_more_info: async (req, res, next) => {
		const { enquiry_type, product_name } = req.value.body;
		let err_count = 0;

		if (enquiry_type == '1') {

			// let check_enquiry = await faModel.get_productus(product_name);
			// if (check_enquiry) {
			// 	//ACCESS GRANTED
			// } else {
			// 	err_count++;
			// }

		} else if (enquiry_type == '3') {

			// let check_enquiry = await faModel.get_productema(product_name);
			// if (check_enquiry) {
			// 	//ACCESS GRANTED
			// } else {
			// 	err_count++;
			// }

		} else if (enquiry_type == '5') {

			// let check_enquiry = await faModel.get_patent(product_name);
			// if (check_enquiry) {
			// 	//ACCESS GRANTED
			// } else {
			// 	err_count++;
			// }

		}

		if (err_count == 0) {
			next();
		} else {
			let return_err = { 'status': 5, 'errors': 'Task enquiry not found' };
			return res.status(400).json(return_err);
		}

	},
	post_rating: async(req,res,next)=>{
		let err_count     = 0;
		var err           = {};
		const {
			rating,
			comment,
			options,
			product,
			task_rating_application
		  } = req.body;

			
		if(!common.inArray(rating,[1,2,3,4,5]) || !common.inArray(task_rating_application,[1,2,3,4,5])){
			err_count++;
		}

		for (let index = 0; index < options.length; index++) {
			const element = options[index];

			let check_options = await faModel.rating_option_exists(element);

			if(check_options == false){
				err_count++;
				break;
			}
			
		}

		// let previously_rated = await faModel.check_task_rating(req.user.customer_id,req.user.role);

		// if(previously_rated){
		// 	err_count++;
		// }
			

		
		if (err_count == 0) {
			next();
		}else{
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	}
}

