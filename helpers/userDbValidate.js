const db = require('../configuration/dbConn');
const userModel = require('../models/user');
const agentModel = require('../models/agents');
const validateModel = require('../models/validate');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const common = require('../controllers/common');


module.exports = {

	register: async (req, res, next) => {
		//console.log('CB0')
		next()
	},
	contact: async (req, res, next) => {
		// SATYAJIT
		let err = {};
		let err_cnt = 0;

		var product_data = await userModel.validate_product(req.body.product_id);
		if (product_data.length > 0) {
			req.value.body.product_name = product_data[0].product_name
		} else {
			err.product_id = "Invalid Product.";
			err_cnt++;
		}

		const countryArray = await userModel.get_country_name(req.body.country_id);
		if (countryArray.length > 0) {

		} else {
			err.country = "Invalid country posted";
			err_cnt++;
		}

		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(400).json(return_err);
		}
	},
	qa_form: async (req, res, next) => {
		let err_count = 0;
		var err = {};
		const {
			customer_name,
			batch_number,
			lot_no
		} = req.body;

		let ent_customer_name = entities.encode(customer_name.toLowerCase());
		let ent_batch_number = entities.encode(batch_number.toLowerCase());
		let ent_lot_no = entities.encode(lot_no.toLowerCase());

		let check_prev = await userModel.check_coa(ent_customer_name, ent_batch_number, ent_lot_no);

		if (check_prev) {
			err_count++;
			err.customer_name = `Data already exixts.`;
		}

		if (err_count == 0) {
			next();
		} else {
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	},
	add_user: async (req, res, next) => {

		const {
			email,
			mobile,
			country_id,
			language_code
		} = req.value.body;
		const user = {
			email: entities.encode(email),
			mobile: mobile,
			country_id: country_id,
			language_code: language_code
		}

		let err = {};
		const emailExists = await userModel.email_exists_general(user.email);
		if (emailExists.success) {
			//err.email = "Email already exists";
			if (language_code == 'zh') {
				err.email = "电子邮件已经存在";
			} else if (language_code == 'pt') {
				err.email = "e-mail já existe";
			} else if (language_code == 'es') {
				err.email = "el Email ya existe";
			} else if (language_code == 'ja') {
				err.email = "メールは既に存在します";
			} else {
				err.email = "Email already exists";
			}
		}
		const emailExists2 = await userModel.email_exists_dummy(user.email);
		if (emailExists2.length > 1) {
			//err.email = "Email already exists";
			if (language_code == 'zh') {
				err.email = "电子邮件已经存在";
			} else if (language_code == 'pt') {
				err.email = "e-mail já existe";
			} else if (language_code == 'es') {
				err.email = "el Email ya existe";
			} else if (language_code == 'ja') {
				err.email = "メールは既に存在します";
			} else {
				err.email = "Email already exists";
			}
		}
		const emailExists1 = await agentModel.email_exists(user.email);
		if (emailExists1.success) {
			//err.email = "Email already exists";
			if (language_code == 'zh') {
				err.email = "电子邮件已经存在";
			} else if (language_code == 'pt') {
				err.email = "e-mail já existe";
			} else if (language_code == 'es') {
				err.email = "el Email ya existe";
			} else if (language_code == 'ja') {
				err.email = "メールは既に存在します";
			} else {
				err.email = "Email already exists";
			}
		}
		const countryArray = await userModel.get_country_name(user.country_id);
		if (countryArray.length > 0) {

		} else {
			err.country = "Invalid country posted";
		}

		const languageArray = await userModel.get_language_name(user.language_code);
		if (languageArray.length > 0) {

		} else {
			err.language = "Invalid language posted";
		}

		if (common.isEmptyObj(err)) {
			next()
		} else {
			let return_err = {
				'status': 2,
				'errors': err
			};
			return res.status(400).json(return_err);
		}

	},
	update_notification: async (req, res, next) => {

		const {
			notification_id
		} = req.value.body;
		const customer_id = req.user.customer_id
		let role = '';
		let err = {};

		if (req.user.role == 1) {
			role = 'C';
		} else if (req.user.role == 2) {
			role = 'A';
		} else {
			err.notification = "Invalid value submitted";
		}

		if (common.isEmptyObj(err)) {
			const notificationExists = await userModel.notification_exists(customer_id, notification_id, role);
			if (!notificationExists.success) {
				err.notification = "Invalid data submitted";
			}
		}

		if (common.isEmptyObj(err)) {
			next()
		} else {
			let return_err = {
				'status': 2,
				'errors': err
			};
			return res.status(400).json(return_err);
		}
	},
	update_account: async (req, res, next) => {

		const {
			country_id,
			language_code
		} = req.body;
		const user = {
			country_id: country_id,
			language_code: language_code
		}

		let err = {};

		const countryArray = await userModel.get_country_name(user.country_id);
		if (countryArray.length > 0) {

		} else {
			err.country = "Invalid country posted";
		}

		const languageArray = await userModel.get_language_name(user.language_code);
		if (languageArray.length > 0) {

		} else {
			err.language = "Invalid language posted";
		}

		if (common.isEmptyObj(err)) {
			next()
		} else {
			let return_err = {
				'status': 2,
				'errors': err
			};
			return res.status(400).json(return_err);
		}

	},
	dashboard: async (req, res, next) => {
		var company_id = req.query.cid;
		let err_count = 0;

		if (company_id && company_id != '') {
			var pattern = new RegExp('^[0-9]*$');
			var check_comany_id = pattern.test(company_id);
			if (check_comany_id) {
				if (req.user.role == 2) {
					let check_company = await validateModel.check_company(company_id);
					if (check_company) {
						let check_agent_company = await validateModel.check_agent_company(req.user.customer_id, company_id);
						if (check_agent_company) {
							//ACCESS GRANTED
							console.log('ACCESS GRANTED');
						} else {
							err_count++;
						}
					} else {
						err_count++;
					}
				} else {
					err_count++;
				}
			} else {
				err_count++;
			}
		}

		if (err_count == 0) {
			next()
		} else {
			let return_err = {
				'status': 5,
				'errors': 'Invalid Access'
			};
			return res.status(400).json(return_err);
		}

	},
	my_product: async (req, res, next) => {
		let err = {};
		let err_cnt = 0;

		if (req.user.role != 1) {
			err.customer_name = "Invalid customer details.";
			err_cnt++;
		}

		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(400).json(return_err);
		}
	},
	process_customer_response: async (req, res, next) => {
		let err = {};
		let err_cnt = 0;

		if(req.user.customer_id){
			if (req.user.payload.tyds == 'I') {
				let check_preship_invoice = await validateModel.check_preship_invoice(req.user.payload.tyti);
				if (check_preship_invoice) {
					//ACCESS GRANTED
					console.log('ACCESS GRANTED');
				} else {
					err_cnt++;
					err.process_customer_response = "Invalid Access.";
				}
			} else if (req.user.payload.tyds == 'C') {
				let check_preship_invoice = await validateModel.check_preship_coa(req.user.payload.tyti);
				if (check_preship_invoice) {
					//ACCESS GRANTED
					console.log('ACCESS GRANTED');
				} else {
					err_cnt++;
					err.process_customer_response = "Invalid Access.";
				}
			} else {
				err_cnt++;
				err.process_customer_response = "Invalid Access.";
			}
		} else {
			err_cnt++;
			err.process_customer_response = "Invalid Access.";
		}


		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(401).json(return_err);
		}
	},
	validate_customer_rejection: async (req, res, next) => {
		let err = {};
		let err_cnt = 0;
		//console.log(req.user.payload);
		if(req.user.customer_id){
			if (req.user.payload.need_form != 1) {
				err.reason_of_rejection = "Invalid Access.";
				err_cnt++;
			} else { ///customer validation
				if (req.user.payload.tyds == 'I') {
					let check_preship_invoice = await validateModel.check_preship_invoice(req.user.payload.tyti);
					if (check_preship_invoice) {
						//ACCESS GRANTED
						console.log('ACCESS GRANTED');
					} else {
						err_cnt++;
						err.reason_of_rejection = "Invalid Access.";
					}
				}else if (req.user.payload.tyds == 'C') {
					let check_preship_invoice = await validateModel.check_preship_coa(req.user.payload.tyti);
					if (check_preship_invoice) {
						//ACCESS GRANTED
						console.log('ACCESS GRANTED');
					} else {
						err_cnt++;
						err.process_customer_response = "Invalid Access.";
					}
				} else {
					err_cnt++;
					err.process_customer_response = "Invalid Access.";
				}
			}
		} else {
			err_cnt++;
			err.reason_of_rejection = "Invalid Access.";
		}

		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(401).json(return_err);
		}
	},
}