const Joi = require('joi');
const common        = require('../controllers/common');
const Config   = require('../configuration/config'); 


module.exports = {
    validateBody: (schema) => {
      return (req, res, next) => {
        const result = Joi.validate(req.body, schema,{abortEarly: false});
        if (result.error) {
          let err_msg = {};
          for (let counter in result.error.details) {
  
            let k   = result.error.details[counter].context.key;
            let val = result.error.details[counter].message;
            err_msg[k] = val;
          }
          let return_err = {status:2,errors:err_msg};
          return res.status(400).json(return_err);
        }
  
        if (!req.value) { req.value = {}; }
        req.value['body'] = result.value;
        next();
      }
    },
    validateParam: (schema) => {
      return (req, res, next) => {
  
        const result = Joi.validate(req.params, schema);
        if (result.error) {
    
          let return_err = {status:2,errors:"Invalid argument"};
          return res.status(400).json(return_err);
        }
  
        if (!req.value) { req.value = {}; }
        req.value['params'] = result.value;
        next();
      }
    },
    schemas: {
        check_stock_availability : Joi.object().keys({
          product_name    : Joi.string().optional(),
          product_code    : Joi.number().required(),
          market          : Joi.string().required(),
          quantity        : Joi.number().required().min(1),
          unit            : Joi.string().required(),
          rdd             : Joi.string().required(),
          ship_to_party   : Joi.number().required(),
          additional_comment : Joi.string().optional().allow('').allow(null),
          check_early     : Joi.boolean().required(),
          as_admin        : Joi.number().optional(),
        }),
        reserve_stock :  Joi.object().keys({
          request_id      : Joi.string().required(),
          supply_type     : Joi.number().optional(),
          line_items      : Joi.array().optional().allow('').allow(null),
          line_items_date      : Joi.array().optional().allow('').allow(null)
        }),
        approve_po  :  Joi.object().keys({
          request_id      : Joi.string().required(),
          sold_to_party     : Joi.number().required()          
        }),
        delay_po :  Joi.object().keys({
          request_id      : Joi.string().required()          
        }),
        stock_task_details :  Joi.object().keys({
          id      : Joi.number().required()          
        }),
        more_stock_availability : Joi.object().keys({
          request_id      : Joi.string().required(),
          quantity        : Joi.number().required().min(1),
          unit            : Joi.string().required(),
          rdd             : Joi.string().required(),
        }),
        enquiry_cancel :  Joi.object().keys({
          task_id      : Joi.number().required()
        }),
        remove_reservation : Joi.object().keys({
          request_id      : Joi.string().required(),
          reservation_no  : Joi.number().required().min(1),
        }),
        product_id: Joi.object().keys({
          id      : Joi.number().required()          
        }),
        fetch_invoice: Joi.object().keys({
          so_number    : Joi.string().required().regex(/^[a-zA-z\d]+$/,'Invalid So Number'),
          invoice_no   : Joi.string().required().regex(/^[a-zA-z\d]+$/,'Invalid Invoice No'),
        }),
    }
}