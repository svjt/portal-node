const Joi = require('joi');
const common        = require('../controllers/common');
const Config   = require('../configuration/config'); 
const dateFormat = require('dateformat');
const fs = require("fs");
//s3 bucket
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require("multer");
var trim            = require('trim');
const Entities      = require('html-entities').AllHtmlEntities;
const entities      = new Entities();
var striptags       = require('striptags');
const uuidv4        = require('uuid/v4');
const userModel = require('../models/user');
var path = require("path");

const s3Config = new AWS.S3({
  accessKeyId: Config.aws.accessKeyId,
  secretAccessKey: Config.aws.secretAccessKey,
  Bucket: Config.aws.bucketName
});

var storage_client = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'public/client_uploads')
  },
  filename: function(req, file, callback) {
    var extention = path.extname(file.originalname);
    var new_file_name = (Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    callback(null, new_file_name);
  }
});

var storage_client_discussion = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'public/client_uploads')
  },
  filename: function(req, file, callback) {
    var extention = path.extname(file.originalname);
    var new_file_name = (Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    callback(null, new_file_name);
  }
});

var multerS3Config = multerS3({
  s3: s3Config,
  bucket: Config.aws.bucketName,
  metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    var extention  = path.extname(file.originalname);
    var new_file_name = 'client_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    cb(null, new_file_name)
  }
});

var multerS3ConfigEscalation = multerS3({
  s3: s3Config,
  bucket: Config.aws.bucketName,
  metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    var extention  = path.extname(file.originalname);
    var new_file_name = 'escalation_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    cb(null, new_file_name)
  }
});

module.exports = {
  validateBody: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema,{abortEarly: false});
      if (result.error) {
        let err_msg = {};
        for (let counter in result.error.details) {

          let k   = result.error.details[counter].context.key;
          let val = result.error.details[counter].message;
          err_msg[k] = val;
        }
        let return_err = {status:2,errors:err_msg};
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['body'] = result.value;
      next();
    }
  },
  validateParam: (schema) => {
    return (req, res, next) => {

      const result = Joi.validate(req.params, schema);
      console.log("REQUEST",req.params);
      console.log("RESULT",result);
      if (result.error) {
  
        let return_err = {status:2,errors:"Invalid argument"};
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['params'] = result.value;
      next();
    }
  },
  schemas: {
    escalation:Joi.object().keys({
      content:Joi.string().required().error(() => {
        return {
          message: 'Please enter comment.',
        };
      }),
      file: Joi.optional()
    }),
    task_general_request: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      //product_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      pharmacopoeia:Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      dmf_number: Joi.optional(),
      rdfrc: Joi.optional(),
      notification_number: Joi.optional(),
      apos_document_type: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),

    genotoxic_request: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().required(),
      pharmacopoeia:Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),

    task_vendor_questionnaire: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_spec_moa: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      pharmacopoeia:Joi.string().allow('').allow(null),
      polymorphic_form : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_audit_visit: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      audit_visit_site_name:Joi.string().required(),
      request_audit_visit_date : Joi.string().required(),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    nitrosoamine: Joi.object().keys({
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id: Joi.array().items({
        value: Joi.number().required(),
        label: Joi.string().required()
      }),
      dmf_number: Joi.string().required(),
      rdd : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent')
    }),
    task_tga: Joi.object().keys({
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),    
      gmp_clearance_id: Joi.string().required(),
      tga_email_id:  Joi.string().required(),
      applicant_name:  Joi.string().required(),
      doc_required: Joi.string().required(),     
      rdd : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id: Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent')
    }),
    task_stability_data: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      stability_data_type : Joi.string().required(),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),

    task_new_order: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      // product_id: Joi.array().items(
			// 	Joi.object().keys({
			// 	  value: Joi.number().required(),
      //     label: Joi.string().required()
      //   })
      // ).required(),
      country_id:Joi.string().required(),

      swi_data:Joi.array().items(
				Joi.object().keys({
          product_id     : Joi.number().required(),
          common_err     : Joi.string().allow(''),
          product_name   : Joi.string().required(),
          product_select : Joi.object().keys({
            value : Joi.string(),
            label : Joi.string()
          }).required(),
          rdd          : Joi.string().allow('').allow(null).optional(),
          quantity     : Joi.string().optional(),
          unit         : Joi.array().items(
            Joi.object().keys({
              value : Joi.string(),
              label : Joi.string()
            })
          ).required()
        })
      ),
      po_number: Joi.string().max(100).allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),

    task_proforma: Joi.object().keys({
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().required(),
      quantity : Joi.string().required(),
      rdd : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      pharmacopoeia: Joi.string().allow('').allow(null)
    }),

    task_order_general: Joi.object().keys({
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().required(),
      quantity : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      pharmacopoeia: Joi.string().allow('').allow(null)
    }),

    task_price_quote: Joi.object().keys({
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().required(),
      quantity : Joi.string().allow('').allow(null),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_complaint: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      quantity : Joi.string().allow('').allow(null),
      nature_of_issue : Joi.string().allow('').allow(null),
      batch_number : Joi.string().required(),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_logistics_complaint: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      quantity : Joi.string().allow('').allow(null),
      nature_of_issue : Joi.string().allow('').allow(null),
      batch_number : Joi.string().required(),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_sample_standards: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      product_id: Joi.array().items(
				Joi.object().keys({
				  value: Joi.number().required(),
          label: Joi.string().required()
        })
      ).required(),
      country_id:Joi.string().allow('').allow(null),
      pharmacopoeia:Joi.string().allow('').allow(null),

      swi_data:Joi.array().items(
				Joi.object().keys({

          product_id   : Joi.number().required(),
          product_name : Joi.string().required(),
          sample:Joi.object().keys({
            chk:Joi.boolean().required(),
            batch:Joi.number(),
            quantity:Joi.string().allow(''),
            unit:Joi.array().items(
              Joi.object().keys({
                value: Joi.string(),
                label: Joi.string()
              })
            ).optional()
          }),
          impurities: Joi.object().keys({
            chk:Joi.boolean().required(),
            specify_impurities:Joi.string().allow(''),
            quantity:Joi.string().allow(''),
            unit:Joi.array().items(
              Joi.object().keys({
                value: Joi.string(),
                label: Joi.string()
              })
            ).optional()
          }),
          
          working_standards:Joi.object().keys({
            chk:Joi.boolean().required(),
            quantity:Joi.string().allow(''),
            unit:Joi.array().items(
              Joi.object().keys({
                value: Joi.string(),
                label: Joi.string()
              })
            ).optional()
          }),
          common_err:Joi.string().allow('')

        })
      ),

      shipping_address : Joi.string().allow('').allow(null).optional(),
      description:Joi.string().allow('').allow(null),
      request_type_id:Joi.string().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent')
    }),

    task_forecasts: Joi.object().keys({
      //customer_id: Joi.number().required(),
      //task_id: Joi.number().required(),
      description:Joi.string().allow('').allow(null),
      request_category_type:Joi.number().required(),
      file: Joi.optional(),
      cc_customers: Joi.optional(),
      agent_customer_id: Joi.optional(),
      share_with_agent:Joi.string().required().regex(/^[0|1]$/, 'shared with agent'),
      //cc_customers: Joi.array().optional()
    }),
    task_details: Joi.object().keys({
      id       : Joi.number().required(),  
    }),
    hash_id: Joi.object().keys({
      hash_id       : Joi.string().min(50).regex(/^[a-zA-z\d]+$/,'invalid key').required()
    }),
    ship_doc: Joi.object().keys({
      id       : Joi.string().regex(/^[a-zA-z\d]+$/,'invalid key').required(),
      type     : Joi.string().required(),
    }),
    po_doc: Joi.object().keys({
      sapid    : Joi.string().required(),
    }),
    download_file: Joi.object().keys({
      ciphered_key : Joi.string().regex(/^[a-zA-z\d]+$/,'invalid key').required()
      //inactual_key :  Joi.string().required(), 
    }),
    add_discussion: Joi.object().keys({
      task_id : Joi.number().required(),
      comment : Joi.string().allow('').allow(null),
      file : Joi.optional(),
      owner : Joi.number().required(),
      sap_cancel: Joi.number().optional()
    }),
    closed_discussion: Joi.object().keys({
      task_id : Joi.number().required(),
      comment : Joi.string().allow('').allow(null),
      selected_options:Joi.string().required().regex(/^[1|2]$/, 'option selected'),
      file : Joi.optional()
    }),
    send_email: Joi.object().keys({
      task_attachments: Joi.array().items(
        Joi.object().keys({
          task_discussion_files: Joi.array().items(
            Joi.object().keys({
              actual_file_name: Joi.string().required(),
              chk: Joi.boolean().required(),
              upload_id: Joi.number().required(),
            })
          ),
          task_files: Joi.array().items(
            Joi.object().keys({
              actual_file_name: Joi.string().required(),
              chk: Joi.boolean().required(),
              upload_id: Joi.number().required(),
            })
          )
        })
      ).optional().allow('').allow(null),
      subject:Joi.string().required(),
      notes:Joi.string().allow('').allow(null),
      exclude_task_ref:Joi.boolean().required(),
      email_list: Joi.array().items(Joi.string().email()).required(),
      file: Joi.optional(),
    }),
    edit_rdd: Joi.object().keys({
      rdd:Joi.string().required(),
      temp_id:Joi.number().required()
    }),
    approve_invoice:  Joi.object().keys({
      invoice_number: Joi.string().required().regex(/^([0-9])*$/, 'Invoice must be integer'),
      task_id: Joi.number().required(),
      type: Joi.string().required().regex(/(?:approve|reject)$/, 'option selected'),
      reject_comment: Joi.string().optional(),
    }),
    approve_coa: Joi.object().keys({
      coa: Joi.string().regex(/^[a-zA-z\d]+$/,'invalid key').required(),
      task_id: Joi.number().required(),
      type: Joi.string().required().regex(/(?:approve|reject)$/, 'option selected'),
      reject_comment: Joi.string().optional(),
    }),
  },
  
  schema_posts:{
    general_request: async (req, res, next) => {
      //console.log('task valdiate helper',process.env.NODE_ENV);
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);
          // console.log("file", file);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

        upload(req, res, async function(err) {
          // console.log({err});
          if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
              res.status(400).json({
                status:2,
                errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                ).end()
            }else {
              res.status(400).json({
                status:2,
                errors: {message : err}}
                ).end()
            }
          }else{
            try {
              const requestArr = ['General Request', 'Quality General Request', 'Regulatory General Request', 'Legal General Request']
              if(requestArr.indexOf(req.body.request_type_id) == -1){
                res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
              }
  
              let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
              if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
              }else{
                if(process.env.NODE_ENV == 'production')
                {
                  if(req.files && req.files.length > 0)
                  {
                    for (let index = 0; index < req.files.length; index++) {
                      console.log(req.files[index].key,req.files[index].buffer);
                      req.files[index].filename = req.files[index].key;
                    }
                  }
                }
                next();
              } 
            } catch (error) {
              res.status(400).json({
                status  : 2,
                errors  : error.stack
              }).end();
            }   
          }
        })
    },
    genotoxic: async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

        upload(req, res, async function(err) {
          if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
              res.status(400).json({
                status:2,
                errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                ).end()
            }else {
              res.status(400).json({
                status:2,
                errors: {message : err}}
                ).end()
            }
          }else {
            try {
              const requestArr = ['Genotoxic Impurity Assessment']
              if(requestArr.indexOf(req.body.request_type_id) == -1){
                res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
              }
  
              let result = Joi.validate(req.body, module.exports.schemas.genotoxic_request,{abortEarly:false});
              if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
              }else{
                if(process.env.NODE_ENV == 'production')
                {
                  if(req.files && req.files.length > 0)
                  {
                    for (let index = 0; index < req.files.length; index++) {
                      console.log(req.files[index].key,req.files[index].buffer);
                      req.files[index].filename = req.files[index].key;
                    }
                  }
                }
                next();
              } 
            } catch (error) {
              res.status(400).json({
                status  : 2,
                errors  : error.stack
              }).end();
            } 
          }         
        })
    },
    vendor:  async (req, res, next) => {
      console.log('process.env.NODE_ENV',process.env.NODE_ENV);
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        }else {
          try {
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    spec_moa:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        }else {
          try {
            if(req.body.request_type_id != 'Spec and MOA'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_spec_moa,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    method_queries:  async (req, res, next) => {

      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        }else {
          try {
            if(req.body.request_type_id != 'Method Related Queries'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    typical_coa:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Typical CoA'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_spec_moa,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    rec_coa:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Recertification of CoA'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    audit_visit:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Audit/Visit Request'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_audit_visit,{abortEarly:false});
            
            if (result.error) {
              let err_msg = {};
              for (let counter in result.error.details) {

                let k   = result.error.details[counter].context.key;
                let val = result.error.details[counter].message;
                err_msg[k] = val;
              }
              let return_err = {status:2,errors:err_msg};
              res.status(400).json(return_err).end();
          }else{
            if(process.env.NODE_ENV == 'production')
            {
              if(req.files && req.files.length > 0)
              {
                for (let index = 0; index < req.files.length; index++) {
                  req.files[index].filename = req.files[index].key;
                }
              }
            }
            next();
          }
        } catch (error) {
          res.status(400).json({
            status  : 2,
            errors  : error.stack
          }).end();
        }
        }
      })
    },
    cust_spec:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Customized Spec Request'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    quality_equivalence:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {

            if(req.body.request_type_id != 'Quality Equivalence Request'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    stability:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        }, 
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Stability Data'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_stability_data,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    elementary:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Elemental Impurity (EI) Declaration'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    residual:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Residual Solvents Declaration'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    transport:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Storage and Transport Declaration'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    additional:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {

            if(req.body.request_type_id != 'Request for Additional Declarations'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    cda_sales:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'CDA/Sales Agreement'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    quality_agreement:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Quality Agreement'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    loc_loe:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Request for LOC/LOE'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_vendor_questionnaire,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    request_loa:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {

            if(req.body.request_type_id != 'Request for LOA'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    request_cep:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Request for CEP Copy'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }

            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    request_dmf:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {

          try {
            if(req.body.request_type_id != 'Request for DMF'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    samples:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Samples/Working Standards/Impurities'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_sample_standards,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    new_order:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'New Order Request'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_new_order,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    proforma_invoice:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Request for Proforma Invoice'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_proforma,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    order_general_request:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext!=='.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Order General Request'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_order_general,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    complaints:  async (req, res, next) => {
     
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
            try {
              if(req.body.request_type_id != 'Quality Related Complaints'){
                res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
              }
              let result = Joi.validate(req.body, module.exports.schemas.task_complaint,{abortEarly:false});
              if (result.error) {
                  let err_msg = {};
                  for (let counter in result.error.details) {

                    let k   = result.error.details[counter].context.key;
                    let val = result.error.details[counter].message;
                    err_msg[k] = val;
                  }
                  let return_err = {status:2,errors:err_msg};
                  res.status(400).json(return_err).end();
              }else{
                if(process.env.NODE_ENV == 'production')
                {
                  if(req.files && req.files.length > 0)
                  {
                    for (let index = 0; index < req.files.length; index++) {
                      req.files[index].filename = req.files[index].key;
                    }
                  }
                }
                next();
              }
            } catch (error) {
              res.status(400).json({
                status  : 2,
                errors  : error.stack
              }).end();
            }
          }
      })
    },
    forecast:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {           
            let result = Joi.validate(req.body, module.exports.schemas.task_forecasts,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    dmf_queries:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Queries related to DMF'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    deficiency:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Queries related to Deficiencies'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    notification_queries:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Queries related to Notifications'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    qos:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Quality Overall Summary (QOS)'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    price_quote:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext!=='.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Request for a Price Quote'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_price_quote,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    ip:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'IP Related Enquiries'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    file_agreement:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Master File Agreement'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    logistics_complaints:  async (req, res, next) => {
     
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
            try {
              if(req.body.request_type_id != 'Logistics Related Complaints'){
                res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
              }
              let result = Joi.validate(req.body, module.exports.schemas.task_logistics_complaint,{abortEarly:false});
              if (result.error) {
                  let err_msg = {};
                  for (let counter in result.error.details) {

                    let k   = result.error.details[counter].context.key;
                    let val = result.error.details[counter].message;
                    err_msg[k] = val;
                  }
                  let return_err = {status:2,errors:err_msg};
                  res.status(400).json(return_err).end();
              }else{
                
                if(process.env.NODE_ENV == 'production')
                {
                  if(req.files && req.files.length > 0)
                  {
                    for (let index = 0; index < req.files.length; index++) {
                      req.files[index].filename = req.files[index].key;
                    }
                  }
                }
                next();
              }
            } catch (error) {
              res.status(400).json({
                status  : 2,
                errors  : error.stack
              }).end();
            }
          }
      })
    },
    apostallation:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();
          //console.log("file extension", ext);

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        }, 
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'Apostallation of Documents'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_general_request,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {

                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production')
              {
                if(req.files && req.files.length > 0)
                {
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    add_discussion: async(req, res, next) => {
      try{
          // const posted_by = req.user.employee_id;
          // const task_id   = req.value.params.id;

          var upload = multer({
            storage: Config.environment == 'local' ? storage_client_discussion : multerS3Config,//bucket storage will be there if env production and qa
            fileFilter: function(req, files, callback) {
              var ext = path.extname(files.originalname).toLowerCase();
              //console.log("file extension", ext);

              if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

                callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
              }
              else
              {
                callback(null, true)
              }
              
            },
            limits: { fileSize: Config.maxFileUploadSize }
          }).array('file',20);

          upload(req, res, async function(err) {
            if (err) {
              if (err.code == 'LIMIT_FILE_SIZE') {
                res.status(400).json({
                  status:2,
                  errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                  ).end()
              }else {
                res.status(400).json({
                  status:2,
                  errors: {message : err}}
                  ).end()
              }
            } else {
              //VALIDATE USING JOI
              const result = Joi.validate(req.body, module.exports.schemas.add_discussion,{abortEarly:false});
              if (result.error) {
                  let err_msg = {};
                  for (let counter in result.error.details) {

                    let k   = result.error.details[counter].context.key;
                    let val = result.error.details[counter].message;
                    err_msg[k] = val;
                  }
                  let return_err = {status:2,errors:err_msg};
                  res.status(400).json(return_err).end();
              }else{
                  // CUSTOM VALIDATIONS
                  if(process.env.NODE_ENV == 'production')
                    {
                        for (let index = 0; index < req.files.length; index++) {
                          req.files[index].filename = req.files[index].key;
                          
                        }
                    }
                  let cust_err = {};
                  let err_cnt  = 0;
                  const {comment} = req.body;

                  //COMMENT    
                  console.log('comment',comment);
                  var comment_content = trim(entities.decode(striptags(comment,['img'])));
                  if(comment_content == ''){
                    cust_err.comment = 'Comment cannot be blank.';
                    err_cnt++;
                  }

                  // let emp_details = await employeeModel.get_user(posted_by);

                  // //     //CHECK DESIGNATION
                  //     if(emp_details[0].desig_id == 1 || emp_details[0].desig_id == 2 || emp_details[0].desig_id == 3 || emp_details[0].desig_id == 5){
                  //       //CHECK CLOSE TASK
                  //       let current_assignemnt = await taskModel.validate_discussion(task_id,posted_by);
                  //       if(current_assignemnt.length > 0){
                  //         //DO NOTHING
                  //         //console.log('validation success');
                  //       }else{
                  //         cust_err.comment = 'Please check task ownership.';
                  //         err_cnt++;
                  //       }
                  //     }else{
                  //       cust_err.comment = 'Please check task ownership.';
                  //       err_cnt++;
                  //     } 

                  if(err_cnt == 0){
                    if(process.env.NODE_ENV == 'production')
                      {
                        if(req.files.length > 0)
                        {
                          for (let index = 0; index < req.files.length; index++) {
                            req.files[index].filename = req.files[index].key;
                          }
                        }
                      }

                    next();
                  }else{
                    res.status(400).json({
                      errors  : cust_err,
                      status  : 2
                    }).end();
                  }
              }
            }
          });  
      }catch(err){
          common.logError(err);
          res.status(400).json({
            status  : 3,
            message : Config.errorText.value
          });
      }        
    },
    nitrosoamine:  async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'N-Nitrosoamines Declarations'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.nitrosoamine,{abortEarly:false});
            
            if (result.error) {
              let err_msg = {};
              for (let counter in result.error.details) {

                let k   = result.error.details[counter].context.key;
                let val = result.error.details[counter].message;
                err_msg[k] = val;
              }
              let return_err = {status:2,errors:err_msg};
              res.status(400).json(return_err).end();
          }else{
            if(process.env.NODE_ENV == 'production')
            {
              if(req.files && req.files.length > 0)
              {
                for (let index = 0; index < req.files.length; index++) {
                  req.files[index].filename = req.files[index].key;
                }
              }
            }
            next();
          }
        } catch (error) {
          res.status(400).json({
            status  : 2,
            errors  : error.stack
          }).end();
        }
        }
      })
    },
    tga: async (req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            if(req.body.request_type_id != 'TGA GMP Clearance Documents'){
              res.status(400).json({status:2,errors: 'Invalid request submitted'}).end();
            }
            let result = Joi.validate(req.body, module.exports.schemas.task_tga,{abortEarly:false});
            
            if (result.error) {
              let err_msg = {};
              for (let counter in result.error.details) {

                let k   = result.error.details[counter].context.key;
                let val = result.error.details[counter].message;
                err_msg[k] = val;
              }
              let return_err = {status:2,errors:err_msg};
              res.status(400).json(return_err).end();
          }else{
            if(process.env.NODE_ENV == 'production')
            {
              if(req.files && req.files.length > 0)
              {
                for (let index = 0; index < req.files.length; index++) {
                  req.files[index].filename = req.files[index].key;
                }
              }
            }
            next();
          }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    escalation_files:async(req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3ConfigEscalation,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {
            let result = Joi.validate(req.body, module.exports.schemas.escalation,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production'){
                if(req.files && req.files.length > 0){
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    task_attachments:async(req, res, next) => {
      var upload = multer({
        storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
        fileFilter: function(req, file, callback) {
          var ext = path.extname(file.originalname).toLowerCase();

          if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {

            callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
          }
          else
          {
            callback(null, true)
          }
          
        },
        limits: { fileSize: Config.maxFileUploadSize }
      }).array('file',20);

      upload(req, res, async function(err) {
        if (err) {
          if (err.code == 'LIMIT_FILE_SIZE') {
            res.status(400).json({
              status:2,
              errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
              ).end()
          }else {
            res.status(400).json({
              status:2,
              errors: {message : err}}
              ).end()
          }
        } else {
          try {

            let result = Joi.validate(req.body, module.exports.schemas.send_email,{abortEarly:false});
            if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
            }else{
              if(process.env.NODE_ENV == 'production'){
                if(req.files && req.files.length > 0){
                  for (let index = 0; index < req.files.length; index++) {
                    req.files[index].filename = req.files[index].key;
                  }
                }
              }
              next();
            }
          } catch (error) {
            res.status(400).json({
              status  : 2,
              errors  : error.stack
            }).end();
          }
        }
      })
    },
    closed_discussion: async(req, res, next) => {
      try{
          // const posted_by = req.user.employee_id;
          // const task_id   = req.value.params.id;
  
          var upload = multer({
            storage: Config.environment == 'local' ? storage_client_discussion : multerS3Config,//bucket storage will be there if env production and qa
            fileFilter: function(req, files, callback) {
              var ext = path.extname(files.originalname).toLowerCase();
              //console.log("file extension", ext);
  
              if (ext !== '.zip' && ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {
  
                callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
              }
              else
              {
                callback(null, true)
              }
              
            }, 
            limits: { fileSize: Config.maxFileUploadSize }
          }).array('file',20);
  
          upload(req, res, async function(err) {
            if (err) {
              if (err.code == 'LIMIT_FILE_SIZE') {
                res.status(400).json({
                  status:2,
                  errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                  ).end()
              }else {
                res.status(400).json({
                  status:2,
                  errors: {message : err}}
                  ).end()
              }
            } else {
              //VALIDATE USING JOI
              const result = Joi.validate(req.body, module.exports.schemas.closed_discussion,{abortEarly:false});
              if (result.error) {
                  let err_msg = {};
                  for (let counter in result.error.details) {
  
                    let k   = result.error.details[counter].context.key;
                    let val = result.error.details[counter].message;
                    err_msg[k] = val;
                  }
                  let return_err = {status:2,errors:err_msg};
                  res.status(400).json(return_err).end();
              }else{
                  // CUSTOM VALIDATIONS
                  if(process.env.NODE_ENV == 'production')
                    {
                        for (let index = 0; index < req.files.length; index++) {
                          req.files[index].filename = req.files[index].key;
                          
                        }
                    }
                  let cust_err = {};
                  let err_cnt  = 0;
                  const {comment} = req.body;
  
                  //COMMENT    
                  console.log('comment',comment);
                  var comment_content = trim(entities.decode(striptags(comment,['img'])));
                  if(comment_content == ''){
                    cust_err.comment = 'Comment cannot be blank.';
                    err_cnt++;
                  }
  
                  if(err_cnt == 0){
                    if(process.env.NODE_ENV == 'production')
                      {
                        if(req.files.length > 0)
                        {
                          for (let index = 0; index < req.files.length; index++) {
                            req.files[index].filename = req.files[index].key;
                          }
                        }
                      }
  
                    next();
                  }else{
                    res.status(400).json({
                      errors  : cust_err,
                      status  : 2
                    }).end();
                  }
              }
            }
          });  
      }catch(err){
          common.logError(err);
          res.status(400).json({
            status  : 3,
            message : Config.errorText.value
          });
      }        
    }
  },
  log_request:async(req, res, next) => {
    console.log(req.value);
    return false;
    var log_now         = common.calcTime();
    var log_date_format = dateFormat(log_now, "yyyy-mm-dd HH:MM:ss");

    var log_err = `\n DateTime: ${log_date_format} \n ${JSON.stringify(req.value.body)} \n`;

    var log_file = `log/complain_request${dateFormat(log_now, "mm_yyyy")}.txt`;

    fs.appendFile(log_file,log_err, (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
    next();
  }
}