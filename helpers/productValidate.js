const Joi = require("joi");
const common = require("../controllers/common");
const Config = require("../configuration/config");
const dateFormat = require("dateformat");
const fs = require("fs");
const multer = require("multer");
var trim = require("trim");
const Entities = require("html-entities").AllHtmlEntities;
const entities = new Entities();
var striptags = require("striptags");
const uuidv4 = require("uuid/v4");
var path = require("path");

module.exports = {
  validateBody: (schema) => {
    //console.log('validation')
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema, {
        abortEarly: false,
      });
      if (result.error) {
        let err_msg = {};
        for (let counter in result.error.details) {
          let k = result.error.details[counter].context.key;
          let val = result.error.details[counter].message;
          err_msg[k] = val;
        }
        let return_err = {
          status: 2,
          errors: err_msg,
        };
        return res.status(400).json(return_err);
      }

      if (!req.value) {
        req.value = {};
      }
      req.value["body"] = result.value;
      next();
    };
  },
  validateParam: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.params, schema);
      if (result.error) {
        let return_err = {
          status: 2,
          errors: "Invalid argument",
        };
        return res.status(400).json(return_err);
      }

      if (!req.value) {
        req.value = {};
      }
      req.value["params"] = result.value;
      next();
    };
  },
  schemas: {
    product_details: Joi.object().keys({
      product_id: Joi.number().required(),
    }),
    search_products: Joi.object().keys({
      title: Joi.string().optional().allow("").allow(null),
      development_status: Joi.array().optional(),
      api_technology: Joi.array().optional(),
      dose_form: Joi.array().optional(),
      therapy_category: Joi.array().optional(),
    }),
  },
};
