const db =require('../configuration/dbConn');
const common = require('../controllers/common');
const userModel     = require("../models/user");
const agentModel = require('../models/agents');
const sapModel = require('../models/sap');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


module.exports = {	
	
	products_details :async (req, res, next)=> {
		var product_id = req.query.id;
		let err_count  = 0;

		if(product_id && product_id != ''){

			let check_product = await sapModel.get_products_details(product_id);
			if(check_product){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},

	product_shipto_exist :async (req, res, next)=> {
		var shipto_id = req.value.params.shipto_id;
		let err_count  = 0;

		if(shipto_id && shipto_id != ''){

			let check_product = await sapModel.get_customer_products_details(shipto_id);
			if(check_product){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},

	product_code_exist :async (req, res, next)=> {
		let product_id = req.value.params.id;
		let err_count  = 0;

		if(product_id && product_id != ''){

			let check_product = await sapModel.get_products_code(product_id);
			if(check_product){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},

	shipto_exist :async (req, res, next)=> {
		var shipto_id = req.value.params.shipto_id;
		let err_count  = 0;

		if(shipto_id && shipto_id != ''){

			let check_shipto = await sapModel.get_customer_shipto_details(shipto_id);
			if(check_shipto){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},

	request_exist :async (req, res, next)=> {
		const request_id = req.body.request_id;
		let err_count  = 0;
		let cust_id = 0;
		if(req.user.role == 2){
			cust_id = req.body.agent_customer_id;
		}else{
			cust_id = req.user.customer_id;
		}

		if(request_id && request_id != ''){

			let check_request = await sapModel.sap_request_exist(request_id,cust_id);
			if(check_request){				
				//ACCESS GRANTED
				console.log('ACCESS GRANTED');				
			}else{
				err_count++;
			}
				
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Product not found'};
			return res.status(400).json(return_err);
		}

	},

	check_stock_availability: async (req,res,next) => {
		const {
			product_name,
			product_code,
			market,
			quantity,
			productunit,
			rdd,
			ship_to_party,
			additional_comment,
			as_admin,
			cc_customers,
			share_with_agent
		} = req.body;

		  	let err = {};
		  	let err_cnt = 0;
		  	let cust_id = 0;
			if(req.user.role == 2){
				cust_id = req.body.agent_customer_id;
			}else{
				cust_id = req.user.customer_id;
			}

		  if (common.isEmptyObj(err)) {			

			if(req.user.role == 2){
				let agent_customer_id = req.body.agent_customer_id;
				let agent_id = req.user.customer_id;
				let agent_details = await agentModel.get_agent_company(agent_id);
				if(agent_customer_id && agent_customer_id > 0 ){
					//console.log('agent_customer_id',agent_customer_id);
					let customer_arr = await userModel.get_user(agent_customer_id);
					//console.log('customer_arr',customer_arr);
					if(customer_arr && customer_arr.length > 0){
						//DO NOTHING
						let company_arr = []; 
						for (let index = 0; index < agent_details.length; index++) {
							const element = agent_details[index];
							company_arr.push(element.company_id);
						}
						//console.log(customer_arr[0].company_id,company_arr);
						if(!common.inArray(customer_arr[0].company_id,company_arr)){
							err.product_id = "Invalid customer details.";
							err_cnt++;
						}

					}else{
						err.product_id = "Invalid customer details.";
						err_cnt++;
					}
				}else{
					err.product_id = "Please select a user.";
					err_cnt++;
				}
			}

			if(err_cnt == 0){
				if(cc_customers){
					let customer_details = await userModel.get_user(cust_id);
					
					// let pre_selected = await module.exports.get_pre_selected_cc_list(cust_id);

					// let not_in_cust_arr = [cust_id];
					// for (let index = 0; index < pre_selected.length; index++) {
					// 	const element = pre_selected[index];
					// 	not_in_cust_arr.push(element.customer_id);
					// }

					let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,req.user.role,req.user.customer_id);
					
					let db_cc = await userModel.get_cc_cust_company(company_arr,cust_id);

					if(db_cc && db_cc.length > 0){
						let db_cc_arr = [];
						for (let index = 0; index < db_cc.length; index++) {
							const element = db_cc[index];
							db_cc_arr.push(element.customer_id);
						}

						let cc_cust_arr = JSON.parse(cc_customers);

						for (let index = 0; index < cc_cust_arr.length; index++) {
							const element = cc_cust_arr[index].customer_id;

							if(!common.inArray(element,db_cc_arr)){
								err.cc_customers = "Invalid CC List.";
								err_cnt++;
								break;
							}

						}
					}
				}

				var ship_to_party_data = await sapModel.validate_ship_to_party(ship_to_party, cust_id);				
				if (ship_to_party_data.length > 0) {
					//DO NOTHING
				} else {
					err.product_code = "Invalid ShipTo Address.";
					err_cnt++;
				} 

				var product_data = await sapModel.validate_product(product_code,ship_to_party, cust_id);				
				if (product_data.length > 0) {
					//DO NOTHING
				} else {
					err.product_code = "Invalid Product.";
					err_cnt++;
				}

				if( typeof market == "number"){					
					let country_data = await sapModel.get_country_by_id(market);					
					if (country_data.length > 0) {
					//DO NOTHING
					} else {
						err.market = "Invalid Country.";
						err_cnt++;						
					}
				} else {
					err.market = "Invalid Country.";
					err_cnt++;					
				}
				
				if(quantity == 0){
					err.quantity = "Quantity should be greater than 0.";
					err_cnt++;
				}else if (typeof quantity != "number") {
					err.quantity = "Please enter numeric value.";
					err_cnt++;
				}

				if (productunit) {
					if (!common.inArray(productunit.toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
						err.productunit = "Invalid units.";
						err_cnt++;
					}
				} else {
					err.productunit = "Invalid units.";
					err_cnt++;
				}
			}
		  }

		  if (err_cnt == 0) {
			next();
		  } else {
			let return_err = { 'status': 2, 'errors': err };
			return res.status(400).json(return_err).end();
		  }
	},

	reserve_stock: async (req,res,next) => {
		const {
			request_id,
			supply_type,
			line_items,
			line_items_date
		  } = req.body;

		let err = {};
		let err_cnt = 0;

		let cust_id = 0;
		if(req.user.role == 2){
			cust_id = req.body.agent_customer_id;
		}else{
			cust_id = req.user.customer_id;
		}

		if (common.isEmptyObj(err)) {
			if(supply_type==9){
				let check_request = await sapModel.sap_request_exist(request_id,cust_id);
				if(check_request){				
					//ACCESS GRANTED
					console.log('ACCESS GRANTED');				
				}else{
					err_count++;
				}
			}else{
				var request_data = await sapModel.getResevation(request_id, cust_id);			
				if(request_data.success){
					//DO NOTHING
				} else {
					err.request_id = "Invalid reservation for stock.";
					err_cnt++;
				}
			}
		}
		
		if (err_cnt == 0) {
			next();
		} else {
			let return_err = { 'status': 2, 'errors': err };
			return res.status(400).json(return_err).end();
		}
	},

	more_stock_availability: async (req,res,next) => {
		const {
			request_id,
			quantity,
			productunit,
			rdd
		  } = req.body;

		let err = {};
		let err_cnt = 0;
		let cust_id = 0;
		if(req.user.role == 2){
			cust_id = req.body.agent_customer_id;
		}else{
			cust_id = req.user.customer_id;
		}

		if (common.isEmptyObj(err)) {
			console.log(request_id +","+ cust_id)
			var request_data = await sapModel.getLastRequest(request_id, cust_id);
			if (typeof request_id != "number") {			
				if(request_data.success){
					//DO NOTHING
				} else {
					err.request_id = "Invalid reservation for stock.";
					err_cnt++;
				}
			} else {
				err.request_id = "Invalid request reserve.";
				err_cnt++;
			}

			if(quantity == 0){ console.log('qty',quantity);
				err.quantity = "Quantity should be greater than 0.";
				err_cnt++;
			}else if (typeof quantity != "number") {
				err.quantity = "Please enter numeric value.";
				err_cnt++;
			}

			if (productunit) {console.log('unit',productunit);
				if (!common.inArray(productunit.toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
					err.productunit = "Invalid units.";
					err_cnt++;
				}
			} else {
				err.productunit = "Invalid units.";
				err_cnt++;
			}
		}
		console.log(err_cnt);
		if (err_cnt == 0) {
			next();
		} else {
			let return_err = { 'status': 2, 'errors': err };
			return res.status(400).json(return_err).end();
		}
	},

	__get_company_list: async (company_id,role,agent_id) => {
		let parent_company_id = await userModel.get_parent_company(company_id);

		let company_arr = [];
		if(parent_company_id > 0){
			let associate_company = await userModel.get_child_company(parent_company_id);
			let in_agent_arr = [];
			if(role == 2){
				let agent_company_list = await agentModel.list_company(agent_id);
				for (let index = 0; index < agent_company_list.length; index++) {
					const element = agent_company_list[index];
					in_agent_arr.push(element.company_id);
				}
			}
			for (let index = 0; index < associate_company.length; index++) {
				const element = associate_company[index];
				if(role == 2){
					if(in_agent_arr.length > 0 && common.inArray(element.company_id,in_agent_arr)){
						company_arr.push(element.company_id);
					}
				}else{
					company_arr.push(element.company_id);
				}
			}
		}else{
			company_arr.push(company_id);
		}
		return company_arr;
	},
}

