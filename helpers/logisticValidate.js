const Joi = require('joi');
const dateFormat = require('dateformat');
const multer = require('multer');
const logisticModel = require("../models/logistics");
//s3 bucket
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const uuidv4 = require('uuid/v4');
var path = require('path');
const Config = require('../configuration/config');
const common = require('../controllers/common');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


module.exports = {
    validateBody: (schema) => {
      return (req, res, next) => {
  
        const result = Joi.validate(req.body, schema, {
          abortEarly: false
        });
        if (result.error) {
          let err_msg = {};
         
          // const logistics = module.exports.schema_posts.lite_validate();
          // console.log("logistic log id : ",logistics);
          for (let counter in result.error.details) {
  
            let k = result.error.details[counter].context.key;
            let val = result.error.details[counter].message;
            err_msg[k] = val;
          }
          let return_err = {
            status: 2,
            errors: err_msg
          };
          return res.status(400).json(return_err);
        }
  
        if (!req.value) {
          req.value = {};
        }
        req.value['body'] = result.value;
        next();
      }
    },
    validateParam: (schema) => {
      return (req, res, next) => {
        const result = Joi.validate(req.params, schema);
        if (result.error) {
          let return_err = '';
          if (typeof req.params.task_ref != undefined) {
            return_err = {
              status: 2,
              errors: "Please enter a valid task reference number"
            };
          } else {
            return_err = {
              status: 2,
              errors: "Invalid argument"
            };
          }
          return res.status(400).json(return_err);
        }
  
        if (!req.value) {
          req.value = {};
        }
        req.value['params'] = result.value;
        next();
      }
    }, 
    schemas : {
        liteSchema :Joi.object().keys({
            statustracking :Joi.array().items(
                Joi.object().keys({
                    Shipment: Joi.object().keys({
                        SenderID: Joi.string().max(50).required(),
                        ReceiverID: Joi.string().max(20).required(), 
                        WaybillNo: Joi.string().max(12).required(),
                        RefNo: Joi.string().required().max(20),
                        Prodcode :  Joi.string().required().max(1),
                        SubProductCode :  Joi.string().required().max(1).allow(''),
                        Feature :  Joi.string().required().max(1).allow(''),
                        Origin :  Joi.string().required().max(25).allow(''),
                        OriginAreaCode :  Joi.string().required().max(3),
                        Destination :  Joi.string().required().max(25),
                        DestinationAreaCode :  Joi.string().required().max(3),
                        PickUpDate :  Joi.string().required(),
                        PickUpTime :  Joi.string().required().max(4),
                        Weight :  Joi.string().required(),
                        ShipmentMode :  Joi.string().required().max(3),
                        ExpectedDeliveryDate :  Joi.string().required(),
                        DynamicExpectedDeliveryDate : Joi.string().optional().max(25).allow(""),
                        CustomerCode : Joi.string().optional().max(25).allow(""),
                        Scans : Joi.object().keys({
                          ScanDetail : Joi.array().items(
                            Joi.object().keys({
                              ScanType: Joi.string().required().max(2).allow(''),
                              ScanGroupType: Joi.string().required().max(2),
                              ScanCode: Joi.string().required().max(3),
                              Scan: Joi.string().required().max(50),
                              ScanDate: Joi.string().required(),
                              ScanTime: Joi.string().required().max(4),
                              ScannedLocationCode: Joi.string().required().max(3),
                              ScannedLocation : Joi.string().required().max(25),
                              ScannedLocationStateCode : Joi.string().required().max(2),
                              ScannedLocationCity : Joi.string().optional().max(25),
                              SorryCardNumber  : Joi.string().optional().max(25).allow(''),
                              ReachedDestinationLocation  : Joi.string().optional().max(10),
                              SecureCode  : Joi.string().optional().max(25).allow(''),
                              StatusTimeZone : Joi.string().required().max(4),
                              Comments : Joi.string().required().max(50).allow(""),
                              StatusLatitude : Joi.string().required().max(25).allow(""),
                              StatusLongitude : Joi.string().required().max(25).allow("")                        
                            })
                          ).min(1).max(20),
                          DeliveryDetails : Joi.object().keys({
                            ReceivedBy : Joi.string().required().max(30).allow(''),
                            Relation : Joi.string().required().max(30).allow(''),
                            IDType : Joi.string().optional().max(2).allow(''),
                            IDNumber : Joi.string().required().max(20).allow(''),
                            IDImage : Joi.string().required().allow(''),
                            Signature : Joi.string().required().allow(''),
                            SecurityCodeDelivery: Joi.string().optional().allow('')
                          }),
                          PODDCImages : Joi.object().keys({
                            PODImage : Joi.string().optional().allow(""),
                            DCImage : Joi.string().optional().allow("")
                          }).optional(),
                          FieldExecutiveDetails : Joi.object().keys({
                            FeName : Joi.string().optional().allow(""),
                            FeMobileNo :Joi.string().optional().allow(""),
                            Feactivity : Joi.string().optional().allow("")
                          }).optional(),
                          Reweigh : Joi.object().keys({
                            MPSNumber : Joi.string().optional().allow(""),
                            RWActualWeight : Joi.string().optional().allow(""),
                            RWLength : Joi.string().optional().allow(""),
                            RWBreadth : Joi.string().optional().allow(""),
                            RWHeight : Joi.string().optional().allow(""),
                            RWVolWeight : Joi.string().optional().allow("")
                          }).optional(),
                          CallLogs : Joi.object().keys({
                            Message : Joi.string().optional(),
                            LogDate : Joi.string().optional(),
                            LogTime : Joi.string().optional()
                          }).optional()
                        })
                        
                    })
                }))
        })
        
    },
    schema_posts: {
        lite_validate : async (req, res, next) => {
            try { 
              next();  
            }catch (err) {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              }}
    }
  }