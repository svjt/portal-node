const db            = require('../configuration/dbConn');
const Config 		= require('../configuration/config');
const taskmodel     = require('../models/task');
const feedModel     = require("../models/feed");
const userModel     = require("../models/user");
const agentModel    = require('../models/agents');
const taskModel     = require('../models/task');
const validateModel = require('../models/validate');
const Entities      = require('html-entities').AllHtmlEntities;
const entities      = new Entities();
const common        = require('../controllers/common');
var trim            = require('trim');
const Cryptr 		= require('cryptr');
const cryptr 		= new Cryptr(Config.cryptR.secret);
var Hashids        	= require('hashids');
var hashids        	= new Hashids('', 50);

const AWS = require('aws-sdk');


module.exports = {

	tasks: async (req,res,next) => {
		const {
			product_id,
			swi_data,
			country_id,
			customer_id,
			request_type_id,
			pharmacopoeia,
			polymorphic_form,
			description,
			stability_data_type,
			audit_visit_site_name,
			request_audit_visit_date,
			nature_of_issue,
			batch_number,
			quantity,
			// unit,

			rdd,
			po_number,
			gmp_clearance_id,
			tga_email_id,
			applicant_name,
			doc_required,
			apos_document_type,

			shipping_address,
			cc_customers,
			agent_customer_id
		} = req.body;
		// const {
		// 	product_id,
		// 	country_id,
		// 	pharmacopoeia,
		// 	description,
		// 	request_type_id,
		// 	//files,
		// 	submitted_by,

		// 	// customer_id,
		// 	// task_id,
		// 	// product_name,
		// 	// country,
		// 	// pharmacopoeia,
		// 	// polymorphic_form,
		// 	// description,
		// 	// request_type_id,
		// 	// request_category_type,
		// 	// submitted_by,
		// 	// cc_customers
		// } = req.body;

		let err = {};

		// const drupalExists = await extModel.drupal_exists(customer_id);
		// if (drupalExists.length==1){
		// 	req.body.customer_id = drupalExists[0].customer_id;
		// }else{
		// 	err.customer_id="Invalid customer id";
		// }
		// const productArray = await extModel.product_exists(entities.encode(product_name));
		// if (productArray.length==1){
		// 	req.body.product_id = productArray[0].product_id;
		// }else{
		// 	err.product_name="Invalid product name";
		// }

		//console.log(request_type_id);

		const requestArray = await taskmodel.request_exists(request_type_id);

		//console.log(requestArray);

		if (requestArray.length == 1) {
			req.body.request_id = requestArray[0].type_id;
		} else {
			err.request_type_id = "Invalid request type posted";
		}

		

		if (common.isEmptyObj(err)) {

			let err_cnt = 0;
			if (err_cnt == 0) {

				if(req.user.role == 2){
					
					let agent_id = req.user.customer_id;
					let agent_details = await agentModel.get_agent_company(agent_id);
					if(agent_customer_id && agent_customer_id > 0 ){
						//console.log('agent_customer_id',agent_customer_id);
						let customer_arr = await userModel.get_user(agent_customer_id);
						//console.log('customer_arr',customer_arr);
						if(customer_arr && customer_arr.length > 0){
							//DO NOTHING
							let company_arr = []; 
							for (let index = 0; index < agent_details.length; index++) {
								const element = agent_details[index];
								company_arr.push(element.company_id);
							}
							//console.log(customer_arr[0].company_id,company_arr);
							if(!common.inArray(customer_arr[0].company_id,company_arr)){
								err.product_id = "Invalid customer details.";
								err_cnt++;
							}

						}else{
							err.product_id = "Invalid customer details.";
							err_cnt++;
						}
					}else{
						err.product_id = "Please select a user.";
						err_cnt++;
					}
				}

				if (err_cnt == 0) {

					if(cc_customers){

						if(req.user.role == 2){
							var cust_id = agent_customer_id;
						}else{
							var cust_id = req.user.customer_id;
						}
						let customer_details = await userModel.get_user(cust_id);
						
						// let pre_selected = await module.exports.get_pre_selected_cc_list(cust_id);

						// let not_in_cust_arr = [cust_id];
						// for (let index = 0; index < pre_selected.length; index++) {
						// 	const element = pre_selected[index];
						// 	not_in_cust_arr.push(element.customer_id);
						// }

						let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,req.user.role,req.user.customer_id);
						
						let db_cc = await userModel.get_cc_cust_company(company_arr,cust_id);

						if(db_cc && db_cc.length > 0){
							let db_cc_arr = [];
							for (let index = 0; index < db_cc.length; index++) {
								const element = db_cc[index];
								db_cc_arr.push(element.customer_id);
							}

							let cc_cust_arr = JSON.parse(cc_customers);

							for (let index = 0; index < cc_cust_arr.length; index++) {
								const element = cc_cust_arr[index].customer_id;

								if(!common.inArray(element,db_cc_arr)){
									err.product_id = "Invalid CC List.";
									err_cnt++;
									break;
								}

							}
						}
					}

					//CHECK 24 FORMS
					if (req.body.request_id == 1) {
						//console.log('here');
						let product_arr = JSON.parse(product_id);
						//console.log('product_arr',product_arr);
						for (let index = 0; index < product_arr.length; index++) {
							const element = product_arr[index];
							//console.log('value',element.value);
							var product_data = await feedModel.validate_product(element.value);

							if (product_data.length > 0) {
								//DO NOTHING
							} else {
								err.product_id = "Invalid Product.";
								err_cnt++;
							}
						}
					}else if (req.body.request_id == 24 || req.body.request_id == 23) {
						//DO NOTHING
					} else {
						let product_arr = JSON.parse(product_id);
						//console.log('product_arr',product_arr);
						for (let index = 0; index < product_arr.length; index++) {
							const element = product_arr[index];
							//console.log('value',element.value);
							var product_data = await feedModel.validate_product(element.value);

							if (product_data.length > 0) {
								//DO NOTHING
							} else {
								err.product_id = "Invalid Product.";
								err_cnt++;
							}
						}
					}

					if (req.body.request_id == 1) {
						let err = 0;
						let swi_data_arr = JSON.parse(swi_data);
						for (let index = 0; index < swi_data_arr.length; index++) {
							const element = swi_data_arr[index];
							let err_arr = [];

							var product_data = await feedModel.validate_product(element.product_id);

							if (product_data.length > 0) {
								//DO NOTHING
							} else {
								err_arr.push(`Invalid Product.`);
								err++;
							}
					  
							if(element.sample.chk === false && element.impurities.chk === false && element.working_standards.chk === false ){
							  err_arr.push(`Please select atleast one Samples/Working Standards/Impurities.`);
							  err++;
							}else{
							  let numbers = /^(\d*\.)?\d+$/;
							  if(element.sample.chk === true){
					  
								if(+element.sample.batch === 0){
								  err_arr.push(`Samples: Invalid no of batch.`);
								  err++;
								}else if(!common.inArray(+element.sample.batch,[1,2,3])){
								  err_arr.push(`Samples: Invalid no of batch.`);
								  err++;
								}
					  
								if(element.sample.quantity != '' || element.sample.unit.length > 0){
								  if(element.sample.quantity === 0){
									err_arr.push(`Samples: Quantity should be greater than 0.`);
									err++;
								  }else if(!element.sample.quantity.match(numbers)){
									err_arr.push(`Samples: Invalid quantity.`);
									err++;
								  }
					  
								  if(element.sample.unit.length > 0){
									if(!common.inArray(element.sample.unit[0].value.toLowerCase(), ["mgs","gms","kgs","vials"])){
									  err_arr.push("Samples: Invalid units.");
									  err++;
									}
								  }else{
									err_arr.push("Samples: Please enter units.");
									err++;
								  }
					  
								}
								
							  }
					  
							  if(element.impurities.chk === true){
								if(element.impurities.quantity != '' || element.impurities.unit.length > 0){
								  if(element.impurities.quantity === 0){
									err_arr.push(`Impurities: Quantity should be greater than 0.`);
									err++;
								  }else if(!element.impurities.quantity.match(numbers)){
									err_arr.push(`Impurities: Invalid quantity`);
									err++;
								  }
								  if(element.impurities.unit.length > 0){
									if(!common.inArray(element.impurities.unit[0].value.toLowerCase(), ["mgs","gms","kgs","vials"])){
									  err_arr.push("Impurities: Invalid units.");
									  err++;
									}
								  }else{
									err_arr.push("Impurities: Please enter units.");
									err++;
								  }
					  
								}else{
								  err_arr.push("Impurities: Invalid quantity/units.");
								  err++;
								}
							  }
					  
							  if(element.working_standards.chk === true){
								if(element.working_standards.quantity != '' || element.working_standards.unit.length > 0){
								  if(element.working_standards.quantity === 0){
									err_arr.push(`Working Standards: Quantity should be greater than 0.`);
									err++;
								  }else if(!element.working_standards.quantity.match(numbers)){
									err_arr.push(`Working Standards: Invalid quantity`);
									err++;
								  }
					  
								  if(element.working_standards.unit.length > 0){
									if(!common.inArray(element.working_standards.unit[0].value.toLowerCase(), ["mgs","gms","kgs","vials"])){
									  err_arr.push("Working Standards: Invalid units.");
									  err++;
									}
								  }else{
									err_arr.push("Working Standards: Please enter units.");
									err++;
								  }  
					  
								}else{
								  err_arr.push("Working Standards: Invalid quantity/units.");
								  err++;
								}
							  }
							}
					  
							if(err_arr.length > 0){
							  //err_arr.push(`${element.product_name}`);
							  element.common_err = `<li>${err_arr.reverse().join('</li><li>')}<li>`;
							}
							
						}


						if(err > 0){
							err.SWI_ERR = JSON.stringify(swi_data_arr);
							err_cnt++;
						}  
						

						//SHIPPING ADDRESS
						if (shipping_address == "" || shipping_address.trim() == "") {
							err.shippingAddress = "Please enter shipping address.";
							err_cnt++;
						}

						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 2) {
						//TYPICAL COA
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 3) {
						//SPECS AND MOA
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 4) {
						//Method Related Queries
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 5) {
						//Vendor Questionnaire
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 6) {
						//CDA/Sales Agreement
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 7) {
						//COMPLINTS
						var right_format = /[a-zA-Z]{4}\d{6}/;
						if (batch_number == "" || batch_number.trim() == "") {
							err.batch_number = "Please enter batch number.";
							err_cnt++;
						} else if (batch_number.trim().length < 10) {
							err.batch_number = "Batch Number cannot be less than 10 characters.";
							err_cnt++;
						} else if (!batch_number.match(right_format) || batch_number.trim().length != 10) {
							err.batch_number = "Batch Number field is not in the right format (ex: ABCD000000).";
							err_cnt++;
						}

						//QUANTITY
						// || (unit && trim(unit) != "")
								let array_sample = quantity.split(" ");
								var numbers = /^(\d*\.)?\d+$/;

								if ((array_sample[0] && array_sample[0].trim() != "") || (array_sample[1] && array_sample[1].trim() != "")) {

									if (array_sample[0]) {
										if(array_sample[0] == 0){
											err.quantity = "Quantity should be greater than 0.";
											err_cnt++;
										}else if (!array_sample[0].match(numbers)) {
											err.quantity = "Please enter numeric value.";
											err_cnt++;
										}
									} else {
										err.quantity = "Quantity cannot be blank.";
										err_cnt++;
									}

									if (array_sample[1]) {
										if (!common.inArray(array_sample[1].toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
											err.quantity = "Invalid units.";
											err_cnt++;
										}
									} else {
										err.quantity = "Invalid units.";
										err_cnt++;
									}

								}
						// if ((quantity && trim(quantity) != "")) {
						// 	var numbers = /^[0-9]*$/;
						// 	if (quantity) {
						// 		// if (!quantity.match(numbers)) {
						// 		// 	err.quantity = "Invalid quantity.";
						// 		// 	err_cnt++;
						// 		// }
						// 	} else {
						// 		err.quantity = "Invalid quantity.";
						// 		err_cnt++;
						// 	}

						// 	// if (unit) {
						// 	// 	if (!common.inArray(unit.toLowerCase(), ["mgs", "gms", "kgs"])) {
						// 	// 		err.unit = "Invalid units.";
						// 	// 		err_cnt++;
						// 	// 	}
						// 	// } else {
						// 	// 	err.unit = "Invalid units.";
						// 	// 	err_cnt++;
						// 	// }
						// }

						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}

					} else if (req.body.request_id == 8) {
						//Recertification of CoA
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 9) {
						//AUDIT VISIT DATE
						//AUDIT VISIT SITE NAME
						//console.log('audit_visit_site_name',trim(audit_visit_site_name));
						if (audit_visit_site_name == "" || audit_visit_site_name.trim() == "") {
							err.audit_visit_site_name = "Please enter site name.";
							err_cnt++;
						}
						//AUDIT VISIT DATE
						if (request_audit_visit_date == null) {
							err.request_audit_visit_date = "Audit/Visit date required.";
							err_cnt++;
						}
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 10) {
						//Customized Spec Request
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 11) {
						//Quality Agreement
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 12) {
						//Quality Equivalence Request
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 13) {
						//console.log('stability data ====>', req.body);
						//Stability Data
						if (stability_data_type == "" || stability_data_type.trim() == "") {
							err.stability_data_type = "Please select data type.";
							err_cnt++;
						} else if (
							stability_data_type && !common.inArray(stability_data_type.toLowerCase(), [
								"acc",
								"long term",
								"others"
							])
						) {
							err.stability_data_type = "Invalid data type.";
							err_cnt++;
						}

						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 14) {
						//Elemental Impurity (EI) Declaration
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 15) {
						//Residual Solvents Declaration
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 16) {
						//Storage and Transport Declaration
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 17) {
						//Request for DMF
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 18) {
						//Request for CEP Copy
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 19) {
						//Request for LOA
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 20) {
						//Request for LOC/LOE
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 21) {
						//Request for Additional Declarations
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 22) {
						//General Request
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 23) {
						//New Order Request
						console.log('I am Here Order');
						let err = 0;
						let product_rdd = [];
						let swi_data_arr = JSON.parse(swi_data);
						for (let index = 0; index < swi_data_arr.length; index++) {
							const element = swi_data_arr[index];
							let err_arr = [];

							if(product_rdd.length > 0){
								let arr = product_rdd.filter((val)=>{
								  return (val.product_id == element.product_id && val.rdd == element.rdd)
								});
				  
								if(arr.length > 0){
								  err_arr.push(`Please select a different RDD for this product.`);
								  err++;
								}
				  
							}

							var product_data = await feedModel.validate_product(element.product_id);

							if (product_data.length > 0) {
								//DO NOTHING
							} else {
								err_arr.push(`Invalid Product.`);
								err++;
							}

							if(element.rdd == "" ||element.rdd == null){
								err_arr.push(`RDD is mandatory.`);
								err++;
							  }
					  

							let numbers = /^(\d*\.)?\d+$/;
							if(element.quantity != '' || element.unit.length > 0){
								if(element.quantity === 0){
									err_arr.push(`Quantity should be greater than 0.`);
									err++;
								}else if(!element.quantity.match(numbers)){
									err_arr.push(`Invalid quantity.`);
									err++;
								}
					
								if(element.unit.length > 0){
								if(!common.inArray(element.unit[0].value.toLowerCase(), ["mgs","gms","kgs","vials"])){
									err_arr.push("Invalid units.");
									err++;
								}
								}else{
									err_arr.push("Please enter units.");
									err++;
								}
					
							}else{
								err_arr.push(`Please enter quantity.`);
								err++;
							}

							product_rdd.push({index:index,product_id:element.product_id,rdd:element.rdd});
					  
							if(err_arr.length > 0){
							  //err_arr.push(`${element.product_name}`);
							  element.common_err = `<li>${err_arr.reverse().join('</li><li>')}<li>`;
							}
							
						}


						if(err > 0){
							err.SWI_ERR = JSON.stringify(swi_data_arr);
							err_cnt++;
						} 
						//console.log('swi error',err_cnt);
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						} else {
							err.country_id = "Please select country.";
							err_cnt++;
						}
						//console.log('country',err_cnt);
						
						//FILES
						// || req.outlook_attachments != ''
						if (req.files) {
							// || req.outlook_attachments != ''
							if (req.files.length > 0) {

							} else {
								err.file_name = "Attach PO field is required.";
								err_cnt++;
							}
						} else {
							err.file_name = "Attach PO field is required.";
							err_cnt++;
						}
						err_cnt = 0;
						//console.log('files',err_cnt);

					} else if (req.body.request_id == 24) {

						if ((description == "" || description.trim() == "") && (typeof req.files == 'undefined' || req.files.length == 0)) {
							err.file_name = "Attachment is required.";
							err_cnt++;
						}
					} else if (req.body.request_id == 28) {
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 29) {
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 31) {
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
						//QUANTITY
						let numbers = /^(\d*\.)?\d+$/;
						// unit && 
						let array_sample = quantity.split(" ");

						if ((array_sample[0] && array_sample[0].trim() != "") || (array_sample[1] && array_sample[1].trim() != "")) {

							if (array_sample[0]) {
								if (!array_sample[0].match(numbers)) {
									err.quantity = "Invalid quantity.";
									err_cnt++;
								}
							} else {
								err.quantity = "Invalid quantity.";
								err_cnt++;
							}

							if (array_sample[1]) {
								if (!common.inArray(array_sample[1].toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
									err.quantity = "Invalid units.";
									err_cnt++;
								}
							} else {
								err.quantity = "Invalid units.";
								err_cnt++;
							}

						}
					} else if (req.body.request_id == 32) {
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if (req.body.request_id == 33) {
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
						//FILES
						if (req.files) {
							if (req.files.length > 0) {

							} else {
								err.file_name = "Attach PO field is required.";
								err_cnt++;
							}
						} else {
							err.file_name = "Attach PO field is required.";
							err_cnt++;
						}
					} else if (req.body.request_id == 34) {
						//COMPLINTS
						var right_format = /[a-zA-Z]{4}\d{6}/;
						if (batch_number == "" || batch_number.trim() == "") {
							err.batch_number = "Please enter batch number.";
							err_cnt++;
						} else if (batch_number.trim().length < 10) {
							err.batch_number = "Batch Number cannot be less than 10 characters.";
							err_cnt++;
						} else if (!batch_number.match(right_format) || batch_number.trim().length != 10) {
							err.batch_number = "Batch Number field is not in the right format (ex: ABCD000000).";
							err_cnt++;
						}

						//QUANTITY
						// || (unit && trim(unit) != "")
						let array_sample = quantity.split(" ");
						var numbers = /^(\d*\.)?\d+$/;

						if ((array_sample[0] && array_sample[0].trim() != "") || (array_sample[1] && array_sample[1].trim() != "")) {

							if (array_sample[0]) {
								if(array_sample[0] == 0){
									err.quantity = "Quantity should be greater than 0.";
									err_cnt++;
								}else if (!array_sample[0].match(numbers)) {
									err.quantity = "Please enter numeric value.";
									err_cnt++;
								}
							} else {
								err.quantity = "Quantity cannot be blank.";
								err_cnt++;
							}

							if (array_sample[1]) {
								if (!common.inArray(array_sample[1].toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
									err.quantity = "Invalid units.";
									err_cnt++;
								}
							} else {
								err.quantity = "Invalid units.";
								err_cnt++;
							}

						}
						// if ((quantity && trim(quantity) != "")) {
						// 	var numbers = /^[0-9]*$/;
						// 	if (quantity) {
						// 		// if (!quantity.match(numbers)) {
						// 		// 	err.quantity = "Invalid quantity.";
						// 		// 	err_cnt++;
						// 		// }
						// 	} else {
						// 		err.quantity = "Invalid quantity.";
						// 		err_cnt++;
						// 	}

						// 	// if (unit) {
						// 	// 	if (!common.inArray(unit.toLowerCase(), ["mgs", "gms", "kgs"])) {
						// 	// 		err.unit = "Invalid units.";
						// 	// 		err_cnt++;
						// 	// 	}
						// 	// } else {
						// 	// 	err.unit = "Invalid units.";
						// 	// 	err_cnt++;
						// 	// }
						// }

						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}

					} else if(req.body.request_id == 35){
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
						
					} else if(req.body.request_id == 36){
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if(req.body.request_id == 37){
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					} else if(req.body.request_id == 38) {

						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}

						if(apos_document_type && apos_document_type != ''){
							var parsed_apos_document_type = JSON.parse(apos_document_type);
							for (let index = 0; index < parsed_apos_document_type.length; index++) {
							  if(!common.inArray(trim(parsed_apos_document_type[index].value),['Site GMP','Site Manufacturing License','Written Confirmation'])){
								err.country_id="Invalid document type posted";
								err_cnt++;
								break;
							  }
							}
						}
					} else if (req.body.request_id == 42) {
						//TYPICAL COA
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}
					}  else if (req.body.request_id == 44) {
						//Order General Request
						//COUNTRY
						if(country_id && country_id != ''){
							var parsed_country_id = JSON.parse(country_id);
							for (let index = 0; index < parsed_country_id.length; index++) {
								if( typeof parsed_country_id[index].value == "number"){	
									//console.log('typeof number success');
									// COUNTRY CHECKING SATYAJIT
									  let country_data = await feedModel.validate_country(parsed_country_id[index].value);
									
									  if (country_data.length > 0) {
									  //DO NOTHING
									  } else {
										  err.country_id = "Invalid Country.";
										  err_cnt++;
										  break;
									  }
								  } else {
									  err.country_id = "Invalid Country.";
									  err_cnt++;
									  break;
								  }
							}
						}

						if(quantity && quantity != ''){
							let array_sample = quantity.split(" ");
							var numbers = /^(\d*\.)?\d+$/;

							if ((array_sample[0] && array_sample[0].trim() != "") || (array_sample[1] && array_sample[1].trim() != "")) {

								if (array_sample[0]) {
									if(array_sample[0] == 0){
										err.quantity = "Quantity should be greater than 0.";
										err_cnt++;
									}else if (!array_sample[0].match(numbers)) {
										err.quantity = "Please enter numeric value.";
										err_cnt++;
									}
								} else {
									err.quantity = "Quantity cannot be blank.";
									err_cnt++;
								}

								if (array_sample[1]) {
									if (!common.inArray(array_sample[1].toLowerCase(), ["mgs", "gms", "kgs","vials"])) {
										err.quantity = "Invalid units.";
										err_cnt++;
									}
								} else {
									err.quantity = "Invalid units.";
									err_cnt++;
								}

							}
						}
					} 
				}
			}

			console.log(err_cnt);

			if (err_cnt == 0) {
				next();
			} else {
				let return_err = { status: 2, errors: err };
				//======== ERROR MAIL ==========
				// var mail_req = req.body;
				// mail_req.files = [];
				// common.sendMail({
				//   from: Config.webmasterMail, // sender address
				//   to: "soumyadeep@indusnet.co.in", // list of receivers
				//   subject: `URL || ${Config.drupal.url} || Task DB Faliure`, // Subject line
				//   html: `Error: ${JSON.stringify(
				//     return_err
				//   )} <br> Request Body: ${JSON.stringify(mail_req)}` // plain text body
				// });
				//======== ERROR MAIL ==========
				return res.status(400).json(return_err);
			}
			//next()
		} else {
			console.log('here error');
			let return_err = { 'status': 2, 'errors': err };
			return res.status(400).json(return_err).end();
		}

		// if(country){
		// 	const countryArray = await extModel.country_exists(entities.encode(country));
		//     if (countryArray.length==1){
		//     	req.body.country_id = countryArray[0].country_id;
		//     }else{
		//     	err.country="Invalid country posted";
		//     }
		// }else{
		// 	req.body.country_id = 0;
		// 	}	   

		// 	if(submitted_by && submitted_by > 0){
		// 		const agentArray = await extModel.agent_exists(submitted_by);
		// 			if (agentArray.length==1){
		// 				req.body.agent_id = agentArray[0].agent_id;
		// 			}else{
		// 				err.submitted_by="Invalid value for submitted_by";
		// 			}
		// 	}else{
		// 		req.body.agent_id = 0;
		// 	}

		// 	var ccc_arr = [];
		// 	if(cc_customers && cc_customers.length > 0){

		// 		for (var index = 0; index < cc_customers.length; index++) {
		// 			const elam_cc = cc_customers[index];
		// 			if(elam_cc > 0){

		// 				const drupalCCExists = await extModel.drupal_exists(elam_cc);
		// 				if (drupalCCExists.length == 1 && drupalCCExists[0].customer_id >0){

		// 					if(drupalExists[0].customer_id != drupalCCExists[0].customer_id){

		// 						var cust_arr = [drupalExists[0].customer_id,drupalCCExists[0].customer_id];

		// 						const companyArr = await extModel.check_drupal_company(cust_arr);
		// 						if(companyArr[0].company_id == companyArr[1].company_id){
		// 							ccc_arr.push(drupalCCExists[0].customer_id);
		// 						}else{
		// 							err.cc_customers="CC'd customers do not belong to same company.";
		// 							break;
		// 						}
		// 					}else{
		// 						err.cc_customers="main customer should not be in CC'd customer.";
		// 						break;
		// 					}
		// 				}else{
		// 					err.cc_customers="Invalid customer_drupal_id provided.";
		// 					break;
		// 				}
		// 			}else{
		// 				err.cc_customers="Invalid array provided. Contains customer_drupal_id 0 in array.";
		// 				break;
		// 			}
		// 		}
		// 	}

		// if(isEmpty(err)){     
		// 		req.body.cc_customers = ccc_arr;
		// 	next();
		// }else{
		// 	let return_err = {'status':2,'errors':err};
		// 	//======== ERROR MAIL ==========
		// 	var mail_req = req.body;
		// 	mail_req.files = [];
		// 	common.sendMail({
		// 	  from   : Config.webmasterMail, // sender address
		// 	  to     : 'soumyadeep@indusnet.co.in', // list of receivers
		// 	  subject: `URL || ${Config.drupal.url} || Ext DB Faliure`, // Subject line
		// 	  html   : `Error: ${JSON.stringify(return_err)} <br> Request Body: ${JSON.stringify(mail_req)}`// plain text body
		// 	});
		// 	//======== ERROR MAIL ==========
		//      return res.status(400).json(return_err);
		// }

	},
	forecast:async (req,res,next) => {
		const {
			cc_customers,
			agent_customer_id
		} = req.body;

		let err = {};
		let err_cnt = 0;

		if(req.user.role == 2){
			
			if(agent_customer_id && agent_customer_id > 0 ){
				let customer_arr = await userModel.get_user(agent_customer_id);
				if(customer_arr && customer_arr.length > 0){
					//DO NOTHING
				}else{
					err.customer_name = "Invalid customer details.";
					err_cnt++;
				}
			}else{
				err.customer_name = "Please select a user.";
				err_cnt++;
			}
		}

		if (err_cnt == 0) {

			if(cc_customers){

				if(req.user.role == 2){
					var customer_id = agent_customer_id;
				}else{
					var customer_id = req.user.customer_id;
				}
				let customer_details = await userModel.get_user(customer_id);

				// let pre_selected = await module.exports.get_pre_selected_cc_list(customer_id);

				// let not_in_cust_arr = [customer_id];
				// for (let index = 0; index < pre_selected.length; index++) {
				// 	const element = pre_selected[index];
				// 	not_in_cust_arr.push(element.customer_id);
				// }

				let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,req.user.role,req.user.customer_id);
				
				let db_cc = await userModel.get_cc_cust_company(company_arr,customer_id);

				if(db_cc && db_cc.length > 0){
					let db_cc_arr = [];
					for (let index = 0; index < db_cc.length; index++) {
						const element = db_cc[index];
						db_cc_arr.push(element.customer_id);
					}

					let cc_cust_arr = JSON.parse(cc_customers);

					for (let index = 0; index < cc_cust_arr.length; index++) {
						const element = cc_cust_arr[index].customer_id;

						if(!common.inArray(element,db_cc_arr)){
							err.product_id = "Invalid CC List.";
							err_cnt++;
							break;
						}

					}
				}
			}
			
		}

		if (err_cnt == 0) {
			next();
		}else{
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	},
	add_discussion :async (req,res,next)=> {
		let err_count   = 0;
	    const {task_id} = req.body;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);
		
		if(check_task_id){
			if(req.user.role == 2){
				//AGENT
				let agent_companies = await agentModel.list_company(req.user.customer_id);
				if(agent_companies && agent_companies.length > 0){
					let company_arr = [];
					for (let index = 0; index < agent_companies.length; index++) {
						const element = agent_companies[index];
						company_arr.push(element.company_id);
					}
					let agent_customers = await agentModel.list_company_customers_arr(company_arr);
					if(agent_customers && agent_customers.length > 0){
						let customer_arr = [];
						for (let index = 0; index < agent_customers.length; index++) {
							const element = agent_customers[index];
							customer_arr.push(element.customer_id);
						}

						let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
						if(check_task_details){
							console.log('TASK DETAILS ACCESS GRANTED');
						}else{
							err_count++;
						}
					}else{
						err_count++;
					}
				}else{
					err_count++;
				}
			}else{
				//CUSTOMER
				let customer_arr = await module.exports.__check_roles(req.user.customer_id);

				let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
				if(check_task_details){
					console.log('TASK DETAILS ACCESS GRANTED');
				}else{
					err_count++;
				}
			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'Invalid Access'};
			return res.status(400).json(return_err);
		}

	},
	coa_task_file: async(req,res,next)=>{
		let err_count     = 0;
		const coa_key     = req.value.params.ciphered_key;
		var pattern       = new RegExp('^[a-zA-z0-9]*$');
		var check_id = pattern.test(coa_key);

		if(check_id){
			const invoice_arr    = cryptr.decrypt(req.value.params.ciphered_key).split('#');
      		const type    = req.value.params.type;
      		let invoice_id = (invoice_arr.length >0 ) ? invoice_arr[0] : '';
			let task_id = (invoice_arr.length >0 ) ? invoice_arr[1] : '';
			let coa_id = (invoice_arr.length >0 ) ? invoice_arr[2] : '';
			  
			if(invoice_id && task_id){				
				//console.log(entities.encode(invoice_id) +"==="+ entities.encode(task_id));
				let coaFiles = await taskModel.is_coa_exist(entities.encode(invoice_id), entities.encode(task_id),entities.encode(coa_id));				
				if(coaFiles){
					console.log('COA ACCESS GRANTED');
				}else{
					err_count++;
				}
			}else{
				err_count++;
			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to download the file'};
			return res.status(400).json(return_err);
		}

	},
	ship_doc: async(req,res,next)=>{
		let err_count     = 0;
		const invoce_key     = req.value.params.id;
		var pattern       = new RegExp('^[a-zA-z0-9]*$');
		var check_id = pattern.test(invoce_key);

		if(check_id){
			const invoice_arr    = cryptr.decrypt(req.value.params.id).split('#');
      		const type    = req.value.params.type;
      		let invoice_id = (invoice_arr.length >0 ) ? invoice_arr[0] : '';
			let task_id = (invoice_arr.length >0 ) ? invoice_arr[1] : '';
			  
			if(invoice_id && task_id){
				/*if(req.user.role == 2){
					//AGENT
					let agent_companies = await agentModel.list_company(req.user.customer_id);
					if(agent_companies && agent_companies.length > 0){
						let company_arr = [];
						for (let index = 0; index < agent_companies.length; index++) {
							const element = agent_companies[index];
							company_arr.push(element.company_id);
						}
						let agent_customers = await agentModel.list_company_customers_arr(company_arr);
						if(agent_customers && agent_customers.length > 0){
							let customer_arr = [];
							for (let index = 0; index < agent_customers.length; index++) {
								const element = agent_customers[index];
								customer_arr.push(element.customer_id);
							}
	
							let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
							if(check_task_details){
								console.log('TASK DETAILS ACCESS GRANTED');
							}else{
								err_count++;
							}
						}else{
							err_count++;
						}
					}else{
						err_count++;
					}
				}else{
					//CUSTOMER
					let customer_arr = await module.exports.__check_roles(req.user.customer_id);
	
					//console.log('customer_arr',customer_arr);
	
					let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
					if(check_task_details){
						console.log('TASK DETAILS ACCESS GRANTED');
					}else{
						err_count++;
					}
	
				}*/
				//console.log(entities.encode(invoice_id) +"==="+ entities.encode(task_id));
				let shipFiles = await taskModel.is_invoice_exist(entities.encode(invoice_id), entities.encode(task_id));				
				if(shipFiles){
					console.log('INVOICE ACCESS GRANTED');
				}else{
					err_count++;
				}
			}else{
				err_count++;
			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to download the file'};
			return res.status(400).json(return_err);
		}

	},
	task_details: async(req,res,next)=>{
		let err_count     = 0;
		const task_id     = req.value.params.id;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);

		if(check_task_id){
			if(req.user.role == 2){
				//AGENT
				let agent_companies = await agentModel.list_company(req.user.customer_id);
				if(agent_companies && agent_companies.length > 0){
					let company_arr = [];
					for (let index = 0; index < agent_companies.length; index++) {
						const element = agent_companies[index];
						company_arr.push(element.company_id);
					}
					let agent_customers = await agentModel.list_company_customers_arr(company_arr);
					if(agent_customers && agent_customers.length > 0){
						let customer_arr = [];
						for (let index = 0; index < agent_customers.length; index++) {
							const element = agent_customers[index];
							customer_arr.push(element.customer_id);
						}

						let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
						if(check_task_details){
							console.log('TASK DETAILS ACCESS GRANTED');
						}else{
							err_count++;
						}
					}else{
						err_count++;
					}
				}else{
					err_count++;
				}
			}else{
				//CUSTOMER
				let customer_arr = await module.exports.__check_roles(req.user.customer_id);

				//console.log('customer_arr',customer_arr);

				let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
				if(check_task_details){
					console.log('TASK DETAILS ACCESS GRANTED');
				}else{
					err_count++;
				}

			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the task'};
			return res.status(400).json(return_err);
		}
	},
	task_details_by_id: async(req,res,next)=>{
		let err_count     = 0;
		const task_id     = req.body.task_id;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);
		console.log('check_task_id',check_task_id);

		if(check_task_id){
			//CUSTOMER
			let customer_arr = await module.exports.__check_roles(req.user.customer_id);

			console.log('customer_arr',customer_arr);

			let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
			if(check_task_details){
				console.log('TASK DETAILS ACCESS GRANTED');
			}else{
				err_count++;
			}
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the task'};
			return res.status(400).json(return_err);
		}
	},
	task_files_download: async(req,res,next)=>{
		let err_count     = 0;
		const task_arr    = hashids.decode(req.value.params.hash_id);
		
      	if(task_arr && task_arr.length > 0 && task_arr[0] > 0 ){
			const task_id     = task_arr[0];  
			var pattern       = new RegExp('^[0-9]*$');
			var check_task_id = pattern.test(task_id);

			if(check_task_id){
				let modelT           = require('../models/task');
				let taskDetailsFiles = await modelT.select_task_files(task_id);

				if(taskDetailsFiles && taskDetailsFiles.length > 1){
					console.log('Access Granted...');
				}else{
					err_count++;
				}
			}else{
				err_count++;
			}

			if(err_count == 0){
				next()
			}else{
				let return_err = {'status':5,'errors':'You do not have access to view the task'};
				return res.status(400).json(return_err);
			}	
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the task'};
				return res.status(400).json(return_err);
		}
	},
	download_ship_doc: async(req,res,next)=>{
		let err_count     = 0;
		const invoice_id    = req.value.params.invoice_id;
		const type    = req.value.params.type;
		
      	if(invoice_id && type){
			// let modelT = require('../models/task');
			// let shipFiles = await modelT.select_ship_files(invoice_id);

			// if(shipFiles){
			// 	if(type=='invoice' && shipFiles.invoice_doc==''){
			// 		err_count++;
			// 	}else if(type=='package' && shipFiles.packing_list==''){
			// 		err_count++
			// 	}else if(type=='flight' && shipFiles.flight_booked_doc==''){
			// 		err_count++
			// 	}else if(type=='hawb' && shipFiles.hawb_doc==''){
			// 		err_count++
			// 	}
			// }else{
			// 	err_count++;
			// }
			

			if(err_count == 0){
				next()
			}else{
				let return_err = {'status':5,'errors':'No such file found.'};
				return res.status(400).json(return_err);
			}	
		}else{
			let return_err = {'status':5,'errors':'No such file found.'};
				return res.status(400).json(return_err);
		}
	},
	comment_files_download: async(req,res,next)=>{
		let err_count     = 0;
		const comment_arr    = hashids.decode(req.value.params.hash_id);
		
      	if(comment_arr && comment_arr.length > 0 && comment_arr[0] > 0 ){
			const comment_id     = comment_arr[0];  
			var pattern       = new RegExp('^[0-9]*$');
			var check_comment_id = pattern.test(comment_id);

			if(check_comment_id){
				let modelT    = require('../models/task');
				let files_arr = await modelT.select_discussion_comment_specific_files(comment_id);

				if(files_arr && files_arr.length > 1){
					console.log('Access Granted...');
				}else{
					err_count++;
				}
				
			}else{
				err_count++;
			}

			if(err_count == 0){
				next()
			}else{
				let return_err = {'status':5,'errors':'You do not have access to view the comment'};
				return res.status(400).json(return_err);
			}	
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the comment'};
				return res.status(400).json(return_err);
		}
	},	
	cc_list: async(req,res,next)=>{
		let err_count     = 0;
		var err           = {};
		const task_id     = entities.encode(req.body.task_id);
		var pattern       = new RegExp('^[0-9]*$');
		var pattern_shared = new RegExp('^[0|1]*$');
		var check_task_id = pattern.test(task_id);

		if(check_task_id){
			if(req.user.role == 2){
				//AGENT
				let agent_companies = await agentModel.list_company(req.user.customer_id);
				if(agent_companies && agent_companies.length > 0){
					let company_arr = [];
					for (let index = 0; index < agent_companies.length; index++) {
						const element = agent_companies[index];
						company_arr.push(element.company_id);
					}
					let agent_customers = await agentModel.list_company_customers_arr(company_arr);
					if(agent_customers && agent_customers.length > 0){
						let customer_arr = [];
						for (let index = 0; index < agent_customers.length; index++) {
							const element = agent_customers[index];
							customer_arr.push(element.customer_id);
						}

						let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
						if(check_task_details){
							console.log('TASK DETAILS ACCESS GRANTED');
						}else{
							err.product_id = "Invalid Access.";
							err_count++;
						}
					}else{
						err.product_id = "Invalid Access.";
						err_count++;
					}
				}else{
					err.product_id = "Invalid Access.";
					err_count++;
				}
			}else{
				//CUSTOMER
				let customer_arr = await module.exports.__check_roles(req.user.customer_id);

				if(Object.prototype.hasOwnProperty.call(req.body, 'share_with_agent'))
				{
					if(!pattern_shared.test(req.body.share_with_agent))
					{
						err.product_id = "Invalid Display this request to Agent value.";
						err_count++;
					}
				}

				let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
				if(check_task_details){
					console.log('TASK DETAILS ACCESS GRANTED');
				}else{
					err.product_id = "Invalid Access.";
					err_count++;
				}

			}
		}else{
			err.product_id = "Invalid Access.";
			err_count++;
		}

		const cc_customers = req.body.cc.map(function (x) { 
			return parseInt(x, 10); 
		});
		if(cc_customers){

			let task_details = await taskmodel.tasks_details(task_id);
			var customer_id = task_details[0].customer_id;
			
			let customer_details = await userModel.get_user(customer_id);

			

			let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,req.user.role,req.user.customer_id);

			let db_cc = await userModel.get_cc_cust_company(company_arr,customer_id);
			
			if(db_cc && db_cc.length > 0){
				let db_cc_arr = [];
				for (let index = 0; index < db_cc.length; index++) {
					const element = db_cc[index];
					db_cc_arr.push(parseInt(element.customer_id));
				}
				//console.log('cc_customers',cc_customers);
				//console.log('db_cc_arr',db_cc_arr);
				for (let index = 0; index < cc_customers.length; index++) {
					const element = cc_customers[index];

					if(!common.inArray(parseInt(element),db_cc_arr)){
						err.product_id = "Invalid CC List.";
						err_count++;
						break;
					}

				}
			}
		}  

		if (err_count == 0) {
			next();
		}else{
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	},
	__check_roles: async (customer_id) => {
		let customer_details = await userModel.get_user(customer_id);
		let ret_arr = [];
		//ROLE 0 => Is Admin YES
		if(customer_details[0].role_type == 0){

			let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,1,null);

			let sel_cust_arr = await userModel.get_user_company(company_arr);
			for (let index = 0; index < sel_cust_arr.length; index++) {
				const element = sel_cust_arr[index];
				ret_arr.push(element.customer_id);
			}
	
		//ROLE TYPE 1 => Is Admin NO, Role Type ADMIN  
		}else if(customer_details[0].role_type == 1){

			let company_arr = await module.exports.__get_company_list(customer_details[0].company_id,1,null);

			let sel_roles_arr = await userModel.get_user_role(customer_id);
			let role_arr = [];
			for (let index = 0; index < sel_roles_arr.length; index++) {
				const element = sel_roles_arr[index];
				role_arr.push(element.role_id);
			}
		
			let sel_cust_arr = await userModel.get_user_company_role(company_arr,role_arr);
			for (let index = 0; index < sel_cust_arr.length; index++) {
				const element = sel_cust_arr[index];
				ret_arr.push(element.customer_id);
			}
	
		//ROLE TYPE 2 => Is Admin NO, Role Type REGULAR  
		}else if(customer_details[0].role_type == 2){
		  	ret_arr.push(customer_id);
		}
	
		return ret_arr;
	
	},
	__get_company_list: async (company_id,role,agent_id) => {
		let parent_company_id = await userModel.get_parent_company(company_id);

		let company_arr = [];
		if(parent_company_id > 0){
			let associate_company = await userModel.get_child_company(parent_company_id);
			let in_agent_arr = [];
			if(role == 2){
				let agent_company_list = await agentModel.list_company(agent_id);
				for (let index = 0; index < agent_company_list.length; index++) {
					const element = agent_company_list[index];
					in_agent_arr.push(element.company_id);
				}
			}
			for (let index = 0; index < associate_company.length; index++) {
				const element = associate_company[index];
				if(role == 2){
					if(in_agent_arr.length > 0 && common.inArray(element.company_id,in_agent_arr)){
						company_arr.push(element.company_id);
					}
				}else{
					company_arr.push(element.company_id);
				}
			}
		}else{
			company_arr.push(company_id);
		}
		return company_arr;
	},
	get_pre_selected_cc_list: async(customer_id) =>{
	  let ret_arr = [];
	  let customer_details = await userModel.get_user(customer_id);
	  //ROLE 0 => Is Admin YES
	  if(customer_details[0].role_type == 0){
		ret_arr = await userModel.get_super_users(customer_id,customer_details[0].company_id);
		//ROLE TYPE 1 => Is Admin NO, Role Type ADMIN  
	  }else if(customer_details[0].role_type == 1){
		let super_admins = await userModel.get_super_users(customer_id,customer_details[0].company_id);
		let not_in = [customer_id];
		ret_arr = super_admins;
  
		for (let index = 0; index < super_admins.length; index++) {
		  const element = super_admins[index];
		  not_in.push(element.customer_id);
		}
  
		let cust_role_arr = await userModel.get_user_role(customer_id);
		let role_arr = [];
		for (let index = 0; index < cust_role_arr.length; index++) {
		  const element = cust_role_arr[index];
		  role_arr.push(element.role_id);
		}
  
		let sel_cust_arr = await userModel.get_department_admins(not_in,role_arr,customer_details[0].company_id);
		for (let index = 0; index < sel_cust_arr.length; index++) {
		  const element = sel_cust_arr[index];
		  ret_arr.push(element);
		}
  
	  //ROLE TYPE 2 => Is Admin NO, Role Type REGULAR  
	  }else if(customer_details[0].role_type == 2){
		let super_admins = await userModel.get_super_users(customer_id,customer_details[0].company_id);
		let not_in = [];
		ret_arr = super_admins;
  
		for (let index = 0; index < super_admins.length; index++) {
		  const element = super_admins[index];
		  not_in.push(element.customer_id);
		}
  
		let cust_role_arr = await userModel.get_user_role(customer_id);
		let role_arr = [];
		for (let index = 0; index < cust_role_arr.length; index++) {
		  const element = cust_role_arr[index];
		  role_arr.push(element.role_id);
		}
  
		let sel_cust_arr = await userModel.get_department_admins(not_in,role_arr,customer_details[0].company_id);
		for (let index = 0; index < sel_cust_arr.length; index++) {
		  const element = sel_cust_arr[index];
		  ret_arr.push(element);
		}
  
	  }
  
	  return ret_arr;
	},

	post_rating: async(req,res,next)=>{
		let err_count     = 0;
		var err           = {};
		const {
			task_id,
			rating,
			comment,
			options
		  } = req.body;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);

		if(check_task_id){
			
			if(!common.inArray(rating,[1,2,3,4,5])){
				err_count++;
			}

			for (let index = 0; index < options.length; index++) {
				const element = options[index];

				let check_options = await taskmodel.rating_option_exists(element);

				if(check_options == false){
					err_count++;
					break;
				}
				
			}

			let previously_rated = await taskmodel.check_task_rating(task_id);

			if(previously_rated){
				err_count++;
			}
			

		}else{
			err_count++;
		}
		
		if (err_count == 0) {
			next();
		}else{
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	},
	skip_rating: async(req,res,next)=>{
		let err_count     = 0;
		var err           = {};

		const {
			task_id
		} = req.body;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);

		if(check_task_id){
			//DO NOTHING
		}else{
			err_count++;
		}
		
		if (err_count == 0) {
			next();
		}else{
			let return_err = { status: 2, errors: err };
			return res.status(400).json(return_err);
		}

	},
	task_details_rating: async(req,res,next)=>{
		let err_count     = 0;
		const {task_id}   = req.body;
		var pattern       = new RegExp('^[0-9]*$');
		var check_task_id = pattern.test(task_id);

		if(check_task_id){
			if(req.user.role == 2){
				//AGENT
				let agent_companies = await agentModel.list_company(req.user.customer_id);
				if(agent_companies && agent_companies.length > 0){
					let company_arr = [];
					for (let index = 0; index < agent_companies.length; index++) {
						const element = agent_companies[index];
						company_arr.push(element.company_id);
					}
					let agent_customers = await agentModel.list_company_customers_arr(company_arr);
					if(agent_customers && agent_customers.length > 0){
						let customer_arr = [];
						for (let index = 0; index < agent_customers.length; index++) {
							const element = agent_customers[index];
							customer_arr.push(element.customer_id);
						}

						let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
						if(check_task_details){
							console.log('TASK DETAILS ACCESS GRANTED');
						}else{
							err_count++;
						}
					}else{
						err_count++;
					}
				}else{
					err_count++;
				}
			}else{
				//CUSTOMER
				let customer_arr = await module.exports.__check_roles(req.user.customer_id);

				//console.log('customer_arr',customer_arr);

				let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
				if(check_task_details){
					console.log('TASK DETAILS ACCESS GRANTED');
				}else{
					err_count++;
				}

			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the task'};
			return res.status(400).json(return_err);
		}
	},
	task_details_explicit: async(req,res,next)=>{
		let err_count     = 0;

		const task_arr = hashids.decode(req.params.ciphered_key);

		console.log(task_arr);

		if(task_arr && task_arr.length > 0 && task_arr[0] > 0 ){

			const task_id     = task_arr[0];
			var pattern       = new RegExp('^[0-9]*$');
			var check_task_id = pattern.test(task_id);

			if(check_task_id){
				if(req.user.role == 2){
					//AGENT
					let agent_companies = await agentModel.list_company(req.user.customer_id);
					if(agent_companies && agent_companies.length > 0){
						let company_arr = [];
						for (let index = 0; index < agent_companies.length; index++) {
							const element = agent_companies[index];
							company_arr.push(element.company_id);
						}
						let agent_customers = await agentModel.list_company_customers_arr(company_arr);
						if(agent_customers && agent_customers.length > 0){
							let customer_arr = [];
							for (let index = 0; index < agent_customers.length; index++) {
								const element = agent_customers[index];
								customer_arr.push(element.customer_id);
							}

							let check_task_details = await validateModel.check_task_details_agent(req.user.customer_id,customer_arr,task_id);
							if(check_task_details){
								console.log('TASK DETAILS ACCESS GRANTED');
							}else{
								err_count++;
							}
						}else{
							err_count++;
						}
					}else{
						err_count++;
					}
				}else{
					//CUSTOMER
					let customer_arr = await module.exports.__check_roles(req.user.customer_id);

					//console.log('customer_arr',customer_arr);

					let check_task_details = await validateModel.check_task_details(req.user.customer_id,customer_arr,task_id);
					if(check_task_details){
						console.log('TASK DETAILS ACCESS GRANTED');
					}else{
						err_count++;
					}

				}
			}else{
				err_count++;
			}
		}else{
			err_count++;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the task'};
			return res.status(400).json(return_err);
		}
	},
	check_size: async(req,res,next)=>{
		let err_count     = 0;
		let err = 0;

		const {task_attachments,email_list} = req.body;

		let size = 0;

		let task_attachments_parsed = JSON.parse(task_attachments);

		if(task_attachments_parsed[0].task_files.length > 0){
			for (let index = 0; index < task_attachments_parsed[0].task_files.length; index++) {
				const element = task_attachments_parsed[0].task_files[index];
				if(element.chk === true){
					let s3_key     = await taskModel.select_task_files_send_mail_single(element.upload_id);
					let file_size  = await module.exports.__get_s3_size(s3_key);
					size           = size + file_size;
				}
			
			}
		}

		if(task_attachments_parsed[0].task_discussion_files.length > 0){
			for (let index = 0; index < task_attachments_parsed[0].task_discussion_files.length; index++) {
				const element = task_attachments_parsed[0].task_discussion_files[index];
				if(element.chk === true){
					let s3_key    = await taskModel.select_task_discussion_files_send_mail_single(element.upload_id);
					let file_size = await module.exports.__get_s3_size(s3_key);
					asize         = size + file_size;
				}
			
			}
		}

		let email_list_parsed = JSON.parse(email_list);

		if(email_list_parsed && email_list_parsed.length > 0){
			for (let index = 0; index < email_list_parsed.length; index++) {
				const element = email_list_parsed[index];
				
			}
		}else{
			err_count++;
			err = 2;
		}

		if(size > 15728640){
			err_count++;
			err = 1;
		}

		if(err_count == 0){
			next()
		}else{
			let return_err = '';
			if(err == 1){
				return_err = {'status':5,'errors':'total attachment size exceeds 15 MB.'}
			}else if(err == 2){
				return_err = {'status':5,'errors':'please enter atlest one email address.'}
			}
			return res.status(400).json(return_err);
		}
	},
	__get_s3_size:async (key) => {
		AWS.config.update({
		  accessKeyId: Config.aws.accessKeyId,
		  secretAccessKey: Config.aws.secretAccessKey
		});
		var s3 = new AWS.S3();
  
		var options = {
		  Bucket: Config.aws.bucketName,
		  Key: key
		};
		console.log(options);
		return new Promise((resolve, reject) => {
			s3.headObject(options, function (err, data) {
				if(err === null){
				console.log('size',data.ContentLength);
				resolve(data.ContentLength);
				}else{
				resolve(null);
				}
		  	});
		});
	},
	internal_discussion_files_download: async(req,res,next)=>{
		let err_count     = 0;
		const comment_arr    = hashids.decode(req.value.params.hash_id);
		
      	if(comment_arr && comment_arr.length > 0 && comment_arr[0] > 0 ){
			const comment_id     = comment_arr[0];  
			var pattern       = new RegExp('^[0-9]*$');
			var check_comment_id = pattern.test(comment_id);

			if(check_comment_id){
				let files_arr = await taskModel.select_comment_specific_files(comment_id);
				console.log(files_arr);
				if(files_arr && files_arr.length > 1){
					console.log('Access Granted...');
				}else{
					err_count++;
				}
				
			}else{
				err_count++;
			}

			if(err_count == 0){
				next()
			}else{
				let return_err = {'status':5,'errors':'You do not have access to view the comment'};
				return res.status(400).json(return_err);
			}	
		}else{
			let return_err = {'status':5,'errors':'You do not have access to view the comment'};
				return res.status(400).json(return_err);
		}
	},
}

