const Joi = require('joi');
const common        = require('../controllers/common');
const Config   = require('../configuration/config');

//s3 bucket
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require("multer");
const uuidv4 = require('uuid/v4');
var path = require("path");

var storage_client = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'public/client_uploads')
  },
  filename: function(req, file, callback) {
    var extention = path.extname(file.originalname);
    var new_file_name = (Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    callback(null, new_file_name);
  }
});

const s3Config = new AWS.S3({
  accessKeyId: Config.aws.accessKeyId,
  secretAccessKey: Config.aws.secretAccessKey,
  Bucket: Config.aws.bucketName
}); 

var multerS3Config = multerS3({
  s3: s3Config,
  bucket: Config.aws.bucketName,
  metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    var extention  = path.extname(file.originalname);
    var new_file_name = 'po_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
    cb(null, new_file_name)
  }
});

module.exports = {
    validateBody: (schema) => {
      return (req, res, next) => {
        const result = Joi.validate(req.body, schema,{abortEarly: false});
        if (result.error) {
          let err_msg = {};
          for (let counter in result.error.details) {
  
            let k   = result.error.details[counter].context.key;
            let val = result.error.details[counter].message;
            err_msg[k] = val;
          }
          let return_err = {status:2,errors:err_msg};
          return res.status(400).json(return_err);
        }
  
        if (!req.value) { req.value = {}; }
        req.value['body'] = result.value;
        next();
      }
    },
    validateParam: (schema) => {
      return (req, res, next) => {
  
        const result = Joi.validate(req.params, schema);
        if (result.error) {
    
          let return_err = {status:2,errors:"Invalid argument"};
          return res.status(400).json(return_err);
        }
  
        if (!req.value) { req.value = {}; }
        req.value['params'] = result.value;
        next();
      }
    },
    schemas: {
        check_stock_availability : Joi.object().keys({
          product_name    : Joi.string().optional(),
          product_code    : Joi.number().required(),
          market          : Joi.string().required(),
          quantity        : Joi.number().required().min(1),
          unit            : Joi.string().required(),
          rdd             : Joi.string().required(),
          ship_to_party   : Joi.number().required(),
          additional_comment : Joi.string().optional().allow('').allow(null),
          check_early     : Joi.boolean().required(),
          as_admin        : Joi.number().optional(),          
        }),
        reserve_stock :  Joi.object().keys({
          request_id      : Joi.string().required(),
          supply_type     : Joi.number().optional(),
          line_items      : Joi.array().optional().allow('').allow(null),
          line_items_date      : Joi.array().optional().allow('').allow(null)
        }),
        check_stock_availability_new : Joi.object().keys({
          product_name    : Joi.string().optional(),
          product_code    : Joi.number().required(),
          market          : Joi.number().required(),
          quantity        : Joi.number().required().min(1),
          productunit     : Joi.string().required(),
          rdd             : Joi.string().required(),
          ship_to_party   : Joi.number().required(),
          additional_comment : Joi.string().optional().allow('').allow(null),
          check_early     : Joi.boolean().required(),
          as_admin        : Joi.number().optional(),
          share_with_agent: Joi.string().optional().regex(/^[0|1]$/, 'shared with agent'),
          cc_customers    : Joi.optional(),
          agent_customer_id : Joi.string().optional().allow('').allow(null)
        }),
        reserve_stock_new :  Joi.object().keys({
          request_id      : Joi.string().required(),
          supply_type     : Joi.number().optional(),
          line_items      : Joi.array().optional().allow('').allow(null),
          line_items_date      : Joi.array().optional().allow('').allow(null),
          agent_customer_id : Joi.string().optional().allow('').allow(null)
        }),
        approve_po  :  Joi.object().keys({
          request_id      : Joi.string().required(),
          sold_to_party     : Joi.number().required(),
          agent_customer_id : Joi.string().optional().allow('').allow(null)          
        }),
        delay_po :  Joi.object().keys({
          request_id      : Joi.string().required() ,
          agent_customer_id : Joi.string().optional().allow('').allow(null)         
        }),
        stock_task_details :  Joi.object().keys({
          id      : Joi.number().required()          
        }),
        more_stock_availability : Joi.object().keys({
          request_id      : Joi.string().required(),
          quantity        : Joi.number().required().min(1),
          productunit     : Joi.string().required(),
          rdd             : Joi.string().required(),
          agent_customer_id : Joi.string().optional().allow('').allow(null)
        }),
        enquiry_cancel :  Joi.object().keys({
          task_id      : Joi.number().required()
        }),
        remove_reservation : Joi.object().keys({
          request_id      : Joi.string().required(),
          reservation_no  : Joi.number().required().min(1),
          agent_customer_id : Joi.string().optional().allow('').allow(null)
        }),
        product_id: Joi.object().keys({
          id      : Joi.number().required()          
        }),
        fetch_invoice: Joi.object().keys({
          so_number    : Joi.string().required().regex(/^[a-zA-z\d]+$/,'Invalid So Number'),
          invoice_no   : Joi.string().required().regex(/^[a-zA-z\d]+$/,'Invalid Invoice No'),
        }),
        shipto_id : Joi.object().keys({
            shipto_id  : Joi.string().required(),  
        }),
        approve_po_request  :  Joi.object().keys({
          request_id      : Joi.string().required(),
          po_no      : Joi.string().required(), 
          agent_customer_id : Joi.string().optional().allow('').allow(null)                
        }),
        customer_shipto :  Joi.object().keys({
          customer_id      : Joi.number().optional()          
        }),
    },

    schema_posts:{
      approve_po_request: async (req, res, next) => {
        //console.log('task valdiate helper',process.env.NODE_ENV);
        var upload = multer({
          storage: Config.environment == 'local' ? storage_client : multerS3Config,//bucket storage will be there if env production and qa
          fileFilter: function(req, file, callback) {
            var ext = path.extname(file.originalname).toLowerCase();
            //console.log("file extension", ext);

            ///if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.doc' && ext !== '.docx' && ext !== '.xls' && ext !== '.xlsx' && ext !== '.pdf' && ext !== '.txt' && ext !== '.odt' && ext !== '.rtf' && ext !== '.wpd' && ext !== '.tex' && ext !== '.wks' && ext !== '.wps' && ext !== '.xlr' && ext !== '.ods' && ext !== '.csv' && ext !== '.ppt' && ext !== '.pptx' && ext !== '.pps' && ext !== '.key' && ext !== '.odp' && ext !== '.ai' && ext !== '.bmp' && ext !== '.ico' && ext !== '.svg' && ext !== '.tif' && ext !== '.tiff' && ext !== '.eml') {
            if (ext !== '.pdf'){
              //callback("Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml", null)
              callback("Only files with the following extensions are allowed: pdf", null)
            }
            else
            {
              callback(null, true)
            }
            
          },
          limits: { fileSize: Config.maxFileUploadSize }
        }).array('file',20);

        upload(req, res, async function(err) {
          if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
              res.status(400).json({
                status:2,
                errors: {message: `File size is too large. Allowed file size is ${Config.maxFileUploadSize / (1024 * 1024)}MB`}}
                ).end()
            }else {
              res.status(400).json({
                status:2,
                errors: {message : err}}
                ).end()
            }
          } else {
            try {            
              let result = Joi.validate(req.body, module.exports.schemas.approve_po_request,{abortEarly:false});
              if (result.error) {
                let err_msg = {};
                for (let counter in result.error.details) {
  
                  let k   = result.error.details[counter].context.key;
                  let val = result.error.details[counter].message;
                  err_msg[k] = val;
                }
                let return_err = {status:2,errors:err_msg};
                res.status(400).json(return_err).end();
              }else{
                if(process.env.NODE_ENV == 'production')
                {
                  if(req.files && req.files.length > 0)
                  {
                    for (let index = 0; index < req.files.length; index++) {
                      //console.log(req.files[index].key,req.files[index].buffer);
                      req.files[index].filename = req.files[index].key;
                    }
                  }
                }
                next();
              } 
            } catch (error) {
              res.status(400).json({
                status  : 2,
                errors  : error.stack
              }).end();
            }
          }          
        })
      },
    }
}