const db = require('../configuration/dbConn');
const userModel = require('../models/user');
const productModel = require('../models/product');
const validateModel = require('../models/validate');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const common = require('../controllers/common');


module.exports = {
	product_details: async (req, res, next) => {
		let err = {};
		let err_cnt = 0;

		//console.log('cb validate', req.body.product_id);

		//let product_exists = await productModel.product_id_exists(req.value.params.product_id);
		let product_exists = await productModel.product_id_exists(req.body.product_id)
		if (product_exists.success) {

		} else {
			err.product = "Invalid product.";
			err_cnt++;
		}

		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(400).json(return_err);
		}
	},
	check_customer: async (req, res, next) => {
		let err = {};
		let err_cnt = 0;

		if (req.user.role == 1 || req.user.role == 2) {
			//DO NOTHING			
		} else {
			err.customer_name = "Invalid customer details.";
			err_cnt++;
		}

		if (err_cnt == 0) {
			next();
		} else {
			let return_err = {
				status: 2,
				errors: err
			};
			return res.status(400).json(return_err);
		}
	}

}