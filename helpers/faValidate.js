const Joi = require('joi');
const common = require('../controllers/common');
const Config = require('../configuration/config');


module.exports = {
  validateBody: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema, { abortEarly: false });
      if (result.error) {
        let err_msg = {};
        for (let counter in result.error.details) {

          let k = result.error.details[counter].context.key;
          let val = result.error.details[counter].message;
          err_msg[k] = val;
        }
        let return_err = { status: 2, errors: err_msg };
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['body'] = result.value;
      next();
    }
  },
  validateParam: (schema) => {
    return (req, res, next) => {

      const result = Joi.validate(req.params, schema);
      if (result.error) {

        let return_err = { status: 2, errors: "Invalid argument" };
        return res.status(400).json(return_err);
      }

      if (!req.value) { req.value = {}; }
      req.value['params'] = result.value;
      next();
    }
  },
  schemas: {
    suggest: Joi.object().keys({
      id: Joi.string().required().regex(/^[1-7]$/, 'id allow only 1-7'),
      txt: Joi.string().required().min(3),
    }),
    search: Joi.object().keys({
      id: Joi.string().required().regex(/^[1-7]$/, 'id allow only 1-7'),
      txt: Joi.string().required().min(3),
      ndc_code: Joi.array().optional(),
      functional_category: Joi.string().allow('').allow(null).optional(),
      unni: Joi.string().allow('').allow(null).optional(),
      similarity_index: Joi.string().allow('').allow(null).optional(),
      organization: Joi.array().optional(),
    }),
    search_tracking: Joi.object().keys({
      enquiry_type: Joi.string().required().regex(/^[1-7]$/, 'id allow only 1-7'),
      product_name: Joi.string().required()
    }),
    task_enquiry: Joi.object().keys({
      id: Joi.number().required()
    }),
    get_excipients: Joi.object().keys({
      ndc_code: Joi.string().required().min(3).max(15)
    }),
    request_more_info: Joi.object().keys({
      enquiry_type: Joi.string().required().regex(/^[1|3|5]$/, 'id allow only 1,3,5'),
      product_name: Joi.string().required(),
      comment: Joi.string().optional(),
    }),
    download_file: Joi.object().keys({
      ciphered_key : Joi.string().regex(/^[a-zA-z\d]+$/,'invalid key').required()
    }),

  }
}