const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const {
  ExtractJwt
} = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const Config = require('./configuration/config');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(Config.cryptR.secret);
const User = require('./models/user');
const Agent = require('./models/agents');
// const empObj         = require('./models/employees');
const BasicStrategy = require('passport-http').BasicStrategy;


isValidPassword = async function (newPassword, existingPassword) {
  try {
    return await bcrypt.compare(newPassword, existingPassword);
  } catch (error) {
    throw new Error(error);
  }
}

passport.use('jwtUser', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {

  try {
    if (!payload.user) {
      return done(null, {
        id: 0
      });
    }

    if (!payload.sub) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.ag) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.role) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.exp) {
      return done(null, {
        id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          id: 0
        });
      }
    }

    if (payload.role == 1) {
      const user = await User.findByUserId(cryptr.decrypt(payload.sub));
      if (user.length > 0) {
        user[0].ag = cryptr.decrypt(payload.ag);
        user[0].role = payload.role;
        user[0].empe = (payload.empe && payload.empe != '') ? cryptr.decrypt(payload.empe) : 0;
        user[0].lang = payload.lang;
        done(null, user[0]);
      }
    } else if (payload.role == 2) {
      const agent = await Agent.findByUserId(cryptr.decrypt(payload.sub));

      if (agent.length > 0) {
        agent[0].ag = cryptr.decrypt(payload.ag);
        agent[0].role = payload.role;
        agent[0].empe = (payload.empe && payload.empe != '') ? cryptr.decrypt(payload.empe) : 0;
        agent[0].customer_id = agent[0].agent_id;
        agent[0].lang = payload.lang;
        done(null, agent[0]);
      }
    } else {
      return done(null, {
        id: 0
      });
    }


  } catch (error) {
    done(error, false);
    // done(null, user[0]);
  }
}));
passport.use('jwtFileDownlode', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token'),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {

  try {
    if (!payload.user) {
      return done(null, {
        id: 0
      });
    }

    if (!payload.sub) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.ag) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.role) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.exp) {
      return done(null, {
        id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          id: 0
        });
      }
    }

    if (payload.role == 1) {
      const user = await User.findByUserId(cryptr.decrypt(payload.sub));
      if (user.length > 0) {
        user[0].ag = cryptr.decrypt(payload.ag);
        user[0].role = payload.role;
        user[0].empe = (payload.empe && payload.empe != '') ? cryptr.decrypt(payload.empe) : 0;
        user[0].lang = payload.lang;
        done(null, user[0]);
      }
    } else if (payload.role == 2) {
      const agent = await Agent.findByUserId(cryptr.decrypt(payload.sub));

      if (agent.length > 0) {
        agent[0].ag = cryptr.decrypt(payload.ag);
        agent[0].role = payload.role;
        agent[0].empe = (payload.empe && payload.empe != '') ? cryptr.decrypt(payload.empe) : 0;
        agent[0].customer_id = agent[0].agent_id;
        agent[0].lang = payload.lang;
        done(null, agent[0]);
      }
    } else {
      return done(null, {
        id: 0
      });
    }


  } catch (error) {
    done(error, false);
    // done(null, user[0]);
  }
}));


passport.use('shadowUser', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {
  try {

    if (!payload.shdw) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.shde) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.exp) {
      return done(null, {
        id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          id: 0
        });
      }
    }
    const user = await User.findActivatedUserId(payload.shdw);
    const agent = await Agent.findActivatedUserId(payload.shdw);
    //console.log('shadow user',user);
    if (user.length > 0) {
      //user[0].ag = cryptr.decrypt(payload.ag);
      user[0].role = 1;
      user[0].empe = payload.shde;
      done(null, user[0]);
    } else if (agent.length > 0) {
      //agent[0].ag = cryptr.decrypt(payload.ag);
      agent[0].role = 2;
      agent[0].customer_id = agent[0].agent_id;
      agent[0].empe = payload.shde;

      done(null, agent[0]);
    } else {
      return done(null, {
        id: 0
      });
    }

  } catch (error) {
    done(error, false);
    // done(null, user[0]);
  }
}));

passport.use('customerResponse', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {
  try {

    if (!payload.cstm) { // customer_id
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.tyds || (payload.tyds != 'C' && payload.tyds != 'I')) { // type
      return done(null, {
        customer_id: 0
      }); 
    }
    if (!payload.tyti) { // type_id
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.prcr || payload.prcr != 1) { // process_response
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.cr || (payload.cr != 1 && payload.cr != 2)) {  // response
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.exp) {
      return done(null, {
        customer_id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          customer_id: 0
        });
      }
    }

    const user = await User.findByCustomerId(payload.cstm);

    if (user.length > 0) {
      user[0].payload = payload; 
      done(null, user[0]);
    }else {
      return done(null, {
        id: 0
      });
    }
  } catch (error) {
    console.log({error});
    done(error, false);
  }
}));

passport.use('customerResponseSecondStep', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {
  try {

    if (!payload.cstm) { // customer_id
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.tyds || (payload.tyds != 'C' && payload.tyds != 'I')) { // type
      return done(null, {
        customer_id: 0
      }); 
    }
    if (!payload.tyti) { // type_id
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.prcr || payload.prcr != 1) { // process_response
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.cr || (payload.cr != 1 && payload.cr != 2)) {  // response
      return done(null, {
        customer_id: 0
      });
    }
    if (!payload.need_form || (payload.need_form != 1)) {  // Second Step Validation
      return done(null, {
        customer_id: 0
      });
    } 
    if (!payload.exp) {
      return done(null, {
        customer_id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          customer_id: 0
        });
      }
    }

    const user = await User.findByCustomerId(payload.cstm);

    if (user.length > 0) {
      user[0].payload = payload; 
      done(null, user[0]);
    }else {
      return done(null, {
        id: 0
      });
    }
  } catch (error) {
    console.log({error});
    done(error, false);
  }
}));


// passport.use('jwtEmp',new JwtStrategy({
//   jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
//   secretOrKey: Config.jwt.secret
// }, async (payload, done) => {

//   try { 

//     if(!payload.did){
//       return done(null, {id:0});
//     }
//     if(!payload.sid){
//       return done(null, {id:0});
//     }
//     if(!payload.ag){
//       return done(null, {id:0});
//     }
//     if(!payload.exp){
//       return done(null, {id:0});
//     }else{
//       var current_time = Math.round((new Date().getTime())/1000);
//       if(current_time>payload.exp){
//         return done(null, {id:0});
//       }
//     }

//     //const user = await empObj.findById(cryptr.decrypt(payload.did)); 
//     const user = await empObj.findById(payload.did);
//     if (user.length>0) {
//       user[0].ag = cryptr.decrypt(payload.ag);
//       done(null, user[0]);
//     }else{
//       return done(null, {id:0});
//     }

//   } catch(error) {
//     done(null, user[0]);
//   }
// }));


passport.use('localUser', new LocalStrategy(
  async (username, password, done) => {
    try {

      const user = await User.findByUsername(entities.encode(username));
      const agent = await Agent.findByUsername(entities.encode(username));
      if (user.length > 0 && user[0].password != null && user[0].password != '') {
        const isMatch = await isValidPassword(entities.encode(password), user[0].password);
        user[0].role = 1;

        if (!isMatch) {
          return done(null, {
            id: 0
          });
        }
        done(null, user[0]);
      } else if (agent.length > 0 && agent[0].password != null && agent[0].password != '') {
        const isMatch1 = await isValidPassword(entities.encode(password), agent[0].password);
        agent[0].role = 2;
        agent[0].customer_id = agent[0].agent_id;

        if (!isMatch1) {
          return done(null, {
            id: 0
          });
        }
        done(null, agent[0]);
      } else {
        return done(null, {
          id: 0
        });
      }

    } catch (error) {
      done(error, false);
    }
  }
));

passport.use('localUserQA', new LocalStrategy(
  async (username, password, done) => {
    try {

      const user = await User.findByUsernameQA(entities.encode(username));
      if (user.length > 0 && user[0].password != null && user[0].password != '') {
        const isMatch = await isValidPassword(entities.encode(password), user[0].password);
        if (!isMatch) {
          return done(null, {
            id: 0
          });
        }
        done(null, user[0]);
      } else {
        return done(null, {
          id: 0
        });
      }

    } catch (error) {
      done(error, false);
    }
  }
));

passport.use('logisticBasic',new BasicStrategy(
  async (userid, password, done) => {
    try{
      //console.log("user Id"+userid + "password"+ password);
      if(password == Config.BlueDart.password && userid == Config.BlueDart.userid){
        done(null, "xyz");
      }else{
        return done(null,false)
      }   
    }catch (error) {
      done(error, false);
    }
    
  }
));

passport.use('jwtQAUser', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {

  try {
    if (!payload.sub) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.ag) {
      return done(null, {
        id: 0
      });
    }
    if (!payload.exp) {
      return done(null, {
        id: 0
      });
    } else {
      var current_time = Math.round((new Date().getTime()) / 1000);
      if (current_time > payload.exp) {
        return done(null, {
          id: 0
        });
      }
    }

    const user = await User.findByUserQAId(cryptr.decrypt(payload.sub));

    user[0].user_agent = cryptr.decrypt(payload.ag);

    done(null, user[0]);

  } catch (error) {
    done(error, false);
    // done(null, user[0]);
  }
}));

passport.use('SAP',new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Config.jwt.secret
}, async (payload, done) => {

  try { 

    if(!payload.iss){
      return done(null, {id:0});
    }    
    if(!payload.sap || payload.sap != 1){
      return done(null, {id:0});
    }else{
      done(null, 1);
    }
  } catch(error) {
    done(null, false);
  }
}));