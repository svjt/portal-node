const db = require("../configuration/dbConn");
const common = require("../controllers/common");
const Config = require("../configuration/config");

module.exports = {
  /**
   * @desc Add Customer
   * @param
   * @return json
   */
  add_user: async (agent) => {
    return new Promise(function (resolve, reject) {
      x;
      db.one(
        "INSERT INTO tbl_agents(first_name,last_name,email,status,agent_ref,date_added,added_by,drupal_id) VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING agent_id",
        [
          agent.first_name,
          agent.last_name,
          agent.email,
          agent.status,
          agent.agent_ref,
          agent.date_added,
          agent.added_by,
          agent.drupal_id,
        ]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Add Company
   * @param
   * @return json
   */
  add_company: async (agent_id, company_id) => {
    return new Promise(function (resolve, reject) {
      db.one(
        "INSERT INTO tbl_agent_company(agent_id,company_id,status) VALUES($1,$2,$3) RETURNING ag_id",
        [agent_id, company_id, 1]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Check Existence Company
   * @param
   * @return json
   */
  check_company: async (agent_id, company_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "SELECT ag_id from tbl_agent_company where agent_id=($1) and company_id=($2) and status=1",
        [agent_id, company_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update a company
   * @param
   * @return json
   */
  update_company: async (agent_id, company) => {
    return new Promise(function (resolve, reject) {
      const company_string = company.join();

      db.result(
        "UPDATE tbl_agent_company set status='2' where agent_id=($1) and status='1' and company_id  not in(" +
        company +
        ")",
        [agent_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Remove a company
   * @param
   * @return json
   */
  remove_company: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agent_company set status='2' where agent_id=($1) and status='1'",
        [agent_id],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Delete a customer
   * @param
   * @return json
   */
  delete_user: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "Update tbl_agents set status='2' where agent_id=($1)",
        [agent_id],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update customer
   * @param
   * @return json
   */
  update_user: async (agent) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agents set first_name=($1),last_name=($2),email=($3),status=($4),agent_ref=($5) where agent_id=($6)",
        [
          agent.first_name,
          agent.last_name,
          agent.email,
          agent.status,
          agent.agent_ref,
          agent.agent_id,
        ],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer details
   * @param
   * @return json
   */
  get_user: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "Select tour_counter,tour_counter_new,tour_counter_closed_task,agent_id,status,first_name,language_code,last_name,email,agent_ref,drupal_id,profile_pic,new_feature,new_feature_send_mail,new_feature_fa,multi_lang_access,phone_no,password,country_id,dnd,to_char(date_added, 'Mon YYYY') as date_added from tbl_agents where agent_id =($1) and status!=2",
        [agent_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Agent Company List
   * @param
   * @return json
   */
  fetch_agent_company: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "Select tbl_master_company.company_id,tbl_master_company.company_name from tbl_agent_company left JOIN tbl_master_company on tbl_agent_company.company_id=tbl_master_company.company_id where tbl_agent_company.agent_id=($1) and tbl_agent_company.status='1'",
        [agent_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Check Existence of a company
   * @param
   * @return json
   */
  company_exists: async (company_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "SELECT company_id FROM tbl_master_company WHERE company_id=($1)",
        [company_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List of all active agents
   * @param
   * @return json
   */
  list_all_user: async (limit, offset) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "Select agent_id, first_name, last_name, email, status from tbl_agents where status !=2 order by agent_id desc limit $1 offset $2",
        [limit, offset]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          console.log(err.query);
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Count of active agents
   * @param
   * @return json
   */
  count_all_user: async () => {
    return new Promise(function (resolve, reject) {
      db.any("Select count(agent_id) as cnt from tbl_agents where status !=2")
        .then(function (data) {
          resolve(data[0].cnt);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Check agent/customer email id
   * @param
   * @return json
   */
  email_exists: async (email, agent_id) => {
    return new Promise(function (resolve, reject) {
      if (agent_id === undefined) {
        db.any(
          "select agent_id from tbl_agents where LOWER(email)=LOWER($1) and status !=2",
          [email]
        )
          .then(function (data) {
            if (data.length > 0) resolve({ success: true });
            else resolve({ success: false });
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      } else {
        db.any(
          "select agent_id from tbl_agents where LOWER(email)=LOWER($1) and status=1 and agent_id!=($2)",
          [email, agent_id]
        )
          .then(function (data) {
            if (data.length > 0) resolve({ success: true });
            else resolve({ success: false });
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      }
    });
  },
  /**
   * @desc Check active agent email id
   * @param
   * @return json
   */
  active_email_exists: async (email) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select agent_id from tbl_agents where LOWER(email)=LOWER($1) and status = 1 AND activated = 1 ",
        [email]
      )
        .then(function (data) {
          if (data.length > 0) resolve({ success: true });
          else resolve({ success: false });
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Check active agent email id
   * @param
   * @return json
   */
  findByEmail: async (email) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select tbl_agents.*,to_char(tbl_agents.date_added, 'Mon YYYY') as date_added from tbl_agents where LOWER(email)=LOWER($1) and status='1'",
        [email]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Check agent ref
   * @param
   * @return json
   */
  agent_ref_exists: async (agent_ref, agent_id) => {
    return new Promise(function (resolve, reject) {
      if (agent_id === undefined) {
        db.any(
          "select agent_id from tbl_agents where agent_ref=($1) and status !=2",
          [agent_ref]
        )
          .then(function (data) {
            if (data.length > 0) resolve({ success: true });
            else resolve({ success: false });
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      } else {
        db.any(
          "select agent_id from tbl_agents where agent_ref=($1) and status=1 and agent_id!=($2)",
          [agent_ref, agent_id]
        )
          .then(function (data) {
            if (data.length > 0) resolve({ success: true });
            else resolve({ success: false });
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      }
    });
  },
  /**
   * @desc Get Agents of a company
   * @param company_id
   * @return json
   */
  get_company_agents: async (company_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "SELECT AGNT.first_name,AGNT.last_name,AGNT.email,AGNT.drupal_id FROM tbl_agent_company AS AGNT_CMP LEFT JOIN tbl_agents AS AGNT ON AGNT_CMP.agent_id = AGNT.agent_id WHERE AGNT.status = 1 AND AGNT_CMP.status = 1 AND AGNT_CMP.company_id = ($1)",
        [company_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Find by Username
   * @param username
   * @return json
   */
  findByUsername: async (username) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select tbl_agents.*,to_char(tbl_agents.date_added, 'Mon YYYY') as date_added from tbl_agents where LOWER(email)=LOWER($1) and status='1'",
        [username]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Find by id
   * @param id
   * @return json
   */
  findByUserId: async (id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select tbl_agents.*,to_char(tbl_agents.date_added, 'Mon YYYY') as date_added from tbl_agents where agent_id=($1) and status='1'",
        [id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },

  update_feature: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.result('UPDATE tbl_agents set new_feature=2 where agent_id=($1)', [agent_id], r => r.rowCount)
        .then(function (data) {
          resolve(data)
        }).catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },

  update_feature_send_mail: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.result('UPDATE tbl_agents set new_feature_send_mail=2 where agent_id=($1)', [agent_id], r => r.rowCount)
        .then(function (data) {
          resolve(data)
        }).catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Find by active user id
   * @param id
   * @return json
   */
  findActivatedUserId: async (id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select * from tbl_agents where agent_id=($1) and status='1' and activated ='1'",
        [id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Fetch password of an agent
   * @param id
   * @return json
   */
  get_pass: async (id) => {
    return new Promise(function (resolve, reject) {
      db.any("SELECT password from tbl_agents where agent_id=($1)", [id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update password of an agent
   * @param pass,id
   * @return json
   */
  update_pass: async (pass, id) => {
    return new Promise(function (resolve, reject) {
      db.result(
        `UPDATE tbl_agents set password=($1),pass_update_date=($3) where agent_id=($2)`,
        [pass, id, common.currentDateTime()],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Change agent status to active
   * @param id
   * @return json
   */
  update_activated: async (id) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agents set activated=1 where agent_id=($1)",
        [id],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update agent token
   * @param id,token
   * @return json
   */
  update_token: async (id, token) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agents set token=($2) where agent_id=($1)",
        [id, token],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update agent picture
   * @param userObj,userId
   * @return json
   */
  update_agent: async (userObj, userId) => {
    return new Promise(function (resolve, reject) {
      if (userObj.profile_pic != null) {
        db.result(
          "UPDATE tbl_agents set profile_pic=($2) where agent_id=($1)",
          [userId, userObj.profile_pic],
          (r) => r.rowCount
        )
          .then(function (data) {
            resolve(data);
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      } else {
        /* 'UPDATE tbl_agents set phone_no=($2),password=($3),country_id=($4) where agent_id=($1)',[userId,userObj.phone_no,userObj.password,userObj.country_id],r => r.rowCount */
        db.result(
          "UPDATE tbl_agents set phone_no=($2),password=($3),country_id=($4),dnd=($5),language_code=($6) where agent_id=($1)",
          [
            userId,
            userObj.phone_no,
            userObj.password,
            userObj.country_id,
            userObj.dnd,
            userObj.language_code,
          ],
          (r) => r.rowCount
        )
          .then(function (data) {
            resolve(data);
          })
          .catch(function (err) {
            var errorText = common.getErrorText(err);
            var error = new Error(errorText);
            reject(error);
          });
      }
    });
  },
  update_agent_language: async (lang, userId) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agents set language_code=($1) where agent_id=($2)",
        [lang, userId], (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });

    });
  },
  /**
   * @desc Remove agent picture
   * @param customer_id
   * @return json
   */
  remove_profile_pic: async (customer_id) => {
    return new Promise(function (resolve, reject) {
      db.result(
        "UPDATE tbl_agents set profile_pic=null where agent_id=($1)",
        [customer_id],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get company list of an agent
   * @param agent_id
   * @return json
   */
  list_company: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      let sql = `
			SELECT C.* 
			FROM tbl_agent_company AS AC
			LEFT JOIN tbl_master_company AS C
			ON AC.company_id = C.company_id 
			WHERE AC.agent_id=($1) 
			AND AC.status='1' ORDER BY C.company_name ASC`;
      db.any(sql, [agent_id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Common Request List
   * @param agent_id,cust_arr,query_arr,limit, offset
   * @return json
   */
  request_list: async (agent_id, cust_arr, query_arr, limit, offset) => {
    return new Promise(async function (resolve, reject) {
      var query_string = "";
      let lang_code = await module.exports.get_agent_language(agent_id);
      if (
        query_arr &&
        query_arr.date_from != null &&
        query_arr.date_to != null
      ) {
        query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
      } else if (query_arr && query_arr.date_from != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
      } else if (query_arr && query_arr.date_to != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
      }

      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      /*var sql =`SELECT 
      tbl_tasks.task_id,
      tbl_tasks.close_status,
      tbl_tasks.task_ref,
      to_char(tbl_tasks.date_added, 'dd MON YYYY') as date_added,
      tbl_master_request_type.req_name
      ,tbl_master_product.product_name,
      tbl_tasks.status,
      tbl_customer.first_name,
      tbl_customer.last_name,
      b.required_action,
      to_char(b.expected_closure_date,'dd MON YYYY') as expected_closure_date,
      CASE WHEN tbl_tasks.close_status = 0 THEN b.status
      WHEN tbl_tasks.close_status = 1 THEN 'Closed'
      END AS status,h.status AS highlight_status,
      CASE WHEN tbl_tasks.close_status = 0 THEN NULL
      WHEN tbl_tasks.close_status = 1 THEN to_char(tbl_tasks.closed_date, 'dd MON YYYY')
      END AS closed_date,
      tbl_tasks.submitted_by,
      tbl_tasks.ccp_posted_by,
      tbl_agents.first_name AS agent_fname,
      tbl_agents.last_name AS agent_lname,
      tbl_employee.first_name AS emp_posted_by_fname,
      tbl_employee.last_name AS emp_posted_by_lname
       from tbl_tasks
       LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
       LEFT JOIN
        (
          SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
          FROM tbl_customer_response WHERE resp_status = 1
          GROUP BY task_id
        ) b ON tbl_tasks.task_id = b.task_id
       LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
       LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
       LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
       LEFT JOIN tbl_customer_task_highligher AS h 
       ON tbl_tasks.task_id = h.task_id 
       AND h.customer_id = ($4) 
       AND h.identifier  = ($5)  
       LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
       where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0  ${query_string} order by tbl_tasks.task_id desc limit $2 offset $3`;*/

      var sql = `SELECT 
			tbl_tasks.task_id,
      MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.date_added), 'dd MON YYYY') as date_added,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
      END AS req_name,
      
      MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,

			MAX(tbl_tasks.language) as language,
			MAX(tbl_tasks.status) as status_task,
			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			to_char(MAX(b.expected_closure_date),'dd MON YYYY') as expected_closure_date,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,

			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			
			
			MAX(h.status) AS highlight_status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN NULL
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd MON YYYY')
			END AS closed_date,
			tbl_tasks.submitted_by,
			tbl_tasks.ccp_posted_by,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN
				(
					SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					GROUP BY task_id
				) b ON tbl_tasks.task_id = b.task_id
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 	
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by


			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 	 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5)  
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
			 where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0  ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;
      console.log(sql);
      console.log('cust_arr', cust_arr);
      console.log('agent_id', agent_id);
      db.any(sql, [cust_arr, limit, offset, agent_id, "A"])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Count Common Request List
   * @param agent_id,cust_arr,query_arr
   * @return json
   */
  count_request_list: async (agent_id, cust_arr, query_arr) => {
    return new Promise(async function (resolve, reject) {
      var query_string = "";
      let lang_code = await module.exports.get_agent_language(agent_id);
      if (query_arr && query_arr.s && query_arr.s.length > 0) {

        if (lang_code != Config.default_language) {
          query_string += `
          AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
          OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
          AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
          OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN
					   (
              SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
              FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
              GROUP BY task_id
             ) b ON tbl_tasks.task_id = b.task_id
          LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
          LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id    
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 		LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
					where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0  ${query_string} group by tbl_tasks.task_id`;

      db.any(sql, [cust_arr, agent_id])
        .then(function (data) {
          if (data.length > 0) {
            resolve(data.length);
          } else {
            resolve(0);
          }
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Order List
   * @param agent_id,cust_arr,query_arr,limit, offset
   * @return json
   */
  order_list: async (agent_id, cust_arr, query_arr, limit, offset) => {
    return new Promise(async function (resolve, reject) {
      let lang_code = await module.exports.get_agent_language(agent_id);
      var query_string = "";

      if (
        query_arr &&
        query_arr.date_from != null &&
        query_arr.date_to != null
      ) {
        query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
      } else if (query_arr && query_arr.date_from != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
      } else if (query_arr && query_arr.date_to != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
      }

      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as  task_ref,
			to_char(MAX(tbl_tasks.rdd), \'dd MON YYYY\') as rdd,
			to_char(MAX(tbl_tasks.date_added), \'dd MON YYYY\') as date_added,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
      END AS req_name,
      
      MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,

			MAX(tbl_tasks.quantity) as quantity,
			MAX(tbl_tasks.language) as language,
			MAX(tbl_tasks.status) as status,
			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			MAX(b.po_number) as po_number,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN MAX(b.status)
			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 9
			)
			END AS status,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 9
			)
			END AS status,

			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			MAX(h.status) AS highlight_status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN to_char(MAX(b.expected_closure_date), 'dd MON YYYY')
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd MON YYYY')
			END AS expected_closure_date,
			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN
				(
					SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(po_number) as po_number, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					GROUP BY task_id
				) b ON tbl_tasks.task_id = b.task_id
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 	
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

 			 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5)  
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
			 where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;

      db.any(sql, [cust_arr, limit, offset, agent_id, "A"])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Count Order List
   * @param agent_id,cust_arr,query_arr
   * @return json
   */
  count_order_list: async (agent_id, cust_arr, query_arr) => {
    return new Promise(async function (resolve, reject) {
      var query_string = "";
      let lang_code = await module.exports.get_agent_language(agent_id);
      if (query_arr && query_arr.s && query_arr.s.length > 0) {

        if (lang_code != Config.default_language) {
          query_string += `
          AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
          OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
          AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
          OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
          OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN
				   (
            SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					  FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					  GROUP BY task_id
           ) b ON tbl_tasks.task_id = b.task_id
        LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
        LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id    
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
				LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		  LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
				LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
				where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;

      db.any(sql, [cust_arr, agent_id])
        .then(function (data) {
          if (data.length > 0) {
            resolve(data.length);
          } else {
            resolve(0);
          }
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Forecast List
   * @param agent_id,cust_arr,query_arr,limit, offset
   * @return json
   */
  forecast_list: async (agent_id, cust_arr, query_arr, limit, offset) => {
    return new Promise(async function (resolve, reject) {
      let lang_code = await module.exports.get_agent_language(agent_id);
      var query_string = "";

      if (
        query_arr &&
        query_arr.date_from != null &&
        query_arr.date_to != null
      ) {
        query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
      } else if (query_arr && query_arr.date_from != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
      } else if (query_arr && query_arr.date_to != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
      }

      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(9);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.date_added), \'dd MON YYYY\') as date_added,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
      END AS req_name,
      
      MAX(tbl_master_request_type.req_name) AS rating_req_name,

			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,	

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
					 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
					 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
					 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
					 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,

			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			MAX(h.status) AS highlight_status,
			MAX(tbl_tasks.language) as language,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN MAX(b.expected_closure_date)
			WHEN MAX(tbl_tasks.close_status) = 1 THEN MAX(tbl_tasks.closed_date)
			END AS expected_closure_date,
			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN
				(
					SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					GROUP BY task_id
				) b ON tbl_tasks.task_id = b.task_id
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 	
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

 			 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer_task_highligher AS h ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5) 
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			 where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type = 24 and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;

      db.any(sql, [cust_arr, limit, offset, agent_id, "A"])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Count Forecast List
   * @param agent_id,cust_arr,query_arr
   * @return json
   */
  count_forecast_list: async (agent_id, cust_arr, query_arr) => {
    return new Promise(async function (resolve, reject) {
      var query_string = "";
      let lang_code = await module.exports.get_agent_language(agent_id);
      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(9);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN
				   (
					  SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					  FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					  GROUP BY task_id
           ) b ON tbl_tasks.task_id = b.task_id
        LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
        LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id    
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
				LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 	LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
				LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
				where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type = 24 and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;
      db.any(sql, [cust_arr, agent_id])
        .then(function (data) {
          if (data.length > 0) {
            resolve(data.length);
          } else {
            resolve(0);
          }
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Complaint List
   * @param agent_id,cust_arr,query_arr,limit,offset
   * @return json
   */
  complaint_list: async (agent_id, cust_arr, query_arr, limit, offset) => {
    return new Promise(async function (resolve, reject) {
      let lang_code = await module.exports.get_agent_language(agent_id);
      var query_string = "";

      if (
        query_arr &&
        query_arr.date_from != null &&
        query_arr.date_to != null
      ) {
        query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
      } else if (query_arr && query_arr.date_from != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
      } else if (query_arr && query_arr.date_to != null) {
        query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
      }

      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.date_added), 'dd MON YYYY') as date_added,
			
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
      END AS req_name,
      
      MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,

			MAX(tbl_tasks.batch_number) as batch_number,
			MAX(tbl_tasks.quantity) as quantity,
			MAX(tbl_tasks.language) as language,
			MAX(tbl_tasks.status) as status,
			MAX(tbl_tasks.nature_of_issue) as nature_of_issue,
			MAX(tbl_customer.first_name) as  first_name,
			MAX(tbl_customer.last_name) as last_name,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			MAX(h.status) AS highlight_status,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,

			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN to_char(MAX(b.expected_closure_date), 'dd MON YYYY')
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd MON YYYY')
			END AS expected_closure_date,
			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN
				(
					SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					GROUP BY task_id
				) b ON tbl_tasks.task_id = b.task_id
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 	
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

 			 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5) 
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			 where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;

      db.any(sql, [cust_arr, limit, offset, agent_id, "A"])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Customer Count Complaint
   * @param agent_id,cust_arr,query_arr
   * @return json
   */
  count_complaint_list: async (agent_id, cust_arr, query_arr) => {
    return new Promise(async function (resolve, reject) {
      var query_string = "";
      let lang_code = await module.exports.get_agent_language(agent_id);
      if (query_arr && query_arr.s && query_arr.s.length > 0) {
        if (lang_code != Config.default_language) {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        } else {
          query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${query_arr.s.toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${query_arr.s.toLowerCase()}%'`;
        }

        let closed_arr = await module.exports.get_task_status(4);

        if (
          closed_arr[0].status_name_en
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) >
          -1 ||
          closed_arr[0].status_name_pt
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_es
            .toLowerCase()
            .indexOf(query_arr.s.toLowerCase()) > -1 ||
          closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1
        ) {
          query_string += ` OR tbl_tasks.close_status = 1`;
        }
        query_string += `)`;
      }

      var sql = `SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN
			   (
          SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date, MAX(status_id) AS status_id, MAX(action_id) AS action_id
					FROM tbl_customer_response WHERE resp_status = 1 AND approval_status = 1
					GROUP BY task_id
         ) b ON tbl_tasks.task_id = b.task_id
      LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
      LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id          
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;

      db.any(sql, [cust_arr, agent_id])
        .then(function (data) {
          if (data.length > 0) {
            resolve(data.length);
          } else {
            resolve(0);
          }
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get Dashboard Overview
   * @param agent_id,cust_arr
   * @return json
   */
  get_task_status: async (status_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        `SELECT status_name_en,status_name_zh,status_name_pt,status_name_es,status_name_ja FROM tbl_master_task_status 
				WHERE status_id=($1)`,
        [status_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  dashboard_overview: async (agent_id, cust_arr) => {
    // let dashboard = [];
    //product
    let productQuery = 0;
    //requests
    let requestQuery = new Promise((resolve, reject) => {
      db.any(
        `select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN
			   (
				   SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
				   FROM tbl_customer_response WHERE resp_status = 1
				   GROUP BY task_id
			   ) b ON tbl_tasks.task_id = b.task_id
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type not in (23,24,25,7,34,41) and parent_id = 0`,
        [cust_arr, agent_id]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });

    //complaints
    let complaintsQuery = new Promise((resolve, reject) => {
      db.any(
        `select count(tbl_tasks.task_id) from tbl_tasks LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN
			   (
				   SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
				   FROM tbl_customer_response WHERE resp_status = 1
				   GROUP BY task_id
			   ) b ON tbl_tasks.task_id = b.task_id
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0`,
        [cust_arr, agent_id]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });

    //orders
    let ordersQuery = new Promise((resolve, reject) => {
      db.any(
        `select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN
			   (
				   SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(po_number) as po_number, MAX(date_added) max_date
				   FROM tbl_customer_response WHERE resp_status = 1
				   GROUP BY task_id
			   ) b ON tbl_tasks.task_id = b.task_id
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0`,
        [cust_arr, agent_id]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });

    //forecasts
    let forecastQuery = new Promise((resolve, reject) => {
      db.any(
        `select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN
			   (
				   SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
				   FROM tbl_customer_response WHERE resp_status = 1
				   GROUP BY task_id
			   ) b ON tbl_tasks.task_id = b.task_id
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type = 24 and parent_id = 0`,
        [cust_arr, agent_id]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });

    //payments
    let paymentQuery = new Promise((resolve, reject) => {
      db.any(
        "select count(task_id) from tbl_tasks where customer_id IN ($1:csv)  and request_type in (25) and parent_id = 0 and close_status = 0",
        [cust_arr]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });

    //active discussions
    let discussionQuery = new Promise((resolve, reject) => {
      db.any(
        "select count(tbl_task_discussion.task_id) from tbl_tasks INNER JOIN tbl_task_discussion ON tbl_tasks.task_id = tbl_task_discussion.task_id where  ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1))",
        [cust_arr, agent_id]
      )
        .then(function (data) {
          resolve(data[0].count);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
    // Run both queries at the same time and handle both resolve results or first reject
    return Promise.all([
      productQuery,
      requestQuery,
      complaintsQuery,
      ordersQuery,
      forecastQuery,
      paymentQuery,
      discussionQuery,
    ])
      .then((results) => {
        return {
          product: results[0],
          request: results[1],
          complaints: results[2],
          orders: results[3],
          forecasts: results[4],
          payment: results[5],
          discussion: results[6],
        };
        //return res.send();
      })
      .catch((err) => {
        // Catch error
        return err;
      });
  },
  /**
   * @desc Get Customers of a company from given array
   * @param company_id
   * @return json
   */
  get_company_customers: async (company_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        `SELECT customer_id FROM tbl_customer WHERE company_id IN ($1:csv)`,
        [company_id]
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List of customers working in a company
   * @param company_id
   * @return json
   */
  list_company_customers: async (company_id) => {
    return new Promise(function (resolve, reject) {
      let sql = `
			SELECT 
				customer_id,
				CONCAT(first_name,' ',last_name) AS cust_name,
				first_name,
				last_name
			FROM tbl_customer
			WHERE company_id = ($1)
			  AND status = 1 
				AND activated = 1`;
      db.any(sql, [company_id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc company and their customers list from given array
   * @param company_arr
   * @return json
   */
  list_company_customers_arr: async (company_arr) => {
    return new Promise(function (resolve, reject) {
      let sql = `
			SELECT 
				customer_id,
				CONCAT(first_name,' ',last_name) AS cust_name,
				first_name,
				last_name
			FROM tbl_customer
			WHERE company_id IN ($1:csv)
			AND( status=1 OR status=3)`;
      db.any(sql, [company_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log(err);
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Number of agent notification
   * @param agent_id,role,company_id
   * @return json
   */
  countAgentNotification: async (agent_id, role, company_id) => {
    return new Promise(function (resolve, reject) {
      let sql = `SELECT 
				COUNT(NOTI.*) AS cnt 
			FROM tbl_customer_notification AS NOTI
			LEFT JOIN tbl_tasks AS TASKS
				ON NOTI.task_id = TASKS.task_id
			LEFT JOIN tbl_customer AS CUST
				ON TASKS.customer_id = CUST.customer_id	
			WHERE 
				NOTI.customer_id = ($1)
				AND NOTI.customer_type = ($2)
				AND NOTI.read_status = 0
				AND CUST.company_id = ($3)
				AND extract(epoch from ('${common.currentDateTime()}'- NOTI.add_date)) <= 2592000`;
      db.any(sql, [agent_id, role, company_id])
        .then(function (data) {
          resolve(data.length > 0 ? data[0].cnt : 0);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Get agent notification list
   * @param agent_id,role,company_id
   * @return json
   */
  fetchAgentNotification: async (agent_id, role, company_id) => {
    return new Promise(function (resolve, reject) {
      let sql = `SELECT 
				NOTI.*,
				TASKS.task_ref 
			FROM tbl_customer_notification AS NOTI
			LEFT JOIN tbl_tasks AS TASKS
				ON NOTI.task_id = TASKS.task_id
			LEFT JOIN tbl_customer AS CUST
				ON TASKS.customer_id = CUST.customer_id	
			WHERE 
				NOTI.customer_id = ($1)
				AND NOTI.customer_type = ($2)
				AND CUST.company_id = ($3)
				AND extract(epoch from ('${common.currentDateTime()}'- NOTI.add_date)) <= 2592000
			ORDER BY NOTI.add_date DESC`;
      db.any(sql, [agent_id, role, company_id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Agent company list
   * @param agent_id
   * @return json
   */
  get_agent_company: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT * 
			 FROM tbl_agent_company 
			 WHERE agent_id = ($1) 
			 AND status = 1`;
      db.any(sql, [agent_id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  get_agent_language: async (agent_id) => {
    return new Promise(function (resolve, reject) {
      db.one("SELECT language_code FROM tbl_agents WHERE agent_id = ($1)", [
        agent_id,
      ])
        .then(function (data) {
          resolve(data.language_code);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search common request
   * @param customer_arr,customer_id, keyword
   * @return json
   */
  task_request_search: async (customer_arr, customer_id, keyword) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT task_ref as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(task_ref) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0 GROUP BY task_ref`;

      db.any(sql, [customer_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log('=====>', err)
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search files in common request
   * @param customer_arr, keyword
   * @return json
   */
  task_uploads_request_search: async (customer_arr, keyword) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT tbl_task_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
								LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 	
								LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id				
								LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
								LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
								where LOWER(tbl_task_uploads.actual_file_name) LIKE '%${keyword}%' 
								and (tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1) 
								and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) 
								and parent_id = 0`;

      db.any(sql, [customer_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log('=====>', err)
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search discussion files in common request
   * @param customer_arr, keyword
   * @return json
   */
  task_discussion_request_search: async (customer_arr, keyword) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT tbl_task_discussion_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
								LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 	
								LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id				
								LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
								LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
								where LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${keyword}%' 
								and (tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1) 
								and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) 
								and parent_id = 0`;

      db.any(sql, [customer_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log('=====>', err)
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search task using request type
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  task_other_search: async (
    customer_arr,
    customer_id,
    keyword,
    request_type
  ) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT task_ref as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(task_ref) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY task_ref`;
      db.any(sql, [customer_arr, request_type])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log('=====>', err)
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search task files using request type
   * @param customer_arr, keyword, request_type
   * @return json
   */
  task_uploads_other_search: async (customer_arr, keyword, request_type) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT tbl_task_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
									LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
									LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
									LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
									LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
									where LOWER(tbl_task_uploads.actual_file_name) LIKE '%${keyword}%' and (
									tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1) 
									and tbl_tasks.request_type in ($2:csv) and parent_id = 0`;
      db.any(sql, [customer_arr, request_type])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          //console.log('=====>', err)
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search discussion using request type
   * @param customer_arr, keyword, request_type
   * @return json
   */
  task_discussion_other_search: async (customer_arr, keyword, request_type) => {
    return new Promise(function (resolve, reject) {
      var sql = `SELECT tbl_task_discussion_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
									LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
									LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
									LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
									LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
									where LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${keyword}%' and (
									tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1) 
									and tbl_tasks.request_type in ($2:csv) and parent_id = 0`;
      db.any(sql, [customer_arr, request_type])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search general task using title
   * @param customer_arr,customer_id, keyword
   * @return json
   */
  title_request_search: async (
    customer_arr,
    customer_id,
    keyword,
    lang_code
  ) => {
    return new Promise(function (resolve, reject) {
      if (lang_code != Config.default_language) {
        var sql = `SELECT tbl_master_request_type.req_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0 GROUP BY tbl_master_request_type.req_name_${lang_code}`;
      } else {
        var sql = `SELECT tbl_master_request_type.req_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_request_type.req_name) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0 GROUP BY tbl_master_request_type.req_name`;
      }

      db.any(sql, [customer_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search general task using product
   * @param customer_arr,customer_id, keyword
   * @return json
   */
  product_request_search: async (
    customer_arr,
    customer_id,
    keyword,
    lang_code
  ) => {
    return new Promise(function (resolve, reject) {
      if (lang_code != Config.default_language) {
        var sql = `SELECT tbl_master_product.product_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0 GROUP BY tbl_master_product.product_name_${lang_code}`;
      } else {
        var sql = `SELECT tbl_master_product.product_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) and parent_id = 0 GROUP BY tbl_master_product.product_name`;
      }

      db.any(sql, [customer_arr])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search other task using product
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  product_other_search: async (
    customer_arr,
    customer_id,
    keyword,
    request_type,
    lang_code
  ) => {
    return new Promise(function (resolve, reject) {
      if (lang_code != Config.default_language) {
        var sql = `SELECT tbl_master_product.product_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY tbl_master_product.product_name_${lang_code}`;
      } else {
        var sql = `SELECT tbl_master_product.product_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name) LIKE '%${keyword}%' and ((tbl_tasks.customer_id IN ($1:csv) AND tbl_tasks.share_with_agent = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY tbl_master_product.product_name`;
      }

      db.any(sql, [customer_arr, request_type])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Search general task
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  request_search: async (customer_arr, customer_id, keyword) => {
    return new Promise(async function (resolve, reject) {
      var finalArr = [];
      keyword = keyword.toLowerCase();
      let lang_code = await module.exports.get_agent_language(customer_id);
      // ======================== SATYAJIT//
      await module.exports
        .task_request_search(customer_arr, customer_id, keyword)
        .then((taskRecord) => {
          //console.log('task', taskRecord)
          if (taskRecord.length > 0) {
            //finalArr.push(taskRecord)
            for (let index = 0; index < taskRecord.length; index++) {
              const element = taskRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      //   // ======================== SATYAJIT//
      await module.exports
        .title_request_search(customer_arr, customer_id, keyword, lang_code)
        .then((titleRecord) => {
          //console.log('ttile', titleRecord)
          if (titleRecord.length > 0) {
            //finalArr.push(titleRecord)
            for (let index = 0; index < titleRecord.length; index++) {
              const element = titleRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      //   // //  // ======================== SATYAJIT//
      await module.exports
        .product_request_search(customer_arr, customer_id, keyword, lang_code)
        .then((prodRecord) => {
          //console.log('rpo', prodRecord)
          if (prodRecord.length > 0) {
            //finalArr.push(prodRecord)
            for (let index = 0; index < prodRecord.length; index++) {
              const element = prodRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      // //  // ======================== SATYAJIT//
      var labelArr = [];
      //===============//
      await module.exports
        .task_uploads_request_search(customer_arr, keyword)
        .then((upRec) => {
          if (upRec.length > 0) {
            for (let index = 0; index < upRec.length; index++) {
              const element = upRec[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      // =================== SATYAJIT
      await module.exports
        .task_discussion_request_search(customer_arr, keyword)
        .then((disRec) => {
          if (disRec.length > 0) {
            for (let index = 0; index < disRec.length; index++) {
              const element = disRec[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      resolve(finalArr);
    });
  },
  /**
   * @desc Search order task
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  order_search: async (customer_arr, customer_id, keyword) => {
    return new Promise(async function (resolve, reject) {
      var finalArr = [];
      keyword = keyword.toLowerCase();
      let lang_code = await module.exports.get_agent_language(customer_id);
      // ======================== SATYAJIT//
      await module.exports
        .task_other_search(customer_arr, customer_id, keyword, [23])
        .then((taskRecord) => {
          //console.log('task', taskRecord)
          if (taskRecord.length > 0) {
            //finalArr.push(taskRecord)
            for (let index = 0; index < taskRecord.length; index++) {
              const element = taskRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      //   // //  // ======================== SATYAJIT//
      await module.exports
        .product_other_search(
          customer_arr,
          customer_id,
          keyword,
          [23],
          lang_code
        )
        .then((prodRecord) => {
          //console.log('rpo', prodRecord)
          if (prodRecord.length > 0) {
            //finalArr.push(prodRecord)
            for (let index = 0; index < prodRecord.length; index++) {
              const element = prodRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      //===================== SATYAJIT
      var labelArr = [];
      //==============//
      await module.exports
        .task_uploads_other_search(customer_arr, keyword, [23])
        .then((ordUpload) => {
          if (ordUpload.length > 0) {
            for (let index = 0; index < ordUpload.length; index++) {
              const element = ordUpload[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      await module.exports
        .task_discussion_other_search(customer_arr, keyword, [23])
        .then((disUpload) => {
          if (disUpload.length > 0) {
            for (let index = 0; index < disUpload.length; index++) {
              const element = disUpload[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      resolve(finalArr);
    });
  },
  /**
   * @desc Search complaint task
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  complaint_search: async (customer_arr, customer_id, keyword) => {
    return new Promise(async function (resolve, reject) {
      var finalArr = [];
      keyword = keyword.toLowerCase();
      let lang_code = await module.exports.get_agent_language(customer_id);
      // ======================== SATYAJIT//
      await module.exports
        .task_other_search(customer_arr, customer_id, keyword, [7, 34])
        .then((taskRecord) => {
          //console.log('task', taskRecord)
          if (taskRecord.length > 0) {
            //finalArr.push(taskRecord)
            for (let index = 0; index < taskRecord.length; index++) {
              const element = taskRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      //   // //  // ======================== SATYAJIT//
      await module.exports
        .product_other_search(
          customer_arr,
          customer_id,
          keyword,
          [7, 34],
          lang_code
        )
        .then((prodRecord) => {
          //console.log('rpo', prodRecord)
          if (prodRecord.length > 0) {
            //finalArr.push(prodRecord)
            for (let index = 0; index < prodRecord.length; index++) {
              const element = prodRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      // ======================== SATYAJIT//
      var labelArr = [];
      //==============//
      await module.exports
        .task_uploads_other_search(customer_arr, keyword, [7, 34])
        .then((compRec) => {
          if (compRec.length > 0) {
            for (let index = 0; index < compRec.length; index++) {
              const element = compRec[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });
      // ======================== SATYAJIT//
      await module.exports
        .task_discussion_other_search(customer_arr, keyword, [7, 34])
        .then((compDis) => {
          if (compDis.length > 0) {
            for (let index = 0; index < compDis.length; index++) {
              const element = compDis[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      resolve(finalArr);
    });
  },
  /**
   * @desc Search forecast task
   * @param customer_arr,customer_id, keyword, request_type
   * @return json
   */
  forecast_search: async (customer_arr, customer_id, keyword) => {
    return new Promise(async function (resolve, reject) {
      var finalArr = [];
      keyword = keyword.toLowerCase();

      // ======================== SATYAJIT//
      await module.exports
        .task_other_search(customer_arr, customer_id, keyword, [24])
        .then((taskRecord) => {
          //console.log('task', taskRecord)
          if (taskRecord.length > 0) {
            //finalArr.push(taskRecord)
            for (let index = 0; index < taskRecord.length; index++) {
              const element = taskRecord[index];
              if (element.value != null) {
                finalArr.push(element);
              }
            }
          }
        });

      // ======================== SATYAJIT//
      var labelArr = [];
      //==============//
      await module.exports
        .task_uploads_other_search(customer_arr, keyword, [24])
        .then((upOther) => {
          if (upOther.length > 0) {
            for (let index = 0; index < upOther.length; index++) {
              const element = upOther[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      await module.exports
        .task_discussion_other_search(customer_arr, keyword, [24])
        .then((upOther) => {
          if (upOther.length > 0) {
            for (let index = 0; index < upOther.length; index++) {
              const element = upOther[index];
              if (element.value != null) {
                if (labelArr.length > 0) {
                  if (labelArr.indexOf(element.label) == -1) {
                    finalArr.push(element);
                    labelArr.push(element.label);
                  }
                } else {
                  finalArr.push(element);
                  labelArr.push(element.label);
                }
              }
            }
          }
        });

      resolve(finalArr);
    });
  },
  /**
   * @desc Get CC customers from company
   * @param company_id,customer_id
   * @return json
   */
  get_cc_cust_company: async (company_id, customer_id) => {
    return new Promise(function (resolve, reject) {
      // status
      var sql = `SELECT 
					C.customer_id::INTEGER,
					C.first_name,
					C.last_name,
					COMP.company_name
				FROM tbl_customer AS C 
				LEFT JOIN tbl_master_company AS COMP
				ON C.company_id = COMP.company_id
				WHERE ((C.status = 1 
					AND C.activated  = 1) OR C.status = 3)
					AND C.company_id IN ($1:csv)
					AND C.customer_id != ($2)
				ORDER BY  COMP.company_id,C.first_name ASC`;

      db.any(sql, [company_id, customer_id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc Update agent notification
   * @param customer_id, n_id, task_id, role
   * @return json
   */
  updateAgentNotification: async (customer_id, n_id, task_id, role) => {
    return new Promise(function (resolve, reject) {
      db.result(
        `UPDATE tbl_customer_notification 				
				SET read_status = 1
				WHERE customer_id=($1) 
				AND n_id=($2)
				AND task_id = ($3)
				AND customer_type=($4)
				AND read_status = 0`,
        [customer_id, n_id, task_id, role],
        (r) => r.rowCount
      )
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
	update_tour_counter: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter = tour_counter + 1
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

  },
  tour_done: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter = 4
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
  update_tour_counter_new: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter_new = tour_counter_new + 1
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

  },
  tour_done_new: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter_new = 4
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
  update_tour_counter_closed_task: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter_closed_task = tour_counter_closed_task + 1
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

  },
  tour_done_closed_task: async (agent_id) => {
		return new Promise(function (resolve, reject) {

			db.any(`UPDATE tbl_agents				
			SET tour_counter_closed_task = 4
			WHERE agent_id=($1)`, [agent_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	}

  /*list_user_mono: async ()=>{
    return new Promise(function(resolve, reject) {

        db.any('Select agent_id, first_name, last_name, email, status from tbl_agents where status !=2 order by agent_id desc')
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          console.log(err.query)
          var errorText = common.getErrorText(err);
          var error     = new Error(errorText);
          reject(error);
        });

    }); 

  }*/
  

};
