const db =require('../configuration/dbConn');
const common = require('../controllers/common');
const dateFormat    = require('dateformat');

module.exports = {
	
	insertRequest: async (reqObj)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_sap_request(request_id,user_id,rdd,product_sku,ship_to_party,quantity,unit,market,request_date, additional_specification, reservation_no,is_admin,share_with_agent,cc_customers) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING id',[reqObj.request_id, reqObj.user_id, reqObj.rdd, reqObj.product_sku, reqObj.ship_to_party, reqObj.quantity, reqObj.unit, reqObj.market, reqObj.request_date, reqObj.additional_specification, reqObj.reservation_no,reqObj.is_admin,reqObj.share_with_agent, reqObj.cc_customers])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { //console.log('SQL : ', this.sql);
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},

	insertResponse: async (reqObj)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_sap_response(sap_request_id,fbpo_number,itm_id,epdd,total_available_quantity,unit_of_measure,rtl_flag) VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING id',[reqObj.sap_request_id, reqObj.fbpo_number, reqObj.itm_ID, reqObj.epdd,reqObj.total_available_quantity, reqObj.unit_of_measure, reqObj.rtl_flag])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	sold_to_data_exists : async (customer)=>{
		
		return new Promise(function(resolve, reject) {
			db.any('SELECT sap_ref_no as sold_to from tbl_master_company_soldto where sap_ref_no = $1 ',[customer])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	sold_to_ship_to_relation : async (customer)=>{
		
		return new Promise(function(resolve, reject) {
			db.any('SELECT shipto_id from tbl_company_shipto where soldto_id = $1 ',[customer])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	update_relation_ship_sold : async (relObj)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('UPDATE tbl_company_shipto set country = $2 , city= $3 , post_code = $4 ,street=$5, shipto_name = $7 where  shipto_id = $1 and soldto_id = $6 RETURNING shipto_id',[
				relObj.shipto_id,relObj.country,relObj.city,relObj.post_code,relObj.street,relObj.soldto_id,relObj.shipto_name])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	add_relation_ship_sold : async (relObj)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_company_shipto(shipto_id,country,city,post_code,street,soldto_id,shipto_name) values ($1,$2,$3,$4,$5,$6,$7) RETURNING shipto_id ',[
				relObj.shipto_id,relObj.country,relObj.city,relObj.post_code,relObj.street,relObj.soldto_id,relObj.shipto_name])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	relation: async (soldto_id,shipto_id)=>{
		
		return new Promise(function(resolve, reject) {
			db.any('SELECT shipto_id from tbl_company_shipto where soldto_id = $1 and shipto_id = $2 ',[soldto_id,shipto_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},

	insertRequestLog: async (logObj)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_sap_log(request,response,method,url,datetime,request_type) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id',[logObj.request, logObj.response, logObj.method, logObj.url,logObj.datetime,logObj.request_type])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	add_coa : async (req_data)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_tasks_coa(customer_name,batch_no,inspection_lot_no,coa_file,date_added,source,ip,coa_file_path,material_id,customer_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING coa_id',[req_data.customer_name, req_data.batch_number, req_data.lot_no, req_data.file_path,req_data.date_added,'1',req_data.ip,req_data.coa_file_path,req_data.material_id,req_data.customer_id])
		    .then(function (data) {
			  resolve(data);
			  console.log("this is returning : ",data);
			  
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	insertInvoice : async (req_data)=>{
		
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_task_invoice(invoice_number,invoice_location,sales_order_no,packing_location,packing_file_name,packing_file_path,invoice_file_name,invoice_file_path,invoice_date,packing_date,logistics_partner,payment_status,date_added,date_updated) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING id',[req_data.invoice_number, req_data.invoice_location, req_data.sap_so_number, req_data.packing_location,req_data.packing_file_name,req_data.packing_file_path,req_data.invoice_file_name,req_data.invoice_file_path,req_data.invoice_date,req_data.packing_date,req_data.logistics_partner,req_data.payment_status,req_data.date_added,req_data.date_updated])
		    .then(function (data) {
			  resolve(data);
			 // console.log("this is returning : ",data);
			  
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	// get_coa : async (req_data)=>{
		
	// 	return new Promise(function(resolve, reject) {
	// 		db.any('select coa_id from  tbl_tasks_coa where customer_name = $1 AND batch_no = $2 AND inspection_lot_no = $3',[req_data.customer_name, req_data.batch_number, req_data.lot_no])
	// 		.then(function (data) {
	// 			if(data.length > 0){
	// 				resolve({success : true})
	// 			}else{
	// 				resolve({success : false})
	// 			}
		    	
				
	// 		  })
	// 		  .catch(function (err) {
	// 			var errorText = common.getErrorText(err);
	// 			var error = new Error(errorText);
	// 			reject(error);
	// 		  });
	// 	  });
	// 	},
		get_old_product : async (req_data)=>{
		
			return new Promise(function(resolve, reject) {
				db.any('select material_code from  tbl_master_sap_product where material_code = $1 AND sorg = $2 AND dchl = $3 ',[req_data.material_code, req_data.sorg, req_data.dchl])
				.then(function (data) {
					if(data.length > 0){
						resolve({success : true})
					}else{
						resolve({success : false})
					}
				})
				.catch(function (err) {
				  var errorText = common.getErrorText(err);
				  var error = new Error(errorText);
				  reject(error);
				});
			});
		  },
		//   db.one('select customer from  tbl_sap_customer_master where customer = $1 and sorg = $2 and dchl = $3 and funct = $4 and customer_ship_to = $5 ',[req_data.customer,req_data.sorg,req_data.dchl,req_data.funct,req_data.customer_ship_to])
		get_customer_relation : async (req_data)=>{
			//console.log("|||||+++||||||",req_data);
			  return new Promise(function(resolve, reject) {
				  db.any('select customer ,customer_id  from  tbl_sap_customer_master where customer = $1 and sorg = $2 and dchl = $3 and funct = $4 and customer_ship_to = $5 ',[req_data.customer,req_data.sorg,req_data.dchl,req_data.funct,req_data.customer_ship_to])
				  .then(function (data) {
					  resolve(data)	
					  //console.log("hello ++++++++++++ hello ++++++++++++++");
					  
				  })
				  .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				  });
			  });
			},
		  add_product :  async (req_data)=>{
		
			return new Promise(function(resolve, reject) {
				db.one('INSERT INTO tbl_master_sap_product(material_code,material_description,sorg,sorg_desc,dchl,dchl_desc,div,div_desc,x_plntmat_status,x_distchain_status,distchainspecstatus,created_date,created_by,last_chg,changed_by,mtyp,bun,date_added,plant,status,date_updated) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21) RETURNING id',[req_data.material_code,req_data.material_description,req_data.sorg,req_data.sorg_desc,req_data.dchl,req_data.dchl_desc,req_data.div,req_data.div_desc,req_data.x_plntmat_status,req_data.x_distchain_status,req_data.distchainspecstatus,req_data.created_date,req_data.created_by,req_data.last_chg,req_data.changed_by,req_data.mtyp,req_data.bun,req_data.date_added,req_data.plant,req_data.status,req_data.date_updated])
				.then(function (data) {
				  resolve(data);
				 // console.log("this is returning : ",data);
				  
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}); 
	
		},
		add_sales_order :  async (req_data)=>{
		
			return new Promise(function(resolve, reject) {
				db.one('INSERT INTO tbl_sales_order_temp(sales_order,customer_po_number,creation_date,sales_document_type,customer,item,material,net_value,currency,order_quantity,order_unit) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING id',[req_data.sales_order,req_data.customer_po_number,req_data.creation_date,req_data.sales_document_type,req_data.customer,req_data.item,req_data.material,req_data.net_value,req_data.currency,req_data.order_quantity,req_data.order_unit])
				.then(function (data) {
				  resolve(data);
				 // console.log("this is returning : ",data);
				  
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}); 
	
		},
		delete_shipto_product :  async (shipto_id)=>{
			return new Promise(function(resolve, reject) {
				db.any(`DELETE FROM tbl_company_sap_product WHERE shipto_id = $1 `,[shipto_id]).then(function (data) {
					resolve(data);
				   // console.log("this is returning : ",data);
					
				  })
				  .catch(function (err) {
					  var errorText = common.getErrorText(err);
					  var error     = new Error(errorText);
					  reject(error);
				  });
			 })
		 },
		 check_shipto_product : async (shipto_id)=>{
			return new Promise(function(resolve, reject) {
				db.any(`select material_master from tbl_sap_customer_product_master where ship_to_party = $1 `,[shipto_id]).then(function (data) {
					resolve(data);
				   // console.log("this is returning : ",data);
					
				  })
				  .catch(function (err) {
					  var errorText = common.getErrorText(err);
					  var error     = new Error(errorText);
					  reject(error);
				  });
			 })
		 },
		 add_shipto_product :  async (shipto_id,product_sku)=>{
			return new Promise(function(resolve, reject) {
				db.any(`INSERT INTO tbl_company_sap_product(shipto_id,product_sku) values($1,$2) `,[shipto_id,product_sku]).then(function (data) {
					resolve(data);
				   // console.log("this is returning : ",data);
					
				  })
				  .catch(function (err) {
					  var errorText = common.getErrorText(err);
					  var error     = new Error(errorText);
					  reject(error);
				  });
			 })
		 }, 
		get_compare_data :  async (id)=>{
		
			return new Promise(function(resolve, reject) {
				db.any('select material_code ,x_plntmat_status ,x_distchain_status,distchainspecstatus from tbl_master_sap_product where id = $1 and status = 1',[id])
				.then(function (data) {
				  resolve(data);
				 // console.log("this is returning : ",data);
				  
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}); 
	
		},
		update_product_status :  async (arr,date)=>{
			//console.log("this array : ",arr)
			
		
			return new Promise(function(resolve, reject) {
				db.any('update tbl_master_sap_product  set status = 0 , date_updated = $2 where material_code IN ($1:csv) ',[arr,date])
				.then(function (data) {
				  resolve(data);
				 // console.log("this is returning : ",data);
				  
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}); 
	
		},
		add_customer : async (req_data)=>{
		
			return new Promise(function(resolve, reject) {
				db.one('INSERT INTO tbl_sap_customer_master(customer,name1,sorg,sorg_desc,dchl,dchl_desc,dv,dv_desc,funct,funct_desc,parc,customer_ship_to,incot,inco2,curr,payt,cty,cty_name,city,postalcode,street,date,created_by,customer_group,date_added,date_updated,status) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27) RETURNING customer_id',[req_data.customer,req_data.name1,req_data.sorg,req_data.sorg_desc,req_data.dchl,req_data.dchl_desc,req_data.dv,req_data.dv_desc,req_data.funct,req_data.funct_desc,req_data.parc,req_data.customer_ship_to,req_data.incot,req_data.inco2,req_data.curr,req_data.payt,req_data.cty,req_data.cty_name,req_data.city,req_data.postalcode,req_data.street,req_data.date,req_data.created_by,req_data.customer_group,req_data.date_added,req_data.date_updated,req_data.status])
				.then(function (data) {
				  resolve(data);
				 // console.log("this is returning : ",data);
				  
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}); 
	
		},
		update_payment_status : async(invoice_number,payment_status,date)=>{
                 return new Promise(function(resolve,reject){
					db.one('update tbl_task_invoice  set payment_status = $2 , date_updated = $3 where invoice_number = $1 RETURNING id ',[invoice_number,payment_status,date])
					.then(function (data) {
					  resolve(data);
					 console.log("this is returning : ",data);
					  
					})
					.catch(function (err) {
						var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
					});
				 })
		},
		update_product : async (req_data)=>{
		
			return new Promise(function(resolve, reject) {
				db.one(`UPDATE tbl_master_sap_product set material_description = $1 ,sorg_desc = $2 ,dchl_desc = $3,div = $4,div_desc = $5,x_plntmat_status = $6,x_distchain_status = $7 ,distchainspecstatus = $8,created_date = $9,created_by = $10,last_chg = $11,changed_by = $12 ,mtyp = $13 ,bun = $14 ,plant = $18 , date_updated = $19 WHERE material_code = $15 AND sorg = $16 AND dchl = $17  RETURNING id`,[req_data.material_description,req_data.sorg_desc,req_data.dchl_desc,req_data.div,req_data.div_desc,req_data.x_plntmat_status,req_data.x_distchain_status,req_data.distchainspecstatus,req_data.created_date,req_data.created_by,req_data.last_chg,req_data.changed_by,req_data.mtyp,req_data.bun,req_data.material_code,req_data.sorg,req_data.dchl,req_data.plant,req_data.date_updated])
				.then(function (data) {
					resolve(data);				
				  })
				  .catch(function (err) {
					  var errorText = common.getErrorText(err);
					  var error     = new Error(errorText);
					  reject(error);
				  });
			});
		  },  
		  update_customer : async (req_data,customer_id)=>{
		
			return new Promise(function(resolve, reject) {
				db.one(`UPDATE tbl_sap_customer_master set name1 = $1 ,sorg_desc = $2 ,dchl_desc = $3,dv = $4,dv_desc = $5,funct_desc = $6,parc = $7 ,incot = $8,inco2 = $9,curr = $10,payt = $11,cty = $12 ,cty_name = $13 ,city = $14,postalcode = $15 , street = $16 ,date = $17 ,created_by = $18 ,customer_group = $19 , date_updated = $21 , status = $22 WHERE  customer_id = $20 RETURNING customer_id`,[req_data.name1,
					req_data.sorg_desc,
					req_data.dchl_desc,
					req_data.dv,
					req_data.dv_desc,
					req_data.funct_desc,
					req_data.parc,
					req_data.incot,
					req_data.inco2,
					req_data.curr,
					req_data.payt,
					req_data.cty,
					req_data.cty_name,
					req_data.city,
					req_data.postalcode,
					req_data.street,
					req_data.date,
					req_data.created_by,
					req_data.customer_group,
					customer_id,
					req_data.date_updated,
					req_data.status])
				.then(function (data) {
					resolve(data);				
				  })
				  .catch(function (err) {
					  var errorText = common.getErrorText(err);
					  var error     = new Error(errorText);
					  reject(error);
				  });
			});
		  },  
		  check_invoice : async (invoice_number,sap_so_number)=>{
		
			return new Promise(function(resolve, reject) {
				db.any('select id from  tbl_task_invoice where invoice_number = $1 AND sales_order_no = $2 ',[invoice_number,sap_so_number])
				.then(function (data) {
					if(data.length > 0){
						resolve({success : true})
					}else{
						resolve({success : false})
					}
					
					
				  })
				  .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				  });
			  });
			},
			packing_exists : async (invoice_number)=>{
		           // console.log("+++++invoice_number +++++",invoice_number);
					
				return new Promise(function(resolve, reject) {
					db.any('select packing_file_path from  tbl_task_invoice where invoice_number = $1  ',[invoice_number])
					.then(function (data) {
						//console.log(data);
						
						if(data[0].packing_file_path != ''){
							resolve({success : true})
						}else{
							resolve({success : false})
						}
						
						
					  })
					  .catch(function (err) {
						var errorText = common.getErrorText(err);
						var error = new Error(errorText);
						reject(error);
					  });
				  });
				},
				updatePacking : async (data_obj,invoice_number)=>{
		
					return new Promise(function(resolve, reject) {
						db.any('UPDATE tbl_task_invoice set packing_date = $1 , packing_location = $2 ,logistics_partner = $3 , payment_status = $4 , packing_file_name = $5 , packing_file_path = $6 , date_updated = $8 where invoice_number = $7 RETURNING id',[data_obj.packing_date,data_obj.packing_location,data_obj.logistics_partner,data_obj.payment_status,data_obj.packing_file_name,data_obj.packing_file_path,invoice_number,data_obj.date_updated])
						.then(function (data) {
							if(data.length > 0){
								resolve({success : true})
							}else{
								resolve({success : false})
							}
							
							
						  })
						  .catch(function (err) {
							var errorText = common.getErrorText(err);
							var error = new Error(errorText);
							reject(error);
						  });
					  });
					},		

	updateRequestLog: async (logid,response,reason)=>{
		
		return new Promise(function(resolve, reject) {
			db.any('UPDATE tbl_sap_log SET response = $2 , reason = $3 WHERE id = $1',[logid,response,reason])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},

	getResevation: async (request_id,user_id)=>{
		return new Promise(function (resolve, reject) {
			db.any("SELECT * from tbl_sap_request where request_id=($1) and user_id =($2) and status=0 ORDER BY id DESC",[request_id, user_id])
			  .then(function (data) {
				if(data.length == 1){
					resolve({ success: true, data : data });
				}else{					
					resolve({ success: false, message : 'There are more that one request.' });
				}
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	getAllResevationList: async (reservation)=>{
		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_sap_response.*,tbl_sap_request.request_id,tbl_sap_request.ship_to_party, tbl_sap_request.product_sku, tbl_sap_request.market, tbl_sap_request.unit as req_unit,tbl_sap_request.reservation_no, tbl_sap_request.additional_specification, tbl_sap_request.rdd from tbl_sap_response LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_sap_response.sap_request_id  where tbl_sap_response.sap_request_id=($1) AND tbl_sap_response.status=0 order by itm_id ASC",[reservation.id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	getSpecificResevationList: async (reservation,line_items )=>{
		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_sap_response.*,tbl_sap_request.request_id,tbl_sap_request.ship_to_party, tbl_sap_request.product_sku, tbl_sap_request.market, tbl_sap_request.unit as req_unit,tbl_sap_request.reservation_no, tbl_sap_request.user_id, tbl_sap_request.additional_specification,tbl_sap_request.rdd from tbl_sap_response LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_sap_response.sap_request_id  where tbl_sap_response.sap_request_id=($1) AND tbl_sap_response.status=0 AND tbl_sap_response.id IN ($2:csv) order by itm_id ASC",[reservation.id, line_items])
			.then(function (data) {			
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
		
	},

	updateAllRequestForReservation: async (reservation, total_reserved)=>{
		return new Promise(function (resolve, reject) {
			db.any("UPDATE tbl_sap_response SET status = 1, so_id = 1 WHERE status = 0 AND sap_request_id = $1",[reservation.id])
			.then(async function (data) {	
				db.any("UPDATE tbl_sap_request SET status = 1, reserved_quantity = $3,supply_type=1  WHERE id = $1 AND reservation_no = $2",[reservation.id, reservation.reservation_no, total_reserved])
				.then(function (data1) {
					resolve({success: true});
				}).catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},

	updateSpecificRequestForReservation: async (reservation, line_items, total_reserved)=>{
		return new Promise(function (resolve, reject) {
			db.any("UPDATE tbl_sap_response SET status = 1 WHERE status = 0 AND sap_request_id = $1 AND tbl_sap_response.id IN ($2:csv)",[reservation.id, line_items])
			.then(async function (data) {
				db.any("UPDATE tbl_sap_response SET status = 2 WHERE status = 0 AND sap_request_id = $1 AND tbl_sap_response.id NOT IN ($2:csv)",[reservation.id, line_items])
				.then(async function (data1) {				
					db.any("UPDATE tbl_sap_request SET status = 1, reserved_quantity = $3,supply_type=2 WHERE status = 0 AND id = $1 AND reservation_no = $2",[reservation.id, reservation.reservation_no, total_reserved])
					.then(function (data2) {			
						resolve({success: true});
					}).catch(function (err) {
						var errorText = common.getErrorText(err);
						var error = new Error(errorText);
						reject(error);
					});
				}).catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},

	updateResponseSO: async (response_id, so_id)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_response SET so_id = $2 WHERE id = $1',[response_id, so_id])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	addSapPOFile: async (file)=>{
		return new Promise(function(resolve, reject) {

			db.any('INSERT INTO tbl_task_po_uploads(request_id,new_file_name,actual_file_name,date_added) VALUES($1,$2,$3,$4) RETURNING po_id',[file.request_id,file.new_file_name,file.actual_file_name,file.date_added])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},

	updateSoldToParty: async (request_id, sold_to_party)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_request SET sold_to_party = $2, po_status = 1 WHERE status = 1 AND po_status IN (0,2) AND request_id = $1',[request_id, sold_to_party])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	updateDelayPO: async (request_id)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_request SET po_status = 2 WHERE status = 1 AND po_status = 0 AND request_id = $1',[request_id])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	list_products: async (customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT id, name from tbl_customer_sap_product WHERE customer_id = $1  order by name ASC",[customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	get_products_details: async (product_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT id, name from tbl_customer_sap_product WHERE id = $1",[product_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	list_products_code: async (product_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT id, code from tbl_customer_sap_product_code WHERE product_id = $1 order by code ASC",[product_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	list_shipto: async (customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT ship_to_party_id as code, ship_to_party_name as name from tbl_master_sap_customer_shipto WHERE customer_id = $1 order by id ASC",[customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	list_soldto: async (customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT sold_to_party_id as code, sold_to_party_name as name from tbl_master_sap_customer_soldto WHERE customer_id = $1 order by id ASC",[customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	list_enquiries: async (customer_id,status,limit, offset) => {
		return new Promise(function (resolve, reject) {
			let additional_condition ='';
			if(status == 'cancel'){
				additional_condition = ' AND tbl_tasks.close_status = 1';
			}else if (status == 'active'){
				additional_condition = ' AND tbl_tasks.close_status = 0';
			}else{
				additional_condition ='';
			}
			let sql = `select tbl_tasks.task_id, tbl_tasks.task_ref, tbl_tasks.date_added,tbl_tasks.due_date, tbl_tasks.product_id, tbl_tasks.country_id, tbl_tasks.quantity,tbl_customer_sap_product.name as product_name, tbl_master_country.country_name,tbl_customer_response.required_action,tbl_customer_response.status,tbl_customer_response.po_number,tbl_customer_response.date_added as po_date_added,tbl_customer_response.comment,tbl_customer.first_name,tbl_customer.last_name, tbl_sap_request.po_status,tbl_sap_request.status as request_status, tbl_tasks.close_status,tbl_tasks.cancel_comment, tbl_tasks.cancel_reservation FROM tbl_tasks LEFT join tbl_customer_sap_product ON tbl_tasks.product_id = tbl_customer_sap_product.id LEFT join tbl_master_country ON tbl_tasks.country_id = tbl_master_country.country_id LEFT join tbl_customer_response ON tbl_tasks.task_id = tbl_customer_response.task_id LEFT join tbl_sap_request ON tbl_tasks.sap_request_id = tbl_sap_request.id LEFT join tbl_customer ON tbl_tasks.customer_id = tbl_customer.customer_id WHERE tbl_tasks.status = 1 AND tbl_tasks.request_type = '43' AND tbl_tasks.customer_id = $1 ${additional_condition} ORDER BY tbl_tasks.task_id DESC limit $2 offset $3`;
			
		  	db.any(sql, [customer_id, limit, offset]).then(function (data) {
				resolve(data);
		  	})
			.catch(function (err) {
			  var errorText = common.getErrorText(err);
			  var error = new Error(errorText);
			  reject(error);
			});
		});
	},
	count_enquiries: async (customer_id,status) => {
		return new Promise(function (resolve, reject) {
			let additional_condition ='';
			if(status == 'cancel'){
				additional_condition = ' AND tbl_tasks.close_status = 1';
			}else if (status == 'active'){
				additional_condition = ' AND tbl_tasks.close_status = 0';
			}else{
				additional_condition ='';
			}
		  	db.any(`Select count(tbl_tasks.task_id) as cnt from tbl_tasks LEFT join tbl_customer_sap_product ON tbl_tasks.product_id = tbl_customer_sap_product.id LEFT join tbl_master_country ON tbl_tasks.country_id = tbl_master_country.country_id LEFT join tbl_master_task_status ON tbl_tasks.status = tbl_master_task_status.status_id LEFT join tbl_sap_request ON tbl_tasks.sap_request_id = tbl_sap_request.id LEFT join tbl_customer ON tbl_tasks.customer_id = tbl_customer.customer_id WHERE tbl_tasks.status = 1 AND tbl_tasks.request_type = 43 AND tbl_tasks.customer_id = $1 ${additional_condition}`, [customer_id])
			.then(function (data) {
			  resolve(data[0].cnt);
			})
			.catch(function (err) {
			  var errorText = common.getErrorText(err);
			  var error = new Error(errorText);
			  reject(error);
			});
		});
	},

	stock_task_details: async (task_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_tasks.customer_id as user_id,tbl_tasks.sap_request_id, tbl_sap_request.request_id, tbl_sap_request.po_status, tbl_sap_request.ship_to_party from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE task_id = $1 AND tbl_sap_request.po_status IN (0,2)",[task_id])
			  .then(function (data) {		
				resolve(data[0]);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	get_stock_task_details: async (task_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_tasks.sap_request_id,tbl_tasks.so_id, tbl_sap_request.request_id,tbl_sap_request.reservation_no,tbl_tasks.sales_order_no from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE task_id = $1 AND tbl_tasks.status = 1 AND (tbl_sap_request.po_status = 1 OR tbl_sap_request.po_status = 2)",[task_id])
			  .then(function (data) {	
				resolve(data[0]);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	addTaskRequest: async (task)=>{
		return new Promise(function(resolve, reject) {
			
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,content,date_added,product_id,country_id,quantity,submitted_by,rdd,sap_request_id,so_id,status,close_status,owner,current_ownership,language,share_with_agent) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19) RETURNING task_id',[task.customer_id,task.request_type,task.due_date,task.parent_id,task.content,task.current_date,task.product_id,task.country_id,task.quantity,task.submitted_by,task.rdd,task.sap_request_id,task.so_id,1,0,0,0,task.language,task.share_with_agent])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},

	addCronTaskRequest: async (task)=>{
		return new Promise(function(resolve, reject) {
			
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,content,date_added,product_id,country_id,quantity,submitted_by,rdd,sap_request_id,so_id,status,close_status,owner,current_ownership,language,ccp_posted_with) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19) RETURNING task_id',[task.customer_id,task.request_type,task.due_date,task.parent_id,task.content,task.current_date,task.product_id,task.country_id,task.quantity,task.submitted_by,task.rdd,task.sap_request_id,task.so_id,1,0,0,0,task.language,task.ccp_posted_with])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},

	addCustomerResponse: async (task)=>{
		return new Promise(function(resolve, reject) {
			
				db.one('INSERT INTO tbl_customer_response(task_id,customer_id,required_action,status,expected_closure_date,resp_status,date_added,pause_sla,status_id,action_id,approval_status) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING rs_id',[task.task_id,task.customer_id,task.action_req,task.status,task.expected_closure_date,0,task.date_added,0,1,7,1])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},
	add_inspection_lot : async (data)=>{
		return new Promise(function(resolve, reject) {
			
				db.one('INSERT INTO tbl_tasks_coa_sap(invoice_number,sales_order_number,batch_number, two_series_inspection_lot_number,two_series_material_number,date_added,customer_no) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING  id',[data.invoice_number,data.sales_order_number,data.batch_number,data.two_series_inspection_lot_number,data.two_series_material_numbe,data.date_added,data.customer_no])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},

	getTaskListByRequest: async (request_id) =>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT tbl_tasks.task_id from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.po_status = 1",[request_id])
				.then(async function (data) {
					resolve(data);
				}).catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		})
	},

	UpdateCustomerResponse: async (taskObj) =>{	console.log(taskObj)
		return new Promise(function(resolve, reject) {

			db.any("SELECT tbl_tasks.task_id from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.po_status = 1",[taskObj.request_id])
				.then(async function (data1) {
					if(data1.length!=0){
						for (let index = 0; index <= (data1.length - 1); index++) {			
							await db.any("UPDATE tbl_customer_response SET required_action = $1, status = $2, po_number = $4, resp_status = 1, action_id = '4',status_id = '5' WHERE task_id = $3",[taskObj.action_req, taskObj.status, data1[index].task_id, taskObj.po_no])
							.then(function (data2) {			
								
							}).catch(function (err) {
								var errorText = common.getErrorText(err);
								var error = new Error(errorText);
								reject(error);
							});

							await db.any("UPDATE tbl_tasks SET po_no = $1 WHERE task_id = $2",[taskObj.po_no, data1[index].task_id])
						}
						resolve({success: true});
					}
				}).catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		})
	},

	getLastRequest: async (request_id)=>{
		return new Promise(function (resolve, reject) {
			db.any("SELECT * from tbl_sap_request where request_id=($1) and status=1 ORDER BY id DESC",[request_id])
			  .then(function (data) {
				if(data.length){
					resolve({ success: true, data : data });
				}else{					
					resolve({ success: false, message : 'There are no previous request.' });
				}
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	cancelTask: async (task_id, cancel_comment="",today='')=>{
		return new Promise(function (resolve, reject) {
			if(today==''){
				today = common.currentDateTime();
			}			
			db.any('UPDATE tbl_tasks SET cancel_reservation = 1,cancel_comment = $2,cancel_reservation_date= $3 WHERE task_id = $1',[task_id,cancel_comment,today])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	cancelTaskByReqid: async (sap_request_id, cancel_comment="",today='')=>{
		return new Promise(function (resolve, reject) {
			if(today==''){
				today = common.currentDateTime();
			}			
			db.any('UPDATE tbl_tasks SET cancel_reservation = 1,cancel_comment = $2,cancel_reservation_date= $3 WHERE sap_request_id = $1',[sap_request_id,cancel_comment,today])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},	

	getParticularRequest : async (request_id,reservation_no,so_id)=>{
		return new Promise(function (resolve, reject) {
			db.any('SELECT tbl_sap_request.*,tbl_tasks.task_id,tbl_tasks.language from tbl_sap_request LEFT JOIN tbl_tasks ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.reservation_no = $2 AND tbl_sap_request.status = 1 AND tbl_tasks.so_id= $3 AND tbl_tasks.status = 1 AND tbl_tasks.close_status = 0',[request_id,reservation_no,so_id])
		    .then(function (data) { 
				if(data.length>0){
					resolve(data[0])
				}else{
					resolve(false);
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});	
			
		});
	},

	updateSONumber: async (task_id, so_number, update_date_time = '')=>{
		return new Promise(function (resolve, reject) {
			let today = '';
			if(update_date_time){
			 	today = update_date_time;
			}else{
				today = common.currentDateTime();
			}
			db.any('UPDATE tbl_tasks SET sales_order_no = $2,sales_order_date= $3 WHERE task_id = $1',[task_id,so_number,today])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	getParticularReservation : async (request_id,reservation_no,user_id)=>{
		return new Promise(function (resolve, reject) {
			db.any('SELECT tbl_sap_request.* FROM tbl_sap_request LEFT JOIN tbl_sap_response ON tbl_sap_request.id = tbl_sap_response.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.reservation_no = $2 AND tbl_sap_request.user_id= $3 ORDER BY id DESC',[request_id,reservation_no,user_id])
		    .then(function (data) { 
				resolve(data[0])
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		})
	},

	cancelAvailable : async(sap_request) =>{
		return new Promise(function (resolve, reject) {
			db.any("UPDATE tbl_sap_response SET status = 2 WHERE status = 0 AND sap_request_id = $1",[sap_request.id])
				.then(async function (data1) {				
					db.any("UPDATE tbl_sap_request SET status = 3 WHERE status = 0 AND id = $1 AND reservation_no = $2",[sap_request.id, sap_request.reservation_no])
					.then(function (data2) {
						resolve({success: true});
					}).catch(function (err) {
						var errorText = common.getErrorText(err);
						var error = new Error(errorText);
						reject(error);
					});
				}).catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		})
	},

	cancelAvailableRequest : async(sap_request) =>{
		return new Promise(function (resolve, reject) {
							
			db.any("UPDATE tbl_sap_request SET status = 3 WHERE status = 0 AND id = $1",[sap_request.id])
			.then(function (data2) {
				resolve({success: true});
			}).catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
				
		})
	},

	getAllReservedResponse: async(request_id,user_id) =>{
		return new Promise(function (resolve, reject) {
			db.any('SELECT tbl_sap_request.*,tbl_sap_response.so_id FROM tbl_sap_response LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_sap_response.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.status = 1 AND tbl_sap_response.status = 1 AND tbl_sap_request.user_id= $2',[request_id,user_id])
		    .then(function (data) {
				if(data.length){
					resolve({ success: true, data : data });
				}else{					
					resolve({ success: false, message : 'There are no previous request.' });
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		})
	},

	getproductDetailsByCode: async(code, user_id) =>{
		return new Promise(function (resolve, reject) {
			db.any('SELECT tbl_customer_sap_product.id, tbl_customer_sap_product.name FROM tbl_customer_sap_product LEFT JOIN tbl_customer_sap_product_code ON tbl_customer_sap_product_code.product_id = tbl_customer_sap_product.id WHERE tbl_customer_sap_product_code.code = $1 AND tbl_customer_sap_product.customer_id = $2',[code, user_id])
		    .then(function (data) { 
				if(data.length){
					resolve({ success: true, data : data });
				}else{					
					resolve({ success: false, message : 'There are no previous request.' });
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},

	getmasterproductByCode: async(code) =>{
		return new Promise(function (resolve, reject) {
			db.any('SELECT product_name as name, product_id as id FROM tbl_master_product WHERE sku_code = $1',[code])
		    .then(function (data) { 
				if(data.length){
					resolve({ success: true, data : data });
				}else{					
					resolve({ success: false, message : 'There are no previous request.' });
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},

	updatePreferedDateForSingle: async (request_id, preffered)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_response SET preferred_date = $2 WHERE sap_request_id = $1',[request_id, preffered])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	updatePreferedDateForMultiple: async (response_id, preffered)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_response SET preferred_date = $2 WHERE id = $1',[response_id, preffered])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},

	list_customer_shipto: async (customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_company_shipto.shipto_id as code, tbl_company_shipto.shipto_name  as name, tbl_company_shipto.city, tbl_company_shipto.country,tbl_company_shipto.soldto_id from tbl_company_shipto LEFT JOIN tbl_master_company_soldto ON tbl_master_company_soldto.sap_ref_no = tbl_company_shipto.soldto_id LEFT JOIN tbl_customer ON tbl_customer.company_id = tbl_master_company_soldto.company_id WHERE tbl_customer.customer_id = $1 order by tbl_company_shipto.shipto_name ASC",[customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	get_customer_products_details: async (shipto_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT product_sku from tbl_company_sap_product WHERE shipto_id = $1",[shipto_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	get_customer_shipto_details: async (shipto_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT * from tbl_company_shipto WHERE shipto_id = $1",[shipto_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	list_customer_products: async (shipto_id, lang ='')=>{		
		return new Promise(function (resolve, reject) {
			let product_colm =  'tbl_master_product.product_name';
			if(lang!= '' && lang!= 'en'){
				product_colm = 'tbl_master_product.product_name_'+lang;
			}
			db.any(`SELECT tbl_master_product.product_id as id, product_sku, ${product_colm} as name from tbl_company_sap_product INNER JOIN tbl_master_product ON tbl_master_product.sku_code = tbl_company_sap_product.product_sku WHERE tbl_company_sap_product.shipto_id = $1 order by ${product_colm} ASC`,[shipto_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	get_products_code: async (product_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT sku_code as code from tbl_master_product WHERE product_id = $1",[product_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	get_customer_soldto_details:  async (shipto_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_master_company_soldto.street, tbl_master_company_soldto.city, tbl_master_company_soldto.country, tbl_master_company_soldto.postalcode, tbl_master_company.company_name from tbl_company_shipto LEFT JOIN tbl_master_company_soldto ON tbl_master_company_soldto.sap_ref_no = tbl_company_shipto.soldto_id LEFT JOIN tbl_master_company ON tbl_master_company.company_id = tbl_master_company_soldto.company_id WHERE tbl_company_shipto.shipto_id = $1",[shipto_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	sap_request_exist: async (request_id, customer_id = 0)=>{		
		return new Promise(function (resolve, reject) {
			let sql = "SELECT * from tbl_sap_request WHERE tbl_sap_request.request_id = $1";
			let parm_arr = [request_id];
			if(customer_id!=0){
				sql = "SELECT * from tbl_sap_request WHERE tbl_sap_request.request_id = $1 AND user_id = $2";
				parm_arr = [request_id,customer_id];
			}console.log(parm_arr)

			db.any(sql,parm_arr)
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	updateCustomerDelayPO: async (request_id, soldto_id)=>{
		return new Promise(function (resolve, reject) {
			db.any('UPDATE tbl_sap_request SET po_status = 2, sold_to_party = $2 WHERE status = 1 AND po_status = 0 AND request_id = $1',[request_id, soldto_id])
		    .then(function (data) {
				resolve({success: true});
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},
	get_country: async (country_name) => {
		return new Promise(function (resolve, reject) {

			db.any('select country_id as id from tbl_master_country WHERE country_name = ($1) LIMIT 1', [country_name])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	get_country_by_id: async (country_id) => {
		return new Promise(function (resolve, reject) {

			db.any('select * from tbl_master_country WHERE country_id = ($1) LIMIT 1', [country_id])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_ship_status: async (invoice_id) => {
		return new Promise(function (resolve, reject) {

			db.one('select tbl_task_logistic.*, tbl_task_invoice.invoice_number, tbl_task_invoice.invoice_date, tbl_task_invoice.packing_date, tbl_task_invoice.invoice_location, tbl_task_invoice.packing_location, tbl_task_invoice.packing_file_name, tbl_task_invoice.invoice_file_name from tbl_task_invoice LEFT JOIN tbl_task_logistic ON tbl_task_invoice.invoice_number = tbl_task_logistic.invoice_id WHERE tbl_task_invoice.invoice_number = ($1) limit 1', [invoice_id])
				.then(function (data) {					
					resolve(data);					
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	
	validate_ship_to_party: async (shipto_id, customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_company_shipto.shipto_id, tbl_company_shipto.shipto_name, tbl_company_shipto.city, tbl_company_shipto.country,tbl_company_shipto.soldto_id from tbl_company_shipto LEFT JOIN tbl_master_company_soldto ON tbl_master_company_soldto.sap_ref_no = tbl_company_shipto.soldto_id LEFT JOIN tbl_customer ON tbl_customer.company_id = tbl_master_company_soldto.company_id WHERE tbl_company_shipto.shipto_id = $1 AND tbl_customer.customer_id = $2",[shipto_id, customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},

	validate_product: async (product_code, shipto_id, customer_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_company_shipto.shipto_id, tbl_company_shipto.shipto_name, tbl_company_shipto.city, tbl_company_shipto.country,tbl_company_shipto.soldto_id from tbl_master_product LEFT JOIN tbl_company_sap_product On tbl_company_sap_product.product_sku = tbl_master_product.sku_code LEFT JOIN  tbl_company_shipto ON tbl_company_shipto.shipto_id = tbl_company_sap_product.shipto_id LEFT JOIN tbl_master_company_soldto ON tbl_master_company_soldto.sap_ref_no = tbl_company_shipto.soldto_id LEFT JOIN tbl_customer ON tbl_customer.company_id = tbl_master_company_soldto.company_id  WHERE tbl_master_product.sku_code = $1 AND  tbl_company_shipto.shipto_id = $2 AND tbl_customer.customer_id = $3",[product_code, shipto_id, customer_id])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
	isExistSONumber: async (task_id)=>{
		return new Promise(function (resolve, reject) {			
			db.any('select sales_order_no tbl_tasks WHERE task_id = $1',[task_id])
		    .then(function (data) {
				resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});	
			
		});
	},
}
