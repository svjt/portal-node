const db =require('../configuration/dbConn');
const common = require('../controllers/common');

module.exports = {
	list_user: async (limit, offset, keyword)=>{
		return new Promise(function(resolve, reject) {
			// SATYAJIT
			var condition = '';
			keyword = keyword || "";
			if( keyword != ''){
				condition = ' AND (LOWER(tbl_employee.first_name) LIKE \'%'+ keyword.toLowerCase() +'%\' OR LOWER(tbl_employee.last_name) LIKE \'%'+ keyword.toLowerCase() +'%\')';
			}
			// SATYAJIT
			var sql = 
			`SELECT 
				tbl_employee.employee_id::INTEGER, 
				tbl_employee.first_name, 
				tbl_employee.last_name, 
				tbl_designation.desig_name,
				tbl_department.dept_name,
				tbl_employee.status,
				TO_CHAR(login_log.max_login_date,'dd/mm/yyyy HH12:MI AM') AS display_login_date
			FROM tbl_employee 
			LEFT JOIN tbl_designation 
				ON tbl_employee.desig_id = tbl_designation.desig_id 
			LEFT JOIN tbl_department 
				ON tbl_employee.dept_id = tbl_department.dept_id 
			LEFT JOIN (
				SELECT 
					employee_id,
					MAX(login_date) AS max_login_date
				FROM tbl_login_logs GROUP BY employee_id
			) AS login_log 
			ON tbl_employee.employee_id = login_log.employee_id 
			WHERE tbl_employee.status!=2 
				AND tbl_employee.employee_id != 1 
				${condition} 
			ORDER BY tbl_employee.employee_id DESC 
			LIMIT $1 offset $2`;
			db.any(sql,[limit, offset])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	login_log : async (login_attempt)=>{
		return new Promise(function(resolve, reject) {
		var sql = 
		`INSERT INTO 
		tbl_login_logs(
			employee_id,login_date,ip_address
		) 
		VALUES($1,$2,$3) 
		RETURNING log_id`;
		db.one(sql,[login_attempt.employee_id,login_attempt.login_date,login_attempt.ip_address])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	count_all_user: async ( keyword )=>{
		return new Promise(function(resolve, reject) {
					// SATYAJIT
					var condition = '';
					keyword = keyword || "";
					if( keyword != ''){
						condition = ' AND (LOWER(tbl_employee.first_name) LIKE \'%'+ keyword.toLowerCase() +'%\' OR LOWER(tbl_employee.last_name) LIKE \'%'+ keyword.toLowerCase() +'%\')';
					}
					// SATYAJIT
			    db.any('Select count(employee_id) as cnt from tbl_employee where status !=2 AND tbl_employee.employee_id != 1'+condition)
			    .then(function (data) {	

			      resolve(data[0].cnt);
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });

		}); 

	},
	list_all_user: async ()=>{
		return new Promise(function(resolve, reject) {

			db.any("select tbl_employee.employee_id::INTEGER, tbl_employee.first_name, tbl_employee.last_name, tbl_designation.desig_name,tbl_department.dept_name,tbl_employee.status from tbl_employee LEFT JOIN tbl_designation on tbl_employee.desig_id = tbl_designation.desig_id   LEFT JOIN tbl_department on tbl_employee.dept_id = tbl_department.dept_id where tbl_employee.status=1 AND tbl_employee.employee_id != 1 ORDER BY tbl_employee.employee_id DESC")
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	list_cc_employee : async (task_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
				`SELECT 
					tbl_employee.employee_id::INTEGER,
					tbl_employee.first_name,
					tbl_employee.last_name,
					tbl_designation.desig_name,
					tbl_employee.status 
				FROM tbl_employee 
				LEFT JOIN tbl_cc_employees 
					ON tbl_employee.employee_id = tbl_cc_employees.employee_id  
				LEFT JOIN tbl_designation 
					ON tbl_employee.desig_id = tbl_designation.desig_id  
				WHERE tbl_employee.status = 1 
					AND tbl_employee.employee_id != 1 
					AND tbl_cc_employees.task_id = ($1)
					AND tbl_cc_employees.status = 1
				ORDER BY tbl_employee.employee_id DESC`;

			db.any(sql,[task_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	select_cc_emp : async (task_id,employee_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = `SELECT * FROM tbl_cc_employees WHERE task_id = ($1) AND employee_id = ($2) `;

			db.any(sql,[task_id,employee_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	insert_cc_emp : async (cc_details)=>{
		return new Promise(function(resolve, reject) {
		var sql = 
		`INSERT INTO 
		tbl_cc_employees(
			employee_id,task_id,status
		) 
		VALUES($1,$2,$3) 
		RETURNING ecc_id`;
		db.one(sql,[cc_details.employee_id,cc_details.task_id,cc_details.status])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	update_cc_emp : async (task_id,employee_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`UPDATE tbl_cc_employees 
			 SET 
				status = 1
			 WHERE task_id = ($1)
			 AND employee_id = ($2) 
			 AND status = 2`;

			db.result(sql,[task_id,employee_id],r => r.rowCount)
			.then(function (sdata) {
				resolve(sdata)
			})
			.catch(function (terr) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 

	},
	remove_bulk_cc_employee :  async (task_id,employee)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`UPDATE tbl_cc_employees 
			 SET 
				status = 2
			 WHERE task_id = ($1)
			 AND employee_id NOT IN ($2:csv) 
			 AND status = 1`;

			db.result(sql,[task_id,employee],r => r.rowCount)
			.then(function (sdata) {
				resolve(sdata)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 

	},
	remove_all_cc_employees :  async (task_id,employee)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`UPDATE tbl_cc_employees 
			 SET 
				status = 2
			 WHERE task_id = ($1)
			 AND status = 1`;

			db.result(sql,[task_id,employee],r => r.rowCount)
			.then(function (sdata) {
				resolve(sdata)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 

	},
	task_cc_employees : async (task_id,assigned_to)=>{
		return new Promise(function(resolve, reject) {
				var sql = 
					`SELECT 

						tbl_employee.employee_id::INTEGER,
						tbl_employee.first_name,
						tbl_employee.last_name,
						tbl_employee.email,
						tbl_designation.desig_name,
						tbl_employee.status 

					FROM tbl_employee 
					LEFT JOIN tbl_cc_employees 
						ON tbl_employee.employee_id = tbl_cc_employees.employee_id  
					LEFT JOIN tbl_designation 
						ON tbl_employee.desig_id = tbl_designation.desig_id  
					WHERE tbl_employee.status = 1 
						AND tbl_employee.employee_id != 1 

						AND tbl_cc_employees.task_id = ($1)
						AND tbl_cc_employees.employee_id != ($2) 
						AND tbl_cc_employees.status = 1

					ORDER BY tbl_employee.employee_id DESC`;

				db.any(sql,[task_id,assigned_to])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	task_cc_employees_arr : async (task_id,assigned_to_arr)=>{
		return new Promise(function(resolve, reject) {
				var sql = 
					`SELECT 

						tbl_employee.employee_id::INTEGER,
						tbl_employee.first_name,
						tbl_employee.last_name,
						tbl_employee.email,
						tbl_designation.desig_name,
						tbl_employee.status 

					FROM tbl_employee 
					LEFT JOIN tbl_cc_employees 
						ON tbl_employee.employee_id = tbl_cc_employees.employee_id  
					LEFT JOIN tbl_designation 
						ON tbl_employee.desig_id = tbl_designation.desig_id  
					WHERE tbl_employee.status = 1 
						AND tbl_employee.employee_id != 1 

						AND tbl_cc_employees.task_id = ($1)
						AND tbl_cc_employees.employee_id NOT IN ($2:csv) 
						AND tbl_cc_employees.status = 1

					ORDER BY tbl_employee.employee_id DESC`;

				db.any(sql,[task_id,assigned_to_arr])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	get_user: async (emp_id)=>{
		return new Promise(function(resolve, reject) {

			db.any("Select TE.sales_emp_id,TE.employee_id,TE.cqt_emp_id,TE.first_name,TE.status,TE.level,TE.last_name,TE.employee_ref as emp_ref, TE.manager_id, TE.email,TE.desig_id, TE.dept_id,TE.drupal_uid, TE2.first_name as manager_fname, TE2.last_name as manager_lname, DSG.desig_name,TE.on_leave from tbl_employee AS TE LEFT join tbl_employee as TE2 on TE.manager_id = TE2.employee_id LEFT join tbl_designation AS DSG ON TE2.desig_id = DSG.desig_id where TE.employee_id=($1) and TE.status!=2",[emp_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	add_user: async (emp)=>{
		return new Promise(function(resolve, reject) {
			
		db.one('INSERT INTO tbl_employee(first_name,last_name,email,manager_id,desig_id,dept_id,employee_ref,status,password,drupal_uid,date_added,level) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING employee_id',[emp.first_name,emp.last_name,emp.email,emp.manager_id,emp.desig_id,emp.dept_id,emp.emp_ref,emp.status,emp.password,emp.drupal_uid,emp.date_added,emp.level])
		    .then(function (data) {
		      resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_user: async (emp,emp_id)=>{
		return new Promise(function(resolve, reject) {
		
		db.result("UPDATE tbl_employee set first_name=($1),last_name=($2),email=($3),manager_id=($4),desig_id=($5),dept_id=($6),employee_ref=($7),status=($9),level=($10) where employee_id=($8)",[emp.first_name,emp.last_name,emp.email,emp.manager_id,emp.desig_id,emp.dept_id,emp.emp_ref,emp_id,emp.status,emp.level],r => r.rowCount)
		    .then(function (data) {
		    	//console.log(data);
		      resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_password: async (passwordHash,emp_id)=>{
		return new Promise(function(resolve, reject) {
		
		db.result("UPDATE tbl_employee SET password=($1) WHERE employee_id=($2)",[passwordHash,emp_id],r => r.rowCount)
		    .then(function (data) {
		    	//console.log(data);
		      resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	findByEmail: async (email)=>{
		return new Promise(function(resolve, reject) {
			db.any("Select tbl_employee.employee_id::INTEGER,tbl_employee.password,tbl_employee.first_name,tbl_designation.desig_name,tbl_employee.desig_id,tbl_employee.drupal_uid,tbl_employee.level from tbl_employee left join tbl_designation on tbl_employee.desig_id=tbl_designation.desig_id where LOWER(tbl_employee.email)=LOWER($1) and tbl_employee.status='1'",[email])
		    .then(function (data) {  	
		      resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	findById: async (emp_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select employee_id::INTEGER from tbl_employee where employee_id=($1) and status='1'",[
	        emp_id])
		    .then(function (data) {  	
		      resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	my_customers: async (emp_id)=>{		// dependency on leave module --- SATYAJIT
		return new Promise(function(resolve, reject) {

		    db.any('select tbl_customer.customer_id, tbl_customer.first_name, tbl_customer.last_name, tbl_customer.vip_customer,tbl_master_company.company_name,tbl_customer.email, tbl_customer.drupal_id from tbl_customer_team INNER JOIN tbl_customer ON tbl_customer_team.customer_id = tbl_customer.customer_id LEFT JOIN tbl_master_company ON tbl_customer.company_id=tbl_master_company.company_id where tbl_customer_team.employee_id=($1) and tbl_customer_team.status=1 AND tbl_customer.status = 1;',[emp_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	my_customers_multi: async (emp_id,query_arr)=>{		// dependency on leave module --- SATYAJIT
		return new Promise(function(resolve, reject) {

				var query_string = '';

				if(query_arr && query_arr.compid && query_arr.compid.length > 0){	
					query_string += ` AND C.company_id IN (${query_arr.compid}) `;
				}

				var sql = 
				`SELECT 
					C.customer_id,
					C.first_name,
					C.last_name,
					C.vip_customer 
				FROM tbl_customer_team AS CT 
				LEFT JOIN tbl_customer AS C 
					ON CT.customer_id = C.customer_id 
				WHERE CT.employee_id = ($1)  
				AND C.status = 1 
				${query_string} 
				GROUP BY C.customer_id;`

			db.any(sql,[emp_id])
			.then(function (data) {
			resolve(data)
			})
			.catch(function (err) { 
			var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});

		});  

	},
	company_customers: async (emp_id,company_id)=>{
		return new Promise(function(resolve, reject) {

		    db.any('select tbl_customer.customer_id, tbl_customer.first_name, tbl_customer.last_name, tbl_customer.vip_customer,tbl_master_company.company_name,tbl_customer.email, tbl_customer.drupal_id from tbl_customer_team INNER JOIN tbl_customer ON tbl_customer_team.customer_id = tbl_customer.customer_id LEFT JOIN tbl_master_company ON tbl_customer.company_id=tbl_master_company.company_id where tbl_customer_team.employee_id=($1) and tbl_master_company.company_id=($2) and tbl_customer_team.status=1;',[emp_id,company_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	exclude_company_customers: async (emp_id,company_id,customer_id)=>{
		return new Promise(function(resolve, reject) {
				var sql = `select tbl_customer.customer_id, tbl_customer.first_name, tbl_customer.last_name, tbl_customer.vip_customer,tbl_master_company.company_name,tbl_customer.email, tbl_customer.drupal_id from tbl_customer_team INNER JOIN tbl_customer ON tbl_customer_team.customer_id = tbl_customer.customer_id LEFT JOIN tbl_master_company ON tbl_customer.company_id=tbl_master_company.company_id where tbl_customer_team.employee_id=($1) and tbl_master_company.company_id=($2) and tbl_customer.customer_id!=($3) and tbl_customer_team.status=1 and tbl_customer.role_type = '2'  ;`
		    db.any(sql,[emp_id,company_id,customer_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	check_employee_team: async (emp_id,customer_id)=>{
		return new Promise(function(resolve, reject) {
				var sql = 
				`SELECT 
					team_id 
				FROM 
					tbl_customer_team 
				WHERE employee_id=($1) 
				AND customer_id=($2)`;
		    db.any(sql,[emp_id,customer_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	email_exists: async (email,emp_id)=>{
		return new Promise(function(resolve, reject) {

			if(emp_id === undefined){ 
				db.any('select employee_id::INTEGER from tbl_employee where LOWER(email)=LOWER($1) and status=1',[email])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}else{
				db.any('select employee_id::INTEGER from tbl_employee where LOWER(email)=LOWER($1) and status=1 and employee_id!=($2)',[email,emp_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}

		}); 

	},
	delete_user: async (emp_id)=>{
		return new Promise(function(resolve, reject) {
			
		db.result("Update tbl_employee set status='2' where employee_id=($1)",[emp_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    })
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	list_other_user: async (emp_id)=>{
		return new Promise(function(resolve, reject) {


			db.any("select tbl_employee.employee_id::INTEGER, tbl_employee.first_name, tbl_employee.last_name, tbl_designation.desig_name, tbl_department.dept_name,tbl_employee.on_leave from tbl_employee LEFT JOIN tbl_designation on tbl_employee.desig_id = tbl_designation.desig_id LEFT JOIN tbl_department on tbl_employee.dept_id = tbl_department.dept_id  where tbl_employee.status='1' and tbl_employee.employee_id !=($1) AND tbl_employee.employee_id !=1",[emp_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	emp_ref_exists: async (ref,emp_id)=>{
		return new Promise(function(resolve, reject) {
			if(emp_id === undefined){ 
				db.any('select employee_id::INTEGER from tbl_employee where employee_ref=($1) and status=1',[ref])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}else{
				db.any('select employee_id::INTEGER from tbl_employee where employee_ref=($1) and status=1 and employee_id!=($2)',[ref,emp_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}

		}); 

	},
	emp_id_exists: async (emp_id,my_emp_id)=>{
		return new Promise(function(resolve, reject) {
			if(my_emp_id === undefined){ 
				db.any('select employee_id::INTEGER from tbl_employee where employee_id=($1) and status=1',[emp_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}else{
				db.any('select employee_id::INTEGER from tbl_employee where employee_id=($1) and status=1 and employee_id!=($2)',[emp_id,my_emp_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
			}
		}); 

	},
	emp_desig_exists: async (desig_id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select desig_id from tbl_designation where desig_id=($1) and status=1',[desig_id])
		    .then(function (data) {
		    	if (data.length > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	get_desig_name: async (desig_id)=>{
		return new Promise(function(resolve, reject) {
			db.one('select desig_name from tbl_designation where desig_id=($1) and status=1',[desig_id])
		    .then(function (data) {
		      	resolve( data);
		    })
		    .catch(function (err) {
		    	//console.log(err);
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	get_dept_name: async (dept_id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select dept_name from tbl_department where dept_id=($1) and status=1',[dept_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	get_drupal_uid: async (emp_id)=>{
		return new Promise(function(resolve, reject) {
			db.one('select drupal_uid from tbl_employee where employee_id=($1)',[emp_id])
		    .then(function (data) {
		      	resolve( data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	get_designations: async ()=>{
		return new Promise(function(resolve, reject) {
			db.any('select desig_id, desig_name from tbl_designation where status=1')
		    .then(function (data) { 
		    	resolve(data);
		    })
		    .catch(function (err) { 
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	emp_dept_exists: async (dept_id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select dept_id from tbl_department where dept_id=($1) and status=1',[dept_id])
		    .then(function (data) {
		    	if (data.length > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	get_departments: async ()=>{
		return new Promise(function(resolve, reject) {
			db.any('select dept_id, dept_name from tbl_department where status=1')
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	list_company: async (emp_id)=>{
		return new Promise(function(resolve, reject) {
				db.any('SELECT DISTINCT(tbl_master_company.company_id),tbl_master_company.company_name FROM tbl_customer_team INNER JOIN tbl_customer ON tbl_customer_team.customer_id = tbl_customer.customer_id LEFT JOIN tbl_master_company ON tbl_customer.company_id = tbl_master_company.company_id WHERE tbl_customer_team.employee_id=($1) AND tbl_customer_team.status=1 ;',[emp_id])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });
		}); 
	},
	emp_exists: async (employee_id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select employee_id::INTEGER from tbl_employee where employee_id=($1) and status=1',[employee_id])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].employee_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_emp_leave_status: async (employee_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				employee_id
			FROM tbl_leaves 
			WHERE employee_id=($1) 
			AND status = 1 
			AND (
				(($2) BETWEEN date_from AND date_to ) OR 
				(($3) BETWEEN date_from AND date_to )
			)  `;
			db.any(sql,[employee_id,from_date,to_date])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].employee_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_emp_leave_management_status: async (employee_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				new_emp_id
			FROM tbl_leaves_management 
			WHERE new_emp_id=($1) 
			AND status = 1 
			AND (
				(($2) BETWEEN date_from AND date_to ) OR 
				(($3) BETWEEN date_from AND date_to )
			)  `;
			db.any(sql,[employee_id,from_date,to_date])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].new_emp_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_emp_updt_leave_status: async (employee_id,leave_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				employee_id
			FROM tbl_leaves 
			WHERE employee_id = ($1) 
			AND id != ($4)
			AND status = 1 
			AND (
				(($2) BETWEEN date_from AND date_to ) OR 
				(($3) BETWEEN date_from AND date_to )
			)  `;

			db.any(sql,[employee_id,from_date,to_date,leave_id])
		    .then(function (data) {
				console.log('employee data',data);
		    	if (data.length > 0 && data[0].employee_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_emp_updt_leave_management_status: async (employee_id,leave_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				new_emp_id
			FROM tbl_leaves_management 
			WHERE new_emp_id = ($1) 
			AND leaves_id != ($4)
			AND status = 1 
			AND (
				(($2) BETWEEN date_from AND date_to ) OR 
				(($3) BETWEEN date_from AND date_to )
			)  `;
			db.any(sql,[employee_id,from_date,to_date,leave_id])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].new_emp_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_emp_customer: async (employee_id,customer_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				team_id 
			FROM tbl_customer_team
			WHERE customer_id = ($2) 
			AND employee_id = ($1) 
			AND status = 1 `;
			db.any(sql,[employee_id,customer_id])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].team_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_leave_emp_customer: async (employee_id,customer_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				lm_id 
			FROM tbl_leaves_management
			WHERE new_emp_id = ($1) 
			AND customer_id = ($2)
			AND (
				(($3) BETWEEN date_from AND date_to ) OR 
				(($4) BETWEEN date_from AND date_to )
			) 
			AND status = 1 `;
			db.any(sql,[employee_id,customer_id,from_date,to_date])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].lm_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	check_leave_emp_customer_update: async (employee_id,customer_id,from_date,to_date)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				lm_id 
			FROM tbl_leaves_management
			WHERE new_emp_id = ($1) 
			AND customer_id = ($2)
			AND (
				(($3) BETWEEN date_from AND date_to ) OR 
				(($4) BETWEEN date_from AND date_to )
			) 
			AND status = 1 `;
			db.any(sql,[employee_id,customer_id,from_date,to_date])
		    .then(function (data) {
		    	if (data.length > 0 && data[0].lm_id > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	get_employee_details: async (employee_id, isMulti = false) => {
		return new Promise(function (resolve, reject) {

			if(isMulti){
				db.any(`select tbl_employee.employee_id::INTEGER,tbl_master_language.language,tbl_master_language.code AS lang_code, tbl_employee.first_name, tbl_employee.last_name, tbl_employee.email, tbl_designation.desig_name,tbl_department.dept_name,tbl_employee.status from tbl_employee LEFT JOIN tbl_designation on tbl_employee.desig_id = tbl_designation.desig_id LEFT JOIN tbl_master_language ON tbl_employee.language_code=tbl_master_language.code  LEFT JOIN tbl_department on tbl_employee.dept_id = tbl_department.dept_id where tbl_employee.status=1 AND tbl_employee.employee_id != 1 AND tbl_employee.employee_id IN ($1:csv) ORDER BY tbl_employee.employee_id DESC`,[employee_id])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
			}else{
				db.any(`select tbl_employee.employee_id::INTEGER,tbl_master_language.language,tbl_master_language.code AS lang_code, tbl_employee.first_name, tbl_employee.last_name, tbl_employee.email, tbl_designation.desig_name,tbl_department.dept_name,tbl_employee.status from tbl_employee LEFT JOIN tbl_designation on tbl_employee.desig_id = tbl_designation.desig_id LEFT JOIN tbl_master_language ON tbl_employee.language_code=tbl_master_language.code  LEFT JOIN tbl_department on tbl_employee.dept_id = tbl_department.dept_id where tbl_employee.status=1 AND tbl_employee.employee_id != 1 AND tbl_employee.employee_id = ($1) ORDER BY tbl_employee.employee_id DESC`, [employee_id])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
			}
		});
	},
}
