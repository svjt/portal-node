const db = require('../configuration/dbConn');
const common = require('../controllers/common');

module.exports = {
	list_country: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			if (lang == 'en') {
				var sql = 'select country_id, country_name from tbl_master_country';
			} else {
				var lang_code = 'country_name_' + lang;
				var sql = `select ${lang_code} as country_name, country_id from tbl_master_country`;
			}
			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	list_document_type: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			if (lang == 'en') {
				var sql = 'select doc_name as doc_id, doc_name as doc_name from tbl_master_task_document_type';
			} else {
				var lang_code = 'doc_name_' + lang;
				var sql = `select ${lang_code} as doc_name, doc_name as doc_id from tbl_master_task_document_type`;
			}
			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});

	},
	list_product_unit: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			if (lang == 'en') {
				var sql = 'select unit_name as unit_id, unit_name as unit_name from tbl_master_unit';
			} else {
				var lang_code = 'unit_name_' + lang;
				var sql = `select ${lang_code} as unit_name, unit_name as unit_id from tbl_master_unit`;
			}
			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	list_stability_data: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			if (lang == 'en') {
				var sql = 'select name as id, name as name from tbl_master_task_stability_data';
			} else {
				var lang_code = 'name_' + lang;
				var sql = `select ${lang_code} as name, name as id from tbl_master_task_stability_data`;
			}
			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	list_language: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			// if (lang == 'en') {
			// 	var sql = 'select id, language, code from tbl_master_language';
			// } else {
			// 	var lang_code = 'language_' + lang;
			// 	var sql = `select ${lang_code} as language, id, code from tbl_master_language`;
			// }
			//console.log('=====', sql)
			var lang_code = 'language_' + lang;
			var sql = `select ${lang_code} as language, id, code from tbl_master_language`;
			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});

	},
	list_company: async () => {
		return new Promise(function (resolve, reject) {

			db.any('select  company_id, company_name from tbl_master_company')
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	checkCCCustomer: async (customer_id, task_id) => {
		return new Promise(function (resolve, reject) {
			var sql =
				`SELECT 
					customer_id::INTEGER
				FROM tbl_cc_customers 
				WHERE task_id = ($1)
					AND customer_id = ($2)`;

			db.any(sql, [task_id, customer_id])
				.then(function (data) {
					if (data && data.length > 0 && data[0].customer_id > 0) {
						resolve(data[0].customer_id);
					} else {
						resolve(0);
					}
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	udpate_cc_customer: async (customer_id) => {
		return new Promise(function (resolve, reject) {

			db.result("UPDATE tbl_cc_customers SET status = 1 WHERE customer_id=($1) AND status = 2 ", [customer_id], r => r.rowCount)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	insert_cc_customer: async (cc_cust) => {
		return new Promise(function (resolve, reject) {

			db.one('INSERT INTO tbl_cc_customers (customer_id,task_id,status) VALUES ($1,$2,$3) RETURNING ccc_id', [cc_cust.customer_id, cc_cust.task_id, cc_cust.status])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	remove_excess_cc_customers: async (total_cc_custs, task_id) => {
		return new Promise(function (resolve, reject) {

			db.result("UPDATE tbl_cc_customers SET status = 2 WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND status = 1 ", [total_cc_custs, task_id], r => r.rowCount)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	udpateAllCCCustomer: async (task_id) => {
		return new Promise(function (resolve, reject) {
			db.result("UPDATE tbl_cc_customers SET status = 2 WHERE task_id=($1) AND status = 1 ", [task_id], r => r.rowCount)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	list_all_products: async (lang) => {
		return new Promise(function (resolve, reject) {
			lang = (typeof lang !== 'undefined') ? lang : 'en';
			if (lang == 'en') {
				var sql = 'select  product_name, product_id from tbl_master_product WHERE status = 1 ORDER BY product_name ASC';
			} else {
				var lang_code = 'product_name_' + lang;
				var sql = `select 
				(CASE 
					WHEN 
						(${lang_code} IS NULL OR ${lang_code} = '') 
					THEN 
						product_name
					ELSE
						${lang_code}
				END)
				AS product_name, product_id from tbl_master_product WHERE status = 1 ORDER BY product_name ASC`;
			}

			db.any(sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	get_sales_force_customer: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			var sql =
				`SELECT sales_customer_id,status,activated,company_id,country_id,first_name,last_name,email,phone_no,cqt_customer_id FROM tbl_customer WHERE customer_id =($1)`
			db.any(sql, [customer_id])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_company_details: async (company_id) => {
		return new Promise(function (resolve, reject) {

			let sql = `SELECT * FROM tbl_master_company WHERE company_id = $1`;
			db.any(sql, [company_id])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	country_name: async (country_id) => {
		return new Promise(function (resolve, reject) {

			db.any('select country_name from tbl_master_country WHERE country_id = ($1) LIMIT 1', [country_id])
				.then(function (data) {
					resolve(data[0].country_name);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	}
}