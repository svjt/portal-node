const db =require('../configuration/dbConn');
const common = require('../controllers/common');
var trim = require('trim');

module.exports = {
	add_task: async (task)=>{
		return new Promise(function(resolve, reject) {

			if(
				task.request_type_id == 22 || task.request_type_id == 4 || task.request_type_id == 10 || task.request_type_id == 12 || task.request_type_id == 21 || task.request_type_id == 19 || task.request_type_id == 18 || task.request_type_id == 17 || 
				task.request_type_id == 27 || task.request_type_id == 28 || 
				task.request_type_id == 29 || task.request_type_id == 30 || 
				task.request_type_id == 32 || task.request_type_id == 33 || task.request_type_id == 35 || task.request_type_id == 36 || task.request_type_id == 37 || task.request_type_id == 38 || task.request_type_id == 39 || task.request_type_id == 40 || task.request_type_id == 42
			){
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,pharmacopoeia,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,dmf_number,rdfrc,notification_number,apos_document_type,gmp_clearance_id,tga_email_id,applicant_name,doc_required,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.pharmacopoeia,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.dmf_number,task.rdfrc,task.notification_number,task.apos_document_type,task.gmp_clearance_id,task.tga_email_id,task.applicant_name,task.doc_required,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 5 || task.request_type_id == 8 || task.request_type_id == 14 || task.request_type_id == 15 || task.request_type_id == 16 || task.request_type_id == 6 || task.request_type_id == 11 || task.request_type_id == 20)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 3 || task.request_type_id == 2)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,polymorphic_form,pharmacopoeia,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.polymorphic_form,task.pharmacopoeia,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 9)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,audit_visit_site_name,request_audit_visit_date,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.audit_visit_site_name,task.request_audit_visit_date,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 13)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,stability_data_type,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.stability_data_type,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 23 || task.request_type_id == 31 || task.request_type_id == 41 || task.request_type_id == 44)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,quantity,rdd,submitted_by,close_status,drupal_task_id,priority,front_due_date,request_category,share_with_agent,pharmacopoeia,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.quantity,task.rdd,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.front_due_date,1,task.share_with_agent,task.pharmacopoeia,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 7 || task.request_type_id == 34)
			{
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,product_id,country_id,discussion,quantity,batch_number,nature_of_issue,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.quantity,task.batch_number,task.nature_of_issue,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,3,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}

			if(task.request_type_id == 1)
			{
				let service_request_array = [];

				if(task.sample == 1)
					service_request_array.push("Samples");
				 
				if(task.working_standards == 1)
					service_request_array.push("Working Standards");

				if(task.impurities == 1)
					service_request_array.push("Impurities");

				let service_request_type = service_request_array.toString();
				
				console.log("Sample body",task);
				db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,service_request_type,status,content,date_added,product_id,country_id,discussion,quantity,number_of_batches,pharmacopoeia,working_quantity,specify_impurity_required,impurities_quantity,shipping_address,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,ccp_posted_by,language,rating_skipped) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29) RETURNING task_id',[task.customer_id,task.request_type_id,task.due_date,task.parent_id,service_request_type,1,task.description,task.current_date,task.product_id,task.country_id,task.discussion,task.quantity,task.number_of_batches,task.pharmacopoeia,task.working_quantity,task.specify_impurity_required,task.impurities_quantity,task.shipping_address,task.submitted_by,task.close_status,task.drupal_task_id,task.priority,task.rdd,task.front_due_date,2,task.share_with_agent,task.ccp_posted_by,task.language,0])
				.then(function (data) { 
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}
			

		}); 

	},
	add_task_translate:async (task)=>{
		return new Promise(function(resolve, reject) {
			db.one(
				`INSERT INTO tbl_tasks_translate(task_id,
					content,
					pharmacopoeia,
					polymorphic_form,
					stability_data_type,
					audit_visit_site_name,
					quantity,
					batch_number,
					nature_of_issue,
					working_quantity,
					impurities_quantity,
					specify_impurity_required,
					shipping_address,
					number_of_batches,
					payment_orders,
					payment_status,
					payment_pending,
					change_category,
					notification_action,
					notification_type,
					dmf_number,
					notification_number,
					apos_document_type,
					cancel_comment,
					gmp_clearance_id,
					tga_email_id,
					applicant_name,
					doc_required,
					add_date,
					update_date,
					service_request_type) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31) RETURNING translate_id`,[task.task_id,
						task.content,
						task.pharmacopoeia,
						task.polymorphic_form,
						task.stability_data_type,
						task.audit_visit_site_name,
						task.quantity,
						task.batch_number,
						task.nature_of_issue,
						task.working_quantity,
						task.impurities_quantity,
						task.specify_impurity_required,
						task.shipping_address,
						task.number_of_batches,
						task.payment_orders,
						task.payment_status,
						task.payment_pending,
						task.change_category,
						task.notification_action,
						task.notification_type,
						task.dmf_number,
						task.notification_number,
						task.apos_document_type,
						task.cancel_comment,
						task.gmp_clearance_id,
						task.tga_email_id,
						task.applicant_name,
						task.doc_required,
						task.add_date,
						task.update_date,
						task.service_request_type])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},
	get_all_pending_ratings: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.parent_id = 0
			AND tbl_tasks.rating = 0
			AND tbl_tasks.rating_skipped = 0
			AND tbl_tasks.close_status = 1
			AND tbl_tasks.customer_id = ($1)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_explicit_pending_ratings: async (cust_id,task_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.parent_id    = 0
			AND tbl_tasks.rating         = 0
			AND tbl_tasks.rating_skipped = 0
			AND tbl_tasks.close_status   = 1
			AND tbl_tasks.customer_id    = ($1)
			AND tbl_tasks.task_id        = ($2)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id,task_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_all_pending_ratings_counts: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
				COUNT(tbl_tasks.task_id) AS count
				
				FROM tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				WHERE tbl_tasks.parent_id = 0
				AND tbl_tasks.rating = 0
				AND tbl_tasks.close_status = 1
				AND tbl_tasks.customer_id = ($1)`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_other_pending_ratings: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42) 
			AND tbl_tasks.parent_id = 0
			AND tbl_tasks.rating = 0
			AND tbl_tasks.close_status = 1
			AND tbl_tasks.customer_id = ($1)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_other_pending_ratings_counts: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
				COUNT(tbl_tasks.task_id) AS count
				
				FROM tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				WHERE tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42) 
				AND tbl_tasks.parent_id = 0
				AND tbl_tasks.rating = 0
				AND tbl_tasks.close_status = 1
				AND tbl_tasks.customer_id = ($1)`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_complaints_pending_ratings: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.request_type in (7,34) 
			AND tbl_tasks.parent_id = 0
			AND tbl_tasks.rating = 0
			AND tbl_tasks.close_status = 1
			AND tbl_tasks.customer_id = ($1)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_complaints_pending_ratings_counts: async (cust_id)=>{
		//console.log('=====================',cust_id);
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
				COUNT(tbl_tasks.task_id) AS count
				
				FROM tbl_tasks
				WHERE tbl_tasks.request_type in (7,34) 
				AND tbl_tasks.parent_id = 0
				AND tbl_tasks.rating = 0
				AND tbl_tasks.close_status = 1
				AND tbl_tasks.customer_id = ($1)`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
			  console.log(data);	
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_orders_pending_ratings: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.request_type in (23,41) 
			AND tbl_tasks.parent_id = 0
			AND tbl_tasks.rating = 0
			AND tbl_tasks.close_status = 1
			AND tbl_tasks.customer_id = ($1)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_orders_pending_ratings_counts: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
				COUNT(tbl_tasks.task_id) AS count
				
				FROM tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				WHERE tbl_tasks.request_type in (23,41) 
				AND tbl_tasks.parent_id = 0
				AND tbl_tasks.rating = 0
				AND tbl_tasks.close_status = 1
				AND tbl_tasks.customer_id = ($1)`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_forecast_pending_ratings: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
			tbl_tasks.task_id,
			tbl_tasks.task_ref,
			tbl_tasks.language AS task_language,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_request_type.req_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_request_type.req_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_request_type.req_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_request_type.req_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_request_type.req_name
			END AS req_name,

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_product.product_name_zh
				 WHEN tbl_tasks.language = 'pt' THEN tbl_master_product.product_name_pt
				 WHEN tbl_tasks.language = 'es' THEN tbl_master_product.product_name_es
				 WHEN tbl_tasks.language = 'ja' THEN tbl_master_product.product_name_ja
				 WHEN tbl_tasks.language = 'en' THEN tbl_master_product.product_name
			END AS product_name

			
			FROM tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			WHERE tbl_tasks.request_type = 24
			AND tbl_tasks.parent_id = 0
			AND tbl_tasks.rating = 0
			AND tbl_tasks.close_status = 1
			AND tbl_tasks.customer_id = ($1)
			ORDER BY tbl_tasks.closed_date desc`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_forecast_pending_ratings_counts: async (cust_id)=>{
		return new Promise( async function(resolve, reject) {
			
			var sql =`SELECT 
				COUNT(tbl_tasks.task_id) AS count
				
				FROM tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				WHERE tbl_tasks.request_type = 24
				AND tbl_tasks.parent_id = 0
				AND tbl_tasks.rating = 0
				AND tbl_tasks.close_status = 1
				AND tbl_tasks.customer_id = ($1)`;
			
		    db.any(sql,[cust_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_sales_force_data: async (task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = `
				SELECT 
					T.*,
					C.sales_customer_id,
					R.req_name,
					CONCAT(C.first_name,' ',C.last_name) AS customer_full_name,
					C.cqt_customer_id,
					P.cqt_product_id,
					COMP.sales_company_id
				FROM 
					tbl_tasks AS T
				LEFT JOIN 
					tbl_master_product AS P
					ON T.product_id = P.product_id	
				LEFT JOIN 
					tbl_master_request_type AS R
					ON T.request_type = R.type_id		
				LEFT JOIN 
					tbl_customer AS C
					ON T.customer_id = C.customer_id
				LEFT JOIN 
					tbl_master_company AS COMP
					ON C.company_id = COMP.company_id
				WHERE 
					T.task_id = ($1)`;

			db.any(sql,[task_id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	get_sales_force_countries: async(task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = "SELECT ARRAY(SELECT country_name FROM tbl_master_country WHERE country_id IN (SELECT country_id FROM tbl_task_countries WHERE task_id = ($1))) AS task_countries";

			db.any(sql,[task_id]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	update_task_details: async (sales_task_id,task_id)=>{
		return new Promise(function(resolve, reject) {
			
			db.result("Update tbl_tasks set sales_task_id=($1) where task_id=($2)",[sales_task_id,task_id],r => r.rowCount)
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	request_exists: async (request_name)=>{
		return new Promise(function(resolve, reject) {
			db.any('select type_id from tbl_master_request_type where LOWER(req_name)=LOWER($1) and status=1',[request_name])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	add_sub_task: async (task)=>{
		return new Promise(function(resolve, reject) {

			db.one('INSERT INTO tbl_tasks(title,customer_id,content,parent_id,status,priority,close_status,date_added,due_date,owner,discussion) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING task_id',[task.title,task.customer_id,task.content,task.parent_id,1,2,0,task.current_date,task.due_date,task.owner,task.discussion])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	add_task_file: async (file)=>{
		return new Promise(function(resolve, reject) {

			db.one('INSERT INTO tbl_task_uploads(task_id,new_file_name,actual_file_name,date_added) VALUES($1,$2,$3,$4) RETURNING upload_id',[file.task_id,file.new_file_name,file.actual_file_name,file.date_added])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {console.log(err);
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_task_owner: async (emp_id,task_id)=>{
		return new Promise(function(resolve, reject) {
			let task_ref ='TSK'+task_id+'#';

			  db.result('UPDATE tbl_tasks set owner=($1) where task_id=($2)',[emp_id,task_id],r => r.rowCount)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	update_ref_no: async (ref_no,task_id)=>{
		return new Promise(function(resolve, reject) {
			//let task_ref ='TSK'+task_id+'#';

			  db.result('UPDATE tbl_tasks set task_ref=($1) where task_id=($2)',[ref_no,task_id],r => r.rowCount)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_allocated_manager: async (customer_id)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any('SELECT tbl_employee.manager_id,tbl_employee.employee_id,tbl_employee.email,tbl_employee.first_name,tbl_employee.last_name,tbl_employee.on_leave,tbl_employee.desig_id,tbl_designation.desig_name from tbl_customer_team left join tbl_employee on tbl_customer_team.employee_id=tbl_employee.employee_id LEFT join tbl_designation ON tbl_employee.desig_id = tbl_designation.desig_id where tbl_customer_team.customer_id=($1) and tbl_employee.desig_id = 1 AND tbl_customer_team.status = 1 limit 1',[customer_id])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_employee_details: async (employee_id)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any('SELECT tbl_employee.email,tbl_employee.first_name,tbl_employee.last_name FROM tbl_employee WHERE employee_id = ($1) limit 1',[employee_id])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_employee_manager: async (employee_id)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any('SELECT tbl_employee.manager_id::INTEGER FROM tbl_employee WHERE employee_id = ($1) limit 1',[employee_id])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_allocated_spoc: async (customer_id,language)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any('SELECT tbl_employee.employee_id,tbl_employee.email,tbl_employee.first_name,tbl_employee.last_name,tbl_employee.on_leave,tbl_employee.desig_id,tbl_designation.desig_name from tbl_customer_team left join tbl_employee on tbl_customer_team.employee_id=tbl_employee.employee_id LEFT join tbl_designation ON tbl_employee.desig_id = tbl_designation.desig_id where tbl_customer_team.customer_id=($1) and tbl_employee.language_code = ($2) and tbl_employee.desig_id = 7 AND tbl_customer_team.status = 1 limit 1',[customer_id,language])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_allocated_employee: async (customer_id,desig_id)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any('SELECT tbl_employee.employee_id,tbl_employee.email,tbl_employee.first_name,tbl_employee.last_name,tbl_employee.on_leave,tbl_employee.desig_id,tbl_designation.desig_name from tbl_customer_team left join tbl_employee on tbl_customer_team.employee_id=tbl_employee.employee_id LEFT join tbl_designation ON tbl_employee.desig_id = tbl_designation.desig_id where tbl_customer_team.customer_id=($1) and tbl_employee.desig_id = ($2) AND tbl_customer_team.status = 1 limit 1',[customer_id,desig_id])
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_task_language: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			
			  db.one('SELECT language FROM tbl_tasks WHERE task_id=($1)',[task_id])
			    .then(function (data) {
			      resolve(data.language)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_rating_options: async (language)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any(`SELECT option_name_${language} AS name,rating,o_id FROM tbl_master_rating_options WHERE status = 1`)
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	get_rating_questions: async (language)=>{
		return new Promise(function(resolve, reject) {
			
			  db.any(`SELECT question_name_${language} AS name,rating,q_id FROM tbl_master_rating_questions WHERE status = 1`)
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	rate_task: async (task_arr) => {
		return new Promise(function(resolve, reject) {
			let sql = 'UPDATE tbl_tasks SET rating=($2),rating_skipped=($3),rated_on=($4),rating_options=($5),rating_comment=($6),rated_by=($7),rated_by_type=($8) WHERE task_id=($1)';

			db.result(sql,task_arr,r => r.rowCount)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		});
	},
	skip_task: async (task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = 'UPDATE tbl_tasks SET rating_skipped=($2) WHERE task_id=($1)';

			db.result(sql,[task_id,1],r => r.rowCount)
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});

		});
	},
	assign_task: async (tasks)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_task_assignment(task_id,assigned_to,assigned_by,status,due_date,assign_date,assigned,responded,closed,assignment_status,reopen_date,parent_assignment_id) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING assignment_id',[tasks.task_id,tasks.assigned_to,tasks.assigned_by,1,tasks.due_date,tasks.assign_date,1,0,0,1,tasks.reopen_date,tasks.parent_assignment_id])
		    .then(function (data) {
		    	resolve(data);

		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	discussion_exists: async (task_id)=>{
		return new Promise(function(resolve, reject) {

			db.any('SELECT discussion_id FROM tbl_task_discussion WHERE task_id = ($1) AND status = 1;',[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	discussion_exists_posting_comments: async (task_id)=>{
		return new Promise(function(resolve, reject) {

			db.any('SELECT discussion_id FROM tbl_task_discussion WHERE task_id = ($1);',[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	update_discussion: async (discussion_id,updt_arr)=>{
		return new Promise(function(resolve, reject) {
			  db.result('UPDATE tbl_task_discussion SET comment=($1),date_updated=($2),status=($4) WHERE discussion_id=($3)',[updt_arr.comment,updt_arr.date_updated,discussion_id,updt_arr.status],r => r.rowCount)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) { console.log(err);
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });

		}); 

	},
	insert_discussion: async (discussion)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_task_discussion (task_id,comment,date_added) VALUES($1,$2,$3) RETURNING discussion_id',[discussion.task_id,discussion.comment,discussion.date_added])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) { console.log(err);
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 
	},
	insert_discussion_comment: async (d_comment)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_task_discussion_comment (discussion_id,comment,date_added,added_by,added_by_type,language) VALUES($1,$2,$3,$4,$5,$6) RETURNING comment_id',[d_comment.discussion_id,d_comment.comment,d_comment.date_added,d_comment.added_by,d_comment.added_by_type,d_comment.language])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function(err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	select_task_discussion_files :async (task_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT DU.upload_id,DU.new_file_name,DU.actual_file_name,DU.comment_id,to_char(DU.date_added, 'dd Mon YYYY') as date_added  FROM tbl_task_discussion_uploads AS DU LEFT JOIN tbl_tasks AS T ON DU.task_id = T.task_id WHERE T.task_id=($1) order by DU.comment_id desc`,[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_discussion_files_send_mail :async (task_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT DU.upload_id,DU.actual_file_name,false AS chk  FROM tbl_task_discussion_uploads AS DU LEFT JOIN tbl_tasks AS T ON DU.task_id = T.task_id WHERE T.task_id=($1) order by DU.comment_id desc`,[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_discussion_files_send_mail_single :async (upload_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT DU.new_file_name FROM tbl_task_discussion_uploads AS DU LEFT JOIN tbl_tasks AS T ON DU.task_id = T.task_id WHERE DU.upload_id=($1) order by DU.comment_id desc`,[upload_id])
		    .then(function (data = []) {
				console.log(data);
		    	resolve(data[0].new_file_name);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_comment_discussion:async (task_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT distinct comment_id FROM tbl_task_discussion_uploads WHERE tbl_task_discussion_uploads.task_id=($1) order by comment_id desc',[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_comment_discussion_files:async (comment_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT upload_id,new_file_name,actual_file_name,to_char(tbl_task_discussion_uploads.date_added, 'dd Mon YYYY') as date_added,tbl_task_discussion_comment.added_by_type,tbl_customer.first_name as customer_first_name,tbl_customer.last_name as customer_last_name,tbl_employee.first_name as employee_first_name,tbl_employee.last_name as employee_last_name,tbl_agents.first_name as agents_first_name,tbl_agents.last_name as agents_last_name FROM tbl_task_discussion_uploads LEFT JOIN tbl_task_discussion_comment ON tbl_task_discussion_uploads.comment_id = tbl_task_discussion_comment.comment_id LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_task_discussion_comment.added_by
			LEFT JOIN tbl_employee ON tbl_employee.employee_id = tbl_task_discussion_comment.added_by
			LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_task_discussion_comment.added_by 
			WHERE tbl_task_discussion_uploads.comment_id=($1)`,[comment_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	insert_discussion_file: async (file)=>{
		return new Promise(function(resolve, reject) {
			console.log("table called")
			db.one('INSERT INTO tbl_task_discussion_uploads (comment_id,new_file_name,actual_file_name,date_added,task_id) VALUES($1,$2,$3,$4,$5) RETURNING upload_id',[file.comment_id,file.new_file_name,file.actual_file_name,file.date_added,file.task_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	add_forecast: async (forecast_obj)=>{
		return new Promise(function(resolve, reject) {
  
			db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,status,content,date_added,discussion,submitted_by,close_status,drupal_task_id,priority,rdd,front_due_date,request_category,share_with_agent,language) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17) RETURNING task_id',[forecast_obj.customer_id,forecast_obj.request_type_id,forecast_obj.due_date,forecast_obj.parent_id,1,forecast_obj.description,forecast_obj.current_date,forecast_obj.discussion,forecast_obj.submitted_by,forecast_obj.close_status,forecast_obj.drupal_task_id,forecast_obj.priority,forecast_obj.rdd,forecast_obj.front_due_date,forecast_obj.request_category_type,forecast_obj.share_with_agent,forecast_obj.language])
		    .then(function (data) { 
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	add_payment: async (payment)=>{
		return new Promise(function(resolve, reject) {
  			
			db.one('INSERT INTO tbl_tasks(customer_id,request_type,content,parent_id,status,priority,close_status,date_added,due_date,drupal_task_id,request_category,days_remaining,payment_orders,payment_status,payment_pending,task_ref) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING task_id',[payment.customer_id,payment.request_id,payment.content,0,1,2,0,payment.current_date,payment.due_date,payment.task_id,payment.request_category_type,payment.days_remaining,payment.payment_orders,payment.payment_status,payment.payment_pending,payment.ref_no])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	add_notification: async (notification)=>{
		return new Promise(function(resolve, reject) {
      
			db.one('INSERT INTO tbl_tasks(customer_id,product_id,request_type,content,parent_id,status,priority,close_status,date_added,due_date,drupal_task_id,request_category,change_category,notification_action,notification_type,target_for_approval,target_for_implementation,task_ref,submitted_by) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19) RETURNING task_id',[notification.customer_id,notification.product_id,notification.request_id,notification.content,0,1,2,0,notification.current_date,notification.due_date,notification.task_id,notification.request_category_type,notification.change_category,notification.notification_action,notification.notification_type,notification.target_for_approval,notification.target_for_implementation,notification.ref_no,notification.submitted_by])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	sub_tasks_of_task: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT task_id FROM tbl_tasks WHERE parent_id=($1) ",[task_id])
		    .then(function (data){
		      resolve(data);
		    })
		    .catch(function (err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 

	},
	get_current_task_owner: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT assigned_to::INTEGER FROM tbl_task_assignment WHERE task_id=($1) AND assigned = 1 AND assignment_status = 1",[task_id])
		    .then(function (data){
		      resolve(data);
		    })
		    .catch(function (err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	get_auto_assigned_task_owner: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
			`SELECT 
				tbl_task_assignment.assigned_to::INTEGER,
				tbl_employee.desig_id
			FROM tbl_task_assignment 
			LEFT JOIN tbl_employee 
			ON tbl_task_assignment.assigned_to = tbl_employee.employee_id
			WHERE tbl_task_assignment.task_id = ($1) 
			AND tbl_task_assignment.assignment_status = 1
			ORDER BY tbl_task_assignment.assignment_id ASC 
			LIMIT 1 OFFSET 1`;
			db.any(sql,[task_id])
		    .then(function (data){
		      resolve(data);
		    })
		    .catch(function (err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	insert_product: async (product)=>{	// SATYAJIT
		return new Promise(function(resolve, reject) {			
		    db.one('INSERT INTO tbl_master_product(product_id,product_name,cqt_product_id) VALUES($1,$2,$3) RETURNING product_id',[product.product_id,product.product_name,product.cqt_product_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	update_product: async (product)=>{	// SATYAJIT
		return new Promise(function(resolve, reject) {			
		    db.result('UPDATE tbl_master_product SET product_name=($1) WHERE product_id=($2) AND cqt_product_id=($3)',[product.product_name,product.product_id,product.cqt_product_id],r => r.rowCount)
			.then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		});
	},
	tasks_details_assigned: async (task_id,assign_id)=>{
		return new Promise(function(resolve, reject) {

			db.any(`SELECT tbl_tasks.dmf_number,tbl_tasks.notification_number,TO_CHAR(tbl_tasks.rdfrc,'dd/mm/yyyy') AS rdfrc,tbl_tasks.back_sla_processed,tbl_tasks.front_sla_processed,tbl_tasks.front_due_date,tbl_tasks.total_sla_processed,tbl_tasks.working_quantity,tbl_tasks.sla_status,tbl_tasks.impurities_quantity,tbl_tasks.number_of_batches,tbl_tasks.service_request_type,tbl_tasks.specify_impurity_required,tbl_tasks.shipping_address,tbl_tasks.stability_data_type,tbl_task_assignment.assignment_id,tbl_task_assignment.assigned_by::INTEGER,tbl_task_assignment.assigned_to::INTEGER,tbl_task_assignment.responded,tbl_tasks.close_status,tbl_tasks.product_id,tbl_tasks.country_id,tbl_tasks.parent_id::INTEGER,tbl_customer.vip_customer,tbl_customer.email,tbl_tasks.audit_visit_site_name,tbl_tasks.request_audit_visit_date::varchar,tbl_tasks.task_id,tbl_tasks.request_type,tbl_tasks.task_ref,tbl_tasks.owner::INTEGER,tbl_tasks.request_category,tbl_tasks.pharmacopoeia,tbl_tasks.polymorphic_form,tbl_tasks.drupal_task_id,tbl_tasks.title,tbl_tasks.content, tbl_tasks.date_added,tbl_tasks.due_date,tbl_task_assignment.due_date AS assign_due_date,tbl_tasks.priority,tbl_tasks.status,tbl_customer.first_name,tbl_customer.last_name,tbl_tasks.customer_id, tbl_master_request_type.req_name, tbl_master_country.country_name,tbl_master_product.product_name,tbl_tasks.quantity,tbl_tasks.batch_number,tbl_tasks.nature_of_issue,tbl_tasks.payment_orders,tbl_tasks.payment_status,tbl_tasks.payment_pending,tbl_tasks.days_remaining,tbl_tasks.change_category,tbl_tasks.notification_action,tbl_tasks.notification_type,tbl_tasks.target_for_approval::varchar,tbl_tasks.target_for_implementation::varchar,tbl_tasks.rdd::varchar,tbl_tasks.cqt,tbl_tasks.cqt_file_id,tbl_employee.first_name AS emp_first_name,tbl_employee.last_name AS emp_last_name,tbl_designation.desig_name,tbl_tasks.submitted_by,tbl_agents.first_name AS agnt_first_name,tbl_agents.last_name AS agnt_last_name,tbl_task_assignment.breach_reason FROM tbl_tasks LEFT JOIN tbl_customer ON tbl_tasks.customer_id = tbl_customer.customer_id LEFT JOIN tbl_task_assignment ON tbl_tasks.task_id = tbl_task_assignment.task_id LEFT JOIN tbl_employee ON tbl_task_assignment.assigned_to=tbl_employee.employee_id LEFT JOIN tbl_designation ON tbl_employee.desig_id = tbl_designation.desig_id LEFT JOIN tbl_master_country ON tbl_tasks.country_id = tbl_master_country.country_id LEFT JOIN tbl_master_request_type ON tbl_tasks.request_type = tbl_master_request_type.type_id LEFT JOIN tbl_master_product ON tbl_tasks.product_id = tbl_master_product.product_id LEFT JOIN tbl_agents ON tbl_tasks.submitted_by = tbl_agents.agent_id WHERE tbl_tasks.task_id=($1) AND tbl_task_assignment.assignment_id = ($2) AND tbl_task_assignment.assignment_status = 1`,[task_id,assign_id])
		    .then(function (data) { 
		    	
		      resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
		    });

		}); 

	},
	tasks_details: async (task_id)=>{
		return new Promise(function(resolve, reject) {

			db.any(`SELECT 
			tbl_tasks.current_ownership::INTEGER,
			tbl_tasks.apos_document_type,
			tbl_tasks.dmf_number,
			tbl_tasks.language,
			tbl_tasks.notification_number,
			TO_CHAR(tbl_tasks.rdfrc, 'dd Mon YYYY') AS rdfrc,
			tbl_tasks.share_with_agent,
			tbl_agents.agent_id,
			tbl_tasks.back_sla_processed,
			tbl_tasks.front_sla_processed,
			tbl_tasks.front_due_date,
			tbl_tasks.total_sla_processed,
			tbl_tasks.working_quantity,
			tbl_tasks.sla_status,
			tbl_tasks.impurities_quantity,
			tbl_tasks.number_of_batches,
			tbl_tasks.service_request_type,
			tbl_tasks.specify_impurity_required,
			tbl_tasks.shipping_address,
			tbl_tasks.stability_data_type,
			tbl_tasks.close_status,
			tbl_tasks.product_id::INTEGER,
			tbl_tasks.country_id,
			tbl_tasks.parent_id::INTEGER,
			tbl_customer.vip_customer,
			tbl_customer.email,
			tbl_customer.company_id,
			tbl_customer.customer_id,
			tbl_tasks.audit_visit_site_name,
			to_char(tbl_tasks.request_audit_visit_date, 'dd Mon YYYY') as request_audit_visit_date,
			tbl_tasks.task_id,
			tbl_tasks.request_type,
			tbl_tasks.task_ref,
			tbl_tasks.owner::INTEGER,
			tbl_tasks.request_category,
			tbl_tasks.pharmacopoeia,
			tbl_tasks.polymorphic_form,
			tbl_tasks.drupal_task_id,
			tbl_tasks.title,
			tbl_tasks.content,
			to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,
			tbl_tasks.due_date,
			tbl_tasks.priority,
			tbl_tasks.status,
			tbl_customer.first_name,
			tbl_customer.last_name,
			tbl_tasks.customer_id,
			tbl_master_request_type.req_name,
			tbl_master_request_type.req_name_zh,
			tbl_master_request_type.req_name_es,
			tbl_master_request_type.req_name_ja,
			tbl_master_request_type.req_name_pt,
			tbl_master_country.country_name,
			tbl_master_product.product_name,
			tbl_master_product.product_name_zh,
			tbl_master_product.product_name_es,
			tbl_master_product.product_name_ja,
			tbl_master_product.product_name_pt,
			tbl_tasks.quantity,
			tbl_tasks.batch_number,
			tbl_tasks.nature_of_issue,
			tbl_tasks.payment_orders,
			tbl_tasks.payment_status,
			tbl_tasks.payment_pending,
			tbl_tasks.days_remaining,
			tbl_tasks.change_category,
			tbl_tasks.notification_action,
			tbl_tasks.notification_type,
			tbl_tasks.target_for_approval::varchar,
			tbl_tasks.target_for_implementation::varchar,
			to_char(tbl_tasks.rdd, 'dd Mon YYYY') as rdd,
			tbl_tasks.cqt,
			tbl_tasks.cqt_file_id,
			tbl_tasks.submitted_by,
			tbl_tasks.ccp_posted_by,
			tbl_tasks.gmp_clearance_id,
			tbl_tasks.tga_email_id,
			tbl_tasks.applicant_name,
			tbl_tasks.doc_required,
			tbl_tasks.duplicate_task,
			tbl_tasks.language,
			tbl_tasks_translate.content as content_translate,
			tbl_tasks_translate.pharmacopoeia as pharmacopoeia_translate,
			tbl_tasks_translate.polymorphic_form as polymorphic_form_translate,
			tbl_tasks_translate.audit_visit_site_name as audit_visit_site_name_translate,
			tbl_tasks_translate.batch_number as batch_number_translate,
			tbl_tasks_translate.nature_of_issue as nature_of_issue_translate,
			tbl_tasks_translate.working_quantity as working_quantity_translate,
			tbl_tasks_translate.impurities_quantity as impurities_quantity_translate,
			tbl_tasks_translate.number_of_batches as number_of_batches_translate,
			tbl_tasks_translate.notification_action as notification_action_translate,
			tbl_tasks_translate.notification_type as notification_type_translate,
			tbl_tasks_translate.dmf_number as dmf_number_translate,
			tbl_tasks_translate.notification_number as notification_number_translate,
			tbl_tasks_translate.cancel_comment as cancel_comment_translate,
			tbl_tasks_translate.gmp_clearance_id as gmp_clearance_id_translate,
			tbl_tasks_translate.tga_email_id as tga_email_id_translate,
			tbl_tasks_translate.applicant_name as applicant_name_translate,
			tbl_tasks_translate.doc_required as doc_required_translate,
			tbl_tasks_translate.shipping_address as shipping_address_translate,
			tbl_tasks_translate.specify_impurity_required as specify_impurity_required_translate,
			

			CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_task_action_required.action_name_zh
					WHEN tbl_tasks.language = 'pt' THEN tbl_master_task_action_required.action_name_pt
					WHEN tbl_tasks.language = 'es' THEN tbl_master_task_action_required.action_name_es
					WHEN tbl_tasks.language = 'ja' THEN tbl_master_task_action_required.action_name_ja
					WHEN tbl_tasks.language = 'en' THEN tbl_master_task_action_required.action_name_en
			END AS required_action,

			tbl_customer.first_name,
			tbl_customer.last_name,	
			tbl_employee.first_name AS posted_by_first_name,
			tbl_employee.last_name AS posted_by_last_name,
			to_char(b.expected_closure_date, 'dd Mon YYYY') as expected_closure_date,

			CASE WHEN tbl_tasks.close_status = 0 
			THEN 
				CASE WHEN tbl_tasks.language = 'zh' THEN tbl_master_task_status.status_name_zh
						WHEN tbl_tasks.language = 'pt' THEN tbl_master_task_status.status_name_pt
						WHEN tbl_tasks.language = 'es' THEN tbl_master_task_status.status_name_es
						WHEN tbl_tasks.language = 'ja' THEN tbl_master_task_status.status_name_ja
						WHEN tbl_tasks.language = 'en' THEN tbl_master_task_status.status_name_en
				END
			WHEN tbl_tasks.close_status = 1 and tbl_master_request_type.type_id != 23 and tbl_master_request_type.type_id != 41
			THEN CASE WHEN tbl_tasks.language = 'zh' THEN (SELECT ss.status_name_zh FROM tbl_master_task_status AS ss WHERE ss.status_id = 4)
					WHEN tbl_tasks.language = 'pt' THEN (SELECT ss.status_name_pt FROM tbl_master_task_status AS ss WHERE ss.status_id = 4)
					WHEN tbl_tasks.language = 'es' THEN (SELECT ss.status_name_es FROM tbl_master_task_status AS ss WHERE ss.status_id = 4)
					WHEN tbl_tasks.language = 'ja' THEN (SELECT ss.status_name_ja FROM tbl_master_task_status AS ss WHERE ss.status_id = 4)
					WHEN tbl_tasks.language = 'en' THEN (SELECT ss.status_name_en FROM tbl_master_task_status AS ss WHERE ss.status_id = 4)
				END
			ELSE CASE WHEN tbl_tasks.language = 'zh' THEN (SELECT ss.status_name_zh FROM tbl_master_task_status AS ss WHERE status_id = 9)
				WHEN tbl_tasks.language = 'pt' THEN (SELECT ss.status_name_pt FROM tbl_master_task_status AS ss WHERE status_id = 9)
				WHEN tbl_tasks.language = 'es' THEN (SELECT ss.status_name_es FROM tbl_master_task_status AS ss WHERE status_id = 9)
				WHEN tbl_tasks.language = 'ja' THEN (SELECT ss.status_name_ja FROM tbl_master_task_status AS ss WHERE status_id = 9)
				WHEN tbl_tasks.language = 'en' THEN (SELECT ss.status_name_en FROM tbl_master_task_status AS ss WHERE status_id = 9)
			END
			END AS status_details,

			CASE WHEN tbl_tasks.close_status = 0 THEN NULL
			WHEN tbl_tasks.close_status = 1 THEN to_char(tbl_tasks.closed_date, 'dd Mon YYYY')
			END AS closed_date
			FROM tbl_tasks
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
			LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 
			LEFT JOIN tbl_customer ON tbl_tasks.customer_id = tbl_customer.customer_id
			LEFT JOIN tbl_agents ON tbl_tasks.submitted_by = tbl_agents.agent_id 
			LEFT JOIN tbl_master_country ON tbl_tasks.country_id = tbl_master_country.country_id 
			LEFT JOIN tbl_master_request_type ON tbl_tasks.request_type = tbl_master_request_type.type_id 
			LEFT JOIN tbl_master_product ON tbl_tasks.product_id = tbl_master_product.product_id
			LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			LEFT JOIN tbl_tasks_translate ON tbl_tasks.task_id = tbl_tasks_translate.task_id  
			WHERE tbl_tasks.task_id=($1)`,[task_id])
		    .then(function (data) { 
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
		    });

		}); 

	},
	get_translated_country: async (task_id,language) => {
		return new Promise(function(resolve, reject) {
			
			let sql = `SELECT array_to_string((ARRAY(SELECT country_name_${language} FROM tbl_master_country WHERE country_id IN (SELECT country_id FROM tbl_task_countries WHERE task_id = ($1)))),', ') AS country_name`;

			db.any(sql,[task_id]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_translated_request_name: async (request_type,language) => {
		return new Promise(function(resolve, reject) {
			
			let sql = `SELECT req_name_${language} AS name FROM tbl_master_request_type WHERE type_id = ($1) `;

			db.any(sql,[request_type]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_translated_stability_data_type: async (stability_data_type,language) => {
		return new Promise(function(resolve, reject) {
			let sql = `SELECT name_${language} AS name FROM tbl_master_task_stability_data WHERE name = ($1) `;

			db.any(sql,[trim(stability_data_type)]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_translated_quantity: async (quantity,language) => {
		return new Promise(function(resolve, reject) {
			let sql = '';
			if(language == 'en'){
				sql = `SELECT unit_name AS name FROM tbl_master_unit WHERE unit_name = ($1) `;
			}else{
				sql = `SELECT unit_name_${language} AS name FROM tbl_master_unit WHERE unit_name = ($1) `;
			}
			

			db.any(sql,[trim(quantity)]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	check_language_spoc: async (language) => {
		return new Promise(function(resolve, reject) {
			let sql = `SELECT employee_id FROM tbl_employee WHERE language_code = ($1) AND desig_id = '7' AND (on_leave IS NULL OR on_leave = 0) ORDER BY employee_id DESC LIMIT 1`;

			db.any(sql,[language]).then(function (data) {
				console.log(data);
				if(data && data.length > 0 && data[0].employee_id > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_global_spoc: async (language) => {
		return new Promise(function(resolve, reject) {
			let sql = `
			SELECT 

				tbl_employee.employee_id,
				tbl_employee.email,
				tbl_employee.first_name,
				tbl_employee.last_name,
				tbl_employee.on_leave,
				tbl_employee.desig_id,
				tbl_designation.desig_name

			FROM tbl_employee 
			LEFT join tbl_designation 
				ON tbl_employee.desig_id = tbl_designation.desig_id
			WHERE tbl_employee.language_code = ($1) 
			AND tbl_employee.desig_id = '7' 
			AND (on_leave IS NULL OR on_leave = 0)
			ORDER BY tbl_employee.employee_id DESC LIMIT 1`;

			db.any(sql,[language]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_translated_apos_document_type: async (doc_name,language) => {
		return new Promise(function(resolve, reject) {

			let modified = doc_name.split(',');
			// doc_name.split(',').map(function(doc){
			// 	// Wrap each element of the dates array with quotes
			// 	return `'${doc}'`;
			// }).join(",");

			console.log(modified);

			let sql = `SELECT doc_name_${language} AS name FROM tbl_master_task_document_type WHERE doc_name IN ($1:csv) `;

			db.any(sql,[modified]).then(function (data) {
				if(data && data.length > 0){
					let data_type = data.map(function(doc){
						// Wrap each element of the dates array with quotes
						return `${doc.name}`;
					}).join(', ');
					resolve(data_type);
				}else{
					resolve('');
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	get_translated_product: async (product_id,language) => {
		return new Promise(function(resolve, reject) {
			let sql = `SELECT (CASE 
				WHEN 
				  (product_name_${language} IS NULL OR product_name_${language} = '') 
				THEN 
				  product_name
				ELSE
				  product_name_${language}
			  END) AS name FROM tbl_master_product WHERE product_id = ($1) `;

			db.any(sql,[product_id]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	insert_discussion_comment_translated: async (t_comment)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_task_translate_discussion_comment (discussion_id,comment,date_added,comment_id,language,status) VALUES($1,$2,$3,$4,$5,$6) RETURNING translate_comment_id',[t_comment.discussion_id,t_comment.comment,t_comment.date_added,t_comment.comment_id,t_comment.language,t_comment.status])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function(err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	get_total_sla : async (request_id) => {
		return new Promise(function(resolve, reject) {
			db.any("SELECT total_sla,frontend_sla FROM tbl_master_request_type WHERE type_id=($1) ",[request_id])
			.then(function (data) { 
				
				resolve(data[0]);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});
		});
	},
	update_task_assignment: async (task_id,emp_id,value)=>{
		return new Promise(function(resolve, reject) {

		db.result("UPDATE tbl_task_assignment SET assigned=($1) WHERE task_id=($2) AND assigned_to=($3) AND assigned='1'",[value,task_id,emp_id],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_task_status: async (task_id)=>{
		return new Promise(function(resolve, reject) {

		db.result("UPDATE tbl_task_assignment SET close_task_pink=($1) WHERE task_id=($2) AND assignment_status=($3) ",[1,task_id,1],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_exceed_status: async (task_id,emp_id)=>{
		return new Promise(function(resolve, reject) {

		db.result("UPDATE tbl_task_assignment SET status=1 WHERE status=2 AND task_id=($1) AND assigned_to=($2) AND assignment_status='1'",[task_id,emp_id],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	get_assignment_id :async (emp_id,task_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT assignment_id FROM tbl_task_assignment WHERE (assigned_to = ($1) OR assigned_by = ($1)) AND task_id=($2) AND assignment_status = 1 LIMIT 1',[emp_id,task_id])
		    .then(function (data) {
		    	if(data && data.length > 0 && data[0].assignment_id > 0){
					resolve(data[0].assignment_id);
				}else{
					resolve(0);
				}
		    	
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_files :async (task_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT upload_id,new_file_name,actual_file_name FROM tbl_task_uploads WHERE task_id=($1)',[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_files_send_mail :async (task_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT upload_id,actual_file_name,false AS chk FROM tbl_task_uploads WHERE task_id=($1)',[task_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_files_send_mail_single :async (upload_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT new_file_name FROM tbl_task_uploads WHERE upload_id=($1)',[upload_id])
		    .then(function (data = []) {
		    	
		    	resolve(data[0].new_file_name);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_task_files_upload :async (upload_id) => {

		return new Promise(function(resolve, reject) {

			db.any('SELECT new_file_name,actual_file_name FROM tbl_task_uploads WHERE upload_id=($1)',[upload_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_discussion_comment_specific_files_upload :async (upload_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT new_file_name,actual_file_name FROM tbl_task_discussion_uploads WHERE upload_id=($1)`,[upload_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_discussion_comment_specific_files :async (comment_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT upload_id, new_file_name,actual_file_name,to_char(date_added, 'dd Mon YYYY') as date_added FROM tbl_task_discussion_uploads WHERE comment_id=($1)`,[comment_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_discussion_comment_log :async (discussion_id) => {
		return new Promise(function(resolve, reject) {
				db.any(`
				SELECT 
					tbl_task_discussion_comment.comment_id,
					tbl_task_discussion_comment.comment,
					to_char(tbl_task_discussion_comment.date_added, 'Day, Mon dd, YYYY - HH24:MI') as date_added,
					tbl_task_discussion_comment.added_by_type,
					tbl_task_translate_discussion_comment.comment AS translated_comment,
					tbl_task_translate_discussion_comment.translate_comment_id,
					tbl_customer.first_name as customer_first_name,
					tbl_customer.last_name as customer_last_name,
					tbl_employee.first_name as employee_first_name,
					tbl_employee.last_name as employee_last_name,
					tbl_agents.first_name as agents_first_name,
					tbl_agents.last_name as agents_last_name,
					tbl_employee.profile_pic as employee_profile_pic,
					tbl_task_discussion_comment.language, 
					tbl_task_translate_discussion_comment.language AS translated_language
				FROM tbl_task_discussion_comment 
				LEFT JOIN tbl_task_translate_discussion_comment 
					ON tbl_task_discussion_comment.comment_id = tbl_task_translate_discussion_comment.comment_id 
				LEFT JOIN tbl_customer 
					ON tbl_task_discussion_comment.added_by = tbl_customer.customer_id 
				LEFT JOIN tbl_employee 
					ON tbl_task_discussion_comment.added_by = tbl_employee.employee_id 
				LEFT JOIN tbl_agents 
					ON tbl_task_discussion_comment.added_by = tbl_agents.agent_id 
				WHERE tbl_task_discussion_comment.discussion_id=($1) 
				AND tbl_task_discussion_comment.status = 1
				ORDER BY comment_id DESC`,[discussion_id])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},
	select_comment_specific_files :async (comment_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT tcu_id, new_file_name,actual_file_name,to_char(date_added, 'dd Mon YYYY') as date_added FROM tbl_task_comment_uploads WHERE comment_id=($1)`,[comment_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	select_comment_log :async (task_id) => {
		return new Promise(function(resolve, reject) {
				db.any('SELECT tbl_task_comments.comment_id,tbl_task_comments.comment,tbl_task_comments.post_date,tbl_customer.first_name,tbl_customer.last_name FROM tbl_task_comments LEFT JOIN tbl_customer ON tbl_task_comments.posted_by = tbl_customer.customer_id WHERE tbl_task_comments.task_id=($1) ORDER BY comment_id DESC',[task_id])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},
	insert_comment_log: async (comment_log)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_task_comments (posted_by,task_id,comment,post_date,assignment_id) VALUES ($1,$2,$3,$4,$5) RETURNING comment_id',[comment_log.posted_by,comment_log.task_id,comment_log.comment,comment_log.post_date,comment_log.assignment_id])
		    .then(function (data) {
		    	resolve(data.comment_id);
		    })
		    .catch(function (err) { 
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	update_current_owner: async(task_id,emp_id) => {
		return new Promise(function(resolve, reject) {
			db.result('UPDATE tbl_tasks SET current_ownership = ($2) WHERE task_id = ($1) ',[task_id,emp_id],r => r.rowCount)
			.then(function (sdata) {
				resolve(sdata)
			})
			.catch(function (terr) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	task_highlight: async(insrt_arr) => {
		return new Promise(function(resolve, reject) {
		    db.one('INSERT INTO tbl_customer_task_highligher (task_id,customer_id,status,identifier) VALUES ($1,$2,$3,$4) RETURNING id',insrt_arr)
		    .then(function (data) {
		    	resolve(data.comment_id);
		    })
		    .catch(function (err) { 
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	task_exists: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT task_id FROM tbl_tasks WHERE parent_id=0 and task_id = ($1)",[task_id])
		    .then(function (data){
		      resolve(data);
		    })
		    .catch(function (err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
			});
	
		});
	},
	update_highlight:async (task_id,customer_id,identifier,status) => {
		return new Promise(function(resolve, reject) {

			db.result("UPDATE tbl_customer_task_highligher SET status=($1) WHERE task_id=($2) AND customer_id=($3) AND identifier=($4) ",[status,task_id,customer_id,identifier],r => r.rowCount)
				.then(function (data) {
				  resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
	
			});
	},
	activate_highlighter:async (task_id) => {
		return new Promise(function(resolve, reject) {

			db.result("UPDATE tbl_customer_task_highligher SET status=($1) WHERE task_id=($2)",[1,task_id],r => r.rowCount)
				.then(function (data) {
				  resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
	
			});
	},
	notify:async(insrt_arr) => {

		return new Promise(function(resolve, reject) {
		    db.one(`INSERT INTO tbl_customer_notification (
				n_type,	 
				task_id,
				customer_id,
				add_date,
				read_status,
				customer_type) VALUES ($1,$2,$3,$4,$5,$6) RETURNING n_id`,insrt_arr)
		    .then(function (data) {
		    	resolve(data.n_id);
		    })
		    .catch(function (err) { 
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});

	},
	notify_employee:async(insrt_arr) => {

		return new Promise(function(resolve, reject) {
		    db.one(`INSERT INTO tbl_employee_notifications (
				n_type,	 
				task_id,
				employee_id,
				add_date,
				read_status,
				notification_text,
				notification_id,
				notification_subject) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING n_id`,insrt_arr)
		    .then(function (data) {
		    	resolve(data.n_id);
		    })
		    .catch(function (err) { 
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});

	},	
	task_cc_customer:async(task_id) => {
		return new Promise(function(resolve, reject) {
			db.any('SELECT customer_id FROM tbl_cc_customers WHERE task_id=($1) AND status=($2)',[task_id,1])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	check_highlight_entry:async(task_id,customer_id,identifier) => {
		return new Promise(function(resolve, reject) {
			//console.log(customer_id+"  "+identifier+" "+task_id);
			db.any('SELECT id FROM tbl_customer_task_highligher WHERE task_id=($1) AND customer_id=($2) AND identifier=($3)',[task_id,customer_id,identifier])
			.then(function (data) {
				console.log("data",data);
				if(data.length > 0 && data[0].id > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	tasks_details_countries: async(task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = "SELECT array_to_string((ARRAY(SELECT country_name FROM tbl_master_country WHERE country_id IN (SELECT country_id FROM tbl_task_countries WHERE task_id = ($1)))),', ') AS country_name";

			db.any(sql,[task_id]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	map_countries: async (task_id,country_id)=>{
		return new Promise(function(resolve, reject) {
			//console.log(customer);
			db.one('INSERT INTO tbl_task_countries (task_id,country_id) VALUES($1,$2) RETURNING id',[task_id,country_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	update_shared_with_agent:async (task_id,value) => {
		return new Promise(function(resolve, reject) {

			db.result("UPDATE tbl_tasks SET share_with_agent=($2) WHERE task_id=($1)",[task_id,value],r => r.rowCount)
				.then(function (data) {
				  resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
	
			});
	},
	master_activity_log: async(id) => { // SATYAJIT
		return new Promise(function(resolve, reject) {
			db.any('SELECT * FROM tbl_master_activity_log WHERE id=($1)',[id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	get_allocated_leave_employee: async (employee_id,customer_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = `
			SELECT 
				tbl_employee.employee_id,
				tbl_employee.email,
				tbl_employee.first_name,
				tbl_employee.last_name,
				tbl_employee.on_leave,
				tbl_employee.desig_id,
				tbl_designation.desig_name 
			FROM tbl_employee 
			LEFT JOIN tbl_leaves_management 
				ON tbl_employee.employee_id = tbl_leaves_management.new_emp_id 
			LEFT join tbl_designation 
				ON tbl_employee.desig_id = tbl_designation.desig_id	
			WHERE tbl_leaves_management.existing_emp_id = ($1) 
			AND tbl_leaves_management.customer_id = ($2)
			AND tbl_leaves_management.status = 1 
			AND tbl_employee.status = 1`;
			
			db.any(sql,[employee_id,customer_id])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});

		}); 

	},
	log_activity: async (id, task_id, params)=> { // SATYAJIT
		return new Promise(async function(resolve, reject) {
			await module.exports.master_activity_log(id)
			.then((data) => {
					if(data.length > 0){

							let resp = data[0];

							var today              = common.currentDateTime();
							var log_content        = resp.log_content;
							var task_ref           = params.task_ref;
							var request_type       = params.request_type;
							var customer_name      = params.customer_name;
							var agent_name 		     = params.agent_name;
							var employee_sender    = params.employee_sender;
							var employee_recipient = params.employee_recipient;
							var post_date        	 = common.formatDate( today, "d/m/yyyy, h:MM TT" );

							// replace text
							var description = log_content.replace(/\[task_ref\]/i, task_ref).replace(/\[request_type\]/i, request_type).replace(/\[customer_name\]/i, customer_name).replace(/\[post_date\]/i, post_date).replace(/\[agent_name\]/i, agent_name).replace(/\[employee_sender\]/i, employee_sender).replace(/\[employee_recipient\]/i, employee_recipient);

							// start query
							db.one('INSERT INTO tbl_activity_log (task_id,description,activity_id,add_date) VALUES($1,$2,$3,$4) RETURNING id',[task_id,description,id,today])
							.then(function (data) {
								resolve(data)
							})
							.catch(function (err) {
								var errorText = common.getErrorText(err);
								var error     = new Error(errorText);
								reject(error);
							});
					}else{
						var errorText = common.getErrorText("Error in getting response for activity");
						var error     = new Error(errorText);
						reject(error);
					}					
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			})
		});
	},
	master_activity_notification_log: async(id) => { // SATYAJIT
		return new Promise(function(resolve, reject) {
			db.any('SELECT * FROM tbl_master_notification_log WHERE id=($1)',[id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	get_notification_text: async (id, params)=> { // SATYAJIT
		return new Promise(async function(resolve, reject) {
			await module.exports.master_activity_notification_log(id)
			.then((data) => {
					if(data.length > 0){

							let resp          = data[0];
							let log_content   = resp.log_content;

							let customer_name = params.customer_name;
							let agent_name    = params.agent_name;
							let employee_name     = params.employee_name;
							let assigned_employee = params.assigned_employee;
							// replace text
							let description = 
							log_content
							.replace(/\[employee_name\]/i, employee_name)
							.replace(/\[assigned_employee\]/i, assigned_employee)
							.replace(/\[agent_name\]/i, agent_name)
							.replace(/\[customer_name\]/i, customer_name);

							resolve(description);
					}else{
						let errorText = common.getErrorText("Error in getting response for activity");
						let error     = new Error(errorText);
						reject(error);
					}					
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			})
		});
	},
	get_task_status:async (id_arr,lang) => {
		return new Promise(function(resolve, reject) {

			db.any(`
				SELECT 
					status_id,
					status_name_${lang} AS name
				FROM tbl_master_task_status
				WHERE status_id IN ($1:csv) ORDER BY order_status ASC`,[id_arr])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});	
	},
	get_task_action:async (lang) => {
		return new Promise(function(resolve, reject) {

			db.any(`
				SELECT 
					action_name_${lang} AS name
				FROM tbl_master_task_action_required
				WHERE true`)
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});	
	},
	get_task_comment_details:async (comment_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`
				SELECT 
					tbl_tasks.task_ref,
					tbl_master_request_type.req_name
				FROM tbl_task_discussion_comment 
				LEFT JOIN tbl_task_discussion 
				ON tbl_task_discussion_comment.discussion_id = tbl_task_discussion.discussion_id
				LEFT JOIN tbl_tasks
				ON tbl_tasks.task_id = tbl_task_discussion.task_id
				LEFT JOIN tbl_master_request_type
				ON tbl_master_request_type.type_id = tbl_tasks.request_type
				WHERE tbl_task_discussion_comment.comment_id=($1)`,[comment_id])
		    .then(function (data) {
		    	
		    	resolve(data);
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	get_translated_task:async (task_id) => {
	  return new Promise(function (resolve, reject) {
  
		let sql = `SELECT * FROM tbl_tasks_translate WHERE task_id = ($1)`;
  
		db.any(sql, [task_id]).then(function (data) {
			resolve(data);
		  })
		  .catch(function (err) {
			var errorText = common.getErrorText(err);
			var error = new Error(errorText);
			reject(error);
		  });
	  });
	},
	rating_option_exists:async (option_id) => {
		return new Promise(function (resolve, reject) {
	
		  	let sql = `SELECT o_id FROM tbl_master_rating_options WHERE o_id = ($1)`;
	
		  	db.any(sql, [option_id]).then(function (data) {
				if(data && data.length > 0 && data[0].o_id > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			  
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	  },
	  check_task_rating:async (task_id) => {
		return new Promise(function (resolve, reject) {
	
		  	let sql = `SELECT rating FROM tbl_tasks WHERE task_id = ($1)`;
	
		  	db.any(sql, [task_id]).then(function (data) {
				if(data && data.length > 0 && data[0].rating > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			  
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},
	insert_customer_escalation: async (escalation)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_customer_escalation (user_id,task_id,content,date_added,user_type) VALUES($1,$2,$3,$4,$5) RETURNING id',escalation)
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function(err){
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	insert_customer_escalation_file: async (file)=>{
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_customer_escalation_uploads (ce_id,new_file_name,actual_file_name,date_added) VALUES($1,$2,$3,$4) RETURNING upload_id',file)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		}); 
	},
	list_track_shipment: async (task_id)=>{
		return new Promise(function(resolve, reject) {
			
		    let sql = `SELECT * FROM tbl_track_shipment WHERE task_id = ($1)`;
	
		  	db.any(sql, [task_id]).then(function (data) {
				resolve(data[0]);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		}); 
	},
	select_ship_files: async (invoice_id)=>{
		return new Promise(function(resolve, reject) {
			
		    let sql = `SELECT * FROM tbl_track_shipment WHERE invoice_id = ($1)`;
	
		  	db.any(sql, [invoice_id]).then(function (data) {
				resolve(data[0]);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		}); 
	},

	get_task_details_close_comment: async (task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = `
				SELECT 
					T.*
				FROM 
					tbl_tasks AS T
				WHERE 
					T.task_id = ($1)`;

			db.any(sql,[task_id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},

}
