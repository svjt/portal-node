const db =require('../configuration/dbConn');
const common = require('../controllers/common');
const dateFormat   = require('dateformat');

module.exports = {
	get_task_details_close_comment: async (task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = `
				SELECT 
					T.*,
					T.content AS description
				FROM 
					tbl_tasks AS T
				WHERE 
					T.task_id = ($1)`;

			db.any(sql,[task_id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	check_outlook_post: async (ot_id) =>{
		return new Promise(function(resolve,reject){
			var sql = `SELECT * FROM tbl_outlook_post WHERE id = ($1)`;

			db.any(sql,[ot_id])
			.then(function (data) { 
				if(data && data.length > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	get_outlook_files: async (upload_id,ot_id)=>{
		return new Promise(function(resolve,reject){
			var sql = `SELECT * FROM tbl_outlook_uploads WHERE upload_id = ($1) AND post_id = ($2)`;

			db.any(sql,[upload_id,ot_id])
			.then(function (data) { 
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	get_excess_outlook_files: async (upload_id_arr,ot_id) =>{
		return new Promise(function(resolve,reject){
			var sql = `SELECT new_file_name FROM tbl_outlook_uploads WHERE upload_id NOT IN ($1:csv) AND post_id = ($2)`;

			db.any(sql,[upload_id_arr,ot_id])
			.then(function (data) { 
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	delete_outlook_files: async (ot_id) => {
		return new Promise(function(resolve,reject){
			var sql = `DELETE FROM tbl_outlook_uploads WHERE post_id = ($1)`;

			db.any(sql,[ot_id])
			.then(function (data) { 
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	delete_outlook: async (ot_id) => {
		return new Promise(function(resolve,reject){
			var sql = `DELETE FROM tbl_outlook_post WHERE id = ($1)`;

			db.any(sql,[ot_id])
			.then(function (data) { 
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
	},
	// 2 => Typical CoA
	// 3 => Spec and MOA
	add_task1: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				pharmacopoeia,
				polymorphic_form,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.pharmacopoeia,
				task.polymorphic_form,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 5  => Vendor Questionnaire
	// 6  => CDA/Sales Agreement
	// 8  => Recertification of CoA
	// 11 => Quality Agreement
	// 14 => Elemental Impurity (EI) Declaration
	// 15 => Residual Solvents Declaration
	// 16 => Storage and Transport Declaration
	// 20 => Request for LOC/LOE
	add_task2: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 4  => Method Related Queries
	// 10 => Customized Spec Request
	// 12 => Quality Equivalence Request
	// 17 => Request for DMF
	// 18 => Request for CEP Copy
	// 19 => Request for LOA
	// 21 => Request for Additional Declarations
	// 22 => General Request
	add_task3: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				pharmacopoeia,
				share_with_agent,
				dmf_number,
				rdfrc,
				notification_number,
				apos_document_type,
				gmp_clearance_id,
				tga_email_id,
				applicant_name,
				doc_required,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24,
				$25,
				$26,
				$27,
				$28,
				$29,
				$30,
				$31
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.pharmacopoeia,
				task.share_with_agent,
				task.dmf_number,
				task.date_of_response,
				task.notification_number,
				task.apos_document_type,
				task.gmp_clearance_id,
				task.tga_email_id,
				task.applicant_name,
				task.doc_required,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 1 => Samples/Working Standards/Impurities
	add_task4: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				pharmacopoeia,
				shipping_address,         
				number_of_batches,             
				quantity,                 
				specify_impurity_required,
				impurities_quantity,      
				working_quantity,         
				service_request_type,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24,
				$25,
				$26,
				$27,
				$28,
				$29,
				$30
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.pharmacopoeia,
				task.shipping_address,         
				task.number_of_batches,             
				task.quantity,                 
				task.specify_impurity_required,
				task.impurities_quantity,      
				task.working_quantity,         
				task.service_request_type,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language

			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 13 => Stability Data
	add_task5: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				stability_data_type,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.stability_data_type,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 9 => Audit/Visit Date
	add_task6: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				audit_visit_site_name,
				request_audit_visit_date,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.audit_visit_site_name,
				task.request_audit_visit_date,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 24 => Forecast
	add_task7 : async (task) => {

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	// 7 => Complaints
	add_task8: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				quantity,
				nature_of_issue,
				batch_number,
				share_with_agent,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24,
				$25
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.quantity,
				task.nature_of_issue,
				task.batch_number,
				task.share_with_agent,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	// 23 => New Order
	add_task9: async (task)=>{

		return new Promise(function(resolve, reject) {
			var sql = 
			`INSERT INTO 
			tbl_tasks(
				due_date,
				rdd,
				front_due_date,
				date_added,
				status,
				close_status,
				priority,
				parent_id,
				task_ref,
				drupal_task_id,
				request_category,
				total_sla_processed,
				front_sla_processed,
				back_sla_processed,
				ccp_posted_by,
				request_type,
				content,
				customer_id,
				product_id,
				quantity,
				share_with_agent,
				pharmacopoeia,
				ccp_posted_with,
				language
			) 
			VALUES(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6,
				$7,
				$8,
				$9,
				$10,
				$11,
				$12,
				$13,
				$14,
				$15,
				$16,
				$17,
				$18,
				$19,
				$20,
				$21,
				$22,
				$23,
				$24
			) RETURNING task_id`;

			db.one(sql,[
				task.due_date,
				task.rdd,
				task.front_due_date,
				task.date_added,
				task.status,
				task.close_status,
				task.priority,
				task.parent_id,
				task.task_ref,
				task.drupal_task_id,
				task.request_category,
				task.total_sla_processed,
				task.front_sla_processed,
				task.back_sla_processed,
				task.ccp_posted_by,
				task.request_type,
				task.content,
				task.customer_id,
				task.product_id,
				task.quantity,
				task.share_with_agent,
				task.pharmacopoeia,
				task.ccp_posted_with,
				task.language
			])
		    .then(function (data) { 
		      resolve(data.task_id)
		    })
		    .catch(function (err) {
		      	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	map_countries: async (task_id,country_id)=>{
		return new Promise(function(resolve, reject) {
			//console.log(customer);
			db.one('INSERT INTO tbl_task_countries (task_id,country_id) VALUES($1,$2) RETURNING id',[task_id,country_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	map_countries_translated: async (task_id,country_id)=>{
		return new Promise(function(resolve, reject) {
			console.log(task_id,country_id);
			db.one('INSERT INTO tbl_task_translate_countries (task_id,country_id) VALUES($1,$2) RETURNING id',[task_id,country_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	update_ref_no: async (ref_no,task_id)=>{
		return new Promise(function(resolve, reject) {
			//let task_ref ='TSK'+task_id+'#';
			  db.result('UPDATE tbl_tasks set task_ref=($1) where task_id=($2)',[ref_no,task_id],r => r.rowCount)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
		});
	},
	emailCodeTemplate: async (email_code,lang)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT * from tbl_master_emails 
				WHERE email_code=($1) 
				AND language=($2)
				AND status = 1`, [email_code, lang])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	emailSendLog: async (email_id,email_code,mailFrom,mailTo,fromType,toType,mailSubject,mailHtml,senddate,ccMail)=>{
		return new Promise(function(resolve, reject) {
		var sql = 
		`INSERT INTO 
		tbl_email_logs(
			email_id,email_code,from_email,to_email,from_type,to_type,mail_subject,mail_content,mail_sent_date,status,cc_email
		) 
		VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) 
		RETURNING email_log_id`;
		db.one(sql,[email_id,email_code,mailFrom,mailTo,fromType,toType,mailSubject,mailHtml,senddate,1,ccMail])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	task_countries: async(task_id) => {
		return new Promise(function(resolve, reject) {
			let sql = "SELECT country_id FROM tbl_task_countries WHERE task_id = ($1)";

			db.any(sql,[task_id]).then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	}
}
