const db =require('../configuration/dbConn');
const common = require('../controllers/common');

module.exports = {
	check_company: async (company_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT company_id 
			FROM tbl_master_company
			WHERE company_id=($1) `;

			db.any(sql,[company_id])
		    .then(function (data) {
			    if(data && data.length > 0){
					resolve(true);
			    }else{
					resolve(false);
			    }
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	check_agent_company: async (agent_id,company_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT ag_id 
			FROM tbl_agent_company
			WHERE agent_id = ($1) 
			AND company_id=($2)`;

			db.any(sql,[agent_id,company_id])
		    .then(function (data) {
			  if(data && data.length > 0){
				resolve(true);
			  }else{
				resolve(false);
			  }
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	check_task_details_agent: async (agent_id,cust_arr,task_id)=>{
		return new Promise(function(resolve, reject) {

			var sql =`
			 SELECT tbl_tasks.task_id
			 FROM tbl_tasks
			 WHERE 
			 	(
					 (tbl_tasks.customer_id IN ($1:csv) 
					 AND tbl_tasks.share_with_agent = 1) 
					 OR tbl_tasks.submitted_by = ($2)
				) 
			AND parent_id = 0
			AND task_id = ($3)`;

			 db.any(sql,[cust_arr,agent_id,task_id])
		    .then(function (data) {
				if(data && data.length > 0){
					resolve(true);
				}else{
					resolve(false);
				}
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	check_task_details: async (customer_id,cust_arr,task_id)=>{
		
		return new Promise(function(resolve, reject) {

			var sql =
			`SELECT tbl_tasks.task_id
			 FROM tbl_tasks
			 WHERE 
			 (
				tbl_tasks.customer_id IN ($1:csv) 
				OR tbl_tasks.task_id IN (
					 SELECT task_id 
					 FROM tbl_cc_customers 
					 WHERE customer_id = ($2)
					 AND status = '1'
				)
			 )
			 AND parent_id = 0
			 AND task_id = ($3)`;


			 db.any(sql,[cust_arr,customer_id,task_id])
		    .then(function (data) {
				if(data && data.length > 0){
					resolve(true);
				}else{
					resolve(false);
				}
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	check_preship_invoice: async (invoice) => {
		return new Promise(function (resolve, reject) {

			let sql = `SELECT tbl_task_invoice.invoice_number, tbl_task_invoice.approve_status FROM tbl_task_invoice LEFT JOIN tbl_sales_order_sap ON tbl_task_invoice.sales_order_no=tbl_sales_order_sap.sales_order LEFT JOIN tbl_tasks	ON tbl_task_invoice.sales_order_no = tbl_tasks.sales_order_no WHERE tbl_task_invoice.id=($1) AND tbl_tasks.sales_order_no IS NOT NULL AND tbl_tasks.sales_order_no!='' AND tbl_task_invoice.invoice_type='U' AND tbl_task_invoice.approve_status = 0`;			
			db.any(sql, [invoice]).then(function (data) {
				if(data && data.length > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	check_preship_coa: async (invoice) => {
		return new Promise(function (resolve, reject) {

			let sql = `SELECT approve_status FROM tbl_tasks_coa LEFT JOIN tbl_tasks_coa_sap ON tbl_tasks_coa_sap.batch_number = tbl_tasks_coa.batch_no AND tbl_tasks_coa.customer_id = tbl_tasks_coa_sap.customer_no LEFT JOIN tbl_sales_order_sap ON tbl_tasks_coa_sap.sales_order_number=tbl_sales_order_sap.sales_order WHERE tbl_tasks_coa.coa_id =($1) AND tbl_tasks_coa_sap.sales_order_number IS NOT NULL AND tbl_tasks_coa_sap.sales_order_number!='' AND tbl_tasks_coa.approve_status = 0`;			
			db.any(sql, [invoice]).then(function (data) {
				if(data && data.length > 0){
					resolve(true);
				}else{
					resolve(false);
				}
			})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
}
