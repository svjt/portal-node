const db =require('../configuration/dbConn');
const common = require('../controllers/common');
const dateFormat    = require('dateformat');

module.exports = {
	log_req_resp: async (req_resp)=>{
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_sf_req_resp_log (request,response,method,url,datetime,type) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id',req_resp)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	get_token: async (req_resp)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT token FROM tbl_sf_token WHERE '${common.currentDateTime()}' < expiry_date LIMIT 1`,[])
		    .then(function (data) {
				console.log(data);
				if(data && data.length > 0){
					resolve(data[0].token);
				}else{
					resolve('');
				}
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	insert_token: async (token)=>{
		return new Promise(function(resolve, reject) {
			db.one('INSERT INTO tbl_sf_token(token,expiry_date) VALUES($1,$2) RETURNING token',[token,dateFormat(common.nextDate(23,'hours'), "yyyy-mm-dd HH:MM:ss")])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	}
}
