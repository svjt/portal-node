const db = require("../configuration/dbConn");
const common = require("../controllers/common");

module.exports = {
  /**
   * @desc Check product id exists
   * @param
   * @return json
   */

  product_id_exists: async (product_id) => {
    return new Promise(function (resolve, reject) {
      db.any(
        "select product_id from tbl_master_product where product_id=($1) AND status = 1 AND new_order IN ('0','1')",
        [product_id]
      )
        .then(function (data) {
          if (data.length > 0)
            resolve({
              success: true,
            });
          else
            resolve({
              success: false,
            });
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List all active product category
   * @param
   * @return json
   */
  product_category: async (lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      if (lang == "en") {
        var sql =
          "select name, id, drupal_id, status from tbl_master_product_category WHERE status = 1";
      } else {
        var lang_code = "name_" + lang;
        var sql = `select ${lang_code} as name, id, drupal_id, status from tbl_master_product_category WHERE status = 1`;
      }
      db.any(sql)
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List all active product dosage
   * @param
   * @return json
   */
  product_dosage: async (lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      if (lang == "en") {
        var sql =
          "select name, id, drupal_id, status from tbl_master_product_dosage WHERE status = 1";
      } else {
        var lang_code = "name_" + lang;
        var sql = `select ${lang_code} as name, id, drupal_id, status from tbl_master_product_dosage WHERE status = 1`;
      }
      db.any(sql)
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List all active product stage
   * @param
   * @return json
   */
  product_stage: async (lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      if (lang == "en") {
        var sql =
          "select name, id, drupal_id, status from tbl_master_product_stage WHERE status = 1";
      } else {
        var lang_code = "name_" + lang;
        var sql = `select ${lang_code} as name, id, drupal_id, status from tbl_master_product_stage WHERE status = 1`;
      }
      db.any(sql)
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List all active product technology
   * @param
   * @return json
   */
  product_technology: async (lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      if (lang == "en") {
        var sql =
          "select name, id, drupal_id, status from tbl_master_product_technology WHERE status = 1";
      } else {
        var lang_code = "name_" + lang;
        var sql = `select ${lang_code} as name, id, drupal_id, status from tbl_master_product_technology WHERE status = 1`;
      }
      db.any(sql)
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List all active products
   * @param
   * @return json
   */
  get_products: async (lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      if (lang == "en") {
        var sql =
          "select product_name, product_id, cqt_product_id, status from tbl_master_product where status = 1 AND new_order IN ('0','1') ORDER BY product_name ASC";
      } else {
        var lang_code = "product_name_" + lang;
        var sql = `select (CASE 
					WHEN 
						(${lang_code} IS NULL) 
					THEN 
						product_name
					ELSE
						${lang_code}
				END) as product_name, product_id, cqt_product_id, status from tbl_master_product where status = 1 AND new_order IN ('0','1') ORDER BY product_name ASC`;
      }
      db.any(sql)
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
  /**
   * @desc List my products
   * @param
   * @return json
   */
  my_products: async (id, lang) => {
    return new Promise(function (resolve, reject) {
      lang = typeof lang !== "undefined" ? lang : "en";
      let sql = "";
      if (lang == "en") {
        sql = `select tbl_customer_products.product_id,tbl_master_product.product_name from tbl_customer_products LEFT JOIN tbl_master_product ON tbl_customer_products.product_id=tbl_master_product.product_id where tbl_customer_products.customer_id = ($1) and tbl_master_product.status = 1 and tbl_customer_products.status = 1 AND tbl_master_product.new_order IN ('0','1') ORDER BY tbl_master_product.product_name ASC`;
      } else {
        let lang_code = `tbl_master_product.product_name_${lang}`;
        sql = `select tbl_customer_products.product_id, (CASE 
					WHEN 
						(${lang_code} IS NULL) 
					THEN 
						tbl_master_product.product_name
					ELSE
						${lang_code}
				END) as product_name from tbl_customer_products LEFT JOIN tbl_master_product ON tbl_customer_products.product_id=tbl_master_product.product_id where tbl_customer_products.customer_id = ($1) and tbl_master_product.status = 1 and tbl_customer_products.status = 1 AND tbl_master_product.new_order IN ('0','1') ORDER BY tbl_master_product.product_name ASC`;
      }

      db.any(sql, [id])
        .then(function (data) {
          resolve(data);
        })
        .catch(function (err) {
          var errorText = common.getErrorText(err);
          var error = new Error(errorText);
          reject(error);
        });
    });
  },
};
