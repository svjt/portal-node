const db =require('../configuration/dbConn');
const common = require('../controllers/common');

module.exports = {
	departments: async ()=>{
		return new Promise(function(resolve, reject) {

			db.any("select dept_id,dept_name from tbl_department where status='1';")
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	designations: async ()=>{
		return new Promise(function(resolve, reject) {

			db.any("select desig_id,desig_name from tbl_designation where status ='1';")
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log(err);
		      //reject(new Error("test error outside"));
		      reject("Error in connection");
		    });

		}); 

	},
	request_type: async ()=>{
		return new Promise(function(resolve, reject) {

			db.any("SELECT type_id,req_name,total_sla,frontend_sla,backend_sla,status FROM tbl_master_request_type WHERE status ='1' ORDER BY type_id;")
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	country: async ()=>{
		return new Promise(function(resolve, reject) {

			    db.any('select  country_id, country_name from tbl_master_country')
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	},
	products: async ()=>{
		return new Promise(function(resolve, reject) {

			    db.any(`select  product_id, product_name from tbl_master_product WHERE status = 1 AND new_order IN ('0','1')`)
			    .then(function (data) {
			      resolve(data)
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	},
	get_request_type: async (request_type_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT total_sla,frontend_sla,backend_sla,req_name,status FROM tbl_master_request_type WHERE type_id =($1);",[request_type_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	update_request_type: async (request_type_id,total_sla,frontend_sla,backend_sla)=>{
		return new Promise(function(resolve, reject) {
			db.result("UPDATE tbl_master_request_type SET total_sla=($1), frontend_sla=($3), backend_sla=($4) WHERE type_id =($2);",[total_sla,request_type_id,frontend_sla,backend_sla],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 
	},
	validate_country: async (country_id)=>{
		return new Promise(function(resolve, reject) {

			    db.any('select  country_id from tbl_master_country WHERE country_id = ($1)',[country_id])
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	},
	validate_product: async (product_id)=>{
		return new Promise(function(resolve, reject) {

			    db.any(`select  product_id from tbl_master_product WHERE product_id = ($1) AND new_order IN ('0','1')`,[product_id])
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	},

	validate_material_id: async (material_id)=>{
		return new Promise(function(resolve, reject) {

			    db.any(`select material_code from tbl_master_sap_product WHERE material_code = ($1) AND status = 1`,[material_id])
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	},
	employee_company: async (emp_id)=>{
		return new Promise(function(resolve, reject) {
					var sql = 
						`SELECT 
							COMP.company_id,
							COMP.company_name
						FROM tbl_master_company AS COMP 
						LEFT JOIN tbl_customer AS CUST
							ON COMP.company_id = CUST.company_id
						LEFT JOIN tbl_customer_team AS CUST_TEAM
							ON CUST.customer_id = CUST_TEAM.customer_id
						WHERE CUST_TEAM.employee_id = ($1)
							AND CUST.status = 1 
							AND CUST_TEAM.status = 1
							GROUP BY COMP.company_id,COMP.company_name`;
			    db.any(sql,[emp_id])
			    .then(function (data) {
			      resolve(data);
			    })
			    .catch(function (err) {
			      var errorText = common.getErrorText(err);
						var error     = new Error(errorText);
						reject(error);
			    });

		}); 

	}
}
