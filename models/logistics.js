const db = require('../configuration/dbConn');
const errorLog = require('../controllers/common');

module.exports = {
	getValidToken: async (now)=>{
		return new Promise(function(resolve, reject) {
			db.any("select token from tbl_track_shipment_token where expiry > $1",[now])
		    .then(function (data) {		    	
		      	resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	getToken: async (now)=>{
		return new Promise(function(resolve, reject) {
			db.any("select token,refresh_token from tbl_track_shipment_token where id=1")
		    .then(function (data) {		    	
		      	resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},	
	updateToken: async (token, expires_on)=>{		
		return new Promise(function(resolve, reject) {
			db.any('UPDATE tbl_track_shipment_token SET token = $1, expiry = $2 WHERE id=1',[token,expires_on])
		    .then(function (data) {
		      	resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
	    		var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	getAllInvoice: async (logistic="")=>{
		return new Promise(function(resolve, reject) {
			let sql = '';
			if(logistic!=''){
				sql =`select tbl_task_invoice.invoice_number from tbl_tasks LEFT JOIN tbl_task_invoice on tbl_tasks.sales_order_no = tbl_task_invoice.sales_order_no where tbl_tasks.request_type=43 AND tbl_task_invoice.logistic_status = 0 AND tbl_task_invoice.invoice_number != '' AND tbl_tasks.sales_order_no !='' AND tbl_task_invoice.logistics_partner IS NOT NULL AND tbl_task_invoice.logistics_partner != '' AND (tbl_task_invoice.invoice_number like '9013%' OR tbl_task_invoice.invoice_number like '9021%') AND tbl_task_invoice.invoice_type = 'M' AND LOWER(tbl_task_invoice.logistics_partner) LIKE '%${logistic}%'`;
			}else{
				sql = "select tbl_task_invoice.invoice_number from tbl_tasks LEFT JOIN tbl_task_invoice on tbl_tasks.sales_order_no = tbl_task_invoice.sales_order_no where tbl_tasks.request_type=43 AND tbl_task_invoice.logistic_status = 0 AND tbl_task_invoice.invoice_number != '' AND tbl_tasks.sales_order_no !='' AND tbl_task_invoice.logistics_partner IS NOT NULL AND tbl_task_invoice.logistics_partner != '' AND (tbl_task_invoice.invoice_number like '9013%' OR tbl_task_invoice.invoice_number like '9021%') AND tbl_task_invoice.invoice_type = 'M'";
				//logistic = "1";
			}

			db.any(sql,[])
		    .then(function (data) {
		      	resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	// getInvoiceByTaskId: async (task)=>{
	// 	return new Promise(function(resolve, reject) {
	// 		db.any("select * from tbl_track_shipment where task_id = $1",[task])
	// 	    .then(function (data) {		    	
	// 			if (data && data.length > 0) {
	// 				resolve(data[0]);
	// 			} else {
	// 				resolve('');
	// 			}
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = errorLog.getErrorText(err);
	// 	    	var error     = new Error(errorText);
	//             reject(error);
	// 	    });
	// 	});
	// },
	// insertShippingTrack: async (task,shipping_data)=>{
	// 	return new Promise(function(resolve, reject) {
	// 		db.any("INSERT INTO tbl_track_shipment (task_id, invoice_id, shipment_date, flight_booked, hawb, shipment_departed, shipment_arrived_at_destination, delivery_at_customer_destination, tracking_info_updated, zoho_id, invoice_doc_link, packing_list_link, flight_booked_doc_link, hawb_doc_link, invoice_doc_location, invoice_doc_time, packing_list_location, packing_list_time, flight_booked_location, flight_booked_time, hawb_location,  hawb_time, shipment_pickup_location, shipment_departed_location, shipment_arrived_location, enter_invoice_no_shipment_pickup_comments, enter_awb_hawb_comments, enter_shipment_departed_comments, enter_shipment_arrived_at_destination_comment, enter_delivery_at_customer_destination_comment, approval_for_document_from_consignee_location, await_approval_for_haz_clearance_from_consignee, approval_for_document_from_consignee_time, customs_clearance_completion_location, customs_clearance_completion_time, fda_approval_time, fda_approval_awaited, approval_for_haz_clearance_from_consignee_location, await_approval_for_document_from_consignee, delivery_at_customer_destination_location, approval_for_haz_clearance_from_consignee_time, customs_clearance, fda_approval_location) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,$41,$42,$43) RETURNING id",
	// 		[task, shipping_data.Invoice_No, shipping_data.Shipment_Picked_from_DRL_by_Agency, shipping_data.Flight_Booked, shipping_data.HAWB, shipping_data.Shipment_Departed, shipping_data.Shipment_Arrived_at_Destination,  shipping_data.Delivery_at_Customer_Destination, shipping_data.Tracking_Info_Updated, shipping_data.ID, shipping_data.Invoice_Doc, shipping_data.Packing_List, shipping_data.Flight_Booked_AWB_PDF, shipping_data.HAWB_Doc, shipping_data.Invoice_Doc_Location, shipping_data.Invoice_Doc_Time,	shipping_data.Packing_list_location, shipping_data.Packing_List_Time, shipping_data.Flight_booked_Location, shipping_data.Flight_Booked_Time, shipping_data.HAWB_Location, shipping_data.HAWB_Time, shipping_data.Shipment_Pickup_Location, shipping_data.Shipment_Departed_Location, shipping_data.Shipment_Arrived_Location, shipping_data.Enter_Invoice_no_Shipment_pickup_comments, shipping_data.Enter_AWB_HAWB_Comments, shipping_data.Enter_Shipment_Departed_Comments, shipping_data.Enter_Shipment_Arrived_at_Destination_Comment,shipping_data.Enter_Delivery_at_Customer_Destination_Comment, shipping_data.Approval_for_document_from_consignee_location,shipping_data.Await_approval_for_Haz_clearance_from_consignee, shipping_data.Approval_for_document_from_consignee_time, shipping_data.Customs_clearance_completion_location, shipping_data.Customs_clearance_completion_time,shipping_data.FDA_Approval_Time, shipping_data.FDA_Approval_Awaited, shipping_data.Approval_for_Haz_clearance_from_consignee_location, shipping_data.Await_approval_for_document_from_consignee, shipping_data.Delivery_at_Customer_Destination_Location, shipping_data.Approval_for_Haz_clearance_from_consignee_time, shipping_data.Customs_clearance, shipping_data.FDA_Approval_Location])
	// 	    .then(function (data) {		    	
	// 			resolve(data);
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = errorLog.getErrorText(err);
	// 	    	var error     = new Error(errorText);
	//             reject(error);
	// 	    });
	// 	});
	// },
	// updateShippingTrack: async (task,shipping_data)=>{	//console.log(shipping_data.Invoice_Doc_Time);
	// 	return new Promise(function(resolve, reject) {
	// 		db.any("UPDATE tbl_track_shipment SET shipment_date = $2, flight_booked =$3, hawb = $4, shipment_departed = $5, shipment_arrived_at_destination = $6, delivery_at_customer_destination = $7, tracking_info_updated = $8, zoho_id = $9, invoice_doc_link = $10, packing_list_link = $11, flight_booked_doc_link = $12, hawb_doc_link = $13, invoice_doc_location = $14, invoice_doc_time = $15, packing_list_location = $16, packing_list_time = $17, flight_booked_location = $18, flight_booked_time = $19, hawb_location = $20,  hawb_time = $21, shipment_pickup_location = $22, shipment_departed_location = $23, shipment_arrived_location = $24, enter_invoice_no_shipment_pickup_comments = $25, enter_awb_hawb_comments = $26, enter_shipment_departed_comments = $27, enter_shipment_arrived_at_destination_comment = $28, enter_delivery_at_customer_destination_comment = $29, approval_for_document_from_consignee_location = $30, await_approval_for_haz_clearance_from_consignee = $31, approval_for_document_from_consignee_time = $32, customs_clearance_completion_location = $33, customs_clearance_completion_time = $34, fda_approval_time = $35, fda_approval_awaited = $36, approval_for_haz_clearance_from_consignee_location = $37, await_approval_for_document_from_consignee = $38, delivery_at_customer_destination_location = $39, approval_for_haz_clearance_from_consignee_time = $40, customs_clearance = $41, fda_approval_location = $42  WHERE task_id = $1",
	// 		[task, shipping_data.Shipment_Picked_from_DRL_by_Agency, shipping_data.Flight_Booked, shipping_data.HAWB, shipping_data.Shipment_Departed, shipping_data.Shipment_Arrived_at_Destination, shipping_data.Delivery_at_Customer_Destination, shipping_data.Tracking_Info_Updated, shipping_data.ID, shipping_data.Invoice_Doc, shipping_data.Packing_List, shipping_data.Flight_Booked_AWB_PDF, shipping_data.HAWB_Doc, shipping_data.Invoice_Doc_Location, shipping_data.Invoice_Doc_Time, shipping_data.Packing_list_location, shipping_data.Packing_List_Time, shipping_data.Flight_booked_Location, shipping_data.Flight_Booked_Time, shipping_data.HAWB_Location, shipping_data.HAWB_Time, shipping_data.Shipment_Pickup_Location, shipping_data.Shipment_Departed_Location, shipping_data.Shipment_Arrived_Location, shipping_data.Enter_Invoice_no_Shipment_pickup_comments, shipping_data.Enter_AWB_HAWB_Comments, shipping_data.Enter_Shipment_Departed_Comments, shipping_data.Enter_Shipment_Arrived_at_Destination_Comment,shipping_data.Enter_Delivery_at_Customer_Destination_Comment, shipping_data.Approval_for_document_from_consignee_location,shipping_data.Await_approval_for_Haz_clearance_from_consignee, shipping_data.Approval_for_document_from_consignee_time, shipping_data.Customs_clearance_completion_location, shipping_data.Customs_clearance_completion_time,shipping_data.FDA_Approval_Time, shipping_data.FDA_Approval_Awaited, shipping_data.Approval_for_Haz_clearance_from_consignee_location, shipping_data.Await_approval_for_document_from_consignee, shipping_data.Delivery_at_Customer_Destination_Location, shipping_data.Approval_for_Haz_clearance_from_consignee_time, shipping_data.Customs_clearance, shipping_data.FDA_Approval_Location ])
	// 	    .then(function (data) {		    	
	// 			resolve(data);
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = errorLog.getErrorText(err);
	// 	    	var error     = new Error(errorText);
	//             reject(error);
	// 	    });
	// 	});
	// },
	// updatePackingList: async (task,packing_list)=>{
	// 	return new Promise(function(resolve, reject) {
	// 		db.any("UPDATE tbl_track_shipment SET packing_list = $2 WHERE task_id = $1 AND (packing_list='' OR packing_list IS NULL)",
	// 		[task, packing_list ])
	// 	    .then(function (data) {		    	
	// 			resolve(data);
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = errorLog.getErrorText(err);
	// 	    	var error     = new Error(errorText);
	//             reject(error);
	// 	    });
	// 	});
	// },
	// updateInvoiceDoc: async (task,invoice_doc)=>{
	// 	return new Promise(function(resolve, reject) {
	// 		db.any("UPDATE tbl_track_shipment SET invoice_doc = $2 WHERE task_id = $1 AND (invoice_doc='' OR invoice_doc IS NULL)",
	// 		[task, invoice_doc ])
	// 	    .then(function (data) {		    	
	// 			resolve(data);
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = errorLog.getErrorText(err);
	// 	    	var error     = new Error(errorText);
	//             reject(error);
	// 	    });
	// 	});
	// },
	updateFlightBookedDoc: async (invoice_no,flight_booked_doc)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic SET flight_booked_doc = $2 WHERE invoice_id = $1 AND (flight_booked_doc='' OR flight_booked_doc IS NULL)",
			[invoice_no, flight_booked_doc ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateHAWBDoc: async (invoice_no,hawb_doc)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic SET hawb_doc = $2 WHERE invoice_id = $1 AND (hawb_doc='' OR hawb_doc IS NULL)",
			[invoice_no, hawb_doc ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateTrackingInfo: async(invoice_number) => {
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_invoice SET logistic_status = 1 WHERE invoice_number = $1",
			[invoice_number])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	getInvoiceById: async (invoice_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select * from tbl_task_logistic where invoice_id = $1",[invoice_id])
		    .then(function (data) {
				if (data && data.length > 0) {
					resolve(data[0]);
				} else {
					resolve('');
				}
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	master_data :  async ()=>{
		return new Promise(function(resolve, reject) {
			db.any("select concat(cstat_type,cstat_code) as master_data from tbl_bluedart_logistic_master",[])
		    .then(function (data) {
				resolve(data);
					
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	insertFNPShippingTrack: async (invoice_no,shipping_data)=>{
		return new Promise(function(resolve, reject) {
			db.any("INSERT INTO tbl_task_logistic (invoice_id, shipment_date, flight_booked, hawb, shipment_departed, shipment_arrived_at_destination, delivery_at_customer_destination, tracking_info_updated,   flight_booked_doc_link, hawb_doc_link, flight_booked_location, flight_booked_time, hawb_location,  hawb_time, shipment_pickup_location, shipment_departed_location, shipment_arrived_location, enter_invoice_no_shipment_pickup_comments, enter_awb_hawb_comments, enter_shipment_departed_comments, enter_shipment_arrived_at_destination_comment, enter_delivery_at_customer_destination_comment, approval_for_document_from_consignee_location, await_approval_for_haz_clearance_from_consignee, approval_for_document_from_consignee_time, customs_clearance_completion_location, customs_clearance_completion_time, fda_approval_time, fda_approval_awaited, approval_for_haz_clearance_from_consignee_location, await_approval_for_document_from_consignee, delivery_at_customer_destination_location, approval_for_haz_clearance_from_consignee_time, customs_clearance, fda_approval_location) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35) RETURNING id",
			[invoice_no, shipping_data.Shipment_Picked_from_DRL_by_Agency, shipping_data.Flight_Booked, shipping_data.HAWB, shipping_data.Shipment_Departed, shipping_data.Shipment_Arrived_at_Destination,  shipping_data.Delivery_at_Customer_Destination, shipping_data.Tracking_Info_Updated,  shipping_data.Flight_Booked_AWB_PDF, shipping_data.HAWB_Doc,   shipping_data.Flight_booked_Location, shipping_data.Flight_Booked_Time, shipping_data.HAWB_Location, shipping_data.HAWB_Time, shipping_data.Shipment_Pickup_Location, shipping_data.Shipment_Departed_Location, shipping_data.Shipment_Arrived_Location, shipping_data.Enter_Invoice_no_Shipment_pickup_comments, shipping_data.Enter_AWB_HAWB_Comments, shipping_data.Enter_Shipment_Departed_Comments, shipping_data.Enter_Shipment_Arrived_at_Destination_Comment,shipping_data.Enter_Delivery_at_Customer_Destination_Comment, shipping_data.Approval_for_document_from_consignee_location,shipping_data.Await_approval_for_Haz_clearance_from_consignee, shipping_data.Approval_for_document_from_consignee_time, shipping_data.Customs_clearance_completion_location, shipping_data.Customs_clearance_completion_time,shipping_data.FDA_Approval_Time, shipping_data.FDA_Approval_Awaited, shipping_data.Approval_for_Haz_clearance_from_consignee_location, shipping_data.Await_approval_for_document_from_consignee, shipping_data.Delivery_at_Customer_Destination_Location, shipping_data.Approval_for_Haz_clearance_from_consignee_time, shipping_data.Customs_clearance, shipping_data.FDA_Approval_Location])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateFNPShippingTrack: async (invoice_no,shipping_data)=>{	//console.log(shipping_data.Invoice_Doc_Time);
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic SET shipment_date = $2, flight_booked =$3, hawb = $4, shipment_departed = $5, shipment_arrived_at_destination = $6, delivery_at_customer_destination = $7, tracking_info_updated = $8,  flight_booked_doc_link = $9, hawb_doc_link = $10, flight_booked_location = $11, flight_booked_time = $12, hawb_location = $13,  hawb_time = $14, shipment_pickup_location = $15, shipment_departed_location = $16, shipment_arrived_location = $17, enter_invoice_no_shipment_pickup_comments = $18, enter_awb_hawb_comments = $19, enter_shipment_departed_comments = $20, enter_shipment_arrived_at_destination_comment = $21, enter_delivery_at_customer_destination_comment = $22, approval_for_document_from_consignee_location = $23, await_approval_for_haz_clearance_from_consignee = $24, approval_for_document_from_consignee_time = $25, customs_clearance_completion_location = $26, customs_clearance_completion_time = $27, fda_approval_time = $28, fda_approval_awaited = $29, approval_for_haz_clearance_from_consignee_location = $30, await_approval_for_document_from_consignee = $31, delivery_at_customer_destination_location = $32, approval_for_haz_clearance_from_consignee_time = $33, customs_clearance = $34, fda_approval_location = $35  WHERE invoice_id = $1",
			[invoice_no, shipping_data.Shipment_Picked_from_DRL_by_Agency, shipping_data.Flight_Booked, shipping_data.HAWB, shipping_data.Shipment_Departed, shipping_data.Shipment_Arrived_at_Destination, shipping_data.Delivery_at_Customer_Destination, shipping_data.Tracking_Info_Updated, shipping_data.Flight_Booked_AWB_PDF, shipping_data.HAWB_Doc, shipping_data.Flight_booked_Location, shipping_data.Flight_Booked_Time, shipping_data.HAWB_Location, shipping_data.HAWB_Time, shipping_data.Shipment_Pickup_Location, shipping_data.Shipment_Departed_Location, shipping_data.Shipment_Arrived_Location, shipping_data.Enter_Invoice_no_Shipment_pickup_comments, shipping_data.Enter_AWB_HAWB_Comments, shipping_data.Enter_Shipment_Departed_Comments, shipping_data.Enter_Shipment_Arrived_at_Destination_Comment,shipping_data.Enter_Delivery_at_Customer_Destination_Comment, shipping_data.Approval_for_document_from_consignee_location,shipping_data.Await_approval_for_Haz_clearance_from_consignee, shipping_data.Approval_for_document_from_consignee_time, shipping_data.Customs_clearance_completion_location, shipping_data.Customs_clearance_completion_time,shipping_data.FDA_Approval_Time, shipping_data.FDA_Approval_Awaited, shipping_data.Approval_for_Haz_clearance_from_consignee_location, shipping_data.Await_approval_for_document_from_consignee, shipping_data.Delivery_at_Customer_Destination_Location, shipping_data.Approval_for_Haz_clearance_from_consignee_time, shipping_data.Customs_clearance, shipping_data.FDA_Approval_Location ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},

	insertAGLShippingTrack: async (invoice_no,shipping_data)=>{	//flight_booked_doc_link,
		return new Promise(function(resolve, reject) {	//shipping_data.Flight_Booked_AWB_PDF, 
			db.any("INSERT INTO tbl_task_logistic (invoice_id, shipment_date, flight_booked, hawb, shipment_departed, shipment_arrived_at_destination, delivery_at_customer_destination, tracking_info_updated, hawb_doc_link, flight_booked_location, flight_booked_time, hawb_location,  hawb_time, shipment_pickup_location, shipment_departed_location, shipment_arrived_location, approval_for_document_from_consignee_location, await_approval_for_haz_clearance_from_consignee, approval_for_document_from_consignee_time, customs_clearance_completion_location, customs_clearance_completion_time, fda_approval_time, fda_approval_awaited, approval_for_haz_clearance_from_consignee_location, await_approval_for_document_from_consignee, delivery_at_customer_destination_location, approval_for_haz_clearance_from_consignee_time, customs_clearance, fda_approval_location,shipping_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30) RETURNING id",
			[invoice_no, shipping_data.shipment_picked_date, shipping_data.mawb_number, shipping_data.hawb_number, shipping_data.shipment_departed_date, shipping_data.shipment_arrival_date,  shipping_data.shipment_arrival_customer_date, shipping_data.shipment_completed_flag,  shipping_data.hawb_number_doc,   shipping_data.mawb_location, shipping_data.mawb_date, shipping_data.hawb_location, shipping_data.hawb_date, shipping_data.shipment_picked_location, shipping_data.shipment_departed_location, shipping_data.shipment_arrival_location,   shipping_data.shipping_progress_location1,shipping_data.shipping_progress2, shipping_data.shipping_progress_date1, shipping_data.shipping_progress_location4, shipping_data.shipping_progress_date4,shipping_data.shipping_progress_date3, shipping_data.shipping_progress3, shipping_data.shipping_progress_location2, shipping_data.shipping_progress1, shipping_data.shipment_arrival_customer_location, shipping_data.shipping_progress_date2, shipping_data.shipping_progress4, shipping_data.shipping_progress_location3,"agility"])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updatePushTrackData : async (inputData)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_bluedart_logistic_log SET status = $1 , reason = $2 WHERE id = $3 RETURNING id",
			[inputData.status,inputData.reason,inputData.id])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateAGLShippingTrack: async (invoice_no,shipping_data)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic SET shipment_date = $2, flight_booked =$3, hawb = $4, shipment_departed = $5, shipment_arrived_at_destination = $6, delivery_at_customer_destination = $7, tracking_info_updated = $8,  hawb_doc_link = $9, flight_booked_location = $10, flight_booked_time = $11, hawb_location = $12,  hawb_time = $13, shipment_pickup_location = $14, shipment_departed_location = $15, shipment_arrived_location = $16,  approval_for_document_from_consignee_location = $17, await_approval_for_haz_clearance_from_consignee = $18, approval_for_document_from_consignee_time = $19, customs_clearance_completion_location = $20, customs_clearance_completion_time = $21, fda_approval_time = $22, fda_approval_awaited = $23, approval_for_haz_clearance_from_consignee_location = $24, await_approval_for_document_from_consignee = $25, delivery_at_customer_destination_location = $26, approval_for_haz_clearance_from_consignee_time = $27, customs_clearance = $28, fda_approval_location = $29  WHERE invoice_id = $1",
			[invoice_no, shipping_data.shipment_picked_date, shipping_data.mawb_number, shipping_data.hawb_number, shipping_data.shipment_departed_date, shipping_data.shipment_arrival_date, shipping_data.shipment_arrival_customer_date, shipping_data.shipment_completed_flag, shipping_data.hawb_number_doc, shipping_data.mawb_location, shipping_data.mawb_date, shipping_data.hawb_location, shipping_data.hawb_date, shipping_data.shipment_picked_location, shipping_data.shipment_departed_location, shipping_data.shipment_arrival_location, shipping_data.shipping_progress_location1,shipping_data.shipping_progress2, shipping_data.shipping_progress_date1, shipping_data.shipping_progress_location4, shipping_data.shipping_progress_date4,shipping_data.shipping_progress_date3, shipping_data.shipping_progress3, shipping_data.shipping_progress_location2, shipping_data.shipping_progress1, shipping_data.shipment_arrival_customer_location, shipping_data.shipping_progress_date2, shipping_data.shipping_progress4, shipping_data.shipping_progress_location3 ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},

	insertPushTrackData: async (inputData)=>{
		return new Promise(function(resolve, reject) {
			db.any("INSERT INTO tbl_bluedart_logistic_log (json_data, date_added,status,reason) VALUES ($1,$2,$3,$4) RETURNING id",
			[inputData.json_data,inputData.date_added,inputData.status,inputData.reason])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	insertData :  async (inputData , date)=>{
		return new Promise(function(resolve, reject) {
			db.any("INSERT INTO tbl_bluedart_logistic_data (sender_id,receiver_id,way_bill_no, ref_no,prodcode,sub_product_code,feature,origin,origin_area_code,destination,destination_area_code,pick_up_date,pick_up_time,weight,shipment_mode,expected_delivery_date,date_added,dynamic_expected_delivery_date,customer_code) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19) RETURNING id",
			[inputData.sender_id,inputData.receiver_id,inputData.way_bill_no,inputData.ref_no , inputData.prodcode,inputData.sub_product_code,inputData.feature,inputData.origin,inputData.origin_area_code ,inputData.destination,inputData.destination_area_code,inputData.pick_up_date,inputData.pick_up_time,inputData.weight,inputData.shipment_mode,inputData.expected_delivery_date,date,inputData.dynamic_expected_delivery_date , inputData.customer_code ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	insertScanData :  async (inputData ,logistic_data_id, date,file_path)=>{
		return new Promise(function(resolve, reject) {
			db.any("INSERT INTO tbl_bluedart_logistic_scan_data (scan_type,scan_group_type,scan_code, scan,scan_date,scan_time,scanned_location_code,scanned_location,scanned_location_state_code,status_timeZone,comments,status_latitude,status_longitude,logistic_data_id,date_added,scanned_location_city,sorry_card_number,reached_destination_location,secure_code,file_path) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20) RETURNING id",
			[inputData.scan_type,inputData.scan_group_type,inputData.scan_code,inputData. scan,inputData.scan_date,inputData.scan_time,inputData.scanned_location_code,inputData.scanned_location,inputData.scanned_location_state_code,inputData.status_timeZone,inputData.comments,inputData.status_latitude,inputData.status_longitude,logistic_data_id,date,inputData.scanned_location_city,inputData.sorry_card_number,inputData.reached_destination_location,inputData.secure_code,file_path ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	update_scan_details : async (inputData ,logistic_data_id, date,file_name)=>{
		return new Promise(function(resolve, reject) {
			let sql = '';
			let arr_data = [];
			if(file_name == ''){
				sql = "UPDATE tbl_bluedart_logistic_scan_data SET scan = $4, scan_date = $5 , scan_time = $6 ,date_updated = $8 , scanned_location = $9 ,scanned_location_state_code = $10 ,status_timeZone = $11 ,comments = $12 ,status_latitude = $13 , status_longitude = $14 , scanned_location_city =$16 ,sorry_card_number = $17 ,reached_destination_location = $18 ,secure_code = $19 WHERE scan_type = $1 AND  scan_group_type = $2 AND scan_code = $3 AND scanned_location_code =$7 AND logistic_data_id = $15  RETURNING id";
				arr_data = [inputData.scan_type,inputData.scan_group_type , inputData.scan_code,inputData.scan,inputData.scan_date,inputData.scan_time,inputData.scanned_location_code , date, inputData.scanned_location ,inputData.scanned_location_state_code , inputData.status_timeZone , inputData.comments , inputData.status_latitude , inputData.status_longitude ,logistic_data_id,inputData.scanned_location_city,inputData.sorry_card_number,inputData.reached_destination_location,inputData.secure_code] 
		   }else{
			   sql = "UPDATE tbl_bluedart_logistic_scan_data SET scan = $4, scan_date = $5 , scan_time = $6 ,date_updated = $8 , scanned_location = $9 ,scanned_location_state_code = $10 ,status_timeZone = $11 ,comments = $12 ,status_latitude = $13 , status_longitude = $14 , scanned_location_city =$16 ,sorry_card_number = $17 ,reached_destination_location = $18 ,secure_code = $19 , file_path = $20 WHERE scan_type = $1 AND  scan_group_type = $2 AND scan_code = $3 AND scanned_location_code =$7 AND logistic_data_id = $15  RETURNING id";
			   arr_data = [inputData.scan_type,inputData.scan_group_type , inputData.scan_code,inputData.scan,inputData.scan_date,inputData.scan_time,inputData.scanned_location_code , date, inputData.scanned_location ,inputData.scanned_location_state_code , inputData.status_timeZone , inputData.comments , inputData.status_latitude , inputData.status_longitude ,logistic_data_id,inputData.scanned_location_city,inputData.sorry_card_number,inputData.reached_destination_location,inputData.secure_code,file_name] 
		   }
			db.any(sql,arr_data)
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
update_logistic_details : async (inputData , update_date)=>{
		return new Promise(function(resolve, reject) {
			db.any(`UPDATE tbl_bluedart_logistic_data SET sender_id = $1, receiver_id = $2 , prodcode = $5 ,sub_product_code = $6 , feature = $7 ,origin = $8 ,origin_area_code = $9 ,destination = $10 ,destination_area_code = $11 , pick_up_date = $12 , pick_up_time =$13 ,weight = $14 ,shipment_mode = $15 ,expected_delivery_date = $16, dynamic_expected_delivery_date = $17 , customer_code = $18 , date_updated = $19  WHERE way_bill_no = $3 AND  ref_no = $4 RETURNING id`,[inputData.sender_id,inputData.receiver_id , inputData.way_bill_no,inputData.ref_no,inputData.prodcode,inputData.sub_product_code,inputData.feature ,inputData.origin ,inputData.origin_area_code , inputData.destination , inputData.destination_area_code , inputData.pick_up_date , inputData.pick_up_time,inputData.weight,inputData.shipment_mode,inputData.expected_delivery_date,inputData.dynamic_expected_delivery_date,inputData.customer_code,update_date])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	check_shipment : async (way_bill_no , ref_no)=>{
		return new Promise(function(resolve, reject) {
			db.any("select id from tbl_bluedart_logistic_data WHERE way_bill_no = $1 AND ref_no = $2",
			[way_bill_no,ref_no])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	check_scan : async (scan_type ,scan_group_type,scan_code,scanned_location_code ,logistic_data_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select id from tbl_bluedart_logistic_scan_data WHERE scan_type = $1 AND scan_group_type = $2 AND scan_code = $3 AND scanned_location_code = $4 AND logistic_data_id = $5 ",
			[scan_type ,scan_group_type,scan_code,scanned_location_code,logistic_data_id])
		    .then(function (data) {		    	
                   resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	getBlueDartShipping: async (sales_order_no) => {
		return new Promise(function (resolve, reject) {
			let sql = `
			SELECT 
			tbl_task_invoice.invoice_number,
			tbl_task_invoice.invoice_location,
			tbl_task_invoice.invoice_file_name,
			tbl_task_invoice.invoice_date,
			tbl_task_invoice.packing_location,
			tbl_task_invoice.packing_file_name,
			tbl_task_invoice.packing_date,
			tbl_task_invoice.logistics_partner,
			tbl_tasks.task_id,
			tbl_bluedart_logistic_data.ref_no, origin,destination, pick_up_date, pick_up_time, expected_delivery_date, dynamic_expected_delivery_date
			   
			FROM tbl_tasks		
			LEFT JOIN tbl_task_invoice ON tbl_task_invoice.sales_order_no = tbl_tasks.sales_order_no
			LEFT JOIN tbl_bluedart_logistic_data ON tbl_task_invoice.invoice_number = tbl_bluedart_logistic_data.way_bill_no
			
			WHERE tbl_task_invoice.sales_order_no = ($1) AND (tbl_task_invoice.invoice_number like '9013%' OR tbl_task_invoice.invoice_number like '9021%') AND tbl_task_invoice.invoice_type = 'M' AND LOWER(tbl_task_invoice.logistics_partner) LIKE '%blue%'`;	//

			db.any(sql, [sales_order_no]).then(function (data) {
				resolve(data);
			})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},

	getBlueDartShippingSteps: async (invoice_number) => {
		return new Promise(function (resolve, reject) {
			let sql = `
			SELECT scan, scan_date, scan_time, scanned_location	
			FROM tbl_bluedart_logistic_scan_data		
			LEFT JOIN tbl_bluedart_logistic_data ON tbl_bluedart_logistic_data.id = tbl_bluedart_logistic_scan_data.logistic_data_id
			
			WHERE tbl_bluedart_logistic_data.way_bill_no::bigint  = ($1) 
			ORDER BY tbl_bluedart_logistic_scan_data.id ASC`;

			db.any(sql, [invoice_number]).then(function (data) {
				resolve(data);
			})
				.catch(function (err) {
					var errorText = errorLog.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},

	get_sto_invoice_details: async (sales_order_no) => {
		return new Promise(function (resolve, reject) {
	
		  let sql = `
					SELECT 
						DISTINCT(tbl_task_invoice.invoice_number) as invoice_number,
						tbl_task_invoice.invoice_location,
						tbl_task_invoice.invoice_file_name,
						tbl_task_invoice.invoice_file_path,
						tbl_task_invoice.invoice_date,
						tbl_task_invoice.packing_location,
						tbl_task_invoice.packing_file_name,
						tbl_task_invoice.packing_file_path,
						tbl_task_invoice.packing_date,
						tbl_task_invoice.logistics_partner,
						tbl_task_logistic.* 
						   
					FROM tbl_task_invoice
					LEFT JOIN tbl_task_logistic	ON tbl_task_invoice.invoice_number = tbl_task_logistic.invoice_id
					LEFT JOIN tbl_sales_order_sap ON tbl_task_invoice.sales_order_no = tbl_sales_order_sap.sto_no
					WHERE tbl_sales_order_sap.sales_order = ($1)
					AND (tbl_task_invoice.invoice_number like '9013%' OR tbl_task_invoice.invoice_number like '9021%') AND tbl_task_invoice.invoice_type = 'M' AND LOWER(logistics_partner) NOT LIKE '%blue%'`;// 
	
		  db.any(sql, [sales_order_no]).then(function (data) {
			resolve(data);
		  })
			.catch(function (err) {
			  var errorText = errorLog.getErrorText(err);
			  var error = new Error(errorText);
			  reject(error);
			});
	
		});
	  },
	  getSTOBlueDartShipping: async (sales_order_no) => {
		return new Promise(function (resolve, reject) {
			let sql = `
			SELECT 
			tbl_task_invoice.invoice_number,
			tbl_task_invoice.invoice_location,
			tbl_task_invoice.invoice_file_name,
			tbl_task_invoice.invoice_date,
			tbl_task_invoice.packing_location,
			tbl_task_invoice.packing_file_name,
			tbl_task_invoice.packing_date,
			tbl_task_invoice.logistics_partner,	
			tbl_bluedart_logistic_data.ref_no, origin,destination, pick_up_date, pick_up_time, expected_delivery_date, dynamic_expected_delivery_date
			   
			FROM tbl_task_invoice 
			LEFT JOIN tbl_sales_order_sap ON tbl_task_invoice.sales_order_no = tbl_sales_order_sap.sto_no
			LEFT JOIN tbl_bluedart_logistic_data ON tbl_task_invoice.invoice_number = tbl_bluedart_logistic_data.way_bill_no
			
			WHERE tbl_sales_order_sap.sales_order = ($1) AND (tbl_task_invoice.invoice_number like '9013%' OR tbl_task_invoice.invoice_number like '9021%') AND tbl_task_invoice.invoice_type = 'M' AND LOWER(tbl_task_invoice.logistics_partner) LIKE '%blue%'`;	//

			db.any(sql, [sales_order_no]).then(function (data) {
				resolve(data);
			})
				.catch(function (err) {
					var errorText = errorLog.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},






	/** ++ Temp for live Agility data fetch */
	getInvoiceById_Live: async (invoice_id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select * from tbl_task_logistic_live where invoice_id = $1",[invoice_id])
		    .then(function (data) {
				if (data && data.length > 0) {
					resolve(data[0]);
				} else {
					resolve('');
				}
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	insertAGLShippingTrack_Live: async (invoice_no,shipping_data)=>{	//flight_booked_doc_link,
		return new Promise(function(resolve, reject) {	//shipping_data.Flight_Booked_AWB_PDF, 
			db.any("INSERT INTO tbl_task_logistic_live (invoice_id, shipment_date, flight_booked, hawb, shipment_departed, shipment_arrived_at_destination, delivery_at_customer_destination, tracking_info_updated, hawb_doc_link, flight_booked_location, flight_booked_time, hawb_location,  hawb_time, shipment_pickup_location, shipment_departed_location, shipment_arrived_location, approval_for_document_from_consignee_location, await_approval_for_haz_clearance_from_consignee, approval_for_document_from_consignee_time, customs_clearance_completion_location, customs_clearance_completion_time, fda_approval_time, fda_approval_awaited, approval_for_haz_clearance_from_consignee_location, await_approval_for_document_from_consignee, delivery_at_customer_destination_location, approval_for_haz_clearance_from_consignee_time, customs_clearance, fda_approval_location,shipping_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30) RETURNING id",
			[invoice_no, shipping_data.shipment_picked_date, shipping_data.mawb_number, shipping_data.hawb_number, shipping_data.shipment_departed_date, shipping_data.shipment_arrival_date,  shipping_data.shipment_arrival_customer_date, shipping_data.shipment_completed_flag,  shipping_data.hawb_number_doc,   shipping_data.mawb_location, shipping_data.mawb_date, shipping_data.hawb_location, shipping_data.hawb_date, shipping_data.shipment_picked_location, shipping_data.shipment_departed_location, shipping_data.shipment_arrival_location,   shipping_data.shipping_progress_location1,shipping_data.shipping_progress2, shipping_data.shipping_progress_date1, shipping_data.shipping_progress_location4, shipping_data.shipping_progress_date4,shipping_data.shipping_progress_date3, shipping_data.shipping_progress3, shipping_data.shipping_progress_location2, shipping_data.shipping_progress1, shipping_data.shipment_arrival_customer_location, shipping_data.shipping_progress_date2, shipping_data.shipping_progress4, shipping_data.shipping_progress_location3,"agility"])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateAGLShippingTrack_Live: async (invoice_no,shipping_data)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic_live SET shipment_date = $2, flight_booked =$3, hawb = $4, shipment_departed = $5, shipment_arrived_at_destination = $6, delivery_at_customer_destination = $7, tracking_info_updated = $8,  hawb_doc_link = $9, flight_booked_location = $10, flight_booked_time = $11, hawb_location = $12,  hawb_time = $13, shipment_pickup_location = $14, shipment_departed_location = $15, shipment_arrived_location = $16,  approval_for_document_from_consignee_location = $17, await_approval_for_haz_clearance_from_consignee = $18, approval_for_document_from_consignee_time = $19, customs_clearance_completion_location = $20, customs_clearance_completion_time = $21, fda_approval_time = $22, fda_approval_awaited = $23, approval_for_haz_clearance_from_consignee_location = $24, await_approval_for_document_from_consignee = $25, delivery_at_customer_destination_location = $26, approval_for_haz_clearance_from_consignee_time = $27, customs_clearance = $28, fda_approval_location = $29  WHERE invoice_id = $1",
			[invoice_no, shipping_data.shipment_picked_date, shipping_data.mawb_number, shipping_data.hawb_number, shipping_data.shipment_departed_date, shipping_data.shipment_arrival_date, shipping_data.shipment_arrival_customer_date, shipping_data.shipment_completed_flag, shipping_data.hawb_number_doc, shipping_data.mawb_location, shipping_data.mawb_date, shipping_data.hawb_location, shipping_data.hawb_date, shipping_data.shipment_picked_location, shipping_data.shipment_departed_location, shipping_data.shipment_arrival_location, shipping_data.shipping_progress_location1,shipping_data.shipping_progress2, shipping_data.shipping_progress_date1, shipping_data.shipping_progress_location4, shipping_data.shipping_progress_date4,shipping_data.shipping_progress_date3, shipping_data.shipping_progress3, shipping_data.shipping_progress_location2, shipping_data.shipping_progress1, shipping_data.shipment_arrival_customer_location, shipping_data.shipping_progress_date2, shipping_data.shipping_progress4, shipping_data.shipping_progress_location3 ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateFlightBookedDoc_Live: async (invoice_no,flight_booked_doc)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic_live SET flight_booked_doc = $2 WHERE invoice_id = $1 AND (flight_booked_doc='' OR flight_booked_doc IS NULL)",
			[invoice_no, flight_booked_doc ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateHAWBDoc_Live: async (invoice_no,hawb_doc)=>{
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic_live SET hawb_doc = $2 WHERE invoice_id = $1 AND (hawb_doc='' OR hawb_doc IS NULL)",
			[invoice_no, hawb_doc ])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	updateTrackingInfo_Live: async(invoice_number) => {
		return new Promise(function(resolve, reject) {
			db.any("UPDATE tbl_task_logistic_live SET logistic_status = 1 WHERE invoice_number = $1",
			[invoice_number])
		    .then(function (data) {		    	
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = errorLog.getErrorText(err);
		    	var error     = new Error(errorText);
	            reject(error);
		    });
		});
	},
	/** -- Temp for live Agility data fetch */
}
