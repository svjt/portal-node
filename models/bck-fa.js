const db = require('../configuration/dbConn');
const common = require('../controllers/common');

module.exports = {
	suggest_all: async (string) => {
		return new Promise(function (resolve, reject) {
			let query_sql = 
			`SELECT 
				name AS label 
			FROM tbl_master_fa_ndc_code 
			WHERE 
				LOWER(name) LIKE '%${string.toLowerCase()}%' 
				AND name !='' 
				AND name IS NOT NULL 
				AND name != '\\N' 
				AND type = 'API' 
			LIMIT 10`;
			db.any(query_sql)
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});

	},
	get_ndc_codes: async (string) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				ndc_code 
			FROM tbl_master_fa_ndc_code 
			WHERE 
				LOWER(name)= ($1)`;
			db.any(query_sql, [string.toLowerCase()])
				.then(function (data = []) {
					if(data.length > 0){
						resolve(data[0].ndc_code);
					}else{
						resolve('')
					}
					
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	//==================== US =========================//

	find_product_us_ndc: async (ndc_arr, limit, offset) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv) 
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
			LIMIT $2 OFFSET $3`;
			db.any(query_sql, [ndc_arr,limit, offset])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},
	count_product_us_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				count(id) as count  
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv) 
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N'`;
			db.any(query_sql,[ndc_arr])
			.then(function (data = []) {
				resolve(data[0].count)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},
	find_product_us_download_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv) 
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N'`;
			db.any(query_sql,[ndc_arr])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},
	find_product_us_download_request_more_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization,
				no_of_excepients,
				dosage_form,
				route,
				color,
				shape,
				sizes,
				strength,
				rld 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' `;
			db.any(query_sql,[ndc_arr])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//===================== AU =======================//

	find_product_au_ndc: async (ndc_arr, limit, offset) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				product_name,
				ndc_code,
				ndc_name,
				submission_number,
				sponsor,
				submission_type,
				authorization_status,
				auspar_date,
				publication_date 
			FROM tbl_master_fa_product_au 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
			LIMIT $2 OFFSET $3`;
			//console.log(query_sql);
			db.any(query_sql, [ndc_arr,limit, offset])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	count_product_au_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				count(id) as count  
			FROM tbl_master_fa_product_au 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N'`;
			db.any(query_sql,[ndc_arr])
			.then(function (data) {
				resolve(data[0].count)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_au_download_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				product_name,
				ndc_code,
				ndc_name,
				submission_number,
				sponsor,
				submission_type,
				authorization_status,
				auspar_date,
				publication_date 
			FROM tbl_master_fa_product_au 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N'`;

			db.any(query_sql,[ndc_arr])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//============== EMA =====================//

	find_product_ema_ndc: async (ndc_arr, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = 
			`SELECT 
				productname,
				ndc_code,
				product_number,
				authorisation_status,
				therapeutic_area,
				generic,
				biosimilar,
				additional_monitoring,
				orphan_drug,
				marketing_date,
				pharma_class,
				company_name 
			FROM tbl_master_fa_product_ema 
			WHERE 
				ndc_code IN ($1:csv) 
				AND productname !='' 
				AND productname IS NOT NULL 
				AND productname != '\\N' 
			LIMIT $2 OFFSET $3`;
			db.any(query_sql, [ndc_arr, limit, offset])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	count_product_ema_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = 
			`SELECT 
				count(ema_id) as count 
			FROM tbl_master_fa_product_ema 
			WHERE 
				ndc_code IN ($1:csv) 
				AND productname !='' 
				AND productname IS NOT NULL 
				AND productname != '\\N'`;
			db.any(query_sql, [ndc_arr])
			.then(function (data = []) {
				resolve(data[0].count)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_ema_download_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = 
			`SELECT 
				productname,
				ndc_code,
				product_number,
				authorisation_status,
				therapeutic_area,
				generic,
				biosimilar,
				additional_monitoring,
				orphan_drug,
				marketing_date,
				pharma_class,
				company_name 
			FROM tbl_master_fa_product_ema 
			WHERE 
				ndc_code IN ($1:csv) 
				AND productname !='' 
				AND productname IS NOT NULL 
				AND productname != '\\N'`;
			db.any(query_sql, [ndc_arr])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_ema_download_request_more_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = 
			`SELECT 
				productname,
				ndc_code,
				product_number,
				authorisation_status,
				therapeutic_area,
				generic,
				biosimilar,
				additional_monitoring,
				orphan_drug,
				marketing_date,
				pharma_class,
				company_name,
				published_date,
				indication 
			FROM tbl_master_fa_product_ema 
			WHERE 
				ndc_code IN ($1:csv) 
				AND productname !='' 
				AND productname IS NOT NULL 
				AND productname != '\\N'`;
			db.any(query_sql, [ndc_arr])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//==================== DRUG BANK ==================//

	find_product_drugbank_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
			SELECT 
				(CASE WHEN (dcname IS NULL OR dcname = '\\N' ) THEN '-' ELSE dcname END) AS dcname,
				(CASE WHEN (ndccode IS NULL OR ndccode = '\\N' ) THEN '-' ELSE ndccode END) AS ndccode,
				(CASE WHEN (drugbank_id IS NULL OR drugbank_id = '\\N' ) THEN '-' ELSE drugbank_id END) AS drugbank_id,
				(CASE WHEN (productname IS NULL OR productname = '\\N' ) THEN '-' ELSE productname END) AS productname,
				(CASE WHEN (water_sol_x IS NULL OR water_sol_x = '\\N' ) THEN '-' ELSE water_sol_x END) AS water_sol_x,
				(CASE WHEN (logp_x IS NULL OR logp_x = '\\N' ) THEN '-' ELSE logp_x END) AS logp_x,
				(CASE WHEN (pka IS NULL OR pka = '\\N' ) THEN '-' ELSE pka END) AS pka,
				(CASE WHEN (smiles IS NULL OR smiles = '\\N' ) THEN '-' ELSE smiles END) AS smiles,
				(CASE WHEN (casnum IS NULL OR casnum = '\\N' ) THEN '-' ELSE casnum END) AS casnum,
				(CASE WHEN (boiling_point IS NULL OR boiling_point = '\\N' ) THEN '-' ELSE boiling_point END) AS boiling_point,
				(CASE WHEN (hydrophobicity IS NULL OR hydrophobicity = '\\N' ) THEN '-' ELSE hydrophobicity END) AS hydrophobicity,
				(CASE WHEN (isoelectric_point IS NULL OR isoelectric_point = '\\N' ) THEN '-' ELSE isoelectric_point END) AS isoelectric_point,
				(CASE WHEN (melting_point IS NULL OR melting_point = '\\N' ) THEN '-' ELSE melting_point END) AS melting_point,
				(CASE WHEN (molecular_formula_x IS NULL OR molecular_formula_x = '\\N' ) THEN '-' ELSE molecular_formula_x END) AS molecular_formula_x,
				(CASE WHEN (molecular_weight_x IS NULL OR molecular_weight_x = '\\N' ) THEN '-' ELSE molecular_weight_x END) AS molecular_weight_x,
				(CASE WHEN (radioactivity IS NULL OR radioactivity = '\\N' ) THEN '-' ELSE radioactivity END) AS radioactivity,
				(CASE WHEN (caco2_permeability IS NULL OR caco2_permeability = '\\N' ) THEN '-' ELSE caco2_permeability END) AS caco2_permeability,
				(CASE WHEN (logs_x IS NULL OR logs_x = '\\N' ) THEN '-' ELSE logs_x END) AS logs_x,
				(CASE WHEN (description IS NULL OR description = '\\N' ) THEN '-' ELSE description END) AS description,
				(CASE WHEN (indication IS NULL OR indication = '\\N' ) THEN '-' ELSE indication END) AS indication,
				(CASE WHEN (average_mass IS NULL OR average_mass = '\\N' ) THEN '-' ELSE average_mass END) AS average_mass,
				(CASE WHEN (monoisotopic_mass IS NULL OR monoisotopic_mass = '\\N' ) THEN '-' ELSE monoisotopic_mass END) AS monoisotopic_mass,
				(CASE WHEN (state IS NULL OR state = '\\N' ) THEN '-' ELSE state END) AS state,
				(CASE WHEN (pharmacodynamics IS NULL OR pharmacodynamics = '\\N' ) THEN '-' ELSE pharmacodynamics END) AS pharmacodynamics,
				(CASE WHEN (clearance IS NULL OR clearance = '\\N' ) THEN '-' ELSE clearance END) AS clearance,
				(CASE WHEN (mechanism_of_action IS NULL OR mechanism_of_action = '\\N' ) THEN '-' ELSE mechanism_of_action END) AS mechanism_of_action,
				(CASE WHEN (toxicity IS NULL OR toxicity = '\\N' ) THEN '-' ELSE toxicity END) AS toxicity,
				(CASE WHEN (metabolism IS NULL OR metabolism = '\\N' ) THEN '-' ELSE metabolism END) AS metabolism,
				(CASE WHEN (absorption IS NULL OR absorption = '\\N' ) THEN '-' ELSE absorption END) AS absorption,
				(CASE WHEN (half_life IS NULL OR half_life = '\\N' ) THEN '-' ELSE half_life END) AS half_life,
				(CASE WHEN (protein_binding IS NULL OR protein_binding = '\\N' ) THEN '-' ELSE protein_binding END) AS protein_binding,
				(CASE WHEN (route_of_elimination IS NULL OR route_of_elimination = '\\N' ) THEN '-' ELSE route_of_elimination END) AS route_of_elimination,
				(CASE WHEN (volume_of_distribution IS NULL OR volume_of_distribution = '\\N' ) THEN '-' ELSE volume_of_distribution END) AS volume_of_distribution,
				(CASE WHEN (h_bond_acceptor_count IS NULL OR h_bond_acceptor_count = '\\N' ) THEN '-' ELSE h_bond_acceptor_count END) AS h_bond_acceptor_count,
				(CASE WHEN (h_bond_donor_ch_bond_donor_countount IS NULL OR h_bond_donor_ch_bond_donor_countount = '\\N' ) THEN '-' ELSE h_bond_donor_ch_bond_donor_countount END) AS h_bond_donor_ch_bond_donor_countount,
				(CASE WHEN (iupac_name IS NULL OR iupac_name = '\\N' ) THEN '-' ELSE iupac_name END) AS iupac_name,
				(CASE WHEN (molecular_formula_y IS NULL OR molecular_formula_y = '\\N' ) THEN '-' ELSE molecular_formula_y END) AS molecular_formula_y,
				(CASE WHEN (molecular_weight_y IS NULL OR molecular_weight_y = '\\N' ) THEN '-' ELSE molecular_weight_y END) AS molecular_weight_y,
				(CASE WHEN (physiological_charge IS NULL OR physiological_charge = '\\N' ) THEN '-' ELSE physiological_charge END) AS physiological_charge,
				(CASE WHEN (polar_surface_area_psa IS NULL OR polar_surface_area_psa = '\\N' ) THEN '-' ELSE polar_surface_area_psa END) AS polar_surface_area_psa,
				(CASE WHEN (polarizability IS NULL OR polarizability = '\\N' ) THEN '-' ELSE polarizability END) AS polarizability,
				(CASE WHEN (refractivity IS NULL OR refractivity = '\\N' ) THEN '-' ELSE refractivity END) AS refractivity,
				(CASE WHEN (rotatable_bond_count IS NULL OR rotatable_bond_count = '\\N' ) THEN '-' ELSE rotatable_bond_count END) AS rotatable_bond_count,
				(CASE WHEN (water_solubility_y IS NULL OR water_solubility_y = '\\N' ) THEN '-' ELSE water_solubility_y END) AS water_solubility_y,
				(CASE WHEN (logp_y IS NULL OR logp_y = '\\N' ) THEN '-' ELSE logp_y END) AS logp_y,
				(CASE WHEN (logs_y IS NULL OR logs_y = '\\N' ) THEN '-' ELSE logs_y END) AS logs_y,
				(CASE WHEN (pka_strongest_acidic IS NULL OR pka_strongest_acidic = '\\N' ) THEN '-' ELSE pka_strongest_acidic END) AS pka_strongest_acidic,
				(CASE WHEN (pka_strongest_basic IS NULL OR pka_strongest_basic = '\\N' ) THEN '-' ELSE pka_strongest_basic END) AS pka_strongest_basic,
				(CASE WHEN (dc_direct_parent IS NULL OR dc_direct_parent = '\\N' ) THEN '-' ELSE dc_direct_parent END) AS dc_direct_parent,
				(CASE WHEN (pharm_classes IS NULL OR pharm_classes = '\\N') THEN '-' ELSE pharm_classes END) AS pharm_classes
				 
			FROM tbl_master_fa_drug_bank 
			WHERE 
				ndccode IN ($1:csv) 
				AND dcname !='' 
				AND dcname IS NOT NULL 
				AND dcname != '\\N' 
				LIMIT 1`;
			db.any(query_sql, [ndc_arr])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},
	find_product_drugbank_download_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = `SELECT 
				(CASE WHEN (dcname IS NULL OR dcname = '\\N' ) THEN '-' ELSE dcname END) AS dcname,
				(CASE WHEN (ndccode IS NULL OR ndccode = '\\N' ) THEN '-' ELSE ndccode END) AS ndccode,
				(CASE WHEN (drugbank_id IS NULL OR drugbank_id = '\\N' ) THEN '-' ELSE drugbank_id END) AS drugbank_id,
				(CASE WHEN (productname IS NULL OR productname = '\\N' ) THEN '-' ELSE productname END) AS productname,
				(CASE WHEN (water_sol_x IS NULL OR water_sol_x = '\\N' ) THEN '-' ELSE water_sol_x END) AS water_sol_x,
				(CASE WHEN (logp_x IS NULL OR logp_x = '\\N' ) THEN '-' ELSE logp_x END) AS logp_x,
				(CASE WHEN (pka IS NULL OR pka = '\\N' ) THEN '-' ELSE pka END) AS pka,
				(CASE WHEN (smiles IS NULL OR smiles = '\\N' ) THEN '-' ELSE smiles END) AS smiles,
				(CASE WHEN (casnum IS NULL OR casnum = '\\N' ) THEN '-' ELSE casnum END) AS casnum,
				(CASE WHEN (boiling_point IS NULL OR boiling_point = '\\N' ) THEN '-' ELSE boiling_point END) AS boiling_point,
				(CASE WHEN (hydrophobicity IS NULL OR hydrophobicity = '\\N' ) THEN '-' ELSE hydrophobicity END) AS hydrophobicity,
				(CASE WHEN (isoelectric_point IS NULL OR isoelectric_point = '\\N' ) THEN '-' ELSE isoelectric_point END) AS isoelectric_point,
				(CASE WHEN (melting_point IS NULL OR melting_point = '\\N' ) THEN '-' ELSE melting_point END) AS melting_point,
				(CASE WHEN (molecular_formula_x IS NULL OR molecular_formula_x = '\\N' ) THEN '-' ELSE molecular_formula_x END) AS molecular_formula_x,
				(CASE WHEN (molecular_weight_x IS NULL OR molecular_weight_x = '\\N' ) THEN '-' ELSE molecular_weight_x END) AS molecular_weight_x,
				(CASE WHEN (radioactivity IS NULL OR radioactivity = '\\N' ) THEN '-' ELSE radioactivity END) AS radioactivity,
				(CASE WHEN (caco2_permeability IS NULL OR caco2_permeability = '\\N' ) THEN '-' ELSE caco2_permeability END) AS caco2_permeability,
				(CASE WHEN (logs_x IS NULL OR logs_x = '\\N' ) THEN '-' ELSE logs_x END) AS logs_x,
				(CASE WHEN (description IS NULL OR description = '\\N' ) THEN '-' ELSE description END) AS description,
				(CASE WHEN (indication IS NULL OR indication = '\\N' ) THEN '-' ELSE indication END) AS indication,
				(CASE WHEN (average_mass IS NULL OR average_mass = '\\N' ) THEN '-' ELSE average_mass END) AS average_mass,
				(CASE WHEN (monoisotopic_mass IS NULL OR monoisotopic_mass = '\\N' ) THEN '-' ELSE monoisotopic_mass END) AS monoisotopic_mass,
				(CASE WHEN (state IS NULL OR state = '\\N' ) THEN '-' ELSE state END) AS state,
				(CASE WHEN (pharmacodynamics IS NULL OR pharmacodynamics = '\\N' ) THEN '-' ELSE pharmacodynamics END) AS pharmacodynamics,
				(CASE WHEN (clearance IS NULL OR clearance = '\\N' ) THEN '-' ELSE clearance END) AS clearance,
				(CASE WHEN (mechanism_of_action IS NULL OR mechanism_of_action = '\\N' ) THEN '-' ELSE mechanism_of_action END) AS mechanism_of_action,
				(CASE WHEN (toxicity IS NULL OR toxicity = '\\N' ) THEN '-' ELSE toxicity END) AS toxicity,
				(CASE WHEN (metabolism IS NULL OR metabolism = '\\N' ) THEN '-' ELSE metabolism END) AS metabolism,
				(CASE WHEN (absorption IS NULL OR absorption = '\\N' ) THEN '-' ELSE absorption END) AS absorption,
				(CASE WHEN (half_life IS NULL OR half_life = '\\N' ) THEN '-' ELSE half_life END) AS half_life,
				(CASE WHEN (protein_binding IS NULL OR protein_binding = '\\N' ) THEN '-' ELSE protein_binding END) AS protein_binding,
				(CASE WHEN (route_of_elimination IS NULL OR route_of_elimination = '\\N' ) THEN '-' ELSE route_of_elimination END) AS route_of_elimination,
				(CASE WHEN (volume_of_distribution IS NULL OR volume_of_distribution = '\\N' ) THEN '-' ELSE volume_of_distribution END) AS volume_of_distribution,
				(CASE WHEN (h_bond_acceptor_count IS NULL OR h_bond_acceptor_count = '\\N' ) THEN '-' ELSE h_bond_acceptor_count END) AS h_bond_acceptor_count,
				(CASE WHEN (h_bond_donor_ch_bond_donor_countount IS NULL OR h_bond_donor_ch_bond_donor_countount = '\\N' ) THEN '-' ELSE h_bond_donor_ch_bond_donor_countount END) AS h_bond_donor_ch_bond_donor_countount,
				(CASE WHEN (iupac_name IS NULL OR iupac_name = '\\N' ) THEN '-' ELSE iupac_name END) AS iupac_name,
				(CASE WHEN (molecular_formula_y IS NULL OR molecular_formula_y = '\\N' ) THEN '-' ELSE molecular_formula_y END) AS molecular_formula_y,
				(CASE WHEN (molecular_weight_y IS NULL OR molecular_weight_y = '\\N' ) THEN '-' ELSE molecular_weight_y END) AS molecular_weight_y,
				(CASE WHEN (physiological_charge IS NULL OR physiological_charge = '\\N' ) THEN '-' ELSE physiological_charge END) AS physiological_charge,
				(CASE WHEN (polar_surface_area_psa IS NULL OR polar_surface_area_psa = '\\N' ) THEN '-' ELSE polar_surface_area_psa END) AS polar_surface_area_psa,
				(CASE WHEN (polarizability IS NULL OR polarizability = '\\N' ) THEN '-' ELSE polarizability END) AS polarizability,
				(CASE WHEN (refractivity IS NULL OR refractivity = '\\N' ) THEN '-' ELSE refractivity END) AS refractivity,
				(CASE WHEN (rotatable_bond_count IS NULL OR rotatable_bond_count = '\\N' ) THEN '-' ELSE rotatable_bond_count END) AS rotatable_bond_count,
				(CASE WHEN (water_solubility_y IS NULL OR water_solubility_y = '\\N' ) THEN '-' ELSE water_solubility_y END) AS water_solubility_y,
				(CASE WHEN (logp_y IS NULL OR logp_y = '\\N' ) THEN '-' ELSE logp_y END) AS logp_y,
				(CASE WHEN (logs_y IS NULL OR logs_y = '\\N' ) THEN '-' ELSE logs_y END) AS logs_y,
				(CASE WHEN (pka_strongest_acidic IS NULL OR pka_strongest_acidic = '\\N' ) THEN '-' ELSE pka_strongest_acidic END) AS pka_strongest_acidic,
				(CASE WHEN (pka_strongest_basic IS NULL OR pka_strongest_basic = '\\N' ) THEN '-' ELSE pka_strongest_basic END) AS pka_strongest_basic,
				(CASE WHEN (dc_direct_parent IS NULL OR dc_direct_parent = '\\N' ) THEN '-' ELSE dc_direct_parent END) AS dc_direct_parent,
				(CASE WHEN (pharm_classes IS NULL OR pharm_classes = '\\N') THEN '-' ELSE pharm_classes END) AS pharm_classes
			FROM tbl_master_fa_drug_bank 
			WHERE 
				ndccode IN ($1:csv)
				AND dcname !='' 
				AND dcname IS NOT NULL 
				AND dcname != '\\N' 
				LIMIT 1`;

			db.any(query_sql,[ndc_arr])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//====================== PATENT ======================//

	find_product_patent_ndc: async (ndc_arr, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				SELECT 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (productname IS NULL OR productname = '\\N') THEN '-' ELSE productname END) AS productname,
					(CASE WHEN (exclusivity_information IS NULL OR exclusivity_information = '\\N') THEN '-' ELSE exclusivity_information END) AS exclusivity_information,
					(CASE WHEN (submission_date IS NULL OR submission_date = '\\N') THEN '-' ELSE submission_date END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (current_assignee IS NULL OR current_assignee = '\\N') THEN '-' ELSE current_assignee END) AS current_assignee,
					(CASE WHEN (patent_title IS NULL OR patent_title = '\\N') THEN '-' ELSE patent_title END) AS patent_title
				FROM tbl_master_fa_patent 
				WHERE 
					ndccode IN ($1:csv)
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N' 
				LIMIT $2 OFFSET $3 `;

			db.any(query_sql, [ndc_arr, limit, offset])
			.then(function (data =[]) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	count_product_patent_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = 
			`SELECT 
				count(patent_number) as count 
			FROM tbl_master_fa_patent 
			WHERE 
				ndccode IN ($1:csv)
				AND productname !='' 
				AND productname IS NOT NULL 
				AND productname != '\\N'`;
			db.any(query_sql,[ndc_arr])
			.then(function (data =[]) {
				resolve(data[0].count);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_patent_download_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				SELECT 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (MAX(productname) IS NULL OR MAX(productname) = '\\N') THEN '-' ELSE MAX(productname) END) AS productname,
					(CASE WHEN (MAX(exclusivity_information) IS NULL OR MAX(exclusivity_information) = '\\N') THEN '-' ELSE MAX(exclusivity_information) END) AS exclusivity_information,
					(CASE WHEN (MAX(submission_date) IS NULL OR MAX(submission_date) = '\\N') THEN '-' ELSE MAX(submission_date) END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (MAX(current_assignee) IS NULL OR MAX(current_assignee) = '\\N') THEN '-' ELSE MAX(current_assignee) END) AS current_assignee,
					(CASE WHEN (MAX(patent_title) IS NULL OR MAX(patent_title) = '\\N') THEN '-' ELSE MAX(patent_title) END) AS patent_title
				FROM tbl_master_fa_patent 
				WHERE 
					ndccode IN ($1:csv) 
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N' `;

			db.any(query_sql, [ndc_arr])
			.then(function (data =[]) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_patent_download_request_more_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				SELECT 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (productname IS NULL OR productname = '\\N') THEN '-' ELSE productname END) AS productname,
					(CASE WHEN (exclusivity_information IS NULL OR exclusivity_information = '\\N') THEN '-' ELSE exclusivity_information END) AS exclusivity_information,
					(CASE WHEN (submission_date IS NULL OR submission_date = '\\N') THEN '-' ELSE submission_date END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (current_assignee IS NULL OR current_assignee = '\\N') THEN '-' ELSE current_assignee END) AS current_assignee,
					(CASE WHEN (patent_title IS NULL OR patent_title = '\\N') THEN '-' ELSE patent_title END) AS patent_title
				FROM tbl_master_fa_patent 
				WHERE 
					ndccode IN ($1:csv) 
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N'`;

			db.any(query_sql, [ndc_arr])
			.then(function (data =[]) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//================== US EXCIPENTS ============================//

	find_product_us_excipient_ndc: async (ndc_arr, organization, limit, offset) => {
		return new Promise(function (resolve, reject) {
			let where_clause = '';
			if (organization.length > 0) {
				where_clause += " AND organization IN ($4:csv)";
			}

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization,
				no_of_excepients,
				dosage_form,
				route,
				color,
				shape,
				sizes,
				strength,
				rld 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
				${where_clause} 
			LIMIT $2 OFFSET $3`;

			console.log(query_sql,ndc_arr,organization,limit, offset);
			db.any(query_sql, [ndc_arr, limit, offset, organization])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	count_product_us_excipient_ndc: async (ndc_arr, organization) => {
		return new Promise(function (resolve, reject) {


			let where_clause = '';
			if (organization.length > 0) {
				where_clause += " AND organization IN ($2:csv)";
			}

			let query_sql = 
			`SELECT 
				count(id) as count  
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv) 
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
			${where_clause}`;

			db.any(query_sql, [ndc_arr, organization])
				.then(function (data = []) {
					resolve(data[0].count);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_us_excipient_download_ndc: async (ndc_arr, organization) => {
		return new Promise(function (resolve, reject) {
			let where_clause = '';
			if (organization.length > 0) {
				where_clause += " AND organization IN ($2:csv)";
			}

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization,
				no_of_excepients,
				dosage_form,
				route,
				color,
				shape,
				sizes,
				strength,
				rld 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
				${where_clause}`;

			db.any(query_sql, [ndc_arr, organization])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_product_us_excipient_organization_list_ndc: async (ndc_arr) => {
		return new Promise(function (resolve, reject) {
			query_sql = 
			`SELECT 
				DISTINCT ON (organization) organization 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv) 
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
				AND organization!='' 
				AND organization IS NOT NULL 
				AND organization != '\\N'`;

			db.any(query_sql,[ndc_arr])
			.then(function (data = []) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//================== ALTERNATE EXCIPENTS ============================//

	find_product_alternate_excipient_ndc: async (ndc_code,functional_category,unni,limit,offset) => {
		return new Promise(function (resolve, reject) {
			//console.log(ndc_code,functional_category,unni,limit,offset);
			let query_sql = 
			`SELECT 
				unii, 
				excipient_name, 
				popularity_score, 
				potencydetails,
				similarity_index 
			FROM tbl_master_fa_tablets 
			WHERE 
				LOWER(functional_category) LIKE '%${functional_category.toLowerCase()}%'
				AND LOWER(excipient_name) = ($1)
				AND similarity_index != '0'
				AND similarity_index != '\\N'
			ORDER BY id, popularity_score
			LIMIT $2 OFFSET $3`;
			//console.log(query_sql,unni);
			db.any(query_sql, [unni.toLowerCase(), limit, offset])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	count_product_alternate_excipient_ndc: async (ndc_code,functional_category,unni) => {
		return new Promise(function (resolve, reject) {

			let query_sql = 
			`SELECT 
				COUNT(id) AS count
			FROM tbl_master_fa_tablets 
			WHERE 
				LOWER(functional_category) like '%${functional_category.toLowerCase()}%'
				AND excipient_name = ($1)
				AND similarity_index != '0'
				AND similarity_index != '\\N'`;
			db.any(query_sql, [unni.toLowerCase()])
			.then(function (data = []) {
				if(data.length > 0){
					resolve(data[0].count);
				}
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	find_cosine:async (sim_index) => {
		return new Promise(function (resolve, reject) {
			
			let query_sql = 
			`SELECT ${sim_index} AS index FROM tbl_master_fa_cosine`;
			console.log(query_sql);
			db.any(query_sql)
			.then(function (data){
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},
	find_product_alternate_excipient_download_ndc: async (ndc_arr, organization) => {
		return new Promise(function (resolve, reject) {
			let where_clause = '';
			if (organization.length > 0) {
				where_clause += " AND organization IN ($2:csv)";
			}

			let query_sql = 
			`SELECT 
				ndc_code,
				applicationnumber,
				organization,
				no_of_excepients,
				dosage_form,
				route,
				color,
				shape,
				sizes,
				strength,
				rld 
			FROM tbl_master_fa_product_us 
			WHERE 
				ndc_code IN ($1:csv)
				AND product_name !='' 
				AND product_name IS NOT NULL 
				AND product_name != '\\N' 
				${where_clause}`;

			db.any(query_sql, [ndc_arr, organization])
			.then(function (data) {
				resolve(data)
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});

		});
	},

	//===================================================================//

	product_us: async () => {
		return new Promise(function (resolve, reject) {

			db.any("select CONCAT(id,'_us') AS id, product_name from tbl_master_fa_product_us where true;")
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	product_au: async () => {
		return new Promise(function (resolve, reject) {

			db.any("select CONCAT(id,'_au') AS id, product_name from tbl_master_fa_product_au where true;")
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	product_ema: async () => {
		return new Promise(function (resolve, reject) {

			db.any("select CONCAT(ema_id,'_ema') AS id,productname AS product_name from tbl_master_fa_product_ema where true;")
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	suggest_products_us: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select DISTINCT ON (product_name) product_name as label from tbl_master_fa_product_us where LOWER(product_name) LIKE '" + string.toLowerCase() + "%' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' LIMIT 10";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	//NEED TO CHANGE
	find_product_us: async (string, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ndc_code,applicationnumber,MAX(organization) AS organization from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' GROUP BY (ndc_code,applicationnumber) LIMIT $1 OFFSET $2";
			db.any(query_sql, [limit, offset])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	//NEED TO CHANGE
	find_product_us_download: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ndc_code,applicationnumber,MAX(organization) AS organization from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' GROUP BY (ndc_code,applicationnumber) ";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	//NEED TO CHANGE
	find_product_us_download_request_more: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ndc_code,applicationnumber,MAX(organization) AS organization,MAX(no_of_excepients) AS no_of_excepients,MAX(dosage_form) AS dosage_form,MAX(route) AS route,MAX(color) AS color,MAX(shape) AS shape,MAX(sizes) AS sizes,MAX(strength) AS strength,MAX(rld) AS rld from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' GROUP BY (ndc_code,applicationnumber) ";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	//NEED TO CHANGE
	count_product_us: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(id) as count  from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' GROUP BY (ndc_code,applicationnumber) ";
			db.any(query_sql)
				.then(function (data = []) {
					if(data.length > 0){
						resolve(data.length);
					}else{
						resolve(0);
					}
					
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	suggest_products_drugbank: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select DISTINCT ON (dcname) dcname as label from tbl_master_fa_drug_bank where LOWER(dcname) LIKE '" + string.toLowerCase() + "%' AND dcname !='' AND dcname IS NOT NULL AND dcname != '\\N' LIMIT 10";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_drugbank: async (string, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
			select 
				(CASE WHEN (dcname IS NULL OR dcname = '\\N' ) THEN '-' ELSE dcname END) AS dcname,
				(CASE WHEN (ndccode IS NULL OR ndccode = '\\N' ) THEN '-' ELSE ndccode END) AS ndccode,
				(CASE WHEN (drugbank_id IS NULL OR drugbank_id = '\\N' ) THEN '-' ELSE drugbank_id END) AS drugbank_id,
				(CASE WHEN (productname IS NULL OR productname = '\\N' ) THEN '-' ELSE productname END) AS productname,
				(CASE WHEN (water_sol_x IS NULL OR water_sol_x = '\\N' ) THEN '-' ELSE water_sol_x END) AS water_sol_x,
				(CASE WHEN (logp_x IS NULL OR logp_x = '\\N' ) THEN '-' ELSE logp_x END) AS logp_x,
				(CASE WHEN (pka IS NULL OR pka = '\\N' ) THEN '-' ELSE pka END) AS pka,
				(CASE WHEN (smiles IS NULL OR smiles = '\\N' ) THEN '-' ELSE smiles END) AS smiles,
				(CASE WHEN (casnum IS NULL OR casnum = '\\N' ) THEN '-' ELSE casnum END) AS casnum,
				(CASE WHEN (boiling_point IS NULL OR boiling_point = '\\N' ) THEN '-' ELSE boiling_point END) AS boiling_point,
				(CASE WHEN (hydrophobicity IS NULL OR hydrophobicity = '\\N' ) THEN '-' ELSE hydrophobicity END) AS hydrophobicity,
				(CASE WHEN (isoelectric_point IS NULL OR isoelectric_point = '\\N' ) THEN '-' ELSE isoelectric_point END) AS isoelectric_point,
				(CASE WHEN (melting_point IS NULL OR melting_point = '\\N' ) THEN '-' ELSE melting_point END) AS melting_point,
				(CASE WHEN (molecular_formula_x IS NULL OR molecular_formula_x = '\\N' ) THEN '-' ELSE molecular_formula_x END) AS molecular_formula_x,
				(CASE WHEN (molecular_weight_x IS NULL OR molecular_weight_x = '\\N' ) THEN '-' ELSE molecular_weight_x END) AS molecular_weight_x,
				(CASE WHEN (radioactivity IS NULL OR radioactivity = '\\N' ) THEN '-' ELSE radioactivity END) AS radioactivity,
				(CASE WHEN (caco2_permeability IS NULL OR caco2_permeability = '\\N' ) THEN '-' ELSE caco2_permeability END) AS caco2_permeability,
				(CASE WHEN (logs_x IS NULL OR logs_x = '\\N' ) THEN '-' ELSE logs_x END) AS logs_x,
				(CASE WHEN (description IS NULL OR description = '\\N' ) THEN '-' ELSE description END) AS description,
				(CASE WHEN (indication IS NULL OR indication = '\\N' ) THEN '-' ELSE indication END) AS indication,
				(CASE WHEN (average_mass IS NULL OR average_mass = '\\N' ) THEN '-' ELSE average_mass END) AS average_mass,
				(CASE WHEN (monoisotopic_mass IS NULL OR monoisotopic_mass = '\\N' ) THEN '-' ELSE monoisotopic_mass END) AS monoisotopic_mass,
				(CASE WHEN (state IS NULL OR state = '\\N' ) THEN '-' ELSE state END) AS state,
				(CASE WHEN (pharmacodynamics IS NULL OR pharmacodynamics = '\\N' ) THEN '-' ELSE pharmacodynamics END) AS pharmacodynamics,
				(CASE WHEN (clearance IS NULL OR clearance = '\\N' ) THEN '-' ELSE clearance END) AS clearance,
				(CASE WHEN (mechanism_of_action IS NULL OR mechanism_of_action = '\\N' ) THEN '-' ELSE mechanism_of_action END) AS mechanism_of_action,
				(CASE WHEN (toxicity IS NULL OR toxicity = '\\N' ) THEN '-' ELSE toxicity END) AS toxicity,
				(CASE WHEN (metabolism IS NULL OR metabolism = '\\N' ) THEN '-' ELSE metabolism END) AS metabolism,
				(CASE WHEN (absorption IS NULL OR absorption = '\\N' ) THEN '-' ELSE absorption END) AS absorption,
				(CASE WHEN (half_life IS NULL OR half_life = '\\N' ) THEN '-' ELSE half_life END) AS half_life,
				(CASE WHEN (protein_binding IS NULL OR protein_binding = '\\N' ) THEN '-' ELSE protein_binding END) AS protein_binding,
				(CASE WHEN (route_of_elimination IS NULL OR route_of_elimination = '\\N' ) THEN '-' ELSE route_of_elimination END) AS route_of_elimination,
				(CASE WHEN (volume_of_distribution IS NULL OR volume_of_distribution = '\\N' ) THEN '-' ELSE volume_of_distribution END) AS volume_of_distribution,
				(CASE WHEN (h_bond_acceptor_count IS NULL OR h_bond_acceptor_count = '\\N' ) THEN '-' ELSE h_bond_acceptor_count END) AS h_bond_acceptor_count,
				(CASE WHEN (h_bond_donor_ch_bond_donor_countount IS NULL OR h_bond_donor_ch_bond_donor_countount = '\\N' ) THEN '-' ELSE h_bond_donor_ch_bond_donor_countount END) AS h_bond_donor_ch_bond_donor_countount,
				(CASE WHEN (iupac_name IS NULL OR iupac_name = '\\N' ) THEN '-' ELSE iupac_name END) AS iupac_name,
				(CASE WHEN (molecular_formula_y IS NULL OR molecular_formula_y = '\\N' ) THEN '-' ELSE molecular_formula_y END) AS molecular_formula_y,
				(CASE WHEN (molecular_weight_y IS NULL OR molecular_weight_y = '\\N' ) THEN '-' ELSE molecular_weight_y END) AS molecular_weight_y,
				(CASE WHEN (physiological_charge IS NULL OR physiological_charge = '\\N' ) THEN '-' ELSE physiological_charge END) AS physiological_charge,
				(CASE WHEN (polar_surface_area_psa IS NULL OR polar_surface_area_psa = '\\N' ) THEN '-' ELSE polar_surface_area_psa END) AS polar_surface_area_psa,
				(CASE WHEN (polarizability IS NULL OR polarizability = '\\N' ) THEN '-' ELSE polarizability END) AS polarizability,
				(CASE WHEN (refractivity IS NULL OR refractivity = '\\N' ) THEN '-' ELSE refractivity END) AS refractivity,
				(CASE WHEN (rotatable_bond_count IS NULL OR rotatable_bond_count = '\\N' ) THEN '-' ELSE rotatable_bond_count END) AS rotatable_bond_count,
				(CASE WHEN (water_solubility_y IS NULL OR water_solubility_y = '\\N' ) THEN '-' ELSE water_solubility_y END) AS water_solubility_y,
				(CASE WHEN (logp_y IS NULL OR logp_y = '\\N' ) THEN '-' ELSE logp_y END) AS logp_y,
				(CASE WHEN (logs_y IS NULL OR logs_y = '\\N' ) THEN '-' ELSE logs_y END) AS logs_y,
				(CASE WHEN (pka_strongest_acidic IS NULL OR pka_strongest_acidic = '\\N' ) THEN '-' ELSE pka_strongest_acidic END) AS pka_strongest_acidic,
				(CASE WHEN (pka_strongest_basic IS NULL OR pka_strongest_basic = '\\N' ) THEN '-' ELSE pka_strongest_basic END) AS pka_strongest_basic,
				(CASE WHEN (dc_direct_parent IS NULL OR dc_direct_parent = '\\N' ) THEN '-' ELSE dc_direct_parent END) AS dc_direct_parent,
				(CASE WHEN (pharm_classes IS NULL OR pharm_classes = '\\N') THEN '-' ELSE pharm_classes END) AS pharm_classes
				 
			from tbl_master_fa_drug_bank where LOWER(dcname)= '${string.toLowerCase()}' AND dcname !='' AND dcname IS NOT NULL AND dcname != '\\N' LIMIT 1`;
			db.any(query_sql, [limit, offset])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	find_product_drugbank_download: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = `select 
				(CASE WHEN (dcname IS NULL OR dcname = '\\N' ) THEN '-' ELSE dcname END) AS dcname,
				(CASE WHEN (ndccode IS NULL OR ndccode = '\\N' ) THEN '-' ELSE ndccode END) AS ndccode,
				(CASE WHEN (drugbank_id IS NULL OR drugbank_id = '\\N' ) THEN '-' ELSE drugbank_id END) AS drugbank_id,
				(CASE WHEN (productname IS NULL OR productname = '\\N' ) THEN '-' ELSE productname END) AS productname,
				(CASE WHEN (water_sol_x IS NULL OR water_sol_x = '\\N' ) THEN '-' ELSE water_sol_x END) AS water_sol_x,
				(CASE WHEN (logp_x IS NULL OR logp_x = '\\N' ) THEN '-' ELSE logp_x END) AS logp_x,
				(CASE WHEN (pka IS NULL OR pka = '\\N' ) THEN '-' ELSE pka END) AS pka,
				(CASE WHEN (smiles IS NULL OR smiles = '\\N' ) THEN '-' ELSE smiles END) AS smiles,
				(CASE WHEN (casnum IS NULL OR casnum = '\\N' ) THEN '-' ELSE casnum END) AS casnum,
				(CASE WHEN (boiling_point IS NULL OR boiling_point = '\\N' ) THEN '-' ELSE boiling_point END) AS boiling_point,
				(CASE WHEN (hydrophobicity IS NULL OR hydrophobicity = '\\N' ) THEN '-' ELSE hydrophobicity END) AS hydrophobicity,
				(CASE WHEN (isoelectric_point IS NULL OR isoelectric_point = '\\N' ) THEN '-' ELSE isoelectric_point END) AS isoelectric_point,
				(CASE WHEN (melting_point IS NULL OR melting_point = '\\N' ) THEN '-' ELSE melting_point END) AS melting_point,
				(CASE WHEN (molecular_formula_x IS NULL OR molecular_formula_x = '\\N' ) THEN '-' ELSE molecular_formula_x END) AS molecular_formula_x,
				(CASE WHEN (molecular_weight_x IS NULL OR molecular_weight_x = '\\N' ) THEN '-' ELSE molecular_weight_x END) AS molecular_weight_x,
				(CASE WHEN (radioactivity IS NULL OR radioactivity = '\\N' ) THEN '-' ELSE radioactivity END) AS radioactivity,
				(CASE WHEN (caco2_permeability IS NULL OR caco2_permeability = '\\N' ) THEN '-' ELSE caco2_permeability END) AS caco2_permeability,
				(CASE WHEN (logs_x IS NULL OR logs_x = '\\N' ) THEN '-' ELSE logs_x END) AS logs_x,
				(CASE WHEN (description IS NULL OR description = '\\N' ) THEN '-' ELSE description END) AS description,
				(CASE WHEN (indication IS NULL OR indication = '\\N' ) THEN '-' ELSE indication END) AS indication,
				(CASE WHEN (average_mass IS NULL OR average_mass = '\\N' ) THEN '-' ELSE average_mass END) AS average_mass,
				(CASE WHEN (monoisotopic_mass IS NULL OR monoisotopic_mass = '\\N' ) THEN '-' ELSE monoisotopic_mass END) AS monoisotopic_mass,
				(CASE WHEN (state IS NULL OR state = '\\N' ) THEN '-' ELSE state END) AS state,
				(CASE WHEN (pharmacodynamics IS NULL OR pharmacodynamics = '\\N' ) THEN '-' ELSE pharmacodynamics END) AS pharmacodynamics,
				(CASE WHEN (clearance IS NULL OR clearance = '\\N' ) THEN '-' ELSE clearance END) AS clearance,
				(CASE WHEN (mechanism_of_action IS NULL OR mechanism_of_action = '\\N' ) THEN '-' ELSE mechanism_of_action END) AS mechanism_of_action,
				(CASE WHEN (toxicity IS NULL OR toxicity = '\\N' ) THEN '-' ELSE toxicity END) AS toxicity,
				(CASE WHEN (metabolism IS NULL OR metabolism = '\\N' ) THEN '-' ELSE metabolism END) AS metabolism,
				(CASE WHEN (absorption IS NULL OR absorption = '\\N' ) THEN '-' ELSE absorption END) AS absorption,
				(CASE WHEN (half_life IS NULL OR half_life = '\\N' ) THEN '-' ELSE half_life END) AS half_life,
				(CASE WHEN (protein_binding IS NULL OR protein_binding = '\\N' ) THEN '-' ELSE protein_binding END) AS protein_binding,
				(CASE WHEN (route_of_elimination IS NULL OR route_of_elimination = '\\N' ) THEN '-' ELSE route_of_elimination END) AS route_of_elimination,
				(CASE WHEN (volume_of_distribution IS NULL OR volume_of_distribution = '\\N' ) THEN '-' ELSE volume_of_distribution END) AS volume_of_distribution,
				(CASE WHEN (h_bond_acceptor_count IS NULL OR h_bond_acceptor_count = '\\N' ) THEN '-' ELSE h_bond_acceptor_count END) AS h_bond_acceptor_count,
				(CASE WHEN (h_bond_donor_ch_bond_donor_countount IS NULL OR h_bond_donor_ch_bond_donor_countount = '\\N' ) THEN '-' ELSE h_bond_donor_ch_bond_donor_countount END) AS h_bond_donor_ch_bond_donor_countount,
				(CASE WHEN (iupac_name IS NULL OR iupac_name = '\\N' ) THEN '-' ELSE iupac_name END) AS iupac_name,
				(CASE WHEN (molecular_formula_y IS NULL OR molecular_formula_y = '\\N' ) THEN '-' ELSE molecular_formula_y END) AS molecular_formula_y,
				(CASE WHEN (molecular_weight_y IS NULL OR molecular_weight_y = '\\N' ) THEN '-' ELSE molecular_weight_y END) AS molecular_weight_y,
				(CASE WHEN (physiological_charge IS NULL OR physiological_charge = '\\N' ) THEN '-' ELSE physiological_charge END) AS physiological_charge,
				(CASE WHEN (polar_surface_area_psa IS NULL OR polar_surface_area_psa = '\\N' ) THEN '-' ELSE polar_surface_area_psa END) AS polar_surface_area_psa,
				(CASE WHEN (polarizability IS NULL OR polarizability = '\\N' ) THEN '-' ELSE polarizability END) AS polarizability,
				(CASE WHEN (refractivity IS NULL OR refractivity = '\\N' ) THEN '-' ELSE refractivity END) AS refractivity,
				(CASE WHEN (rotatable_bond_count IS NULL OR rotatable_bond_count = '\\N' ) THEN '-' ELSE rotatable_bond_count END) AS rotatable_bond_count,
				(CASE WHEN (water_solubility_y IS NULL OR water_solubility_y = '\\N' ) THEN '-' ELSE water_solubility_y END) AS water_solubility_y,
				(CASE WHEN (logp_y IS NULL OR logp_y = '\\N' ) THEN '-' ELSE logp_y END) AS logp_y,
				(CASE WHEN (logs_y IS NULL OR logs_y = '\\N' ) THEN '-' ELSE logs_y END) AS logs_y,
				(CASE WHEN (pka_strongest_acidic IS NULL OR pka_strongest_acidic = '\\N' ) THEN '-' ELSE pka_strongest_acidic END) AS pka_strongest_acidic,
				(CASE WHEN (pka_strongest_basic IS NULL OR pka_strongest_basic = '\\N' ) THEN '-' ELSE pka_strongest_basic END) AS pka_strongest_basic,
				(CASE WHEN (dc_direct_parent IS NULL OR dc_direct_parent = '\\N' ) THEN '-' ELSE dc_direct_parent END) AS dc_direct_parent,
				(CASE WHEN (pharm_classes IS NULL OR pharm_classes = '\\N') THEN '-' ELSE pharm_classes END) AS pharm_classes
			from tbl_master_fa_drug_bank where LOWER(dcname)= '${string.toLowerCase()}' AND dcname !='' AND dcname IS NOT NULL AND dcname != '\\N' LIMIT 1`;

			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	count_product_drugbank: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(id) as count  from tbl_master_fa_drug_bank where LOWER(dcname)= '" + string.toLowerCase() + "' AND dcname !='' AND dcname IS NOT NULL AND dcname != '\\N'";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	suggest_products_patent: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select DISTINCT ON (productname) productname as product_name from tbl_master_fa_patent where (LOWER(key_words) LIKE '%" + string.toLowerCase() + "%' OR LOWER(productname) LIKE '" + string.toLowerCase() + "%' ) AND productname !='' AND productname IS NOT NULL AND productname != '\\N' LIMIT 10";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_patent: async (string, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				select 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (MAX(productname) IS NULL OR MAX(productname) = '\\N') THEN '-' ELSE MAX(productname) END) AS productname,
					(CASE WHEN (MAX(exclusivity_information) IS NULL OR MAX(exclusivity_information) = '\\N') THEN '-' ELSE MAX(exclusivity_information) END) AS exclusivity_information,
					(CASE WHEN (MAX(submission_date) IS NULL OR MAX(submission_date) = '\\N') THEN '-' ELSE MAX(submission_date) END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (MAX(current_assignee) IS NULL OR MAX(current_assignee) = '\\N') THEN '-' ELSE MAX(current_assignee) END) AS current_assignee,
					(CASE WHEN (MAX(patent_title) IS NULL OR MAX(patent_title) = '\\N') THEN '-' ELSE MAX(patent_title) END) AS patent_title,
					(CASE WHEN (MAX(global_dossier) IS NULL OR MAX(global_dossier) = '\\N') THEN '-' ELSE MAX(global_dossier) END) AS global_dossier
				from tbl_master_fa_patent 
				where 
					LOWER(productname)= ($3) 
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N' 
				GROUP BY (patent_number,patentexpiry,dp,ds)
				LIMIT $1 OFFSET $2 `;

			//query_sql = "select patent_number,productname,exclusivity_information,submission_date,patentexpiry,dp,ds,current_assignee,patent_title,key_words,global_dossier from tbl_master_fa_patent where LOWER(productname)= '"+string.toLowerCase()+"' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' LIMIT $1 OFFSET $2 GROUP BY";
			db.any(query_sql, [limit, offset, string.toLowerCase()])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_patent_download: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				select 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (MAX(productname) IS NULL OR MAX(productname) = '\\N') THEN '-' ELSE MAX(productname) END) AS productname,
					(CASE WHEN (MAX(exclusivity_information) IS NULL OR MAX(exclusivity_information) = '\\N') THEN '-' ELSE MAX(exclusivity_information) END) AS exclusivity_information,
					(CASE WHEN (MAX(submission_date) IS NULL OR MAX(submission_date) = '\\N') THEN '-' ELSE MAX(submission_date) END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (MAX(current_assignee) IS NULL OR MAX(current_assignee) = '\\N') THEN '-' ELSE MAX(current_assignee) END) AS current_assignee,
					(CASE WHEN (MAX(patent_title) IS NULL OR MAX(patent_title) = '\\N') THEN '-' ELSE MAX(patent_title) END) AS patent_title,
					(CASE WHEN (MAX(global_dossier) IS NULL OR MAX(global_dossier) = '\\N') THEN '-' ELSE MAX(global_dossier) END) AS global_dossier
				from tbl_master_fa_patent 
				where 
					LOWER(productname)= ($1) 
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N' 
				GROUP BY (patent_number,patentexpiry,dp,ds) `;

			db.any(query_sql, [string.toLowerCase()])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_patent_download_request_more: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = `
				select 
					(CASE WHEN (patent_number IS NULL OR patent_number = '\\N' ) THEN '-' ELSE patent_number END) AS patent_number,
					(CASE WHEN (productname IS NULL OR productname = '\\N') THEN '-' ELSE productname END) AS productname,
					(CASE WHEN (exclusivity_information IS NULL OR exclusivity_information = '\\N') THEN '-' ELSE exclusivity_information END) AS exclusivity_information,
					(CASE WHEN (submission_date IS NULL OR submission_date = '\\N') THEN '-' ELSE submission_date END) AS submission_date,
					(CASE WHEN (patentexpiry IS NULL OR patentexpiry = '\\N') THEN '-' ELSE patentexpiry END) AS patentexpiry,
					(CASE WHEN (dp IS NULL OR dp = '\\N') THEN '-' ELSE dp END) AS dp,
					(CASE WHEN (ds IS NULL OR ds = '\\N') THEN '-' ELSE ds END) AS ds,
					(CASE WHEN (current_assignee IS NULL OR current_assignee = '\\N') THEN '-' ELSE current_assignee END) AS current_assignee,
					(CASE WHEN (patent_title IS NULL OR patent_title = '\\N') THEN '-' ELSE patent_title END) AS patent_title,
					(CASE WHEN (global_dossier IS NULL OR global_dossier = '\\N') THEN '-' ELSE global_dossier END) AS global_dossier
				from tbl_master_fa_patent 
				where 
					LOWER(productname)= ($1) 
					AND productname !='' 
					AND productname IS NOT NULL 
					AND productname != '\\N'`;

			db.any(query_sql, [string.toLowerCase()])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	count_product_patent: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(patent_number) as count from tbl_master_fa_patent where LOWER(productname)= '" + string.toLowerCase() + "' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' GROUP BY (patent_number,patentexpiry,dp,ds) ";
			db.any(query_sql)
				.then(function (data) {
					console.log('data', data);
					if (data && data.length > 0) {
						resolve(data.length);
					} else {
						resolve(0);
					}
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	suggest_products_ema: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select DISTINCT ON (productname) productname as label from tbl_master_fa_product_ema where LOWER(productname) LIKE '" + string.toLowerCase() + "%' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' LIMIT 10";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_ema: async (string, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select productname,ndc_code,product_number,authorisation_status,therapeutic_area,generic,biosimilar,additional_monitoring,orphan_drug,marketing_date,pharma_class,company_name from tbl_master_fa_product_ema where LOWER(productname)= '" + string.toLowerCase() + "' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' LIMIT $1 OFFSET $2";
			db.any(query_sql, [limit, offset])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_ema_download: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select productname,ndc_code,product_number,authorisation_status,therapeutic_area,generic,biosimilar,additional_monitoring,orphan_drug,marketing_date,pharma_class,company_name from tbl_master_fa_product_ema where LOWER(productname)= '" + string.toLowerCase() + "' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' ";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_ema_download_request_more: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select productname,ndc_code,product_number,authorisation_status,therapeutic_area,generic,biosimilar,additional_monitoring,orphan_drug,marketing_date,pharma_class,company_name,published_date,indication from tbl_master_fa_product_ema where LOWER(productname)= '" + string.toLowerCase() + "' AND productname !='' AND productname IS NOT NULL AND productname != '\\N' ";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	count_product_ema: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(ema_id) as count from tbl_master_fa_product_ema where LOWER(productname)= '" + string.toLowerCase() + "' AND productname !='' AND productname IS NOT NULL AND productname != '\\N'";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	suggest_products_au: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select DISTINCT ON (product_name) product_name as label from tbl_master_fa_product_au where LOWER(product_name) LIKE '" + string.toLowerCase() + "%' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' LIMIT 10";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_au: async (string, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select product_name,ndc_code,ndc_name,submission_number,sponsor,submission_type,authorization_status,auspar_date,publication_date from tbl_master_fa_product_au where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' LIMIT $1 OFFSET $2";
			//console.log(query_sql);
			db.any(query_sql, [limit, offset])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	find_product_au_download: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select product_name,ndc_code,ndc_name,submission_number,sponsor,submission_type,authorization_status,auspar_date,publication_date from tbl_master_fa_product_au where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' ";

			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	count_product_au: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(id) as count  from tbl_master_fa_product_au where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N'";
			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_us_excipient: async (str, ndc_code, organization, limit, offset) => {
		return new Promise(function (resolve, reject) {

			let addtional = '';
			if (ndc_code.length > 0) {
				addtional += " AND ndc_code IN ($3:csv)";
			}
			if (organization.length > 0) {
				addtional += " AND organization IN ($4:csv)";
			}

			query_sql = "select ndc_code, MAX(applicationnumber) AS applicationnumber,MAX(organization) AS organization,MAX(no_of_excepients) AS no_of_excepients,MAX(dosage_form) AS dosage_form,MAX(route) AS route,MAX(color) AS color,MAX(shape) AS shape,MAX(sizes) AS sizes,MAX(strength) AS strength,MAX(rld) AS rld from tbl_master_fa_product_us where LOWER(product_name) = '" + str.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' " + addtional + " GROUP BY ndc_code LIMIT $1 OFFSET $2";

			console.log(query_sql,limit, offset);
			db.any(query_sql, [limit, offset, ndc_code, organization])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	find_product_us_excipient_download: async (str, ndc_code, organization) => {
		return new Promise(function (resolve, reject) {

			let addtional = '';
			if (ndc_code.length > 0) {
				addtional += " AND ndc_code IN ($1:csv)";
			}
			if (organization.length > 0) {
				addtional += " AND organization IN ($2:csv)";
			}

			query_sql = "select ndc_code, MAX(applicationnumber) AS applicationnumber,MAX(organization) AS organization,MAX(no_of_excepients) AS no_of_excepients,MAX(dosage_form) AS dosage_form,MAX(route) AS route,MAX(color) AS color,MAX(shape) AS shape,MAX(sizes) AS sizes,MAX(strength) AS strength,MAX(rld) AS rld from tbl_master_fa_product_us where LOWER(product_name)= '" + str.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' " + addtional + " GROUP BY ndc_code ";

			db.any(query_sql, [ndc_code, organization])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	count_product_us_excipient: async (string, ndc_code, organization) => {
		return new Promise(function (resolve, reject) {

			let addtional = '';
			if (ndc_code.length > 0) {
				addtional += " AND ndc_code IN ($1:csv)";
			}
			if (organization.length > 0) {
				addtional += " AND organization IN ($2:csv)";
			}

			query_sql = "select count(id) as count  from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' "+addtional+" GROUP BY ndc_code";
			db.any(query_sql, [ndc_code, organization])
				.then(function (data = []) {
					if(data.length > 0){
						resolve(data.length);
					}else{
						resolve(0);
					}
					
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	find_product_us_excipient_ndc_list: async (str) => {
		return new Promise(function (resolve, reject) {
			query_sql = "select DISTINCT ON (ndc_code) ndc_code from tbl_master_fa_product_us where LOWER(product_name)= '" + str.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' AND ndc_code!='' AND ndc_code IS NOT NULL";

			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	find_product_us_excipient_organization_list: async (str) => {
		return new Promise(function (resolve, reject) {
			query_sql = "select DISTINCT ON (organization) organization from tbl_master_fa_product_us where LOWER(product_name)= '" + str.toLowerCase() + "' AND product_name !='' AND product_name IS NOT NULL AND product_name != '\\N' AND organization!='' AND organization IS NOT NULL AND organization != '\\N'";

			db.any(query_sql)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	get_task_enquiry_list: async (customer_id, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select tbl_fa_task_enquiry.id, tbl_master_fa_enquiry.enquiry_name, tbl_fa_task_enquiry.customer_id, tbl_fa_task_enquiry.product_name,to_char( tbl_fa_task_enquiry.date_added, 'YYYY-MM-DD HH:ii:ss') as date_added, tbl_fa_task_enquiry.processed from tbl_fa_task_enquiry LEFT JOIN tbl_master_fa_enquiry ON tbl_master_fa_enquiry.id = tbl_fa_task_enquiry.enquiry_type where customer_id = $1 ORDER BY tbl_fa_task_enquiry.id DESC LIMIT $2 OFFSET $3";
			console.log(query_sql);
			db.any(query_sql, [customer_id, limit, offset])
				.then(function (data) {
					console.log(data)
					resolve(data)
				})
				.catch(function (err) {
					console.log(err);
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	count_task_enquiry: async (customer_id) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select count(id) as count from tbl_fa_task_enquiry where customer_id = $1 ";
			db.any(query_sql, [customer_id])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_task_enquiry_details: async (enquiry_id) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select enquiry_type, customer_id, product_name from tbl_fa_task_enquiry where id = $1";
			//console.log(query_sql);
			db.any(query_sql, [enquiry_id])
				.then(function (data) {
					resolve(data[0])
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	check_task_enquiry_details: async (enquiry_id,customer_id) => {
		return new Promise(function (resolve, reject) {

			query_sql = "SELECT id FROM tbl_fa_task_enquiry WHERE id = $1 AND customer_id = ($2) ";
			//console.log(query_sql);
			db.any(query_sql, [enquiry_id,customer_id])
				.then(function (data = []) {
					if(data.length > 0){
						resolve(true);
					}else{
						resolve(false);
					}
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	ndc_code_exist: async (ndc_code) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ndc_code from tbl_master_fa_unii where ndc_code = $1";
			db.any(query_sql, [ndc_code])
				.then(function (data) {
					if (data.length > 0)
						resolve({ success: true });
					else
						resolve({ success: false });
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	get_excipients_list_all: async (ndc_code) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ndc_code, unii, product_name, excipientname, dailydosagelimit, alternate_excipients, functional_category, incompatibilities from tbl_master_fa_unii where ndc_code = $1";
			//console.log(query_sql);
			db.any(query_sql, [ndc_code])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	get_alternate_excipients_list: async (ndc_code) => {
		return new Promise(function (resolve, reject) {

			query_sql = "SELECT excipientname, functional_category FROM tbl_master_fa_unii where ndc_code = $1";
			//console.log(query_sql);
			db.any(query_sql, [ndc_code])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},

	get_excipients_list: async (ndc_code, limit, offset) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select MAX(ndc_code) AS ndc_code, MAX(unii) AS unii, product_name, excipientname, MAX(dailydosagelimit) AS dailydosagelimit, MAX(alternate_excipients) AS alternate_excipients, MAX(functional_category) AS functional_category, MAX(incompatibilities) AS incompatibilities from tbl_master_fa_unii where ndc_code = $1 GROUP BY (product_name,excipientname) LIMIT $2 OFFSET $3";
			//console.log(query_sql);
			db.any(query_sql, [ndc_code, limit, offset])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_excipients_count: async (ndc_code) => {
		return new Promise(function (resolve, reject) {

			query_sql = "SELECT COUNT(ndc_code) AS count FROM tbl_master_fa_unii WHERE ndc_code = $1 GROUP BY (product_name,excipientname)";
			//console.log(query_sql);
			db.any(query_sql, [ndc_code])
				.then(function (data = []) {
					if(data.length > 0){
						resolve(data.length);
					}else{
						resolve(0);
					}
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_productus: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select id from tbl_master_fa_product_us where LOWER(product_name)= '" + string.toLowerCase() + "' ";
			//console.log(query_sql);			
			db.any(query_sql)
				.then(function (data) {
					resolve(data[0])
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_productema: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select ema_id from tbl_master_fa_product_ema where LOWER(productname)= '" + string.toLowerCase() + "' ";
			//console.log(query_sql);			
			db.any(query_sql)
				.then(function (data) {
					resolve(data[0])
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	get_patent: async (string) => {
		return new Promise(function (resolve, reject) {

			query_sql = "select id from tbl_master_fa_patent where LOWER(productname)= '" + string.toLowerCase() + "' ";
			//console.log(query_sql);			
			db.any(query_sql)
				.then(function (data) {
					resolve(data[0])
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	request_more_info: async (obj) => {
		return new Promise(function (resolve, reject) {

			db.one('INSERT INTO tbl_fa_task_enquiry(enquiry_type,customer_id,product_name,comment,date_added,due_date,processed) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING id', [obj.enquiry_type, obj.customer_id, obj.product_name, obj.comment, obj.date_added, obj.due_date, obj.processed])
				.then(function (data) {
					resolve(data.id)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});
	},
	update_request_more_info: async (arr) => {
		return new Promise(function (resolve, reject) {

			db.result('UPDATE tbl_fa_task_enquiry set file_name=($1),file_path=($2) where id=($3)', arr, r => r.rowCount)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	enquiry_details_file :async (enqiry_id) => {

		return new Promise(function(resolve, reject) {

			db.any(`SELECT file_path AS new_file_name,file_name AS actual_file_name FROM tbl_fa_task_enquiry WHERE id=($1)`,[enqiry_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		});
	},
	enquiry_details: async (enquiry_id) => {
		return new Promise(function (resolve, reject) {

			query_sql = "SELECT *,TO_CHAR(date_added,'dd/mm/yyyy HH12:MI AM') AS date_added_converted FROM tbl_fa_task_enquiry WHERE id = ($1)";
			db.any(query_sql, [enquiry_id])
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	}

}
