const db =require('../configuration/dbConn');
const common = require('../controllers/common');
const Config = require('../configuration/config');

module.exports = {
	findByUsername: async (username)=>{
		return new Promise(function(resolve, reject) {
			db.any("select tour_counter,tour_counter_new,tour_counter_closed_task,multi_lang_access,new_feature,new_feature_send_mail,new_feature_fa,email,profile_pic,language_code,phone_no,customer_id,password,first_name,last_name,to_char(date_added, 'Mon YYYY') as date_added from tbl_customer where LOWER(email)=LOWER($1) and status='1'",[
	        username])
		    .then(function (data) { 
		    	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	}, 
	findByUsernameQA: async (username)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT id,password,first_name,last_name,to_char(date_added, 'Mon YYYY') as date_added FROM tbl_qa_login WHERE LOWER(email)=LOWER($1) AND status='1'",[username])
		    .then(function (data) {
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_customer_language: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			db.one('SELECT language_code FROM tbl_customer WHERE customer_id = ($1)', [customer_id])
				.then(function (data) {
					resolve(data.language_code)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	validate_product: async (product_id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select  product_id, product_name from tbl_master_product WHERE product_id = ($1)',[product_id])
			.then(function (data) {
				resolve(data);
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			});
		}); 
	},
	login_log : async (login_attempt)=>{
		return new Promise(function(resolve, reject) {
		var sql = 
		`INSERT INTO 
		tbl_customers_login_logs(
			customer_id,login_date,ip_address,customer_type
		) 
		VALUES($1,$2,$3,$4) 
		RETURNING log_id`;
		db.one(sql,[login_attempt.customer_id,login_attempt.login_date,login_attempt.ip_address,login_attempt.customer_type])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	get_user: async (id)=>{
		return new Promise(function(resolve, reject) {

			db.any("Select tbl_master_company.company_name,language_code,profile_pic,first_name,last_name,email,phone_no,new_feature,new_feature_send_mail,new_feature_fa,tour_counter,tour_counter_new,tour_counter_closed_task,multi_lang_access,country_id,profile_pic,tbl_customer.company_id,role_type,dnd,to_char(tbl_customer.date_added, 'Mon YYYY') as date_added,status,ignore_spoc from tbl_customer LEFT JOIN tbl_master_company ON  tbl_customer.company_id = tbl_master_company.company_id where customer_id=($1) and ((status='1' and activated ='1') OR status='3')",[id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_parent_company: async (company_id)=>{
		return new Promise(function(resolve, reject) {

			if(company_id && company_id > 0){
				var sql = `
				SELECT parent_id 
				FROM tbl_master_company
				WHERE company_id=($1)`;

				db.any(sql,[company_id])
					.then(function (data) {
					if(data && data[0].parent_id > 0){
						resolve(data[0].parent_id);
					}else{
						resolve(0);
					}
					})
					.catch(function (err) {
						var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
					});
			}else{
				resolve(0);
			}

		}); 

	},
	get_child_company: async (parent_company_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT company_id 
			FROM tbl_master_company
			WHERE parent_id=($1)`;

			db.any(sql,[parent_company_id])
		    .then(function (data) {
				resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_user_company: async (company_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT customer_id 
			FROM tbl_customer
			WHERE company_id IN ($1:csv) 
			AND status=1`;

			db.any(sql,[company_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_user_role: async (customer_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT role_id 
			FROM tbl_customer_role 
			WHERE customer_id=($1) 
			AND status=1 `;

			db.any(sql,[customer_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_user_company_role: async (company_id,role_arr)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT DISTINCT(C.customer_id) 
			FROM tbl_customer AS C 
			LEFT JOIN tbl_customer_role AS ROLE
			ON C.customer_id = ROLE.customer_id 
			WHERE C.company_id IN ($1:csv)
			AND ROLE.role_id IN ($2:csv)
			AND ROLE.status = 1
			AND C.status=1`;

			db.any(sql,[company_id,role_arr])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	insert_cc_customer : async (cc_cust)=>{
		return new Promise(function(resolve, reject) {

			db.one('INSERT INTO tbl_cc_customers (customer_id,task_id,status) VALUES ($1,$2,$3) RETURNING ccc_id',[cc_cust.customer_id,cc_cust.task_id,cc_cust.status])
		    .then(function (data) { 
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	get_cc_cust_company : async (company_id,customer_id) =>{
		return new Promise(function(resolve, reject) {
			// status 
			var sql = 
				`SELECT 
					C.customer_id::INTEGER,
					C.first_name,
					C.last_name,
					COMP.company_name
				FROM tbl_customer AS C 
				LEFT JOIN tbl_master_company AS COMP
				ON C.company_id = COMP.company_id
				WHERE ((C.status = 1 
				    AND C.activated  = 1) OR C.status = 3)
					AND C.company_id IN ($1:csv)
					AND C.customer_id != ($2)
				ORDER BY COMP.company_id,C.first_name ASC`;

			db.any(sql,[company_id,customer_id])
			.then(function (data) {
					resolve(data);
			})
			.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			});

		});
	},
	list_cc_customer : async (task_id)=>{
		return new Promise(function(resolve, reject) {
			var sql = 
				`SELECT 
					tbl_customer.customer_id::INTEGER,
					tbl_customer.first_name,
					tbl_customer.last_name,
					tbl_customer.email,
					tbl_customer.status 
				FROM tbl_customer 
				LEFT JOIN tbl_cc_customers 
					ON tbl_customer.customer_id = tbl_cc_customers.customer_id  
				WHERE ((tbl_customer.status = 1 AND tbl_customer.activated = 1) OR tbl_customer.status = 3)  
					AND tbl_cc_customers.task_id = ($1)
					AND tbl_cc_customers.status = 1
				ORDER BY tbl_customer.customer_id DESC`;

			db.any(sql,[task_id])
			.then(function (data) {
					resolve(data);
			})
			.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			});

		}); 

	},
	update_customer: async (userObj,userId)=>{
		return new Promise(function(resolve, reject) {
			if(userObj.profile_pic != null)
			{
				db.result('UPDATE tbl_customer set profile_pic=($2) where customer_id=($1)',[userId,userObj.profile_pic],r => r.rowCount)
				.then(function (data) {
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}	
			else
			{
				/* 'UPDATE tbl_customer set phone_no=($2),password=($3),country_id=($4) where customer_id=($1)',[userId,userObj.phone_no,userObj.password,userObj.country_id],r => r.rowCount */
				db.result('UPDATE tbl_customer set phone_no=($2),password=($3),country_id=($4),dnd=($5),language_code=($6) where customer_id=($1)',[userId,userObj.phone_no,userObj.password,userObj.country_id,userObj.dnd,userObj.language_code],r => r.rowCount)
				.then(function (data) {
				resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
			}
		    

		}); 

	},
	update_customer_language: async (lang,userId)=>{
		return new Promise(function(resolve, reject) {
			db.result('UPDATE tbl_customer set language_code=($1) where customer_id=($2)',[lang,userId],r => r.rowCount)
			.then(function (data) {
				resolve(data)
			}).catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},
	update_feature: async (customer_id)=>{
		return new Promise(function(resolve, reject) {
			db.result('UPDATE tbl_customer set new_feature=2 where customer_id=($1)',[customer_id],r => r.rowCount)
			.then(function (data) {
				resolve(data)
			}).catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},
	update_feature_send_mail: async (customer_id)=>{
		return new Promise(function(resolve, reject) {
			db.result('UPDATE tbl_customer set new_feature_send_mail=2 where customer_id=($1)',[customer_id],r => r.rowCount)
			.then(function (data) {
				resolve(data)
			}).catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			});
		});
	},
	findByUserId: async (id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select tbl_customer.*,to_char(tbl_customer.date_added, 'Mon YYYY') as date_added from tbl_customer where customer_id=($1) and status='1'",[
	        id])
		    .then(function (data) {  	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	findByUserQAId: async (id)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT id AS user_id FROM tbl_qa_login WHERE id=($1) and status='1'",[
	        id])
		    .then(function (data) {  	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	check_coa: async (customer_name,batch_number,lot_no)=>{
		return new Promise(function(resolve, reject) {
			db.any("SELECT coa_id FROM tbl_tasks_coa WHERE LOWER(customer_name)=($1) AND LOWER(batch_no)=($2) AND LOWER(inspection_lot_no)=($3)",[
				customer_name,batch_number,lot_no])
		    .then(function (data = []) {
				if(data.length > 0 && data[0].coa_id > 0){
					resolve(true);
				}else{
					resolve(false);
				}
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	insert_coa:async (data)=>{
		return new Promise(function(resolve, reject) {
			db.one(
				`INSERT INTO tbl_tasks_coa(
					customer_name,
					batch_no,
					inspection_lot_no,
					date_added,
					source,
					ip,
					coa_file,
					coa_file_path) 
				 VALUES($1,$2,$3,$4,$5,$6,$7,$8) 
				 RETURNING coa_id`,[
						data.customer_name,
						data.batch_number,
						data.lot_no,
						data.date_added,
						data.source,
						data.ip,
						data.coa_file,
						data.coa_file_path])
				.then(function (data) { 
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});
		});
	},
	findActivatedUserId: async (id)=>{
		return new Promise(function(resolve, reject) {
			db.any("select tbl_customer.*,to_char(tbl_customer.date_added, 'Mon YYYY') as date_added from tbl_customer where customer_id=($1) and status='1' and activated ='1'",[
	        id])
		    .then(function (data) {  	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	findByEmail: async (email)=>{
		return new Promise(function(resolve, reject) {
			console.log('db',db);
			db.any("select tbl_customer.*,to_char(tbl_customer.date_added, 'Mon YYYY') as date_added from tbl_customer where LOWER(email)=LOWER($1) and status='1'",[
				email])
		    .then(function (data) {  
				console.log(data);	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	findByEmailQA: async (email)=>{
		return new Promise(function(resolve, reject) {
			db.any("select tbl_qa_login.*,to_char(tbl_qa_login.date_added, 'Mon YYYY') as date_added FROM tbl_qa_login WHERE LOWER(email)=LOWER($1) AND status='1'",[
				email])
		    .then(function (data) {  
				console.log(data);	
		      resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	add_user: async (user)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_customer(drupal_id,vip_customer,sap_ref,role_type,role,first_name,last_name,email,phone_no,password,country_id,company_id,date_added,status,dnd,language_code) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING customer_id',[0,0,null,0,0,user.first_name,
	        user.last_name,user.email,user.phone_no,user.password,user.country_id,user.company_id,user.date_added,0,user.dnd,user.language_code])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	update_dummy_user: async (user,customer_id)=>{
		return new Promise(function(resolve, reject) {
				var sql = 
				`UPDATE 
					tbl_customer SET 
					first_name = ($1), 
					last_name = ($2), 
					phone_no = ($3), 
					country_id = ($4), 
					company_id = ($5), 
					date_added = ($6), 
					status = ($7), 
					language_code = ($9) 
				WHERE customer_id = ($8)`;
				db.result(sql,
				[
					user.first_name,
					user.last_name,
					user.phone_no,
					user.country_id,
					user.company_id,
					user.date_added,
					0,
					customer_id,
					user.language_code
				],r => r.rowCount)
				.then(function (data) {
					resolve(data)
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	add_contact: async (user)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_contact(name,email,mobile,message,country_id,company_name,date_added,status,sales_force_token,product_id) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING id',[user.name,
	        user.email,user.mobile,user.message,user.country_id,user.company_name,user.date_added,1,user.sales_force_token,user.product_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	update_pass: async (pass,id)=>{
		return new Promise(function(resolve, reject) {
			resolve(true);
		    db.result(`UPDATE tbl_customer set password=($1),pass_update_date=($3) where customer_id=($2)`,[pass,id,common.currentDateTime()],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	update_activated: async (id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.result('UPDATE tbl_customer set activated=1 where customer_id=($1)',[id],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	update_token: async (id,token)=>{
		return new Promise(function(resolve, reject) {
			
		    db.result('UPDATE tbl_customer set token=($2) where customer_id=($1)',[id,token],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	get_pass: async (id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.any('SELECT password from tbl_customer where customer_id=($1)',[id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},

	customer_team: async (id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.any('SELECT tbl_employee.first_name,tbl_employee.last_name,tbl_employee.email,tbl_designation.desig_name,tbl_department.dept_name, tbl_employee.profile_pic from tbl_customer_team INNER JOIN tbl_employee ON tbl_employee.employee_id = tbl_customer_team.employee_id INNER JOIN tbl_designation ON tbl_designation.desig_id = tbl_employee.desig_id INNER JOIN tbl_department ON tbl_department.dept_id = tbl_employee.dept_id where tbl_customer_team.customer_id = ($1) and  tbl_customer_team.status = 1',[id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	email_exists: async (email,user_id)=>{
		return new Promise(function(resolve, reject) {
			if(user_id === undefined){ 

				db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1)",[email])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}else{
			   db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) and customer_id!=($2) and status='1'",[email,user_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}
			

		}); 

	},
	email_exists_general: async (email)=>{
		return new Promise(function(resolve, reject) {

			db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) AND status != 3",[email])
				.then(function (data) {
					if (data.length > 0)
							resolve({success: true});
						else
							resolve({success: false});
				})
				.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
				});
		}); 

	},
	email_exists_dummy: async (email)=>{
		return new Promise(function(resolve, reject) {

			db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) AND status = 3",[email])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
				});
		}); 

	},
	active_email_exists: async (email)=>{
		return new Promise(function(resolve, reject) {
			db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) AND status = 1 AND activated = 1",[email])
			.then(function (data) {
				if (data.length > 0)
					resolve({success: true});
				else
					resolve({success: false});
			})
			.catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}); 
	},
	mobile_exists: async (mobile,user_id)=>{
		return new Promise(function(resolve, reject) {
			if(user_id === undefined){ 
				db.any("select customer_id from tbl_customer where phone_no=($1) and status='1'",[mobile])
			    .then(function (data) {
					if (data.length == 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}else{
			   db.any("select customer_id from tbl_customer where phone_no=($1) and customer_id!=($2) and status='1'",[mobile,user_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}
			

		}); 

	},
	username_exists: async (username,user_id)=>{
		return new Promise(function(resolve, reject) {
			if(user_id === undefined){ 
				db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) and status='1'",[username])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}else{
				db.any("select customer_id from tbl_customer where LOWER(email)=LOWER($1) and customer_id !=($2) and status='1'",[username,user_id])
			    .then(function (data) {
			    	if (data.length > 0)
			      		resolve({success: true});
			      	else
			      		resolve({success: false});
			    })
			    .catch(function (err) {
			      	var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
			}

		}); 

	},

	task_request_search: async (customer_arr,customer_id, keyword)=>{
		return new Promise(function(resolve, reject) {
			var sql =`SELECT task_ref as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(task_ref) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 GROUP BY task_ref`;

			db.any(sql,[customer_arr,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	task_other_search: async (customer_arr,customer_id, keyword, request_type)=>{
		return new Promise(function(resolve, reject) {
			var sql = `SELECT task_ref as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(task_ref) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY task_ref`;
			db.any(sql,[customer_arr,request_type,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	task_uploads_other_search: async (customer_arr,customer_id, keyword, request_type)=>{
		return new Promise(function(resolve, reject) {
			var sql = `SELECT tbl_task_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_task_uploads.actual_file_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0`;
			db.any(sql,[customer_arr,request_type,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	task_uploads_request_search: async (customer_arr,customer_id, keyword)=> {
		return new Promise(function(resolve, reject) {
			var sql = `SELECT tbl_task_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					where LOWER(tbl_task_uploads.actual_file_name) LIKE '%${keyword}%' 
					and (
						tbl_tasks.customer_id IN ($1:csv) 
					OR 
						tbl_tasks.task_id IN 
						(SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)
					) 
					and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) 
					and parent_id = 0`;
			db.any(sql,[customer_arr,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	task_discussion_other_search:async (customer_arr,customer_id, keyword, request_type)=>{
		return new Promise(function(resolve, reject) {
			var sql = `SELECT tbl_task_discussion_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_task_discussion_uploads ON tbl_tasks.task_id = tbl_task_discussion_uploads.task_id
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0`;
			db.any(sql,[customer_arr,request_type,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	task_discussion_request_search: async (customer_arr,customer_id, keyword)=> {
		return new Promise(function(resolve, reject) {
			var sql = `SELECT tbl_task_discussion_uploads.actual_file_name as label, tbl_tasks.task_id as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_task_discussion_uploads ON tbl_tasks.task_id = tbl_task_discussion_uploads.task_id
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${keyword}%' 
					and (
						tbl_tasks.customer_id IN ($1:csv) 
					OR 
						tbl_tasks.task_id IN 
						(SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)
					) 
					and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) 
					and parent_id = 0`;
			db.any(sql,[customer_arr,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
		    	//console.log('=====>', err)
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	title_request_search: async (customer_arr,customer_id, keyword, lang_code)=>{
		return new Promise(function(resolve, reject) {

			if(lang_code != Config.default_language){
				var sql =`SELECT tbl_master_request_type.req_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv)
					 OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 GROUP BY tbl_master_request_type.req_name_${lang_code}`;
			}else{
				var sql =`SELECT tbl_master_request_type.req_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_request_type.req_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv)
					 OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 GROUP BY tbl_master_request_type.req_name`;
			}

			db.any(sql,[customer_arr,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	title_other_search: async (customer_arr,customer_id, keyword,  request_type)=>{
		return new Promise(function(resolve, reject) {
			var sql =`SELECT tbl_master_request_type.req_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_request_type.req_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2) and parent_id = 0 GROUP BY tbl_master_request_type.req_name`;
			db.any(sql,[customer_arr, request_type,customer_id])
		    .then(function (data) {
		    	resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	product_request_search: async(customer_arr,customer_id, keyword, lang_code) => {
		return new Promise(function(resolve, reject) {
			if(lang_code != Config.default_language){
				var sql =`SELECT tbl_master_product.product_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 GROUP BY tbl_master_product.product_name_${lang_code}`;
			}else{
				var sql =`SELECT tbl_master_product.product_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 GROUP BY tbl_master_product.product_name`;
			}
			
			db.any(sql,[customer_arr,customer_id])
			    .then(function (data) {
			    	resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
		})
	},

	product_other_search: async(customer_arr,customer_id, keyword, request_type, lang_code) => {
		return new Promise(function(resolve, reject) {

			if(lang_code != Config.default_language){
				var sql =`SELECT tbl_master_product.product_name_${lang_code} as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY tbl_master_product.product_name_${lang_code}`;
			}else{
				var sql =`SELECT tbl_master_product.product_name as label, MAX(tbl_tasks.task_id) as value from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					where LOWER(tbl_master_product.product_name) LIKE '%${keyword}%' and (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($3) AND status = 1)) and tbl_tasks.request_type in ($2:csv) and parent_id = 0 GROUP BY tbl_master_product.product_name`;
			}

			
			db.any(sql,[customer_arr,request_type,customer_id])
			    .then(function (data) {
			    	resolve(data)
			    })
			    .catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			    });
		})
	},

	response_status_search: async(customer_arr,customer_id, task_id, keyword) => {
		return new Promise(async(resolve, reject) => {
			db.any(`select status as label, MAX(task_id) as value
				from tbl_customer_response
				where customer_id = $1
				and task_id = $2
				and LOWER(status) LIKE '%${keyword}%'
				AND status IS NOT NULL
				GROUP BY status`,[customer_id, task_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}) 
	},

	response_action_search: async(customer_arr,customer_id, task_id, keyword) => {
		return new Promise((resolve, reject) => {
			db.any(`select required_action as label, MAX(task_id) as value
				from tbl_customer_response
				where customer_id = $1
				and task_id = $2
				and LOWER(required_action) LIKE '%${keyword}%'
				AND required_action IS NOT NULL
				GROUP BY required_action`,[customer_id, task_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		}) 
	},

	status_request_search: (customer_arr,customer_id, keyword) => {
		return new Promise((resolve, reject) => {
			db.any(`select task_id
						from tbl_tasks
						where customer_id IN ($1)
						AND request_type IN (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33)`,[customer_arr])
		    .then(async (data) => {
		    	var status_arr = [];
		    	let l = data.length;
	    		for (var i=0; i < l; i++) {
	    			await module.exports.response_status_search( customer_id, data[i].task_id, keyword ).then(c => {
	    				if(c.length > 0){
	    					status_arr.push(c[0]);	
	    				}
    				})
	    		}
	    		
	    		resolve(status_arr)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},
	
	status_other_search: (customer_arr,customer_id, keyword, request_type) => {
		return new Promise((resolve, reject) => {
			db.any(`select task_id
						from tbl_tasks
						where customer_id IN ($1)
						AND request_type = $2`,[customer_arr, request_type])
		    .then(async (data) => {
		    	var status_arr = [];
		    	let l = data.length;
	    		for (var i=0; i < l; i++) {
	    			await module.exports.response_status_search( customer_id, data[i].task_id, keyword ).then(c => {
	    				if(c.length > 0){
	    					status_arr.push(c[0]);	
	    				}
    				})
	    		}
	    		
	    		resolve(status_arr)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	action_request_search: async(customer_arr,customer_id, keyword) => {
		return new Promise(function(resolve, reject) {
			db.any(`select task_id
						from tbl_tasks
						where customer_id IN ($1)
						AND request_type IN (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33)`,[customer_arr])
		    .then(async (data) => {

		    	var action_arr = [];
		    	let l = data.length;
	    		for (var i=0; i < l; i++) {
	    			await module.exports.response_action_search( customer_id, data[i].task_id, keyword ).then(c => {
	    				if(c.length > 0){
	    					action_arr.push(c[0]);	
	    				}
    				})
	    		}
	    		resolve(action_arr)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	action_other_search: async(customer_arr,customer_id, keyword, request_type) => {
		return new Promise(function(resolve, reject) {
			db.any(`select task_id
						from tbl_tasks
						where customer_id IN ($1)
						AND request_type = $2`,[customer_arr, request_type])
		    .then(async (data) => {

		    	var action_arr = [];
		    	let l = data.length;
	    		for (var i=0; i < l; i++) {
	    			await module.exports.response_action_search( customer_id, data[i].task_id, keyword ).then(c => {
	    				if(c.length > 0){
	    					action_arr.push(c[0]);	
	    				}
    				})
	    		}
	    		resolve(action_arr)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		})
	},

	request_search:  async (customer_arr,customer_id, keyword)=>{
		return new Promise(async function(resolve, reject) {	
			var finalArr = [];
			keyword = keyword.toLowerCase();
			//customer language
			let lang_code = await module.exports.get_customer_language(customer_id);
			// ======================== SATYAJIT//
		    await module.exports.task_request_search(customer_arr,customer_id, keyword).then( taskRecord => {
		    	//console.log('task', taskRecord)
		    	if(taskRecord.length > 0){
					//finalArr.push(taskRecord)
					for (let index = 0; index < taskRecord.length; index++) {
						const element = taskRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
		    })
		    
		  //   // ======================== SATYAJIT//
		    await module.exports.title_request_search(customer_arr,customer_id, keyword,lang_code).then( titleRecord => {
		    	//console.log('ttile', titleRecord)
		    	if(titleRecord.length > 0){
					//finalArr.push(titleRecord)
					for (let index = 0; index < titleRecord.length; index++) {
						const element = titleRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
		    })		    

		//   // //  // ======================== SATYAJIT//
		    await module.exports.product_request_search(customer_arr,customer_id, keyword,lang_code).then( prodRecord => {
		    	//console.log('rpo', prodRecord)
		    	if(prodRecord.length > 0){
					//finalArr.push(prodRecord)
					for (let index = 0; index < prodRecord.length; index++) {
						const element = prodRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
				var labelArr = [];
				//===============//
				await module.exports.task_uploads_request_search(customer_arr,customer_id, keyword)
				.then( fileArr => {
		    	if(fileArr.length > 0){							
							for (let index = 0; index < fileArr.length; index++) {
								const element = fileArr[index];
								if(element.value != null){
									if(labelArr.length > 0){
										if(labelArr.indexOf(element.label) == -1){
											finalArr.push(element);
											labelArr.push(element.label)	
										}
									}else{
										finalArr.push(element);
										labelArr.push(element.label)
									}
								}
							}
			    }
				})

				// ======================== SATYAJIT//
				await module.exports.task_discussion_request_search(customer_arr,customer_id, keyword)
				.then( discArr => {
		    	//console.log('discussion files===>', discArr)
		    	if(discArr.length > 0){
					for (let index = 0; index < discArr.length; index++) {
						const element = discArr[index];
						if(element.value != null){
							if(labelArr.length > 0){
								if(labelArr.indexOf(element.label) == -1){
									finalArr.push(element);
									labelArr.push(element.label)	
								}
							}else{
								finalArr.push(element);
								labelArr.push(element.label)
							}
						}
					}
			    }
				})
				
		//   //   // ======================== SATYAJIT//
		//     await module.exports.status_request_search(customer_id, keyword).then( statRecord => {
		//     	console.log('st', statRecord)
		// 	    if(statRecord.length > 0){
		// 	    	finalArr.push(statRecord)
		// 	    }
		//     })
		    
		//   //   // ======================== SATYAJIT//
		//     await module.exports.action_request_search(customer_id, keyword).then( actRecord => {
		//     	console.log('ac', actRecord)
		//     	if(actRecord.length > 0){
		// 	    	finalArr.push(actRecord)
		// 	    }
		//     })
			//console.log('final arr',finalArr);
			//console.log('final arr flat',finalArr.flat(1));

			resolve(finalArr);
		});
	},

	order_search:  async (customer_arr,customer_id, keyword)=>{
		return new Promise(async function(resolve, reject) {	
			var finalArr = [];
			keyword = keyword.toLowerCase();
			let lang_code = await module.exports.get_customer_language(customer_id);
			// ======================== SATYAJIT//
		    await module.exports.task_other_search(customer_arr,customer_id, keyword, [23,41]).then( taskRecord => {
		    	//console.log('task', taskRecord)
		    	if(taskRecord.length > 0){
					//finalArr.push(taskRecord)
					for (let index = 0; index < taskRecord.length; index++) {
						const element = taskRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
				var labelArr = [];
				//================//
				await module.exports.task_uploads_other_search(customer_arr,customer_id, keyword, [23,41]).then( orderFiles => {
		    	//console.log('task', orderFiles)
		    	if(orderFiles.length > 0){
					//finalArr.push(orderFiles)
							for (let index = 0; index < orderFiles.length; index++) {
									const element = orderFiles[index];
									if(element.value != null){
										if(labelArr.length > 0){
											if(labelArr.indexOf(element.label) == -1){
												finalArr.push(element);
												labelArr.push(element.label)	
											}
										}else{
											finalArr.push(element);
											labelArr.push(element.label)
										}
									}
							}
			    }
				})
				
				// ======================== SATYAJIT//
				await module.exports.task_discussion_other_search(customer_arr,customer_id, keyword, [23,41]).then( discFiles => {
		    	//console.log('task', discFiles)
		    	if(discFiles.length > 0){
					//finalArr.push(discFiles)
							for (let index = 0; index < discFiles.length; index++) {
								const element = discFiles[index];
								if(element.value != null){
									if(labelArr.length > 0){
										if(labelArr.indexOf(element.label) == -1){
											finalArr.push(element);
											labelArr.push(element.label)	
										}
									}else{
										finalArr.push(element);
										labelArr.push(element.label)
									}
								}
							}
			    }
		    })
		    
		  //   // ======================== SATYAJIT//
		    // await module.exports.title_other_search(customer_id, keyword, 23).then( titleRecord => {
		    // 	console.log('ttile', titleRecord)
		    // 	if(titleRecord.length > 0){
			//     	finalArr.push(titleRecord)
			//     }
		    // })		    

		//   // //  // ======================== SATYAJIT//
		    await module.exports.product_other_search(customer_arr,customer_id, keyword, [23,41],lang_code).then( prodRecord => {
		    	//console.log('rpo', prodRecord)
		    	if(prodRecord.length > 0){
					//finalArr.push(prodRecord)
					for (let index = 0; index < prodRecord.length; index++) {
						const element = prodRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
		    })
		//   //   // ======================== SATYAJIT//
		//     await module.exports.status_other_search(customer_id, keyword, 23).then( statRecord => {
		//     	console.log('st', statRecord)
		// 	    if(statRecord.length > 0){
		// 	    	finalArr.push(statRecord)
		// 	    }
		//     })
		    
		//   //   // ======================== SATYAJIT//
		//     await module.exports.action_other_search(customer_id, keyword, 23).then( actRecord => {
		//     	console.log('ac', actRecord)
		//     	if(actRecord.length > 0){
		// 	    	finalArr.push(actRecord)
		// 	    }
		//     })

			resolve(finalArr);
		});
	},

	complaint_search:  async (customer_arr,customer_id, keyword)=>{
		return new Promise(async function(resolve, reject) {	
			var finalArr = [];
			keyword = keyword.toLowerCase();
			let lang_code = await module.exports.get_customer_language(customer_id);
			// ======================== SATYAJIT//
		    await module.exports.task_other_search(customer_arr,customer_id, keyword, [7,34]).then( taskRecord => {
		    	//console.log('task', taskRecord)
		    	if(taskRecord.length > 0){
					//finalArr.push(taskRecord)
					for (let index = 0; index < taskRecord.length; index++) {
						const element = taskRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
		    })
		    
		  //   // ======================== SATYAJIT//
		    // await module.exports.title_other_search(customer_id, keyword, 7).then( titleRecord => {
		    // 	console.log('ttile', titleRecord)
		    // 	if(titleRecord.length > 0){
			//     	finalArr.push(titleRecord)
			//     }
		    // })		    

		//   // //  // ======================== SATYAJIT//
		    await module.exports.product_other_search(customer_arr,customer_id, keyword,[7,34],lang_code).then( prodRecord => {
		    	//console.log('rpo', prodRecord)
		    	if(prodRecord.length > 0){
					//finalArr.push(prodRecord)
					for (let index = 0; index < prodRecord.length; index++) {
						const element = prodRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
				var labelArr = [];
				//===============//
		    await module.exports.task_uploads_other_search(customer_arr,customer_id, keyword, [7,34]).then( compRecord => {
		    	//console.log('task', compRecord)
		    	if(compRecord.length > 0){
					//finalArr.push(compRecord)
					for (let index = 0; index < compRecord.length; index++) {
						const element = compRecord[index];
						if(element.value != null){
							if(labelArr.length > 0){
								if(labelArr.indexOf(element.label) == -1){
									finalArr.push(element);
									labelArr.push(element.label)	
								}
							}else{
								finalArr.push(element);
								labelArr.push(element.label)
							}
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
		    await module.exports.task_discussion_other_search(customer_arr,customer_id, keyword, [7,34]).then( discsRecord => {
		    	//console.log('task', discsRecord)
		    	if(discsRecord.length > 0){
					//finalArr.push(discsRecord)
					for (let index = 0; index < discsRecord.length; index++) {
						const element = discsRecord[index];
						if(element.value != null){
							if(labelArr.length > 0){
								if(labelArr.indexOf(element.label) == -1){
									finalArr.push(element);
									labelArr.push(element.label)	
								}
							}else{
								finalArr.push(element);
								labelArr.push(element.label)
							}
						}
					}
			    }
		    })

		//   //   // ======================== SATYAJIT//
		//     await module.exports.status_other_search(customer_id, keyword, 7).then( statRecord => {
		//     	console.log('st', statRecord)
		// 	    if(statRecord.length > 0){
		// 	    	finalArr.push(statRecord)
		// 	    }
		//     })
		    
		//   //   // ======================== SATYAJIT//
		//     await module.exports.action_other_search(customer_id, keyword, 7).then( actRecord => {
		//     	console.log('ac', actRecord)
		//     	if(actRecord.length > 0){
		// 	    	finalArr.push(actRecord)
		// 	    }
		//     })

			resolve(finalArr);
		});
	},

	forecast_search: async (customer_arr,customer_id, keyword)=>{
		return new Promise(async function(resolve, reject) {	
			var finalArr = [];
			keyword = keyword.toLowerCase();

			// ======================== SATYAJIT//
		    await module.exports.task_other_search(customer_arr,customer_id, keyword,[24]).then( taskRecord => {
		    	//console.log('task', taskRecord)
		    	if(taskRecord.length > 0){
					//finalArr.push(taskRecord)
					for (let index = 0; index < taskRecord.length; index++) {
						const element = taskRecord[index];
						if(element.value != null){
							finalArr.push(element);
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
				var labelArr = [];
				//==============//
		    await module.exports.task_uploads_other_search(customer_arr,customer_id, keyword,[24]).then( fRecord => {
		    	//console.log('task', fRecord)
		    	if(fRecord.length > 0){
					//finalArr.push(fRecord)
					for (let index = 0; index < fRecord.length; index++) {
						const element = fRecord[index];
						if(element.value != null){
							if(labelArr.length > 0){
								if(labelArr.indexOf(element.label) == -1){
									finalArr.push(element);
									labelArr.push(element.label)	
								}
							}else{
								finalArr.push(element);
								labelArr.push(element.label)
							}
						}
					}
			    }
				})
				
				// ======================== SATYAJIT//
		    await module.exports.task_discussion_other_search(customer_arr,customer_id, keyword, [24]).then( fdRecord => {
		    	//console.log('task', fdRecord)
		    	if(fdRecord.length > 0){
						//finalArr.push(fdRecord)
						for (let index = 0; index < fdRecord.length; index++) {
							const element = fdRecord[index];
							if(element.value != null){
								if(labelArr.length > 0){
									if(labelArr.indexOf(element.label) == -1){
										finalArr.push(element);
										labelArr.push(element.label)	
									}
								}else{
									finalArr.push(element);
									labelArr.push(element.label)
								}
							}
						}
			    }
		    })
		    
		  //   // ======================== SATYAJIT//
		    // await module.exports.title_other_search(customer_id, keyword,24).then( titleRecord => {
		    // 	console.log('ttile', titleRecord)
		    // 	if(titleRecord.length > 0){
			//     	finalArr.push(titleRecord)
			//     }
		    // })		    

		//   // //  // ======================== SATYAJIT//
		    // await module.exports.product_other_search(customer_id, keyword,24).then( prodRecord => {
		    // 	//console.log('rpo', prodRecord)
		    // 	if(prodRecord.length > 0){
			//     	finalArr.push(prodRecord)
			//     }
		    // })
		//   //   // ======================== SATYAJIT//
		//     await module.exports.status_other_search(customer_id, keyword,24).then( statRecord => {
		//     	console.log('st', statRecord)
		// 	    if(statRecord.length > 0){
		// 	    	finalArr.push(statRecord)
		// 	    }
		//     })
		    
		//   //   // ======================== SATYAJIT//
		//     await module.exports.action_other_search(customer_id, keyword,24).then( actRecord => {
		//     	console.log('ac', actRecord)
		//     	if(actRecord.length > 0){
		// 	    	finalArr.push(actRecord)
		// 	    }
		//     })

			resolve(finalArr);
		});
	},
	//SERVICE REQUESTS
	request_list: async (arr_id,cust_id,query_arr,limit, offset,identifier)=>{
		return new Promise( async function(resolve, reject) {
			var query_string = '';
			let lang_code = await module.exports.get_customer_language(cust_id);
			if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
			}else if(query_arr && query_arr.date_from != null){
					query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
			}else if(query_arr && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
			}

			if(query_arr && query_arr.s && query_arr.s.length > 0){

				if(lang_code != Config.default_language){
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}else{
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}

				
				let closed_arr = await module.exports.get_task_status(4);

				
				if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
					{
						
						query_string +=  ` OR tbl_tasks.close_status = 1`;
					}
				query_string +=  `)`;
					
			}

			// (
			// 	SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
			// 	FROM tbl_customer_response WHERE resp_status = 1
			// 	GROUP BY task_id
			// ) b 

			/*var sql =`SELECT tbl_tasks.task_id,tbl_tasks.close_status,tbl_tasks.task_ref,to_char(tbl_tasks.date_added, 'dd Mon YYYY') as date_added,tbl_master_request_type.req_name,tbl_master_product.product_name,tbl_tasks.status as status_task,tbl_customer.first_name,tbl_customer.last_name,
			b.required_action,to_char(b.expected_closure_date, 'dd Mon YYYY') as expected_closure_date,h.status AS highlight_status,
			CASE WHEN tbl_tasks.close_status = 0 THEN b.status
			WHEN tbl_tasks.close_status = 1 THEN 'Closed'
			END AS status,
			CASE WHEN tbl_tasks.close_status = 0 THEN NULL
			WHEN tbl_tasks.close_status = 1 THEN to_char(tbl_tasks.closed_date, 'dd Mon YYYY')
			END AS closed_date,
			tbl_tasks.submitted_by,
			tbl_tasks.ccp_posted_by,
			tbl_agents.first_name AS agent_fname,
			tbl_agents.last_name AS agent_lname,
			tbl_employee.first_name AS emp_posted_by_fname,
			tbl_employee.last_name AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5) 
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
			 where 
			 (
			 	tbl_tasks.customer_id IN ($1:csv) 
				or 
				tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($4) AND status = 1)

			) 
			and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38) 
			and parent_id = 0  ${query_string} 
			order by tbl_tasks.task_id desc 
			limit $2 offset $3`;*/

			var sql =`SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.date_added), 'dd Mon YYYY') as date_added,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
			END AS req_name,

			MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,

			MAX(tbl_tasks.status) as status_task,
			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			
			to_char(MAX(b.expected_closure_date), 'dd Mon YYYY') as expected_closure_date,
			MAX(h.status) AS highlight_status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
					 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
					 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
					 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
					 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN NULL
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd Mon YYYY')
			END AS closed_date,
			tbl_tasks.submitted_by,
			tbl_tasks.ccp_posted_by,
			tbl_tasks.ccp_posted_with,
			
			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.customer_id) as customer_id,
			MAX(tbl_tasks.language) as task_language,


			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			 LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id AND b.resp_status = 1  AND b.approval_status = 1
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		     LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5) 
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
			 where 
			 (
			 	tbl_tasks.customer_id IN ($1:csv) 
				or 
				tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($4) AND status = 1)

			) 
			and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) 
			and parent_id = 0  ${query_string} 
			group by tbl_tasks.task_id
			order by tbl_tasks.task_id desc 
			limit $2 offset $3`;
			//console.log(sql);
      		//console.log('arr_id',arr_id);
      		//console.log('cust_id',cust_id);
		    db.any(sql,[arr_id,limit, offset,cust_id,'C'])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	count_request_list: async (arr_id,cust_id,query_arr)=>{
		return new Promise( async function(resolve, reject) {
				var query_string = '';
				let lang_code = await module.exports.get_customer_language(cust_id);
				if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
				}else if(query_arr && query_arr.date_from != null){
						query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
				}else if(query_arr && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
				}


				if(query_arr && query_arr.s && query_arr.s.length > 0){
			
					if(lang_code != Config.default_language){
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}else{
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}
	
					let closed_arr = await module.exports.get_task_status(4);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
						
				}
					
					var sql =`SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
					LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
					LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
					LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 		LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id
					LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
					LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
					LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
					LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 		LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
					where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type in (1,4,2,3,5,6,22,21,16,15,14,8,9,10,11,12,13,17,18,19,20,27,28,29,30,31,32,33,35,36,37,38,39,40,42,44) and parent_id = 0 ${query_string}
					group by tbl_tasks.task_id`;
				
					db.any(sql,[arr_id,cust_id]) 
			    .then(function (data) {	
						//console.log('count',data);
						if(data.length > 0){
							resolve(data.length);
						}else{
							resolve(0);
						}
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });

		}); 

	},
	//COMPLAINT REQUESTS
	complaint_list: async (arr_id,cust_id,query_arr,limit,offset)=>{
		return new Promise( async function(resolve, reject) {
			var query_string = '';
			let lang_code = await module.exports.get_customer_language(cust_id);
			if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
			}else if(query_arr && query_arr.date_from != null){
					query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
			}else if(query_arr && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
			}

			if(query_arr && query_arr.s && query_arr.s.length > 0){
		
				if(lang_code != Config.default_language){
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}else{
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}	

				let closed_arr = await module.exports.get_task_status(4);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
					
			}

			// LEFT JOIN
			// 	(
			// 		SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
			// 		FROM tbl_customer_response WHERE resp_status = 1
			// 		GROUP BY task_id
			// 	) b ON tbl_tasks.task_id = b.task_id

			var sql =`SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.date_added), 'dd Mon YYYY') as date_added,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
			END AS req_name,

			MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,
			MAX(tbl_tasks.batch_number) as batch_number,
			MAX(tbl_tasks.quantity) as quantity,
			MAX(tbl_tasks.language) as language,
			MAX(tbl_tasks.status) as status,
			MAX(tbl_tasks.nature_of_issue) as nature_of_issue,
			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			MAX(h.status) AS highlight_status,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN to_char(MAX(b.expected_closure_date), 'dd Mon YYYY')
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd Mon YYYY')
			END AS expected_closure_date,
			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.customer_id) as customer_id,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_tasks.ccp_posted_with) as ccp_posted_with,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type
			 LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
 			 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5) 
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			 where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($4) AND status = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;

		    db.any(sql,[arr_id,limit, offset,cust_id,'C'])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	count_complaint_list: async (arr_id,cust_id,query_arr)=>{
		return new Promise( async function(resolve, reject) {
			var query_string = '';
			let lang_code = await module.exports.get_customer_language(cust_id);
			if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
			}else if(query_arr && query_arr.date_from != null){
					query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
			}else if(query_arr && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
			}

			if(query_arr && query_arr.s && query_arr.s.length > 0){
				if(lang_code != Config.default_language){
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}else{
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}
				
				let closed_arr = await module.exports.get_task_status(4);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
					
			}

			var sql =`SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
			LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  			LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;

			    db.any(sql,[arr_id,cust_id]) 
			    .then(function (data) {	
						if(data.length > 0){
							resolve(data.length);
						}else{
							resolve(0);
						}
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });

		}); 

	},
	//ORDER REQUESTS
	order_list: async (arr_id,cust_id,query_arr,limit, offset)=>{
		return new Promise( async function(resolve, reject) {

			var query_string = '';
			let lang_code = await module.exports.get_customer_language(cust_id);	
			if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
			}else if(query_arr && query_arr.date_from != null){
					query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
			}else if(query_arr && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
			}

			if(query_arr && query_arr.s && query_arr.s.length > 0){
				if(lang_code != Config.default_language){
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}else{
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}

				let closed_arr = await module.exports.get_task_status(9);

				if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
				closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
					{
						
						query_string +=  ` OR tbl_tasks.close_status = 1`;
					}
				query_string +=  `)`;
					
			}

			var sql =`SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			to_char(MAX(tbl_tasks.rdd), \'dd Mon YYYY\') as rdd,
			to_char(MAX(tbl_tasks.date_added), \'dd Mon YYYY\') as date_added,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
			END AS req_name,

			MAX(tbl_master_request_type.req_name) AS rating_req_name,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_product.product_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_product.product_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_product.product_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_product.product_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_product.product_name)
			END AS product_name,
			MAX(tbl_tasks.quantity) as quantity,
			MAX(tbl_tasks.language) as language,
			MAX(tbl_tasks.status) as status,
			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,
			
			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.customer_id) as customer_id,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,
			MAX(b.po_number) as po_number,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 9
			)
			END AS status,

			MAX(h.status) AS highlight_status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN to_char(MAX(b.expected_closure_date), 'dd Mon YYYY')
			WHEN MAX(tbl_tasks.close_status) = 1 THEN to_char(MAX(tbl_tasks.closed_date), 'dd Mon YYYY')
			END AS expected_closure_date,
			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_tasks.ccp_posted_with) as ccp_posted_with,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type
			 LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
             LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		     LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by

			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_customer_task_highligher AS h 
			 ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5)  
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
			 where (
				 (tbl_tasks.customer_id IN ($1:csv)) 
					 OR 
				 (tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($4) AND status = 1))
			 ) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;
			//console.log(sql);
			db.any(sql,[arr_id,limit, offset,cust_id,'C'])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	count_order_list: async (arr_id,cust_id,query_arr)=>{
		return new Promise( async function(resolve, reject) {

				var query_string = '';
				let lang_code = await module.exports.get_customer_language(cust_id);
				if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
				}else if(query_arr && query_arr.date_from != null){
						query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
				}else if(query_arr && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
				}

				if(query_arr && query_arr.s && query_arr.s.length > 0){
					if(lang_code != Config.default_language){
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}else{
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}
	
					let closed_arr = await module.exports.get_task_status(9);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
						
				}

				var sql =`SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1 
				LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 	LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
				LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
				LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 		LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
				where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;

			    db.any(sql,[arr_id,cust_id]) 
			    .then(function (data) {	
						if(data.length > 0){
							resolve(data.length);
						}else{
							resolve(0);
						}
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });

		}); 

	},
	//FORECAST REQUESTS
	forecast_list: async (arr_id,cust_id,query_arr,limit, offset)=>{
		return new Promise( async function(resolve, reject) {

			var query_string = '';
			let lang_code = await module.exports.get_customer_language(cust_id);
			if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
			}else if(query_arr && query_arr.date_from != null){
					query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
			}else if(query_arr && query_arr.date_to != null){
				query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
			}

			if(query_arr && query_arr.s && query_arr.s.length > 0){
				if(lang_code != Config.default_language){
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}else{
					query_string += `
					AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
					OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
					OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
				}

				let closed_arr = await module.exports.get_task_status(4);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
					
			}

			// LEFT JOIN
			// 	(
			// 		SELECT MAX(task_id) as task_id,MAX(required_action) as required_action,MAX(status) as status,MAX(expected_closure_date) as expected_closure_date, MAX(date_added) max_date
			// 		FROM tbl_customer_response WHERE resp_status = 1
			// 		GROUP BY task_id
			// 	) b ON tbl_tasks.task_id = b.task_id

			var sql =`SELECT tbl_tasks.task_id,
			MAX(tbl_tasks.close_status) as close_status,
			MAX(tbl_tasks.duplicate_task) as duplicate_task,
			MAX(tbl_tasks.task_ref) as task_ref,
			
			MAX(tbl_tasks.rating_skipped) as rating_skipped,
			MAX(tbl_tasks.rating) as rating,
			MAX(tbl_tasks.rated_by) as rated_by,
			MAX(tbl_tasks.rated_by_type) as rated_by_type,
			MAX(tbl_tasks.customer_id) as customer_id,
			MAX(tbl_tasks.language) as task_language,

			MAX(rated_by.first_name) as first_name_rated_by,
			MAX(rated_by.last_name) as last_name_rated_by,

			to_char(MAX(tbl_tasks.date_added), \'dd Mon YYYY\') as date_added,
			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_request_type.req_name_zh)
				 WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_request_type.req_name_pt)
				 WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_request_type.req_name_es)
				 WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_request_type.req_name_ja)
				 WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_request_type.req_name)
			END AS req_name,

			MAX(tbl_master_request_type.req_name) AS rating_req_name,

			MAX(tbl_customer.first_name) as first_name,
			MAX(tbl_customer.last_name) as last_name,

			MAX(rated_by_agent.first_name) as first_name_rated_by_agent,
			MAX(rated_by_agent.last_name) as last_name_rated_by_agent,

			

			CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_action_required.action_name_zh)
					WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_action_required.action_name_pt)
					WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_action_required.action_name_es)
					WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_action_required.action_name_ja)
					WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_action_required.action_name_en)
			END AS required_action,

			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN 

				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(tbl_master_task_status.status_name_zh)
						WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(tbl_master_task_status.status_name_pt)
						WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(tbl_master_task_status.status_name_es)
						WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(tbl_master_task_status.status_name_ja)
						WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(tbl_master_task_status.status_name_en)
				END

			WHEN MAX(tbl_tasks.close_status) = 1 THEN (
				SELECT 
				CASE WHEN MAX(tbl_tasks.language) = 'zh' THEN MAX(c.status_name_zh)
				WHEN MAX(tbl_tasks.language) = 'pt' THEN MAX(c.status_name_pt)
				WHEN MAX(tbl_tasks.language) = 'es' THEN MAX(c.status_name_es)
				WHEN MAX(tbl_tasks.language) = 'ja' THEN MAX(c.status_name_ja)
				WHEN MAX(tbl_tasks.language) = 'en' THEN MAX(c.status_name_en)
				END FROM tbl_master_task_status AS c WHERE c.status_id = 4
			)
			END AS status,

			MAX(h.status) AS highlight_status,
			CASE WHEN MAX(tbl_tasks.close_status) = 0 THEN MAX(b.expected_closure_date)
			WHEN MAX(tbl_tasks.close_status) = 1 THEN MAX(tbl_tasks.closed_date)
			END AS expected_closure_date,
			MAX(tbl_tasks.submitted_by) as submitted_by,
			MAX(tbl_tasks.ccp_posted_by) as ccp_posted_by,
			MAX(tbl_tasks.ccp_posted_with) as ccp_posted_with,
			MAX(tbl_agents.first_name) AS agent_fname,
			MAX(tbl_agents.last_name) AS agent_lname,
			MAX(tbl_employee.first_name) AS emp_posted_by_fname,
			MAX(tbl_employee.last_name) AS emp_posted_by_lname
			 from tbl_tasks
			 LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type
			 LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
			 LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
             LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id 
			 LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			 LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
 			 LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
			 LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id

			 LEFT JOIN tbl_customer AS rated_by ON rated_by.customer_id = tbl_tasks.rated_by
			 LEFT JOIN tbl_agents AS rated_by_agent ON rated_by_agent.agent_id = tbl_tasks.rated_by
			 
			 LEFT JOIN tbl_employee ON tbl_tasks.ccp_posted_by = tbl_employee.employee_id
			 LEFT JOIN tbl_customer_task_highligher AS h ON tbl_tasks.task_id = h.task_id 
			 AND h.customer_id = ($4) 
			 AND h.identifier  = ($5)   
			 LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by 
			 where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($4) AND status = 1)) and tbl_tasks.request_type = 24 and parent_id = 0 ${query_string} group by tbl_tasks.task_id order by tbl_tasks.task_id desc limit $2 offset $3`;

			 db.any(sql,[arr_id,limit, offset,cust_id,'C'])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		}); 
	},
	count_forecast_list: async (arr_id,cust_id,query_arr)=>{
		return new Promise( async function(resolve, reject) {

				var query_string = '';
				let lang_code = await module.exports.get_customer_language(cust_id);
				if(query_arr && query_arr.date_from != null && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) BETWEEN '${query_arr.date_from}'::date AND '${query_arr.date_to}'::date `;
				}else if(query_arr && query_arr.date_from != null){
						query_string += ` AND DATE(tbl_tasks.date_added) >= '${query_arr.date_from}'::date `;
				}else if(query_arr && query_arr.date_to != null){
					query_string += ` AND DATE(tbl_tasks.date_added) <= '${query_arr.date_to}'::date `;
				}

				if(query_arr && query_arr.s && query_arr.s.length > 0){
					if(lang_code != Config.default_language){
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_${lang_code})  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_${lang_code}) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}else{
						query_string += `
						AND (LOWER(tbl_tasks.task_ref) LIKE '%${(query_arr.s).toLowerCase()}%' 
						OR LOWER(tbl_master_request_type.req_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_product.product_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_status.status_name_en)  LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_master_task_action_required.action_name_en) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_customer.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.first_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_agents.last_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'
						OR LOWER(tbl_task_discussion_uploads.actual_file_name) LIKE '%${(query_arr.s).toLowerCase()}%'`;
					}
	
					let closed_arr = await module.exports.get_task_status(4);

					if(closed_arr[0].status_name_en.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_zh.indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_pt.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_es.toLowerCase().indexOf(query_arr.s.toLowerCase()) > -1 || 
					closed_arr[0].status_name_ja.indexOf(query_arr.s.toLowerCase()) > -1 )
						{
							
							query_string +=  ` OR tbl_tasks.close_status = 1`;
						}
					query_string +=  `)`;
						
				}

				var sql =`SELECT count(tbl_tasks.task_id) as cnt from tbl_tasks
				LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
				LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and b.resp_status = 1 AND b.approval_status = 1
				LEFT JOIN tbl_master_task_status ON b.status_id = tbl_master_task_status.status_id 
			 	LEFT JOIN tbl_master_task_action_required ON b.action_id = tbl_master_task_action_required.action_id
				LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
				LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
				LEFT JOIN tbl_task_uploads ON tbl_task_uploads.task_id = tbl_tasks.task_id
  		 		LEFT JOIN tbl_task_discussion_uploads ON tbl_task_discussion_uploads.task_id = tbl_tasks.task_id
				LEFT JOIN tbl_agents ON tbl_agents.agent_id = tbl_tasks.submitted_by
				where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type = 24 and parent_id = 0 ${query_string} group by tbl_tasks.task_id`;
			    db.any(sql,[arr_id,cust_id]) 
			    .then(function (data) {	
						if(data.length > 0){
							resolve(data.length);
						}else{
							resolve(0);
						}
			    })
			    .catch(function (err) {
			    	var errorText = common.getErrorText(err);
			    	var error     = new Error(errorText);
			    	reject(error);
			    });

		}); 

	},
	//TASK STATUS
	get_task_status: async (status_id)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT status_name_en,status_name_zh,status_name_pt,status_name_es,status_name_ja FROM tbl_master_task_status 
				WHERE status_id=($1)`, [status_id])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	//PRODUCTS
	my_products : async (id)=>{
		return new Promise(function(resolve, reject) {
			db.any('select product_id from tbl_customer_products where customer_id = ($1) and status = 1',[id])
		    .then(function (data) {
			  resolve(data);
			  
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});

		});  

	},
	
	dashboard_overview: async (cust_arr,customer_id)=>{
		// let dashboard = [];
		//product
		let productQuery = new Promise((resolve, reject) => {
			db.any('select count(product_id) from tbl_customer_products where customer_id = ($1) and status = 1',[customer_id])
		    .then(function (data) {
			  resolve(data[0].count);
			  
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
		//requests
		let requestQuery = new Promise((resolve, reject) => {
			
			db.any(`select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type not in (23,24,25,7,34,41) and parent_id = 0`,[cust_arr,customer_id])
		    .then(function (data) {
				resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});

		//complaints
		let complaintsQuery = new Promise((resolve, reject) => {
			
			
			db.any(`select count(tbl_tasks.task_id) from tbl_tasks LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and (tbl_tasks.request_type = 7 OR tbl_tasks.request_type = 34) and parent_id = 0`,[cust_arr,customer_id])
		    .then(function (data) {
		      resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});

		//orders
		let ordersQuery = new Promise((resolve, reject) => {
			
			db.any(`select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and (tbl_tasks.request_type = 23 OR tbl_tasks.request_type = 41) and parent_id = 0`,[cust_arr,customer_id])
		    .then(function (data) {
		      resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});

		//forecasts
		let forecastQuery = new Promise((resolve, reject) => {
			db.any(`select count(tbl_tasks.task_id) from tbl_tasks
			LEFT JOIN tbl_master_request_type ON tbl_master_request_type.type_id = tbl_tasks.request_type 
			LEFT JOIN tbl_customer_response as b ON tbl_tasks.task_id = b.task_id and resp_status = 1
			LEFT JOIN tbl_master_product ON tbl_master_product.product_id = tbl_tasks.product_id 
			LEFT JOIN tbl_customer ON tbl_customer.customer_id = tbl_tasks.customer_id
			where (tbl_tasks.customer_id IN ($1:csv) OR tbl_tasks.task_id IN (SELECT task_id FROM tbl_cc_customers WHERE customer_id = ($2) AND status = 1)) and tbl_tasks.request_type = 24 and parent_id = 0`,[cust_arr,customer_id])
		    .then(function (data) {
		      resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});

		//payments
		let paymentQuery = new Promise((resolve, reject) => {
			db.any('select count(task_id) from tbl_tasks where customer_id IN ($1:csv)  and request_type in (25) and parent_id = 0 and close_status = 0',[cust_arr])
		    .then(function (data) {
				resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});

		//active discussions
		let discussionQuery = new Promise((resolve, reject) => {
			db.any('select count(tbl_task_discussion.task_id) from tbl_tasks INNER JOIN tbl_task_discussion ON tbl_tasks.task_id = tbl_task_discussion.task_id where tbl_tasks.customer_id IN ($1:csv)',[cust_arr])
		    .then(function (data) {
				resolve(data[0].count);
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
			});
		});
		// Run both queries at the same time and handle both resolve results or first reject
		return Promise.all([productQuery,requestQuery,complaintsQuery,ordersQuery,forecastQuery,paymentQuery,discussionQuery])
			.then((results) => {
				//console.log('results',results);
				return({ "product": results[0], "request": results[1], "complaints": results[2], "orders": results[3], "forecasts": results[4], "payment": results[5], "discussion": results[6] }); 
				//return res.send();
			})
			.catch((err) => {
				// Catch error 
				return(err); 
			});
	},
	get_company_id: async (company_name)=>{
		return new Promise(function(resolve, reject) {
			
		    db.any('SELECT company_id from tbl_master_company where LOWER(tbl_master_company.company_name) = LOWER($1) ',[company_name])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_country_name: async (country_id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.any('SELECT country_name from tbl_master_country where country_id = ($1)',[country_id])
		    .then(function (data) {
					// if(data.length > 0){
					// 	resolve(data)
					// }else{
					// 	reject('Invalid country posted')
					// }
					resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	get_language_name: async (id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.any('SELECT language from tbl_master_language where code = ($1)',[id])
		    .then(function (data) {
					// if(data.length > 0){
					// 	resolve(data)
					// }else{
					// 	reject('Invalid country posted')
					// }
					resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	add_company: async (company_name)=>{
		return new Promise(function(resolve, reject) {
			
		    db.one('INSERT INTO tbl_master_company(company_name) VALUES($1) RETURNING company_id',[company_name])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	remove_profile_pic: async (customer_id)=>{
		return new Promise(function(resolve, reject) {
			
		    db.result('UPDATE tbl_customer set profile_pic=null where customer_id=($1)',[customer_id],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	udpate_cc_customer : async (customer_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("UPDATE tbl_cc_customers SET status = 1 WHERE customer_id=($1) AND status = 2 ",[customer_id],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });

		}); 

	},
	checkCCCustomer : async (customer_id,task_id) => {
		return new Promise(function(resolve, reject) {
			var sql = 
				`SELECT 
					customer_id::INTEGER
				FROM tbl_cc_customers 
				WHERE task_id = ($1)
					AND customer_id = ($2)`;

			db.any(sql,[task_id,customer_id])
			.then(function (data) {
					if(data && data.length > 0 && data[0].customer_id > 0){
						resolve(data[0].customer_id);
					}else{
						resolve(0);
					}
			})
			.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
			});

		});
	},
	remove_excess_cc_customers : async (total_cc_custs,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("UPDATE tbl_cc_customers SET status = 2 WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND status = 1 ",[total_cc_custs,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    })
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_customer_highlights: async (total_cc_custs,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_task_highligher WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND identifier = 'C'",[total_cc_custs,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    })
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_customer_notifications: async (total_cc_custs,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_notification WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND customer_type = 'C'",[total_cc_custs,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    })
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_agent_highlights: async (total_cc_agnt,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_task_highligher WHERE customer_id IN ($1:csv) AND task_id=($2) AND identifier = 'A'",[total_cc_agnt,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    }) 
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_agent_highlights_not_in: async (total_cc_agnt,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_task_highligher WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND identifier = 'A'",[total_cc_agnt,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    }) 
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_agent_notifications: async (total_cc_agnt,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_notification WHERE customer_id IN ($1:csv) AND task_id=($2) AND customer_type = 'A'",[total_cc_agnt,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    }) 
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	remove_excess_agent_notifications_not_in: async (total_cc_agnt,task_id)=>{
		return new Promise(function(resolve, reject) {

			db.result("DELETE FROM tbl_customer_notification WHERE customer_id NOT IN ($1:csv) AND task_id=($2) AND customer_type = 'A'",[total_cc_agnt,task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    }) 
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });

		}); 

	},
	udpateAllCCCustomer : async (task_id) => {
		return new Promise(function(resolve, reject) {
			db.result("UPDATE tbl_cc_customers SET status = 2 WHERE task_id=($1) AND status = 1 ",[task_id],r => r.rowCount)
	    .then(function (data) {
	      resolve(data)
	    })
	    .catch(function (err) {
	      var errorText = common.getErrorText(err);
	    	var error     = new Error(errorText);
	    	reject(error);
	    });
		});
	},
	fetchCustomerNotification: async (customer_id, role) => {
		return new Promise(function(resolve, reject) {
			db.any(`select tbl_customer_notification.*, tbl_tasks.task_ref 
				from tbl_customer_notification
				LEFT JOIN tbl_tasks 
					ON tbl_tasks.task_id = tbl_customer_notification.task_id
				WHERE tbl_customer_notification.customer_id=($1)
				AND tbl_customer_notification.customer_type = ($2)
				AND extract(epoch from ('${common.currentDateTime()}'- tbl_customer_notification.add_date)) <= 2592000 
				order by tbl_customer_notification.n_id DESC`,[customer_id,role])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	countCustomerNotification: async (customer_id, role) => {
		return new Promise(function(resolve, reject) {
			db.any(`select COUNT(*) AS cnt from tbl_customer_notification 				
				WHERE customer_id=($1)
				AND read_status = 0
				AND customer_type=($2)
				AND extract(epoch from ('${common.currentDateTime()}'- add_date)) <= 2592000`,[customer_id,role])
		    .then(function (data) {
		      resolve(data.length > 0 ? data[0].cnt : 0)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	updateCustomerNotification: async (customer_id, n_id, task_id, role) => {
	
		return new Promise(function(resolve, reject) {
			db.result(`UPDATE tbl_customer_notification 				
				SET read_status = 1
				WHERE customer_id=($1) 
				AND n_id=($2)
				AND task_id = ($3)
				AND customer_type=($4)
				AND read_status = 0`,[customer_id,n_id,task_id,role],r => r.rowCount)
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		      var errorText = common.getErrorText(err);
		    	var error     = new Error(errorText);
		    	reject(error);
		    });
		});
	},
	notification_exists:  async (customer_id, n_id, role)=>{
		return new Promise(function(resolve, reject) {
			db.any(`select * from tbl_customer_notification 
				where customer_id=($1) 
				and n_id=($2)
				and customer_type=($3)`,[customer_id, n_id, role])
		    .then(function (data) {
		    	if (data.length > 0)
		      		resolve({success: true});
		      	else
		      		resolve({success: false});
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		});
	},
	get_super_users:  async (customer_id, company_id)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT 
						customer_id::INTEGER,
						first_name,
						last_name 
					FROM tbl_customer 
					WHERE customer_id != ($2) 
					AND company_id = ($1)
					AND role_type = 0 
					AND status = 1 
					ORDER BY first_name ASC`,[company_id,customer_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		});
	},
	get_agent_company: async(company_id)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT 
						A.agent_id::INTEGER
					FROM tbl_agents AS A
					LEFT JOIN tbl_agent_company AS AC
					ON A.agent_id = AC.agent_id 
					WHERE AC.company_id = ($1) 
					AND A.status = 1 
					AND AC.status = 1 `,[company_id])
		    .then(function (data) {
		    	resolve(data);
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });
		});
	},
	get_department_admins: async (cust_arr,role_arr,company_id)=>{
		return new Promise(function(resolve, reject) {

			var sql = `
			SELECT 
				DISTINCT(C.customer_id),
				C.first_name,
				C.last_name 
			FROM tbl_customer AS C 
				LEFT JOIN tbl_customer_role AS ROLE
			ON C.customer_id = ROLE.customer_id 
			WHERE 
				ROLE.role_id IN ($1:csv)
				AND ROLE.status = 1
				AND C.status    = 1
				AND C.customer_id NOT IN ($2:csv)
				AND C.company_id = ($3)
				AND C.role_type = 1`;

			db.any(sql,[role_arr,cust_arr,company_id])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	saerch_user_company: async (keyword)=>{
		return new Promise(function(resolve, reject) {
			var query_string = '';
			if(keyword &&  keyword.length > 0){
				query_string += ` HAVING LOWER(company_name) LIKE '%${keyword.toLowerCase()}%'`;
			} 
			var sql = `
			SELECT MAX(company_id) as company_id,company_name 
			FROM tbl_master_company GROUP BY company_name
			${query_string} ORDER BY company_name ASC`;
			
			db.any(sql,[keyword])
		    .then(function (data) {
		      resolve(data)
		    })
		    .catch(function (err) {
		    	var errorText = common.getErrorText(err);
				var error     = new Error(errorText);
				reject(error);
		    });

		}); 

	},
	emailCodeTemplate: async (email_code,lang)=>{
		return new Promise(function(resolve, reject) {
			db.any(`SELECT * from tbl_master_emails 
				WHERE email_code=($1) 
				AND language=($2)
				AND status = 1`, [email_code, lang])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	emailSendLog: async (email_id,email_code,mailFrom,mailTo,fromType,toType,mailSubject,mailHtml,senddate,ccMail)=>{
		return new Promise(function(resolve, reject) {
		var sql = 
		`INSERT INTO 
		tbl_email_logs(
			email_id,email_code,from_email,to_email,from_type,to_type,mail_subject,mail_content,mail_sent_date,status,cc_email
		) 
		VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) 
		RETURNING email_log_id`;
		db.one(sql,[email_id,email_code,mailFrom,mailTo,fromType,toType,mailSubject,mailHtml,senddate,1,ccMail])
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error     = new Error(errorText);
					reject(error);
				});

		}); 

	},
	update_tour_counter: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter = tour_counter + 1
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	tour_done: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter = 4
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	update_tour_counter_new: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter_new = tour_counter_new + 1
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	tour_done_new: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter_new = 4
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},	
	update_tour_counter_closed_task: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter_closed_task = tour_counter_closed_task + 1
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});
		});
	},
	tour_done_closed_task: async (customer_id) => {
		return new Promise(function (resolve, reject) {
			console.log('customer_id',customer_id);
			db.any(`UPDATE tbl_customer				
			SET tour_counter_closed_task = 4
			WHERE customer_id=($1)`, [customer_id],(r) => r.rowCount)
				.then(function (data) {
					resolve(data);
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	},
	get_cqt_product_id: async (product_id) => {
		return new Promise(function (resolve, reject) {
			db.any(`SELECT cqt_product_id FROM tbl_master_product WHERE product_id=($1)`, [product_id],(r) => r.rowCount)
				.then(function (data = []) {
					if(data.length > 0 && data[0].cqt_product_id > 0){
						resolve(data[0].cqt_product_id);
					}else{
						resolve(0);
					}
					
				})
				.catch(function (err) {
					var errorText = common.getErrorText(err);
					var error = new Error(errorText);
					reject(error);
				});

		});

	}

	// ,
	// get_department_admins: async (role_arr)=>{
	// 	return new Promise(function(resolve, reject) {

	// 		var sql = `
	// 		SELECT 
	// 			DISTINCT(C.customer_id),
	// 			C.first_name,
	// 			C.last_name 
	// 		FROM tbl_customer AS C 
	// 			LEFT JOIN tbl_customer_role AS ROLE
	// 		ON C.customer_id = ROLE.customer_id 
	// 		WHERE 
	// 			ROLE.role_id IN ($1:csv)
	// 			AND ROLE.status = 1
	// 			AND C.status    = 1
	// 			AND C.role_type = 0`;

	// 		db.any(sql,[role_arr])
	// 	    .then(function (data) {
	// 	      resolve(data)
	// 	    })
	// 	    .catch(function (err) {
	// 	    	var errorText = common.getErrorText(err);
	// 			var error     = new Error(errorText);
	// 			reject(error);
	// 	    });

	// 	}); 

	// }
}
