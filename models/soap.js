const db = require('../configuration/dbConn');
const common = require('../controllers/common');
const dateFormat = require('dateformat');
const Entities = require("html-entities").AllHtmlEntities;
const entities = new Entities();
var trim = require("trim");
const Config = require('../configuration/config');





module.exports = {
    insertRequestLog: async (logObj) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_sap_log(request,response,method,url,datetime,request_type) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id', [logObj.request, logObj.response, logObj.method, logObj.url, logObj.datetime, logObj.request_type])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    updateRequestLog: async (logid, response, reason) => {

        return new Promise(function (resolve, reject) {
            db.any('UPDATE tbl_sap_log SET response = $2 , reason = $3 WHERE id = $1', [logid, response, reason])
                .then(function (data) {
                    resolve({
                        success: true
                    });
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    add_inspection_lot: async (data) => {
       // console.log('++++data++++',data);
        return new Promise(function (resolve, reject) {

            db.one('INSERT INTO tbl_tasks_coa_sap(invoice_number,sales_order_number,batch_number, inspection_lot_number,material_number,date_added,customer_no) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING  id', [data.invoice_number, data.sales_order_number, data.batch_number, data.inspection_lot_number, data.material_number, data.date_added, data.customer_no])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    get_old_product: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.any('select material_code,id from  tbl_master_sap_product where material_code = $1 AND sorg = $2 AND dchl = $3 AND status = 1', [req_data.material_code, req_data.sorg, req_data.dchl])
                .then(function (data) {
                    if (data.length > 0) {
                        resolve(data)
                    } else {
                        resolve([{
                            id: 0
                        }])
                    }


                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    add_product: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_master_sap_product(material_code,material_description,sorg,sorg_desc,dchl,dchl_desc,div,div_desc,x_plntmat_status,x_distchain_status,distchainspecstatus,created_date,created_by,last_chg,changed_by,mtyp,bun,date_added,plant,status,date_updated) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21) RETURNING id', [req_data.material_code, req_data.material_description, req_data.sorg, req_data.sorg_desc, req_data.dchl, req_data.dchl_desc, req_data.div, req_data.div_desc, req_data.x_plntmat_status, req_data.x_distchain_status, req_data.distchainspecstatus, req_data.created_date, req_data.created_by, req_data.last_chg, req_data.changed_by, req_data.mtyp, req_data.bun, req_data.date_added, req_data.plant, req_data.status, req_data.date_updated])
                .then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    update_product: async (req_data, id) => {

        return new Promise(function (resolve, reject) {
            db.one(`UPDATE tbl_master_sap_product set material_description = $1 ,sorg_desc = $2 ,dchl_desc = $3,div = $4,div_desc = $5,x_plntmat_status = $6,x_distchain_status = $7 ,distchainspecstatus = $8,mtyp = $9 ,bun = $10 ,plant = $14 , date_updated = $15 WHERE material_code = $11 AND sorg = $12 AND dchl = $13 AND id = $16 RETURNING id`, [req_data.material_description, req_data.sorg_desc, req_data.dchl_desc, req_data.div, req_data.div_desc, req_data.x_plntmat_status, req_data.x_distchain_status, req_data.distchainspecstatus, req_data.mtyp, req_data.bun, req_data.material_code, req_data.sorg, req_data.dchl, req_data.plant, req_data.date_updated, id])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    update_chenge_data : async (last_chg, changed_by,id) => {

        return new Promise(function (resolve, reject) {
            db.any(`UPDATE tbl_master_sap_product set last_chg = $1 ,changed_by = $2 where id = $3 RETURNING id`, [last_chg, changed_by,id])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    get_compare_data: async (id) => {

        return new Promise(function (resolve, reject) {
            db.any('select material_code ,x_plntmat_status ,x_distchain_status,distchainspecstatus from tbl_master_sap_product where id = $1 and status = 1', [id])
                .then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    update_product_status: async (material_code, date) => {
        //console.log("this array : ",arr)


        return new Promise(function (resolve, reject) {
            db.any('update tbl_master_sap_product  set status = 0 , date_updated = $2 where material_code = $1 ', [material_code, date])
                .then(function (data) {
                    db.any("DELETE FROM tbl_company_sap_product WHERE product_sku = $1",[material_code])
                    .then(function (data1){
                        resolve(data);
                        // console.log("this is returning : ",data);
                    })
                    .catch(function (err) {
                        var errorText = common.getErrorText(err);
                        var error = new Error(errorText);
                        reject(error);
                    });

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    add_coa: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_tasks_coa(customer_name,batch_no,inspection_lot_no,coa_file,date_added,source,ip,processed,material_id,customer_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING coa_id', [req_data.customer_name, req_data.batch_number, req_data.lot_no, req_data.file_path, req_data.date_added, '1', req_data.ip, req_data.processed, req_data.material_id, req_data.customer_id])
                .then(function (data) {
                    resolve(data);
                    //console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    check_invoice: async (invoice_number, sap_so_number) => {

        return new Promise(function (resolve, reject) {
            db.any('select * from  tbl_task_invoice where invoice_number = $1 AND sales_order_no = $2 ', [invoice_number, sap_so_number])
                .then(function (data) {
               
                     resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    check_sales_order : async (sales_order, item) => {

        return new Promise(function (resolve, reject) {
            db.any('select id from  tbl_sales_order_sap where sales_order = $1 AND item = $2 ', [sales_order, item])
                .then(function (data) {
               
                     resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    check_inspection : async (inspection_lot_number, batch_number,material_number) => {

        return new Promise(function (resolve, reject) {
            db.any('select id from  tbl_tasks_coa_sap where inspection_lot_number = $1 AND batch_number = $2  AND material_number = $3 ', [inspection_lot_number, batch_number,material_number])
                .then(function (data) {
               
                     resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    insertInvoice: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_task_invoice(invoice_number,invoice_location,sales_order_no,invoice_file_name,invoice_file_path,invoice_date,date_added,date_updated) values ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id', [req_data.invoice_number, req_data.invoice_location, req_data.sap_so_number, req_data.invoice_file_name, req_data.invoice_file_path, req_data.invoice_date, req_data.date_added, req_data.date_updated])
                .then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    packing_exists: async (invoice_number,sales_order_no) => {
         //console.log("+++++invoice_number +++++",invoice_number);
         //console.log("+++++sales_order_no +++++",sales_order_no);


        return new Promise(function (resolve,reject) {
            db.any('select packing_file_path from  tbl_task_invoice where invoice_number = $1 AND sales_order_no = $2 ', [invoice_number,sales_order_no])
                .then(function (data) {
                    console.log("hellloooo ++++++++++++++",data);

                    if (data[0].packing_file_path != '' && data[0].packing_file_path != null ) {
                        resolve({
                            success: true
                        })
                    } else {
                        resolve({
                            success: false
                        })
                    }


                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    updatePacking: async (data_obj, invoice_number,sales_order_no) => {
        console.log(invoice_number);
        console.log(sales_order_no);
        console.log("+++++++",data_obj);

        

        return new Promise(function (resolve, reject) {
            db.any('UPDATE tbl_task_invoice set packing_date = $1 , packing_location = $2 , packing_file_name = $3 , packing_file_path = $4 , date_updated = $6 where invoice_number = $5 and sales_order_no = $7 RETURNING id', [data_obj.packing_date, data_obj.packing_location,  data_obj.packing_file_name, data_obj.packing_file_path, invoice_number, data_obj.date_updated,sales_order_no])
                .then(function (data) {  
                    if (data.length > 0) {
                        resolve({
                            success: true
                        })
                    } else {
                        resolve({
                            success: false
                        })
                    }


                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    get_customer_relation: async (req_data) => {
        //console.log("|||||+++||||||",req_data);
        return new Promise(function (resolve, reject) {
            db.any('select customer ,customer_id  from  tbl_sap_customer_master where customer = $1 and sorg = $2 and dchl = $3 and funct = $4 and parc = $5 and status = $6  ', [req_data.customer, req_data.sorg, req_data.dchl, req_data.funct, req_data.parc,'1'])
                .then(function (data) {
                    resolve(data)
                    //console.log("hello ++++++++++++ hello ++++++++++++++");

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    add_customer: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_sap_customer_master(customer,name1,sorg,sorg_desc,dchl,dchl_desc,dv,dv_desc,funct,funct_desc,parc,customer_ship_to,incot,inco2,curr,payt,cty,cty_name,city,postalcode,street,date,created_by,customer_group,date_added,date_updated,status) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27) RETURNING customer_id', [req_data.customer, req_data.name1, req_data.sorg, req_data.sorg_desc, req_data.dchl, req_data.dchl_desc, req_data.dv, req_data.dv_desc, req_data.funct, req_data.funct_desc, req_data.parc, req_data.customer_ship_to, req_data.incot, req_data.inco2, req_data.curr, req_data.payt, req_data.cty, req_data.cty_name, req_data.city, req_data.postalcode, req_data.street, req_data.date, req_data.created_by, req_data.customer_group, req_data.date_added, req_data.date_updated, req_data.status])
                .then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    update_customer: async (req_data, customer_id) => {


        return new Promise(function (resolve, reject) {
            db.one(`UPDATE tbl_sap_customer_master set name1 = $1 ,sorg_desc = $2 ,dchl_desc = $3,dv = $4,dv_desc = $5,funct_desc = $6,parc = $7 ,incot = $8,inco2 = $9,curr = $10,payt = $11,cty = $12 ,cty_name = $13 ,city = $14,postalcode = $15 , street = $16 ,date = $17 ,created_by = $18 ,customer_group = $19 , date_updated = $21 , status = $22 WHERE  customer_id = $20 RETURNING customer_id`, [req_data.name1,
                    req_data.sorg_desc,
                    req_data.dchl_desc,
                    req_data.dv,
                    req_data.dv_desc,
                    req_data.funct_desc,
                    req_data.parc,
                    req_data.incot,
                    req_data.inco2,
                    req_data.curr,
                    req_data.payt,
                    req_data.cty,
                    req_data.cty_name,
                    req_data.city,
                    req_data.postalcode,
                    req_data.street,
                    req_data.date,
                    req_data.created_by,
                    req_data.customer_group,
                    customer_id,
                    req_data.date_updated,
                    req_data.status
                ])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });
    },
    getParticularRequest: async (request_id, reservation_no, so_id) => {
        return new Promise(function (resolve, reject) {
            db.any('SELECT tbl_sap_request.*,tbl_tasks.task_id,tbl_tasks.language from tbl_sap_request LEFT JOIN tbl_tasks ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE tbl_sap_request.request_id = $1 AND tbl_sap_request.reservation_no = $2 AND tbl_sap_request.status = 1 AND tbl_tasks.so_id= $3 AND tbl_tasks.status = 1 AND tbl_tasks.close_status = 0', [request_id, reservation_no, so_id])
                .then(function (data) {
                    resolve(data[0])
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });

        });
    },
    updateSONumber: async (task_id, so_number, update_date_time = '') => {
        return new Promise(function (resolve, reject) {
            let today = '';
            if (update_date_time) {
                today = update_date_time;
            } else {
                today = common.currentDateTime();
            }
            db.any('UPDATE tbl_tasks SET sales_order_no = $2,sales_order_date= $3 WHERE task_id = $1', [task_id, so_number, today])
                .then(function (data) {
                    resolve({
                        success: true
                    });
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });

        });
    },
    cancelTask: async (task_id, cancel_comment = "", today = '') => {
        return new Promise(function (resolve, reject) {
            if (today == '') {
                today = common.currentDateTime();
            }
            db.any('UPDATE tbl_tasks SET cancel_reservation = 1,cancel_comment = $2,cancel_reservation_date= $3 WHERE task_id = $1', [task_id, cancel_comment, today])
                .then(function (data) {
                    resolve({
                        success: true
                    });
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });

        });
    },
    update_status: async (status, invoice_number) => {
        return new Promise(function (resolve, reject) {

            db.one('UPDATE tbl_task_invoice SET payment_status = $1 WHERE invoice_number  = $2 returning id', [status, invoice_number])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });

        });
    },
    add_sales_order: async (req_data) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_sales_order_sap(sales_order,customer_po_number,creation_date,sales_document_type,customer,item,material,net_value,currency,order_quantity,order_unit,date_added) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id', [req_data.sales_order, req_data.customer_po_number, req_data.creation_date, req_data.sales_document_type, req_data.customer, req_data.item, req_data.material, req_data.net_value, req_data.currency, req_data.order_quantity, req_data.order_unit,req_data.date_added])
                .then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    sold_to_data_exists: async (customer) => {

        return new Promise(function (resolve, reject) {
            db.any('SELECT sap_ref_no as sold_to from tbl_master_company_soldto where sap_ref_no = $1 ', [customer])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    sold_to_ship_to_relation: async (customer) => {

        return new Promise(function (resolve, reject) {
            db.any('SELECT shipto_id from tbl_company_shipto where soldto_id = $1 ', [customer])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    relation: async (soldto_id, shipto_id) => {

        return new Promise(function (resolve, reject) {
            db.any('SELECT shipto_id from tbl_company_shipto where soldto_id = $1 and shipto_id = $2 ', [soldto_id, shipto_id])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    update_relation_ship_sold: async (relObj) => {

        return new Promise(function (resolve, reject) {
            db.one('UPDATE tbl_company_shipto set country = $2 , city= $3 , post_code = $4 ,street=$5, shipto_name = $7 where  shipto_id = $1 and soldto_id = $6 RETURNING shipto_id', [
                    relObj.shipto_id, relObj.country, relObj.city, relObj.post_code, relObj.street, relObj.soldto_id, relObj.shipto_name
                ])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    delete_relation_sold_ship : async (sold_to,ship_to) => {

        return new Promise(function (resolve, reject) {
            db.any('DELETE FROM tbl_company_shipto WHERE  soldto_id = $1 AND shipto_id = $2', [sold_to,ship_to])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },

    add_relation_ship_sold: async (relObj) => {

        return new Promise(function (resolve, reject) {
            db.one('INSERT INTO tbl_company_shipto(shipto_id,country,city,post_code,street,soldto_id,shipto_name) values ($1,$2,$3,$4,$5,$6,$7) RETURNING shipto_id ', [
                    relObj.shipto_id, relObj.country, relObj.city, relObj.post_code, relObj.street, relObj.soldto_id, relObj.shipto_name
                ])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        });

    },
    check_shipto_product: async (shipto_id) => {
        return new Promise(function (resolve, reject) {
               db.any(`select distinct(material_master) as material_master  from tbl_sap_customer_product_master where ship_to_party = $1 `, [shipto_id]).then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    delete_shipto_product: async (shipto_id) => {
        return new Promise(function (resolve, reject) {
            db.any(`DELETE FROM tbl_company_sap_product WHERE shipto_id = $1 `, [shipto_id]).then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    add_shipto_product: async (shipto_id, product_sku) => {
        return new Promise(function (resolve, reject) {
            db.any(`INSERT INTO tbl_company_sap_product(shipto_id,product_sku) values($1,$2) `, [shipto_id, product_sku]).then(function (data) {
                    resolve(data);
                    // console.log("this is returning : ",data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    update_payment_status: async (invoice_number, payment_status, date,sales_order_no) => {
        return new Promise(function (resolve, reject) {
            db.one('update tbl_task_invoice  set payment_status = $2 , date_updated = $3 where invoice_number = $1 and sales_order_no = $4 RETURNING id ', [invoice_number, payment_status, date,sales_order_no])
                .then(function (data) {
                    resolve(data);
                    console.log("this is returning : ", data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
   
    update_logistics_partner : async (invoice_number, logistics_partner, date,sales_order_no) => {
        return new Promise(function (resolve, reject) {
            db.one('update tbl_task_invoice  set logistics_partner = $2 , date_updated = $3 where invoice_number = $1 AND sales_order_no = $4 RETURNING id ', [invoice_number, logistics_partner, date,sales_order_no])
                .then(function (data) {
                    resolve(data);
                    console.log("this is returning : ", data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    check_logistic : async (invoice_number,sales_order_no) => {
        return new Promise(function (resolve, reject) {
            db.one('select logistics_partner from  tbl_task_invoice  where invoice_number = $1 AND sales_order_no = $2 ', [invoice_number,sales_order_no])
                .then(function (data) {
                    resolve(data);
                    console.log("this is returning : ", data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
  
    get_all_sap_order: async () => {
        return new Promise(function (resolve, reject) {
            db.any('SELECT * FROM tbl_sales_order_sap WHERE sales_order != $1 AND status = 0 ORDER BY id ASC', ['?'])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    is_so_number_exist: async (so_number) => {
        return new Promise(function (resolve, reject) {
            db.any('SELECT * FROM tbl_tasks WHERE sales_order_no = $1', [so_number])
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    update_sales_order_table: async (so_number) => {
        return new Promise(function (resolve, reject) {
            db.one('update tbl_sales_order_sap set status = 1 where id = $1 RETURNING id ', [so_number])
                .then(function (data) {
                    resolve(data);
                    console.log("this is returning : ", data);

                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error = new Error(errorText);
                    reject(error);
                });
        })
    },
    addCustomerResponse: async (task)=>{
        return new Promise(function(resolve, reject) {
            
                db.one('INSERT INTO tbl_customer_response(task_id,customer_id,required_action,status,expected_closure_date,resp_status,date_added,pause_sla,status_id,action_id,approval_status, po_number) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING rs_id',[task.task_id,task.customer_id,task.action_req,task.status,task.expected_closure_date,1,task.date_added,0,7,1,1, task.po_number])
                .then(function (data) { 
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error     = new Error(errorText);
                    reject(error);
                });
        });
    },
    addCronTaskRequest: async (task)=>{
        return new Promise(function(resolve, reject) {
            
                db.one('INSERT INTO tbl_tasks(customer_id,request_type,due_date,parent_id,content,date_added,product_id,country_id,quantity,submitted_by,rdd,status,close_status,owner,current_ownership,language,ccp_posted_with,sales_order_no,sales_order_date) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9, $10,$11,$12,$13,$14, $15, $16,$17,$18,$19) RETURNING task_id',[task.customer_id,task.request_type,task.due_date,task.parent_id,task.content,task.current_date,task.product_id,task.country_id,task.quantity,task.submitted_by,task.rdd,1,0,0,0,task.language,task.ccp_posted_with, task.sales_order_no, task.sales_order_date])
                .then(function (data) { 
                    resolve(data)
                })
                .catch(function (err) {
                    var errorText = common.getErrorText(err);
                    var error     = new Error(errorText);
                    reject(error);
                });
        });
    },
    get_all_old_sap_order: async (expiry_date)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_tasks.* from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE tbl_tasks.request_type = 43 AND tbl_sap_request.status = 1 AND tbl_tasks.status = 1 AND tbl_sap_request.po_status != 1 AND cancel_reservation = 0 AND tbl_sap_request.request_date < $1",[expiry_date])
			  .then(function (data) {		
				resolve(data);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
    get_stock_task_details: async (task_id)=>{		
		return new Promise(function (resolve, reject) {
			db.any("SELECT tbl_tasks.sap_request_id,tbl_tasks.so_id, tbl_sap_request.request_id,tbl_sap_request.reservation_no  from tbl_tasks LEFT JOIN tbl_sap_request ON tbl_sap_request.id = tbl_tasks.sap_request_id WHERE task_id = $1 AND tbl_tasks.status = 1",[task_id])
			  .then(function (data) {	
				resolve(data[0]);				
			  })
			  .catch(function (err) {
				var errorText = common.getErrorText(err);
				var error = new Error(errorText);
				reject(error);
			  });
		});
	},
}