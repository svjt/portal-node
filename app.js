var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');


var userRoute = require('./routes/user');
var taskRoute = require('./routes/task');
var agentRoute = require('./routes/agents');
var productRoute = require('./routes/product');
var sapRoute = require('./routes/sap');
var faRoute = require('./routes/fa');
var logisticRoute = require('./routes/logistic');

var app = express();

//mail
const Config = require('./configuration/config');
const common = require('./controllers/common');

// NEW NPM
const nocache = require('nocache')
const helmet = require('helmet')
const frameguard = require('frameguard')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true })); //SVJT FOR OUTLOOKs
app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('etag');


app.use(nocache())
app.use(helmet.noSniff())
app.use(frameguard())
app.use(helmet.xssFilter())

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use('/api', userRoute);
app.use('/api/tasks', taskRoute);
app.use('/api/agents', agentRoute);
app.use('/api/products', productRoute);
app.use('/api/sap', sapRoute);
app.use('/api/fa', faRoute);
app.use('/api/logistic',logisticRoute);

app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;

  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);

  console.log('err', err);

  res.render('error');

  common.sendMail({
    from: Config.webmasterMail, // sender address
    to: 'soumyadeep@indusnet.co.in', // list of receivers
    subject: `URL || ${Config.drupal.url} || Site Error`, // Subject line
    html: `Error: ${JSON.stringify(err)} Url: ${req.path}`// plain text body
  });

  res.json({
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {}
  })
});

module.exports = app;
