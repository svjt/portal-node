/* Load Express */
const express = require('express');
/* Load Router */
const router = require('express-promise-router')();
/* Load Passport */
const passport = require('passport');
/* Load Passport Config */
const passportConf = require('../passport');
/* Load Form validation */
const { validateBody, validateParam, schemas, schema_posts} = require('../helpers/sapValidate');
/* Database Validation Schema */
const validateDbBody = require('../helpers/sapDbValidate');
/* Product Controller */
const sapController = require('../controllers/sap');
/* User Controller */
const userController = require('../controllers/user');
/*  SAP AUTH Token */
const passportSAP   = passport.authenticate('SAP', { session: false });
/* Load Token */
const passportSignIn = passport.authenticate('localUser', {
  session: false
});
const passportJWT = passport.authenticate('jwtUser', {
  session: false
});

router.route('/token').post(sapController.get_token);

router.route('/check_stock_availability').post(passportJWT, userController.handle_auth, validateBody(schemas.check_stock_availability), sapController.check_stock_availability);
router.route('/reserve_stock').post(passportJWT, userController.handle_auth, validateBody(schemas.reserve_stock), sapController.reserve_stock);
router.route('/approve_po').post(passportJWT, userController.handle_auth, validateBody(schemas.approve_po), sapController.approve_po);
router.route('/delay_po').post(passportJWT, userController.handle_auth, validateBody(schemas.delay_po), sapController.delay_po);
router.route('/more_stock_availability').post(passportJWT, userController.handle_auth, validateBody(schemas.more_stock_availability), validateDbBody.more_stock_availability, sapController.more_stock_availability);
router.route('/enquiry_cancel').post(passportJWT, userController.handle_auth, validateBody(schemas.enquiry_cancel), sapController.enquiry_cancel);
router.route('/remove_reservation').post(passportJWT, userController.handle_auth, validateBody(schemas.remove_reservation), sapController.remove_reservation);

//router.route('/update_reservation').post(passportSAP,sapController.update_reservation);

router.get('/products', passportJWT, userController.handle_auth, sapController.list_products);
router.get('/products_code/:id', passportJWT, userController.handle_auth,validateParam(schemas.product_id),validateDbBody.products_details, sapController.list_products_code);
router.get('/shipto', passportJWT, userController.handle_auth, sapController.list_shipto);
router.get('/soldto', passportJWT, userController.handle_auth, sapController.list_soldto);
router.get('/my_enquiries', passportJWT, userController.handle_auth, sapController.list_enquiries);
router.get('/stock_task_details/:id', passportJWT, userController.handle_auth,validateParam(schemas.stock_task_details), sapController.stock_task_details);
router.post('/post_invoice', validateBody(schemas.fetch_invoice),sapController.fetch_invoice_sap);
//router.get('/get_invoice_file', sapController.fetch_invoice_file_sap);


/* For New SAP Request */
router.route('/check_stock_availability_new').post(passportJWT, userController.handle_auth, validateBody(schemas.check_stock_availability_new), validateDbBody.check_stock_availability, sapController.check_stock_availability_new);
router.route('/reserve_stock_new').post(passportJWT, userController.handle_auth, validateBody(schemas.reserve_stock_new), validateDbBody.reserve_stock, sapController.reserve_stock_new);
router.route('/customer_shipto').post(passportJWT, userController.handle_auth,validateBody(schemas.customer_shipto), sapController.list_customer_shipto);
router.get('/customer_products/:shipto_id', passportJWT, userController.handle_auth,validateParam(schemas.shipto_id), validateDbBody.product_shipto_exist, sapController.list_customer_products);
router.get('/customer_products_code/:id', passportJWT, userController.handle_auth,validateParam(schemas.product_id),validateDbBody.products_details, sapController.list_customer_products_code);
router.get('/customer_country/:shipto_id', passportJWT, userController.handle_auth,validateParam(schemas.shipto_id), validateDbBody.shipto_exist, sapController.get_customer_shipto_country);
router.get('/customer_soldto/:shipto_id', passportJWT, userController.handle_auth,validateParam(schemas.shipto_id), validateDbBody.shipto_exist, sapController.get_customer_shipto_soldto);
router.route('/customer_approve_po').post(passportJWT, userController.handle_auth, schema_posts.approve_po_request, validateDbBody.request_exist, sapController.customer_approve_po);
router.route('/customer_delay_po').post(passportJWT, userController.handle_auth, validateBody(schemas.delay_po), validateDbBody.request_exist, sapController.customer_delay_po);

module.exports = router;
