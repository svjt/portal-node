const router = require('express-promise-router')();
/* Passport Library */
const passport = require('passport');
/* Passport Configuration */
const passportConf = require('../passport');
/* Validation Schema */
const { validateBody, validateParam, schemas, schema_posts } = require('../helpers/faValidate');
/* Database Validation Schema */
const validateDbBody = require('../helpers/faDbValidate');
/* User Controller */
const userController = require('../controllers/user');
/* Fa Controller */
const faController = require('../controllers/fa');
/* Import Passport JWT object */
const passportJWT = passport.authenticate('jwtUser', { session: false });

/* GET users listing. */
router.post('/suggest', passportJWT, userController.handle_auth, validateBody(schemas.suggest), faController.suggest);

router.post('/search', passportJWT, userController.handle_auth, validateBody(schemas.search), faController.search);

router.post('/search_tracking', passportJWT, userController.handle_auth, validateBody(schemas.search_tracking), faController.insest_fa_search);

router.post('/search_download', passportJWT, userController.handle_auth, validateBody(schemas.search), faController.search_download);

router.get('/task_enquiry', passportJWT, userController.handle_auth, faController.task_enquiry);

router.route('/rating-options')
.get(passportJWT,userController.handle_auth,faController.get_rating_options);

router.route('/check-rating')
.get(passportJWT,userController.handle_auth,faController.check_rating);

router.get('/enquiry_details/:id', passportJWT, userController.handle_auth,validateParam(schemas.task_enquiry), validateDbBody.check_task_enquiry_details, faController.enquiry_details);

router.route('/get_file/:ciphered_key')
.get(validateParam(schemas.download_file),faController.download_file);

//router.post('/task_enquiry_details', passportJWT, userController.handle_auth, validateBody(schemas.task_enquiry), validateDbBody.task_enquiry, faController.task_enquiry_details);

router.post('/get_excipients', passportJWT, userController.handle_auth, validateBody(schemas.get_excipients), validateDbBody.get_excipients, faController.get_excipients_list);

router.post('/request_more_info', passportJWT, userController.handle_auth, validateBody(schemas.request_more_info), validateDbBody.request_more_info, faController.request_more_info);

router.route('/post-ratings')
.post(passportJWT,userController.handle_auth,validateDbBody.post_rating,faController.rate_task);

module.exports = router;
