/*
	coming from drupal end
*/
const express = require('express');
const router  = require('express-promise-router')();
const bluedartController  = require('../controllers/push_bluedart');
const {
  validateBody,
  validateParam,
  schemas,
  schema_posts,
} = require('../helpers/logisticValidate');


const passport       = require('passport');
const passportConf   = require('../passport');

const BasicLogistic = passport.authenticate('logisticBasic',{ session: false });

/**
  * @desc get push api data from bluedart
  * @param 
  * @return json
*/
router.route('/push_updates').post(BasicLogistic,schema_posts.lite_validate,validateBody(schemas.liteSchema),bluedartController.getBlueDartPushTrackData);


module.exports = router;