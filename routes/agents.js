/* Express Module */
const express = require('express');
/* Express Router */
const router = require('express-promise-router')();
/* Passport Library */
const passport = require('passport');
/* Passport Configuration */
const passportConf = require('../passport');
/* Validation Schema */
const { validateBody, validateParam, schemas, schema_posts } = require('../helpers/userValidate');
/* Database Validation Schema */
const validateDbBody = require('../helpers/userDbValidate');
/* Agent Controller */
const agentController = require('../controllers/agents');
/* User Controller */
const userController = require('../controllers/user');
/* Import Passport JWT object */
const passportJWT = passport.authenticate('jwtUser', { session: false });

/**
  * @desc List of companies
  * @param 
  * @return json
*/
router.get('/company', passportJWT, userController.handle_auth, agentController.list_company);  //AKJ

/**
  * @desc List of customers
  * @param 
  * @return json
*/
router.get('/customers', passportJWT, userController.handle_auth, agentController.list_company_customers);  //AKJ     //! Need to be corrected

/**
  * @desc List of cc customers email
  * @param 
  * @return json
*/
router.get('/cc_customers', passportJWT, userController.handle_auth, agentController.list_customer_cc);  //AKJ       //!Need to be corrected

/**
  * @desc Total number of notification with respect to a particular company
  * @param company_id
  * @return json
*/
router.get('/count_notification/:company_id', passportJWT, validateParam(schemas.notification), userController.handle_auth, agentController.count_notification);  //AKJ

/**
  * @desc List of notifications with respect to a particular company
  * @param company_id
  * @return json
*/
router.get('/list_notification/:company_id', passportJWT, validateParam(schemas.notification), userController.handle_auth, agentController.list_notification);  //AKJ

/**
  * @desc Update notification
  * @param 
  * @return json
*/
router.route('/update_notification')
  .put(passportJWT, userController.handle_auth, validateBody(schemas.update_notification), validateDbBody.update_notification, agentController.update_notification);  //AKJ


module.exports = router;
