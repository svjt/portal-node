const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const {
  validateBody,
  validateParam,
  schemas,
  schema_posts
} = require('../helpers/userValidate');
const validateDbBody = require('../helpers/userDbValidate');
const userController = require('../controllers/user');

const passportSignIn = passport.authenticate('localUser', {
  session: false
});
const passportSignInQA = passport.authenticate('localUserQA', {
  session: false
});
const passportJWT = passport.authenticate('jwtUser', {
  session: false
});
const passportJWTQA = passport.authenticate('jwtQAUser', {
  session: false
});
const passportShadow = passport.authenticate('shadowUser', {
  session: false
});
const passportCustResponse = passport.authenticate('customerResponse', {
  session: false
});
const passportCustResponseSecondStep = passport.authenticate('customerResponseSecondStep', {
  session: false
});


router.route('/contact')
  .post(passportJWT, userController.handle_auth, validateBody(schemas.contact_me), validateDbBody.contact, userController.contact_me);
/**
 * @desc Add Customer
 * @param 
 * @return json
 */
router.route('/user/add')
  .post(validateBody(schemas.add_user), validateDbBody.add_user, userController.add_user);

/**
 * @desc Forgot Password
 * @param 
 * @return json
 */
 //
router.route('/forgotpassword')
  .post(validateBody(schemas.forgotPassword), userController.forgotPasswordNew);

/**
 * @desc Reset Password
 * @param 
 * @return json
 */
router.route('/resetpassword')
  .post(validateBody(schemas.resetPassword), userController.resetPassword);

/**
 * @desc After registration
 * @param 
 * @return json
 */
router.route('/setpassword')
  .post(validateBody(schemas.setPassword), userController.setPassword);

/**
 * @desc Get customer team
 * @param 
 * @return json
 */
router.route('/team')
  .get(passportJWT, userController.handle_auth, userController.list_teams);

/**
 * @desc Send a query
 * @param 
 * @return json
 */



/**
 * @desc Send a query
 * @param 
 * @return json
 */
router.route('/sample_contact')
  .post(userController.sample_contact);

/**
 * @desc Validate a token
 * @param 
 * @return json
 */
router.route('/check-token')
  .post(validateBody(schemas.token_chck), userController.token_check);


/**
 * @desc Get User from token
 * @param 
 * @return json
 */
router.route('/check-token-pass')
  .post(validateBody(schemas.token_chck), userController.token_check_set_pass);




//done
// router.route('/update/:id')
// .put(passportJWT,userController.handle_auth,validateBody(schemas.update_admin),validateParam(schemas.update_admin_p),validateDbBody.update_admin,userController.update_admin);
//done
// router.route('/:id')
// .delete(passportJWT,userController.handle_auth,validateParam(schemas.delete_admin),userController.delete_admin);
//done
// router.route('/')
// .get(passportJWT,userController.handle_auth,userController.list_admin); 
//done
// router.route('/mydetails')
// .get(passportJWT,userController.handle_auth,userController.my_details); 

// router.route('/groups')
// .get(passportJWT,userController.handle_auth,userController.list_groups);

// router.route('/details/:id')
// .get(passportJWT,userController.handle_auth,validateParam(schemas.get_admin),userController.get_admin);
// //done
//done

/**
 * @desc Customer Login
 * @param 
 * @return json
 */
router.route('/login')
  .post(validateBody(schemas.authSchema), schema_posts.login, passportSignIn, userController.handle_login, userController.login);

router.route('/qa_login')
  .post(validateBody(schemas.authSchema), schema_posts.login_qa, passportSignInQA, userController.handle_login_qa, userController.login_qa);

router.route('/qa_form')
  .post(passportJWTQA, userController.handle_auth_qa,schema_posts.qa_form,validateDbBody.qa_form,userController.qa_form);

/**
 * @desc Generate a new token
 * @param 
 * @return json
 */
router.route('/generate_token')
  .get(passportJWT, userController.handle_auth, userController.generateToken);

router.route('/change_language')
  .post(passportJWT, userController.handle_auth, userController.changeLanguage);

/**
 * @desc Fetch Country List
 * @param 
 * @return json
 */
router.route('/country')
  .get(passportJWT, userController.handle_auth, userController.list_country);

router.route('/document_type')
  .get(passportJWT, userController.handle_auth, userController.list_document_type);

router.route('/product_unit')
  .get(passportJWT, userController.handle_auth, userController.list_product_unit);

router.route('/stability_data')
  .get(passportJWT, userController.handle_auth, userController.list_stability_data);

router.route('/country_open/:type')
  .get(userController.list_country_open);

router.route('/language_open/:type')
  .get(userController.list_language_open);

router.route('/language')
  .get(passportJWT, userController.handle_auth, userController.list_language);

/**
 * @desc Fetch Company list
 * @param 
 * @return json
 */
router.route('/company')
  .get(userController.list_company);

/**
 * @desc Fetch Products
 * @param 
 * @return json
 */
router.route('/products')
  .get(passportJWT, userController.handle_auth, userController.list_all_products);

/**
 * @desc Search from my request
 * @param keyword
 * @return json
 */
router.route('/request-search/:keyword')
  .get(passportJWT, userController.handle_auth, userController.search_my_requests);

/**
 * @desc Search from my order
 * @param keyword
 * @return json
 */
router.route('/order-search/:keyword')
  .get(passportJWT, userController.handle_auth, userController.search_my_order);

/**
 * @desc Search from my payment
 * @param keyword
 * @return json
 */
 router.route('/payment-search/:keyword')
 .get(passportJWT, userController.handle_auth, userController.search_my_payment);

/**
 * @desc Search from my complaint
 * @param keyword
 * @return json
 */
router.route('/complaint-search/:keyword')
  .get(passportJWT, userController.handle_auth, userController.search_my_complaint);

/**
 * @desc Search from my forecast
 * @param keyword
 * @return json
 */
router.route('/forecast-search/:keyword')
  .get(passportJWT, userController.handle_auth, userController.search_my_forecast);

/**
 * @desc Get My Request
 * @param 
 * @return json
 */
router.route('/request-list')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_requests);

/**
 * @desc Get My Complaints
 * @param 
 * @return json
 */
router.route('/complaint-list')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_complaints);

/**
 * @desc Get My Orders
 * @param 
 * @return json
 */
router.route('/order-list')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_order);

/**
 * @desc Get My Orders
 * @param 
 * @return json
 */
router.route('/order-list-new')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_order_new);

/**
 * @desc Get My Orders
 * @param 
 * @return json
 */
 router.route('/payment-list')
 .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_payment);

/**
 * @desc Get My Forecast
 * @param 
 * @return json
 */
router.route('/forecast-list')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.list_my_forecast);

/**
 * @desc Get Dashboard Overview
 * @param 
 * @return json
 */
router.route('/dashboard')
  .get(passportJWT, userController.handle_auth, validateDbBody.dashboard, userController.overview);
/**
 * @desc Get My Products
 * @param 
 * @return json
 */
router.route('/my-products')
  .get(passportJWT, userController.handle_auth, validateDbBody.my_product, userController.my_products);

/**
 * @desc Get My Account
 * @param 
 * @return json
 */
router.route('/user/show')
  .get(passportJWT, userController.handle_auth, userController.my_account);

/**
 * @desc Remove Profile Picture
 * @param 
 * @return json
 */
router.route('/remove/pic')
  .get(passportJWT, userController.handle_auth, userController.delete_prpfile_pic);

/**
 * @desc Update Profile Picture
 * @param 
 * @return json
 */
router.route('/update/pic')
  .post(passportJWT, userController.handle_auth, schema_posts.post_update_pic, userController.update_account);
/**
 * @desc Shadow Login
 * @param  
 * @return json
 */
router.route('/setToken')
  .get(passportShadow, userController.verify_shadow_token);

/**
 * @desc Customer Response
 * @param 
 * @return json
 */
router.route('/process_customer_response')
.get(passportCustResponse, validateDbBody.process_customer_response, userController.process_customer_response);

/**
 * @desc Customer Rejection
 * @param 
 * @return json 
 */
 router.route('/process_customer_rejection')
 .post(passportCustResponseSecondStep, validateBody(schemas.validate_customer_rejection), validateDbBody.validate_customer_rejection, userController.process_customer_rejection);


/**
 * @desc Get My Notifications count
 * @param 
 * @return json
 */
router.route('/count_notification')
  .get(passportJWT, userController.handle_auth, userController.count_notification);

/**
 * @desc List all my notifications
 * @param 
 * @return json
 */
router.route('/list_notification')
  .get(passportJWT, userController.handle_auth, userController.list_notification);
/**
 * @desc Change Read Status of My Notification
 * @param 
 * @return json
 */
router.route('/update_notification')
  .put(passportJWT, userController.handle_auth, validateBody(schemas.update_notification), validateDbBody.update_notification, userController.update_notification);

/**
 * @desc Update My Account
 * @param 
 * @return json
 */
router.route('/user/edit')
  .post(passportJWT, userController.handle_auth, schema_posts.post_update_self, validateDbBody.update_account, userController.update_account);

router.route('/feature-done')
  .post(passportJWT, userController.handle_auth, userController.update_feature);

router.route('/feature-send-mail-done')
  .post(passportJWT, userController.handle_auth, userController.update_feature_send_mail);

router.route('/feature-11-done')
  .post(passportJWT, userController.handle_auth, userController.update_feature_11);
  

/**
 * @desc Autosuggest for company while registering
 * @param keyword
 * @return json
 */
router.route('/company-search/:keyword?')
  .get(userController.search_my_company);

//done
// router.route('/update')
// .put(passportJWT,userController.handle_auth,validateBody(schemas.update_self),validateDbBody.update_self,userController.update_self);
// //done


module.exports = router;