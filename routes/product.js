/* Load Express */
const express = require('express');
/* Load Router */
const router = require('express-promise-router')();
/* Load Passport */
const passport = require('passport');
/* Load Passport Config */
const passportConf = require('../passport');
/* Load Form validation */
const {
  validateBody,
  validateParam,
  schemas,
  schema_posts
} = require('../helpers/productValidate');
/* Load Database validation */
const validateDbBody = require('../helpers/productDbValidate');

/* Product Controller */
const productController = require('../controllers/product');
/* User Controller */
const userController = require('../controllers/user');

/* Load Token */
const passportSignIn = passport.authenticate('localUser', {
  session: false
});
const passportJWT = passport.authenticate('jwtUser', {
  session: false
});

/**
 * @desc List of category
 * @param 
 * @return json
 */
router.route('/category')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.list_category); //AJ

/**
 * @desc List of stage
 * @param 
 * @return json
 */
router.route('/stage')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.list_stage);

/**
 * @desc List of technologies
 * @param 
 * @return json
 */
router.route('/technology')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.list_technology);

/**
 * @desc List of dosage
 * @param 
 * @return json
 */
router.route('/dosage')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.list_dosage);

/**
 * @desc List of all products
 * @param 
 * @return json
 */
router.route('/all')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.all_products);

/**
 * @desc List of my products
 * @param 
 * @return json
 */
router.route('/my_products')
  .get(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.my_products);

/**
 * @desc Search your Products - (POST)
 * @param 
 * @return json
 */
router.route('/my_products_all')
  .post(passportJWT, userController.handle_auth, validateDbBody.check_customer, productController.list_my_products);  

/**
 * @desc List of all Products --- (POST)
 * @param 
 * @return json
 */
router.route('/list_all')
  .post(passportJWT, userController.handle_auth, validateBody(schemas.search_products), validateDbBody.check_customer, productController.list_products);

// router.route('/details/:product_id')
// .get(passportJWT,userController.handle_auth,validateParam(schemas.product_details),validateDbBody.product_details,productController.product_details);

/**
 * @desc Product Details
 * @param product_id
 * @return json
 */
/* router.route('/:product_id')
  .get(passportJWT, userController.handle_auth, validateParam(schemas.product_details), validateDbBody.product_details, productController.product_details); */

router.route('/product_details')
  .post(passportJWT, userController.handle_auth, validateBody(schemas.product_details), validateDbBody.product_details, productController.product_details);

module.exports = router;