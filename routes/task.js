const express = require('express');
const router  = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const { validateBody,validateParam, schemas, schema_posts } = require('../helpers/taskValidate');
const validateDbBody = require('../helpers/taskDbValidate');
const userController = require('../controllers/user');
const taskController = require('../controllers/task');

const passportSignIn = passport.authenticate('localUser', { session: false });
const passportJWT    = passport.authenticate('jwtUser', { session: false });
const passportFileDownlodeJWT    = passport.authenticate('jwtFileDownlode', { session: false });

/**
  * @desc Create New Task
  * @param 
  * @return json
*/
router.route('/')
.post(passportJWT,userController.handle_auth,schema_posts.general_request,validateDbBody.tasks,taskController.add_task);  

/**
  * @desc Request Apostallation Document
  * @param 
  * @return json
*/
router.route('/apostallation-doc')
.post(passportJWT,userController.handle_auth,schema_posts.apostallation,validateDbBody.tasks,taskController.add_task);  

/**
  * @desc Request Vendor Questionnaire
  * @param 
  * @return json
*/
router.route('/vendor')
.post(passportJWT,userController.handle_auth,schema_posts.vendor,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request SPEC/MOA
  * @param 
  * @return json
*/
router.route('/spec-moa')
.post(passportJWT,userController.handle_auth,schema_posts.spec_moa,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Method Queries
  * @param 
  * @return json
*/
router.route('/method-queries')
.post(passportJWT,userController.handle_auth,schema_posts.method_queries,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Typical COA
  * @param 
  * @return json
*/
router.route('/typical-coa')
.post(passportJWT,userController.handle_auth,schema_posts.typical_coa,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Re-certification
  * @param 
  * @return json
*/
router.route('/recertification-coa')
.post(passportJWT,userController.handle_auth,schema_posts.rec_coa,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Audit/Visit
  * @param 
  * @return json
*/
router.route('/audit-visit')
.post(passportJWT,userController.handle_auth,schema_posts.audit_visit,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Customized Spec
  * @param 
  * @return json
*/
router.route('/customized-spec')
.post(passportJWT,userController.handle_auth,schema_posts.cust_spec,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Quality Equivalence
  * @param 
  * @return json
*/
router.route('/quality-equivalence')
.post(passportJWT,userController.handle_auth,schema_posts.quality_equivalence,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Stability Data
  * @param 
  * @return json
*/
router.route('/stability-data')
.post(passportJWT,userController.handle_auth,schema_posts.stability,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Elementary Impurity
  * @param 
  * @return json
*/
router.route('/elemental-impurity')
.post(passportJWT,userController.handle_auth,schema_posts.elementary,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Residual Solvents
  * @param 
  * @return json
*/
router.route('/residual-solvents-declaration')
.post(passportJWT,userController.handle_auth,schema_posts.residual,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Storage Transport Declaration
  * @param 
  * @return json
*/
router.route('/storage-transport-declaration')
.post(passportJWT,userController.handle_auth,schema_posts.transport,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Additional Declaration
  * @param 
  * @return json
*/
router.route('/request-additional-declarations')
.post(passportJWT,userController.handle_auth,schema_posts.additional,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request CDA/Sales Agreement
  * @param 
  * @return json
*/
router.route('/cda-sales-agreement')
.post(passportJWT,userController.handle_auth,schema_posts.cda_sales,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Quality Agreement
  * @param 
  * @return json
*/
router.route('/quality-agreement')
.post(passportJWT,userController.handle_auth,schema_posts.quality_agreement,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request LOC/LOA
  * @param 
  * @return json
*/
router.route('/request-for-loc-loe')
.post(passportJWT,userController.handle_auth,schema_posts.loc_loe,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request LOA
  * @param 
  * @return json
*/
router.route('/request-for-loa')
.post(passportJWT,userController.handle_auth,schema_posts.request_loa,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request CEP
  * @param 
  * @return json
*/
router.route('/request-for-cep-copy')
.post(passportJWT,userController.handle_auth,schema_posts.request_cep,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request DMF
  * @param 
  * @return json
*/
router.route('/request-for-dmf')
.post(passportJWT,userController.handle_auth,schema_posts.request_dmf,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Sample
  * @param 
  * @return json
*/
router.route('/samples-working-standards')
.post(passportJWT,userController.handle_auth,schema_posts.samples,validateDbBody.tasks,taskController.add_sample);
/**
  * @desc Request New Order
  * @param 
  * @return json
*/
router.route('/new-order')
.post(passportJWT,userController.handle_auth,schema_posts.new_order,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Proforma
  * @param 
  * @return json
*/
router.route('/proforma-invoice')
.post(passportJWT,userController.handle_auth,schema_posts.proforma_invoice,validateDbBody.tasks,taskController.add_task);

router.route('/order-general-request')
.post(passportJWT,userController.handle_auth,schema_posts.order_general_request,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Quality Complaints
  * @param 
  * @return json
*/
router.route('/quality-complaints')
.post(passportJWT,userController.handle_auth,schema_posts.complaints,validateDbBody.tasks,taskController.add_complain);
/**
  * @desc Request Forecast
  * @param 
  * @return json
*/
router.route('/forecast')
.post(passportJWT,userController.handle_auth,schema_posts.forecast,validateDbBody.forecast,taskController.create_forecast);

/**
  * @desc Request Queries Related to DMF
  * @param 
  * @return json
*/
router.route('/queries-related-to-dmf')
.post(passportJWT,userController.handle_auth,schema_posts.dmf_queries,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Queries related to Deficiencies
  * @param 
  * @return json
*/
router.route('/queries-related-to-deficiencies')
.post(passportJWT,userController.handle_auth,schema_posts.deficiency,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Queries related to Notifications
  * @param 
  * @return json
*/
router.route('/queries-related-to-notifications')
.post(passportJWT,userController.handle_auth,schema_posts.notification_queries,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request QOS
  * @param 
  * @return json
*/
router.route('/qos')
.post(passportJWT,userController.handle_auth,schema_posts.qos,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request Price Quote
  * @param 
  * @return json
*/
router.route('/price-quote')
.post(passportJWT,userController.handle_auth,schema_posts.price_quote,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Request IP Related Enquiries
  * @param 
  * @return json
*/
router.route('/ip-related-enquiries')
.post(passportJWT,userController.handle_auth,schema_posts.ip,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Master File Agreement 
  * @param 
  * @return json
*/
router.route('/master-file-agreement')
.post(passportJWT,userController.handle_auth,schema_posts.file_agreement,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Logistic Complaints
  * @param 
  * @return json
*/
router.route('/logistics-complaints')
.post(passportJWT,userController.handle_auth,schema_posts.logistics_complaints,validateDbBody.tasks,taskController.add_complain);  
/**
  * @desc Request Quality Complaints
  * @param 
  * @return json
*/
router.route('/quality-general-request')
.post(passportJWT,userController.handle_auth,schema_posts.general_request,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Quality Complaints
  * @param 
  * @return json
*/
router.route('/legal-general-request')
.post(passportJWT,userController.handle_auth,schema_posts.general_request,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Nitrosoamine
  * @param 
  * @return json
*/
router.route('/nitrosoamine')
.post(passportJWT,userController.handle_auth,schema_posts.nitrosoamine,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request TGA Clearance
  * @param 
  * @return json
*/
router.route('/tga')
.post(passportJWT,userController.handle_auth,schema_posts.tga,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Regulatory General
  * @param 
  * @return json
*/
router.route('/regulatory-general-request')
.post(passportJWT,userController.handle_auth,schema_posts.general_request,validateDbBody.tasks,taskController.add_task);
/**
  * @desc Request Legal General
  * @param 
  * @return json
*/
router.route('/genotoxic-impurity-assessment')
.post(passportJWT,userController.handle_auth,schema_posts.genotoxic,validateDbBody.tasks,taskController.add_task);

/**
  * @desc Get CC customer list company wise
  * @param 
  * @return json
*/

/**
  * @desc Request Apostallation Document
  * @param 
  * @return json
*/
router.route('/apostallation-doc')
.post(passportJWT,userController.handle_auth,schema_posts.apostallation,validateDbBody.tasks,taskController.add_task);

router.route('/pending-rating-dashboard')
.get(passportJWT,userController.handle_auth,taskController.get_all_pending_ratings);

router.route('/pending-rating-dashboard-explicit/:ciphered_key')
.get(passportJWT,userController.handle_auth,validateDbBody.task_details_explicit,taskController.get_explicit_pending_ratings);

router.route('/count-pending-rating-dashboard')
.get(passportJWT,userController.handle_auth,taskController.get_all_pending_ratings_counts);

router.route('/pending-rating')
.get(passportJWT,userController.handle_auth,taskController.get_other_pending_ratings);

router.route('/count-pending-rating')
.get(passportJWT,userController.handle_auth,taskController.get_other_pending_ratings_counts);

router.route('/pending-rating-complaints')
.get(passportJWT,userController.handle_auth,taskController.get_complaints_pending_ratings);

router.route('/count-pending-rating-complaints')
.get(passportJWT,userController.handle_auth,taskController.get_complaints_pending_ratings_counts);

router.route('/pending-rating-orders')
.get(passportJWT,userController.handle_auth,taskController.get_orders_pending_ratings);

router.route('/count-pending-rating-orders')
.get(passportJWT,userController.handle_auth,taskController.get_orders_pending_ratings_counts);

router.route('/pending-rating-forecast')
.get(passportJWT,userController.handle_auth,taskController.get_forecast_pending_ratings);

router.route('/count-pending-rating-forecast')
.get(passportJWT,userController.handle_auth,taskController.get_forecast_pending_ratings_counts);

router.route('/get-cc-customers')
.get(passportJWT,userController.handle_auth,taskController.get_cc);
/**
  * @desc Download Discussion files from S3
  * @param ciphered_key
  * @return json
*/

router.route('/get_file/:ciphered_key')
.get(validateParam(schemas.download_file),taskController.download_file);
/**
  * @desc Download Task files from S3
  * @param ciphered_key
  * @return json
*/
router.route('/get_task_file/:ciphered_key')
.get(validateParam(schemas.download_file),taskController.download_task_file);



router.route('/get_packing_task_file/:ciphered_key')
.get(validateParam(schemas.download_file),taskController.download_packing_task_file);

router.route('/get_invoice_task_file/:ciphered_key')
.get(validateParam(schemas.download_file),taskController.download_invoice_task_file);

router.route('/get-task-attachments/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.get_task_attachments);

/**
  * @desc Post a discussion
  * @param 
  * @return json
*/
router.route('/discussion')
.post(passportJWT,userController.handle_auth,schema_posts.add_discussion,validateDbBody.add_discussion,taskController.create_sub_task);
/**
  * @desc Get Selected CC Customers Company wise
  * @param id
  * @return json
*/
router.route('/get-cc-customers/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.get_updated_cc_list);

/**
  * @desc Update CC customers
  * @param 
  * @return json
*/
router.route('/update-cc')
.post(passportJWT,userController.handle_auth,validateDbBody.cc_list,taskController.update_cc);
/**
  * @desc Highlight a task when its data changes
  * @param id
  * @return json
*/
router.route('/update_task_highlight/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),taskController.update_task_highlight);

router.route('/rating-options/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),taskController.get_rating_options);

router.route('/download_ship_doc/:id/:type')
.get(passportFileDownlodeJWT,userController.handle_auth,validateParam(schemas.ship_doc),validateDbBody.ship_doc,taskController.task_ship_file_download); //
router.route('/get_coa_task_file/:ciphered_key')
.get(passportFileDownlodeJWT,userController.handle_auth,validateParam(schemas.download_file),validateDbBody.coa_task_file,taskController.download_coa_task_file);
router.route('/download_po_doc/:sapid')
.get(validateParam(schemas.po_doc),taskController.task_po_file_download);
/**
  * @desc Task Details
  * @param id
  * @return json
*/

router.route('/update_tour_counter_11')
.get(passportJWT,userController.handle_auth,taskController.update_tour_counter_11);



router.route('/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.task_details);

router.route('/get_task_status/:type/:lang')
.get(passportJWT,userController.handle_auth,taskController.get_task_status);

router.route('/get_task_action/:lang')
.get(passportJWT,userController.handle_auth,taskController.get_task_action);

router.route('/download_files/:hash_id')
.get(validateParam(schemas.hash_id),validateDbBody.task_files_download,taskController.task_files_download_all);

router.route('/download_discussion_uploads/:hash_id')
.get(validateParam(schemas.hash_id),validateDbBody.comment_files_download,taskController.discussion_download_all);

router.route('/download_discussion_files/:hash_id')
.get(validateParam(schemas.hash_id),validateDbBody.comment_files_download,taskController.discussion_files_download_all);




router.route('/post-ratings')
.post(passportJWT,userController.handle_auth,validateDbBody.task_details_rating,validateDbBody.post_rating,taskController.rate_task);

router.route('/skip-ratings')
.post(passportJWT,userController.handle_auth,validateDbBody.task_details_rating,validateDbBody.skip_rating,taskController.skip_task);

router.route('/closed-discussion')
.post(passportJWT,userController.handle_auth,schema_posts.closed_discussion,validateDbBody.task_details_rating,taskController.closed_discussion);

//MULTER ATTACHMENT AMAN
router.route('/send-mail-task-details-attachments/:id')
.post(passportJWT,userController.handle_auth,schema_posts.task_attachments,validateParam(schemas.task_details),validateDbBody.task_details,validateDbBody.check_size,taskController.send_mail_task_details_attachments);

// router.route('/send-mail-task-details-attachments/:id')
// .post(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,validateDbBody.check_size,taskController.send_mail_task_details_attachments);

router.route('/send-mail-escalation/:id')
.post(passportJWT,userController.handle_auth,schema_posts.escalation_files,validateParam(schemas.task_details),validateDbBody.task_details,taskController.send_mail_escalation);

router.route('/edit-rdd/:id')
.post(passportJWT,userController.handle_auth,validateBody(schemas.edit_rdd),validateParam(schemas.task_details),validateDbBody.task_details,taskController.edit_rdd);

router.route('/update_tour_counter/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.update_tour_counter);

router.route('/update_tour_counter_new/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.update_tour_counter_new);

router.route('/update_tour_counter_closed_task/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.update_tour_counter_closed_task);

router.route('/tour_done/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.tour_done);

router.route('/tour_done_new/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.tour_done_new);

router.route('/tour_done_closed_task/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.tour_done_closed_task);

router.route('/tour_done_11/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.tour_done_11);

// zip download from xceed
router.route('/download_file_zip_xceed/:hash_id').get(validateParam(schemas.hash_id),validateDbBody.task_files_download,taskController.task_files_download_all);
router.route('/download_comment_file_zip_xceed/:hash_id').get(validateParam(schemas.hash_id),validateDbBody.comment_files_download,taskController.discussion_download_all);
router.route('/download_internal_discussions_zip_xceed/:hash_id').get(validateParam(schemas.hash_id),validateDbBody.internal_discussion_files_download,taskController.download_internal_discussions_all);

router.route('/temp_task_orders/:id')
.get(passportJWT,userController.handle_auth,validateParam(schemas.task_details),validateDbBody.task_details,taskController.temp_task_orders);

/* approve or reject pre shipment invoice  */
router.route('/approve_invoice')
.post(passportJWT,userController.handle_auth,validateBody(schemas.approve_invoice),validateDbBody.task_details_by_id,taskController.approve_invoice);
/* approve or reject pre shipment COA  */
/*router.route('/approve_coa')
.post(passportJWT,userController.handle_auth,validateBody(schemas.approve_coa),validateDbBody.task_details_by_id,taskController.approve_coa);*/

module.exports = router;