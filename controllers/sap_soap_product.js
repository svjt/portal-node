

const Config     = require('../configuration/config');
const sapModel   = require("../models/sap");
const taskModel  = require('../models/task');
const common     = require('./common');
var auth         = require('basic-auth');
var compare      = require('tsscmp')

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();



module.exports = {

    MyService: {
        MyPort: {
            AddUpdateMaterial:async function(args, cb, headers, req) {
                // console.log("hello i am in products Add !!",args['Material_Details']);
                
              
            //   var credentials = auth(req);
            //   var valid = true;
              
            //   if (credentials) {
                 
            //     valid = compare(credentials.name, Config.sapAuth.username) && valid
            //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
            //     if(!valid){
            //         throw {
            //             Fault: {
            //               Code: {
            //                 Value: '401'
            //               },
            //               Reason: { Text: 'Authentication failed' },
            //               statusCode: 401
            //             }
            //           };
            //     }                
            //   }else{
            //     throw {
            //             Fault: {
            //               Code: {
            //                 Value: '401'
            //               },
            //               Reason: { Text: 'Authentication failed' },
            //               statusCode: 401
            //             }
            //           };
            //   } 
                
             
                let insLogObj = {
                    request : JSON.stringify(args),
                    response : '',
                    method : 'POST',
                    url : '',
                    datetime : common.currentDateTime(),
                    request_type : 'PRODUCT_ADD_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj); 
           
               let all_request = args['Material_Details'];
               let error = 0;
               let err_msg = '';
               reason = '';
               let success_message = '';
               let material_obj = []
               if(all_request != null){
                for(let index = 0; index <= (all_request.length - 1); index++) {
                    if(all_request[index]['material_code'] == '' || all_request[index]['material_description'] == '' || all_request[index]['sorg'] == '' ||  all_request[index]['dchl'] == '' ||  all_request[index]['div']== '' || all_request[index]['mtyp'] == '' || all_request[index]['bun']== '' || all_request[index]['plant']== '' ){
                       error ++;
                       err_msg = 'Something went wrong ,plese try again with proper data';
                       reason = '{"reason" : "can not get proper data"}'
                   }else{
              
                    if(all_request[index]['div'] == 30 && all_request[index]['mtyp'] == 'API'){
                        const scarchObj = {
                            material_code : entities.encode(all_request[index]['material_code']),
                            sorg : entities.encode(all_request[index]['sorg']),
                            dchl : entities.encode(all_request[index]['dchl'])    
                        }
                      
                        
                        const dataObj  ={
                            material_code : entities.encode(all_request[index]['material_code']),
                            material_description : all_request[index]['material_description'] ? entities.encode(all_request[index]['material_description']): '',
                            sorg : entities.encode(all_request[index]['sorg']),
                            sorg_desc : all_request[index]['sorg_desc'] ? entities.encode(all_request[index]['sorg_desc']) : '' ,
                            dchl : entities.encode(all_request[index]['dchl']),
                            dchl_desc :all_request[index]['dchl_desc'] ?  entities.encode(all_request[index]['dchl_desc']) : '',
                            div : all_request[index]['div'],
                            div_desc : all_request[index]['div_desc'] ?  entities.encode(all_request[index]['div_desc']) : '',
                            x_plntmat_status : all_request[index]['x_plntmat_status'] ? entities.encode(all_request[index]['x_plntmat_status']) : '' ,
                            x_distchain_status : all_request[index]['x_distchain_status'] ? entities.encode(all_request[index]['x_distchain_status']) : '',
                            distchainspecstatus : all_request[index]['distchainspecstatus'] ?  entities.encode(all_request[index]['distchainspecstatus']) : '',
                            created_date : all_request[index]['created_date'] ? entities.encode(all_request[index]['created_date'].slice(0,4)+"-"+all_request[index]['created_date'].slice(4,6)+"-"+all_request[index]['created_date'].slice(6,8)) : '' ,
                            created_by : all_request[index]['created_by'] ?  entities.encode(all_request[index]['created_by']) : '',
                            last_chg : all_request[index]['last_chg'] ? entities.encode(all_request[index]['last_chg'].slice(0,4)+"-"+all_request[index]['last_chg'].slice(4,6)+"-"+all_request[index]['last_chg'].slice(6,8)) : '',
                            changed_by : all_request[index]['changed_by'] ? entities.encode(all_request[index]['changed_by']) : '',
                            mtyp : all_request[index]['mtyp']? entities.encode(all_request[index]['mtyp']) : '', 
                            bun : all_request[index]['bun'] ? entities.encode(all_request[index]['bun']) : '',
                            plant : all_request[index]['plant'] ? entities.encode(all_request[index]['plant']) : '',
                            date_added : common.currentDateTime(),
                            date_updated : common.currentDateTime()
                           }
                        const get_relation = await sapModel.get_old_product(scarchObj);
                        if(get_relation.success == false){
                            dataObj.status = 1;
                            const add = await sapModel.add_product(dataObj);
                            if(add.id > 0){
                            //do nothing
                            success_message = 'data chenged successfully'
                            }else{
                            error++;
                            err_msg = 'error occers';
                            reason = '{"reason" : "error occers"}'
                            }             
                        }else{
                            const update_product = await sapModel.update_product(dataObj);
                          
                            if(update_product.id > 0){
                                  const get_data = await sapModel.get_compare_data(update_product.id);
                              
                                   if(get_data.length > 0){
                                    if(get_data[0].x_plntmat_status == 'x' || get_data[0].x_distchain_status == 'x' || get_data[0].distchainspecstatus == 'x'){
                                        material_obj.push(get_data[0].material_code);
                                         await sapModel.update_product_status(material_obj,common.currentDateTime())
                                        
                                     }
                                   }
                                  
                                success_message = 'data chenged successfully'
                            }else{
                               error ++;
                               err_msg = 'error occers';
                               reason = '{"reason" : "error occers"}'
                            }
    
                         }
                        }else{
                        error ++;
                        err_msg = 'plese enter proper data in div or mtyp';
                        reason = '{"reason" : "plese enter proper data in div or mtyp"}'
                         }
                        }

                        }
                
                
                     }else{
                        error ++;
                        err_msg = 'can not accept null value.';
                        reason = '{"reason" : "can not accept null value."}'

                    }
                if(error > 0){
                    if(logResponse){
                        sapModel.updateRequestLog(logResponse.id,'{"status" : false}',reason);
                    }
                    common.logError(new Error(err_msg))
                    return {
                        status : false,
                        message :  err_msg
                    };
              }else{
                 if(logResponse){
                        sapModel.updateRequestLog(logResponse.id,'{"status" : true}',reason);
                    }
                    console.log("TRUE");
                    return {
                        status : true,
                        message :  success_message
                };
            }
             
               
            }
           
        }
    }
}