const request = require("request");
const Config = require('../configuration/config');
const crmModel = require('../models/crm');
const common = require('../controllers/common');
var trim = require("trim");

module.exports = {

	token_url: (Config.sampark.use == true) ? Config.sampark.token_url : Config.sales_force.token_url,
	customer_url: (Config.sampark.use == true) ? Config.sampark.customer : Config.sales_force.customer,
	external_discussions_url: (Config.sampark.use == true) ? Config.sampark.external_discussions : Config.sales_force.external_discussions,
	internal_discussions_url: (Config.sampark.use == true) ? Config.sampark.internal_discussions : Config.sales_force.internal_discussions,
	upload_file_url: (Config.sampark.use == true) ? Config.sampark.upload_file : Config.sales_force.upload_file,

	/**
	 * Generates a Get Token
	 * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
	 * @return {Json Object} - Get token used for Token selection
	 */
	get_token: async () => {
		return new Promise(async (resolve, reject) => {

			let token = await crmModel.get_token();

			if (token != '') {
				resolve({
					status: 1,
					token: {
						access_token: token
					}
				});
			} else {
				var options = {
					method: 'POST',
					url: module.exports.token_url,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				};
				request(options, async (error, response, body) => {
					if (error) {
						// crmModel.log_req_resp(
						// 	['',
						// 	error,
						// 	options.method,
						// 	options.url,
						// 	common.currentDateTime(),
						// 	'OUT']
						// );
						resolve({
							status: 2,
							error: error
						});
					} else {
						let resp_body = JSON.parse(response.body);
						// crmModel.log_req_resp(
						// 	['',
						// 	resp_body,
						// 	options.method,
						// 	options.url,
						// 	common.currentDateTime(),
						// 	'OUT']
						// );
						await crmModel.insert_token(resp_body.access_token);
						resolve({
							status: 1,
							token: resp_body
						});
					}
				});
			}
		});
	},
	/**
	 * Generates a Add Task
	 * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @param {Object} url - HTTP response argument to the middleware function
	 * @return {Json Object} - Add Task used for task selection
	 */
	add_task: async (sfBody, url) => {
		console.log('sfBody', sfBody);
		return new Promise(async (resolve, reject) => {

			let validate = await module.exports.validate(sfBody);

			if (validate.proceed == true) {
				let salesForceToken = await module.exports.get_token();
				if (salesForceToken.status == 1) {
					let token = salesForceToken.token.access_token;
					let options = {
						method: 'POST',
						url: url,
						headers: {
							'Content-type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(sfBody)
					};
					request(options, async function (error, response, body) {
						console.log('response body', body);
						if (error) {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									error,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);
							resolve({
								status: 2,
								error: error
							});
						} else {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									response.body,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);

							if (response.body == '' || trim(response.body) == '') {
								resolve({
									status: 2,
									error: 'Response Body Is Blank'
								});
							} else {

								let resp_body = JSON.parse(JSON.parse(response.body));
								if (resp_body.status == 'Error') {
									resolve({
										status: 2,
										error: resp_body.message
									});
								} else {
									resolve({
										status: 1,
										data: resp_body
									});
								}
							}
						}
					});
				} else {
					resolve({
						status: 2,
						error: salesForceToken.error
					});
				}
			} else {
				resolve({
					status: 2,
					error: validate.error
				});
			}
		});
	},
	/**
	 * Generates a External Discussions
	 * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @return {Json Object} - External Discussions used for Discussions selection
	 */
	external_discussions: async (sfBody) => {
		console.log('sfBody', sfBody);
		return new Promise(async (resolve, reject) => {

			let validate = await module.exports.validate(sfBody);
			if (validate.proceed == true) {

				let salesForceToken = await module.exports.get_token();
				if (salesForceToken.status == 1) {
					let token = salesForceToken.token.access_token;
					let options = {
						method: 'POST',
						url: module.exports.external_discussions_url,
						headers: {
							'Content-type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(sfBody)
					};
					request(options, async function (error, response, body) {
						console.log('response body', body);
						if (error) {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									error,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);
							resolve({
								status: 2,
								error: error
							});
						} else {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									response.body,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);

							if (response.body == '' || trim(response.body) == '') {
								resolve({
									status: 2,
									error: 'Response Body Is Blank'
								});
							} else {

								let resp_body = JSON.parse(JSON.parse(response.body));
								if (resp_body.status == 'Error') {
									resolve({
										status: 2,
										error: resp_body.message
									});
								} else {
									resolve({
										status: 1,
										data: resp_body
									});
								}
							}
						}
					});
				} else {
					resolve({
						status: 2,
						error: salesForceToken.error
					});
				}
			} else {
				resolve({
					status: 2,
					error: validate.error
				});
			}
		});
	},
	/**
	 * Generates a Internal Discussions
	 * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @return {Json Object} - Internal Discussions used for Discussions selection
	 */
	internal_discussions: async (sfBody) => {
		console.log('sfBody', sfBody);
		return new Promise(async (resolve, reject) => {
			let validate = await module.exports.validate(sfBody);
			if (validate.proceed == true) {
				let salesForceToken = await module.exports.get_token();
				if (salesForceToken.status == 1) {
					let token = salesForceToken.token.access_token;
					let options = {
						method: 'POST',
						url: module.exports.internal_discussions_url,
						headers: {
							'Content-type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(sfBody)
					};
					request(options, async function (error, response, body) {
						console.log('response body', body);
						if (error) {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									error,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);
							resolve({
								status: 2,
								error: error
							});
						} else {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									response.body,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);

							if (response.body == '' || trim(response.body) == '') {
								resolve({
									status: 2,
									error: 'Response Body Is Blank'
								});
							} else {

								let resp_body = JSON.parse(JSON.parse(response.body));
								if (resp_body.status == 'Error') {
									resolve({
										status: 2,
										error: resp_body.message
									});
								} else {
									resolve({
										status: 1,
										data: resp_body
									});
								}
							}
						}
					});
				} else {
					resolve({
						status: 2,
						error: salesForceToken.error
					});
				}
			} else {
				resolve({
					status: 2,
					error: validate.error
				});
			}
		});
	},
	/**
	 * Generates a Update Customer
	 * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @return {Json Object} - Update Customer used for Customer selection
	 */
	update_customer: async (sfBody) => {
		return new Promise(async (resolve, reject) => {
			let validate = await module.exports.validate(sfBody);
			if (validate.proceed == true) {
				let salesForceToken = await module.exports.get_token();
				if (salesForceToken.status == 1) {
					let token = salesForceToken.token.access_token;
					let options = {
						method: 'PUT',
						url: module.exports.customer_url,
						headers: {
							'Content-type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(sfBody)
					};
					console.log('options', options);
					request(options, async function (error, response, body) {
						if (error) {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									error,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);
							resolve({
								status: 2,
								error: error
							});
						} else {
							//sfBody.uploaded_files = [];
							crmModel.log_req_resp(
								[sfBody,
									response.body,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);

							if (response.body == '' || trim(response.body) == '') {
								resolve({
									status: 2,
									error: 'Response Body Is Blank'
								});
							} else {
								resolve({
									status: 1,
									data: JSON.parse(JSON.parse(response.body))
								});
							}
						}
					});
				} else {
					resolve({
						status: 2,
						error: salesForceToken.error
					});
				}
			} else {
				resolve({
					status: 2,
					error: validate.error
				});
			}
		});
	},
	/**
	 * Generates a Upload File
	 * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @return {Json Object} - Upload File used for upload selection
	 */
	upload_file: async (sfBody) => {
		return new Promise(async (resolve, reject) => {
			let validate = await module.exports.validate(sfBody);
			if (validate.proceed == true) {
				let salesForceToken = await module.exports.get_token();
				if (salesForceToken.status == 1) {
					let token = salesForceToken.token.access_token;
					let options = {
						method: 'POST',
						url: module.exports.upload_file_url,
						headers: {
							'Content-type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(sfBody)
					};
					console.log('options', options);
					request(options, async function (error, response, body) {
						if (error) {
							for (let index = 0; index < sfBody.records.length; index++) {
								sfBody.records[index].VersionData = '';
							}
							crmModel.log_req_resp(
								[sfBody,
									error,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);
							resolve({
								status: 2,
								error: error
							});
						} else {
							for (let index = 0; index < sfBody.records.length; index++) {
								sfBody.records[index].VersionData = '';
							}
							crmModel.log_req_resp(
								[sfBody,
									response.body,
									options.method,
									options.url,
									common.currentDateTime(),
									'OUT'
								]
							);

							if (response.body == '' || trim(response.body) == '') {
								resolve({
									status: 2,
									error: 'Response Body Is Blank'
								});
							} else {
								let resp_body = JSON.parse(response.body);
								if (resp_body.hasErrors == true) {
									resolve({
										status: 2,
										error: resp_body
									});
								} else {
									resolve({
										status: 1,
										data: resp_body
									});
								}
							}
						}
					});
				} else {
					resolve({
						status: 2,
						error: salesForceToken.error
					});
				}
			} else {
				resolve({
					status: 2,
					error: validate.error
				});
			}
		});
	},
	/**
	 * Generates a Validate
	 * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
	 * @param {Object} sfBody - HTTP request argument to the middleware function
	 * @return {Json Object} - Validate used for validation selection
	 */
	validate: async (sfBody) => {
		if ("sales_task_id" in sfBody) {
			if (sfBody.sales_task_id == null || sfBody.sales_task_id == '' || sfBody.sales_task_id == 'null') {
				console.log('here sales_task_id');
				sfBody.uploaded_files = '';
				return {
					proceed: false,
					error: `Sales task ID missing. Request:${JSON.stringify(sfBody)}`
				};
			}
		}
		if ("sales_customer_id" in sfBody) {
			if (sfBody.sales_customer_id == null || sfBody.sales_customer_id == '' || sfBody.sales_customer_id == 'null') {
				console.log('here sales_customer_id');
				sfBody.uploaded_files = '';
				return {
					proceed: false,
					error: `Sales customer ID missing Request:${JSON.stringify(sfBody)}`
				};
			}
		}

		// if("sales_company_id" in sfBody){
		// 	if(sfBody.sales_company_id == null || sfBody.sales_company_id == '' || sfBody.sales_company_id == 'null'){
		// 		console.log('here sales_company_id');
		// 		sfBody.uploaded_files = '';
		// 		return {proceed:false,error:`Sales company ID missing Request:${JSON.stringify(sfBody)}`};
		// 	}
		// }

		return {
			proceed: true
		};
	}

}