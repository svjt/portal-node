const JWT = require("jsonwebtoken");
require("dotenv").config();
const jwtSimple = require("jwt-simple");
const Entities = require("html-entities").AllHtmlEntities;
const entities = new Entities();
const agentModel = require("../models/agents");
const taskModel = require("../models/task");
const customerModel = require("../models/user");
const Config = require("../configuration/config");
const Cryptr = require("cryptr");
const common = require("./common");
const globalLimit = 10;

module.exports = {
  /**
   * Generates a Company List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Company list used for company selection
   */
  list_company: async (req, res, next) => {
    try {
      var agent_id = req.user.customer_id;
      await agentModel
        .list_company(agent_id)
        .then(function (data) {
          res.status(200).json({
            status: 1,
            data: data,
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a Customers List with respect to a Company
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Customers list used for customers selection
   */
  list_company_customers: async (req, res, next) => {
    try {
      var agent_id = req.user.customer_id;
      var company_id = entities.encode(req.query.cid);

      await agentModel
        .list_company_customers(company_id)
        .then(function (data) {
          res.status(200).json({
            status: 1,
            data: data,
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a CC Customer List
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - CC Customer list used for customer selection
   */
  list_customer_cc: async (req, res, next) => {
    try {
      var customer_id = entities.encode(req.query.custid);
      let customer_details = await customerModel.get_user(customer_id);

      if (customer_details.length > 0) {
        let company_arr = await module.exports.__get_company_list(
          customer_details[0].company_id,
          req.user.role,
          req.user.customer_id
        );
        await agentModel
          .get_cc_cust_company(company_arr, customer_id)
          .then(function (data) {
            res.status(200).json({
              status: 1,
              data: data,
              pre_selected: [],
            });
          })
          .catch((err) => {
            common.logError(err);
            res
              .status(400)
              .json({
                status: 3,
                message: Config.errorText.value,
              })
              .end();
          });
      } else {
        res
          .status(400)
          .json({
            status: 2,
            message: "Invalid details",
          })
          .end();
      }
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 2,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a Notification Count
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Notification count used for notification selection
   */
  count_notification: async (req, res, next) => {
    try {
      const agent_id = req.user.customer_id;
      const company_id = entities.encode(req.params.company_id);
      const role = "A";

      await agentModel
        .countAgentNotification(agent_id, role, company_id)
        .then(async function (data) {
          res.status(200).json({
            status: "1",
            counter: parseInt(data),
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a Notification List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Notification list used for notification selection
   */
  list_notification: async (req, res, next) => {
    try {
      const agent_id = req.user.customer_id;
      const company_id = entities.encode(req.params.company_id);
      const role = "A";

      await agentModel
        .fetchAgentNotification(agent_id, role, company_id)
        .then(async function (data) {
          res.status(200).json({
            status: "1",
            data: data,
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a Notification Update
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Notification update used for notification selection
   */
  update_notification: async (req, res, next) => {
    try {
      const customer_id = req.user.customer_id;
      const role = "A";
      const { notification_id, task_id } = req.body;

      await agentModel
        .updateAgentNotification(customer_id, notification_id, task_id, role) //mark as read by updating read_status =1
        .then(async function (data) {
          //let ncounter = await userModel.countCustomerNotification( customer_id, role )
          //let notifications = await userModel.fetchCustomerNotification( customer_id, role )

          res.status(200).json({
            status: "1",
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  /**
   * Generates a Selected CC Customer List
   * @author Soumyadeep Adhikary < soumyadeep @indusnet.co.in >
   * @param  customer_id - HTTP request argument
   * @return {Json Object} - Selected CC customers used for CC customers selection
   */
  get_pre_selected_cc_list: async (customer_id) => {
    let ret_arr = [];
    let customer_details = await customerModel.get_user(customer_id);
    //ROLE 0 => Is Admin YES
    if (customer_details[0].role_type == 0) {
      ret_arr = await customerModel.get_super_users(
        customer_id,
        customer_details[0].company_id
      );
      //ROLE TYPE 1 => Is Admin NO, Role Type ADMIN
    } else if (customer_details[0].role_type == 1) {
      let super_admins = await customerModel.get_super_users(
        customer_id,
        customer_details[0].company_id
      );
      let not_in = [customer_id];
      ret_arr = super_admins;

      for (let index = 0; index < super_admins.length; index++) {
        const element = super_admins[index];
        not_in.push(element.customer_id);
      }

      let cust_role_arr = await customerModel.get_user_role(customer_id);
      let role_arr = [];
      for (let index = 0; index < cust_role_arr.length; index++) {
        const element = cust_role_arr[index];
        role_arr.push(element.role_id);
      }

      let sel_cust_arr = await customerModel.get_department_admins(
        not_in,
        role_arr,
        customer_details[0].company_id
      );
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element);
      }

      //ROLE TYPE 2 => Is Admin NO, Role Type REGULAR
    } else if (customer_details[0].role_type == 2) {
      let super_admins = await customerModel.get_super_users(
        customer_id,
        customer_details[0].company_id
      );
      let not_in = [];
      ret_arr = super_admins;

      for (let index = 0; index < super_admins.length; index++) {
        const element = super_admins[index];
        not_in.push(element.customer_id);
      }

      let cust_role_arr = await customerModel.get_user_role(customer_id);
      let role_arr = [];
      for (let index = 0; index < cust_role_arr.length; index++) {
        const element = cust_role_arr[index];
        role_arr.push(element.role_id);
      }

      let sel_cust_arr = await customerModel.get_department_admins(
        not_in,
        role_arr,
        customer_details[0].company_id
      );
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element);
      }
    }

    return ret_arr;
  },
  /**
   * Generates a Company list
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param  customer_id - HTTP request argument
   * @param role - HTTP request argument
   * @param agent_id - HTTP request argument
   * @return {Json Object} - Selected company list used for company selection
   */
  __get_company_list: async (company_id, role, agent_id) => {
    let parent_company_id = await customerModel.get_parent_company(company_id);

    let company_arr = [];
    if (parent_company_id > 0) {
      let associate_company = await customerModel.get_child_company(parent_company_id);
      let in_agent_arr = [];
      if (role == 2) {
        let agent_company_list = await agentModel.list_company(agent_id);
        for (let index = 0; index < agent_company_list.length; index++) {
          const element = agent_company_list[index];
          in_agent_arr.push(element.company_id);
        }
      }
      for (let index = 0; index < associate_company.length; index++) {
        const element = associate_company[index];
        if (role == 2) {
          if (in_agent_arr.length > 0 && common.inArray(element.company_id, in_agent_arr)) {
            company_arr.push(element.company_id);
          }
        } else {
          company_arr.push(element.company_id);
        }
      }
    } else {
      company_arr.push(company_id);
    }
    return company_arr;
  }
  


};
