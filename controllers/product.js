const JWT = require('jsonwebtoken');
//require('dotenv').config();
const jwtSimple = require('jwt-simple');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const bcrypt = require('bcryptjs');
const axios = require("axios");
const AWS = require('aws-sdk');
const fs = require("fs");
var path = require("path");
const userModel = require('../models/user');
const customerModel = require('../models/customer');
const productModel = require('../models/product');
const Config = require('../configuration/config');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(Config.cryptR.secret);
const common = require('./common');
var trim = require('trim');
const globalLimit = 10;
const dateFormat = require('dateformat');


module.exports = {
  /**
   * Generates a Drupal Product Details
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product Details used for Product selection
   */
  product_details: async (req, res, next) => {
    try {
      //console.log('controller');
      //drupal call for fetching the array of products(3 data required)
      const config = {
        headers: {
          "Authorization": Config.drupal_setting.Authorization,
          "Content-Type": Config.drupal_setting.GenContentType,
          "X-CSRF-Token": Config.drupal_setting.XCSRFToken
        }
      };

      var request_json = {
        id: req.value.body.product_id,
        lang: req.user.lang
      };
      //console.log('request_json==========', request_json)
      /* await axios
        .get(
          Config.drupal_setting.url + "/reddy_api/get-product-details/" + req.value.params.product_id + "?_format=json",

        ) */
      await axios
        .post(
          Config.drupal_setting.url + "/reddy-api/get_product_details.json?_format=json",
          request_json,
          config
        )
        .then(async function (response) {
          //console.log('rspnse here')
          console.log("response Details", response)
          res.status(200)
            .json({
              status: 1,
              data: response.data
            });

        }).catch(err => {
          console.log("ERR", err);
          //common.logError(err);
          res.status(400).json({
            status: 3,
            message: "No Products found"
          }).end();
        })



    } catch (err) {
      console.log("ERR", err);
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product Category
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product category used for Product Category selection
   */
  list_category: async (req, res, next) => {
    try {
      await productModel.product_category(req.user.lang)
        .then(function (data) {

          if (data.length > 0) {
            data.map(async item => {
              if (item.id) {
                item.value = item.drupal_id;
                item.label = item.name;
              }

            })
          }

          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product Stage
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product Stage used for Product Stage selection
   */
  list_stage: async (req, res, next) => {
    try {
      await productModel.product_stage(req.user.lang)
        .then(function (data) {

          if (data.length > 0) {
            data.map(async item => {

              if (item.id) {
                item.value = item.drupal_id;
                item.label = item.name;
              }

            })
          }

          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product Technology
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product technology used for Product Technology selection
   */
  list_technology: async (req, res, next) => {
    try {
      await productModel.product_technology(req.user.lang)
        .then(function (data) {

          if (data.length > 0) {
            data.map(async item => {
              if (item.id) {
                item.value = item.drupal_id;
                item.label = item.name;
              }

            })
          }

          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product Dosage
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product dosage used for Product Dosage selection
   */
  list_dosage: async (req, res, next) => {
    try {
      await productModel.product_dosage(req.user.lang)
        .then(function (data) {
          if (data.length > 0) {
            data.map(async item => {
              if (item.id) {
                item.value = item.drupal_id;
                item.label = item.name;
              }

            })
          }
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product List
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product List used for Product selection
   */
  all_products: async (req, res, next) => {
    try {
      await productModel.get_products(req.user.lang)
        .then(function (data) {
          if (data.length > 0) {
            data.map(async item => {
              if (item.product_id) {
                item.value = item.product_id;
                item.label = item.product_name;
              }

            })
          }
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Customer Product List
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Customer Product List used for Customer Product selection
   */
  my_products: async (req, res, next) => {
    try {
      await productModel.my_products(req.user.customer_id,req.user.lang)
        .then(function (data) {
          if (data.length > 0) {
            data.map(async item => {
              if (item.product_id) {
                item.value = item.product_id;
                item.label = item.product_name;
              }

            })
          }
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Drupal Customer Product List
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Customer Product List used for Customer Product selection
   */
  list_my_products: async (req, res, next) => {
    try {

      await productModel.my_products(req.user.customer_id)
        .then(async function (data) {
          if (data.length > 0) {
            var request_json = {

            };
            request_json.lang = req.user.lang;
            if (req.body.title !== undefined) {
              request_json.title = req.body.title;
            }

            if (req.body.development_status !== undefined) {
              request_json.development_status = req.body.development_status;
            }

            if (req.body.api_technology !== undefined) {
              request_json.api_technology = req.body.api_technology;
            }

            if (req.body.dose_form !== undefined) {
              request_json.dose_form = req.body.dose_form;
            }

            if (req.body.therapy_category !== undefined) {
              request_json.therepy_category = req.body.therapy_category;
            }


            let my_products = []
            data.map(async item => {
              if (item.product_id) {
                my_products.push({
                  id: item.product_id
                });
              }

            })

            request_json.nid = my_products;

            const config = {
              headers: {
                "Authorization": Config.drupal_setting.Authorization,
                "Content-Type": Config.drupal_setting.GenContentType,
                "X-CSRF-Token": Config.drupal_setting.XCSRFToken
              }
            };

            await axios
              .post(
                Config.drupal_setting.url + "/reddy-api/get_my_products.json?_format=json",
                request_json,
                config
              )
              .then(async function (response) {
                //console.log("My Product RESPONSE", response.data)
                res.status(200)
                  .json({
                    status: 1,
                    data: response.data.data,
                    api_product_file: response.data.api_product_file,
                    generic_product_file: response.data.generic_product_file
                  });

              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: "No Products found",
                  api_product_file: (err.api_product_file !== '') ? err.api_product_file : '',
                  generic_product_file: (err.generic_product_file !== '') ? err.generic_product_file : ''
                }).end();
              })
          } else {

            var request_json = {};
            let my_products = [];
            let generic_product_file = '';
            let api_product_file = '';
            request_json.nid = my_products;
            request_json.lang = req.user.lang;
            const config = {
              headers: {
                "Authorization": Config.drupal_setting.Authorization,
                "Content-Type": Config.drupal_setting.GenContentType,
                "X-CSRF-Token": Config.drupal_setting.XCSRFToken
              }
            };

            await axios
              .post(
                Config.drupal_setting.url + "/reddy-api/get_my_products.json?_format=json",
                request_json,
                config
              )
              .then(async function (response) {
                api_product_file = response.data.api_product_file,
                  generic_product_file = response.data.generic_product_file
              }).catch(err => {
                api_product_file = (err.api_product_file !== '') ? err.api_product_file : '',
                  generic_product_file = (err.generic_product_file !== '') ? err.generic_product_file : ''
              });

            res.status(400).json({
              status: 3,
              message: "No Products found",
              api_product_file: api_product_file,
              generic_product_file: generic_product_file
            }).end();
          }

        }).catch(err => {
          common.logError(err);

          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Drupal Products from product catalouge
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Drupal Products from product catalouge used for Product selection
   */
  list_products: async (req, res, next) => {
    try {
      var request_json = {

      };
      // console.log("ENTRY",req.value.body);

      request_json.lang = req.user.lang;

      if (req.value.body.title !== undefined) {
        request_json.title = req.value.body.title;
      }

      if (req.value.body.development_status !== undefined) {
        request_json.development_status = req.value.body.development_status;
      }

      if (req.value.body.api_technology !== undefined) {
        request_json.api_technology = req.value.body.api_technology;
      }

      if (req.value.body.dose_form !== undefined) {
        request_json.dose_form = req.value.body.dose_form;
      }

      if (req.value.body.therapy_category !== undefined) {
        request_json.therepy_category = req.value.body.therapy_category;
      }



      const config = {
        headers: {
          "Authorization": Config.drupal_setting.Authorization,
          "Content-Type": Config.drupal_setting.GenContentType,
          "X-CSRF-Token": Config.drupal_setting.XCSRFToken
        }
      };
      //console.log('request_json',request_json)

      await axios
        .post(
          Config.drupal_setting.url + "/reddy-api/get_products.json?_format=json",
          request_json,
          config
        )
        .then(async function (response) {
          //console.log("RESPONSE",response.data);
          res.status(200)
            .json({
              status: 1,
              data: response.data.data,
              api_product_file: response.data.api_product_file,
              generic_product_file: response.data.generic_product_file
            });

        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: "No Products found",
            api_product_file: res.api_product_file,
            generic_product_file: res.generic_product_file
          }).end();
        })

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  }
}