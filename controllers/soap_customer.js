const Config = require('../configuration/config');
const sapModel = require("../models/soap");

const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp')

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();



module.exports = {




    MyService: {
        MyPort: {
            AddUpdateCustomer: async function(args, cb, headers, req) {
                // console.log("hello i am in products Add !!",args);


                //   var credentials = auth(req);
                //   var valid = true;

                //   if (credentials) {

                //     valid = compare(credentials.name, Config.sapAuth.username) && valid
                //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
                //     if(!valid){
                //         throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //     }                
                //   }else{
                //     throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //   } 


                let insLogObj = {
                    request: JSON.stringify(args),
                    response: '',
                    method: 'POST',
                    url: '',
                    datetime: common.currentDateTime(),
                    request_type: 'CUSTOMER_ADD_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);
                let all_request = args['Customer_Details'];
                let error = 0;
                let err_msg = '';
                let reason = '';
                let success_message = '';
                let dampped_index = [];
                if (all_request != null) {

                    for (let index = 0; index < all_request.length; index++) {

                        if (all_request[index]["customer_no_1"] == '' ||
                            all_request[index]["sorg"] == '' ||
                            all_request[index]["dchl"] == '' ||
                            all_request[index]["funct"] == '' ||
                            all_request[index]["customer_no_2"] == '' ||
                            all_request[index]["customer_no_1"] == '?' ||
                            all_request[index]["sorg"] == '?' ||
                            all_request[index]["dchl"] == '?' ||
                            all_request[index]["funct"] == '?' ||
                            all_request[index]["customer_no_2"] == '?' ||
                            all_request[index]["customer_no_1"] == null ||
                            all_request[index]["sorg"] == null ||
                            all_request[index]["dchl"] == null ||
                            all_request[index]["funct"] == null ||
                            all_request[index]["customer_no_2"] == null ||
                            all_request[index]["blocked"] == '?') {

                            error++;
                            err_msg = 'Something went wrong ,please try again with proper data';
                            dampped_index.push(`All required fields are absent or null for index ${index + 1}`);
                            reason = `{"reason" : '${dampped_index}'}`;

                        } else {
                            if (isNaN(all_request[index]["customer_no_1"]) || isNaN(all_request[index]["customer_no_2"]) || isNaN(all_request[index]["sorg"]) || isNaN(all_request[index]["dchl"])) {
                                error++;
                                err_msg = 'Non numeric values are not accepted';
                                dampped_index.push(`Non numeric values are not accepted for index ${index + 1}`);
                                reason = `{"reason" : '${dampped_index}'}`;
                            } else {
                                if (all_request[index]['date'] != undefined && (all_request[index]['date'].length != 8 || isNaN(all_request[index]["date"]) )) {
                                    error++;
                                    err_msg = 'Please enter proper date';
                                    dampped_index.push(`Please enter proper date for index ${index + 1}`);
                                    reason = `{"reason" : '${dampped_index}'}`;
                                } else {
                                    const dataObj = {
                                        customer: all_request[index]["customer_no_1"] ? entities.encode(all_request[index]["customer_no_1"].replace(/^0+/, '')) : '',
                                        name1: all_request[index]["name1"] ? entities.encode(all_request[index]["name1"]) : '',
                                        sorg: all_request[index]["sorg"] ? entities.encode(all_request[index]["sorg"].replace(/^0+/, '')) : '',
                                        sorg_desc: all_request[index]["sorg_desc"] ? entities.encode(all_request[index]["sorg_desc"]) : '',
                                        dchl: all_request[index]["dchl"] ? entities.encode(all_request[index]["dchl"].replace(/^0+/, '')) : '',
                                        dchl_desc: all_request[index]["dchl_desc"] ? entities.encode(all_request[index]["dchl_desc"]) : '',
                                        dv: all_request[index]["dv"] ? entities.encode(all_request[index]["dv"]) : '',
                                        dv_desc: all_request[index]["dv_desc"] ? entities.encode(all_request[index]["dv_desc"]) : '',
                                        funct: all_request[index]["funct"] ? entities.encode(all_request[index]["funct"].toUpperCase()) : '',
                                        funct_desc: all_request[index]["funct_desc"] ? entities.encode(all_request[index]["funct_desc"]) : '',
                                        parc: all_request[index]["parc"] ? entities.encode(all_request[index]["parc"].replace(/^0+/, '')) : '',
                                        customer_ship_to: all_request[index]["customer_no_2"] ? entities.encode(all_request[index]["customer_no_2"].replace(/^0+/, '')) : '',
                                        incot: all_request[index]["incot"] ? entities.encode(all_request[index]["incot"]) : '',
                                        inco2: all_request[index]["inco2"] ? entities.encode(all_request[index]["inco2"]) : '',
                                        curr: all_request[index]["curr"] ? entities.encode(all_request[index]["curr"]) : '',
                                        payt: all_request[index]["payt"] ? entities.encode(all_request[index]["payt"]) : '',
                                        cty: all_request[index]["cty"] ? entities.encode(all_request[index]["cty"]) : '',
                                        cty_name: all_request[index]["cty_name"] ? entities.encode(all_request[index]["cty_name"]) : '',
                                        city: all_request[index]["city"] ? entities.encode(all_request[index]["city"]) : '',
                                        postalcode: all_request[index]["postalcode"] ? entities.encode(all_request[index]["postalcode"]) : '',
                                        street: all_request[index]["street"] ? entities.encode(all_request[index]["street"]) : '',
                                        date: all_request[index]['date'] ? entities.encode(all_request[index]['date'].slice(0, 4) + "-" + all_request[index]['date'].slice(4, 6) + "-" + all_request[index]['date'].slice(6, 8)) : '',
                                        created_by: all_request[index]["created_by"] ? entities.encode(all_request[index]["created_by"]) : '',
                                        customer_group: all_request[index]["customer_group"] ? entities.encode(all_request[index]["customer_group"]) : '',
                                        date_added: common.currentDateTime(),
                                        date_updated: common.currentDateTime()
                                    }


                                    if (all_request[index]["blocked"] != "") {
                                        dataObj.status = 0;
                                    } else {
                                        dataObj.status = 1;
                                    }
                                    const relation_obj = {
                                        customer: dataObj.customer,
                                        sorg: dataObj.sorg,
                                        dchl: dataObj.dchl,
                                        funct: dataObj.funct,
                                        parc: dataObj.parc
                                    };

                                    const get_relation = await sapModel.get_customer_relation(relation_obj);
                                    /// check status

                                    if (get_relation.length == 0) {

                                        const add_customer = await sapModel.add_customer(dataObj);
                                        if (add_customer.customer_id > 0) {

                                            if (relation_obj.funct.toUpperCase() == 'WE') {
                                             await module.exports.__update_shipto_soldto(dataObj);

                                            }
                                            success_message = "Data added successfully."
                                        } else {
                                            error++;
                                            err_msg = 'Data can not be added';
                                            dampped_index.push(`Data can not be added for index ${index + 1}`);
                                            reason = `{"reason" : '${dampped_index}'}`;
                                           
                                        }

                                    } else {
                                        const update_customer = await sapModel.update_customer(dataObj, get_relation[0].customer_id);
                                        if (update_customer.customer_id > 0) {


                                            if (relation_obj.funct.toUpperCase() == 'WE') {

                                               await module.exports.__update_shipto_soldto(dataObj)

                                            }
                                            success_message = "Data updated successfully"
                                        } else {
                                            error++;
                                            err_msg = 'Data can not be updated';
                                            dampped_index.push(`Data can not be updated for index ${index + 1}`);
                                            reason = `{"reason" : '${dampped_index}'}`;
                                        }

                                    }

                                }

                            }



                        }
                    }
                } else {
                    error++;
                    err_msg = 'Can not accept null data .'
                    dampped_index.push(`Can not accept null data  for index ${index + 1}`);
                    reason = `{"reason" : '${dampped_index}'}`;
                }
                if (error > 0) {
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : false}', reason);
                    }
                    common.logError(new Error(err_msg))
                    return {
                        status: false,
                        message: err_msg
                    };
                } else {
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : true}', reason);
                    }

                    return {
                        status: true,
                        message: success_message
                    };
                }
            }

        }
    },
    __update_shipto_soldto: async (dataObj) => {
        const data_exists = await sapModel.sold_to_data_exists(dataObj.customer)
        if (data_exists.length > 0) {

            const relationObject = {
                shipto_id: dataObj.customer_ship_to,
                country: dataObj.cty_name,
                city: dataObj.city,
                post_code: dataObj.postalcode,
                street: dataObj.street,
                soldto_id: dataObj.customer,
                shipto_name: dataObj.name1,
            }
            const check_relation = await sapModel.sold_to_ship_to_relation(relationObject.soldto_id); ///check shipto soldto mapping table
            if (check_relation.length > 0) {

                const check = await sapModel.relation(relationObject.soldto_id, relationObject.shipto_id);

                if (check.length > 0) {

                    if (dataObj.status == 1) {

                        const update_rel = await sapModel.update_relation_ship_sold(relationObject); //////Update shipto soldto mapping table
                        if (update_rel.shipto_id > 0) {

                            await module.exports.__check_shipto_product(relationObject.shipto_id);
                            return {
                                status: true,
                                message: 'Data changed successfully'
                            }

                        } else {
                            common.logError(new Error('Error occurs in updating data.'))
                            return {
                                status: false,
                                message: 'Error occurs in updating data.'
                            }

                        }
                    } else {
                        await sapModel.delete_relation_sold_ship(relationObject.soldto_id, relationObject.shipto_id); ///Delete from shipto soldto mapping table
                        await sapModel.delete_shipto_product(relationObject.shipto_id);
                    }
                } else {
                    if (dataObj.status == 1) {
                        const add_rel = await sapModel.add_relation_ship_sold(relationObject); //// Insert shipto soldto mapping table

                        if (add_rel.shipto_id > 0) {
                            await module.exports.__check_shipto_product(relationObject.shipto_id);

                            return {
                                status: true,
                                message: 'Data changed successfully'
                            }


                        } else {
                            common.logError(new Error('Error occurs in relation add .'))
                            return {
                                status: false,
                                message: 'Error occurs in relation add .'
                            }

                        }
                    }

                }

            } else {

                if (dataObj.status == 1) {

                    const add_rel = await sapModel.add_relation_ship_sold(relationObject); // Add in shipto soldto mapping table
                    if (add_rel.length > 0) {
                        await module.exports.__check_shipto_product(relationObject.shipto_id);
                        return {
                            status: true,
                            message: 'Data changed successfully'
                        }

                    } else {
                        common.logError(new Error('Error occurs in relation add .'))
                        return {
                            status: false,
                            message: 'Error occurs in relation add .'
                        }

                    }
                }
            }

        } else {
            return {
                status: true,
                message: 'Data changed successfully'
            }
        }
        // });
    },
    __check_shipto_product: async (dataObj) => {

        const check_shipto_product = await sapModel.check_shipto_product(dataObj); //check products wrt this ship to in master

        if (check_shipto_product.length > 0) {

            await sapModel.delete_shipto_product(dataObj); //
            for (let i = 0; i < check_shipto_product.length; i++) {
                await sapModel.add_shipto_product(dataObj, check_shipto_product[i].material_master);
            }

        } else {
            await sapModel.delete_shipto_product(dataObj); // remove the products wrt this shipto
        }

    }

}