const Config = require('../configuration/config');
const logisticModel = require("../models/logistics");
const common = require('../controllers/common');
var auth = require('basic-auth');
var compare = require('tsscmp');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');
var path = require('path');
const fs = require('fs');

const dateFormat = require('dateformat');
var striptags = require('striptags');

const s3Config = new AWS.S3({
    accessKeyId: Config.aws.accessKeyId,
    secretAccessKey: Config.aws.secretAccessKey
});


module.exports = {

    getBlueDartPushTrackData: async function(req, res, next) {

        // if (Config.proj_env_var === 'PROD' || Config.proj_env_var === 'QUALITY') {
        //     var credentials = auth(req);
        //     var valid = true;

        //     if (credentials) {

        //         valid = compare(credentials.name, Config.sapAuth.username) && valid
        //         valid = compare(credentials.pass, Config.sapAuth.password) && valid
        //         if (!valid) {
        //             throw {
        //                 Fault: {
        //                     Code: {
        //                         Value: '401'
        //                     },
        //                     Reason: { Text: 'Authentication failed' },
        //                     statusCode: 401
        //                 }
        //             };
        //         }
        //     } else {
        //         throw {
        //             Fault: {
        //                 Code: {
        //                     Value: '401'
        //                 },
        //                 Reason: { Text: 'Authentication failed' },
        //                 statusCode: 401
        //             }
        //         };
        //     }
        // }

        let all_request = req.body;
        const inputData = {
            json_data : all_request,
            status : '',
            reason : '',
            date_added : common.currentDateTime()
        }
        const logResponse = await logisticModel.insertPushTrackData(inputData);
        let err_count = 0;
        let message = [];
        let file_name = '';
        if(all_request!= null ){
            if(all_request.statustracking != undefined){
                const shipment_arr = all_request.statustracking
                let  POdImage = '';
                 for (let i = 0; i < shipment_arr.length; i++) {

                    let shipment_data = shipment_arr[i].Shipment
                    let scan_details_arr = shipment_data.Scans.ScanDetail;
                    let podimg = shipment_data.Scans.PODDCImages;
                    if(podimg != undefined){
                         POdImage = podimg.PODImage ;
                    }
                    
                    
                    const dataObj = {
                        sender_id: entities.encode(shipment_data.SenderID),
                        receiver_id: entities.encode(shipment_data.ReceiverID),
                        way_bill_no: entities.encode(shipment_data.WaybillNo),
                        ref_no: entities.encode(shipment_data.RefNo),
                        prodcode: entities.encode(shipment_data.Prodcode),
                        sub_product_code: entities.encode(shipment_data.SubProductCode),
                        feature: entities.encode(shipment_data.Feature),
                        origin: entities.encode(shipment_data.Origin),
                        origin_area_code: entities.encode(shipment_data.OriginAreaCode),
                        destination: entities.encode(shipment_data.Destination),
                        destination_area_code: entities.encode(shipment_data.DestinationAreaCode),
                        pick_up_date: entities.encode(shipment_data.PickUpDate),
                        pick_up_time: entities.encode(shipment_data.PickUpTime),
                        weight: entities.encode(shipment_data.Weight),
                        shipment_mode: entities.encode(shipment_data.ShipmentMode),
                        expected_delivery_date: entities.encode(shipment_data.ExpectedDeliveryDate),
                        dynamic_expected_delivery_date: shipment_data.DynamicExpectedDeliveryDate ? entities.encode(shipment_data.DynamicExpectedDeliveryDate) : '',
                        customer_code: shipment_data.CustomerCode ? entities.encode(shipment_data.CustomerCode) : '',
                    }

                    for (let j in scan_details_arr) {
                        const dataObj_scan = {
                            scan_type: entities.encode(scan_details_arr[j].ScanType),
                            scan_group_type: entities.encode(scan_details_arr[j].ScanGroupType),
                            scan_code: entities.encode(scan_details_arr[j].ScanCode),
                            scan: entities.encode(scan_details_arr[j].Scan),
                            scan_date: entities.encode(scan_details_arr[j].ScanDate),
                            scan_time: entities.encode(scan_details_arr[j].ScanTime),
                            scanned_location_code: entities.encode(scan_details_arr[j].ScannedLocationCode),
                            scanned_location: entities.encode(scan_details_arr[j].ScannedLocation),
                            scanned_location_state_code: entities.encode(scan_details_arr[j].ScannedLocationStateCode),
                            status_timeZone: entities.encode(scan_details_arr[j].StatusTimeZone),
                            comments: entities.encode(scan_details_arr[j].Comments),
                            status_latitude: entities.encode(scan_details_arr[j].StatusLatitude),
                            status_longitude: entities.encode(scan_details_arr[j].StatusLongitude),
                            scanned_location_city: scan_details_arr[j].ScannedLocationCity ? entities.encode(scan_details_arr[j].StatusLongitude) : '',
                            sorry_card_number: scan_details_arr[j].SorryCardNumber ? entities.encode(scan_details_arr[j].SorryCardNumber) : '',
                            reached_destination_location: scan_details_arr[j].ReachedDestinationLocation ? entities.encode(scan_details_arr[j].ReachedDestinationLocation) : '',
                            secure_code: scan_details_arr[j].SecureCode ? entities.encode(scan_details_arr[j].SecureCode) : '',
                        }
                        // console.log('+++++++++++',POdImage);
                        /* Creating File Path Of POD Image Based On Certain Conditions */
                        if(dataObj_scan.scan_group_type == 'T' && dataObj_scan.scan_code == 'POD' && POdImage != ''){
                            //console.log("hello");
                            file_name = 'scan_uploads/' + (Math.floor(Date.now() / 1000)) + '-' + uuidv4() + ".jpeg";
                            await module.exports.__upload_to_s3_bucket(file_name, POdImage);                
                        }

                        /*Checking Same Combination Of Way Bill No And Ref No Present in Main Table*/
                        await logisticModel.check_shipment(dataObj.way_bill_no, dataObj.ref_no).then(async function(data){
                            if (data.length > 0) {
                                if (data[0].id > 0) {
                                    /*Checking Same Combination Of scan_type ,scan_group_type , scan_code ,scanned_location_code And Main table primary key Present in Scan Details Table*/
                                    const update_logistic_details =  await logisticModel.update_logistic_details(dataObj,common.currentDateTime());
                                    if(update_logistic_details.length > 0){
                                        let master_data = await logisticModel.master_data();
                                        let master_arr = [];
                                        for(let i in master_data ){
                                            master_arr.push(master_data[i].master_data);
                                        }
                                        //console.log("master_data",master_arr);
                                        let scan_obj = `${dataObj_scan.scan_group_type + dataObj_scan.scan_code}`
                                       // console.log("scan_obj",scan_obj);
                                        if(master_arr.indexOf(scan_obj.replace(/^0+/, '')) > 0){
                                            const check_scan = await logisticModel.check_scan(dataObj_scan.scan_type, dataObj_scan.scan_group_type, dataObj_scan.scan_code, dataObj_scan.scanned_location_code, data[0].id);
                                            if (check_scan.length > 0) {
                                                if (check_scan[0].id > 0) {
                                                    
                                                    const update_scan_details = await logisticModel.update_scan_details(dataObj_scan, data[0].id, common.currentDateTime(),file_name);
                                                    //console.log("+++++++++++",update_scan_details);
                                                }
                                            } else {
                                                await logisticModel.insertScanData(dataObj_scan, data[0].id, common.currentDateTime(),file_name);
                                            }
                                        }
                                    //     else{
                                    //         err_count ++ ;
                                    //         message.push(`Requied condition does not match for scan code ${dataObj_scan.scan_code} and scan group type ${dataObj_scan.scan_group_type} `)
                                    // }
                                        
    
                                    }else{
                                        err_count ++;
                                        message.push("Failed to update logistic data") 
                                    }
                                    
                                }
                            } else {
                                const inserted_id = await logisticModel.insertData(dataObj, common.currentDateTime());
                                if(inserted_id[0].id > 0){
                                    const check_scan = await logisticModel.check_scan(dataObj_scan.scan_type, dataObj_scan.scan_group_type, dataObj_scan.scan_code, dataObj_scan.scanned_location_code, inserted_id[0].id);
    
                                    if (check_scan.length > 0) {
                                        if (check_scan[0].id > 0) {
                                            const update_scan_details = await logisticModel.update_scan_details(dataObj_scan, inserted_id[0].id, common.currentDateTime(),file_name);
                                            //console.log("+++++++++++",update_scan_details);
                                        }
                                    } else {
                                        await logisticModel.insertScanData(dataObj_scan, inserted_id[0].id, common.currentDateTime(),file_name);
                                    }
                                }else{
                                    err_count ++;
                                     message.push("Data insertion failed") 
                                }
                                
                            }
                        }).catch(err => {
                            common.logError(err);
                            res.status(400)
                                .json({
                                    status: 3,
                                    errors: Config.errorText.value
                                }).end();
                        })    
                        //console.log("+++++shipment++++++",check_shipment[0].id);
                        
                    }



                 }
            }else{
                err_count ++;
                 message.push("Please enter proper data") 
            }
        }else{
            err_count ++;
             message.push("Please enter proper data") 
        }
        
        if (err_count > 0) {
            let data = {
                status : false,
                reason : message.toString(),
                id : logResponse[0].id
            }
            await logisticModel.updatePushTrackData(data)
            res.status(400).json({
                status: false,
                message: message.toString()
            });
        } else {
            let data = {
                status : true,
                reason : 'Data added successfully .',
                id : logResponse[0].id
            }
            await logisticModel.updatePushTrackData(data)
            res.status(200).json({
                status: 'true',
                message: "data added successfully"
            });
        }


    },
    __upload_to_s3_bucket: async (new_file_name, file_content) => {
        //console.log("file name : ",new_file_name);
        return new Promise(function(resolve, reject) {
            const imgBuffer = Buffer.from(file_content, 'base64');
            //console.log("buffer : ",imgBuffer);
            let params = {
                Bucket: Config.aws.bucketName,
                Key: new_file_name,
                Body: imgBuffer
            };
            s3Config.upload(params, function(err, data) {
                if (err) {
                    //console.log(err);
                } else {
                    //console.log(data);
                }
            });
            resolve(true);
        });
    }


}