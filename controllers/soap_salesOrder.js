const Config = require('../configuration/config');
const sapModel = require("../models/soap");

const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp')

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


module.exports = {

    MyService: {
        MyPort: {
            AddUpdateSalesOrder: async function(args, cb, headers, req) {
                //console.log("this is args : " , args.sales_order_details);



                //   var credentials = auth(req);
                //   var valid = true;

                //   if (credentials) {

                //     valid = compare(credentials.name, Config.sapAuth.username) && valid
                //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
                //     if(!valid){
                //         throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //     }                
                //   }else{
                //     throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //   } 

                let insLogObj = {
                    request: JSON.stringify(args),
                    response: '',
                    method: 'POST',
                    url: '',
                    datetime: common.currentDateTime(),
                    request_type: 'SALES_ORDER_ADD'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);
                let error = 0;
                let err_msg = '';
                let reason = '';
                let dumped_index = [];
                let all_data = args['sales_order_details'];
                for (let index_1 = 0; index_1 <= (all_data.length - 1); index_1++) {

                    let all_items = all_data[index_1]['Item_Details'];
                    //console.log("this is all items : ",all_items)

                    if (all_items != null) {



                        for (let index = 0; index <= (all_items.length - 1); index++) {

                            if (all_data[index_1]['sales_order'] == '' ||
                                all_data[index_1]['customer_po_number'] == '' ||
                                all_data[index_1]['creation_date'] == '' ||
                                all_data[index_1]['sales_document_type'] == '' ||
                                all_data[index_1]['customer'] == '' ||
                                all_data[index_1]['sales_order'] == '?' ||
                                all_data[index_1]['customer_po_number'] == '?' ||
                                all_data[index_1]['creation_date'] == '?' ||
                                all_data[index_1]['sales_document_type'] == '?' ||
                                all_data[index_1]['customer'] == '?' ||
                                all_data[index_1]['sales_order'] == null ||
                                all_data[index_1]['customer_po_number'] == null ||
                                all_data[index_1]['creation_date'] == null ||
                                all_data[index_1]['sales_document_type'] == null ||
                                all_data[index_1]['customer'] == null ||
                                all_items[index]['item'] == '' ||
                                all_items[index]['item'] == '?' ||
                                all_items[index]['item'] == null ||
                                all_items[index]['material'] == '' ||
                                all_items[index]['material'] == '?' ||
                                all_items[index]['material'] == null ||
                                all_items[index]['net_value'] == '' ||
                                all_items[index]['net_value'] == '?' ||
                                all_items[index]['net_value'] == null ||
                                all_items[index]['currency'] == '' ||
                                all_items[index]['currency'] == '?' ||
                                all_items[index]['currency'] == null ||
                                all_items[index]['order_quantity'] == '' ||
                                all_items[index]['order_quantity'] == '?' ||
                                all_items[index]['order_quantity'] == null ||
                                all_items[index]['order_unit'] == '' ||
                                all_items[index]['order_unit'] == '?' ||
                                all_items[index]['order_unit'] == null) {


                                error++;
                                err_msg = " Null value is not accepted";
                                dumped_index.push(` Null value is not accepted for index ${index + 1 }`);
                                reason = `{"reason" : '${dumped_index}'}` 



                            } else {

                                if (isNaN(all_data[index_1]['customer']) || isNaN(all_items[index]['material']) || isNaN(all_items[index]['order_quantity']) || isNaN(all_data[index_1]['creation_date']) || isNaN(all_data[index_1]['sales_order']) || isNaN(all_items[index]['item']) || isNaN(all_data[index_1]['customer_po_number'])) { //string to number checking []

                                    error++;
                                    err_msg = "Non numeric value posted.";
                                    dumped_index.push(` Non numeric value posted for index ${index + 1 }`);
                                    reason = `{"reason" : '${dumped_index}'}`;

                                } else {

                                    if (all_data[index_1]['creation_date'].length == 8) {

                                        let dataObj = {
                                            sales_order: all_data[index_1]['sales_order'] ? entities.encode(all_data[index_1]['sales_order'].replace(/^0+/, '')) : '',
                                            customer_po_number: all_data[index_1]['customer_po_number'] ? entities.encode(all_data[index_1]['customer_po_number'].replace(/^0+/, '')) : '', // remove 0
                                            creation_date: all_data[index_1]['creation_date'] ? entities.encode(all_data[index_1]['creation_date'].slice(0, 4) + "-" + all_data[index_1]['creation_date'].slice(4, 6) + "-" + all_data[index_1]['creation_date'].slice(6, 8)) : '',
                                            sales_document_type: all_data[index_1]['sales_document_type'] ? entities.encode(all_data[index_1]['sales_document_type']) : '',
                                            customer: all_data[index_1]['customer'] ? entities.encode(all_data[index_1]['customer'].replace(/^0+/, '')) : '',
                                            item: all_items[index]['item'] ? entities.encode(all_items[index]['item'].replace(/^0+/, '')) : '',
                                            material: all_items[index]['material'] ? entities.encode(all_items[index]['material'].replace(/^0+/, '')) : '',
                                            net_value: all_items[index]['net_value'] ? entities.encode(all_items[index]['net_value']) : '',
                                            currency: all_items[index]['currency'] ? entities.encode(all_items[index]['currency']) : '',
                                            order_quantity: all_items[index]['order_quantity'] ? entities.encode(all_items[index]['order_quantity']) : '',
                                            order_unit: all_items[index]['order_unit'] ? entities.encode(all_items[index]['order_unit']) : '',
                                            date_added : common.currentDateTime()

                                        }
                                        const check_sales_order = await sapModel.check_sales_order(dataObj.sales_order ,dataObj.item)
                                       if(check_sales_order.length > 0){
                                        success_message = 'Data already exists'
                                        dumped_index.push(` Data already exists for index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`;
                                       }else{

                                        const add_sales_order = await sapModel.add_sales_order(dataObj);
                                        if (add_sales_order.id > 0) {
                                            success_message = "Data added successfully"
                                        } else {
                                            error++;
                                            err_msg = " Error occured in Sales Order insertion .";
                                            dumped_index.push(` Error occured in data add for index ${index + 1 }`);
                                            reason = `{"reason" : '${dumped_index}'}`

                                        }

                                       }
                                        

                                    } else {

                                        error++;
                                        err_msg = "Please enter a proper date.";
                                        dumped_index.push(` Please enter a proper date for index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`;

                                    }
                                }

                            }

                        }

                    } else {

                        error++;
                        err_msg = 'Can not accept null value.';
                        dumped_index.push(` Can not accept null value for index ${index + 1 }`);
                        reason = `{"reason" : '${dumped_index}'}`

                    }
                }


                if (error > 0) {
    
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : false}', reason);
                    }
                    common.logError(new Error(err_msg))
                   
                    return {
                        status: false,
                        message: err_msg
                    };
                } else {
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : true}', reason);
                    }
                    console.log("TRUE");
                    return {
                        status: true,
                        message: success_message
                    };
                }

            }
        }
    }
}