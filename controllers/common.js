const nodemailer = require('nodemailer');
const dateFormat = require('dateformat');
const dateMath = require('date-arithmetic');
const fs = require('fs');
const Config = require('../configuration/config');


const errorLogFile = Config.errorFileName;
var transportConfig = Config.transportConfig;
var transportConfigNoReply = Config.transportConfigNoReply;

let mime_obj = {
	'.png': 'image/png',
	'.jpg': 'image/jpeg',
	'.gif': 'image/gif',
	'.jpeg': 'image/jpeg',
	'.doc': 'application/msword',
	'.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	'.xls': 'application/vnd.ms-excel',
	'.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	'.pdf': 'application/pdf',
	'.txt': 'text/plain',
	'.odt': 'application/vnd.oasis.opendocument.text',
	'.rtf': 'application/rtf',
	'.wpd': 'application/vnd.wordperfect',
	'.tex': 'application/x-tex',
	'.wks': 'application/vnd.ms-works',
	'.wps': 'application/vnd.ms-works',
	'.xlr': 'application/vnd.ms-excel',
	'.ods': 'application/vnd.oasis.opendocument.spreadsheet',
	'.csv': 'text/csv',
	'.ppt': 'application/vnd.ms-powerpoint',
	'.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	'.pps': 'application/vnd.ms-powerpoint',
	'.key': 'application/octet-stream',
	'.odp': 'application/vnd.oasis.opendocument.presentation',
	'.ai': 'application/postscript',
	'.bmp': 'image/bmp',
	'.ico': 'image/vnd.microsoft.icon',
	'.svg': 'image/svg+xml',
	'.tif': 'image/tiff',
	'.tiff': 'image/tiff',
	'.eml': 'message/rfc822'
};

module.exports = {
	currentDate: () => {
		var now = module.exports.calcTime();
		return dateFormat(now, "yyyy-mm-dd");
	},
	currentTime: () => {
		var now = module.exports.calcTime();
		return dateFormat(now, "HH:MM:ss");
	},
	currentDateTime: () => {
		var now = module.exports.calcTime();
		return dateFormat(now, "yyyy-mm-dd HH:MM:ss");
	},
	changeDateFormat: (date, separator, format) => {
		// format dd.mm.yyyy || mm.dd.yyyy
		var match = format.split('.');
		return dateFormat(date, `${match[0]}${separator}${match[1]}${separator}${match[2]}`);
	},
	formatDate: (input_date, new_format) => {
		return dateFormat(input_date, new_format);
	},
	dayCountExcludingWeekends: (total_sla) => {
		var now_date = module.exports.calcTime();
		var due_date = module.exports.calcTime();
		var allocated_days = total_sla;
		var days_taken = 0;

		for (var i = 1; i < 160; i++) {
			if (days_taken >= allocated_days) {
				break;
			}
			due_date = dateMath.add(now_date, i, "day");
			if (dateFormat(due_date, 'ddd') == 'Sat' || dateFormat(due_date, 'ddd') == 'Sun') {

			} else {
				days_taken++;
			}
		}

		return (i - 1);

	},
	nextDate: (value, unit) => {

		var unitArr = ["milliseconds", "second", "minutes", "hours", "day", "weekday", "month", "year", "decade", "century"];

		if (unitArr.indexOf(unit) == '-1') {
			var e = new Error('Invalid unit given.');
			return e;
		}

		if (value <= 0) {
			var e = new Error('Invalid days given.');
			return e;
		}

		var date = module.exports.calcTime();

		return dateMath.add(date, value, unit);

	},
	get_mime_type: (extention) => {

		let keys_arr = Object.keys(mime_obj);

		if (module.exports.inArray(extention, keys_arr)) {
			return mime_obj[extention];
		} else {
			return '';
		}
	},
	logError: (err) => {
		if (err) {
			var matches = err.stack.split('\n');
			var regex1 = /\((.*):(\d+):(\d+)\)$/;
			var regex2 = /(.*):(\d+):(\d+)$/;
			var errorArr1 = regex1.exec(matches[1]);
			var errorArr2 = regex2.exec(matches[1]);
			if (errorArr1 !== null || errorArr2 !== null) {
				var errorText = matches[0];
				if (errorArr1 !== null) {
					var errorFile = errorArr1[1];
					var errorLine = errorArr1[2];
				} else if (errorArr2 !== null) {
					var errorFile = errorArr2[1];
					var errorLine = errorArr2[2];
				}


				var now = module.exports.calcTime();
				var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

				var errMsg = `\n DateTime: ${date_format} \n ${errorText} \n Line No : ${errorLine} \n File Path: ${errorFile} \n`;

				var errHtml = `<!DOCTYPE html><html><body><p>${errorText}</p><p>Line No : ${errorLine}</p><p>File Path: ${errorFile}</p></body></html>`;

				console.log(errMsg);

				//LOG ERR
				fs.appendFile(errorLogFile, errMsg, (err) => {
					if (err) throw err;
					console.log('The file has been saved!');
				});
				//LOG ERR

				// //SEND MAIL
				var toArr = [
					'soumyadeep@indusnet.co.in',
					'supratim.jetty@indusnet.co.in'
				];
				var mailOptions = {
					from: Config.contactUsMail,
					to: toArr,
					subject: `Error In ${Config.drupal.url}`,
					html: errHtml
				};
				var transporter = nodemailer.createTransport(transportConfigNoReply);
				transporter.sendMail(mailOptions, function (err, info) {
					if (err) {
						console.log(err)
					} else {
						console.log(info);
					}
				});
				// //SEND MAIL
			}
		}
	},
	outwardCrmError: (error, subject, text) => {
		//SEND MAIL
		var toArr = [
			'soumyadeep@indusnet.co.in',
			'suvojit.biswas@indusnet.co.in'
		];
		var mailOptions = {
			from: Config.webmasterMail,
			to: toArr,
			subject: subject,
			html: `${text} Error:${JSON.stringify(error)}`
		};
		var transporter = nodemailer.createTransport(transportConfig);
		transporter.sendMail(mailOptions, function (err, info) {
			if (err) {
				console.log(err);
			} else {
				console.log(info);
			}
		});
		//SEND MAIL
	},
	getErrorText: (err) => {
		var matches = err.stack.split('\n');
		return matches[0];
	},
	sendMail: (mailOptions) => {
		if (mailOptions.to && mailOptions.to != '') {
			// var mailOptions = {
			// 					from    : Config.webmasterMail,
			// 					to      : toArr,
			// 					subject : `Error In ${Config.drupal.url}`,
			// 					html    : errHtml
			// 			};
			var transporter = nodemailer.createTransport(transportConfig);
			transporter.sendMail(mailOptions, function (err, info) {
				if (err) {
					console.log(err)
				} else {
					console.log(info);
				}

			});
			return true;
		}
	},

	sendMailB2C: (mailOptions) => {
		if (mailOptions.to && mailOptions.to != '') {
			var transporter = nodemailer.createTransport(transportConfigNoReply);
			transporter.sendMail(mailOptions, function (err, info) {
				if (err) {
					console.log(err)
				} else {
					console.log(info);
				}
			});
			return true;
		}
	},
	checkUniqueArr: (a) => {
		var counts = [];
		for (var i = 0; i <= a.length; i++) {
			if (counts[a[i]] === undefined) {
				counts[a[i]] = 1;
			} else {
				return true;
			}
		}
		return false;
	},
	isEmptyObj: (obj) => {
		for (var key in obj) {
			if (obj.hasOwnProperty(key))
				return false;
		}
		return true;
	},
	getDrupalErrors: (drupalErr) => {
		var match = /\r|\n/.exec(drupalErr.message);
		if (match) {
			console.log('split', drupalErr.message.split('\n').join(' '))
			return drupalErr.message.split('\n').join(' ');
		} else {
			return drupalErr;
		}
	},
	getDrupalErrorsNew: (drupalErr) => {
		var custom_err = {};

		if (drupalErr.response) {
			custom_err.url = drupalErr.config.url;
			custom_err.request = drupalErr.config.data;
			custom_err.error = drupalErr.response.data;
		} else {
			custom_err.url = drupalErr.config.url;
			custom_err.request = drupalErr.config.data;
			custom_err.error = `errorNo : ${drupalErr.errno} errorCode : ${drupalErr.code}`;
		}

		return custom_err;
	},
	getDrupalErrorsNewFile: (drupalErr) => {
		var custom_err = {};

		if (drupalErr.response) {
			custom_err.url = drupalErr.config.url;
			//custom_err.request = drupalErr.config.data;
			custom_err.error = drupalErr.response.data;
		} else {
			custom_err.url = drupalErr.config.url;
			//custom_err.request = drupalErr.config.data;
			custom_err.error = `errorNo : ${drupalErr.errno} errorCode : ${drupalErr.code}`;
		}

		return custom_err;
	},
	inArray: (needle, haystack) => {
		var length = haystack.length;
		for (var i = 0; i < length; i++) {
			if (haystack[i] == needle) return true;
		}
		return false;
	},
	logErrorText: (custom_text, error_text) => {

		var now = module.exports.calcTime();
		var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

		var errMsg = `\n DateTime: ${date_format} \n ${error_text} \n ${custom_text} \n`;
		//LOG ERR
		fs.appendFile(errorLogFile, errMsg, (err) => {
			if (err) throw err;
			console.log('Error has been logged!');
		});
		//LOG ERR
	},
	calcTime: () => {
		// create Date object for current location
		var d = new Date();

		// convert to msec
		// subtract local time zone offset
		// get UTC time in msec
		var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

		// create new Date object for different city
		// using supplied offset
		var nd = new Date(utc + (3600000 * 5.5));

		// return time as a string
		return nd;
	},
	calcInputTime: (valu) => {
		// create Date object for current location
		var d = new Date(valu);

		// convert to msec
		// subtract local time zone offset
		// get UTC time in msec
		var utc = d.getTime();

		// create new Date object for different city
		// using supplied offset
		var nd = new Date(utc + (3600000 * 5.5));

		// return time as a string
		return nd;
	},
	nl2br: (str, is_xhtml) => {
		if (typeof str === 'undefined' || str === null) {
			return '';
		}
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	},
	isObjectEquivalent: (a, b) => {
		// Create arrays of property names
		var aProps = Object.getOwnPropertyNames(a);
		var bProps = Object.getOwnPropertyNames(b);

		// If number of properties is different,
		// objects are not equivalent
		if (aProps.length != bProps.length) {
			return false;
		}

		for (var i = 0; i < aProps.length; i++) {
			var propName = aProps[i];

			// If values of same property are not equal,
			// objects are not equivalent
			if (a[propName] !== b[propName]) {
				return false;
			}
		}

		// If we made it this far, objects
		// are considered equivalent
		return true;
	},
	sendMailByCodeB2C: async (mailCode, mailTo, mailFrom, mailCc, mailLang, mailParams, toType) => {
		if (mailTo && mailTo != '') {
			const userModel = require('../models/user');
			const Entities = require('html-entities').AllHtmlEntities;
			const entities = new Entities();
			let mailData = await userModel.emailCodeTemplate(mailCode, mailLang);
			if (mailData.length == 0) {
				mailData = await userModel.emailCodeTemplate(mailCode, "en");
			}

			var mailSubject = entities.decode(mailData[0].subject);
			var mailHtml = entities.decode(mailData[0].content);
			var str_array = mailData[0].params.split(',');

			RegExp.quote = function (str) {
				return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
			};

			for (var i = 0; i < str_array.length; i++) {
				var str_array_str = str_array[i].toLowerCase();
				var param_val = mailParams[str_array_str];
				var replace_text = `[${str_array[i]}]`;

				let reg = new RegExp(RegExp.quote(replace_text), "g");
				if(param_val!='' && param_val!= null){
					mailSubject = mailSubject.replace(reg, entities.decode(param_val.toString()));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val.toString()));
				}else{
					mailSubject = mailSubject.replace(reg, entities.decode(param_val));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val));
				}
			}

			var mailOptions = {
				from: mailFrom, //Config.webmasterMail,
				to: mailTo,
				subject: mailSubject,
				html: mailHtml
			};
			if (mailCc != '' && mailCc.length > 0) {
				mailOptions.cc = mailCc;
			}
			var transporter = nodemailer.createTransport(transportConfigNoReply);
			transporter.sendMail(mailOptions, async (err, info) => {
				if (err) {
					console.log(err)
				} else {
					console.log("Template mail send to: " + mailTo);
					console.log(info);
					var now = module.exports.calcTime();
					var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
					await userModel.emailSendLog(mailData[0].email_id, mailData[0].email_code, mailFrom, mailTo, "s", toType, mailSubject, mailHtml, date_format, mailCc);
				}

			});

			return true;
		}
	},
	sendMailWithAttachByCodeB2C: async (mailCode, mailTo, mailFrom, mailCc, mailLang, mailParams, mailAttach, toType) => {
		if (mailTo && mailTo != '') {
			const userModel = require('../models/user');
			const Entities = require('html-entities').AllHtmlEntities;
			const entities = new Entities();
			let mailData = await userModel.emailCodeTemplate(mailCode, mailLang);
			if (mailData.length == 0) {
				mailData = await userModel.emailCodeTemplate(mailCode, "en");
			}

			var mailSubject = entities.decode(mailData[0].subject);
			var mailHtml = entities.decode(mailData[0].content);
			var str_array = mailData[0].params.split(',');

			RegExp.quote = function (str) {
				return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
			};

			for (var i = 0; i < str_array.length; i++) {
				var str_array_str = str_array[i].toLowerCase();
				var param_val = mailParams[str_array_str];
				var replace_text = `[${str_array[i]}]`;

				let reg = new RegExp(RegExp.quote(replace_text), "g");

				if(param_val!='' && param_val!= null){
					mailSubject = mailSubject.replace(reg, entities.decode(param_val.toString()));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val.toString()));
				}else{
					mailSubject = mailSubject.replace(reg, entities.decode(param_val));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val));
				}
			}

			var mailOptions = {
				from: mailFrom, //Config.webmasterMail,
				to: mailTo,
				subject: mailSubject,
				attachments: mailAttach,
				html: mailHtml
			};
			if (mailCc != '' && mailCc.length > 0) {
				mailOptions.cc = mailCc;
			}
			var transporter = nodemailer.createTransport(transportConfigNoReply);
			transporter.sendMail(mailOptions, async (err, info) => {
				if (err) {
					console.log(err)
				} else {
					console.log("Template mail send to: " + mailTo);
					console.log(info);
					var now = module.exports.calcTime();
					var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
					await userModel.emailSendLog(mailData[0].email_id, mailData[0].email_code, mailFrom, mailTo, "s", toType, mailSubject, mailHtml, date_format, mailCc);
				}

			});

			return true;
		}
	},
	sendMailByCodeB2E: async (mailCode, mailTo, mailFrom, mailCc, mailParams, toType) => {
		if (mailTo && mailTo != '') {
			const userModel = require('../models/user');
			const Entities = require('html-entities').AllHtmlEntities;
			const entities = new Entities();
			let mailData = await userModel.emailCodeTemplate(mailCode, "en");

			var mailSubject = entities.decode(mailData[0].subject);
			var mailHtml = entities.decode(mailData[0].content);
			var str_array = mailData[0].params.split(',');

			RegExp.quote = function (str) {
				return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
			};

			for (var i = 0; i < str_array.length; i++) {
				var str_array_str = str_array[i].toLowerCase();
				var param_val = mailParams[str_array_str];
				var replace_text = `[${str_array[i]}]`;

				let reg = new RegExp(RegExp.quote(replace_text), "g");

				if(param_val!='' && param_val!= null){
					mailSubject = mailSubject.replace(reg, entities.decode(param_val.toString()));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val.toString()));
				}else{
					mailSubject = mailSubject.replace(reg, entities.decode(param_val));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val));
				}
			}

			var mailOptions = {
				from: mailFrom, //Config.webmasterMail,
				to: mailTo,
				subject: mailSubject,
				html: mailHtml
			};
			if (mailCc != '' && mailCc.length > 0) {
				mailOptions.cc = mailCc;
			}
			var transporter = nodemailer.createTransport(transportConfig);
			transporter.sendMail(mailOptions, async (err, info) => {
				if (err) {
					console.log(err)
				} else {
					console.log("Emp Template mail send to: " + mailTo);
					console.log(info);
					var now = module.exports.calcTime();
					var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
					await userModel.emailSendLog(mailData[0].email_id, mailData[0].email_code, mailFrom, mailTo, "s", toType, mailSubject, mailHtml, date_format, mailCc);
				}

			});

			return true;
		}
	},
	sendMailWithAttachByCodeB2E: async (mailCode, mailTo, mailFrom, mailCc, mailParams, mailAttach, toType) => {
		if (mailTo && mailTo != '') {
			const userModel = require('../models/user');
			const Entities = require('html-entities').AllHtmlEntities;
			const entities = new Entities();
			let mailData = await userModel.emailCodeTemplate(mailCode, "en");

			var mailSubject = entities.decode(mailData[0].subject);
			var mailHtml = entities.decode(mailData[0].content);
			var str_array = mailData[0].params.split(',');

			RegExp.quote = function (str) {
				return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
			};

			for (var i = 0; i < str_array.length; i++) {
				var str_array_str = str_array[i].toLowerCase();
				var param_val = mailParams[str_array_str];
				var replace_text = `[${str_array[i]}]`;

				let reg = new RegExp(RegExp.quote(replace_text), "g");

				if(param_val!='' && param_val!= null){
					mailSubject = mailSubject.replace(reg, entities.decode(param_val.toString()));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val.toString()));
				}else{
					mailSubject = mailSubject.replace(reg, entities.decode(param_val));
					mailHtml = mailHtml.replace(reg, entities.decode(param_val));
				}
			}

			var mailOptions = {
				from: mailFrom, //Config.webmasterMail,
				to: mailTo,
				subject: mailSubject,
				attachments: mailAttach,
				html: mailHtml
			};
			if (mailCc != '' && mailCc.length > 0) {
				mailOptions.cc = mailCc;
			}
			var transporter = nodemailer.createTransport(transportConfig);
			transporter.sendMail(mailOptions, async (err, info) => {
				if (err) {
					console.log(err)
				} else {
					console.log("Emp Template mail send to: " + mailTo);
					console.log(info);
					var now = module.exports.calcTime();
					var date_format = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
					await userModel.emailSendLog(mailData[0].email_id, mailData[0].email_code, mailFrom, mailTo, "s", toType, mailSubject, mailHtml, date_format, mailCc);
				}

			});

			return true;
		}
	},
	doAWSTranslate: async (inputText, sourceLanguageCode) => {
		//AWS.config.region = 'global';
		//AWS.config.credentials = new AWS.Credentials("AKIA2DSWCK7M46PPFJIS", "cbck6XkADXZ9vWyYFnF6hDH//10Yco/exBu4Pr+m");
		//var translate = new AWS.Translate({region: 'global'});

		const AWS = require('aws-sdk');
		//AWS.config.update({accessKeyId: "AKIA2DSWCK7M46PPFJIS",secretAccessKey:"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",region:"ap-south-1"});		//sumit
		AWS.config.update({
			accessKeyId: "AKIAVXBJKSI65IZ2XSU6",
			secretAccessKey: "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",
			region: "us-west-2"
		}); //anindya

		return new Promise(async (resolve, reject) => {
			const translate = new AWS.Translate({
				apiVersion: '2017-07-01'
			});

			if (!inputText) {
				console.log("Input text cannot be empty.");
			} else {
				var params = {
					Text: inputText,
					SourceLanguageCode: sourceLanguageCode,
					TargetLanguageCode: Config.default_language
				};
				//console.log(params);

				// translate.translateText(params,(err,data)=>{
				// 	if(err){

				// 	}else{
				// 		resolve(data); 
				// 	}
				// });

				const dataTranslated = await translate.translateText(params).promise();
				console.log(dataTranslated);
				resolve(dataTranslated);
			}
		});
	},
	doGoogleTranslate: async (inputText,sourceLanguageCode) => {
		try{
			const projectId = 'gcp-digital-xceed-bot-prd';

			// Imports the Google Cloud client library
			const { Translate } = require('@google-cloud/translate').v2;		

			// Instantiates a client
			const translate = new Translate({projectId});

			console.log('doGoogleTranslate function')

			return new Promise(async (resolve,reject) => {
				if (!inputText) {
					console.log("Input text cannot be empty.");
				} else {

					// Translates some text into default Language
					const [translation] = await translate.translate(inputText, Config.default_language);
					console.log(`Text: ${inputText}`);
					console.log(`Translation: ${translation}`);
					resolve(translation);
				}
			});
		}catch (err) {
	      common.logError(err);
	      res.status(400).json({
	        status: 2,
	        message: Config.errorText.value
	      }).end();
	    }
	},
	checkContentLanguage: (content) => {
		return 'man';
	},
	getConvertedContent: async (content, language) => {
		console.log('getConvertedContent function')
		//let covetedText = await module.exports.doAWSTranslate(content, language);
		//return covetedText.TranslatedText; //'Translated To Specific Language';

		let covetedText = await module.exports.doGoogleTranslate(content,language);
		return covetedText;
	},
	doGoogleTranslateExplicit: async (inputText,targetLanguageCode,sourceLanguageCode) => {
		try{
			const projectId = 'gcp-digital-xceed-bot-prd';

			// Imports the Google Cloud client library
			const { Translate } = require('@google-cloud/translate').v2;

			// Instantiates a client
			const translate = new Translate({projectId});

			console.log('doGoogleTranslate function')

			return new Promise(async (resolve,reject) => {
				if (!inputText) {
					console.log("Input text cannot be empty.");
				} else {
					// Translates some text into default Language
					const [translation] = await translate.translate(inputText, targetLanguageCode);
					console.log(`Text: ${inputText}`);
					console.log(`Translation (${targetLanguageCode}): ${translation}`);
					resolve(translation);
				}
			});
		}catch (err) {
	      common.logError(err);
	      res.status(400).json({
	        status: 2,
	        message: Config.errorText.value
	      }).end();
	    }
	},
	getConvertedContentExplicit: async (content,posted_language,target_language)=>{
		console.log('getConvertedContent function')
		// let covetedText = await module.exports.doAWSTranslateExplicit(content,target_language,posted_language);
		// return covetedText.TranslatedText;
		
		let covetedText = await module.exports.doGoogleTranslateExplicit(content,target_language,posted_language);
		return covetedText;
	},
	getTranslatedTextMail: async (text, language) => {
		return new Promise(async (resolve, reject) => {
			if (language === Config.default_language) {
				resolve(text);
			} else {
				let ret = '';
				let mail_arr = [{
						'Samples': {
							'zh': '样品',
							'ja': 'サンプル',
							'es': 'Muestras',
							'pt': 'Amostras'
						}
					},
					{
						'Number Of Batches': {
							'zh': '批数',
							'ja': 'バッチ数',
							'es': 'Número de lotes',
							'pt': 'Número de lotes'
						}
					},
					{
						'Quantity': {
							'zh': '数量',
							'ja': '量',
							'es': 'Cantidad',
							'pt': 'Quantidade'
						}
					},
					{
						'Product': {
							'zh': '产品',
							'ja': '製品',
							'es': 'Producto',
							'pt': 'Produtos'
						}
					},
					{
						'Working Standards': {
							'zh': '工作标准',
							'ja': '労働基準',
							'es': 'Estándares de trabajo',
							'pt': 'Normas de trabalho'
						}
					},
					{
						'Impurities': {
							'zh': '杂质',
							'ja': '不純物',
							'es': 'Impurezas',
							'pt': 'Impurezas'
						}
					},
					{
						'Specify Impurity Required': {
							'zh': '指定需要的杂质',
							'ja': '必要な不純物を指定してください',
							'es': 'Especifique la impureza requerida',
							'pt': 'Especifique a impureza necessária'
						}
					},
					{
						'Shipping Address': {
							'zh': '送货地址',
							'ja': 'お届け先の住所',
							'es': 'Dirección de Envío',
							'pt': 'endereço de entrega'
						}
					},
					{
						'Pharmacopoeia': {
							'zh': '药典',
							'ja': '薬局方',
							'es': 'Farmacopea',
							'pt': 'Farmacopéia'
						}
					},
					{
						'Name': {
							'zh': '名称',
							'ja': '名前',
							'es': 'Nombre',
							'pt': 'Nome'
						}
					},
					{
						'Date Added': {
							'zh': '添加日期',
							'ja': '追加された日付',
							'es': 'Fecha Agregada',
							'pt': 'Data adicionada'
						}
					},
					{
						'Comment': {
							'zh': '评论',
							'ja': 'コメント',
							'es': 'Comentario',
							'pt': 'Comente'
						}
					},
					{
						'Task Ref': {
							'zh': '任务参考',
							'ja': 'タスク参照',
							'es': 'Tarea Ref',
							'pt': 'Ref. Tarefa'
						}
					},
					{
						'Notes': {
							'zh': '笔记',
							'ja': 'ノート',
							'es': 'Notas',
							'pt': 'Notas'
						}
					},
					{
						'Task Discussions': {
							'zh': '任务讨论',
							'ja': 'タスクディスカッション',
							'es': 'Discusiones de tareas',
							'pt': 'Discussões de tarefas'
						}
					}
				];

				for (let index = 0; index < mail_arr.length; index++) {
					const element = mail_arr[index];

					if (text in element) {
						ret = element[text][language];
						break;
					}

				}
				//console.log(ret);
				resolve(ret);
			}
		});
	},

	getTranslatedTextShipping: async (text, language) => {
		return new Promise(async (resolve, reject) => {
			if (language === Config.default_language) {
				resolve(text);
			} else {
				let ret = '';
				let ship_arr = [{
						'Delivered': {
							'zh': '发表',
							'ja': '配信済み',
							'es': 'Entregado',
							'pt': 'Entregue'
						},
						'Arrived': {
							'zh': '到达的',
							'ja': '到着した',
							'es': 'Llegado',
							'pt': 'Chegado'
						},
						'Departed': {
							'zh': '出发了',
							'ja': '出発',
							'es': 'Salido',
							'pt': 'Partiu'
						},
						'AWB Doc Generated': {
							'zh': '生成AWB文件',
							'ja': 'AWBドキュメントが生成されました',
							'es': 'AWB Doc Generado',
							'pt': 'AWB Doc Gerado'
						},
						'In Progress': {
							'zh': '进行中',
							'ja': '進行中',
							'es': 'En Progreso',
							'pt': 'Em Andamento'
						},
						'Shipment Picked': {
							'zh': '提货',
							'ja': '選択された出荷',
							'es': 'Envío Elegido',
							'pt': 'Remessa Escolhida'
						},
						'Packing List Generated': {
							'zh': '装箱单生成',
							'ja': '生成されたパッキングリスト',
							'es': 'Lista de Empaque Generada',
							'pt': 'Lista de Embalagem Gerada'
						},
						'Invoice Doc Generated': {
							'zh': '发票凭证已生成',
							'ja': '生成された請求書ドキュメント',
							'es': 'Documento de Factura Generado',
							'pt': 'Documento de Fatura Gerado'
						},
					},
				];

				for (let index = 0; index < ship_arr.length; index++) {
					const element = ship_arr[index];

					if (text in element) {
						ret = element[text][language];
						break;
					}

				}
				//console.log(ret);
				resolve(ret);
			}
		});
	}

}