const Config = require('../configuration/config');
const sapModel = require("../models/soap");

const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp')

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();



module.exports = {

    MyService: {
        MyPort: {
            AddInspectionLot: async function(args, cb, headers, req) {
                // console.log("hello i am in  Add !!",args['inspected_item']);


                //   var credentials = auth(req);
                //   var valid = true;

                //   if (credentials) {

                //     valid = compare(credentials.name, Config.sapAuth.username) && valid
                //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
                //     if(!valid){
                //         throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //     }                
                //   }else{
                //     throw {
                //             Fault: {
                //               Code: {
                //                 Value: '401'
                //               },
                //               Reason: { Text: 'Authentication failed' },
                //               statusCode: 401
                //             }
                //           };
                //   } 


                let insLogObj = {
                    request: JSON.stringify(args),
                    response: '',
                    method: 'POST',
                    url: '',
                    datetime: common.currentDateTime(),
                    request_type: 'INSPECTION_LOT_ADD'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);

                let all_request = args['inspected_item'];
                let error = 0;
                let err_msg = '';
                reason = '';
                let success_message = '';
                let dumped_index = [];

                if (all_request != null) {
                    for (let index = 0; index <= (all_request.length - 1); index++) {
                        if ( all_request[index]['invoice_number'] == '' ||
                             all_request[index]['sales_order_number'] == '' ||
                             all_request[index]['batch_number'] == '' || 
                             all_request[index]['material_number'] == '' || 
                             all_request[index]['inspection_lot_number'] == '' || 
                             all_request[index]['invoice_number'] == '?' || 
                             all_request[index]['sales_order_number'] == '?' || 
                             all_request[index]['batch_number'] == '?' || 
                             all_request[index]['material_number'] == '?' || 
                             all_request[index]['inspection_lot_number'] == '?' || 
                             all_request[index]['invoice_number'] == null || 
                             all_request[index]['sales_order_number'] == null || 
                             all_request[index]['batch_number'] == null || 
                             all_request[index]['material_number'] == null || 
                             all_request[index]['inspection_lot_number'] == null || 
                             all_request[index]['customer_no'] == '' || 
                             all_request[index]['customer_no'] == '?' || 
                             all_request[index]['customer_no'] == null) {
                            error++;
                            err_msg = 'Something went wrong, please try again with proper data';
                            dumped_index.push(`Something went wrong ,please try again with proper data for index ${index + 1 }`);
                            reason = `{"reason" : '${dumped_index}'}`;

                        } else {
                            if( isNaN(all_request[index]['invoice_number']) || isNaN(all_request[index]['sales_order_number']) || isNaN(all_request[index]['customer_no']) || isNaN(all_request[index]['material_number']) ){
                                error++;
                                err_msg = 'Non numeric values are not accepted';
                                dampped_index.push(`Non numeric values are not accepted for index ${index + 1}`);
                                reason = `{"reason" : '${dumped_index}'}`;

                            }else{
                                const dataObj = {
                                    invoice_number: entities.encode(all_request[index]['invoice_number'].replace(/^0+/, '')),
                                    sales_order_number: all_request[index]['sales_order_number'] ? entities.encode(all_request[index]['sales_order_number'].replace(/^0+/, '')) : '',
                                    batch_number: entities.encode(all_request[index]['batch_number'].replace(/^0+/, '')),
                                    inspection_lot_number: all_request[index]['inspection_lot_number'] ? entities.encode(all_request[index]['inspection_lot_number'].replace(/^0+/, '')) : '',
                                    material_number: all_request[index]['material_number'] ? entities.encode(all_request[index]['material_number'].replace(/^0+/, '')) : '',
                                    customer_no: all_request[index]['customer_no'] ? entities.encode(all_request[index]['customer_no'].replace(/^0+/, '')) : '',
                                    date_added: common.currentDateTime()
                                }
                                const check_inspection = await sapModel.check_inspection(dataObj.inspection_lot_number,dataObj.batch_number,dataObj.material_number);
                                if(check_inspection.length > 0){
                                        success_message = 'Data already exists'
                                        dumped_index.push(` Data already exists for index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`;

                                }else{
                                    const add_inspection_lot = await sapModel.add_inspection_lot(dataObj);
                                    if (add_inspection_lot.id > 0) {
                                        //do nothing
                                        success_message = 'Data added successfully'
                                    } else {
                                        error++;
                                        err_msg = 'Error occured in addition of data';
                                        dumped_index.push(`Error occurs in addition of data for index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`
                                    }
                                   
                                }
    
                                
                            }

                          


                        }

                    }


                } else {
                    error++;
                    err_msg = 'Can not accept null value.';
                    dumped_index.push(`Can not accept null value for index ${index + 1 }`);
                    reason = `{"reason" : '${dumped_index}'}`

                }
                if (error > 0) {
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : false}', reason);
                    }
                    common.logError(new Error(err_msg))
                    return {
                        status: false,
                        message: err_msg
                    };
                } else {
                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : true}', reason);
                    }
                    return {
                        status: true,
                        message: success_message
                    };
                }


            }

        }
    }
}