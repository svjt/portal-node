const request = require("request");
const Config = require('../configuration/config');
const sapModel = require("../models/soap");

const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp')
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');
var path = require('path');
const fs = require('fs');

const dateFormat = require('dateformat');
var striptags = require('striptags');

const s3Config = new AWS.S3({
    accessKeyId: Config.aws.accessKeyId,
    secretAccessKey: Config.aws.secretAccessKey
});


module.exports = {

    MyService: {
        MyPort: {
            UpdateInvoice: async function(args, cb, headers, req) {



                //    var credentials = auth(req);
                // var valid = true;

                // if (credentials) {

                //   valid = compare(credentials.name, Config.sapAuth.username) && valid
                //   valid = compare(credentials.pass, Config.sapAuth.password) && valid
                //   if(!valid){
                //       throw {
                //           Fault: {
                //             Code: {
                //               Value: '401'
                //             },
                //             Reason: { Text: 'Authentication failed' },
                //             statusCode: 401
                //           }
                //         };
                //   }                
                // }else{
                //   throw {
                //           Fault: {
                //             Code: {
                //               Value: '401'
                //             },
                //             Reason: { Text: 'Authentication failed' },
                //             statusCode: 401
                //           }
                //         };
                // } 

                let all_request = args['invoiceData'];
                if (all_request != null) {
                    let error = 0;
                    let err_msg = '';
                    let reason = '';
                    let success_message = '';
                    let logResponse = {};
                    let dumped_index = [];
                    for (let index = 0; index <= (all_request.length - 1); index++) {

                        const argsObj = {
                            invoice_number: all_request[index]['InvoiceNumber'] ? entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')) : '',
                            invoice_date: all_request[index]['InvoiceDate'] ? entities.encode(all_request[index]['InvoiceDate'].slice(0, 4) + "-" + all_request[index]['InvoiceDate'].slice(4, 6) + "-" + all_request[index]['InvoiceDate'].slice(6, 8)) : "",
                            invoice_location: all_request[index]['InvoiceLocation'] ? entities.encode(all_request[index]['InvoiceLocation']) : '',
                            sap_so_number: all_request[index]['SAPSONumber'] ? entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')) : '',
                            packing_date: all_request[index]['PackingCRDate'] ? entities.encode(all_request[index]['PackingCRDate'].slice(0, 4) + "-" + all_request[index]['PackingCRDate'].slice(4, 6) + "-" + all_request[index]['PackingCRDate'].slice(6, 8)) : "",
                            packing_location: all_request[index]['PackingCRLocation'] ? entities.encode(all_request[index]['PackingCRLocation']) : '',
                            logistics_partner: all_request[index]['logisticsPartner'] ? entities.encode(all_request[index]['logisticsPartner']) : '',
                            payment_status: all_request[index]['paymentStatus'] ? entities.encode(all_request[index]['paymentStatus']) : '',

                        }
                        // console.log("invoice_date : ",argsObj);
                        let insLogObj = {
                            request: JSON.stringify(argsObj),
                            response: '',
                            method: 'POST',
                            url: '',
                            datetime: common.currentDateTime(),
                            request_type: 'INVOICE_UPDATE'
                        };
                        logResponse = await sapModel.insertRequestLog(insLogObj);
                        // console.log("this is log response : ",logResponse);

                        if (all_request[index]['SAPSONumber'] == '' ||
                            all_request[index]['SAPSONumber'] == null ||
                            all_request[index]['SAPSONumber'] == '?' ||
                            all_request[index]['InvoiceNumber'] == '' ||
                            all_request[index]['InvoiceNumber'] == null ||
                            all_request[index]['InvoiceNumber'] == '?') {

                            error++;
                            err_msg = 'Null value is not accepted .';
                            dumped_index.push(`Null value is not accepted at index ${index + 1 }`);
                            //console.log("damped_index", dumped_index)
                            reason = `{"reason" : '${dumped_index}'}`
                            //console.log("++++reason : ", reason);


                        } else {

                            let dataObj = {};
                            let invoice = {};
                            let packing = {};

                            //CHECK INVOICE USED TO CHECK IF THE DATA WITH THE COMBINATION OF InvoiceNumber AND SAPSONumber IS PRESENT OR NOT  //

                            const check_invoice = await sapModel.check_invoice(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')));


                            if (check_invoice.length == 0) {

                                if (all_request[index]['InvoiceDate'] != '' &&
                                    all_request[index]['InvoiceDate'] != null &&
                                    all_request[index]['InvoiceLocation'] != '' &&
                                    all_request[index]['InvoiceLocation'] != null &&
                                    all_request[index]['invoice_upload'] != '' &&
                                    all_request[index]['invoice_upload'] != null
                                ) {
                                    invoice = all_request[index]['invoice_upload'];

                                    let new_file_name_invoice = '';


                                    if (invoice.filename != '' && invoice.filename != null) {
                                        var extention_invoice = path.extname(invoice.filename);
                                        if (extention_invoice === ".pdf") {
                                            new_file_name_invoice = 'invoice_uploads/' + (Math.floor(Date.now() / 1000)) + '-' + uuidv4() + extention_invoice;
                                            await module.exports.MyService.__upload_to_s3_bucket(new_file_name_invoice, invoice.content);
                                        } else {

                                            error++;
                                            err_msg = "invoice file is not a pdf file";
                                            dumped_index.push(`invoice file is not a pdf file at index ${index + 1 }`);
                                            reason = `{"reason" : '${dumped_index}'}`

                                        }
                                    } else {
                                        invoice.filename = '';
                                    }




                                    if (new_file_name_invoice != '' && new_file_name_invoice != null) {
                                        dataObj = {
                                            invoice_number: entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')),
                                            invoice_date: entities.encode(all_request[index]['InvoiceDate'].slice(0, 4) + "-" + all_request[index]['InvoiceDate'].slice(4, 6) + "-" + all_request[index]['InvoiceDate'].slice(6, 8)),
                                            invoice_location: entities.encode(all_request[index]['InvoiceLocation']),
                                            sap_so_number: entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')),
                                            invoice_file_name: entities.encode(invoice.filename),
                                            invoice_file_path: entities.encode(new_file_name_invoice),
                                            date_added: common.currentDateTime(),
                                            date_updated: common.currentDateTime()

                                        }
                                        const add_data = await sapModel.insertInvoice(dataObj);
                                        //console.log("++add data++",add_data );

                                        if (add_data.id > 0) {
                                            let add_on = false;
                                            if (all_request[index]['PackingCRDate'] != '' &&
                                                all_request[index]['PackingCRLocation'] != '' &&
                                                all_request[index]['PackingCRDate'] != null &&
                                                all_request[index]['PackingCRLocation'] != null &&
                                                all_request[index]['packing_upload'] != '' &&
                                                all_request[index]['packing_upload'] != null
                                            ) {
                                                

                                                packing = all_request[index]['packing_upload'];
                                                let new_file_name_packaging = '';


                                                if (packing.Packing_filename != '' && packing.Packing_filename != null) {
                                                    var extention_packaging = path.extname(packing.Packing_filename);
                                                    if (extention_packaging === ".pdf") {
                                                        new_file_name_packaging = 'packing_uploads/' + (Math.floor(Date.now() / 1000)) + '-' + uuidv4() + extention_packaging;
                                                        await module.exports.MyService.__upload_to_s3_bucket(new_file_name_packaging, packing.Packing_content);
                                                    } else {

                                                        error++;
                                                        err_msg = "Invoice file is not a pdf file";
                                                        dumped_index.push(`packing file is not a pdf file at index ${index + 1 }`);
                                                        reason = `{"reason" : '${dumped_index}'}`
                                                    }
                                                } else {
                                                    packing.Packing_filename = ''
                                                }




                                                if (new_file_name_packaging != '' && new_file_name_packaging != null) {
                                                    dataObj = {
                                                        packing_date: entities.encode(all_request[index]['PackingCRDate'].slice(0, 4) + "-" + all_request[index]['PackingCRDate'].slice(4, 6) + "-" + all_request[index]['PackingCRDate'].slice(6, 8)),
                                                        packing_location: entities.encode(all_request[index]['PackingCRLocation']),
                                                        packing_file_name: entities.encode(packing.Packing_filename),
                                                        packing_file_path: entities.encode(new_file_name_packaging),
                                                        date_updated: common.currentDateTime()
                                                    }
                                                    //console.log("dataObj : ",dataObj );

                                                    const update_packing = await sapModel.updatePacking(dataObj, all_request[index]['InvoiceNumber'].replace(/^0+/, ''), all_request[index]['SAPSONumber'].replace(/^0+/, ''));
                                                    if (update_packing.success == true) {

                                                        success_message = "Data successfully updated"
                                                        add_on = true;
                                                    } else {

                                                        error++;
                                                        err_msg = "error occers"
                                                        dumped_index.push(`error occers in packing update at index ${index + 1 }`);
                                                        reason = `{"reason" : '${dumped_index}'}`
                                                    }
                                                }


                                            }
                                            if (all_request[index]['paymentStatus'] != '' && all_request[index]['paymentStatus'] != null) {
                                                //console.log("hello am in ++++++++++++++++++",all_request[index]['InvoiceNumber'].replace(/^0+/, ''));


                                                const update_payment_status = await sapModel.update_payment_status(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), entities.encode(all_request[index]['paymentStatus']), common.currentDateTime(), entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')))
                                                if (update_payment_status && update_payment_status.id > 0) {
                                                    success_message = 'Data added successfully '
                                                    add_on = true;

                                                } else {
                                                    error++;
                                                    err_msg = "Please enter the proper data."
                                                    dumped_index.push(`Please enter the proper data at index ${index + 1 }`);
                                                    reason = `{"reason" : '${dumped_index}'}`

                                                }

                                            }
                                            if (all_request[index]['logisticsPartner'] != '' && all_request[index]['logisticsPartner'] != null) {

                                                const update_logistics_partner = await sapModel.update_logistics_partner(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), all_request[index]['logisticsPartner'], common.currentDateTime(), entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')))
                                                if (update_logistics_partner && update_logistics_partner.id > 0) {
                                                    success_message = 'Data added successfully ';
                                                    add_on = true;

                                                } else {
                                                    error++;
                                                    err_msg = "Please enter the proper data."
                                                    dumped_index.push(`Please enter the proper data at index ${index + 1 }`);
                                                    reason = `{"reason" : '${dumped_index}'}`

                                                }

                                            }
                                            if (add_on == false) {
                                                success_message = "Data added successfully "
                                            }


                                        } else {
                                            error++;
                                            err_msg = "Can not add invoice data "
                                            dumped_index.push(`Can not add invoice data for index ${index + 1 }`);
                                            reason = `{"reason" : '${dumped_index}'}`

                                        }
                                    } else {

                                        error++;
                                        err_msg = "Invoice file is not a pdf file"
                                        dumped_index.push(`Invoice file is not a pdf file at index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`

                                    }
                                } else {
                                    error++;
                                    err_msg = "Please enter the proper data."
                                    dumped_index.push(`Please enter the proper data. at index ${index + 1 }`);
                                    reason = `{"reason" : '${dumped_index}'}`
                                }

                            } else {
                                // console.log("hello i am in ");

                                let data_updated = false;

                                if (all_request[index]['PackingCRDate'] != '' &&
                                    all_request[index]['PackingCRLocation'] != '' &&
                                    all_request[index]['PackingCRDate'] != null &&
                                    all_request[index]['PackingCRLocation'] != null &&
                                    all_request[index]['packing_upload'] != '' &&
                                    all_request[index]['packing_upload'] != null) {

                                    let new_file_name_packaging = '';
                                    packing = all_request[index]['packing_upload'];


                                    const packing_exists = await sapModel.packing_exists(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), entities.encode(all_request[index]['SAPSONumber']).replace(/^0+/, ''));
                                    // console.log("hello packing +++++++++++++",packing_exists);

                                    if (packing_exists.success == false) {

                                        if (packing.Packing_filename != '' && packing.Packing_filename != null) {
                                            var extention_packaging = path.extname(packing.Packing_filename);
                                            if (extention_packaging === ".pdf") {
                                                new_file_name_packaging = 'packing_uploads/' + (Math.floor(Date.now() / 1000)) + '-' + uuidv4() + extention_packaging;
                                                await module.exports.MyService.__upload_to_s3_bucket(new_file_name_packaging, packing.Packing_content);
                                            } else {

                                                error++;
                                                err_msg = "packing file is not a pdf file";
                                                dumped_index.push(`packing file is not a pdf file at index ${index + 1 }`);
                                                reason = `{"reason" : '${dumped_index}'}`
                                            }
                                        } else {
                                            packing.Packing_filename = '';
                                        }

                                        dataObj = {
                                            packing_date: entities.encode(all_request[index]['PackingCRDate'].slice(0, 4) + "-" + all_request[index]['PackingCRDate'].slice(4, 6) + "-" + all_request[index]['PackingCRDate'].slice(6, 8)),
                                            packing_location: entities.encode(all_request[index]['PackingCRLocation']),
                                            packing_file_name: entities.encode(packing.Packing_filename),
                                            packing_file_path: new_file_name_packaging ? entities.encode(new_file_name_packaging) : '',
                                            date_updated: common.currentDateTime()
                                        }



                                        const update_packing = await sapModel.updatePacking(dataObj, all_request[index]['InvoiceNumber'].replace(/^0+/, ''), all_request[index]['SAPSONumber'].replace(/^0+/, ''));
                                        if (update_packing.success == true) {

                                            success_message = "Data successfully updated"
                                            data_updated = true;
                                        } else {

                                            error++;
                                            err_msg = "Error occurs"
                                            dumped_index.push(`error occurs in packing update at index ${index + 1 }`);
                                            reason = `{"reason" : '${dumped_index}'}`
                                        }

                                    }


                                }
                                if (all_request[index]['paymentStatus'] != '' && all_request[index]['paymentStatus'] != null) {

                                    const update_payment_status = await sapModel.update_payment_status(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), entities.encode(all_request[index]['paymentStatus']), common.currentDateTime(), entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')))
                                    if (update_payment_status && update_payment_status.id > 0) {
                                        success_message = 'Data added successfully '
                                        data_updated = true;
                                    } else {
                                        error++;
                                        err_msg = "Please enter the proper data.";
                                        dumped_index.push(`Please enter the proper data at index ${index + 1 }`);
                                        reason = `{"reason" : '${dumped_index}'}`

                                    }

                                }
                               
                                if (all_request[index]['logisticsPartner'] != '' && all_request[index]['logisticsPartner'] != null) {

                                    const check_logistic = await sapModel.check_logistic(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')),entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')));
                                    
                                    if(check_logistic && check_logistic.logistics_partner != null ){
                                        success_message = 'Data added successfully '
                                        data_updated = true;
                                    }else{

                                        const update_logistics_partner = await sapModel.update_logistics_partner(entities.encode(all_request[index]['InvoiceNumber'].replace(/^0+/, '')), all_request[index]['logisticsPartner'], common.currentDateTime(),entities.encode(all_request[index]['SAPSONumber'].replace(/^0+/, '')))
                                        if (update_logistics_partner.id > 0) {
                                            
                                            success_message = 'Data added successfully '
                                            data_updated = true;

                                        } else {
                                            error++;
                                            err_msg = "Please enter the proper data."
                                            dumped_index.push(`Please enter the proper data at index ${index + 1 }`);
                                            reason = `{"reason" : '${dumped_index}'}`

                                        }
                                    }

                                    
                                }
                                

                                if (data_updated == false) {
                                    error++;
                                    err_msg = "Data already exists."
                                    dumped_index.push(`Data already exists for index ${index + 1 }`);
                                    reason = `{"reason" : '${dumped_index}'}`

                                }



                            }

                        }


                    }


                    if (error > 0) {
                        if (logResponse) {
                            sapModel.updateRequestLog(logResponse.id, '{"status" : false}', reason);
                        }
                        common.logError(new Error(err_msg))
                        return {
                            status: false,
                            message: err_msg
                        };
                    } else {
                        if (logResponse) {
                            sapModel.updateRequestLog(logResponse.id, '{"status" : true}', reason);
                        }

                        return {
                            status: true,
                            message: success_message
                        };
                    }

                }
            }
        },
        __upload_to_s3_bucket: async (new_file_name, file_content) => {
            return new Promise(function(resolve, reject) {
                const imgBuffer = Buffer.from(file_content, 'base64');
                let params = {
                    Bucket: Config.aws.bucketName,
                    Key: new_file_name,
                    Body: imgBuffer
                };
                s3Config.upload(params, function(err, data) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(data);
                    }
                });
                resolve(true);
            });
        }

    }
}