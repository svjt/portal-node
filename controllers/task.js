const JWT = require('jsonwebtoken');
//require('dotenv').config();
const jwtSimple = require('jwt-simple');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const bcrypt = require('bcryptjs');
const AWS = require('aws-sdk');
const dateFormat = require('dateformat');
const taskModel = require('../models/task');
const customerModel = require('../models/user');
const employeeModel = require('../models/employees');
const agentModel = require('../models/agents');
const sapModel = require('../models/sap');
const addTaskModel = require('../models/addTaskClosedComment');
const outwardCrm = require('./outward_crm');
const Config = require('../configuration/config');
const Cryptr = require('cryptr');
const fs = require("fs");
var trim = require('trim');
const soapRequest = require('easy-soap-request');
const xmlParser = require('xml2json');

const join = require('path').join
const s3Zip = require('s3-zip')

const cryptr = new Cryptr(Config.cryptR.secret);
const common = require('./common');
// loginToken = user_data => {
//   return JWT.sign({
//     iss  : 'Reddys',
//     sub  : user_data.user_id,
//     name : user_data.name,
//     user : true,
//     ag   : user_data.user_agent,
//     iat  : Math.round((new Date().getTime())/1000),
//     exp  : Math.round((new Date().getTime())/1000)+(24*60*60)
//   }, Config.jwt.secret);
// }

var Hashids = require('hashids');
var hashids = new Hashids('', 50);


module.exports = {
  subject_line: String,
  /**
   * Generates a Add task
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Add task used for task selection
   */
  add_task: async (req, res, next) => {
    var today = common.currentDateTime();
    try {
      let taskObj = {};
      var request_id = req.body.request_id;
      let customer_id = 0;
      let submitted_by = 0;
      if (req.user.role == 2) {
        submitted_by = req.user.customer_id;
        customer_id = req.body.agent_customer_id;
      } else {
        customer_id = req.user.customer_id;
      }
      //console.log('task_customer_id', customer_id);
      var sla = await taskModel.get_total_sla(request_id);
      var days = await common.dayCountExcludingWeekends(sla.total_sla);
      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");

      //console.log( sla_due_date );

      var reference_key = 'R';
      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);

        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }

      const {
        product_id,
        country_id,
        pharmacopoeia,
        description,
        request_type_id,
        cc_customers,
        polymorphic_form,
        audit_visit_site_name,
        request_audit_visit_date,
        stability_data_type,
        gmp_clearance_id,
        tga_email_id,
        applicant_name,
        doc_required,
        quantity,
        rdd,
        po_number,
        share_with_agent,
        dmf_number,
        rdfrc,
        swi_data,
        notification_number,
        apos_document_type
      } = req.body;

      let language = '';
      if (req.user.empe > 0) {
        language = await customerModel.get_customer_language(customer_id);
      } else {
        language = await customerModel.get_customer_language(customer_id);
      }

      let product_arr = [];
      if(req.body.request_id == 23){
        let new_data = JSON.parse(swi_data);
        product_arr.push({label:new_data[0].product_name,value:new_data[0].product_id});
      }else{
        product_arr = JSON.parse(product_id);
      }

      
      let total = 0;

      for (let index = 0; index < product_arr.length; index++) {
        const elem_product_id = product_arr[index].value;

        if (req.body.request_id == 22 || req.body.request_id == 4 || req.body.request_id == 10 || req.body.request_id == 12 || req.body.request_id == 21 || req.body.request_id == 19 || req.body.request_id == 18 || req.body.request_id == 17 || req.body.request_id == 27 || req.body.request_id == 28 || req.body.request_id == 29 || req.body.request_id == 30 || req.body.request_id == 32 || req.body.request_id == 33 || req.body.request_id == 35 || req.body.request_id == 36 || req.body.request_id == 37 || req.body.request_id == 38 || req.body.request_id == 39 || req.body.request_id == 40 || req.body.request_id == 42) {

          let parsed_apos_document_type = [];
          if (req.body.request_id == 38) {
            if (apos_document_type && apos_document_type != '') {
              var parsed_apos_document_type_arr = JSON.parse(apos_document_type);
              for (let index = 0; index < parsed_apos_document_type_arr.length; index++) {
                parsed_apos_document_type.push(parsed_apos_document_type_arr[index].value);
              }

            }
          }

          taskObj = {
            customer_id: customer_id,
            product_id: elem_product_id,
            country_id: 0,
            parent_id: 0,
            due_date: sla_due_date,
            pharmacopoeia: entities.encode(pharmacopoeia),
            current_date: today,
            discussion: 0,
            description: entities.encode(description),
            request_type_id: req.body.request_id,
            gmp_clearance_id: entities.encode(gmp_clearance_id),
            tga_email_id: entities.encode(tga_email_id),
            applicant_name: entities.encode(applicant_name),
            doc_required: entities.encode(doc_required),
            submitted_by: submitted_by,
            close_status: 0,
            drupal_task_id: 0,
            priority: 2,
            rdd: rdd != '' && rdd != undefined ? entities.encode(rdd) : null,
            front_due_date: front_due_date,
            cc_customers: cc_customers,
            share_with_agent: share_with_agent,
            dmf_number: dmf_number ? entities.encode(dmf_number) : '',
            rdfrc: rdfrc ? entities.encode(rdfrc) : null,
            apos_document_type: (parsed_apos_document_type.length > 0) ? parsed_apos_document_type.join(',') : '',
            notification_number: notification_number ? entities.encode(notification_number) : '',
            language: language
          }
        }

        if (req.body.request_id == 5 || req.body.request_id == 8 || req.body.request_id == 14 || req.body.request_id == 15 || req.body.request_id == 16 || req.body.request_id == 6 || req.body.request_id == 11 || req.body.request_id == 20) {
          taskObj = {
            customer_id: customer_id,
            product_id: elem_product_id,
            country_id: 0,
            parent_id: 0,
            current_date: today,
            due_date: sla_due_date,
            discussion: 0,
            description: entities.encode(description),
            request_type_id: req.body.request_id,
            submitted_by: submitted_by,
            close_status: 0,
            drupal_task_id: 0,
            priority: 2,
            rdd: sla_due_date,
            front_due_date: front_due_date,
            cc_customers: cc_customers,
            share_with_agent: share_with_agent,
            language: language
          }
        }

        if (req.body.request_id == 3 || req.body.request_id == 2) {
          taskObj = {
            customer_id: customer_id,
            product_id: elem_product_id,
            country_id: 0,
            due_date: sla_due_date,
            parent_id: 0,
            pharmacopoeia: entities.encode(pharmacopoeia),
            polymorphic_form: entities.encode(polymorphic_form),
            current_date: today,
            discussion: 0,
            description: entities.encode(description),
            request_type_id: req.body.request_id,
            submitted_by: submitted_by,
            close_status: 0,
            drupal_task_id: 0,
            priority: 2,
            rdd: sla_due_date,
            front_due_date: front_due_date,
            cc_customers: cc_customers,
            share_with_agent: share_with_agent,
            language: language
          }
        }

        if (req.body.request_id == 9) {
          taskObj = {
            customer_id: customer_id,
            product_id: elem_product_id,
            country_id: 0,
            parent_id: 0,
            due_date: sla_due_date,
            audit_visit_site_name: entities.encode(audit_visit_site_name),
            request_audit_visit_date: entities.encode(request_audit_visit_date),
            current_date: today,
            discussion: 0,
            description: entities.encode(description),
            request_type_id: req.body.request_id,
            submitted_by: submitted_by,
            close_status: 0,
            drupal_task_id: 0,
            priority: 2,
            rdd: sla_due_date,
            front_due_date: front_due_date,
            cc_customers: cc_customers,
            share_with_agent: share_with_agent,
            language: language
          }
        }

        if (req.body.request_id == 13) {
          taskObj = {
            customer_id: customer_id,
            product_id: elem_product_id,
            country_id: 0,
            parent_id: 0,
            due_date: sla_due_date,
            stability_data_type: entities.encode(stability_data_type),
            current_date: today,
            discussion: 0,
            description: entities.encode(description),
            request_type_id: req.body.request_id,
            submitted_by: submitted_by,
            close_status: 0,
            drupal_task_id: 0,
            priority: 2,
            rdd: sla_due_date,
            front_due_date: front_due_date,
            cc_customers: cc_customers,
            share_with_agent: share_with_agent,
            language: language
          }
        }

        if (req.body.request_id == 23 || req.body.request_id == 31 || req.body.request_id == 41 || req.body.request_id == 44) {
          if (req.body.request_id == 23 || req.body.request_id == 41) {
            reference_key = 'O';
          }

          if (req.body.request_id == 23) {

            const swi_data_arr = JSON.parse(swi_data);

            taskObj = {
              customer_id: customer_id,
              product_id: swi_data_arr[0].product_id,
              country_id: 0,
              parent_id: 0,
              due_date: sla_due_date,
              quantity: entities.encode(`${swi_data_arr[0].quantity} ${swi_data_arr[0].unit[0].value}`),
              rdd: swi_data_arr[0].rdd,
              po_number: po_number ? entities.encode(po_number) : null,
              poDeliveryDate: (po_number != '') ? common.currentDate() : null,
              current_date: today,
              discussion: 0,
              description: entities.encode(description),
              request_type_id: req.body.request_id,
              submitted_by: submitted_by,
              close_status: 0,
              drupal_task_id: 0,
              priority: 2,
              front_due_date: front_due_date,
              cc_customers: cc_customers,
              share_with_agent: share_with_agent,
              pharmacopoeia: entities.encode(pharmacopoeia),
              language: language
            }
          } else {
            taskObj = {
              customer_id: customer_id,
              product_id: elem_product_id,
              country_id: 0,
              parent_id: 0,
              due_date: sla_due_date,
              quantity: entities.encode(quantity),
              rdd: rdd ? entities.encode(rdd) : null,
              po_number: po_number ? entities.encode(po_number) : null,
              poDeliveryDate: null,
              current_date: today,
              discussion: 0,
              description: entities.encode(description),
              request_type_id: req.body.request_id,
              submitted_by: submitted_by,
              close_status: 0,
              drupal_task_id: 0,
              priority: 2,
              front_due_date: front_due_date,
              cc_customers: cc_customers,
              share_with_agent: share_with_agent,
              pharmacopoeia: entities.encode(pharmacopoeia),
              language: language
            }
          }
        }

        //console.log('request.body',taskObj);

        taskObj.ccp_posted_by = (req.user.empe > 0) ? +req.user.empe : 0;

        await taskModel.add_task(taskObj)
          .then(async function (data) {
            let task_id = data.task_id;
            let first_temp_id = 0;
            if (req.body.request_id == 23) {
              const swi_data_arr = JSON.parse(swi_data);
              for (let index = 0; index < swi_data_arr.length; index++) {

                let insert_arr = [
                  task_id,
                  swi_data_arr[index].product_id,
                  swi_data_arr[index].rdd,
                  `${swi_data_arr[index].quantity} ${swi_data_arr[index].unit[0].value}`,
                  0,
                  common.currentDateTime(),
                  0
                ];
                let id = await taskModel.add_task_order(insert_arr);
                if (index == 0) {
                  first_temp_id = id;
                }
              }

              await taskModel.updateTempID(task_id, first_temp_id.id);

            }

            if (language != Config.default_language) {
              await module.exports.__translate_language(taskObj, task_id, country_id);
            }

            if (country_id && country_id != '') {
              var parsed_country_id = JSON.parse(country_id);
              for (let index = 0; index < parsed_country_id.length; index++) {
                await taskModel.map_countries(task_id, parsed_country_id[index].value);
              }
            }

            //console.log('taskObj.customer_id', taskObj.customer_id);
            let manager = await taskModel.get_allocated_manager(taskObj.customer_id).catch(err => {
              common.logError(err);
              res.status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
            });

            let ref_no = 'PHL-' + reference_key + '-' + task_id;
            let customerData = [];
            await taskModel.update_ref_no(ref_no, task_id)
              .then(async () => {
                // ACTIVITY LOG - SATYAJIT
                customerData = await customerModel.get_user(taskObj.customer_id);
                var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
                var params = {
                  task_ref: ref_no,
                  request_type: request_type_id,
                  customer_name: customername
                }
                if (req.user.role == 2) {
                  var agentData = await agentModel.get_user(submitted_by);
                  params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                  await taskModel.log_activity(2, task_id, params)
                } else {
                  await taskModel.log_activity(1, task_id, params)
                }
                // END LOG
              });

            // let spoc_exists = false;
            // let spoc = [];


            // if (language != Config.default_language && customerData[0].ignore_spoc == 2) {
            //   let assigned_spoc = await taskModel.get_allocated_spoc(taskObj.customer_id, language);
            //   assigned_spoc = await module.exports.__get_allocated_leave_employee_spoc(assigned_spoc, taskObj.customer_id);

            //   if (assigned_spoc.length > 0) {
            //     spoc_exists = true;
            //     spoc = assigned_spoc;
            //   } else if (await taskModel.check_language_spoc(language)) {
            //     spoc_exists = true;
            //     global_spoc = await taskModel.get_global_spoc(language);
            //     console.log(global_spoc);
            //     spoc = await module.exports.__get_allocated_leave_employee_spoc(global_spoc, taskObj.customer_id);
            //   }
            // }

            // console.log('spoc', spoc);

            let comment_arr = [];

            // if (spoc_exists && spoc.length > 0) {

            //   const assignment_spoc = {
            //     task_id: task_id,
            //     assigned_to: spoc[0].employee_id,
            //     assigned_by: -1,
            //     assign_date: today,
            //     due_date: sla_due_date,
            //     reopen_date: today,
            //     parent_assignment_id: 0
            //   }

            //   var spoc_parent_assign_id = await taskModel.assign_task(assignment_spoc).catch(err => {
            //     common.logError(err);
            //     res.status(400)
            //       .json({
            //         status: 3,
            //         message: Config.errorText.value
            //       }).end();
            //   });

            //   // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //   var spoc_recipient = `${spoc[0].first_name} ${spoc[0].last_name}`;
            //   var params = {
            //     task_ref: ref_no,
            //     request_type: request_type_id,
            //     employee_sender: 'SYSTEM',
            //     employee_recipient: spoc_recipient + ` (${(spoc[0].desig_name)})`
            //   }
            //   await taskModel.log_activity(8, task_id, params);
            //   // END LOG

            //   let employee_notification = 22;
            //   let param_notification = {};
            //   let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //   let cc_arr = [
            //     employee_notification,
            //     task_id,
            //     spoc[0].employee_id,
            //     common.currentDateTime(),
            //     0,
            //     notification_text,
            //     employee_notification,
            //     mail_subject
            //   ]
            //   await taskModel.notify_employee(cc_arr);


            //   manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

            //   if (manager.length > 0) {
            //     let assignment_val = 2;
            //     await taskModel.update_task_assignment(task_id, spoc[0].employee_id, assignment_val);

            //     let task_details = await taskModel.tasks_details(task_id);

            //     var customer_details = await customerModel.get_user(task_details[0].customer_id);

            //     var mail_subject = '';
            //     if (task_details[0].request_type == 24) {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
            //     } else {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
            //     }

            //     var emp_details = [];
            //     await taskModel.update_task_owner(manager[0].employee_id, task_id).catch(err => {
            //       common.logError(err);
            //       res.status(400)
            //         .json({
            //           status: 3,
            //           message: Config.errorText.value
            //         }).end();
            //     });


            //     const assignment = {
            //       task_id: task_id,
            //       assigned_to: manager[0].employee_id,
            //       assigned_by: spoc[0].employee_id,
            //       assign_date: today,
            //       due_date: sla_due_date,
            //       reopen_date: today,
            //       parent_assignment_id: spoc_parent_assign_id.assignment_id
            //     }

            //     var parent_assign_id = await taskModel.assign_task(assignment).catch(err => {
            //       common.logError(err);
            //       res.status(400)
            //         .json({
            //           status: 3,
            //           message: Config.errorText.value
            //         }).end();
            //     });

            //     // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //     var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
            //     var params = {
            //       task_ref: ref_no,
            //       request_type: request_type_id,
            //       employee_sender: spoc_recipient + ` (${(spoc[0].desig_name)})`,
            //       employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
            //     }
            //     await taskModel.log_activity(8, task_id, params);
            //     // END LOG

            //     let employee_notification = 8;
            //     let param_notification = {
            //       employee_name: `${spoc[0].first_name} (${(spoc[0].desig_name)})`,
            //       assigned_employee: `you`
            //     };
            //     let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //     let cc_arr = [
            //       employee_notification,
            //       task_id,
            //       manager[0].employee_id,
            //       common.currentDateTime(),
            //       0,
            //       notification_text,
            //       employee_notification,
            //       mail_subject
            //     ]
            //     await taskModel.notify_employee(cc_arr);

            //     let task_comment = {
            //       posted_by: spoc[0].employee_id,
            //       task_id: task_id,
            //       comment: `This task has been auto assigned to ${manager[0].first_name} ${manager[0].last_name} (${(manager[0].desig_name)}) by the system on my behalf.`,
            //       assignment_id: spoc_parent_assign_id.assignment_id,
            //       post_date: today
            //     };

            //     await taskModel.insert_comment_log(task_comment);

            //     comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});


            //     //CSC TECHNICAL
            //     if (
            //       request_id === 1 || request_id === 22 ||
            //       request_id === 2 || request_id === 3 ||
            //       request_id === 4 || request_id === 5 ||
            //       request_id === 8 || request_id === 9 ||
            //       request_id === 10 || request_id === 12 ||
            //       request_id === 13 || request_id === 14 ||
            //       request_id === 15 || request_id === 16 ||
            //       request_id === 21 || request_id === 6 ||
            //       request_id === 11 || request_id === 7 ||
            //       request_id === 33 || request_id == 35 ||
            //       request_id == 37 || request_id == 38 ||
            //       request_id == 42
            //     ) {

            //       emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2).catch(err => {
            //         common.logError(err);
            //         res.status(400)
            //           .json({
            //             status: 3,
            //             message: Config.errorText.value
            //           }).end();
            //       });


            //     }

            //     //CSC COMMERCIAL
            //     if (request_id === 23 || request_id == 34 || request_id === 41) {

            //       emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);

            //     }

            //     //RA
            //     if (
            //       request_id === 17 || request_id === 18 ||
            //       request_id === 19 || request_id === 20 ||
            //       request_id === 27 || request_id === 28 ||
            //       request_id === 29 || request_id === 30 ||
            //       request_id == 36 | request_id == 39 || request_id == 40
            //     ) {

            //       emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 3);

            //     }

            //     var bot_assignment_id = 0;

            //     emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

            //     if (emp_details.length > 0) {
            //       let assignment_val = 2;
            //       await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
            //         .then(async function (data) {

            //           const assign_tasks = {
            //             assigned_by: manager[0].employee_id,
            //             assigned_to: emp_details[0].employee_id,
            //             task_id: task_id,
            //             due_date: sla_due_date,
            //             assign_date: today,
            //             parent_assignment_id: parent_assign_id.assignment_id,
            //             comment: '',
            //             reopen_date: today
            //           }

            //           await taskModel.assign_task(assign_tasks)
            //             .then(async function (tdata) {

            //               // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
            //               var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
            //               var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
            //               var params = {
            //                 task_ref: ref_no,
            //                 request_type: request_type_id,
            //                 employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
            //                 employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
            //               }
            //               await taskModel.log_activity(8, task_id, params)
            //               // END LOG

            //               bot_assignment_id = tdata.assignment_id;

            //               let employee_notification = 8;
            //               let param_notification = {
            //                 employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
            //                 assigned_employee: `you`
            //               };
            //               let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //               let cc_arr = [
            //                 employee_notification,
            //                 task_id,
            //                 emp_details[0].employee_id,
            //                 common.currentDateTime(),
            //                 0,
            //                 notification_text,
            //                 employee_notification,
            //                 mail_subject
            //               ]
            //               await taskModel.notify_employee(cc_arr);

            //               const task_comment = {
            //                 posted_by: manager[0].employee_id,
            //                 task_id: task_id,
            //                 comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
            //                 assignment_id: bot_assignment_id,
            //                 post_date: today
            //               };

            //               await taskModel.insert_comment_log(task_comment);

            //               comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

            //               //console.log('child assignment',bot_assignment_id);

            //             }).catch(err => {
            //               common.logError(err);
            //               res.status(400)
            //                 .json({
            //                   status: 3,
            //                   message: Config.errorText.value
            //                 }).end();
            //             });

            //         }).catch(err => {
            //           common.logError(err);
            //           res.status(400)
            //             .json({
            //               status: 3,
            //               message: Config.errorText.value
            //             }).end();
            //         })
            //     } else {

            //       //console.log('parent_assign_id ==>', parent_assign_id)
            //       bot_assignment_id = parent_assign_id.assignment_id;
            //       //console.log('parent assignment',bot_assignment_id);
            //     }


            //     //CURRENT OWNER
            //     if (emp_details.length > 0) {
            //       await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
            //     } else {
            //       await taskModel.update_current_owner(task_id, manager[0].employee_id).catch(err => {
            //         common.logError(err);
            //         res.status(400)
            //           .json({
            //             status: 3,
            //             message: Config.errorText.value
            //           }).end();
            //       });
            //     }


            //     //POSTED CC CUSTOMERS
            //     if (taskObj.cc_customers) {
            //       let cc_cust_arr = JSON.parse(taskObj.cc_customers);
            //       //INSERT CC CUSTOMERS
            //       if (cc_cust_arr.length > 0) {
            //         for (let index = 0; index < cc_cust_arr.length; index++) {
            //           const element = cc_cust_arr[index].customer_id;
            //           let insrt_cc = {
            //             customer_id: element,
            //             task_id: task_id,
            //             status: 1
            //           };
            //           await customerModel.insert_cc_customer(insrt_cc);
            //         }
            //       }
            //     }

            //     //HIGHLIGHT NEW TASK
            //     let customer_id = taskObj.customer_id;

            //     let highlight_arr = [
            //       task_id,
            //       customer_id,
            //       1,
            //       'C'
            //     ];
            //     await taskModel.task_highlight(highlight_arr);

            //     let cc_list = await taskModel.task_cc_customer(task_id);
            //     if (cc_list && cc_list.length > 0) {
            //       for (let index = 0; index < cc_list.length; index++) {
            //         const element = cc_list[index];
            //         let highlight_cc_arr = [
            //           task_id,
            //           element.customer_id,
            //           1,
            //           'C'
            //         ];
            //         await taskModel.task_highlight(highlight_cc_arr);
            //       }
            //     }


            //     if (taskObj.share_with_agent == 1) {
            //       let customer_details = await customerModel.get_user(taskObj.customer_id);
            //       let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            //       if (agent_list && agent_list.length > 0) {
            //         for (let index = 0; index < agent_list.length; index++) {
            //           const element = agent_list[index];
            //           let highlight_agent_arr = [
            //             task_id,
            //             element.agent_id,
            //             1,
            //             'A'
            //           ];
            //           await taskModel.task_highlight(highlight_agent_arr);
            //         }
            //       }
            //     }

            //     //SEND MAIL

            //     var pharmacopoeia, polymorphic_form, stability_data_type, stability_data_type, audit_visit_site_name, request_audit_visit_date, quantity, batch_number, nature_of_issue, shipping_address, samples, impurities, working_standards, rdd, dmf_number, apos_document_type, notification_number, rdfrc = '';

            //     var request_type = task_details[0].request_type;

            //     if (request_type === 1) {

            //       var req_type_arr = task_details[0].service_request_type.split(',');

            //       if (req_type_arr.indexOf('Samples') !== -1) {
            //         samples = '<tr><td>Samples</td><td></td></tr>';

            //         if (task_details[0].number_of_batches != null) {
            //           samples += '<tr><td>Number Of Batches</td><td>' + task_details[0].number_of_batches + '</td></tr>';
            //         } else {
            //           samples += '<tr><td>Number Of Batches</td><td></td></tr>';
            //         }

            //         if (task_details[0].quantity != null) {
            //           samples += '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
            //         } else {
            //           samples += '<tr><td>Quantity</td><td></td></tr>';
            //         }
            //       } else {
            //         samples = '';
            //       }

            //       if (req_type_arr.indexOf('Working Standards') !== -1) {
            //         working_standards = '<tr><td>Working Standards</td><td></td></tr>';

            //         if (task_details[0].working_quantity != null) {
            //           working_standards += '<tr><td>Quantity</td><td>' + task_details[0].working_quantity + '</td></tr>';
            //         } else {
            //           working_standards += '<tr><td>Quantity</td><td></td></tr>';
            //         }

            //       } else {
            //         working_standards = '';
            //       }

            //       if (req_type_arr.indexOf('Impurities') !== -1) {
            //         impurities = '<tr><td>Impurities</td><td></td></tr>';

            //         if (task_details[0].impurities_quantity != null) {
            //           impurities += '<tr><td>Quantity</td><td>' + task_details[0].impurities_quantity + '</td></tr>';
            //         } else {
            //           impurities += '<tr><td>Quantity</td><td></td></tr>';
            //         }

            //         if (task_details[0].specify_impurity_required != null) {
            //           impurities += '<tr><td>Specify Impurity Required</td><td>' + task_details[0].specify_impurity_required + '</td></tr>';
            //         } else {
            //           impurities += '<tr><td>Specify Impurity Required</td><td></td></tr>';
            //         }

            //       } else {
            //         impurities = '';
            //       }

            //       shipping_address = '<tr><td>Shipping Address</td><td>' + task_details[0].shipping_address + '</td></tr>';
            //     } else {
            //       shipping_address = '';
            //       samples = '';
            //       working_standards = '';
            //       impurities = '';
            //     }

            //     if (request_type === 23 || request_type === 7 || request_id === 41) {
            //       quantity = '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
            //     } else {
            //       quantity = '';
            //     }

            //     if (request_type === 23 || request_id === 41) {
            //       rdd = '<tr><td>Requested Date Of Delivery</td><td>' + common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy') + '</td></tr>';
            //     } else {
            //       rdd = '';
            //     }

            //     if (request_type === 7) {
            //       nature_of_issue = '<tr><td>Nature Of Issue</td><td>' + task_details[0].nature_of_issue + '</td></tr>';
            //       batch_number = '<tr><td>Batch No</td><td>' + task_details[0].batch_number + '</td></tr>';
            //     } else {
            //       nature_of_issue = '';
            //       batch_number = '';
            //     }

            //     if (request_type === 1 || request_type === 22 || request_type === 21 || request_type === 19 || request_type === 18 || request_type === 17 || request_type === 2 || request_type === 3 || request_type === 4 || request_type === 10 || request_type === 12 || request_type == 30 || request_type == 32 || request_type == 33 || request_type == 35 || request_type == 36 || request_type == 37) {
            //       pharmacopoeia = '<tr><td>Pharmacopoeia</td><td>' + task_details[0].pharmacopoeia + '</td></tr>';
            //     } else {
            //       pharmacopoeia = '';
            //     }

            //     if (request_type === 2 || request_type === 3) {
            //       polymorphic_form = '<tr><td>Polymorphic Form</td><td>' + task_details[0].polymorphic_form + '</td></tr>';
            //     } else {
            //       polymorphic_form = '';
            //     }

            //     if (request_type === 13) {
            //       stability_data_type = '<tr><td>Stability Data Type</td><td>' + task_details[0].stability_data_type + '</td></tr>';
            //     } else {
            //       stability_data_type = '';
            //     }

            //     if (request_type === 9) {
            //       audit_visit_site_name = '<tr><td>Site Name</td><td>' + task_details[0].audit_visit_site_name + '</td></tr>';

            //       var visit_date = new Date(task_details[0].request_audit_visit_date);
            //       var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');
            //       request_audit_visit_date = '<tr><td>Audit/Visit Date</td><td>' + visit_date_updt + '</td></tr>';
            //     } else {
            //       audit_visit_site_name = '';
            //       request_audit_visit_date = '';
            //     }

            //     var country = await taskModel.tasks_details_countries(task_id);

            //     if (country && country[0].country_name != '') {
            //       var country_name = country[0].country_name;
            //     } else {
            //       var country_name = '';
            //     }

            //     var country_name_by_lang = country_name;
            //     if (language != Config.default_language) {
            //       //COUNTRY
            //       let t_country = await taskModel.get_translated_country(task_id, language);
            //       country_name_by_lang = t_country[0].country_name;
            //     }

            //     if (request_type == 27) {
            //       dmf_number = '<tr><td>DMF Number</td><td>' + task_details[0].dmf_number + '</td></tr>';
            //     } else {
            //       dmf_number = '';
            //     }

            //     if (request_type == 29) {
            //       notification_number = '<tr><td>Notification Number</td><td>' + task_details[0].notification_number + '</td></tr>';
            //     } else {
            //       notification_number = '';
            //     }

            //     if (request_type == 28) {

            //       if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
            //         var rdfrc_date_updt = '';
            //       } else {

            //         var rdfrc_date = new Date(task_details[0].rdfrc);
            //         var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
            //       }

            //       rdfrc = '<tr><td>Requested date of response/closure</td><td>' + rdfrc_date_updt + '</td></tr>';
            //     } else {
            //       rdfrc = '';
            //     }

            //     if (request_type == 38) {
            //       apos_document_type = '<tr><td>Document Type</td><td>' + task_details[0].apos_document_type + '</td></tr>';
            //     } else {
            //       apos_document_type = '';
            //     }


            //     var db_due_date = new Date(task_details[0].due_date);
            //     var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

            //     let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

            //     var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

            //     // if (customer_details[0].language_code != Config.default_language) {

            //     //     let t_product_name = await taskModel.get_translated_product(task_details[0].product_id, data[0].language);
            //     //     product_name_cust = t_product_name[0].name;


            //     // }else{
            //     //     product_name_cust = task_details[0].product_name;
            //     // }
            //     //console.log("MAIL arr ", mail_arr);

            //     // var cust_status_mail = `
            //     // <!DOCTYPE html>
            //     // <html>
            //     //   <body>
            //     //       <p><em>Thank you for your request</em></p>
            //     //       <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
            //     //       <p></p>
            //     //       <p>Thank you for placing a request -  ${task_details[0].req_name}.</p>
            //     //       <p>We have received your request with the following details:</p>
            //     //       <table>
            //     //         <tr>
            //     //             <td>Task Ref</td>
            //     //             <td>${task_details[0].task_ref}</td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>User Name</td>
            //     //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>Request Type</td>
            //     //             <td>${task_details[0].req_name}</td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>Product</td>
            //     //             <td>${task_details[0].product_name}</td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>Market</td>
            //     //             <td>${country_name}</td>
            //     //         </tr>
            //     //           ${pharmacopoeia}
            //     //           ${polymorphic_form}
            //     //           ${stability_data_type}
            //     //           ${audit_visit_site_name}
            //     //           ${request_audit_visit_date}
            //     //           ${quantity}
            //     //           ${rdd}
            //     //           ${batch_number}
            //     //           ${nature_of_issue}
            //     //           ${samples}
            //     //           ${working_standards}
            //     //           ${impurities}
            //     //           ${shipping_address}
            //     //           ${dmf_number}
            //     //           ${apos_document_type}
            //     //           ${notification_number}
            //     //           ${rdfrc}
            //     //         <tr>
            //     //             <td valign="top">Requirement</td>
            //     //             <td valign="top">${entities.decode(task_details[0].content)}</td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //         </tr>
            //     //         <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //         </tr>
            //     //       </table>
            //     //       <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}" >Click here</a> to view the details on this request</p>
            //     //       <p>--  Dr.Reddys API team</p>
            //     //   </body>
            //     // </html>`;


            //     // //console.log(cust_status_mail);
            //     // var mail_options1 = {
            //     //   from: Config.contactUsMail,
            //     //   to: mail_arr[0].to_arr,
            //     //   subject: mail_subject,
            //     //   html: cust_status_mail
            //     // };
            //     // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //     //   mail_options1.cc = mail_arr[1].cc_arr;
            //     // }
            //     // //console.log(mail_options1);
            //     // common.sendMailB2C(mail_options1);
            //     //console.log("REQUEST TYPE ", request_type);

            //     var language_extn = 'req_name_' + task_details[0].language;
            //     var req_name_by_lang = task_details[0].req_name;
            //     var product_name_by_lang = task_details[0].product_name;
            //     switch (entities.decode(task_details[0].language)) {
            //       case 'es':
            //         req_name_by_lang = task_details[0].req_name_es;
            //         product_name_by_lang = task_details[0].product_name_es;
            //         break;
            //       case 'pt':
            //         req_name_by_lang = task_details[0].req_name_pt;
            //         product_name_by_lang = task_details[0].product_name_pt;
            //         break;
            //       case 'ja':
            //         req_name_by_lang = task_details[0].req_name_ja;
            //         product_name_by_lang = task_details[0].product_name_ja;
            //         break;
            //       case 'zh':
            //         req_name_by_lang = task_details[0].req_name_zh;
            //         product_name_by_lang = task_details[0].product_name_zh;
            //         break;
            //       default:
            //         req_name_by_lang = task_details[0].req_name;
            //         product_name_by_lang = task_details[0].product_name;
            //         break;
            //     }
            //     if(product_name_by_lang=='' || product_name_by_lang==null){
            //       product_name_by_lang = task_details[0].product_name;
            //     }

            //     var quantity_by_lang = task_details[0].quantity;
            //     if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
            //       let q_arr = task_details[0].quantity.split(' ');
            //       let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //       quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            //     }

            //     if (request_type === 4 || request_type === 10 || request_type === 12 || request_type === 17 || request_type === 18 || request_type === 19 || request_type === 21 || request_type === 22 || request_type == 30 || request_type == 32 || request_type == 33 || request_type == 35 || request_type == 36 || request_type == 37 || request_type === 42) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail 
            //         let mailParam = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: task_details[0].pharmacopoeia,
            //           req_name: req_name_by_lang
            //         };


            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_15TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', spoc[0].email, Config.webmasterMail, '', spocMailParam, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
            //       }

            //     } else if (request_type === 5 || request_type === 6 || request_type === 8 || request_type === 11 || request_type === 14 || request_type === 15 || request_type === 16 || request_type === 20) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail 
            //         let mailParam1 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang
            //         };

            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_8TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam1, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam1 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', spoc[0].email, Config.webmasterMail, '', spocMailParam1, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam1 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam1, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam1 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam1, "e");
            //       }

            //     } else if (request_type === 2 || request_type === 3) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail 
            //         let mailParam2 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: task_details[0].pharmacopoeia,
            //           polymorphic_form: task_details[0].polymorphic_form,
            //           req_name: req_name_by_lang
            //         };

            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COA_AND_MOA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam2, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam2 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           polymorphic_form: (entities.decode(task_details[0].language) != 'en') ? task_details[0].polymorphic_form_translate : task_details[0].polymorphic_form,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', spoc[0].email, Config.webmasterMail, '', spocMailParam2, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam2 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //         polymorphic_form: (entities.decode(task_details[0].language) != 'en') ? task_details[0].polymorphic_form_translate : task_details[0].polymorphic_form,
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', manager[0].email, Config.webmasterMail, '', managerMailParam2, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam2 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           polymorphic_form: (entities.decode(task_details[0].language) != 'en') ? task_details[0].polymorphic_form_translate : task_details[0].polymorphic_form,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', emp_details[0].email, Config.webmasterMail, '', empMailParam2, "e");
            //       }
            //     } else if (request_type === 7 || request_type === 34) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail 
            //         let mailParam3 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           nature_of_issue: task_details[0].nature_of_issue,
            //           quantity: quantity_by_lang,
            //           batch_number: task_details[0].batch_number,
            //           req_name: req_name_by_lang
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }
            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam3, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam3 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //           quantity: task_details[0].quantity,
            //           batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', spoc[0].email, Config.webmasterMail, '', spocMailParam3, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam3 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //         quantity: task_details[0].quantity,
            //         batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', manager[0].email, Config.webmasterMail, '', managerMailParam3, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam3 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //           quantity: task_details[0].quantity,
            //           batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', emp_details[0].email, Config.webmasterMail, '', empMailParam3, "e");
            //       }
            //     } else if (request_type === 9) {
            //       var visit_date = new Date(task_details[0].request_audit_visit_date);
            //       var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');
            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam4 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           audit_visit_site_name: task_details[0].audit_visit_site_name,
            //           audit_visit_date: visit_date_updt,
            //           req_name: req_name_by_lang
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam4, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam4 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           audit_visit_site_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].audit_visit_site_name_translate : task_details[0].audit_visit_site_name,
            //           audit_visit_date: visit_date_updt,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', spoc[0].email, Config.webmasterMail, '', spocMailParam4, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam4 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         audit_visit_site_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].audit_visit_site_name_translate : task_details[0].audit_visit_site_name,
            //         audit_visit_date: visit_date_updt,
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam4, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam4 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           audit_visit_site_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].audit_visit_site_name_translate : task_details[0].audit_visit_site_name,
            //           audit_visit_date: visit_date_updt,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam4, "e");
            //       }
            //     } else if (request_type === 13) {

            //       var stability_data_type_by_lang = task_details[0].stability_data_type;
            //       if (task_details[0].language != Config.default_language) {
            //         let t_stability_data_type = await taskModel.get_translated_stability_data_type(task_details[0].stability_data_type, task_details[0].language);
            //         stability_data_type_by_lang = t_stability_data_type[0].name;
            //       }

            //       if (mail_arr.length > 0) {
            //         //Customer Mail 
            //         let mailParam5 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           stability_data_type: stability_data_type_by_lang,
            //           req_name: req_name_by_lang
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_STABILITY_DATA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam5, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam5 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           stability_data_type: task_details[0].stability_data_type,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', spoc[0].email, Config.webmasterMail, '', spocMailParam5, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam5 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         stability_data_type: task_details[0].stability_data_type,
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', manager[0].email, Config.webmasterMail, '', managerMailParam5, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam5 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           stability_data_type: task_details[0].stability_data_type,
            //           req_name: task_details[0].req_name,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', emp_details[0].email, Config.webmasterMail, '', empMailParam5, "e");
            //       }
            //     } else if (request_type === 23) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam6 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           quantity: quantity_by_lang,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy')
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NEW_ORDER', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam6, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam6 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', spoc[0].email, Config.webmasterMail, '', spocMailParam6, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam6 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         quantity: task_details[0].quantity,
            //         rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', manager[0].email, Config.webmasterMail, '', managerMailParam6, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam6 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', emp_details[0].email, Config.webmasterMail, '', empMailParam6, "e");
            //       }
            //     } else if (request_type === 41) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam7 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           quantity: quantity_by_lang,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           pharmacopoeia: task_details[0].pharmacopoeia
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam7, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam7 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', spoc[0].email, Config.webmasterMail, '', spocMailParam7, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam7 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         quantity: task_details[0].quantity,
            //         rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //         pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam7, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam7 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam7, "e");
            //       }
            //     } else if (request_type === 31) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam8 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           quantity: quantity_by_lang,
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PRICE_QUOTE', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam8, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam8 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', spoc[0].email, Config.webmasterMail, '', spocMailParam8, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam8 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         quantity: task_details[0].quantity,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', manager[0].email, Config.webmasterMail, '', managerMailParam8, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam8 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           quantity: task_details[0].quantity,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', emp_details[0].email, Config.webmasterMail, '', empMailParam8, "e");
            //       }
            //     } else if (request_type === 27) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam9 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           dmf_number: task_details[0].dmf_number,
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DMF_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam9, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam9 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', spoc[0].email, Config.webmasterMail, '', spocMailParam9, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam9 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam9, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam9 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam9, "e");
            //       }
            //     } else if (request_type === 28) {

            //       if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
            //         var rdfrc_date_updt = '';
            //       } else {
            //         var rdfrc_date = new Date(task_details[0].rdfrc);
            //         var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
            //       }
            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam91 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           rdfrc_date_updt: rdfrc_date_updt,
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam91, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam91 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           rdfrc_date_updt: rdfrc_date_updt,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', spoc[0].email, Config.webmasterMail, '', spocMailParam91, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam91 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         rdfrc_date_updt: rdfrc_date_updt,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam91, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam91 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           rdfrc_date_updt: rdfrc_date_updt,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam91, "e");
            //       }
            //     } else if (request_type === 29) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail                
            //         let mailParam10 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           notification_number: task_details[0].notification_number,
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }
            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam10, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam10 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           notification_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].notification_number_translate : task_details[0].notification_number,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', spoc[0].email, Config.webmasterMail, '', spocMailParam10, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam10 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         notification_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].notification_number_translate : task_details[0].notification_number,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam10, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam10 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           notification_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].notification_number_translate : task_details[0].notification_number,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam10, "e");
            //       }
            //     } else if (request_type === 38) {

            //       if (mail_arr.length > 0) {
            //         //Customer Mail         
            //         var document_type_by_lang = task_details[0].apos_document_type;
            //         if (task_details[0].language != Config.default_language) {
            //           let t_apos_document_type = await taskModel.get_translated_apos_document_type(task_details[0].apos_document_type, task_details[0].language);
            //           document_type_by_lang = t_apos_document_type;
            //         }

            //         let mailParam11 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           document_type: document_type_by_lang,
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam11, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam11 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           document_type: task_details[0].apos_document_type,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', spoc[0].email, Config.webmasterMail, '', spocMailParam11, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam11 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         document_type: task_details[0].apos_document_type,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', manager[0].email, Config.webmasterMail, '', managerMailParam11, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam11 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           document_type: task_details[0].apos_document_type,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', emp_details[0].email, Config.webmasterMail, '', empMailParam11, "e");
            //       }
            //     } else if (request_type === 39) {
            //       if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
            //         var rdfrc_date_updt = '';
            //       } else {

            //         var rdfrc_date = new Date(task_details[0].rdfrc);
            //         var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
            //       }
            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam12 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           dmf_number: task_details[0].dmf_number,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy')
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }

            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam12, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam12 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', spoc[0].email, Config.webmasterMail, '', spocMailParam12, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam12 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //         rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', manager[0].email, Config.webmasterMail, '', managerMailParam12, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam12 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', emp_details[0].email, Config.webmasterMail, '', empMailParam12, "e");
            //       }
            //     } else if (request_type === 40) {
            //       if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
            //         var rdfrc_date_updt = '';
            //       } else {

            //         var rdfrc_date = new Date(task_details[0].rdfrc);
            //         var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
            //       }

            //       if (mail_arr.length > 0) {
            //         //Customer Mail
            //         let mailParam13 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: product_name_by_lang,
            //           country_name: country_name_by_lang,
            //           task_content: entities.decode(task_details[0].content),
            //           url_status: url_status,
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: req_name_by_lang,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           gmp_clearance_id: task_details[0].gmp_clearance_id,
            //           tga_email_id: task_details[0].tga_email_id,
            //           applicant_name: task_details[0].applicant_name,
            //           doc_required: task_details[0].doc_required
            //         };
            //         var mailcc = '';
            //         if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //           mailcc = mail_arr[1].cc_arr;
            //         }
            //         console.log("SENDING TASK MAIL 40");
            //         const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam13, "c");
            //       }

            //       if (spoc[0].email != '') {
            //         //SPOC Mail
            //         let spocMailParam13 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           format_due_date: format_due_date,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           gmp_clearance_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].gmp_clearance_id_translate : task_details[0].gmp_clearance_id,
            //           tga_email_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].tga_email_id_translate : task_details[0].tga_email_id,
            //           applicant_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].applicant_name_translate : task_details[0].applicant_name,
            //           doc_required: (entities.decode(task_details[0].language) != 'en') ? task_details[0].doc_required_translate : task_details[0].doc_required,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: spoc[0].first_name
            //         };
            //         const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', spoc[0].email, Config.webmasterMail, '', spocMailParam13, "e");
            //       }

            //       //Manager Mail
            //       let managerMailParam13 = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         format_due_date: format_due_date,
            //         rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //         gmp_clearance_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].gmp_clearance_id_translate : task_details[0].gmp_clearance_id,
            //         tga_email_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].tga_email_id_translate : task_details[0].tga_email_id,
            //         applicant_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].applicant_name_translate : task_details[0].applicant_name,
            //         doc_required: (entities.decode(task_details[0].language) != 'en') ? task_details[0].doc_required_translate : task_details[0].doc_required,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: manager[0].first_name
            //       };
            //       const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', manager[0].email, Config.webmasterMail, '', managerMailParam13, "e");

            //       if (emp_details.length > 0) {
            //         //Employee Mail
            //         var empMailParam13 = {
            //           first_name: entities.decode(customer_details[0].first_name),
            //           last_name: entities.decode(customer_details[0].last_name),
            //           task_ref: task_details[0].task_ref,
            //           product_name: task_details[0].product_name,
            //           country_name: country_name,
            //           format_due_date: format_due_date,
            //           task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //           company_name: entities.decode(customer_details[0].company_name),
            //           req_name: task_details[0].req_name,
            //           rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
            //           gmp_clearance_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].gmp_clearance_id_translate : task_details[0].gmp_clearance_id,
            //           tga_email_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].tga_email_id_translate : task_details[0].tga_email_id,
            //           applicant_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].applicant_name_translate : task_details[0].applicant_name,
            //           doc_required: (entities.decode(task_details[0].language) != 'en') ? task_details[0].doc_required_translate : task_details[0].doc_required,
            //           ccpurl: Config.ccpUrl,
            //           emp_first_name: emp_details[0].first_name
            //         };
            //         const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', emp_details[0].email, Config.webmasterMail, '', empMailParam13, "e");
            //       }
            //     }



            //     // const mailOptions = {
            //     //   from: Config.webmasterMail, // sender address
            //     //   to: manager[0].email,// manager[0].email, // list of receivers
            //     //   subject: mail_subject, // Subject line
            //     //   html: `<!DOCTYPE html>
            //     //   <html>
            //     //      <body>
            //     //         <p><em>New Task Assigned</em></p>
            //     //         <p>Hello ${manager[0].first_name},</p>
            //     //         <p>You have a new task assigned.</p>
            //     //         <p>Task Details are as below,</p>
            //     //         <table>
            //     //             <tr>
            //     //               <td>Task Ref</td>
            //     //               <td>${task_details[0].task_ref}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>User Name</td>
            //     //               <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>Request Type</td>
            //     //               <td>${task_details[0].req_name}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>Product</td>
            //     //               <td>${task_details[0].product_name}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>Market</td>
            //     //               <td>${country_name}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>Due Date</td>
            //     //               <td>${format_due_date}</td>
            //     //             </tr>
            //     //             ${pharmacopoeia}
            //     //             ${polymorphic_form}
            //     //             ${stability_data_type}
            //     //             ${audit_visit_site_name}
            //     //             ${request_audit_visit_date}
            //     //             ${quantity}
            //     //             ${rdd}
            //     //             ${batch_number}
            //     //             ${nature_of_issue}
            //     //             ${samples}
            //     //             ${working_standards}
            //     //             ${impurities}
            //     //             ${shipping_address}
            //     //             ${dmf_number}
            //     //             ${apos_document_type}
            //     //             ${notification_number}
            //     //             ${rdfrc}
            //     //             <tr>
            //     //               <td>Requirement</td>
            //     //               <td>${entities.decode(task_details[0].content)}</td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>&nbsp;</td>
            //     //               <td></td>
            //     //             </tr>
            //     //             <tr>
            //     //               <td>&nbsp;</td>
            //     //               <td></td>
            //     //             </tr>
            //     //         </table>
            //     //         <p>
            //     //           Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
            //     //         </p>   
            //     //      </body>
            //     //   </html>`
            //     // };
            //     // common.sendMail(mailOptions);

            //     // if (emp_details.length > 0) {
            //     //   const mailOptions2 = {
            //     //     from: Config.webmasterMail, // sender address
            //     //     to: emp_details[0].email, //emp_details[0].email, // list of receivers
            //     //     subject: mail_subject, // Subject line
            //     //     html: `<!DOCTYPE html>
            //     //     <html>
            //     //        <body>
            //     //           <p><em>New Task Assigned</em></p>
            //     //           <p>Hello ${emp_details[0].first_name},</p>
            //     //           <p>You have a new task assigned.</p>
            //     //           <p>Task Details are as below,</p>
            //     //           <table>
            //     //               <tr>
            //     //                 <td>Task Ref</td>
            //     //                 <td>${task_details[0].task_ref}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>User Name</td>
            //     //                 <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>Request Type</td>
            //     //                 <td>${task_details[0].req_name}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>Product</td>
            //     //                 <td>${task_details[0].product_name}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>Market</td>
            //     //                 <td>${country_name}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>Due Date</td>
            //     //                 <td>${format_due_date}</td>
            //     //               </tr>
            //     //               ${pharmacopoeia}
            //     //               ${polymorphic_form}
            //     //               ${stability_data_type}
            //     //               ${audit_visit_site_name}
            //     //               ${request_audit_visit_date}
            //     //               ${quantity}
            //     //               ${rdd}
            //     //               ${batch_number}
            //     //               ${nature_of_issue}
            //     //               ${samples}
            //     //               ${working_standards}
            //     //               ${impurities}
            //     //               ${shipping_address}
            //     //               ${dmf_number}
            //     //               ${apos_document_type}
            //     //               ${notification_number}
            //     //               ${rdfrc}
            //     //               <tr>
            //     //                 <td valign="top">Requirement</td>
            //     //                 <td valign="top">${entities.decode(task_details[0].content)}</td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>&nbsp;</td>
            //     //                 <td></td>
            //     //               </tr>
            //     //               <tr>
            //     //                 <td>&nbsp;</td>
            //     //                 <td></td>
            //     //               </tr>
            //     //           </table>
            //     //           <p>
            //     //             Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
            //     //           </p>   
            //     //        </body>
            //     //     </html>`
            //     //   };
            //     //   common.sendMail(mailOptions2);
            //     // }

            //     //SEND MAIL

            //   } else {
            //     //Unallocated Task Soumyadeep
            //     var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
            //     common.logError(err);
            //   }

            // } else {

            manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);
            if (manager.length > 0) {

              let task_details = await taskModel.tasks_details(task_id);

              var customer_details = await customerModel.get_user(task_details[0].customer_id);

              var mail_subject = '';
              if (task_details[0].request_type == 24) {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
              } else {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
              }

              var emp_details = [];
              await taskModel.update_task_owner(manager[0].employee_id, task_id).catch(err => {
                common.logError(err);
                res.status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value
                  }).end();
              });


              const assignment = {
                task_id: task_id,
                assigned_to: manager[0].employee_id,
                assigned_by: -1,
                assign_date: today,
                due_date: sla_due_date,
                reopen_date: today,
                parent_assignment_id: 0
              }

              var parent_assign_id = await taskModel.assign_task(assignment).catch(err => {
                common.logError(err);
                res.status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value
                  }).end();
              });

              // ACTIVITY LOG FROM SYSTEM - SATYAJIT
              var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
              var params = {
                task_ref: ref_no,
                request_type: request_type_id,
                employee_sender: 'SYSTEM',
                employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
              }
              await taskModel.log_activity(8, task_id, params);
              // END LOG

              let employee_notification = 22;
              let param_notification = {};
              let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

              let cc_arr = [
                employee_notification,
                task_id,
                manager[0].employee_id,
                common.currentDateTime(),
                0,
                notification_text,
                employee_notification,
                mail_subject
              ]
              await taskModel.notify_employee(cc_arr);


              //CSC TECHNICAL
              if (
                request_id === 1 || request_id === 22 ||
                request_id === 2 || request_id === 3 ||
                request_id === 4 || request_id === 5 ||
                request_id === 8 || request_id === 9 ||
                request_id === 10 || request_id === 12 ||
                request_id === 13 || request_id === 14 ||
                request_id === 15 || request_id === 16 ||
                request_id === 21 || request_id === 6 ||
                request_id === 11 || request_id === 7 ||
                request_id === 33 || request_id == 35 ||
                request_id == 37 || request_id == 38 ||
                request_id == 42
              ) {

                emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2).catch(err => {
                  common.logError(err);
                  res.status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value
                    }).end();
                });


              }

              //CSC COMMERCIAL
              if (request_id === 23 || request_id == 34 || request_id === 41 || request_id === 44) {

                emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);

              }

              //RA
              if (
                request_id === 17 || request_id === 18 ||
                request_id === 19 || request_id === 20 ||
                request_id === 27 || request_id === 28 ||
                request_id === 29 || request_id === 30 ||
                request_id == 36 | request_id == 39 || request_id == 40
              ) {

                emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 3);

              }

              var bot_assignment_id = 0;

              emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

              if (emp_details.length > 0) {
                let assignment_val = 2;
                await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                  .then(async function (data) {

                    const assign_tasks = {
                      assigned_by: manager[0].employee_id,
                      assigned_to: emp_details[0].employee_id,
                      task_id: task_id,
                      due_date: sla_due_date,
                      assign_date: today,
                      parent_assignment_id: parent_assign_id.assignment_id,
                      comment: '',
                      reopen_date: today
                    }

                    await taskModel.assign_task(assign_tasks)
                      .then(async function (tdata) {

                        // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                        var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                        var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                        var params = {
                          task_ref: ref_no,
                          request_type: request_type_id,
                          employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                          employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                        }
                        await taskModel.log_activity(8, task_id, params)
                        // END LOG

                        bot_assignment_id = tdata.assignment_id;

                        let employee_notification = 8;
                        let param_notification = {
                          employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                          assigned_employee: `you`
                        };
                        let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                        let cc_arr = [
                          employee_notification,
                          task_id,
                          emp_details[0].employee_id,
                          common.currentDateTime(),
                          0,
                          notification_text,
                          employee_notification,
                          mail_subject
                        ]
                        await taskModel.notify_employee(cc_arr);

                        const task_comment = {
                          posted_by: manager[0].employee_id,
                          task_id: task_id,
                          comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
                          assignment_id: bot_assignment_id,
                          post_date: today
                        };

                        await taskModel.insert_comment_log(task_comment);

                        comment_arr.push({ comment: task_comment.comment, posted_by: task_comment.posted_by });

                        //console.log('child assignment',bot_assignment_id);

                      }).catch(err => {
                        common.logError(err);
                        res.status(400)
                          .json({
                            status: 3,
                            message: Config.errorText.value
                          }).end();
                      });

                  }).catch(err => {
                    common.logError(err);
                    res.status(400)
                      .json({
                        status: 3,
                        message: Config.errorText.value
                      }).end();
                  })
              } else {

                //console.log('parent_assign_id ==>', parent_assign_id)
                bot_assignment_id = parent_assign_id.assignment_id;
                //console.log('parent assignment',bot_assignment_id);
              }


              //CURRENT OWNER
              if (emp_details.length > 0) {
                await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
              } else {
                await taskModel.update_current_owner(task_id, manager[0].employee_id).catch(err => {
                  common.logError(err);
                  res.status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value
                    }).end();
                });
              }


              //POSTED CC CUSTOMERS
              if (taskObj.cc_customers) {
                let cc_cust_arr = JSON.parse(taskObj.cc_customers);
                //INSERT CC CUSTOMERS
                if (cc_cust_arr.length > 0) {
                  for (let index = 0; index < cc_cust_arr.length; index++) {
                    const element = cc_cust_arr[index].customer_id;
                    let insrt_cc = {
                      customer_id: element,
                      task_id: task_id,
                      status: 1
                    };
                    await customerModel.insert_cc_customer(insrt_cc);
                  }
                }
              }

              //HIGHLIGHT NEW TASK
              let customer_id = taskObj.customer_id;

              let highlight_arr = [
                task_id,
                customer_id,
                1,
                'C'
              ];
              await taskModel.task_highlight(highlight_arr);

              let cc_list = await taskModel.task_cc_customer(task_id);
              if (cc_list && cc_list.length > 0) {
                for (let index = 0; index < cc_list.length; index++) {
                  const element = cc_list[index];
                  let highlight_cc_arr = [
                    task_id,
                    element.customer_id,
                    1,
                    'C'
                  ];
                  await taskModel.task_highlight(highlight_cc_arr);
                }
              }


              if (taskObj.share_with_agent == 1) {
                let customer_details = await customerModel.get_user(taskObj.customer_id);
                let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

                if (agent_list && agent_list.length > 0) {
                  for (let index = 0; index < agent_list.length; index++) {
                    const element = agent_list[index];
                    let highlight_agent_arr = [
                      task_id,
                      element.agent_id,
                      1,
                      'A'
                    ];
                    await taskModel.task_highlight(highlight_agent_arr);
                  }
                }
              }

              //SEND MAIL

              var pharmacopoeia, polymorphic_form, stability_data_type, stability_data_type, audit_visit_site_name, request_audit_visit_date, quantity, batch_number, nature_of_issue, shipping_address, samples, impurities, working_standards, rdd, dmf_number, apos_document_type, notification_number, rdfrc = '';

              var request_type = task_details[0].request_type;

              if (request_type === 1) {

                var req_type_arr = task_details[0].service_request_type.split(',');

                if (req_type_arr.indexOf('Samples') !== -1) {
                  samples = '<tr><td>Samples</td><td></td></tr>';

                  if (task_details[0].number_of_batches != null) {
                    samples += '<tr><td>Number Of Batches</td><td>' + task_details[0].number_of_batches + '</td></tr>';
                  } else {
                    samples += '<tr><td>Number Of Batches</td><td></td></tr>';
                  }

                  if (task_details[0].quantity != null) {
                    samples += '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
                  } else {
                    samples += '<tr><td>Quantity</td><td></td></tr>';
                  }
                } else {
                  samples = '';
                }

                if (req_type_arr.indexOf('Working Standards') !== -1) {
                  working_standards = '<tr><td>Working Standards</td><td></td></tr>';

                  if (task_details[0].working_quantity != null) {
                    working_standards += '<tr><td>Quantity</td><td>' + task_details[0].working_quantity + '</td></tr>';
                  } else {
                    working_standards += '<tr><td>Quantity</td><td></td></tr>';
                  }

                } else {
                  working_standards = '';
                }

                if (req_type_arr.indexOf('Impurities') !== -1) {
                  impurities = '<tr><td>Impurities</td><td></td></tr>';

                  if (task_details[0].impurities_quantity != null) {
                    impurities += '<tr><td>Quantity</td><td>' + task_details[0].impurities_quantity + '</td></tr>';
                  } else {
                    impurities += '<tr><td>Quantity</td><td></td></tr>';
                  }

                  if (task_details[0].specify_impurity_required != null) {
                    impurities += '<tr><td>Specify Impurity Required</td><td>' + task_details[0].specify_impurity_required + '</td></tr>';
                  } else {
                    impurities += '<tr><td>Specify Impurity Required</td><td></td></tr>';
                  }

                } else {
                  impurities = '';
                }

                shipping_address = '<tr><td>Shipping Address</td><td>' + task_details[0].shipping_address + '</td></tr>';
              } else {
                shipping_address = '';
                samples = '';
                working_standards = '';
                impurities = '';
              }

              if (request_type === 23 || request_type === 7 || request_type === 41 || request_type === 44) {
                quantity = '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
              } else {
                quantity = '';
              }

              if (request_type === 23 || request_type === 41) {
                rdd = '<tr><td>Requested Date Of Delivery</td><td>' + common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy') + '</td></tr>';
              } else {
                rdd = '';
              }

              if (request_type === 7) {
                nature_of_issue = '<tr><td>Nature Of Issue</td><td>' + task_details[0].nature_of_issue + '</td></tr>';
                batch_number = '<tr><td>Batch No</td><td>' + task_details[0].batch_number + '</td></tr>';
              } else {
                nature_of_issue = '';
                batch_number = '';
              }

              if (request_type === 1 || request_type === 22 || request_type === 21 || request_type === 19 || request_type === 18 || request_type === 17 || request_type === 2 || request_type === 3 || request_type === 4 || request_type === 10 || request_type === 12 || request_type == 30 || request_type == 32 || request_type == 33 || request_type == 35 || request_type == 36 || request_type == 37) {
                pharmacopoeia = '<tr><td>Pharmacopoeia</td><td>' + task_details[0].pharmacopoeia + '</td></tr>';
              } else {
                pharmacopoeia = '';
              }

              if (request_type === 2 || request_type === 3) {
                polymorphic_form = '<tr><td>Polymorphic Form</td><td>' + task_details[0].polymorphic_form + '</td></tr>';
              } else {
                polymorphic_form = '';
              }

              if (request_type === 13) {
                stability_data_type = '<tr><td>Stability Data Type</td><td>' + task_details[0].stability_data_type + '</td></tr>';
              } else {
                stability_data_type = '';
              }

              if (request_type === 9) {
                audit_visit_site_name = '<tr><td>Site Name</td><td>' + task_details[0].audit_visit_site_name + '</td></tr>';

                var visit_date = new Date(task_details[0].request_audit_visit_date);
                var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');
                request_audit_visit_date = '<tr><td>Audit/Visit Date</td><td>' + visit_date_updt + '</td></tr>';
              } else {
                audit_visit_site_name = '';
                request_audit_visit_date = '';
              }

              var country = await taskModel.tasks_details_countries(task_id);

              if (country && country[0].country_name != '') {
                var country_name = country[0].country_name;
              } else {
                var country_name = '';
              }

              var country_name_by_lang = country_name;
              if (language != Config.default_language) {
                //COUNTRY
                let t_country = await taskModel.get_translated_country(task_id, language);
                country_name_by_lang = t_country[0].country_name;
              }

              if (request_type == 27) {
                dmf_number = '<tr><td>DMF Number</td><td>' + task_details[0].dmf_number + '</td></tr>';
              } else {
                dmf_number = '';
              }

              if (request_type == 29) {
                notification_number = '<tr><td>Notification Number</td><td>' + task_details[0].notification_number + '</td></tr>';
              } else {
                notification_number = '';
              }

              if (request_type == 28) {

                if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
                  var rdfrc_date_updt = '';
                } else {

                  var rdfrc_date = new Date(task_details[0].rdfrc);
                  var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
                }

                rdfrc = '<tr><td>Requested date of response/closure</td><td>' + rdfrc_date_updt + '</td></tr>';
              } else {
                rdfrc = '';
              }

              if (request_type == 38) {
                apos_document_type = '<tr><td>Document Type</td><td>' + task_details[0].apos_document_type + '</td></tr>';
              } else {
                apos_document_type = '';
              }

              // New order temp order mail product content
              var temp_task_product_html = `<table><tr><th>${await common.getTranslatedTextMail('Product', task_details[0].language)}</th><th>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</th><th>RDD</th></tr>`;
              var temp_task_product_emp_html = `<table><tr><th>Product</th><th>Quantity</th><th>RDD</th></tr>`;
              if (request_type == 23) {
                var temp_task_details = await taskModel.get_temp_task_orders(task_id);

                for (let index = 0; index < temp_task_details.length; index++) {

                  temp_task_details[index].quantity_en = temp_task_details[index].quantity;
                  if (temp_task_details[index].quantity && trim(temp_task_details[index].quantity) != '') {
                    let q_arr = temp_task_details[index].quantity.split(' ');
                    if (q_arr[1] == 'KG') {
                      q_arr[1] = "Kgs";
                    }
                    let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], temp_task_details[index].task_language);
                    temp_task_details[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
                    let en_q_arr = await taskModel.get_translated_quantity(q_arr[1], 'en');
                    temp_task_details[index].quantity_en = `${q_arr[0]} ${en_q_arr[0].name}`;
                  }

                  let t_product_name = await taskModel.get_translated_product_new(temp_task_details[index].product_id, temp_task_details[index].task_language);
                  temp_task_details[index].product_name = t_product_name[0].name;
                  let en_product_name = await taskModel.get_translated_product_new(temp_task_details[index].product_id, 'en');

                  temp_task_product_html += `<tr><td>${temp_task_details[index].product_name}</td><td>${temp_task_details[index].quantity}</td><td>${temp_task_details[index].rdd}</td></tr>`;
                  temp_task_product_emp_html += `<tr><td>${en_product_name[0].name}</td><td>${temp_task_details[index].quantity_en}</td><td>${temp_task_details[index].rdd}</td></tr>`;
                }
              }
              temp_task_product_html += `</table>`; temp_task_product_emp_html += `</table>`;


              var db_due_date = new Date(task_details[0].due_date);
              var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

              let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

              var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;
              console.log("MAIL arr ", mail_arr);

              // var cust_status_mail = `
              // <!DOCTYPE html>
              // <html>
              //   <body>
              //       <p><em>Thank you for your request</em></p>
              //       <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
              //       <p></p>
              //       <p>Thank you for placing a request -  ${task_details[0].req_name}.</p>
              //       <p>We have received your request with the following details:</p>
              //       <table>
              //         <tr>
              //             <td>Task Ref</td>
              //             <td>${task_details[0].task_ref}</td>
              //         </tr>
              //         <tr>
              //             <td>User Name</td>
              //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //         </tr>
              //         <tr>
              //             <td>Request Type</td>
              //             <td>${task_details[0].req_name}</td>
              //         </tr>
              //         <tr>
              //             <td>Product</td>
              //             <td>${task_details[0].product_name}</td>
              //         </tr>
              //         <tr>
              //             <td>Market</td>
              //             <td>${country_name}</td>
              //         </tr>
              //           ${pharmacopoeia}
              //           ${polymorphic_form}
              //           ${stability_data_type}
              //           ${audit_visit_site_name}
              //           ${request_audit_visit_date}
              //           ${quantity}
              //           ${rdd}
              //           ${batch_number}
              //           ${nature_of_issue}
              //           ${samples}
              //           ${working_standards}
              //           ${impurities}
              //           ${shipping_address}
              //           ${dmf_number}
              //           ${apos_document_type}
              //           ${notification_number}
              //           ${rdfrc}
              //         <tr>
              //             <td valign="top">Requirement</td>
              //             <td valign="top">${entities.decode(task_details[0].content)}</td>
              //         </tr>
              //         <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //         </tr>
              //         <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //         </tr>
              //       </table>
              //       <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}" >Click here</a> to view the details on this request</p>
              //       <p>--  Dr.Reddys API team</p>
              //   </body>
              // </html>`;


              // //console.log(cust_status_mail);
              // var mail_options1 = {
              //   from: Config.contactUsMail,
              //   to: mail_arr[0].to_arr,
              //   subject: mail_subject,
              //   html: cust_status_mail
              // };
              // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
              //   mail_options1.cc = mail_arr[1].cc_arr;
              // }
              // //console.log(mail_options1);
              // common.sendMailB2C(mail_options1);
              console.log("REQUEST TYPE ", request_type);

              var language_extn = 'req_name_' + task_details[0].language;
              var req_name_by_lang = task_details[0].req_name;
              var product_name_by_lang = task_details[0].product_name;
              switch (entities.decode(task_details[0].language)) {
                case 'es':
                  req_name_by_lang = task_details[0].req_name_es;
                  product_name_by_lang = task_details[0].product_name_es;
                  break;
                case 'pt':
                  req_name_by_lang = task_details[0].req_name_pt;
                  product_name_by_lang = task_details[0].product_name_pt;
                  break;
                case 'ja':
                  req_name_by_lang = task_details[0].req_name_ja;
                  product_name_by_lang = task_details[0].product_name_ja;
                  break;
                case 'zh':
                  req_name_by_lang = task_details[0].req_name_zh;
                  product_name_by_lang = task_details[0].product_name_zh;
                  break;
                default:
                  req_name_by_lang = task_details[0].req_name;
                  product_name_by_lang = task_details[0].product_name;
                  break;
              }
              if (product_name_by_lang == '' || product_name_by_lang == null) {
                product_name_by_lang = task_details[0].product_name;
              }

              var quantity_by_lang = task_details[0].quantity;
              if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
                let q_arr = task_details[0].quantity.split(' ');
                let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
                quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
              }


              if (request_type === 4 || request_type === 10 || request_type === 12 || request_type === 17 || request_type === 18 || request_type === 19 || request_type === 21 || request_type === 22 || request_type == 30 || request_type == 32 || request_type == 33 || request_type == 35 || request_type == 36 || request_type == 37 || request_type === 42) {

                if (mail_arr.length > 0) {
                  //Customer Mail 
                  let mailParam = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    pharmacopoeia: task_details[0].pharmacopoeia,
                    req_name: req_name_by_lang
                  };


                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_15TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");
                }

                //Manager Mail
                let managerMailParam = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
                }

              } else if (request_type === 5 || request_type === 6 || request_type === 8 || request_type === 11 || request_type === 14 || request_type === 15 || request_type === 16 || request_type === 20) {

                if (mail_arr.length > 0) {
                  //Customer Mail 
                  let mailParam1 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang
                  };

                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_8TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam1, "c");
                }

                //Manager Mail
                let managerMailParam1 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam1, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam1 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam1, "e");
                }

              } else if (request_type === 2 || request_type === 3) {

                if (mail_arr.length > 0) {
                  //Customer Mail 
                  let mailParam2 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    pharmacopoeia: task_details[0].pharmacopoeia,
                    polymorphic_form: task_details[0].polymorphic_form,
                    req_name: req_name_by_lang
                  };

                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COA_AND_MOA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam2, "c");
                }

                //Manager Mail
                let managerMailParam2 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                  polymorphic_form: (entities.decode(task_details[0].language) != 'en') ? task_details[0].polymorphic_form_translate : task_details[0].polymorphic_form,
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', manager[0].email, Config.webmasterMail, '', managerMailParam2, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam2 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                    polymorphic_form: (entities.decode(task_details[0].language) != 'en') ? task_details[0].polymorphic_form_translate : task_details[0].polymorphic_form,
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', emp_details[0].email, Config.webmasterMail, '', empMailParam2, "e");
                }
              } else if (request_type === 7 || request_type === 34) {

                if (mail_arr.length > 0) {
                  //Customer Mail 
                  let mailParam3 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    nature_of_issue: task_details[0].nature_of_issue,
                    quantity: quantity_by_lang,
                    batch_number: task_details[0].batch_number,
                    req_name: req_name_by_lang
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }
                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam3, "c");
                }

                //Manager Mail
                let managerMailParam3 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
                  quantity: task_details[0].quantity,
                  batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', manager[0].email, Config.webmasterMail, '', managerMailParam3, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam3 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
                    quantity: task_details[0].quantity,
                    batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', emp_details[0].email, Config.webmasterMail, '', empMailParam3, "e");
                }
              } else if (request_type === 9) {
                var visit_date = new Date(task_details[0].request_audit_visit_date);
                var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');
                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam4 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    audit_visit_site_name: task_details[0].audit_visit_site_name,
                    audit_visit_date: visit_date_updt,
                    req_name: req_name_by_lang
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam4, "c");
                }

                //Manager Mail
                let managerMailParam4 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  audit_visit_site_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].audit_visit_site_name_translate : task_details[0].audit_visit_site_name,
                  audit_visit_date: visit_date_updt,
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam4, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam4 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    audit_visit_site_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].audit_visit_site_name_translate : task_details[0].audit_visit_site_name,
                    audit_visit_date: visit_date_updt,
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam4, "e");
                }
              } else if (request_type === 13) {

                var stability_data_type_by_lang = task_details[0].stability_data_type;
                if (task_details[0].language != Config.default_language) {
                  let t_stability_data_type = await taskModel.get_translated_stability_data_type(task_details[0].stability_data_type, task_details[0].language);
                  stability_data_type_by_lang = t_stability_data_type[0].name;
                }

                if (mail_arr.length > 0) {
                  //Customer Mail 
                  let mailParam5 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    stability_data_type: stability_data_type_by_lang,
                    req_name: req_name_by_lang
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_STABILITY_DATA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam5, "c");
                }

                //Manager Mail
                let managerMailParam5 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  stability_data_type: task_details[0].stability_data_type,
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', manager[0].email, Config.webmasterMail, '', managerMailParam5, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam5 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    stability_data_type: task_details[0].stability_data_type,
                    req_name: task_details[0].req_name,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', emp_details[0].email, Config.webmasterMail, '', empMailParam5, "e");
                }
              } else if (request_type === 23) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam6 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    quantity: quantity_by_lang,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    po_number: task_details[0].po_no,
                    temp_task_product_html: temp_task_product_html
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }
                  //const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NEW_ORDER', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam6, "c");

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TEMP_TASK_TEMPLATE_FOR_NEW_ORDER', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam6, "c");
                }

                //Manager Mail
                let managerMailParam6 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  quantity: task_details[0].quantity,
                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name,
                  po_number: task_details[0].po_no,
                  temp_task_product_html: temp_task_product_emp_html
                };
                //const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', manager[0].email, Config.webmasterMail, '', managerMailParam6, "e");
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TEMP_TASK_TEMPLATE_FOR_NEW_ORDER', manager[0].email, Config.webmasterMail, '', managerMailParam6, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam6 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    quantity: task_details[0].quantity,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name,
                    po_number: task_details[0].po_no,
                    temp_task_product_html: temp_task_product_emp_html
                  };
                  //const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', emp_details[0].email, Config.webmasterMail, '', empMailParam6, "e");
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TEMP_TASK_TEMPLATE_FOR_NEW_ORDER', emp_details[0].email, Config.webmasterMail, '', empMailParam6, "e");
                }
              } else if (request_type === 41) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam7 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    quantity: quantity_by_lang,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    pharmacopoeia: task_details[0].pharmacopoeia
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam7, "c");
                }

                //Manager Mail
                let managerMailParam7 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  quantity: task_details[0].quantity,
                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                  pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam7, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam7 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    quantity: task_details[0].quantity,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    pharmacopoeia: (entities.decode(task_details[0].language) != 'en') ? task_details[0].pharmacopoeia_translate : task_details[0].pharmacopoeia,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam7, "e");
                }
              } else if (request_type === 31) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam8 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    quantity: quantity_by_lang,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PRICE_QUOTE', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam8, "c");
                }

                //Manager Mail
                let managerMailParam8 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  quantity: task_details[0].quantity,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', manager[0].email, Config.webmasterMail, '', managerMailParam8, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam8 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    quantity: task_details[0].quantity,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', emp_details[0].email, Config.webmasterMail, '', empMailParam8, "e");
                }
              } else if (request_type === 27) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam9 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    dmf_number: task_details[0].dmf_number,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DMF_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam9, "c");
                }

                //Manager Mail
                let managerMailParam9 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam9, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam9 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam9, "e");
                }
              } else if (request_type === 28) {

                if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
                  var rdfrc_date_updt = '';
                } else {
                  var rdfrc_date = new Date(task_details[0].rdfrc);
                  var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
                }
                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam91 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    rdfrc_date_updt: rdfrc_date_updt,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam91, "c");
                }

                //Manager Mail
                let managerMailParam91 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  rdfrc_date_updt: rdfrc_date_updt,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam91, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam91 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    rdfrc_date_updt: rdfrc_date_updt,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam91, "e");
                }
              } else if (request_type === 29) {

                if (mail_arr.length > 0) {
                  //Customer Mail                
                  let mailParam10 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    notification_number: task_details[0].notification_number,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }
                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam10, "c");
                }

                //Manager Mail
                let managerMailParam10 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  notification_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].notification_number_translate : task_details[0].notification_number,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam10, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam10 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    notification_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].notification_number_translate : task_details[0].notification_number,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam10, "e");
                }
              } else if (request_type === 38) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  var document_type_by_lang = task_details[0].apos_document_type;
                  if (task_details[0].language != Config.default_language) {
                    let t_apos_document_type = await taskModel.get_translated_apos_document_type(task_details[0].apos_document_type, task_details[0].language);
                    document_type_by_lang = t_apos_document_type;
                  }
                  let mailParam11 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    document_type: document_type_by_lang,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam11, "c");
                }

                //Manager Mail
                let managerMailParam11 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  document_type: task_details[0].apos_document_type,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', manager[0].email, Config.webmasterMail, '', managerMailParam11, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam11 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    document_type: task_details[0].apos_document_type,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', emp_details[0].email, Config.webmasterMail, '', empMailParam11, "e");
                }
              } else if (request_type === 39) {
                if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
                  var rdfrc_date_updt = '';
                } else {

                  var rdfrc_date = new Date(task_details[0].rdfrc);
                  var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
                }
                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam12 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    dmf_number: task_details[0].dmf_number,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy')
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam12, "c");
                }

                //Manager Mail
                let managerMailParam12 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', manager[0].email, Config.webmasterMail, '', managerMailParam12, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam12 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    dmf_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].dmf_number_translate : task_details[0].dmf_number,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', emp_details[0].email, Config.webmasterMail, '', empMailParam12, "e");
                }
              } else if (request_type === 40) {
                if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
                  var rdfrc_date_updt = '';
                } else {

                  var rdfrc_date = new Date(task_details[0].rdfrc);
                  var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
                }

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam13 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    gmp_clearance_id: task_details[0].gmp_clearance_id,
                    tga_email_id: task_details[0].tga_email_id,
                    applicant_name: task_details[0].applicant_name,
                    doc_required: task_details[0].doc_required
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }
                  console.log("SENDING TASK MAIL 40");
                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam13, "c");
                }

                //Manager Mail
                let managerMailParam13 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                  gmp_clearance_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].gmp_clearance_id_translate : task_details[0].gmp_clearance_id,
                  tga_email_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].tga_email_id_translate : task_details[0].tga_email_id,
                  applicant_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].applicant_name_translate : task_details[0].applicant_name,
                  doc_required: (entities.decode(task_details[0].language) != 'en') ? task_details[0].doc_required_translate : task_details[0].doc_required,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', manager[0].email, Config.webmasterMail, '', managerMailParam13, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam13 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                    gmp_clearance_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].gmp_clearance_id_translate : task_details[0].gmp_clearance_id,
                    tga_email_id: (entities.decode(task_details[0].language) != 'en') ? task_details[0].tga_email_id_translate : task_details[0].tga_email_id,
                    applicant_name: (entities.decode(task_details[0].language) != 'en') ? task_details[0].applicant_name_translate : task_details[0].applicant_name,
                    doc_required: (entities.decode(task_details[0].language) != 'en') ? task_details[0].doc_required_translate : task_details[0].doc_required,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', emp_details[0].email, Config.webmasterMail, '', empMailParam13, "e");
                }
              } else if (request_type === 44) {

                if (mail_arr.length > 0) {
                  //Customer Mail
                  let mailParam14 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: product_name_by_lang,
                    country_name: country_name_by_lang,
                    task_content: entities.decode(task_details[0].content),
                    url_status: url_status,
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: req_name_by_lang,
                    quantity: quantity_by_lang,
                    pharmacopoeia: task_details[0].pharmacopoeia,
                  };
                  var mailcc = '';
                  if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                    mailcc = mail_arr[1].cc_arr;
                  }

                  const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam14, "c");
                }

                //Manager Mail
                let managerMailParam14 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  quantity: task_details[0].quantity,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: manager[0].first_name,
                  pharmacopoeia: task_details[0].pharmacopoeia,
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', manager[0].email, Config.webmasterMail, '', managerMailParam14, "e");

                if (emp_details.length > 0) {
                  //Employee Mail
                  var empMailParam14 = {
                    first_name: entities.decode(customer_details[0].first_name),
                    last_name: entities.decode(customer_details[0].last_name),
                    task_ref: task_details[0].task_ref,
                    product_name: task_details[0].product_name,
                    country_name: country_name,
                    format_due_date: format_due_date,
                    task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                    company_name: entities.decode(customer_details[0].company_name),
                    req_name: task_details[0].req_name,
                    quantity: task_details[0].quantity,
                    ccpurl: Config.ccpUrl,
                    emp_first_name: emp_details[0].first_name,
                    pharmacopoeia: task_details[0].pharmacopoeia,
                  };
                  const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', emp_details[0].email, Config.webmasterMail, '', empMailParam14, "e");
                }
              }



              // const mailOptions = {
              //   from: Config.webmasterMail, // sender address
              //   to: manager[0].email,// manager[0].email, // list of receivers
              //   subject: mail_subject, // Subject line
              //   html: `<!DOCTYPE html>
              //   <html>
              //      <body>
              //         <p><em>New Task Assigned</em></p>
              //         <p>Hello ${manager[0].first_name},</p>
              //         <p>You have a new task assigned.</p>
              //         <p>Task Details are as below,</p>
              //         <table>
              //             <tr>
              //               <td>Task Ref</td>
              //               <td>${task_details[0].task_ref}</td>
              //             </tr>
              //             <tr>
              //               <td>User Name</td>
              //               <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //             </tr>
              //             <tr>
              //               <td>Request Type</td>
              //               <td>${task_details[0].req_name}</td>
              //             </tr>
              //             <tr>
              //               <td>Product</td>
              //               <td>${task_details[0].product_name}</td>
              //             </tr>
              //             <tr>
              //               <td>Market</td>
              //               <td>${country_name}</td>
              //             </tr>
              //             <tr>
              //               <td>Due Date</td>
              //               <td>${format_due_date}</td>
              //             </tr>
              //             ${pharmacopoeia}
              //             ${polymorphic_form}
              //             ${stability_data_type}
              //             ${audit_visit_site_name}
              //             ${request_audit_visit_date}
              //             ${quantity}
              //             ${rdd}
              //             ${batch_number}
              //             ${nature_of_issue}
              //             ${samples}
              //             ${working_standards}
              //             ${impurities}
              //             ${shipping_address}
              //             ${dmf_number}
              //             ${apos_document_type}
              //             ${notification_number}
              //             ${rdfrc}
              //             <tr>
              //               <td>Requirement</td>
              //               <td>${entities.decode(task_details[0].content)}</td>
              //             </tr>
              //             <tr>
              //               <td>&nbsp;</td>
              //               <td></td>
              //             </tr>
              //             <tr>
              //               <td>&nbsp;</td>
              //               <td></td>
              //             </tr>
              //         </table>
              //         <p>
              //           Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
              //         </p>   
              //      </body>
              //   </html>`
              // };
              // common.sendMail(mailOptions);

              // if (emp_details.length > 0) {
              //   const mailOptions2 = {
              //     from: Config.webmasterMail, // sender address
              //     to: emp_details[0].email, //emp_details[0].email, // list of receivers
              //     subject: mail_subject, // Subject line
              //     html: `<!DOCTYPE html>
              //     <html>
              //        <body>
              //           <p><em>New Task Assigned</em></p>
              //           <p>Hello ${emp_details[0].first_name},</p>
              //           <p>You have a new task assigned.</p>
              //           <p>Task Details are as below,</p>
              //           <table>
              //               <tr>
              //                 <td>Task Ref</td>
              //                 <td>${task_details[0].task_ref}</td>
              //               </tr>
              //               <tr>
              //                 <td>User Name</td>
              //                 <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //               </tr>
              //               <tr>
              //                 <td>Request Type</td>
              //                 <td>${task_details[0].req_name}</td>
              //               </tr>
              //               <tr>
              //                 <td>Product</td>
              //                 <td>${task_details[0].product_name}</td>
              //               </tr>
              //               <tr>
              //                 <td>Market</td>
              //                 <td>${country_name}</td>
              //               </tr>
              //               <tr>
              //                 <td>Due Date</td>
              //                 <td>${format_due_date}</td>
              //               </tr>
              //               ${pharmacopoeia}
              //               ${polymorphic_form}
              //               ${stability_data_type}
              //               ${audit_visit_site_name}
              //               ${request_audit_visit_date}
              //               ${quantity}
              //               ${rdd}
              //               ${batch_number}
              //               ${nature_of_issue}
              //               ${samples}
              //               ${working_standards}
              //               ${impurities}
              //               ${shipping_address}
              //               ${dmf_number}
              //               ${apos_document_type}
              //               ${notification_number}
              //               ${rdfrc}
              //               <tr>
              //                 <td valign="top">Requirement</td>
              //                 <td valign="top">${entities.decode(task_details[0].content)}</td>
              //               </tr>
              //               <tr>
              //                 <td>&nbsp;</td>
              //                 <td></td>
              //               </tr>
              //               <tr>
              //                 <td>&nbsp;</td>
              //                 <td></td>
              //               </tr>
              //           </table>
              //           <p>
              //             Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
              //           </p>   
              //        </body>
              //     </html>`
              //   };
              //   common.sendMail(mailOptions2);
              // }

              //SEND MAIL

            } else {
              //Unallocated Task Soumyadeep
              var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
              common.logError(err);
            }
            //}

            var files = req.files;
            if (files && files.length > 0) {

              for (var j = 0; j < files.length; j++) {

                let file_details = {
                  task_id: task_id,
                  actual_file_name: files[j].originalname,
                  new_file_name: files[j].filename,
                  date_added: today
                }

                await taskModel.add_task_file(file_details);
              }
            }

            //SALES FORCE TASK
            if (Config.activate_sales_force == true && req.body.request_id != 23) {
              await module.exports.__post_sales_force_add_task(task_id, files, req.user.empe);
              if (comment_arr.length > 0) {
                for (let index = 0; index < comment_arr.length; index++) {
                  const element = comment_arr[index];
                  await module.exports.__post_sales_force_internal_discussions(task_id, [], element.comment, element.posted_by);
                }
              }
            }

            total++;
            
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      }

      if(total == product_arr.length){
        res.status(200).json({
          status: '1'
        }).end();
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Add Complain
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Add Complain used for Complain selection
   */
  add_complain: async (req, res, next) => {
    var today = common.currentDateTime();
    try {
      let taskObj = {};
      var request_id = req.body.request_id;

      if (req.user.role == 2) {
        var submitted_by = req.user.customer_id;
        var customer_id = req.body.agent_customer_id;
      } else {
        var submitted_by = 0;
        var customer_id = req.user.customer_id;
      }

      var sla = await taskModel.get_total_sla(request_id);
      var days = await common.dayCountExcludingWeekends(sla.total_sla);
      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
      var reference_key = 'C';
      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);

        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }


      const {
        product_id,
        country_id,
        description,
        request_type_id,
        nature_of_issue,
        batch_number,
        quantity,
        cc_customers,
        share_with_agent
      } = req.body;

      let product_arr = JSON.parse(product_id);

      let total = 0;

      for (let index = 0; index < product_arr.length; index++) {
        const elem_product_id = product_arr[index].value;

        let language = '';
        if (req.user.empe > 0) {
          language = await customerModel.get_customer_language(customer_id);
        } else {
          language = await customerModel.get_customer_language(customer_id);
        }

        taskObj = {
          customer_id: customer_id,
          nature_of_issue: entities.encode(nature_of_issue),
          product_id: elem_product_id,
          country_id: 0,
          parent_id: 0,
          due_date: sla_due_date,
          batch_number: entities.encode(batch_number),
          quantity: entities.encode(quantity),
          current_date: today,
          discussion: 0,
          description: entities.encode(description),
          request_type_id: req.body.request_id,

          submitted_by: submitted_by,
          close_status: 0,
          drupal_task_id: 0,
          priority: 2,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          cc_customers: cc_customers,
          share_with_agent: share_with_agent,
          language: language
        }
        taskObj.ccp_posted_by = (req.user.empe > 0) ? +req.user.empe : 0;
        await taskModel.add_task(taskObj)
          .then(async function (data) {
            let task_id = data.task_id;

            if (language != Config.default_language) {
              await module.exports.__translate_language(taskObj, task_id, country_id);
            }

            if (country_id && country_id != '') {
              var parsed_country_id = JSON.parse(country_id);
              for (let index = 0; index < parsed_country_id.length; index++) {
                await taskModel.map_countries(task_id, parsed_country_id[index].value);
              }
            }

            let manager = await taskModel.get_allocated_manager(taskObj.customer_id);

            // return res.status(400)
            // .json({
            //   status  : 5,
            //   message : manager
            // });
            let ref_no = 'PHL-' + reference_key + '-' + task_id;
            let customerData = [];
            await taskModel.update_ref_no(ref_no, task_id)
              .then(async () => {
                // ACTIVITY LOG - SATYAJIT
                customerData = await customerModel.get_user(taskObj.customer_id);
                var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
                var params = {
                  task_ref: ref_no,
                  request_type: request_type_id,
                  customer_name: customername
                }

                if (req.user.role == 2) {
                  var agentData = await agentModel.get_user(submitted_by);
                  params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                  await taskModel.log_activity(2, task_id, params)
                } else {
                  await taskModel.log_activity(1, task_id, params)
                }
                // END LOG
              });


            // let spoc_exists = false;
            // let spoc = [];
            // if (language != Config.default_language && customerData[0].ignore_spoc == 2) {
            //   let assigned_spoc = await taskModel.get_allocated_spoc(taskObj.customer_id, language);
            //   assigned_spoc = await module.exports.__get_allocated_leave_employee_spoc(assigned_spoc, taskObj.customer_id);
            //   if (assigned_spoc.length > 0) {
            //     spoc_exists = true;
            //     spoc = assigned_spoc;
            //   } else if (await taskModel.check_language_spoc(language)) {
            //     spoc_exists = true;
            //     global_spoc = await taskModel.get_global_spoc(language);
            //     spoc = await module.exports.__get_allocated_leave_employee_spoc(global_spoc, taskObj.customer_id);
            //   }
            // }

            let comment_arr = [];

            // if (spoc_exists && spoc.length > 0) {

            //   const assignment_spoc = {
            //     task_id: task_id,
            //     assigned_to: spoc[0].employee_id,
            //     assigned_by: -1,
            //     assign_date: today,
            //     due_date: sla_due_date,
            //     reopen_date: today,
            //     parent_assignment_id: 0
            //   }

            //   var spoc_parent_assign_id = await taskModel.assign_task(assignment_spoc).catch(err => {
            //     common.logError(err);
            //     res.status(400)
            //       .json({
            //         status: 3,
            //         message: Config.errorText.value
            //       }).end();
            //   });

            //   // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //   var spoc_recipient = `${spoc[0].first_name} ${spoc[0].last_name}`;
            //   var params = {
            //     task_ref: ref_no,
            //     request_type: request_type_id,
            //     employee_sender: 'SYSTEM',
            //     employee_recipient: spoc_recipient + ` (${(spoc[0].desig_name)})`
            //   }
            //   await taskModel.log_activity(8, task_id, params);
            //   // END LOG

            //   let employee_notification = 22;
            //   let param_notification = {};
            //   let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //   let cc_arr = [
            //     employee_notification,
            //     task_id,
            //     spoc[0].employee_id,
            //     common.currentDateTime(),
            //     0,
            //     notification_text,
            //     employee_notification,
            //     mail_subject
            //   ]
            //   await taskModel.notify_employee(cc_arr);

            //   manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

            //   if (manager.length > 0) {
            //     let assignment_val = 2;
            //     await taskModel.update_task_assignment(task_id, spoc[0].employee_id, assignment_val);

            //     let task_details = await taskModel.tasks_details(task_id);

            //     var customer_details = await customerModel.get_user(task_details[0].customer_id);

            //     var mail_subject = '';
            //     if (task_details[0].request_type == 24) {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
            //     } else {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
            //     }

            //     await taskModel.update_task_owner(manager[0].employee_id, task_id);


            //     const assignment = {
            //       task_id: task_id,
            //       assigned_to: manager[0].employee_id,
            //       assigned_by: spoc[0].employee_id,
            //       assign_date: today,
            //       due_date: sla_due_date,
            //       reopen_date: today,
            //       parent_assignment_id: spoc_parent_assign_id.assignment_id
            //     }
            //     let parent_assign_id = await taskModel.assign_task(assignment);


            //     // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //     var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
            //     var params = {
            //       task_ref: ref_no,
            //       request_type: request_type_id,
            //       employee_sender: spoc_recipient + ` (${(spoc[0].desig_name)})`,
            //       employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
            //     }
            //     await taskModel.log_activity(8, task_id, params);

            //     let employee_notification = 8;
            //     let param_notification = {
            //       employee_name: `${spoc[0].first_name} (${(spoc[0].desig_name)})`,
            //       assigned_employee: `you`
            //     };
            //     let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //     let cc_arr = [
            //       employee_notification,
            //       task_id,
            //       manager[0].employee_id,
            //       common.currentDateTime(),
            //       0,
            //       notification_text,
            //       employee_notification,
            //       mail_subject
            //     ]
            //     await taskModel.notify_employee(cc_arr);
            //     // END LOG

            //     let task_comment = {
            //       posted_by: spoc[0].employee_id,
            //       task_id: task_id,
            //       comment: `This task has been auto assigned to ${manager[0].first_name} ${manager[0].last_name} (${(manager[0].desig_name)}) by the system on my behalf.`,
            //       assignment_id: spoc_parent_assign_id.assignment_id,
            //       post_date: today
            //     };

            //     await taskModel.insert_comment_log(task_comment);

            //     comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

            //     //CSC TECHNICAL
            //     if (request_id === 7) {
            //       var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2);
            //     }

            //     //CSC COMMERCIAL
            //     if (request_id === 34) {
            //       var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);
            //     }

            //     var bot_assignment_id = 0;

            //     emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

            //     if (emp_details.length > 0) {
            //       let assignment_val = 2;
            //       await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
            //         .then(async function (data) {

            //           const assign_tasks = {
            //             assigned_by: manager[0].employee_id,
            //             assigned_to: emp_details[0].employee_id,
            //             task_id: task_id,
            //             due_date: sla_due_date,
            //             assign_date: today,
            //             parent_assignment_id: parent_assign_id.assignment_id,
            //             comment: '',
            //             reopen_date: today
            //           }

            //           await taskModel.assign_task(assign_tasks)
            //             .then(async function (tdata) {

            //               // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
            //               var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
            //               var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
            //               var params = {
            //                 task_ref: ref_no,
            //                 request_type: request_type_id,
            //                 employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
            //                 employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
            //               }
            //               await taskModel.log_activity(8, task_id, params)
            //               // END LOG

            //               bot_assignment_id = tdata.assignment_id;

            //               let employee_notification = 8;
            //               let param_notification = {
            //                 employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
            //                 assigned_employee: `you`
            //               };
            //               let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //               let cc_arr = [
            //                 employee_notification,
            //                 task_id,
            //                 emp_details[0].employee_id,
            //                 common.currentDateTime(),
            //                 0,
            //                 notification_text,
            //                 employee_notification,
            //                 mail_subject
            //               ]
            //               await taskModel.notify_employee(cc_arr);

            //               const task_comment = {
            //                 posted_by: manager[0].employee_id,
            //                 task_id: task_id,
            //                 comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
            //                 assignment_id: bot_assignment_id,
            //                 post_date: today
            //               };

            //               await taskModel.insert_comment_log(task_comment);

            //               comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

            //               //console.log('child assignment',bot_assignment_id);

            //             }).catch(err => {
            //               common.logError(err);
            //               res.status(400)
            //                 .json({
            //                   status: 3,
            //                   message: Config.errorText.value
            //                 }).end();
            //             });

            //         }).catch(err => {
            //           common.logError(err);
            //           res.status(400)
            //             .json({
            //               status: 3,
            //               message: Config.errorText.value
            //             }).end();
            //         })
            //     } else {

            //       bot_assignment_id = parent_assign_id.assignment_id;
            //     }

            //     //CURRENT OWNER
            //     if (emp_details.length > 0) {
            //       await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
            //     } else {
            //       await taskModel.update_current_owner(task_id, manager[0].employee_id);
            //     }
            //     //CURRENT OWNER

            //     //Xceed bot -- satyajit
            //     // if( (Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0 ){
            //     //   //console.log('bot_assignment_id',bot_assignment_id);
            //     //   //console.log('url',`${Config.drlBot.assign_url}${bot_assignment_id}`);
            //     //   const request = require('request');
            //     //   var options = {
            //     //     method: 'POST',
            //     //     url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
            //     //     headers:
            //     //     {'x-api-key': 'XAmdVeqheisEoJ1SROQG'}
            //     //   };

            //     //   await request(options, async function (error, response, body) {
            //     //     if (error){
            //     //         var mailOptions = {
            //     //               from: Config.webmasterMail,
            //     //               to: ['sayan.mazumder@indusnet.co.in'],
            //     //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //     //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //     //             };
            //     //         await common.sendMail(mailOptions);
            //     //         await common.logErrorText('Bot entry failed create_task',JSON.stringify(error));

            //     //     } else {                 
            //     //         if(response.statusCode != 200){                 
            //     //             var mailOptions = {
            //     //               from: Config.webmasterMail,
            //     //               to: ['sayan.mazumder@indusnet.co.in'],
            //     //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //     //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //     //             };
            //     //             await common.sendMail(mailOptions);
            //     //             await common.logErrorText('Bot entry failed create_task',JSON.stringify(response));
            //     //         }
            //     //     }
            //     //   });
            //     // }
            //     //end 

            //     //INSERT CC CUSTOMERS
            //     if (taskObj.cc_customers) {
            //       let cc_cust_arr = JSON.parse(taskObj.cc_customers);
            //       //INSERT CC CUSTOMERS
            //       if (cc_cust_arr.length > 0) {
            //         for (let index = 0; index < cc_cust_arr.length; index++) {
            //           const element = cc_cust_arr[index].customer_id;
            //           let insrt_cc = {
            //             customer_id: element,
            //             task_id: task_id,
            //             status: 1
            //           };
            //           await customerModel.insert_cc_customer(insrt_cc);
            //         }
            //       }
            //     }

            //     //HIGHLIGHT NEW TASK
            //     let customer_id = taskObj.customer_id;

            //     let highlight_arr = [
            //       task_id,
            //       customer_id,
            //       1,
            //       'C'
            //     ];
            //     await taskModel.task_highlight(highlight_arr);

            //     let cc_list = await taskModel.task_cc_customer(task_id);
            //     if (cc_list && cc_list.length > 0) {
            //       for (let index = 0; index < cc_list.length; index++) {
            //         const element = cc_list[index];
            //         let highlight_cc_arr = [
            //           task_id,
            //           element.customer_id,
            //           1,
            //           'C'
            //         ];
            //         await taskModel.task_highlight(highlight_cc_arr);
            //       }
            //     }

            //     if (taskObj.share_with_agent == 1) {
            //       let customer_details = await customerModel.get_user(taskObj.customer_id);
            //       let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            //       if (agent_list && agent_list.length > 0) {
            //         for (let index = 0; index < agent_list.length; index++) {
            //           const element = agent_list[index];
            //           let highlight_agent_arr = [
            //             task_id,
            //             element.agent_id,
            //             1,
            //             'A'
            //           ];
            //           await taskModel.task_highlight(highlight_agent_arr);
            //         }
            //       }
            //     }

            //     //SEND MAIL

            //     var quantity, batch_number, nature_of_issue = '';

            //     quantity = '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
            //     nature_of_issue = '<tr><td>Nature Of Issue</td><td>' + task_details[0].nature_of_issue + '</td></tr>';
            //     batch_number = '<tr><td>Batch No</td><td>' + task_details[0].batch_number + '</td></tr>';

            //     var country = await taskModel.tasks_details_countries(task_id);

            //     if (country && country[0].country_name != '') {
            //       var country_name = country[0].country_name;
            //     } else {
            //       var country_name = '';
            //     }

            //     var country_name_by_lang = country_name;
            //     if (language != Config.default_language) {
            //       //COUNTRY
            //       let t_country = await taskModel.get_translated_country(task_id, language);
            //       country_name_by_lang = t_country[0].country_name;
            //     }

            //     var db_due_date = new Date(task_details[0].due_date);
            //     var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

            //     let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

            //     var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

            //     if (mail_arr.length > 0) {
            //       // var cust_status_mail = `<!DOCTYPE html>
            //       // <html>
            //       //   <body>
            //       //     <p><em>Thank you for your request</em></p>
            //       //     <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
            //       //     <p></p>
            //       //     <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
            //       //     <p>We have received your request with the following details:</p>
            //       //     <table>
            //       //       <tr>
            //       //         <td>Task Ref</td>
            //       //         <td>${task_details[0].task_ref}</td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>User Name</td>
            //       //         <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>Request Type</td>
            //       //         <td>${task_details[0].req_name}</td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>Product</td>
            //       //         <td>${task_details[0].product_name}</td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>Market</td>
            //       //         <td>${country_name}</td>
            //       //       </tr>
            //       //         ${quantity}
            //       //         ${batch_number}
            //       //         ${nature_of_issue}
            //       //       <tr>
            //       //         <td valign="top">Requirement</td>
            //       //         <td valign="top">${entities.decode(task_details[0].content)}</td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>&nbsp;</td>
            //       //         <td></td>
            //       //       </tr>
            //       //       <tr>
            //       //         <td>&nbsp;</td>
            //       //         <td></td>
            //       //       </tr>
            //       //     </table>
            //       //     <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
            //       //     <p>-- Dr.Reddys API team</p>
            //       //   </body>
            //       // </html>`;

            //       // //console.log(cust_status_mail);
            //       // var mail_options1 = {
            //       //   from: Config.contactUsMail,
            //       //   to: mail_arr[0].to_arr,
            //       //   subject: mail_subject,
            //       //   html: cust_status_mail
            //       // };
            //       // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //       //   mail_options1.cc = mail_arr[1].cc_arr;
            //       // }
            //       // //console.log(mail_options1);
            //       // common.sendMailB2C(mail_options1);

            //       var req_name_by_lang = task_details[0].req_name;
            //       var product_name_by_lang = task_details[0].product_name;
            //       switch (entities.decode(task_details[0].language)) {
            //         case 'es':
            //           req_name_by_lang = task_details[0].req_name_es;
            //           product_name_by_lang = task_details[0].product_name_es;
            //           break;
            //         case 'pt':
            //           req_name_by_lang = task_details[0].req_name_pt;
            //           product_name_by_lang = task_details[0].product_name_pt;
            //           break;
            //         case 'ja':
            //           req_name_by_lang = task_details[0].req_name_ja;
            //           product_name_by_lang = task_details[0].product_name_ja;
            //           break;
            //         case 'zh':
            //           req_name_by_lang = task_details[0].req_name_zh;
            //           product_name_by_lang = task_details[0].product_name_zh;
            //           break;
            //         default:
            //           req_name_by_lang = task_details[0].req_name;
            //           product_name_by_lang = task_details[0].product_name;
            //           break;
            //       }


            //       var quantity_by_lang = task_details[0].quantity;
            //       if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
            //         let q_arr = task_details[0].quantity.split(' ');
            //         let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //         quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            //       }

            //       let mailParam = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: product_name_by_lang,
            //         country_name: country_name_by_lang,
            //         task_content: entities.decode(task_details[0].content),
            //         url_status: url_status,
            //         company_name: entities.decode(customer_details[0].company_name),
            //         nature_of_issue: task_details[0].nature_of_issue,
            //         quantity: quantity_by_lang,
            //         batch_number: task_details[0].batch_number,
            //         req_name: req_name_by_lang
            //       };

            //       var mailcc = '';
            //       if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //         mailcc = mail_arr[1].cc_arr;
            //       }
            //       //console.log("Complain mail");
            //       const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");
            //     }



            //     //SPOC Mail
            //     let spocMailParam = {
            //       first_name: entities.decode(customer_details[0].first_name),
            //       last_name: entities.decode(customer_details[0].last_name),
            //       task_ref: task_details[0].task_ref,
            //       product_name: task_details[0].product_name,
            //       country_name: country_name,
            //       format_due_date: format_due_date,
            //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //       company_name: entities.decode(customer_details[0].company_name),
            //       nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //       quantity: task_details[0].quantity,
            //       batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //       ccpurl: Config.ccpUrl,
            //       emp_first_name: spoc[0].first_name,
            //       req_name: task_details[0].req_name
            //     };

            //     const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', spoc[0].email, Config.webmasterMail, '', spocMailParam, "e");

            //     //Manager Mail
            //     let managerMailParam = {
            //       first_name: entities.decode(customer_details[0].first_name),
            //       last_name: entities.decode(customer_details[0].last_name),
            //       task_ref: task_details[0].task_ref,
            //       product_name: task_details[0].product_name,
            //       country_name: country_name,
            //       format_due_date: format_due_date,
            //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //       company_name: entities.decode(customer_details[0].company_name),
            //       nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //       quantity: task_details[0].quantity,
            //       batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //       ccpurl: Config.ccpUrl,
            //       emp_first_name: manager[0].first_name,
            //       req_name: task_details[0].req_name
            //     };

            //     const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

            //     if (emp_details.length > 0) {
            //       //Employee Mail
            //       var empMailParam = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
            //         quantity: task_details[0].quantity,
            //         batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
            //         ccpurl: Config.ccpUrl,
            //         emp_first_name: emp_details[0].first_name,
            //         req_name: task_details[0].req_name
            //       };

            //       const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
            //     }

            //     // const mailOptions = {
            //     //   from: Config.webmasterMail, // sender address
            //     //   to: manager[0].email,// manager[0].email, // list of receivers
            //     //   subject: mail_subject,
            //     //   html: `<!DOCTYPE html>
            //     //   <html>
            //     //   <body>
            //     //     <p><em>New Task Assigned</em></p>
            //     //     <p>Hello ${manager[0].first_name},</p>
            //     //     <p>You have a new task assigned.</p>
            //     //     <p>Task Details are as below,</p>
            //     //     <table>
            //     //       <tr>
            //     //         <td>Task Ref</td>
            //     //         <td>${task_details[0].task_ref}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>User Name</td>
            //     //         <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>Request Type</td>
            //     //         <td>${task_details[0].req_name}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>Product</td>
            //     //         <td>${task_details[0].product_name}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>Market</td>
            //     //         <td>${country_name}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>Due Date</td>
            //     //         <td>${format_due_date}</td>
            //     //       </tr>
            //     //         ${quantity}
            //     //         ${batch_number}
            //     //         ${nature_of_issue}
            //     //       <tr>
            //     //         <td valign="top">Requirement</td>
            //     //         <td valign="top">${entities.decode(task_details[0].content)}</td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>&nbsp;</td>
            //     //         <td></td>
            //     //       </tr>
            //     //       <tr>
            //     //         <td>&nbsp;</td>
            //     //         <td></td>
            //     //       </tr>
            //     //     </table>
            //     //     <p>
            //     //       Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
            //     //     </p>
            //     //   </body>
            //     //   </html>`
            //     // };
            //     // common.sendMail(mailOptions);

            //     // if (emp_details.length > 0) {
            //     //   const mailOptions2 = {
            //     //     from: Config.webmasterMail, // sender address
            //     //     to: emp_details[0].email, //emp_details[0].email, // list of receivers
            //     //     subject: mail_subject,
            //     //     html:`<!DOCTYPE html>
            //     //     <html>
            //     //       <body>
            //     //         <p><em>New Task Assigned</em></p>
            //     //         <p>Hello ${emp_details[0].first_name},</p>
            //     //         <p>You have a new task assigned.</p>
            //     //         <p>Task Details are as below,</p>
            //     //         <table>
            //     //           <tr>
            //     //             <td>Task Ref</td>
            //     //             <td>${task_details[0].task_ref}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>User Name</td>
            //     //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Request Type</td>
            //     //             <td>${task_details[0].req_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Product</td>
            //     //             <td>${task_details[0].product_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Market</td>
            //     //             <td>${country_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Due Date</td>
            //     //             <td>${format_due_date}</td>
            //     //           </tr>
            //     //             ${quantity}
            //     //             ${batch_number}
            //     //             ${nature_of_issue}
            //     //           <tr>
            //     //             <td valign="top">Requirement</td>
            //     //             <td valign="top">${entities.decode(task_details[0].content)}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //           </tr>
            //     //         </table>
            //     //         <p>
            //     //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
            //     //         </p>
            //     //       </body>
            //     //     </html>`
            //     //   };
            //     //   common.sendMail(mailOptions2);
            //     // }

            //     //SEND MAIL

            //   } else {
            //     //Unallocated Task Soumyadeep
            //     var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
            //     common.logError(err);
            //   }

            // } else {

            manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

            if (manager.length > 0) {

              let task_details = await taskModel.tasks_details(task_id);

              var customer_details = await customerModel.get_user(task_details[0].customer_id);

              var mail_subject = '';
              if (task_details[0].request_type == 24) {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
              } else {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
              }

              await taskModel.update_task_owner(manager[0].employee_id, task_id);


              const assignment = {
                task_id: task_id,
                assigned_to: manager[0].employee_id,
                assigned_by: -1,
                assign_date: today,
                due_date: sla_due_date,
                reopen_date: today,
                parent_assignment_id: 0
              }
              let parent_assign_id = await taskModel.assign_task(assignment);


              // ACTIVITY LOG FROM SYSTEM - SATYAJIT
              var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
              var params = {
                task_ref: ref_no,
                request_type: request_type_id,
                employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`,
                employee_sender: 'SYSTEM'
              }
              await taskModel.log_activity(8, task_id, params);

              let employee_notification = 22;
              let param_notification = {};
              let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

              let cc_arr = [
                employee_notification,
                task_id,
                manager[0].employee_id,
                common.currentDateTime(),
                0,
                notification_text,
                employee_notification,
                mail_subject
              ]
              await taskModel.notify_employee(cc_arr);
              // END LOG

              //CSC TECHNICAL
              if (request_id === 7) {
                var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2);
              }

              //CSC COMMERCIAL
              if (request_id === 34) {
                var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);
              }

              var bot_assignment_id = 0;

              emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

              if (emp_details.length > 0) {
                let assignment_val = 2;
                await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                  .then(async function (data) {

                    const assign_tasks = {
                      assigned_by: manager[0].employee_id,
                      assigned_to: emp_details[0].employee_id,
                      task_id: task_id,
                      due_date: sla_due_date,
                      assign_date: today,
                      parent_assignment_id: parent_assign_id.assignment_id,
                      comment: '',
                      reopen_date: today
                    }

                    await taskModel.assign_task(assign_tasks)
                      .then(async function (tdata) {

                        // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                        var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                        var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                        var params = {
                          task_ref: ref_no,
                          request_type: request_type_id,
                          employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                          employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                        }
                        await taskModel.log_activity(8, task_id, params)
                        // END LOG

                        bot_assignment_id = tdata.assignment_id;

                        let employee_notification = 8;
                        let param_notification = {
                          employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                          assigned_employee: `you`
                        };
                        let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                        let cc_arr = [
                          employee_notification,
                          task_id,
                          emp_details[0].employee_id,
                          common.currentDateTime(),
                          0,
                          notification_text,
                          employee_notification,
                          mail_subject
                        ]
                        await taskModel.notify_employee(cc_arr);

                        const task_comment = {
                          posted_by: manager[0].employee_id,
                          task_id: task_id,
                          comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
                          assignment_id: bot_assignment_id,
                          post_date: today
                        };

                        await taskModel.insert_comment_log(task_comment);

                        comment_arr.push({ comment: task_comment.comment, posted_by: task_comment.posted_by });

                        //console.log('child assignment',bot_assignment_id);

                      }).catch(err => {
                        common.logError(err);
                        // res.status(400)
                        //   .json({
                        //     status: 3,
                        //     message: Config.errorText.value
                        //   }).end();
                      });

                  }).catch(err => {
                    common.logError(err);
                    // res.status(400)
                    //   .json({
                    //     status: 3,
                    //     message: Config.errorText.value
                    //   }).end();
                  })
              } else {

                bot_assignment_id = parent_assign_id.assignment_id;
              }

              //CURRENT OWNER
              if (emp_details.length > 0) {
                await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
              } else {
                await taskModel.update_current_owner(task_id, manager[0].employee_id);
              }
              //CURRENT OWNER

              //Xceed bot -- satyajit
              // if( (Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0 ){
              //   //console.log('bot_assignment_id',bot_assignment_id);
              //   //console.log('url',`${Config.drlBot.assign_url}${bot_assignment_id}`);
              //   const request = require('request');
              //   var options = {
              //     method: 'POST',
              //     url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
              //     headers:
              //     {'x-api-key': 'XAmdVeqheisEoJ1SROQG'}
              //   };

              //   await request(options, async function (error, response, body) {
              //     if (error){
              //         var mailOptions = {
              //               from: Config.webmasterMail,
              //               to: ['sayan.mazumder@indusnet.co.in'],
              //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
              //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
              //             };
              //         await common.sendMail(mailOptions);
              //         await common.logErrorText('Bot entry failed create_task',JSON.stringify(error));

              //     } else {                 
              //         if(response.statusCode != 200){                 
              //             var mailOptions = {
              //               from: Config.webmasterMail,
              //               to: ['sayan.mazumder@indusnet.co.in'],
              //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
              //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
              //             };
              //             await common.sendMail(mailOptions);
              //             await common.logErrorText('Bot entry failed create_task',JSON.stringify(response));
              //         }
              //     }
              //   });
              // }
              //end 

              //INSERT CC CUSTOMERS
              if (taskObj.cc_customers) {
                let cc_cust_arr = JSON.parse(taskObj.cc_customers);
                //INSERT CC CUSTOMERS
                if (cc_cust_arr.length > 0) {
                  for (let index = 0; index < cc_cust_arr.length; index++) {
                    const element = cc_cust_arr[index].customer_id;
                    let insrt_cc = {
                      customer_id: element,
                      task_id: task_id,
                      status: 1
                    };
                    await customerModel.insert_cc_customer(insrt_cc);
                  }
                }
              }

              //HIGHLIGHT NEW TASK
              let customer_id = taskObj.customer_id;

              let highlight_arr = [
                task_id,
                customer_id,
                1,
                'C'
              ];
              await taskModel.task_highlight(highlight_arr);

              let cc_list = await taskModel.task_cc_customer(task_id);
              if (cc_list && cc_list.length > 0) {
                for (let index = 0; index < cc_list.length; index++) {
                  const element = cc_list[index];
                  let highlight_cc_arr = [
                    task_id,
                    element.customer_id,
                    1,
                    'C'
                  ];
                  await taskModel.task_highlight(highlight_cc_arr);
                }
              }

              if (taskObj.share_with_agent == 1) {
                let customer_details = await customerModel.get_user(taskObj.customer_id);
                let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

                if (agent_list && agent_list.length > 0) {
                  for (let index = 0; index < agent_list.length; index++) {
                    const element = agent_list[index];
                    let highlight_agent_arr = [
                      task_id,
                      element.agent_id,
                      1,
                      'A'
                    ];
                    await taskModel.task_highlight(highlight_agent_arr);
                  }
                }
              }

              //SEND MAIL

              var quantity, batch_number, nature_of_issue = '';

              quantity = '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
              nature_of_issue = '<tr><td>Nature Of Issue</td><td>' + task_details[0].nature_of_issue + '</td></tr>';
              batch_number = '<tr><td>Batch No</td><td>' + task_details[0].batch_number + '</td></tr>';

              var country = await taskModel.tasks_details_countries(task_id);

              if (country && country[0].country_name != '') {
                var country_name = country[0].country_name;
              } else {
                var country_name = '';
              }

              var country_name_by_lang = country_name;
              if (language != Config.default_language) {
                //COUNTRY
                let t_country = await taskModel.get_translated_country(task_id, language);
                country_name_by_lang = t_country[0].country_name;
              }

              var db_due_date = new Date(task_details[0].due_date);
              var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

              let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

              var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

              if (mail_arr.length > 0) {
                // var cust_status_mail = `<!DOCTYPE html>
                // <html>
                //   <body>
                //     <p><em>Thank you for your request</em></p>
                //     <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
                //     <p></p>
                //     <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
                //     <p>We have received your request with the following details:</p>
                //     <table>
                //       <tr>
                //         <td>Task Ref</td>
                //         <td>${task_details[0].task_ref}</td>
                //       </tr>
                //       <tr>
                //         <td>User Name</td>
                //         <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                //       </tr>
                //       <tr>
                //         <td>Request Type</td>
                //         <td>${task_details[0].req_name}</td>
                //       </tr>
                //       <tr>
                //         <td>Product</td>
                //         <td>${task_details[0].product_name}</td>
                //       </tr>
                //       <tr>
                //         <td>Market</td>
                //         <td>${country_name}</td>
                //       </tr>
                //         ${quantity}
                //         ${batch_number}
                //         ${nature_of_issue}
                //       <tr>
                //         <td valign="top">Requirement</td>
                //         <td valign="top">${entities.decode(task_details[0].content)}</td>
                //       </tr>
                //       <tr>
                //         <td>&nbsp;</td>
                //         <td></td>
                //       </tr>
                //       <tr>
                //         <td>&nbsp;</td>
                //         <td></td>
                //       </tr>
                //     </table>
                //     <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
                //     <p>-- Dr.Reddys API team</p>
                //   </body>
                // </html>`;

                // //console.log(cust_status_mail);
                // var mail_options1 = {
                //   from: Config.contactUsMail,
                //   to: mail_arr[0].to_arr,
                //   subject: mail_subject,
                //   html: cust_status_mail
                // };
                // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                //   mail_options1.cc = mail_arr[1].cc_arr;
                // }
                // //console.log(mail_options1);
                // common.sendMailB2C(mail_options1);
                var req_name_by_lang = task_details[0].req_name;
                var product_name_by_lang = task_details[0].product_name;
                switch (entities.decode(task_details[0].language)) {
                  case 'es':
                    req_name_by_lang = task_details[0].req_name_es;
                    product_name_by_lang = task_details[0].product_name_es;
                    break;
                  case 'pt':
                    req_name_by_lang = task_details[0].req_name_pt;
                    product_name_by_lang = task_details[0].product_name_pt;
                    break;
                  case 'ja':
                    req_name_by_lang = task_details[0].req_name_ja;
                    product_name_by_lang = task_details[0].product_name_ja;
                    break;
                  case 'zh':
                    req_name_by_lang = task_details[0].req_name_zh;
                    product_name_by_lang = task_details[0].product_name_zh;
                    break;
                  default:
                    req_name_by_lang = task_details[0].req_name;
                    product_name_by_lang = task_details[0].product_name;
                    break;
                }

                var quantity_by_lang = task_details[0].quantity;
                if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
                  let q_arr = task_details[0].quantity.split(' ');
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
                  quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
                }

                let mailParam = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: product_name_by_lang,
                  country_name: country_name_by_lang,
                  task_content: entities.decode(task_details[0].content),
                  url_status: url_status,
                  company_name: entities.decode(customer_details[0].company_name),
                  nature_of_issue: task_details[0].nature_of_issue,
                  quantity: quantity_by_lang,
                  batch_number: task_details[0].batch_number,
                  req_name: req_name_by_lang
                };

                var mailcc = '';
                if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                  mailcc = mail_arr[1].cc_arr;
                }
                //console.log("Complain mail");
                const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");
              }

              //Manager Mail
              let managerMailParam = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: task_details[0].task_ref,
                product_name: task_details[0].product_name,
                country_name: country_name,
                format_due_date: format_due_date,
                task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
                quantity: task_details[0].quantity,
                batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
                ccpurl: Config.ccpUrl,
                emp_first_name: manager[0].first_name,
                req_name: task_details[0].req_name
              };

              const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

              if (emp_details.length > 0) {
                //Employee Mail
                var empMailParam = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  nature_of_issue: (entities.decode(task_details[0].language) != 'en') ? task_details[0].nature_of_issue_translate : task_details[0].nature_of_issue,
                  quantity: task_details[0].quantity,
                  batch_number: (entities.decode(task_details[0].language) != 'en') ? task_details[0].batch_number_translate : task_details[0].batch_number,
                  ccpurl: Config.ccpUrl,
                  emp_first_name: emp_details[0].first_name,
                  req_name: task_details[0].req_name
                };

                const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
              }

              // const mailOptions = {
              //   from: Config.webmasterMail, // sender address
              //   to: manager[0].email,// manager[0].email, // list of receivers
              //   subject: mail_subject,
              //   html: `<!DOCTYPE html>
              //   <html>
              //   <body>
              //     <p><em>New Task Assigned</em></p>
              //     <p>Hello ${manager[0].first_name},</p>
              //     <p>You have a new task assigned.</p>
              //     <p>Task Details are as below,</p>
              //     <table>
              //       <tr>
              //         <td>Task Ref</td>
              //         <td>${task_details[0].task_ref}</td>
              //       </tr>
              //       <tr>
              //         <td>User Name</td>
              //         <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //       </tr>
              //       <tr>
              //         <td>Request Type</td>
              //         <td>${task_details[0].req_name}</td>
              //       </tr>
              //       <tr>
              //         <td>Product</td>
              //         <td>${task_details[0].product_name}</td>
              //       </tr>
              //       <tr>
              //         <td>Market</td>
              //         <td>${country_name}</td>
              //       </tr>
              //       <tr>
              //         <td>Due Date</td>
              //         <td>${format_due_date}</td>
              //       </tr>
              //         ${quantity}
              //         ${batch_number}
              //         ${nature_of_issue}
              //       <tr>
              //         <td valign="top">Requirement</td>
              //         <td valign="top">${entities.decode(task_details[0].content)}</td>
              //       </tr>
              //       <tr>
              //         <td>&nbsp;</td>
              //         <td></td>
              //       </tr>
              //       <tr>
              //         <td>&nbsp;</td>
              //         <td></td>
              //       </tr>
              //     </table>
              //     <p>
              //       Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
              //     </p>
              //   </body>
              //   </html>`
              // };
              // common.sendMail(mailOptions);

              // if (emp_details.length > 0) {
              //   const mailOptions2 = {
              //     from: Config.webmasterMail, // sender address
              //     to: emp_details[0].email, //emp_details[0].email, // list of receivers
              //     subject: mail_subject,
              //     html:`<!DOCTYPE html>
              //     <html>
              //       <body>
              //         <p><em>New Task Assigned</em></p>
              //         <p>Hello ${emp_details[0].first_name},</p>
              //         <p>You have a new task assigned.</p>
              //         <p>Task Details are as below,</p>
              //         <table>
              //           <tr>
              //             <td>Task Ref</td>
              //             <td>${task_details[0].task_ref}</td>
              //           </tr>
              //           <tr>
              //             <td>User Name</td>
              //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Request Type</td>
              //             <td>${task_details[0].req_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Product</td>
              //             <td>${task_details[0].product_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Market</td>
              //             <td>${country_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Due Date</td>
              //             <td>${format_due_date}</td>
              //           </tr>
              //             ${quantity}
              //             ${batch_number}
              //             ${nature_of_issue}
              //           <tr>
              //             <td valign="top">Requirement</td>
              //             <td valign="top">${entities.decode(task_details[0].content)}</td>
              //           </tr>
              //           <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //           </tr>
              //           <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //           </tr>
              //         </table>
              //         <p>
              //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
              //         </p>
              //       </body>
              //     </html>`
              //   };
              //   common.sendMail(mailOptions2);
              // }

              //SEND MAIL

            } else {
              //Unallocated Task Soumyadeep
              var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
              common.logError(err);
            }

            //}

            var files = req.files;
            if (files && files.length > 0) {

              for (var j = 0; j < files.length; j++) {

                let file_details = {
                  task_id: task_id,
                  actual_file_name: files[j].originalname,
                  new_file_name: files[j].filename,
                  date_added: today
                }

                await taskModel.add_task_file(file_details);
              }
            }

            //SALES FORCE TASK
            if (Config.activate_sales_force == true) {
              await module.exports.__post_sales_force_add_task(task_id, files, req.user.empe);
              if (comment_arr.length > 0) {
                for (let index = 0; index < comment_arr.length; index++) {
                  const element = comment_arr[index];
                  await module.exports.__post_sales_force_internal_discussions(task_id, [], element.comment, element.posted_by);
                }
              }

            }

            total++;
          }).catch(err => {
            common.logError(err);
            // res.status(400).json({
            //   status: 3,
            //   message: Config.errorText.value
            // }).end();
          })
      }

      if(total == product_arr.length){
        res.status(200).json({
          status: '1'
        }).end();
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Add Sample
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Add Sample used for Sample selection
   */
  add_sample_old: async (req, res, next) => {
    try {
      var today = common.currentDateTime();
      let taskObj = {};
      var request_id = req.body.request_id;

      if (req.user.role == 2) {
        var submitted_by = req.user.customer_id;
        var customer_id = req.body.agent_customer_id;
      } else {
        var submitted_by = 0;
        var customer_id = req.user.customer_id;
      }

      var sla = await taskModel.get_total_sla(request_id);
      var days = await common.dayCountExcludingWeekends(sla.total_sla);
      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
      var reference_key = 'R';
      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);

        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }


      const {
        product_id,
        country_id,
        description,
        request_type_id,
        samples,
        samples_batch_no,
        samples_quantity,
        working,
        working_quantity,
        impurities,
        impurities_required,
        impurities_quantity,
        shipping_address,
        files,
        cc_customers,
        pharmacopoeia,
        share_with_agent
      } = req.body;

      taskObj = {
        customer_id: customer_id,
        product_id: product_id,
        country_id: 0,
        parent_id: 0,
        due_date: sla_due_date,
        pharmacopoeia: entities.encode(pharmacopoeia),

        sample: samples,
        number_of_batches: samples_batch_no,
        //quantity : entities.encode(samples_quantity.trim()),
        quantity: entities.encode(samples_quantity),
        working_standards: working,
        working_quantity: entities.encode(working_quantity),
        impurities: impurities,
        specify_impurity_required: entities.encode(impurities_required),
        impurities_quantity: impurities_quantity,
        shipping_address: entities.encode(shipping_address),
        description: entities.encode(description),
        request_type_id: req.body.request_id,

        submitted_by: submitted_by,
        close_status: 0,
        drupal_task_id: 0,
        priority: 2,
        rdd: sla_due_date,
        current_date: today,
        front_due_date: front_due_date,
        cc_customers: cc_customers,
        share_with_agent: share_with_agent
      }

      taskObj.ccp_posted_by = (req.user.empe > 0) ? +req.user.empe : 0;

      await taskModel.add_task(taskObj)
        .then(async function (data) {
          let task_id = data.task_id;

          if (country_id && country_id != '') {
            var parsed_country_id = JSON.parse(country_id);
            for (let index = 0; index < parsed_country_id.length; index++) {
              await taskModel.map_countries(task_id, parsed_country_id[index].value);
            }
          }

          let manager = await taskModel.get_allocated_manager(taskObj.customer_id);

          let ref_no = 'PHL-' + reference_key + '-' + task_id;
          let customerData = [];
          await taskModel.update_ref_no(ref_no, task_id)
            .then(async () => {
              // ACTIVITY LOG - SATYAJIT
              customerData = await customerModel.get_user(taskObj.customer_id);
              var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
              var params = {
                task_ref: ref_no,
                request_type: request_type_id,
                customer_name: customername
              }
              if (req.user.role == 2) {
                var agentData = await agentModel.get_user(submitted_by);
                params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                await taskModel.log_activity(2, task_id, params)
              } else {
                await taskModel.log_activity(1, task_id, params)
              }
              // END LOG
            });

          manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

          if (manager.length > 0) {

            await taskModel.update_task_owner(manager[0].employee_id, task_id);

            //SEND MAIL
            let task_details = await taskModel.tasks_details(task_id);

            var customer_details = await customerModel.get_user(task_details[0].customer_id);

            var mail_subject = '';
            if (task_details[0].request_type == 24) {
              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
            } else {
              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
            }


            const assignment = {
              task_id: task_id,
              assigned_to: manager[0].employee_id,
              assigned_by: -1,
              assign_date: today,
              due_date: sla_due_date,
              reopen_date: today,
              parent_assignment_id: 0
            }
            let parent_assign_id = await taskModel.assign_task(assignment);

            // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
            var params = {
              task_ref: ref_no,
              request_type: request_type_id,
              employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`,
              employee_sender: 'SYSTEM'
            }
            await taskModel.log_activity(8, task_id, params);
            // END LOG

            let employee_notification = 22;
            let param_notification = {};
            let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            let cc_arr = [
              employee_notification,
              task_id,
              manager[0].employee_id,
              common.currentDateTime(),
              0,
              notification_text,
              employee_notification,
              mail_subject
            ]
            await taskModel.notify_employee(cc_arr);

            //CSC TECHNICAL
            var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2);

            emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

            var bot_assignment_id = 0;

            if (emp_details.length > 0) {
              let assignment_val = 2;
              await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                .then(async function (data) {

                  const assign_tasks = {
                    assigned_by: manager[0].employee_id,
                    assigned_to: emp_details[0].employee_id,
                    task_id: task_id,
                    due_date: sla_due_date,
                    assign_date: today,
                    parent_assignment_id: parent_assign_id.assignment_id,
                    comment: '',
                    reopen_date: today
                  }

                  await taskModel.assign_task(assign_tasks)
                    .then(async function (tdata) {

                      // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                      var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                      var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                      var params = {
                        task_ref: ref_no,
                        request_type: request_type_id,
                        employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                        employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                      }
                      await taskModel.log_activity(8, task_id, params)
                      // END LOG

                      bot_assignment_id = tdata.assignment_id;

                      let employee_notification = 8;
                      let param_notification = {
                        employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                        assigned_employee: `you`
                      };
                      let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                      let cc_arr = [
                        employee_notification,
                        task_id,
                        emp_details[0].employee_id,
                        common.currentDateTime(),
                        0,
                        notification_text,
                        employee_notification,
                        mail_subject
                      ]
                      await taskModel.notify_employee(cc_arr);

                      const task_comment = {
                        posted_by: manager[0].employee_id,
                        task_id: task_id,
                        comment: `This task has been auto assigned to ${emp_recipient} ${emp_details[0].desig_name} by the system on my behalf.`,
                        assignment_id: bot_assignment_id,
                        post_date: today
                      };

                      await taskModel.insert_comment_log(task_comment);

                      //console.log('child assignment',bot_assignment_id);

                    }).catch(err => {
                      common.logError(err);
                      res.status(400)
                        .json({
                          status: 3,
                          message: Config.errorText.value
                        }).end();
                    });

                }).catch(err => {
                  common.logError(err);
                  res.status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value
                    }).end();
                })
            } else {
              bot_assignment_id = parent_assign_id.assignment_id;
            }

            //CURRENT OWNER
            if (emp_details.length > 0) {
              await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
            } else {
              await taskModel.update_current_owner(task_id, manager[0].employee_id);
            }
            //CURRENT OWNER

            //Xceed bot -- satyajit
            // if( (Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0 ){
            //   //console.log('bot_assignment_id',bot_assignment_id);
            //   //console.log('url',`${Config.drlBot.assign_url}${bot_assignment_id}`);
            //   const request = require('request');
            //   var options = {
            //     method: 'POST',
            //     url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
            //     headers:
            //     {'x-api-key': 'XAmdVeqheisEoJ1SROQG'}
            //   };

            //   await request(options, async function (error, response, body) {
            //     if (error){
            //         var mailOptions = {
            //               from: Config.webmasterMail,
            //               to: ['sayan.mazumder@indusnet.co.in'],
            //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //             };
            //         await common.sendMail(mailOptions);
            //         await common.logErrorText('Bot entry failed create_task',JSON.stringify(error));

            //     } else {                 
            //         if(response.statusCode != 200){                 
            //             var mailOptions = {
            //               from: Config.webmasterMail,
            //               to: ['sayan.mazumder@indusnet.co.in'],
            //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //             };
            //             await common.sendMail(mailOptions);
            //             await common.logErrorText('Bot entry failed create_task',JSON.stringify(response));
            //         }
            //     }
            //   });
            // }
            //end 

            //INSERT CC CUSTOMERS
            if (taskObj.cc_customers) {
              let cc_cust_arr = JSON.parse(taskObj.cc_customers);
              //INSERT CC CUSTOMERS
              if (cc_cust_arr.length > 0) {
                for (let index = 0; index < cc_cust_arr.length; index++) {
                  const element = cc_cust_arr[index].customer_id;
                  let insrt_cc = {
                    customer_id: element,
                    task_id: task_id,
                    status: 1
                  };
                  await customerModel.insert_cc_customer(insrt_cc);
                }
              }
            }

            //HIGHLIGHT NEW TASK
            let customer_id = taskObj.customer_id;

            let highlight_arr = [
              task_id,
              customer_id,
              1,
              'C'
            ];
            await taskModel.task_highlight(highlight_arr);

            let cc_list = await taskModel.task_cc_customer(task_id);
            if (cc_list && cc_list.length > 0) {
              for (let index = 0; index < cc_list.length; index++) {
                const element = cc_list[index];
                let highlight_cc_arr = [
                  task_id,
                  element.customer_id,
                  1,
                  'C'
                ];
                await taskModel.task_highlight(highlight_cc_arr);
              }
            }

            if (taskObj.share_with_agent == 1) {
              let customer_details = await customerModel.get_user(taskObj.customer_id);
              let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

              if (agent_list && agent_list.length > 0) {
                for (let index = 0; index < agent_list.length; index++) {
                  const element = agent_list[index];
                  let highlight_agent_arr = [
                    task_id,
                    element.agent_id,
                    1,
                    'A'
                  ];
                  await taskModel.task_highlight(highlight_agent_arr);
                }
              }
            }



            var pharmacopoeia, shipping_address, samples, impurities, working_standards = '';

            var request_type = task_details[0].request_type;

            var req_type_arr = task_details[0].service_request_type.split(',');

            if (req_type_arr.indexOf('Samples') !== -1) {
              samples = '<tr><td>Samples</td><td></td></tr>';

              if (task_details[0].number_of_batches != null) {
                samples += '<tr><td>Number Of Batches</td><td>' + task_details[0].number_of_batches + '</td></tr>';
              } else {
                samples += '<tr><td>Number Of Batches</td><td></td></tr>';
              }

              if (task_details[0].quantity != null) {
                samples += '<tr><td>Quantity</td><td>' + task_details[0].quantity + '</td></tr>';
              } else {
                samples += '<tr><td>Quantity</td><td></td></tr>';
              }
            } else {
              samples = '';
            }

            if (req_type_arr.indexOf('Working Standards') !== -1) {
              working_standards = '<tr><td>Working Standards</td><td></td></tr>';

              if (task_details[0].working_quantity != null) {
                working_standards += '<tr><td>Quantity</td><td>' + task_details[0].working_quantity + '</td></tr>';
              } else {
                working_standards += '<tr><td>Quantity</td><td></td></tr>';
              }

            } else {
              working_standards = '';
            }

            if (req_type_arr.indexOf('Impurities') !== -1) {
              impurities = '<tr><td>Impurities</td><td></td></tr>';

              if (task_details[0].impurities_quantity != null) {
                impurities += '<tr><td>Quantity</td><td>' + task_details[0].impurities_quantity + '</td></tr>';
              } else {
                impurities += '<tr><td>Quantity</td><td></td></tr>';
              }

              if (task_details[0].specify_impurity_required != null) {
                impurities += '<tr><td>Specify Impurity Required</td><td>' + task_details[0].specify_impurity_required + '</td></tr>';
              } else {
                impurities += '<tr><td>Specify Impurity Required</td><td></td></tr>';
              }

            } else {
              impurities = '';
            }

            shipping_address = '<tr><td>Shipping Address</td><td>' + task_details[0].shipping_address + '</td></tr>';

            pharmacopoeia = '<tr><td>Pharmacopoeia</td><td>' + task_details[0].pharmacopoeia + '</td></tr>';

            var country = await taskModel.tasks_details_countries(task_id);

            if (country && country[0].country_name != '') {
              var country_name = country[0].country_name;
            } else {
              var country_name = '';
            }

            var db_due_date = new Date(task_details[0].due_date);
            var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

            let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

            var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

            if (mail_arr.length > 0) {
              var cust_status_mail = `<!DOCTYPE html>
              <html>
                <body>
                  <p><em>Thank you for your request</em></p>
                  <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
                  <p></p>
                  <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
                  <p>We have received your request with the following details:</p>

                  <table>
                    <tr>
                      <td>Task Ref</td>
                      <td>${task_details[0].task_ref}</td>
                    </tr>
                    <tr>
                      <td>User Name</td>
                      <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                    </tr>
                    <tr>
                      <td>Request Type</td>
                      <td>${task_details[0].req_name}</td>
                    </tr>
                    <tr>
                      <td>Product</td>
                      <td>${task_details[0].product_name}</td>
                    </tr>
                    <tr>
                      <td>Market</td>
                      <td>${country_name}</td>
                    </tr>
                      ${pharmacopoeia}
                      ${samples}
                      ${working_standards}
                      ${impurities}
                      ${entities.decode(shipping_address)}
                    <tr>
                      <td valign="top">Requirement</td>
                      <td valign="top">${entities.decode(task_details[0].content)}</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
                  </table>
                  <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
                  <p>-- Dr.Reddys API team</p>
                </body>
                
                </html>`;

              //console.log(cust_status_mail);
              var mail_options1 = {
                from: Config.contactUsMail,
                to: mail_arr[0].to_arr,
                subject: mail_subject,
                html: cust_status_mail
              };
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mail_options1.cc = mail_arr[1].cc_arr;
              }
              //console.log(mail_options1);
              common.sendMailB2C(mail_options1);
            }

            const mailOptions = {
              from: Config.webmasterMail, // sender address
              to: manager[0].email, // manager[0].email, // list of receivers
              subject: mail_subject,
              html: `<!DOCTYPE html>
              <html>
                <body>
                  <p><em>New Task Assigned</em></p>
                  <p>Hello ${manager[0].first_name},</p>
                  <p>You have a new task assigned.</p>
                  <p>Task Details are as below,</p>
                  <table>
                    <tr>
                      <td>Task Ref</td>
                      <td>${task_details[0].task_ref}</td>
                    </tr>
                    <tr>
                      <td>User Name</td>
                      <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                    </tr>
                    <tr>
                      <td>Request Type</td>
                      <td>${task_details[0].req_name}</td>
                    </tr>
                    <tr>
                      <td>Product</td>
                      <td>${task_details[0].product_name}</td>
                    </tr>
                    <tr>
                      <td>Market</td>
                      <td>${country_name}</td>
                    </tr>
                    <tr>
                      <td>Due Date</td>
                      <td>${format_due_date}</td>
                    </tr>
                      ${pharmacopoeia}
                      ${samples}
                      ${working_standards}
                      ${impurities}
                      ${entities.decode(shipping_address)}
                    <tr>
                      <td valign="top">Requirement</td>
                      <td valign="top">${entities.decode(task_details[0].content)}</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
                  </table>
                  <p>
                    Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
                  </p>
                </body>
              </html>`
            };
            common.sendMail(mailOptions);

            if (emp_details.length > 0) {
              const mailOptions2 = {
                from: Config.webmasterMail, // sender address
                to: emp_details[0].email, //emp_details[0].email, // list of receivers
                subject: mail_subject,
                html: `<!DOCTYPE html>
                <html>
                  <body>
                    <p><em>New Task Assigned</em></p>
                    <p>Hello ${emp_details[0].first_name},</p>
                    <p>You have a new task assigned.</p>
                    <p>Task Details are as below,</p>
                    <table>
                      <tr>
                        <td>Task Ref</td>
                        <td>${task_details[0].task_ref}</td>
                      </tr>
                      <tr>
                        <td>User Name</td>
                        <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                      </tr>
                      <tr>
                        <td>Request Type</td>
                        <td>${task_details[0].req_name}</td>
                      </tr>
                      <tr>
                        <td>Product</td>
                        <td>${task_details[0].product_name}</td>
                      </tr>
                      <tr>
                        <td>Market</td>
                        <td>${country_name}</td>
                      </tr>
                      <tr>
                        <td>Due Date</td>
                        <td>${format_due_date}</td>
                      </tr>
                        ${pharmacopoeia}
                        ${samples}
                        ${working_standards}
                        ${impurities}
                        ${entities.decode(shipping_address)}
                      <tr>
                        <td valign="top">Requirement</td>
                        <td valign="top">${entities.decode(task_details[0].content)}</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td></td>
                      </tr>
                    </table>
                    <p>
                      Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
                    </p>
                  </body>
                </html>`
              };
              common.sendMail(mailOptions2);
            }

            //SEND MAIL

          } else {
            //Unallocated Task Soumyadeep
            var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
            common.logError(err);
          }

          var files = req.files;
          if (files && files.length > 0) {

            for (var j = 0; j < files.length; j++) {

              let file_details = {
                task_id: task_id,
                actual_file_name: files[j].originalname,
                new_file_name: files[j].filename,
                date_added: today
              }

              await taskModel.add_task_file(file_details);
            }
          }

          res.status(200).json({
            status: '1',
            message: data
          }).end();
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Add Sample
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Add Sample used for Sample selection
   */
  add_sample: async (req, res, next) => {
    try {
      var today = common.currentDateTime();
      let taskObj = {};
      var request_id = req.body.request_id;

      if (req.user.role == 2) {
        var submitted_by = req.user.customer_id;
        var customer_id = req.body.agent_customer_id;
      } else {
        var submitted_by = 0;
        var customer_id = req.user.customer_id;
      }

      var sla = await taskModel.get_total_sla(request_id);
      var days = await common.dayCountExcludingWeekends(sla.total_sla);
      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
      var reference_key = 'R';
      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);

        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }

      const {
        product_id,
        country_id,
        description,
        request_type_id,
        samples,
        samples_batch_no,
        samples_quantity,
        working,
        working_quantity,
        impurities,
        impurities_required,
        impurities_quantity,
        shipping_address,
        files,
        cc_customers,
        pharmacopoeia,
        share_with_agent,
        swi_data
      } = req.body;

      let language = '';
      if (req.user.empe > 0) {
        language = await customerModel.get_customer_language(customer_id);
      } else {
        language = await customerModel.get_customer_language(customer_id);
      }

      let product_counter = 0;
      let task_arr = [];
      let swi_data_arr = JSON.parse(swi_data);
      for (let index = 0; index < swi_data_arr.length; index++) {
        const swi_element = swi_data_arr[index];

        taskObj = {
          customer_id: customer_id,
          product_id: swi_element.product_id,
          country_id: 0,
          parent_id: 0,
          due_date: sla_due_date,
          pharmacopoeia: entities.encode(pharmacopoeia),

          sample: (swi_element.sample.chk) ? 1 : 0,
          number_of_batches: (swi_element.sample.batch > 0) ? swi_element.sample.batch : '',
          quantity: (swi_element.sample.quantity != '' && swi_element.sample.unit.length > 0) ? entities.encode(`${swi_element.sample.quantity} ${swi_element.sample.unit[0].value}`) : '',

          working_standards: (swi_element.working_standards.chk) ? 1 : 0,
          working_quantity: (swi_element.working_standards.quantity != '' && swi_element.working_standards.unit.length > 0) ? entities.encode(`${swi_element.working_standards.quantity} ${swi_element.working_standards.unit[0].value}`) : '',

          impurities: (swi_element.impurities.chk) ? 1 : 0,
          specify_impurity_required: (swi_element.impurities.specify_impurities != '') ? entities.encode(swi_element.impurities.specify_impurities) : '',
          impurities_quantity: (swi_element.impurities.quantity != '' && swi_element.impurities.unit.length > 0) ? entities.encode(`${swi_element.impurities.quantity} ${swi_element.impurities.unit[0].value}`) : '',

          shipping_address: entities.encode(shipping_address),
          description: entities.encode(description),
          request_type_id: req.body.request_id,

          submitted_by: submitted_by,
          close_status: 0,
          drupal_task_id: 0,
          priority: 2,
          rdd: sla_due_date,
          current_date: today,
          front_due_date: front_due_date,
          cc_customers: cc_customers,
          share_with_agent: share_with_agent,
          language: language
        }

        taskObj.ccp_posted_by = (req.user.empe > 0) ? +req.user.empe : 0;

        await taskModel.add_task(taskObj)
          .then(async function (data) {
            let task_id = data.task_id;

            if (language != Config.default_language) {
              await module.exports.__translate_language(taskObj, task_id, country_id);
            }

            if (country_id && country_id != '') {
              var parsed_country_id = JSON.parse(country_id);
              for (let index = 0; index < parsed_country_id.length; index++) {
                await taskModel.map_countries(task_id, parsed_country_id[index].value);
              }
            }

            let manager = await taskModel.get_allocated_manager(taskObj.customer_id);

            let ref_no = 'PHL-' + reference_key + '-' + task_id;
            let customerData = [];
            await taskModel.update_ref_no(ref_no, task_id)
              .then(async () => {
                // ACTIVITY LOG - SATYAJIT
                customerData = await customerModel.get_user(taskObj.customer_id);
                var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
                var params = {
                  task_ref: ref_no,
                  request_type: request_type_id,
                  customer_name: customername
                }
                if (req.user.role == 2) {
                  var agentData = await agentModel.get_user(submitted_by);
                  params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                  await taskModel.log_activity(2, task_id, params)
                } else {
                  await taskModel.log_activity(1, task_id, params)
                }
                // END LOG
              });


            // let spoc_exists = false;
            // let spoc = [];
            // if (language != Config.default_language && customerData[0].ignore_spoc == 2) {
            //   let assigned_spoc = await taskModel.get_allocated_spoc(taskObj.customer_id, language);
            //   assigned_spoc = await module.exports.__get_allocated_leave_employee_spoc(assigned_spoc, taskObj.customer_id);
            //   if (assigned_spoc.length > 0) {
            //     spoc_exists = true;
            //     spoc = assigned_spoc;
            //   } else if (await taskModel.check_language_spoc(language)) {
            //     spoc_exists = true;
            //     global_spoc = await taskModel.get_global_spoc(language);
            //     spoc = await module.exports.__get_allocated_leave_employee_spoc(global_spoc, taskObj.customer_id);
            //   }
            // }

            let comment_arr = [];

            // if (spoc_exists && spoc.length > 0) {

            //   const assignment_spoc = {
            //     task_id: task_id,
            //     assigned_to: spoc[0].employee_id,
            //     assigned_by: -1,
            //     assign_date: today,
            //     due_date: sla_due_date,
            //     reopen_date: today,
            //     parent_assignment_id: 0
            //   }

            //   var spoc_parent_assign_id = await taskModel.assign_task(assignment_spoc).catch(err => {
            //     common.logError(err);
            //     res.status(400)
            //       .json({
            //         status: 3,
            //         message: Config.errorText.value
            //       }).end();
            //   });

            //   // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //   var spoc_recipient = `${spoc[0].first_name} ${spoc[0].last_name}`;
            //   var params = {
            //     task_ref: ref_no,
            //     request_type: request_type_id,
            //     employee_sender: 'SYSTEM',
            //     employee_recipient: spoc_recipient + ` (${(spoc[0].desig_name)})`
            //   }
            //   await taskModel.log_activity(8, task_id, params);
            //   // END LOG

            //   let employee_notification = 22;
            //   let param_notification = {};
            //   let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //   let cc_arr = [
            //     employee_notification,
            //     task_id,
            //     spoc[0].employee_id,
            //     common.currentDateTime(),
            //     0,
            //     notification_text,
            //     employee_notification,
            //     mail_subject
            //   ]
            //   await taskModel.notify_employee(cc_arr);


            //   manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

            //   if (manager.length > 0) {

            //     let assignment_val = 2;
            //     await taskModel.update_task_assignment(task_id, spoc[0].employee_id, assignment_val);

            //     await taskModel.update_task_owner(manager[0].employee_id, task_id);

            //     //SEND MAIL
            //     let task_details = await taskModel.tasks_details(task_id);

            //     var customer_details = await customerModel.get_user(task_details[0].customer_id);

            //     var mail_subject = '';
            //     if (task_details[0].request_type == 24) {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
            //     } else {
            //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
            //     }


            //     const assignment = {
            //       task_id: task_id,
            //       assigned_to: manager[0].employee_id,
            //       assigned_by: spoc[0].employee_id,
            //       assign_date: today,
            //       due_date: sla_due_date,
            //       reopen_date: today,
            //       parent_assignment_id: spoc_parent_assign_id.assignment_id
            //     }
            //     let parent_assign_id = await taskModel.assign_task(assignment);

            //     // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            //     var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
            //     var params = {
            //       task_ref: ref_no,
            //       request_type: request_type_id,
            //       employee_sender: spoc_recipient + ` (${(spoc[0].desig_name)})`,
            //       employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
            //     }
            //     await taskModel.log_activity(8, task_id, params);
            //     // END LOG

            //     let employee_notification = 8;
            //     let param_notification = {
            //       employee_name: `${spoc[0].first_name} (${(spoc[0].desig_name)})`,
            //       assigned_employee: `you`
            //     };
            //     let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //     let cc_arr = [
            //       employee_notification,
            //       task_id,
            //       manager[0].employee_id,
            //       common.currentDateTime(),
            //       0,
            //       notification_text,
            //       employee_notification,
            //       mail_subject
            //     ]
            //     await taskModel.notify_employee(cc_arr);

            //     let task_comment = {
            //       posted_by: spoc[0].employee_id,
            //       task_id: task_id,
            //       comment: `This task has been auto assigned to ${manager[0].first_name} ${manager[0].last_name} (${(manager[0].desig_name)}) by the system on my behalf.`,
            //       assignment_id: spoc_parent_assign_id.assignment_id,
            //       post_date: today
            //     };

            //     await taskModel.insert_comment_log(task_comment);

            //     comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

            //     //CSC TECHNICAL
            //     var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2);

            //     emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

            //     var bot_assignment_id = 0;

            //     if (emp_details.length > 0) {
            //       let assignment_val = 2;
            //       await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
            //         .then(async function (data) {

            //           const assign_tasks = {
            //             assigned_by: manager[0].employee_id,
            //             assigned_to: emp_details[0].employee_id,
            //             task_id: task_id,
            //             due_date: sla_due_date,
            //             assign_date: today,
            //             parent_assignment_id: parent_assign_id.assignment_id,
            //             comment: '',
            //             reopen_date: today
            //           }

            //           await taskModel.assign_task(assign_tasks)
            //             .then(async function (tdata) {

            //               // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
            //               var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
            //               var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
            //               var params = {
            //                 task_ref: ref_no,
            //                 request_type: request_type_id,
            //                 employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
            //                 employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
            //               }
            //               await taskModel.log_activity(8, task_id, params)
            //               // END LOG

            //               bot_assignment_id = tdata.assignment_id;

            //               let employee_notification = 8;
            //               let param_notification = {
            //                 employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
            //                 assigned_employee: `you`
            //               };
            //               let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            //               let cc_arr = [
            //                 employee_notification,
            //                 task_id,
            //                 emp_details[0].employee_id,
            //                 common.currentDateTime(),
            //                 0,
            //                 notification_text,
            //                 employee_notification,
            //                 mail_subject
            //               ]
            //               await taskModel.notify_employee(cc_arr);

            //               const task_comment = {
            //                 posted_by: manager[0].employee_id,
            //                 task_id: task_id,
            //                 comment: `This task has been auto assigned to ${emp_recipient} ${emp_details[0].desig_name} by the system on my behalf.`,
            //                 assignment_id: bot_assignment_id,
            //                 post_date: today
            //               };

            //               await taskModel.insert_comment_log(task_comment);

            //               comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

            //               //console.log('child assignment',bot_assignment_id);

            //             }).catch(err => {
            //               common.logError(err);
            //               res.status(400)
            //                 .json({
            //                   status: 3,
            //                   message: Config.errorText.value
            //                 }).end();
            //             });

            //         }).catch(err => {
            //           common.logError(err);
            //           res.status(400)
            //             .json({
            //               status: 3,
            //               message: Config.errorText.value
            //             }).end();
            //         })
            //     } else {
            //       bot_assignment_id = parent_assign_id.assignment_id;
            //     }

            //     //CURRENT OWNER
            //     if (emp_details.length > 0) {
            //       await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
            //     } else {
            //       await taskModel.update_current_owner(task_id, manager[0].employee_id);
            //     }
            //     //CURRENT OWNER

            //     //INSERT CC CUSTOMERS
            //     if (taskObj.cc_customers) {
            //       let cc_cust_arr = JSON.parse(taskObj.cc_customers);
            //       //INSERT CC CUSTOMERS
            //       if (cc_cust_arr.length > 0) {
            //         for (let index = 0; index < cc_cust_arr.length; index++) {
            //           const element = cc_cust_arr[index].customer_id;
            //           let insrt_cc = {
            //             customer_id: element,
            //             task_id: task_id,
            //             status: 1
            //           };
            //           await customerModel.insert_cc_customer(insrt_cc);
            //         }
            //       }
            //     }

            //     //HIGHLIGHT NEW TASK
            //     let customer_id = taskObj.customer_id;

            //     let highlight_arr = [
            //       task_id,
            //       customer_id,
            //       1,
            //       'C'
            //     ];
            //     await taskModel.task_highlight(highlight_arr);

            //     let cc_list = await taskModel.task_cc_customer(task_id);
            //     if (cc_list && cc_list.length > 0) {
            //       for (let index = 0; index < cc_list.length; index++) {
            //         const element = cc_list[index];
            //         let highlight_cc_arr = [
            //           task_id,
            //           element.customer_id,
            //           1,
            //           'C'
            //         ];
            //         await taskModel.task_highlight(highlight_cc_arr);
            //       }
            //     }

            //     if (taskObj.share_with_agent == 1) {
            //       let customer_details = await customerModel.get_user(taskObj.customer_id);
            //       let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            //       if (agent_list && agent_list.length > 0) {
            //         for (let index = 0; index < agent_list.length; index++) {
            //           const element = agent_list[index];
            //           let highlight_agent_arr = [
            //             task_id,
            //             element.agent_id,
            //             1,
            //             'A'
            //           ];
            //           await taskModel.task_highlight(highlight_agent_arr);
            //         }
            //       }
            //     }



            //     var pharmacopoeia, shipping_address, samples, impurities, working_standards = '';
            //     var pharmacopoeia_translate, shipping_address_translate, samples_translate, impurities_translate, working_standards_translate = '';

            //     var request_type = task_details[0].request_type;

            //     var req_type_arr = task_details[0].service_request_type.split(',');

            //     if (req_type_arr.indexOf('Samples') !== -1) {
            //       samples = `<tr><td>${await common.getTranslatedTextMail('Samples',language)}</td><td></td></tr>`;
            //       samples_translate = `<tr><td>${await common.getTranslatedTextMail('Samples',Config.default_language)}</td><td></td></tr>`;

            //       if (task_details[0].number_of_batches != null) {
            //         samples += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches',language)}</td><td>${task_details[0].number_of_batches}</td></tr>`;

            //         samples_translate += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches',Config.default_language)}</td><td>${task_details[0].number_of_batches}</td></tr>`;

            //       } else {
            //         samples += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches',language)}</td><td></td></tr>`;
            //         samples_translate += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches',Config.default_language)}</td><td></td></tr>`;
            //       }

            //       if (task_details[0].quantity != null) {

            //         let quantity_by_lang = task_details[0].quantity;
            //         samples_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
            //         if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
            //           let q_arr = task_details[0].quantity.split(' ');
            //           let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //           quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            //         }

            //         samples += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td>${quantity_by_lang}</td></tr>`;
            //       } else {
            //         samples += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td></td></tr>`;
            //         samples_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td></td></tr>`;
            //       }
            //     } else {
            //       samples = '';
            //       samples_translate = '';
            //     }

            //     if (req_type_arr.indexOf('Working Standards') !== -1) {
            //       working_standards = `<tr><td>${await common.getTranslatedTextMail('Working Standards',language)}</td><td></td></tr>`;
            //       working_standards_translate = `<tr><td>${await common.getTranslatedTextMail('Working Standards',Config.default_language)}</td><td></td></tr>`;

            //       if (task_details[0].working_quantity != null) {

            //         let quantity_by_lang = task_details[0].working_quantity;
            //         working_standards_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
            //         if (task_details[0].working_quantity && trim(task_details[0].working_quantity) != '' && task_details[0].language != Config.default_language) {
            //           let q_arr = task_details[0].working_quantity.split(' ');
            //           let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //           quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            //         }

            //         working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td>${quantity_by_lang}</td></tr>`;
            //       } else {
            //         working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td></td></tr>`;
            //         working_standards_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td></td></tr>`;
            //       }

            //     } else {
            //       working_standards = '';
            //       working_standards_translate = '';
            //     }

            //     if (req_type_arr.indexOf('Impurities') !== -1) {
            //       impurities = `<tr><td>${await common.getTranslatedTextMail('Impurities',language)}</td><td></td></tr>`;
            //       impurities_translate = `<tr><td>${await common.getTranslatedTextMail('Impurities',Config.default_language)}</td><td></td></tr>`;

            //       if (task_details[0].impurities_quantity != null) {

            //         let quantity_by_lang = task_details[0].impurities_quantity;
            //         impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
            //         if (task_details[0].impurities_quantity && trim(task_details[0].impurities_quantity) != '' && task_details[0].language != Config.default_language) {
            //           let q_arr = task_details[0].impurities_quantity.split(' ');
            //           let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //           quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            //         }

            //         impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td>${quantity_by_lang}</td></tr>`;
            //       } else {
            //         impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity',language)}</td><td></td></tr>`;
            //         impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity',Config.default_language)}</td><td></td></tr>`;
            //       }

            //       if (task_details[0].specify_impurity_required != null) {
            //         impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required',language)}</td><td>${task_details[0].specify_impurity_required}</td></tr>`;
            //         impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required',Config.default_language)}</td><td>${task_details[0].specify_impurity_required_translate}</td></tr>`;
            //       } else {
            //         impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required',language)}</td><td></td></tr>`;
            //         impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required',Config.default_language)}</td><td></td></tr>`;
            //       }

            //     } else {
            //       impurities = '';
            //       impurities_translate = '';
            //     }

            //     shipping_address = `<tr><td>${await common.getTranslatedTextMail('Shipping Address',language)}</td><td>${task_details[0].shipping_address}</td></tr>`;
            //     shipping_address_translate = `<tr><td>${await common.getTranslatedTextMail('Shipping Address',Config.default_language)}</td><td>${task_details[0].shipping_address_translate}</td></tr>`;

            //     pharmacopoeia = `<tr><td>${await common.getTranslatedTextMail('Pharmacopoeia',language)}</td><td>${task_details[0].pharmacopoeia}</td></tr>`;
            //     pharmacopoeia_translate = `<tr><td>${await common.getTranslatedTextMail('Pharmacopoeia',Config.default_language)}</td><td>${task_details[0].pharmacopoeia_translate}</td></tr>`;

            //     var country = await taskModel.tasks_details_countries(task_id);

            //     if (country && country[0].country_name != '') {
            //       var country_name = country[0].country_name;
            //     } else {
            //       var country_name = '';
            //     }

            //     var db_due_date = new Date(task_details[0].due_date);
            //     var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

            //     let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

            //     var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

            //     var country_name_by_lang = country_name;
            //     if (language != Config.default_language) {
            //       //COUNTRY
            //       let t_country = await taskModel.get_translated_country(task_id, language);
            //       country_name_by_lang = t_country[0].country_name;
            //     }

            //     var req_name_by_lang = task_details[0].req_name;
            //     var product_name_by_lang = task_details[0].product_name;
            //     switch (entities.decode(task_details[0].language)) {
            //       case 'es':
            //         req_name_by_lang = task_details[0].req_name_es;
            //         product_name_by_lang = task_details[0].product_name_es;
            //         break;
            //       case 'pt':
            //         req_name_by_lang = task_details[0].req_name_pt;
            //         product_name_by_lang = task_details[0].product_name_pt;
            //         break;
            //       case 'ja':
            //         req_name_by_lang = task_details[0].req_name_ja;
            //         product_name_by_lang = task_details[0].product_name_ja;
            //         break;
            //       case 'zh':
            //         req_name_by_lang = task_details[0].req_name_zh;
            //         product_name_by_lang = task_details[0].product_name_zh;
            //         break;
            //       default:
            //         req_name_by_lang = task_details[0].req_name;
            //         product_name_by_lang = task_details[0].product_name;
            //         break;
            //     }

            //     if (mail_arr.length > 0) {
            //       // var cust_status_mail = `<!DOCTYPE html>
            //       //   <html>
            //       //     <body>
            //       //       <p><em>Thank you for your request</em></p>
            //       //       <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
            //       //       <p></p>
            //       //       <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
            //       //       <p>We have received your request with the following details:</p>

            //       //       <table>
            //       //         <tr>
            //       //           <td>Task Ref</td>
            //       //           <td>${task_details[0].task_ref}</td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>User Name</td>
            //       //           <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>Request Type</td>
            //       //           <td>${task_details[0].req_name}</td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>Product</td>
            //       //           <td>${task_details[0].product_name}</td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>Market</td>
            //       //           <td>${country_name}</td>
            //       //         </tr>
            //       //           ${pharmacopoeia}
            //       //           ${samples}
            //       //           ${working_standards}
            //       //           ${impurities}
            //       //           ${entities.decode(shipping_address)}
            //       //         <tr>
            //       //           <td valign="top">Requirement</td>
            //       //           <td valign="top">${entities.decode(task_details[0].content)}</td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>&nbsp;</td>
            //       //           <td></td>
            //       //         </tr>
            //       //         <tr>
            //       //           <td>&nbsp;</td>
            //       //           <td></td>
            //       //         </tr>
            //       //       </table>
            //       //       <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
            //       //       <p>-- Dr.Reddys API team</p>
            //       //     </body>

            //       //     </html>`;

            //       // //console.log(cust_status_mail);
            //       // var mail_options1 = {
            //       //   from: Config.contactUsMail,
            //       //   to: mail_arr[0].to_arr,
            //       //   subject: mail_subject,
            //       //   html: cust_status_mail
            //       // };
            //       // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //       //   mail_options1.cc = mail_arr[1].cc_arr;
            //       // }
            //       // //console.log(mail_options1);
            //       // await common.sendMailB2C(mail_options1);

            //       let mailParam = {
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: product_name_by_lang,
            //         country_name: country_name_by_lang,
            //         task_content: entities.decode(task_details[0].content),
            //         url_status: url_status,
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: req_name_by_lang,
            //         pharmacopoeia: pharmacopoeia,
            //         samples: samples,
            //         working_standards: working_standards,
            //         impurities: impurities,
            //         shipping_address: entities.decode(shipping_address),
            //       };

            //       var mailcc = '';
            //       if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
            //         mailcc = mail_arr[1].cc_arr;
            //       }

            //       const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_SAMPLE', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");
            //     }

            //     // const mailOptions = {
            //     //   from: Config.webmasterMail, // sender address
            //     //   to: manager[0].email, // manager[0].email, // list of receivers
            //     //   subject: mail_subject,
            //     //   html: `<!DOCTYPE html>
            //     //     <html>
            //     //       <body>
            //     //         <p><em>New Task Assigned</em></p>
            //     //         <p>Hello ${manager[0].first_name},</p>
            //     //         <p>You have a new task assigned.</p>
            //     //         <p>Task Details are as below,</p>
            //     //         <table>
            //     //           <tr>
            //     //             <td>Task Ref</td>
            //     //             <td>${task_details[0].task_ref}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>User Name</td>
            //     //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Request Type</td>
            //     //             <td>${task_details[0].req_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Product</td>
            //     //             <td>${task_details[0].product_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Market</td>
            //     //             <td>${country_name}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>Due Date</td>
            //     //             <td>${format_due_date}</td>
            //     //           </tr>
            //     //             ${pharmacopoeia}
            //     //             ${samples}
            //     //             ${working_standards}
            //     //             ${impurities}
            //     //             ${entities.decode(shipping_address)}
            //     //           <tr>
            //     //             <td valign="top">Requirement</td>
            //     //             <td valign="top">${entities.decode(task_details[0].content)}</td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //           </tr>
            //     //           <tr>
            //     //             <td>&nbsp;</td>
            //     //             <td></td>
            //     //           </tr>
            //     //         </table>
            //     //         <p>
            //     //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
            //     //         </p>
            //     //       </body>
            //     //     </html>`
            //     // };
            //     // await common.sendMail(mailOptions);


            //     //SPOC MAIL
            //     let spocMailParam = {
            //       to_first_name: spoc[0].first_name,
            //       first_name: entities.decode(customer_details[0].first_name),
            //       last_name: entities.decode(customer_details[0].last_name),
            //       task_ref: task_details[0].task_ref,
            //       product_name: task_details[0].product_name,
            //       country_name: country_name,
            //       format_due_date: format_due_date,
            //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //       company_name: entities.decode(customer_details[0].company_name),
            //       req_name: task_details[0].req_name,
            //       ccpurl: Config.ccpUrl,
            //       pharmacopoeia: pharmacopoeia_translate,
            //       samples: samples_translate,
            //       working_standards: working_standards_translate,
            //       impurities: impurities_translate,
            //       shipping_address: entities.decode(shipping_address_translate),

            //     };
            //     const spocmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAMPLE', spoc[0].email, Config.webmasterMail, '', spocMailParam, "e");

            //     let managerMailParam = {
            //       to_first_name: manager[0].first_name,
            //       first_name: entities.decode(customer_details[0].first_name),
            //       last_name: entities.decode(customer_details[0].last_name),
            //       task_ref: task_details[0].task_ref,
            //       product_name: task_details[0].product_name,
            //       country_name: country_name,
            //       format_due_date: format_due_date,
            //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //       company_name: entities.decode(customer_details[0].company_name),
            //       req_name: task_details[0].req_name,
            //       ccpurl: Config.ccpUrl,
            //       pharmacopoeia: pharmacopoeia_translate,
            //       samples: samples_translate,
            //       working_standards: working_standards_translate,
            //       impurities: impurities_translate,
            //       shipping_address: entities.decode(shipping_address_translate),

            //     };
            //     const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAMPLE', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

            //     if (emp_details.length > 0) {
            //       // const mailOptions2 = {
            //       //   from: Config.webmasterMail, // sender address
            //       //   to: emp_details[0].email, //emp_details[0].email, // list of receivers
            //       //   subject: mail_subject,
            //       //   html: `<!DOCTYPE html>
            //       //     <html>
            //       //       <body>
            //       //         <p><em>New Task Assigned</em></p>
            //       //         <p>Hello ${emp_details[0].first_name},</p>
            //       //         <p>You have a new task assigned.</p>
            //       //         <p>Task Details are as below,</p>
            //       //         <table>
            //       //           <tr>
            //       //             <td>Task Ref</td>
            //       //             <td>${task_details[0].task_ref}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>User Name</td>
            //       //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>Request Type</td>
            //       //             <td>${task_details[0].req_name}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>Product</td>
            //       //             <td>${task_details[0].product_name}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>Market</td>
            //       //             <td>${country_name}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>Due Date</td>
            //       //             <td>${format_due_date}</td>
            //       //           </tr>
            //       //             ${pharmacopoeia}
            //       //             ${samples}
            //       //             ${working_standards}
            //       //             ${impurities}
            //       //             ${entities.decode(shipping_address)}
            //       //           <tr>
            //       //             <td valign="top">Requirement</td>
            //       //             <td valign="top">${entities.decode(task_details[0].content)}</td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>&nbsp;</td>
            //       //             <td></td>
            //       //           </tr>
            //       //           <tr>
            //       //             <td>&nbsp;</td>
            //       //             <td></td>
            //       //           </tr>
            //       //         </table>
            //       //         <p>
            //       //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
            //       //         </p>
            //       //       </body>
            //       //     </html>`
            //       // };
            //       // await common.sendMail(mailOptions2);
            //       let empMailParam = {
            //         to_first_name: emp_details[0].first_name,
            //         first_name: entities.decode(customer_details[0].first_name),
            //         last_name: entities.decode(customer_details[0].last_name),
            //         task_ref: task_details[0].task_ref,
            //         product_name: task_details[0].product_name,
            //         country_name: country_name,
            //         format_due_date: format_due_date,
            //         task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
            //         company_name: entities.decode(customer_details[0].company_name),
            //         req_name: task_details[0].req_name,
            //         ccpurl: Config.ccpUrl,
            //         pharmacopoeia: pharmacopoeia_translate,
            //         samples: samples_translate,
            //         working_standards: working_standards_translate,
            //         impurities: impurities_translate,
            //         shipping_address: entities.decode(shipping_address_translate),

            //       };
            //       const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAMPLE', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
            //     }

            //     //SEND MAIL

            //   } else {
            //     //Unallocated Task Soumyadeep
            //     var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
            //     common.logError(err);
            //   }



            // } else {

            manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

            if (manager.length > 0) {

              await taskModel.update_task_owner(manager[0].employee_id, task_id);

              //SEND MAIL
              let task_details = await taskModel.tasks_details(task_id);

              var customer_details = await customerModel.get_user(task_details[0].customer_id);

              var mail_subject = '';
              if (task_details[0].request_type == 24) {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
              } else {
                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
              }


              const assignment = {
                task_id: task_id,
                assigned_to: manager[0].employee_id,
                assigned_by: -1,
                assign_date: today,
                due_date: sla_due_date,
                reopen_date: today,
                parent_assignment_id: 0
              }
              let parent_assign_id = await taskModel.assign_task(assignment);

              // ACTIVITY LOG FROM SYSTEM - SATYAJIT
              var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
              var params = {
                task_ref: ref_no,
                request_type: request_type_id,
                employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`,
                employee_sender: 'SYSTEM'
              }
              await taskModel.log_activity(8, task_id, params);
              // END LOG

              let employee_notification = 22;
              let param_notification = {};
              let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

              let cc_arr = [
                employee_notification,
                task_id,
                manager[0].employee_id,
                common.currentDateTime(),
                0,
                notification_text,
                employee_notification,
                mail_subject
              ]
              await taskModel.notify_employee(cc_arr);

              //CSC TECHNICAL
              var emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 2);

              emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

              var bot_assignment_id = 0;

              if (emp_details.length > 0) {
                let assignment_val = 2;
                await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                  .then(async function (data) {

                    const assign_tasks = {
                      assigned_by: manager[0].employee_id,
                      assigned_to: emp_details[0].employee_id,
                      task_id: task_id,
                      due_date: sla_due_date,
                      assign_date: today,
                      parent_assignment_id: parent_assign_id.assignment_id,
                      comment: '',
                      reopen_date: today
                    }

                    await taskModel.assign_task(assign_tasks)
                      .then(async function (tdata) {

                        // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                        var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                        var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                        var params = {
                          task_ref: ref_no,
                          request_type: request_type_id,
                          employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                          employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                        }
                        await taskModel.log_activity(8, task_id, params)
                        // END LOG

                        bot_assignment_id = tdata.assignment_id;

                        let employee_notification = 8;
                        let param_notification = {
                          employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                          assigned_employee: `you`
                        };
                        let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                        let cc_arr = [
                          employee_notification,
                          task_id,
                          emp_details[0].employee_id,
                          common.currentDateTime(),
                          0,
                          notification_text,
                          employee_notification,
                          mail_subject
                        ]
                        await taskModel.notify_employee(cc_arr);

                        const task_comment = {
                          posted_by: manager[0].employee_id,
                          task_id: task_id,
                          comment: `This task has been auto assigned to ${emp_recipient} ${emp_details[0].desig_name} by the system on my behalf.`,
                          assignment_id: bot_assignment_id,
                          post_date: today
                        };

                        await taskModel.insert_comment_log(task_comment);

                        comment_arr.push({ comment: task_comment.comment, posted_by: task_comment.posted_by });

                        //console.log('child assignment',bot_assignment_id);

                      }).catch(err => {
                        common.logError(err);
                        res.status(400)
                          .json({
                            status: 3,
                            message: Config.errorText.value
                          }).end();
                      });

                  }).catch(err => {
                    common.logError(err);
                    res.status(400)
                      .json({
                        status: 3,
                        message: Config.errorText.value
                      }).end();
                  })
              } else {
                bot_assignment_id = parent_assign_id.assignment_id;
              }

              //CURRENT OWNER
              if (emp_details.length > 0) {
                await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
              } else {
                await taskModel.update_current_owner(task_id, manager[0].employee_id);
              }
              //CURRENT OWNER

              //INSERT CC CUSTOMERS
              if (taskObj.cc_customers) {
                let cc_cust_arr = JSON.parse(taskObj.cc_customers);
                //INSERT CC CUSTOMERS
                if (cc_cust_arr.length > 0) {
                  for (let index = 0; index < cc_cust_arr.length; index++) {
                    const element = cc_cust_arr[index].customer_id;
                    let insrt_cc = {
                      customer_id: element,
                      task_id: task_id,
                      status: 1
                    };
                    await customerModel.insert_cc_customer(insrt_cc);
                  }
                }
              }

              //HIGHLIGHT NEW TASK
              let customer_id = taskObj.customer_id;

              let highlight_arr = [
                task_id,
                customer_id,
                1,
                'C'
              ];
              await taskModel.task_highlight(highlight_arr);

              let cc_list = await taskModel.task_cc_customer(task_id);
              if (cc_list && cc_list.length > 0) {
                for (let index = 0; index < cc_list.length; index++) {
                  const element = cc_list[index];
                  let highlight_cc_arr = [
                    task_id,
                    element.customer_id,
                    1,
                    'C'
                  ];
                  await taskModel.task_highlight(highlight_cc_arr);
                }
              }

              if (taskObj.share_with_agent == 1) {
                let customer_details = await customerModel.get_user(taskObj.customer_id);
                let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

                if (agent_list && agent_list.length > 0) {
                  for (let index = 0; index < agent_list.length; index++) {
                    const element = agent_list[index];
                    let highlight_agent_arr = [
                      task_id,
                      element.agent_id,
                      1,
                      'A'
                    ];
                    await taskModel.task_highlight(highlight_agent_arr);
                  }
                }
              }



              var pharmacopoeia, shipping_address, samples, impurities, working_standards = '';
              var pharmacopoeia_translate, shipping_address_translate, samples_translate, impurities_translate, working_standards_translate = '';

              var request_type = task_details[0].request_type;

              var req_type_arr = task_details[0].service_request_type.split(',');

              if (req_type_arr.indexOf('Samples') !== -1) {
                samples = `<tr><td>${await common.getTranslatedTextMail('Samples', language)}</td><td></td></tr>`;
                samples_translate = `<tr><td>${await common.getTranslatedTextMail('Samples', Config.default_language)}</td><td></td></tr>`;

                if (task_details[0].number_of_batches != null) {
                  samples += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', language)}</td><td>${task_details[0].number_of_batches}</td></tr>`;

                  samples_translate += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', Config.default_language)}</td><td>${task_details[0].number_of_batches}</td></tr>`;

                } else {
                  samples += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', language)}</td><td></td></tr>`;
                  samples_translate += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', Config.default_language)}</td><td></td></tr>`;
                }

                if (task_details[0].quantity != null) {

                  let quantity_by_lang = task_details[0].quantity;
                  samples_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
                  if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
                    let q_arr = task_details[0].quantity.split(' ');
                    let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
                    quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
                  }

                  samples += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td>${quantity_by_lang}</td></tr>`;
                } else {
                  samples += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td></td></tr>`;
                  samples_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td></td></tr>`;
                }
              } else {
                samples = '';
                samples_translate = '';
              }

              if (req_type_arr.indexOf('Working Standards') !== -1) {
                working_standards = `<tr><td>${await common.getTranslatedTextMail('Working Standards', language)}</td><td></td></tr>`;
                working_standards_translate = `<tr><td>${await common.getTranslatedTextMail('Working Standards', Config.default_language)}</td><td></td></tr>`;

                if (task_details[0].working_quantity != null) {

                  let quantity_by_lang = task_details[0].working_quantity;
                  working_standards_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
                  if (task_details[0].working_quantity && trim(task_details[0].working_quantity) != '' && task_details[0].language != Config.default_language) {
                    let q_arr = task_details[0].working_quantity.split(' ');
                    let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
                    quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
                  }

                  working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td>${quantity_by_lang}</td></tr>`;
                } else {
                  working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td></td></tr>`;
                  working_standards_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td></td></tr>`;
                }

              } else {
                working_standards = '';
                working_standards_translate = '';
              }

              if (req_type_arr.indexOf('Impurities') !== -1) {
                impurities = `<tr><td>${await common.getTranslatedTextMail('Impurities', language)}</td><td></td></tr>`;
                impurities_translate = `<tr><td>${await common.getTranslatedTextMail('Impurities', Config.default_language)}</td><td></td></tr>`;

                if (task_details[0].impurities_quantity != null) {

                  let quantity_by_lang = task_details[0].impurities_quantity;
                  impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td>${quantity_by_lang}</td></tr>`;
                  if (task_details[0].impurities_quantity && trim(task_details[0].impurities_quantity) != '' && task_details[0].language != Config.default_language) {
                    let q_arr = task_details[0].impurities_quantity.split(' ');
                    let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
                    quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
                  }

                  impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td>${quantity_by_lang}</td></tr>`;
                } else {
                  impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity', language)}</td><td></td></tr>`;
                  impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Quantity', Config.default_language)}</td><td></td></tr>`;
                }

                if (task_details[0].specify_impurity_required != null) {
                  impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', language)}</td><td>${task_details[0].specify_impurity_required}</td></tr>`;
                  impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', Config.default_language)}</td><td>${task_details[0].specify_impurity_required}</td></tr>`;
                } else {
                  impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', language)}</td><td></td></tr>`;
                  impurities_translate += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', Config.default_language)}</td><td></td></tr>`;
                }

              } else {
                impurities = '';
                impurities_translate = '';
              }

              shipping_address = `<tr><td>${await common.getTranslatedTextMail('Shipping Address', language)}</td><td>${task_details[0].shipping_address}</td></tr>`;
              shipping_address_translate = `<tr><td>${await common.getTranslatedTextMail('Shipping Address', Config.default_language)}</td><td>${task_details[0].shipping_address_translate}</td></tr>`;

              pharmacopoeia = `<tr><td>${await common.getTranslatedTextMail('Pharmacopoeia', language)}</td><td>${task_details[0].pharmacopoeia}</td></tr>`;
              pharmacopoeia_translate = `<tr><td>${await common.getTranslatedTextMail('Pharmacopoeia', Config.default_language)}</td><td>${task_details[0].pharmacopoeia_translate}</td></tr>`;

              var country = await taskModel.tasks_details_countries(task_id);

              if (country && country[0].country_name != '') {
                var country_name = country[0].country_name;
              } else {
                var country_name = '';
              }

              var db_due_date = new Date(task_details[0].due_date);
              var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

              let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

              var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

              var country_name_by_lang = country_name;
              if (language != Config.default_language) {
                //COUNTRY
                let t_country = await taskModel.get_translated_country(task_id, language);
                country_name_by_lang = t_country[0].country_name;
              }

              var req_name_by_lang = task_details[0].req_name;
              var product_name_by_lang = task_details[0].product_name;
              switch (entities.decode(task_details[0].language)) {
                case 'es':
                  req_name_by_lang = task_details[0].req_name_es;
                  product_name_by_lang = task_details[0].product_name_es;
                  break;
                case 'pt':
                  req_name_by_lang = task_details[0].req_name_pt;
                  product_name_by_lang = task_details[0].product_name_pt;
                  break;
                case 'ja':
                  req_name_by_lang = task_details[0].req_name_ja;
                  product_name_by_lang = task_details[0].product_name_ja;
                  break;
                case 'zh':
                  req_name_by_lang = task_details[0].req_name_zh;
                  product_name_by_lang = task_details[0].product_name_zh;
                  break;
                default:
                  req_name_by_lang = task_details[0].req_name;
                  product_name_by_lang = task_details[0].product_name;
                  break;
              }
              if (product_name_by_lang == '' || product_name_by_lang == null) {
                product_name_by_lang = task_details[0].product_name;
              }

              if (mail_arr.length > 0) {
                // var cust_status_mail = `<!DOCTYPE html>
                //   <html>
                //     <body>
                //       <p><em>Thank you for your request</em></p>
                //       <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
                //       <p></p>
                //       <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
                //       <p>We have received your request with the following details:</p>

                //       <table>
                //         <tr>
                //           <td>Task Ref</td>
                //           <td>${task_details[0].task_ref}</td>
                //         </tr>
                //         <tr>
                //           <td>User Name</td>
                //           <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                //         </tr>
                //         <tr>
                //           <td>Request Type</td>
                //           <td>${task_details[0].req_name}</td>
                //         </tr>
                //         <tr>
                //           <td>Product</td>
                //           <td>${task_details[0].product_name}</td>
                //         </tr>
                //         <tr>
                //           <td>Market</td>
                //           <td>${country_name}</td>
                //         </tr>
                //           ${pharmacopoeia}
                //           ${samples}
                //           ${working_standards}
                //           ${impurities}
                //           ${entities.decode(shipping_address)}
                //         <tr>
                //           <td valign="top">Requirement</td>
                //           <td valign="top">${entities.decode(task_details[0].content)}</td>
                //         </tr>
                //         <tr>
                //           <td>&nbsp;</td>
                //           <td></td>
                //         </tr>
                //         <tr>
                //           <td>&nbsp;</td>
                //           <td></td>
                //         </tr>
                //       </table>
                //       <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
                //       <p>-- Dr.Reddys API team</p>
                //     </body>

                //     </html>`;

                // //console.log(cust_status_mail);
                // var mail_options1 = {
                //   from: Config.contactUsMail,
                //   to: mail_arr[0].to_arr,
                //   subject: mail_subject,
                //   html: cust_status_mail
                // };
                // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                //   mail_options1.cc = mail_arr[1].cc_arr;
                // }
                // //console.log(mail_options1);
                // await common.sendMailB2C(mail_options1);

                let mailParam1 = {
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: product_name_by_lang,
                  country_name: country_name_by_lang,
                  task_content: entities.decode(task_details[0].content),
                  url_status: url_status,
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: req_name_by_lang,
                  pharmacopoeia: pharmacopoeia,
                  samples: samples,
                  working_standards: working_standards,
                  impurities: impurities,
                  shipping_address: entities.decode(shipping_address),
                };

                var mailcc = '';
                if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                  mailcc = mail_arr[1].cc_arr;
                }

                const mailSent1 = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_SAMPLE', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam1, "c");
              }

              // const mailOptions = {
              //   from: Config.webmasterMail, // sender address
              //   to: manager[0].email, // manager[0].email, // list of receivers
              //   subject: mail_subject,
              //   html: `<!DOCTYPE html>
              //     <html>
              //       <body>
              //         <p><em>New Task Assigned</em></p>
              //         <p>Hello ${manager[0].first_name},</p>
              //         <p>You have a new task assigned.</p>
              //         <p>Task Details are as below,</p>
              //         <table>
              //           <tr>
              //             <td>Task Ref</td>
              //             <td>${task_details[0].task_ref}</td>
              //           </tr>
              //           <tr>
              //             <td>User Name</td>
              //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Request Type</td>
              //             <td>${task_details[0].req_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Product</td>
              //             <td>${task_details[0].product_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Market</td>
              //             <td>${country_name}</td>
              //           </tr>
              //           <tr>
              //             <td>Due Date</td>
              //             <td>${format_due_date}</td>
              //           </tr>
              //             ${pharmacopoeia}
              //             ${samples}
              //             ${working_standards}
              //             ${impurities}
              //             ${entities.decode(shipping_address)}
              //           <tr>
              //             <td valign="top">Requirement</td>
              //             <td valign="top">${entities.decode(task_details[0].content)}</td>
              //           </tr>
              //           <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //           </tr>
              //           <tr>
              //             <td>&nbsp;</td>
              //             <td></td>
              //           </tr>
              //         </table>
              //         <p>
              //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
              //         </p>
              //       </body>
              //     </html>`
              // };
              // await common.sendMail(mailOptions);
              let managerMailParam1 = {
                to_first_name: manager[0].first_name,
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: task_details[0].task_ref,
                product_name: task_details[0].product_name,
                country_name: country_name,
                format_due_date: format_due_date,
                task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                pharmacopoeia: pharmacopoeia_translate,
                samples: samples_translate,
                working_standards: working_standards_translate,
                impurities: impurities_translate,
                shipping_address: entities.decode(shipping_address_translate),

              };
              const managermailSent1 = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAMPLE', manager[0].email, Config.webmasterMail, '', managerMailParam1, "e");

              if (emp_details.length > 0) {
                // const mailOptions2 = {
                //   from: Config.webmasterMail, // sender address
                //   to: emp_details[0].email, //emp_details[0].email, // list of receivers
                //   subject: mail_subject,
                //   html: `<!DOCTYPE html>
                //     <html>
                //       <body>
                //         <p><em>New Task Assigned</em></p>
                //         <p>Hello ${emp_details[0].first_name},</p>
                //         <p>You have a new task assigned.</p>
                //         <p>Task Details are as below,</p>
                //         <table>
                //           <tr>
                //             <td>Task Ref</td>
                //             <td>${task_details[0].task_ref}</td>
                //           </tr>
                //           <tr>
                //             <td>User Name</td>
                //             <td>${customer_details[0].first_name} ${customer_details[0].last_name}</td>
                //           </tr>
                //           <tr>
                //             <td>Request Type</td>
                //             <td>${task_details[0].req_name}</td>
                //           </tr>
                //           <tr>
                //             <td>Product</td>
                //             <td>${task_details[0].product_name}</td>
                //           </tr>
                //           <tr>
                //             <td>Market</td>
                //             <td>${country_name}</td>
                //           </tr>
                //           <tr>
                //             <td>Due Date</td>
                //             <td>${format_due_date}</td>
                //           </tr>
                //             ${pharmacopoeia}
                //             ${samples}
                //             ${working_standards}
                //             ${impurities}
                //             ${entities.decode(shipping_address)}
                //           <tr>
                //             <td valign="top">Requirement</td>
                //             <td valign="top">${entities.decode(task_details[0].content)}</td>
                //           </tr>
                //           <tr>
                //             <td>&nbsp;</td>
                //             <td></td>
                //           </tr>
                //           <tr>
                //             <td>&nbsp;</td>
                //             <td></td>
                //           </tr>
                //         </table>
                //         <p>
                //           Click <a href="${Config.ccpUrl}/user/dashboard">here</a> to see more details about this query
                //         </p>
                //       </body>
                //     </html>`
                // };
                // await common.sendMail(mailOptions2);
                let empMailParam1 = {
                  to_first_name: emp_details[0].first_name,
                  first_name: entities.decode(customer_details[0].first_name),
                  last_name: entities.decode(customer_details[0].last_name),
                  task_ref: task_details[0].task_ref,
                  product_name: task_details[0].product_name,
                  country_name: country_name,
                  format_due_date: format_due_date,
                  task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
                  company_name: entities.decode(customer_details[0].company_name),
                  req_name: task_details[0].req_name,
                  ccpurl: Config.ccpUrl,
                  pharmacopoeia: pharmacopoeia_translate,
                  samples: samples_translate,
                  working_standards: working_standards_translate,
                  impurities: impurities_translate,
                  shipping_address: entities.decode(shipping_address_translate),

                };
                const empmailSent1 = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAMPLE', emp_details[0].email, Config.webmasterMail, '', empMailParam1, "e");
              }

              //SEND MAIL

            } else {
              //Unallocated Task Soumyadeep
              var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
              common.logError(err);
            }

            //}

            var files = req.files;
            if (files && files.length > 0) {

              for (var j = 0; j < files.length; j++) {

                let file_details = {
                  task_id: task_id,
                  actual_file_name: files[j].originalname,
                  new_file_name: files[j].filename,
                  date_added: today
                }

                await taskModel.add_task_file(file_details);
              }
            }
            task_arr.push({
              task_id: task_id,
              files: files,
              comment_arr: comment_arr
            });
            product_counter++;
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          });

      }

      //SALES FORCE TASK
      if (Config.activate_sales_force == true) {
        for (let index = 0; index < task_arr.length; index++) {
          const element = task_arr[index];
          await module.exports.__post_sales_force_add_task(element.task_id, element.files, req.user.empe);
          if (element.comment_arr != '') {

            for (let index = 0; index < element.comment_arr.length; index++) {
              const element_diss = element.comment_arr[index];
              await module.exports.__post_sales_force_internal_discussions(element.task_id, [], element_diss.comment, element_diss.posted_by);
            }

          }
        }
      }

      res.status(200).json({
        status: '1',
        message: "success"
      }).end();

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Create Forecast
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Create Forecast used for Forecast selection
   */
  create_forecast: async (req, res, next) => {
    try {

      var today = common.currentDateTime();

      var request_id = 24;

      if (req.user.role == 2) {
        var submitted_by = req.user.customer_id;
        var customer_id = req.body.agent_customer_id;
      } else {
        var submitted_by = 0;
        var customer_id = req.user.customer_id;
      }

      var reference_key = 'F';

      var sla = await taskModel.get_total_sla(request_id);

      var days = await common.dayCountExcludingWeekends(sla.total_sla);

      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);

        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }

      const {
        description,
        request_category_type,
        cc_customers,
        share_with_agent
      } = req.body;

      let language = '';
      if (req.user.empe > 0) {
        language = await customerModel.get_customer_language(customer_id);
      } else {
        language = await customerModel.get_customer_language(customer_id);
      }

      const forecast_obj = {
        customer_id: customer_id,
        request_category_type: request_category_type,
        parent_id: 0,
        due_date: sla_due_date,
        current_date: today,
        discussion: 0,
        description: entities.encode(description),
        request_type_id: request_id,

        submitted_by: submitted_by,
        close_status: 0,
        drupal_task_id: 0,
        priority: 2,
        rdd: sla_due_date,
        front_due_date: front_due_date,
        cc_customers: cc_customers,
        share_with_agent: share_with_agent,
        language: language
      }

      forecast_obj.ccp_posted_by = (req.user.empe > 0) ? +req.user.empe : 0;

      await taskModel.add_forecast(forecast_obj)
        .then(async function (data) {

          //let task_ref = await taskModel.update_task_ref(data.task_id);
          let manager = await taskModel.get_allocated_manager(forecast_obj.customer_id);

          let task_id = data.task_id;

          if (language != Config.default_language) {
            await module.exports.__translate_language(forecast_obj, task_id, '');
          }

          let ref_no = 'PHL-' + reference_key + '-' + task_id;
          let customerData = [];
          await taskModel.update_ref_no(ref_no, task_id)
            .then(async () => {
              // ACTIVITY LOG - SATYAJIT
              customerData = await customerModel.get_user(forecast_obj.customer_id);
              var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
              var params = {
                task_ref: ref_no,
                request_type: 'Forecast',
                customer_name: customername
              }
              if (req.user.role == 2) {
                var agentData = await agentModel.get_user(submitted_by);
                params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                await taskModel.log_activity(2, task_id, params)
              } else {
                await taskModel.log_activity(1, task_id, params)
              }
              // END LOG
            });
          //console.log(manager);

          // let spoc_exists = false;
          // let spoc = [];
          // if (language != Config.default_language && customerData[0].ignore_spoc == 2) {
          //   let assigned_spoc = await taskModel.get_allocated_spoc(forecast_obj.customer_id, language);
          //   assigned_spoc = await module.exports.__get_allocated_leave_employee_spoc(assigned_spoc, forecast_obj.customer_id);
          //   if (assigned_spoc.length > 0) {
          //     spoc_exists = true;
          //     spoc = assigned_spoc;
          //   } else if (await taskModel.check_language_spoc(language)) {
          //     spoc_exists = true;
          //     global_spoc = await taskModel.get_global_spoc(language);
          //     spoc = await module.exports.__get_allocated_leave_employee_spoc(global_spoc, forecast_obj.customer_id);
          //   }
          // }

          let comment_arr = [];

          // if (spoc_exists && spoc.length > 0) {

          //   const assignment_spoc = {
          //     task_id: task_id,
          //     assigned_to: spoc[0].employee_id,
          //     assigned_by: -1,
          //     assign_date: today,
          //     due_date: sla_due_date,
          //     reopen_date: today,
          //     parent_assignment_id: 0
          //   }

          //   var spoc_parent_assign_id = await taskModel.assign_task(assignment_spoc).catch(err => {
          //     common.logError(err);
          //     res.status(400)
          //       .json({
          //         status: 3,
          //         message: Config.errorText.value
          //       }).end();
          //   });

          //   // ACTIVITY LOG FROM SYSTEM - SATYAJIT
          //   var spoc_recipient = `${spoc[0].first_name} ${spoc[0].last_name}`;
          //   var params = {
          //     task_ref: ref_no,
          //     request_type: 'Forecast',
          //     employee_sender: 'SYSTEM',
          //     employee_recipient: spoc_recipient + ` (${(spoc[0].desig_name)})`
          //   }
          //   await taskModel.log_activity(8, task_id, params);
          //   // END LOG

          //   let employee_notification = 22;
          //   let param_notification = {};
          //   let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

          //   let cc_arr = [
          //     employee_notification,
          //     task_id,
          //     spoc[0].employee_id,
          //     common.currentDateTime(),
          //     0,
          //     notification_text,
          //     employee_notification,
          //     mail_subject
          //   ]
          //   await taskModel.notify_employee(cc_arr);

          //   manager = await module.exports.__get_allocated_leave_employee(manager, forecast_obj.customer_id);

          //   if (manager.length > 0) {

          //     let assignment_val = 2;
          //     await taskModel.update_task_assignment(task_id, spoc[0].employee_id, assignment_val);

          //     let task_details = await taskModel.tasks_details(data.task_id);

          //     var customer_details = await customerModel.get_user(task_details[0].customer_id);

          //     var mail_subject = '';
          //     if (task_details[0].request_type == 24) {
          //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
          //     } else {
          //       mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
          //     }

          //     const assignment = {
          //       task_id: task_id,
          //       assigned_to: manager[0].employee_id,
          //       assigned_by: spoc[0].employee_id,
          //       assign_date: today,
          //       due_date: sla_due_date,
          //       reopen_date: today,
          //       parent_assignment_id: spoc_parent_assign_id.assignment_id
          //     }
          //     let parent_assign_id = await taskModel.assign_task(assignment);

          //     // ACTIVITY LOG FROM SYSTEM - SATYAJIT
          //     var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
          //     var params = {
          //       task_ref: ref_no,
          //       request_type: 'Forecast',
          //       employee_sender: spoc_recipient + ` (${(spoc[0].desig_name)})`,
          //       employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
          //     }
          //     await taskModel.log_activity(8, task_id, params);
          //     // END LOG

          //     let employee_notification = 8;
          //     let param_notification = {
          //       employee_name: `${spoc[0].first_name} (${(spoc[0].desig_name)})`,
          //       assigned_employee: `you`
          //     };
          //     let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

          //     let cc_arr = [
          //       employee_notification,
          //       task_id,
          //       manager[0].employee_id,
          //       common.currentDateTime(),
          //       0,
          //       notification_text,
          //       employee_notification,
          //       mail_subject
          //     ]
          //     await taskModel.notify_employee(cc_arr);

          //     let task_comment = {
          //       posted_by: spoc[0].employee_id,
          //       task_id: task_id,
          //       comment: `This task has been auto assigned to ${manager[0].first_name} ${manager[0].last_name} (${(manager[0].desig_name)}) by the system on my behalf.`,
          //       assignment_id: spoc_parent_assign_id.assignment_id,
          //       post_date: today
          //     };

          //     await taskModel.insert_comment_log(task_comment);

          //     comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

          //     //CURRENT OWNER
          //     await taskModel.update_current_owner(data.task_id, manager[0].employee_id);
          //     //CURRENT OWNER

          //     await taskModel.update_task_owner(manager[0].employee_id, data.task_id);
          //     //console.log(manager[0].employee_id);

          //     //Xceed bot -- satyajit
          //     // if( (Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0 ){
          //     //   const request = require('request');
          //     //   var options = {
          //     //     method: 'POST',
          //     //     url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
          //     //     headers:
          //     //     {'x-api-key': 'XAmdVeqheisEoJ1SROQG'}
          //     //   };

          //     //   await request(options, async function (error, response, body) {
          //     //     if (error){
          //     //         var mailOptions = {
          //     //               from: Config.webmasterMail,
          //     //               to: ['soumyadeep@indusnet.co.in','satyajit.mondol@indusnet.co.in'],
          //     //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
          //     //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
          //     //             };
          //     //         await common.sendMail(mailOptions);
          //     //         await common.logErrorText('Bot entry failed create_forecast',JSON.stringify(error));

          //     //     } else {                 
          //     //         if(response.statusCode != 200){                 
          //     //             var mailOptions = {
          //     //               from: Config.webmasterMail,
          //     //               to: ['soumyadeep@indusnet.co.in','satyajit.mondol@indusnet.co.in'],
          //     //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
          //     //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
          //     //             };
          //     //             await common.sendMail(mailOptions);
          //     //             await common.logErrorText('Bot entry failed create_forecast',JSON.stringify(response));
          //     //         }
          //     //     }
          //     //   });
          //     // }
          //     //end 

          //     /*SLA COUNT*/
          //     // const sla_arr = {
          //     //   task_id    : task_id,
          //     //   employee_id: manager[0].employee_id,
          //     //   due_date   : sla_due_date
          //     // };
          //     // await taskModel.insert_sla(sla_arr);

          //     //INSERT CC CUSTOMERS
          //     if (forecast_obj.cc_customers) {
          //       let cc_cust_arr = JSON.parse(forecast_obj.cc_customers);
          //       //INSERT CC CUSTOMERS
          //       if (cc_cust_arr.length > 0) {
          //         for (let index = 0; index < cc_cust_arr.length; index++) {
          //           const element = cc_cust_arr[index].customer_id;
          //           let insrt_cc = {
          //             customer_id: element,
          //             task_id: task_id,
          //             status: 1
          //           };
          //           await customerModel.insert_cc_customer(insrt_cc);
          //         }
          //       }
          //     }



          //     //HIGHLIGHT NEW TASK
          //     let customer_id = forecast_obj.customer_id;

          //     let highlight_arr = [
          //       task_id,
          //       customer_id,
          //       1,
          //       'C'
          //     ];
          //     await taskModel.task_highlight(highlight_arr);

          //     let cc_list = await taskModel.task_cc_customer(task_id);
          //     if (cc_list && cc_list.length > 0) {
          //       for (let index = 0; index < cc_list.length; index++) {
          //         const element = cc_list[index];
          //         let highlight_cc_arr = [
          //           task_id,
          //           element.customer_id,
          //           1,
          //           'C'
          //         ];
          //         await taskModel.task_highlight(highlight_cc_arr);
          //       }
          //     }


          //     if (forecast_obj.share_with_agent == 1) {
          //       let customer_details = await customerModel.get_user(forecast_obj.customer_id);
          //       let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

          //       if (agent_list && agent_list.length > 0) {
          //         for (let index = 0; index < agent_list.length; index++) {
          //           const element = agent_list[index];
          //           let highlight_agent_arr = [
          //             task_id,
          //             element.agent_id,
          //             1,
          //             'A'
          //           ];
          //           await taskModel.task_highlight(highlight_agent_arr);
          //         }
          //       }
          //     }

          //     var db_due_date = new Date(task_details[0].due_date);
          //     var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

          //     let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

          //     var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;



          //     if (mail_arr.length > 0) {

          //       // var cust_status_mail = `<!DOCTYPE html>
          //       // <html>
          //       //   <body>
          //       //     <p><em>Thank you for your request</em></p>
          //       //     <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
          //       //     <p></p>
          //       //     <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
          //       //     <p>We have received your request with the following details:</p>
          //       //     <table>
          //       //       <tr>
          //       //         <td>Task Ref</td>
          //       //         <td>${task_details[0].task_ref}</td>
          //       //       </tr>
          //       //       <tr>
          //       //         <td>Customer Name</td>
          //       //         <td>${task_details[0].first_name} ${task_details[0].last_name}</td>
          //       //       </tr>
          //       //       <tr>
          //       //         <td>Request Type</td>
          //       //         <td>${task_details[0].req_name}</td>
          //       //       </tr>
          //       //       <tr>
          //       //         <td valign="top">Requirement</td>
          //       //         <td valign="top">${entities.decode(task_details[0].content)}</td>
          //       //       </tr>
          //       //       <tr>
          //       //         <td>&nbsp;</td>
          //       //         <td></td>
          //       //       </tr>
          //       //       <tr>
          //       //         <td>&nbsp;</td>
          //       //         <td></td>
          //       //       </tr>
          //       //     </table>
          //       //     <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
          //       //     <p>-- Dr.Reddys API team</p>
          //       //   </body>
          //       // </html>`;

          //       // //console.log(cust_status_mail);
          //       // var mail_options1 = {
          //       //   from: Config.contactUsMail,
          //       //   to: mail_arr[0].to_arr,
          //       //   subject: mail_subject,
          //       //   html: cust_status_mail
          //       // };
          //       // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
          //       //   mail_options1.cc = mail_arr[1].cc_arr;
          //       // }
          //       // //console.log(mail_options1);
          //       // common.sendMailB2C(mail_options1);]
          //       var language_extn = 'req_name_' + task_details[0].language;
          //       let req_name_lang = task_details[0].req_name;

          //       switch (entities.decode(task_details[0].language)) {
          //         case 'es':
          //           req_name_lang = task_details[0].req_name_es;
          //           break;
          //         case 'pt':
          //           req_name_lang = task_details[0].req_name_pt;
          //           break;
          //         case 'ja':
          //           req_name_lang = task_details[0].req_name_ja;
          //           break;
          //         case 'zh':
          //           req_name_lang = task_details[0].req_name_zh;
          //           break;
          //         default:
          //           req_name_lang = task_details[0].req_name;
          //           break;
          //       }

          //       let mailParam = {
          //         first_name: entities.decode(customer_details[0].first_name),
          //         last_name: entities.decode(customer_details[0].last_name),
          //         task_ref: task_details[0].task_ref,
          //         task_content: entities.decode(task_details[0].content),
          //         url_status: url_status,
          //         company_name: entities.decode(customer_details[0].company_name),
          //         req_name: req_name_lang
          //       };

          //       var mailcc = '';
          //       if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
          //         mailcc = mail_arr[1].cc_arr;
          //       }
          //       console.log(task_details);
          //       console.log(mailParam);

          //       const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_FORCAST', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");

          //     }

          //     //Manager Mail    

          //     let spocMailParam = {
          //       first_name: entities.decode(customer_details[0].first_name),
          //       last_name: entities.decode(customer_details[0].last_name),
          //       task_ref: task_details[0].task_ref,
          //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
          //       company_name: entities.decode(customer_details[0].company_name),
          //       format_due_date: format_due_date,
          //       ccpurl: Config.ccpUrl,
          //       emp_first_name: spoc[0].first_name,
          //       req_name: task_details[0].req_name
          //     };

          //     const spocrmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_FORCAST', spoc[0].email, Config.webmasterMail, '', spocMailParam, "e");

          //     let managerMailParam = {
          //       first_name: entities.decode(customer_details[0].first_name),
          //       last_name: entities.decode(customer_details[0].last_name),
          //       task_ref: task_details[0].task_ref,
          //       task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
          //       company_name: entities.decode(customer_details[0].company_name),
          //       format_due_date: format_due_date,
          //       ccpurl: Config.ccpUrl,
          //       emp_first_name: manager[0].first_name,
          //       req_name: task_details[0].req_name
          //     };

          //     const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_FORCAST', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

          //     // const mailOptions = {
          //     //   from: Config.webmasterMail,
          //     //   to: manager[0].email,
          //     //   subject: mail_subject,
          //     //   html: `<!DOCTYPE html>
          //     //   <html>
          //     //     <body>
          //     //       <p><em>New Task Assigned</em></p>
          //     //       <p>Hello ${manager[0].first_name},</p>
          //     //       <p>You have a new task assigned.</p>
          //     //       <p>Task Details are as below,</p>
          //     //       <table>
          //     //         <tr>
          //     //           <td>Task Ref</td>
          //     //           <td>${task_details[0].task_ref}</td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td>Customer Name</td>
          //     //           <td>${task_details[0].first_name} ${task_details[0].last_name}</td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td>Request Type</td>
          //     //           <td>${task_details[0].req_name}</td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td>Due Date</td>
          //     //           <td>${format_due_date}</td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td valign="top">Requirement</td>
          //     //           <td valign="top">${entities.decode(task_details[0].content)}</td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td>&nbsp;</td>
          //     //           <td></td>
          //     //         </tr>
          //     //         <tr>
          //     //           <td>&nbsp;</td>
          //     //           <td></td>
          //     //         </tr>
          //     //       </table>
          //     //       <p>
          //     //         Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
          //     //       </p>
          //     //     </body>
          //     //   </html>`,
          //     // };
          //     // common.sendMail(mailOptions);
          //   } else {
          //     //Unallocated Task Soumyadeep
          //     var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${forecast_obj.customer_id}`);
          //     common.logError(err);
          //   }

          // } else {

          manager = await module.exports.__get_allocated_leave_employee(manager, forecast_obj.customer_id);

          if (manager.length > 0) {

            let task_details = await taskModel.tasks_details(data.task_id);

            var customer_details = await customerModel.get_user(task_details[0].customer_id);

            var mail_subject = '';
            if (task_details[0].request_type == 24) {
              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
            } else {
              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
            }

            // ACTIVITY LOG FROM SYSTEM - SATYAJIT
            var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
            var params = {
              task_ref: ref_no,
              request_type: 'Forecast',
              employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`,
              employee_sender: 'SYSTEM'
            }
            await taskModel.log_activity(8, task_id, params);
            // END LOG


            let employee_notification = 22;
            let param_notification = {};
            let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

            let cc_arr = [
              employee_notification,
              task_id,
              manager[0].employee_id,
              common.currentDateTime(),
              0,
              notification_text,
              employee_notification,
              mail_subject
            ]
            await taskModel.notify_employee(cc_arr);

            //CURRENT OWNER
            await taskModel.update_current_owner(data.task_id, manager[0].employee_id);
            //CURRENT OWNER

            await taskModel.update_task_owner(manager[0].employee_id, data.task_id);
            //console.log(manager[0].employee_id);
            const assignment = {
              task_id: data.task_id,
              assigned_to: manager[0].employee_id,
              assigned_by: -1,
              assign_date: today,
              due_date: sla_due_date,
              reopen_date: today,
              parent_assignment_id: 0
            }
            var bot_assignment_id = await taskModel.assign_task(assignment);

            //Xceed bot -- satyajit
            // if( (Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0 ){
            //   const request = require('request');
            //   var options = {
            //     method: 'POST',
            //     url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
            //     headers:
            //     {'x-api-key': 'XAmdVeqheisEoJ1SROQG'}
            //   };

            //   await request(options, async function (error, response, body) {
            //     if (error){
            //         var mailOptions = {
            //               from: Config.webmasterMail,
            //               to: ['soumyadeep@indusnet.co.in','satyajit.mondol@indusnet.co.in'],
            //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //             };
            //         await common.sendMail(mailOptions);
            //         await common.logErrorText('Bot entry failed create_forecast',JSON.stringify(error));

            //     } else {                 
            //         if(response.statusCode != 200){                 
            //             var mailOptions = {
            //               from: Config.webmasterMail,
            //               to: ['soumyadeep@indusnet.co.in','satyajit.mondol@indusnet.co.in'],
            //               subject: `URL || ${Config.drupal.url} || Bot entry failed`,
            //               html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
            //             };
            //             await common.sendMail(mailOptions);
            //             await common.logErrorText('Bot entry failed create_forecast',JSON.stringify(response));
            //         }
            //     }
            //   });
            // }
            //end 

            /*SLA COUNT*/
            // const sla_arr = {
            //   task_id    : task_id,
            //   employee_id: manager[0].employee_id,
            //   due_date   : sla_due_date
            // };
            // await taskModel.insert_sla(sla_arr);

            //INSERT CC CUSTOMERS
            if (forecast_obj.cc_customers) {
              let cc_cust_arr = JSON.parse(forecast_obj.cc_customers);
              //INSERT CC CUSTOMERS
              if (cc_cust_arr.length > 0) {
                for (let index = 0; index < cc_cust_arr.length; index++) {
                  const element = cc_cust_arr[index].customer_id;
                  let insrt_cc = {
                    customer_id: element,
                    task_id: task_id,
                    status: 1
                  };
                  await customerModel.insert_cc_customer(insrt_cc);
                }
              }
            }



            //HIGHLIGHT NEW TASK
            let customer_id = forecast_obj.customer_id;

            let highlight_arr = [
              task_id,
              customer_id,
              1,
              'C'
            ];
            await taskModel.task_highlight(highlight_arr);

            let cc_list = await taskModel.task_cc_customer(task_id);
            if (cc_list && cc_list.length > 0) {
              for (let index = 0; index < cc_list.length; index++) {
                const element = cc_list[index];
                let highlight_cc_arr = [
                  task_id,
                  element.customer_id,
                  1,
                  'C'
                ];
                await taskModel.task_highlight(highlight_cc_arr);
              }
            }


            if (forecast_obj.share_with_agent == 1) {
              let customer_details = await customerModel.get_user(forecast_obj.customer_id);
              let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

              if (agent_list && agent_list.length > 0) {
                for (let index = 0; index < agent_list.length; index++) {
                  const element = agent_list[index];
                  let highlight_agent_arr = [
                    task_id,
                    element.agent_id,
                    1,
                    'A'
                  ];
                  await taskModel.task_highlight(highlight_agent_arr);
                }
              }
            }

            var db_due_date = new Date(task_details[0].due_date);
            var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

            let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

            var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;



            if (mail_arr.length > 0) {

              // var cust_status_mail = `<!DOCTYPE html>
              // <html>
              //   <body>
              //     <p><em>Thank you for your request</em></p>
              //     <p>Dear ${entities.decode(customer_details[0].first_name)} ${entities.decode(customer_details[0].last_name)},</p>
              //     <p></p>
              //     <p>Thank you for placing a request - ${task_details[0].req_name}.</p>
              //     <p>We have received your request with the following details:</p>
              //     <table>
              //       <tr>
              //         <td>Task Ref</td>
              //         <td>${task_details[0].task_ref}</td>
              //       </tr>
              //       <tr>
              //         <td>Customer Name</td>
              //         <td>${task_details[0].first_name} ${task_details[0].last_name}</td>
              //       </tr>
              //       <tr>
              //         <td>Request Type</td>
              //         <td>${task_details[0].req_name}</td>
              //       </tr>
              //       <tr>
              //         <td valign="top">Requirement</td>
              //         <td valign="top">${entities.decode(task_details[0].content)}</td>
              //       </tr>
              //       <tr>
              //         <td>&nbsp;</td>
              //         <td></td>
              //       </tr>
              //       <tr>
              //         <td>&nbsp;</td>
              //         <td></td>
              //       </tr>
              //     </table>
              //     <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. <a href="${url_status}">Click here</a> to view the details on this request</p>
              //     <p>-- Dr.Reddys API team</p>
              //   </body>
              // </html>`;

              // //console.log(cust_status_mail);
              // var mail_options1 = {
              //   from: Config.contactUsMail,
              //   to: mail_arr[0].to_arr,
              //   subject: mail_subject,
              //   html: cust_status_mail
              // };
              // if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
              //   mail_options1.cc = mail_arr[1].cc_arr;
              // }
              // //console.log(mail_options1);
              // common.sendMailB2C(mail_options1);

              let mailParam = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: task_details[0].task_ref,
                task_content: entities.decode(task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
              };
              var language_extn = 'req_name_' + task_details[0].language;
              switch (entities.decode(task_details[0].language)) {
                case 'es':
                  mailParam.req_name = task_details[0].req_name_es;
                  break;
                case 'pt':
                  mailParam.req_name = task_details[0].req_name_pt;
                  break;
                case 'ja':
                  mailParam.req_name = task_details[0].req_name_ja;
                  break;
                case 'zh':
                  mailParam.req_name = task_details[0].req_name_zh;
                  break;
                default:
                  mailParam.req_name = task_details[0].req_name;
                  break;
              }
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_FORCAST', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(task_details[0].language), mailParam, "c");

            }

            //Manager Mail
            let managerMailParam = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: task_details[0].task_ref,
              task_content: (entities.decode(task_details[0].language) != 'en') ? entities.decode(task_details[0].content_translate) : entities.decode(task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              format_due_date: format_due_date,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name,
              req_name: task_details[0].req_name
            };

            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_FORCAST', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

            // const mailOptions = {
            //   from: Config.webmasterMail,
            //   to: manager[0].email,
            //   subject: mail_subject,
            //   html: `<!DOCTYPE html>
            //   <html>
            //     <body>
            //       <p><em>New Task Assigned</em></p>
            //       <p>Hello ${manager[0].first_name},</p>
            //       <p>You have a new task assigned.</p>
            //       <p>Task Details are as below,</p>
            //       <table>
            //         <tr>
            //           <td>Task Ref</td>
            //           <td>${task_details[0].task_ref}</td>
            //         </tr>
            //         <tr>
            //           <td>Customer Name</td>
            //           <td>${task_details[0].first_name} ${task_details[0].last_name}</td>
            //         </tr>
            //         <tr>
            //           <td>Request Type</td>
            //           <td>${task_details[0].req_name}</td>
            //         </tr>
            //         <tr>
            //           <td>Due Date</td>
            //           <td>${format_due_date}</td>
            //         </tr>
            //         <tr>
            //           <td valign="top">Requirement</td>
            //           <td valign="top">${entities.decode(task_details[0].content)}</td>
            //         </tr>
            //         <tr>
            //           <td>&nbsp;</td>
            //           <td></td>
            //         </tr>
            //         <tr>
            //           <td>&nbsp;</td>
            //           <td></td>
            //         </tr>
            //       </table>
            //       <p>
            //         Click <a href="${Config.ccpUrl}/user/dashboard" >here</a> to see more details about this query
            //       </p>
            //     </body>
            //   </html>`,
            // };
            // common.sendMail(mailOptions);
          } else {
            //Unallocated Task Soumyadeep
            var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${forecast_obj.customer_id}`);
            common.logError(err);
          }
          //}

          var files = req.files;
          if (files && files.length > 0) {

            for (var j = 0; j < files.length; j++) {

              let file_details = {
                task_id: task_id,
                actual_file_name: files[j].originalname,
                new_file_name: files[j].filename,
                date_added: today
              }

              await taskModel.add_task_file(file_details);
            }
          }

          //SALES FORCE TASK
          if (Config.activate_sales_force == true) {
            await module.exports.__post_sales_force_add_task(task_id, files, req.user.empe);

            if (comment_arr.length > 0) {
              for (let index = 0; index < comment_arr.length; index++) {
                const element = comment_arr[index];
                await module.exports.__post_sales_force_internal_discussions(task_id, [], element.comment, element.posted_by);
              }
            }

          }

          res.status(200)
            .json({
              status: '1',
              message: "Data added"
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 2,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Create Payment
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Create payment used for payment selection
   */
  create_payment: async (req, res, next) => {
    try {
      const {
        task_id,
        request_id,
        customer_id,
        description,
        ref_no,
        request_category_type,
        field_days_remaining,
        field_due_date,
        field_orders,
        field_payment_status,
        field_amount_pending,
        cc_customers
      } = req.body;

      //===============
      var today = common.currentDateTime();
      var sla_due_date = dateFormat(new Date(field_due_date), "yyyy-mm-dd HH:MM:ss");
      //===============      

      const payment = {
        request_category_type: request_category_type,
        ref_no: ref_no,
        content: entities.encode(description),
        customer_id: customer_id,
        task_id: task_id,
        request_id: request_id,
        due_date: sla_due_date,
        current_date: today,
        days_remaining: field_days_remaining,
        payment_orders: field_orders,
        payment_status: field_payment_status,
        payment_pending: field_amount_pending,
        cc_customers: cc_customers
      }

      await taskModel.add_payment(payment)
        .then(async function (data) {

          //let task_ref = await taskModel.update_task_ref(data.task_id);
          let manager = await taskModel.get_allocated_manager(payment.customer_id);

          if (manager.length > 0) {

            await taskModel.update_task_owner(manager[0].employee_id, data.task_id);
            const assignment = {
              task_id: data.task_id,
              assigned_to: manager[0].employee_id,
              assigned_by: -1,
              assign_date: today,
              due_date: sla_due_date,
              reopen_date: today,
              parent_assignment_id: 0
            }
            var bot_assignment_id = await taskModel.assign_task(assignment);

            //Xceed bot -- satyajit
            if ((Config.environment == 'production' || Config.environment == 'qa') && bot_assignment_id > 0) {
              const request = require('request');
              var options = {
                method: 'POST',
                url: `${Config.drlBot.assign_url}${bot_assignment_id}`,
                headers: {
                  'x-api-key': 'XAmdVeqheisEoJ1SROQG'
                }
              };

              await request(options, async function (error, response, body) {
                if (error) {
                  var mailOptions = {
                    from: Config.webmasterMail,
                    to: ['soumyadeep@indusnet.co.in', 'satyajit.mondol@indusnet.co.in'],
                    subject: `URL || ${Config.drupal.url} || Bot entry failed`,
                    html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
                  };
                  await common.sendMail(mailOptions);
                  await common.logErrorText('Bot entry failed create_payment', JSON.stringify(error));

                } else {
                  if (response.statusCode != 200) {
                    var mailOptions = {
                      from: Config.webmasterMail,
                      to: ['soumyadeep@indusnet.co.in', 'satyajit.mondol@indusnet.co.in'],
                      subject: `URL || ${Config.drupal.url} || Bot entry failed`,
                      html: `Task Id : ${task_id} || Assignment Id: ${bot_assignment_id}`
                    };
                    await common.sendMail(mailOptions);
                    await common.logErrorText('Bot entry failed create_payment', JSON.stringify(response));
                  }
                }
              });
            }
            //end

            //INSERT CC CUSTOMERS
            if (payment.cc_customers && payment.cc_customers.length > 0) {
              for (let index = 0; index < payment.cc_customers.length; index++) {
                const element = payment.cc_customers[index];
                let insrt_cc = {
                  customer_id: element,
                  task_id: task_id,
                  status: 1
                };
                await customerModel.insert_cc_customer(insrt_cc);
              }
            }
          }


          res.status(200)
            .json({
              status: '1',
              message: "Data added"
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 2,
            message: Config.errorText.value
          }).end();
        });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Create Sub Task
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Create Sub Task used for Task selection
   */
  create_sub_task: async (req, res, next) => {
    try {
      const {
        task_id,
        comment,
        request_type,
        owner,
        task_ref,
        due_date
      } = req.body;
      let notify_type = 0;
      if (req.user.role == 2) {
        var agent_id = req.user.customer_id;
        var customer_id = 0;
        notify_type = 2;
      } else {
        var customer_id = req.user.customer_id;
        var agent_id = 0;
        notify_type = 1;
      }
      console.log('task_id ', task_id)
      let allow_cancel = true;
      var files = req.files;
      let lang = await taskModel.get_task_language(task_id);
      await taskModel.tasks_details(task_id)
        .then(async function (task_details) {
          //console.log('task  ',task_details);

          if (task_details.length > 0) {
            console.log('req type ', task_details[0].request_type)

            if (task_details[0].request_type == 43 && req.body.sap_cancel == 1 && task_details[0]['cancel_reservation'] == 0 && (task_details[0]['sales_order_no'] == '' || task_details[0]['sales_order_no'] == null) && (task_details[0]['po_no'] == '' || task_details[0]['po_no'] == null)) {
              console.log('allow')
              allow_cancel = false;
              allow_cancel = await module.exports.__sap_cancel_call(req, task_id);
              console.log('success SAP cancel', allow_cancel);
            }

            if (allow_cancel == true) {

              //check if task id exist with discussion 1 and parent this
              await taskModel.discussion_exists_posting_comments(task_id)
                .then(async function (discussionExists) {

                  if (discussionExists.length > 0) {

                    //if update discussion
                    let discussion_id = discussionExists[0].discussion_id;
                    const updt_arr = {
                      comment: entities.encode(entities.decode(comment)),
                      date_updated: common.currentDateTime(),
                      status: 1
                    }
                    await taskModel.update_discussion(discussion_id, updt_arr)
                      .then(async function (data) {
                        //insert in discussion comment
                        const discussion_comment = {
                          discussion_id: discussion_id,
                          comment: entities.encode(entities.decode(comment)),
                          date_added: common.currentDateTime(),
                          added_by_type: (agent_id > 0) ? 'A' : 'C',
                          added_by: (agent_id > 0) ? agent_id : customer_id,
                          language: lang
                        }
                        await taskModel.insert_discussion_comment(discussion_comment)
                          .then(async (data) => {

                            var task_details = await taskModel.tasks_details(task_id);

                            if (task_details[0].close_status === 1) {
                              await taskModel.update_task_status(task_id);
                            }

                            //insert in discussion files
                            if (files && files.length > 0) {

                              for (var j = 0; j < files.length; j++) {
                                let file_details = {
                                  comment_id: data.comment_id,
                                  actual_file_name: files[j].originalname,
                                  new_file_name: files[j].filename,
                                  date_added: common.currentDateTime(),
                                  task_id: task_id
                                }

                                await taskModel.insert_discussion_file(file_details);
                              }
                            }

                            //await taskModel.activate_highlighter(task_details[0].task_id);
                            //CHANGE STATUS MAIL FROM CUSTOMER PORTAL
                            var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;


                            let sales_force_comment = comment;
                            if (task_details[0].language != Config.default_language) {
                              let translated_language = await common.getConvertedContent(comment, task_details[0].language);
                              let decode_translated_comment = entities.encode(translated_language);

                              let translated_discussion_comment = {
                                discussion_id: discussion_id,
                                comment: decode_translated_comment,
                                date_added: common.currentDateTime(),
                                status: 0,
                                language: Config.default_language,
                                comment_id: data.comment_id
                              }
                              await taskModel.insert_discussion_comment_translated(translated_discussion_comment);
                              sales_force_comment = translated_language;
                            }

                            var customer_details = await customerModel.get_user(task_details[0].customer_id);

                            var mail_subject = '';
                            if (task_details[0].request_type == 24) {
                              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
                            } else {
                              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
                            }

                            //FOR EMPLOYEE NOTIFICATIONS
                            module.exports.subject_line = mail_subject;

                            let mail_arr4 = await module.exports.notify_cc_highlight(req, task_details, notify_type);


                            // ======= ACTIVITY LOG - SATYAJIT =======//
                            var params = {
                              task_ref: task_details[0].task_ref,
                              request_type: task_details[0].req_name
                            }
                            if (req.user.role == 2) {
                              var agentData = await agentModel.get_user(req.user.customer_id);
                              params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                              await taskModel.log_activity(7, task_id, params)
                            } else {
                              var submitted_details = await customerModel.get_user(req.user.customer_id);
                              params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`
                              await taskModel.log_activity(6, task_id, params)
                            }
                            // ====== END LOG ========== //
                            var req_name_lang = task_details[0].req_name;
                            var product_name_by_lang = task_details[0].product_name;
                            switch (entities.decode(task_details[0].language)) {
                              case 'es':
                                req_name_lang = task_details[0].req_name_es;
                                product_name_by_lang = task_details[0].product_name_es;
                                break;
                              case 'pt':
                                req_name_lang = task_details[0].req_name_pt;
                                product_name_by_lang = task_details[0].product_name_pt;
                                break;
                              case 'ja':
                                req_name_lang = task_details[0].req_name_ja;
                                product_name_by_lang = task_details[0].product_name_ja;
                                break;
                              case 'zh':
                                req_name_lang = task_details[0].req_name_zh;
                                product_name_by_lang = task_details[0].product_name_zh;
                                break;
                              default:
                                req_name_lang = task_details[0].req_name;
                                product_name_by_lang = task_details[0].product_name;
                                break;
                            }
                            if (product_name_by_lang == '' || product_name_by_lang == null) {
                              product_name_by_lang = task_details[0].product_name;
                            }
                            console.log("Mails", mail_arr4);
                            if (mail_arr4.length > 0) {
                              // var cust_status_mail = `<!DOCTYPE html>
                              // <html>
                              //     <body>
                              //       <p><em>Your response submitted</em></p>
                              //       <p>Dear ${entities.decode(customer_details[0].first_name)},</p>
                              //       <p></p>
                              //       <p>Thank you for your request. The inputs on ${task_details[0].task_ref} : ${task_details[0].req_name} submitted by you has been noted. Your Dr. Reddy’s team will share a response with you shortly.</p>
                              //       <table>
                              //         <tr>
                              //           <td valign="top">Comment:</td>
                              //         </tr>
                              //         <tr>
                              //           <td valign="top">${comment}</td>
                              //         </tr>
                              //       </table>
                              //       <p><a href="${url_status}" >Click Here</a> to view the details on this request</p>
                              //       <p>--  Dr.Reddys API team</p>
                              //     </body>
                              // </html>
                              // `;

                              // // <p>Please click the following link to view the details on your request::</p>

                              // var mail_options1 = {
                              //   from: Config.contactUsMail,
                              //   to: mail_arr4[0].to_arr,
                              //   subject: mail_subject,
                              //   html: cust_status_mail
                              // };
                              // if (mail_arr4[1].cc_arr && mail_arr4[1].cc_arr.length > 0) {
                              //   mail_options1.cc = mail_arr4[1].cc_arr;
                              // }
                              // common.sendMailB2C(mail_options1);                      

                              let mailParam = {
                                first_name: entities.decode(customer_details[0].first_name),
                                task_ref: task_details[0].task_ref,
                                req_name: req_name_lang,
                                comment: comment,
                                url_status: url_status,
                                company_name: entities.decode(customer_details[0].company_name)
                              };
                              if (task_details[0].request_type != 24) {
                                mailParam.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                              }
                              var mailcc = '';
                              if (mail_arr4[1].cc_arr && mail_arr4[1].cc_arr.length > 0) {
                                mailcc = mail_arr4[1].cc_arr;
                              }

                              const mailSent = common.sendMailByCodeB2C('NEW_DISCUSSION_CUSTOMER', mail_arr4[0].to_arr, "[Dr Reddys] <" + Config.contactUsMail + ">", mailcc, entities.decode(task_details[0].language), mailParam, "c");
                            }


                            var cc_exclude_arr = [];
                            //==== OWNER BM
                            let employee_details = await employeeModel.get_user(task_details[0].owner);
                            if (employee_details && employee_details.length > 0) {
                              let assignment_owner_id = await taskModel.get_assignment_id(task_details[0].owner, task_id);
                              // let html = `<!DOCTYPE html>
                              // <html>
                              //   <body>
                              //     <p><em>New discussion comment from Customer</em></p>
                              //     <p>Hello ${employee_details[0].first_name},</p>
                              //     <p>You have a new discussion.</p>
                              //     <table>
                              //       <tr>
                              //         <td valign="top">Comment:</td>
                              //       </tr>
                              //       <tr>
                              //         <td valign="top">${comment}</td>
                              //       </tr>
                              //       <tr>
                              //         <td>&nbsp;</td>
                              //         <td></td>
                              //       </tr>
                              //       <tr>
                              //         <td>&nbsp;</td>
                              //         <td></td>
                              //       </tr>
                              //     </table>
                              //     <p>Click <a href="${Config.ccpUrl}/user/task_details/${task_id}/${assignment_owner_id}/#discussion">here</a> to see more details about this query</p>
                              //   </body>
                              // </html>`;

                              // const mailOptions2 = {
                              //   from: Config.webmasterMail,
                              //   to: employee_details[0].email,
                              //   subject: mail_subject,
                              //   html: html,
                              // };
                              // common.sendMail(mailOptions2);

                              let mailParam1 = {
                                first_name: employee_details[0].first_name,
                                comment: comment,
                                ccpurl: Config.ccpUrl,
                                task_id: task_id,
                                assignment_owner_id: assignment_owner_id,
                                task_ref: task_details[0].task_ref,
                                company_name: entities.decode(customer_details[0].company_name),
                                customer_name: entities.decode(customer_details[0].first_name)
                              };
                              if (task_details[0].request_type != 24) {
                                mailParam1.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                              }

                              const mailSent = common.sendMailByCodeB2E('NEW_DISCUSSION_BM', employee_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam1, "e");

                              //==== OWNER BM
                              cc_exclude_arr.push(task_details[0].owner);
                              //let auto_assigned_owner = await taskModel.get_auto_assigned_task_owner(task_id);
                              //let auto_assigned_owner = await employeeModel.get_user(task_details[0].current_ownership);
                              //if (auto_assigned_owner && auto_assigned_owner.length > 0 && common.inArray(auto_assigned_owner[0].desig_id, [2, 3, 5])) {
                              if (task_details[0].owner != task_details[0].current_ownership) {
                                //==== ASSIGNED EMPLOYEES
                                let assignment_assign_to_id = await taskModel.get_assignment_id(task_details[0].current_ownership, task_id);
                                let a_emp_details = await employeeModel.get_user(task_details[0].current_ownership);
                                // let html = `<!DOCTYPE html>
                                // <html>
                                //   <body>
                                //     <p><em>New discussion comment from Customer</em></p>
                                //     <p>Hello ${a_emp_details[0].first_name},</p>
                                //     <p>You have a new discussion.</p>
                                //     <table>
                                //       <tr>
                                //         <td valign="top">Comment:</td>
                                //       </tr>
                                //       <tr>
                                //         <td valign="top">${comment}</td>
                                //       </tr>
                                //       <tr>
                                //         <td>&nbsp;</td>
                                //         <td></td>
                                //       </tr>
                                //       <tr>
                                //         <td>&nbsp;</td>
                                //         <td></td>
                                //       </tr>
                                //     </table>
                                //     <p>Click <a href="${Config.ccpUrl}/user/task_details/${task_id}/${assignment_assign_to_id}/#discussion">here</a> to see more details about this query</p>
                                //   </body>
                                // </html>`;

                                // const mailOptions = {
                                //   from: Config.webmasterMail,
                                //   to: a_emp_details[0].email,
                                //   subject: mail_subject,
                                //   html: html,
                                // };
                                // common.sendMail(mailOptions);

                                let mailParam2 = {
                                  first_name: a_emp_details[0].first_name,
                                  comment: comment,
                                  ccpurl: Config.ccpUrl,
                                  task_id: task_id,
                                  assignment_assign_to_id: assignment_assign_to_id,
                                  task_ref: task_details[0].task_ref,
                                  company_name: entities.decode(customer_details[0].company_name),
                                  customer_name: entities.decode(customer_details[0].first_name)
                                };
                                if (task_details[0].request_type != 24) {
                                  mailParam2.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                                }

                                const mailSent = common.sendMailByCodeB2E('NEW_DISCUSSION_ASSIGNED_EMPLOYEES', a_emp_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam2, "e");

                                //==== ASSIGNED EMPLOYEES
                                cc_exclude_arr.push(task_details[0].current_ownership);
                              }
                            }

                            // //CC customer LIST
                            // var ccEmployeeList = await employeeModel.task_cc_employees_arr(task_id,cc_exclude_arr);

                            // if(ccEmployeeList.length > 0){

                            //   for (let index = 0; index < ccEmployeeList.length; index++) {
                            //     const element = ccEmployeeList[index];

                            //     var cc_mail_html = `
                            //     <!DOCTYPE html>
                            //     <html>
                            //       <body>
                            //           <p>Hello ${element.first_name},</p>
                            //           <p>You were copied on the below task notification by the task owner.</p>
                            //           <p>-------------------------</p>
                            //           <p>Hello ${employee_details[0].first_name},</p>
                            //           <p>You have a new discussion.</p>
                            //           <table>
                            //             <tr>
                            //               <td>Comment:</td>
                            //               <td>${comment}</td>
                            //             </tr>
                            //           </table>
                            //           <p>--  Dr.Reddys API team</p>
                            //           <p>-------------------------</p>
                            //       </body>
                            //     </html>`;


                            //     common.sendMail({
                            //       from    : Config.webmasterMail,
                            //       to      : element.email,
                            //       subject : `CC: ${task_details[0].task_ref} | ${task_details[0].req_name} | New discussion comment`,
                            //       html    : cc_mail_html
                            //     });
                            //   }
                            // }

                            //SALES FORCE TASK
                            if (Config.activate_sales_force == true) {
                              console.log('sales force');
                              await module.exports.__post_sales_force_external_discussions(task_id, files, entities.encode(entities.decode(sales_force_comment)));
                            }

                            res.status(200)
                              .json({
                                status: '1',
                                message: "Comment added"
                              });

                          }).catch((err) => {
                            common.logError(err);
                            res.status(400).json({
                              status: 2,
                              message: Config.errorText.value
                            }).end();
                          });

                      }).catch(err => {
                        common.logError(err);
                        res.status(400).json({
                          status: 2,
                          message: Config.errorText.value
                        }).end();
                      });

                  } else {

                    //else insert discussion
                    const insrt_arr = {
                      task_id: task_id,
                      comment: entities.encode(entities.decode(comment)),
                      date_added: common.currentDateTime()
                    }

                    await taskModel.insert_discussion(insrt_arr)
                      .then(async function (dataDiss) {
                        //insert in discussion comment
                        const discussion_comment = {
                          discussion_id: dataDiss.discussion_id,
                          comment: entities.encode(entities.decode(comment)),
                          date_added: common.currentDateTime(),
                          added_by_type: (agent_id > 0) ? 'A' : 'C',
                          added_by: (agent_id > 0) ? agent_id : customer_id,
                          language: lang
                        }

                        await taskModel.insert_discussion_comment(discussion_comment)
                          .then(async (data) => {

                            var task_details = await taskModel.tasks_details(task_id);

                            if (task_details[0].close_status === 1) {
                              await taskModel.update_task_status(task_id);
                            }

                            //insert in discussion files
                            if (files && files.length > 0) {
                              for (var j = 0; j < files.length; j++) {
                                let file_details = {
                                  comment_id: data.comment_id,
                                  actual_file_name: files[j].originalname,
                                  new_file_name: files[j].filename,
                                  date_added: common.currentDateTime(),
                                  task_id: task_id
                                }
                                await taskModel.insert_discussion_file(file_details);
                              }
                            }

                            var cc_exclude_arr = [];
                            //await taskModel.activate_highlighter(task_details[0].task_id);

                            let sales_force_comment = comment;
                            if (task_details[0].language != Config.default_language) {
                              let translated_language = await common.getConvertedContent(comment, task_details[0].language);
                              let decode_translated_comment = entities.encode(translated_language);

                              let translated_discussion_comment = {
                                discussion_id: dataDiss.discussion_id,
                                comment: decode_translated_comment,
                                date_added: common.currentDateTime(),
                                status: 0,
                                language: Config.default_language,
                                comment_id: data.comment_id
                              }
                              await taskModel.insert_discussion_comment_translated(translated_discussion_comment);
                              sales_force_comment = translated_language;
                            }


                            var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

                            var customer_details = await customerModel.get_user(task_details[0].customer_id);

                            var mail_subject = '';
                            if (task_details[0].request_type == 24) {
                              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
                            } else {
                              mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
                            }

                            //FOR EMPLOYEE NOTIFICATIONS
                            module.exports.subject_line = mail_subject;

                            let mail_arr4 = await module.exports.notify_cc_highlight(req, task_details, notify_type);

                            // ======= ACTIVITY LOG - SATYAJIT =======//
                            var params = {
                              task_ref: task_details[0].task_ref,
                              request_type: task_details[0].req_name
                            }
                            if (req.user.role == 2) {
                              var agentData = await agentModel.get_user(req.user.customer_id);
                              params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                              await taskModel.log_activity(7, task_id, params)
                            } else {
                              var submitted_details = await customerModel.get_user(req.user.customer_id);
                              params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`;
                              await taskModel.log_activity(6, task_id, params)
                            }
                            // ====== END LOG ========== //
                            var req_name_lang = task_details[0].req_name;
                            var product_name_by_lang = task_details[0].product_name;
                            switch (entities.decode(task_details[0].language)) {
                              case 'es':
                                req_name_lang = task_details[0].req_name_es;
                                product_name_by_lang = task_details[0].product_name_es;
                                break;
                              case 'pt':
                                req_name_lang = task_details[0].req_name_pt;
                                product_name_by_lang = task_details[0].product_name_pt;
                                break;
                              case 'ja':
                                req_name_lang = task_details[0].req_name_ja;
                                product_name_by_lang = task_details[0].product_name_ja;
                                break;
                              case 'zh':
                                req_name_lang = task_details[0].req_name_zh;
                                product_name_by_lang = task_details[0].product_name_zh;
                                break;
                              default:
                                req_name_lang = task_details[0].req_name;
                                product_name_by_lang = task_details[0].product_name;
                                break;
                            }
                            if (product_name_by_lang == '' || product_name_by_lang == null) {
                              product_name_by_lang = task_details[0].product_name;
                            }

                            if (mail_arr4.length > 0) {
                              // var cust_status_mail = `<!DOCTYPE html>
                              // <html>
                              //     <body>
                              //       <p><em>Your response submitted</em></p>
                              //       <p>Dear ${entities.decode(customer_details[0].first_name)},</p>
                              //       <p></p>
                              //       <p>Thank you for your request. The inputs on ${task_details[0].task_ref} : ${task_details[0].req_name} submitted by you has been noted. Your Dr. Reddy’s team will share a response with you shortly.</p>
                              //       <table>
                              //         <tr>
                              //           <td valign="top">Comment:</td>
                              //         </tr>
                              //         <tr>
                              //           <td valign="top">${comment}</td>
                              //         </tr>
                              //       </table>
                              //       <p><a href="${url_status}" >Click Here</a> to view the details on this request</p>
                              //       <p>--  Dr.Reddys API team</p>
                              //     </body>
                              // </html>
                              // `;

                              // // <p>Please click the following link to view the details on your request::<a href="${url_status}" >Click Here</a></p>

                              // var mail_options1 = {
                              //   from: Config.contactUsMail,
                              //   to: mail_arr4[0].to_arr,
                              //   subject: mail_subject,
                              //   html: cust_status_mail
                              // };
                              // if (mail_arr4[1].cc_arr && mail_arr4[1].cc_arr.length > 0) {
                              //   mail_options1.cc = mail_arr4[1].cc_arr;
                              // }
                              // common.sendMailB2C(mail_options1);                      

                              let mailParam = {
                                first_name: entities.decode(customer_details[0].first_name),
                                task_ref: task_details[0].task_ref,
                                req_name: req_name_lang,
                                comment: comment,
                                url_status: url_status,
                                company_name: entities.decode(customer_details[0].company_name)
                              };
                              if (task_details[0].request_type != 24) {
                                mailParam.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                              }
                              var mailcc = '';
                              if (mail_arr4[1].cc_arr && mail_arr4[1].cc_arr.length > 0) {
                                mailcc = mail_arr4[1].cc_arr;
                              }
                              const mailSent = common.sendMailByCodeB2C('NEW_DISCUSSION_CUSTOMER', mail_arr4[0].to_arr, "[Dr Reddys] <" + Config.contactUsMail + ">", mailcc, entities.decode(task_details[0].language), mailParam, "c");
                            }

                            //==== OWNER BM
                            let employee_details = await employeeModel.get_user(task_details[0].owner);
                            if (employee_details && employee_details.length > 0) {
                              let assignment_owner_id = await taskModel.get_assignment_id(task_details[0].owner, task_id);
                              // let html = `<!DOCTYPE html>
                              // <html>
                              //   <body>
                              //     <p><em>New discussion comment from Customer</em></p>
                              //     <p>Hello ${employee_details[0].first_name},</p>
                              //     <p>You have a new discussion.</p>
                              //     <table>
                              //       <tr>
                              //         <td valign="top">Comment:</td>
                              //       </tr>
                              //       <tr>
                              //         <td valign="top">${comment}</td>
                              //       </tr>
                              //       <tr>
                              //         <td>&nbsp;</td>
                              //         <td></td>
                              //       </tr>
                              //       <tr>
                              //         <td>&nbsp;</td>
                              //         <td></td>
                              //       </tr>
                              //     </table>
                              //     <p>Click <a href="${Config.ccpUrl}/user/task_details/${task_id}/${assignment_owner_id}/#discussion">here</a> to see more details about this query</p>
                              //   </body>
                              // </html>`;

                              // const mailOptions2 = {
                              //   from: Config.webmasterMail,
                              //   to: employee_details[0].email,
                              //   subject: mail_subject,
                              //   html: html,
                              // };
                              // common.sendMail(mailOptions2);
                              let mailParam1 = {
                                first_name: employee_details[0].first_name,
                                comment: comment,
                                ccpurl: Config.ccpUrl,
                                task_id: task_id,
                                assignment_owner_id: assignment_owner_id,
                                task_ref: task_details[0].task_ref,
                                company_name: entities.decode(customer_details[0].company_name),
                                customer_name: entities.decode(customer_details[0].first_name)
                              };
                              if (task_details[0].request_type != 24) {
                                mailParam1.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                              }

                              const mailSent = common.sendMailByCodeB2E('NEW_DISCUSSION_BM', employee_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam1, "e");

                              cc_exclude_arr.push(task_details[0].owner);
                              //==== OWNER BM
                              //let auto_assigned_owner = await taskModel.get_auto_assigned_task_owner(task_id, owner);
                              //let auto_assigned_owner = await employeeModel.get_user(task_details[0].current_ownership);
                              //if (auto_assigned_owner && auto_assigned_owner.length > 0) {
                              if (task_details[0].owner != task_details[0].current_ownership) {
                                let assignment_assign_to_id = await taskModel.get_assignment_id(task_details[0].current_ownership, task_id);
                                //==== ASSIGNED EMPLOYEES
                                let a_emp_details = await employeeModel.get_user(task_details[0].current_ownership);
                                // let html = `<!DOCTYPE html>
                                // <html>
                                //   <body>
                                //     <p><em>New discussion comment from Customer</em></p>
                                //     <p>Hello ${a_emp_details[0].first_name},</p>
                                //     <p>You have a new discussion.</p>
                                //     <table>
                                //       <tr>
                                //         <td valign="top">Comment:</td>
                                //       </tr>
                                //       <tr>
                                //         <td valign="top">${comment}</td>
                                //       </tr>
                                //       <tr>
                                //         <td>&nbsp;</td>
                                //         <td></td>
                                //       </tr>
                                //       <tr>
                                //         <td>&nbsp;</td>
                                //         <td></td>
                                //       </tr>
                                //     </table>
                                //     <p>Click <a href="${Config.ccpUrl}/user/task_details/${task_id}/${assignment_assign_to_id}/#discussion">here</a> to see more details about this query</p>
                                //   </body>
                                // </html>`;

                                // const mailOptions = {
                                //   from: Config.webmasterMail,
                                //   to: a_emp_details[0].email,
                                //   subject: mail_subject,
                                //   html: html,
                                // };
                                // common.sendMail(mailOptions);

                                let mailParam2 = {
                                  first_name: a_emp_details[0].first_name,
                                  comment: comment,
                                  ccpurl: Config.ccpUrl,
                                  task_id: task_id,
                                  assignment_assign_to_id: assignment_assign_to_id,
                                  task_ref: task_details[0].task_ref,
                                  company_name: entities.decode(customer_details[0].company_name),
                                  customer_name: entities.decode(customer_details[0].first_name)
                                };
                                if (task_details[0].request_type != 24) {
                                  mailParam2.product_name = "| " + entities.decode(product_name_by_lang) + " ";
                                }

                                const mailSent = common.sendMailByCodeB2E('NEW_DISCUSSION_ASSIGNED_EMPLOYEES', a_emp_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam2, "e");

                                //==== ASSIGNED EMPLOYEES
                                cc_exclude_arr.push(task_details[0].current_ownership);
                              }
                            }

                            //SALES FORCE TASK
                            if (Config.activate_sales_force == true) {
                              console.log('sales force');
                              await module.exports.__post_sales_force_external_discussions(task_id, files, entities.encode(entities.decode(sales_force_comment)));
                            }

                            res.status(200)
                              .json({
                                status: '1',
                                message: "Comment added"
                              });

                          }).catch((err) => {
                            common.logError(err);
                            res.status(400).json({
                              status: 2,
                              message: Config.errorText.value
                            }).end();
                          });

                      }).catch(err => {
                        common.logError(err);
                        res.status(400).json({
                          status: 2,
                          message: Config.errorText.value
                        }).end();
                      });
                  }
                }).catch(err => {
                  common.logError(err);
                  res.status(400).json({
                    status: 2,
                    message: Config.errorText.value
                  }).end();
                });
            } else {
              common.logError('');
              res.status(400).json({
                status: 2,
                message: Config.errorText.value
              }).end();
            }
          } else {
            common.logError('No Task found.');
            res.status(400).json({
              status: 2,
              message: Config.errorText.value
            }).end();
          }
        }).catch((err) => {
          common.logError(err);
          res.status(400).json({
            status: 2,
            message: Config.errorText.value
          }).end();
        });


    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },


  __sap_cancel_call: async (req, task_id) => {

    return new Promise(async (resolve, reject) => {
      console.log('start', task_id)
      let customer_id = 0;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }
      await sapModel.get_stock_task_details(task_id)
        .then(async function (data) {
          console.log(data)

          if (data && data.request_id != '' && data.request_id != undefined) {
            var headers, body, statusCode;
            var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><ReservationDetails><Main_Req>' + data.request_id + '</Main_Req><Reservation_Request_No>' + data.reservation_no + '</Reservation_Request_No><SO_ID>' + data.so_id + '</SO_ID><Identifier>CANCEL</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number></Ship_to_party_Number><Material_Number></Material_Number><Market></Market><Total_Order_Quantity></Total_Order_Quantity><Unit_Of_Measure></Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID></Itm_ID><FBPO_Number></FBPO_Number><EPDD></EPDD><Request_Del_Date></Request_Del_Date><TotalAvailableQuantity></TotalAvailableQuantity><UnitOfMeasure></UnitOfMeasure><RLT_Flag></RLT_Flag></ReservationDetails></res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';

            console.log('STOCK Cancel XML : ', xml_Request);

            if (Config.sapEnv == 'qa') {
              const sampleHeaders = {
                'Content-Type': 'application/soap+xml;charset=UTF-8',
                'soapAction': Config.sap_stock_soap.soapAction,
                'Authorization': 'Basic ' + Config.sap_stock_soap.authorization
              };
              const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
              var { headers, body, statusCode } = response;
              console.log(headers);
              console.log(body);
              console.log(statusCode);
            }

            if (Config.sapEnv == 'local') {
              var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>UPDATE</Req_Type><STATUS>Success</STATUS><Mode>Customer Cancellation</Mode><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
              var statusCode = 200;
            }

            var body_data = xmlParser.toJson(body);
            var cancel_arr = [];

            var is_error = 0; var cancel_message = '';

            let insLogObj = {
              request: xml_Request,
              response: body,
              method: 'POST',
              url: Config.sapReservationUrl,
              datetime: common.currentDateTime(),
              request_type: 'CANCEL'
            };
            let logResponse = await sapModel.insertRequestLog(insLogObj);

            if (statusCode == 200) {
              body_data = JSON.parse(body_data, true);

              if (body_data != '' && body_data['SOAP:Envelope'] != '') {
                cancel_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];

                if (cancel_arr.length > 1) {
                  for (let index = 0; index <= (cancel_arr.length - 1); index++) {
                    if (cancel_arr[index]['STATUS'] != 'Success') {
                      $is_error++;
                    } else {
                      cancel_message = cancel_arr[index]['Mode'];
                    }
                  }
                } else {
                  if (cancel_arr['STATUS'] != 'Success') {
                    is_error++;
                  } else {
                    cancel_message = cancel_arr['Mode'];
                  }
                }

                if (is_error == 0) {
                  var cancelTask = await sapModel.cancelTask(task_id, cancel_message);
                  resolve(true);
                } else {
                  resolve(false);
                }
              }
            }
          } else {
            resolve(false);
          }

        })
        .catch((err) => {
          common.logError(err);
          resolve(false);
        });
    })
  },

  /**
   * Generates a Task Details
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Task Details used for Task selection
   */
  task_details: async (req, res, next) => {
    //console.log('task controller',process.env.NODE_ENV);
    try {
      //console.log("this is user : ",req.user);
      const task_id = req.value.params.id;
      await taskModel
        .tasks_details(task_id)
        .then(async function (data) {
          if (data[0].request_type == 23 || data[0].request_type == 43) {
            let customer_type = '';
            let customer_id = '';
            if (req.user.role == 1) {
              customer_type = 'C';
              customer_id = req.user.customer_id;
            } else if (req.user.role == 2) {
              customer_type = 'A';
              customer_id = req.user.agent_id
            }
            let trackingObj = {
              task_id: data[0].task_id,
              customer_id: customer_id,
              customer_type: customer_type
            }
            const check_tracking = await taskModel.check_tracking(trackingObj);
            if (check_tracking && check_tracking.length > 0) {
              if (check_tracking[0].id > 0) {
                trackingObj.date_updated = common.currentDateTime();
                const update_tracking = await taskModel.update_tracking(trackingObj)
                if (update_tracking && update_tracking[0].id != undefined) {
                  console.log("Tracking data updated .");
                } else {
                  console.log("Error in Updating Tracking Data");
                }
              }
            } else {
              trackingObj.date_added = common.currentDateTime();
              trackingObj.date_updated = common.currentDateTime();
              const insert_tracking_data = await taskModel.insert_tracking_data(trackingObj)
              if (insert_tracking_data && insert_tracking_data[0].id != undefined) {
                console.log("Tracking data Inserted .");
              } else {
                console.log("Error in Inserting Tracking Data");
              }
            }

          }
          // =========== COUNTRY NAME MULTIPLE ========//
          var country = await taskModel.tasks_details_countries(data[0].task_id);

          //========================================//

          if (data[0].language != Config.default_language) {
            //COUNTRY
            let t_country = await taskModel.get_translated_country(task_id, data[0].language);
            data[0].country_name = t_country[0].country_name;

            if (data[0].request_type == 13 && data[0].stability_data_type != '') {
              let t_stability_data_type = await taskModel.get_translated_stability_data_type(data[0].stability_data_type, data[0].language);
              data[0].stability_data_type = t_stability_data_type[0].name;
            }

            let t_request_type = await taskModel.get_translated_request_name(data[0].request_type, data[0].language);
            data[0].req_name = t_request_type[0].name;

            if (data[0].request_type != 24) {
              let t_product_name = await taskModel.get_translated_product(data[0].product_id, data[0].language);

              data[0].product_name = t_product_name[0].name;
            }


            if (data[0].request_type == 38 && data[0].apos_document_type != '') {
              let t_apos_document_type = await taskModel.get_translated_apos_document_type(data[0].apos_document_type, data[0].language);
              data[0].apos_document_type = t_apos_document_type;
            }

            if (data[0].quantity && trim(data[0].quantity) != '') {
              let q_arr = data[0].quantity.split(' ');
              if (q_arr[1] == 'KG') {
                q_arr[1] = "Kgs";
              }
              let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[0].language);
              data[0].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
            }

            if (data[0].impurities_quantity && trim(data[0].impurities_quantity) != '') {
              let q_arr = data[0].impurities_quantity.split(' ');
              let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[0].language);
              data[0].impurities_quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
            }

            if (data[0].working_quantity && trim(data[0].working_quantity) != '') {
              let q_arr = data[0].working_quantity.split(' ');
              let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[0].language);
              data[0].working_quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
            }

            if (data[0].request_type == 1) {
              data[0].service_request_type = await common.getConvertedContentExplicit(data[0].service_request_type, Config.default_language, data[0].language);
            }


          } else {
            data[0].country_name = country[0].country_name;
          }

          data[0].rdd_pending = false;
          if (data[0].request_type == 23) {
            data[0].rdd_pending = await taskModel.checkPendingRdd(task_id);
          }

          let hasDiscussion;
          let commentArr = [];
          let hasComment;
          let discussionExists = await taskModel.discussion_exists(task_id);
          if (discussionExists.length > 0) {
            hasDiscussion = true;
            commentArr = await taskModel.select_discussion_comment_log(
              discussionExists[0].discussion_id,
            );
            let files_arr = [];
            if (commentArr.length > 0) {
              hasComment = true;

              for (let index = 0; index < commentArr.length; index++) {
                const element = commentArr[index];
                console.log(data[0].language, commentArr[index].added_by_type);
                if (data[0].language != Config.default_language && commentArr[index].added_by_type == 'E') {
                  console.log(data[0].language, commentArr[index].translated_language);
                  if (data[0].language == commentArr[index].translated_language) {
                    commentArr[index].comment = commentArr[index].translated_comment;
                  }

                }

                commentArr[index].customer_first_name = entities.decode(commentArr[index].customer_first_name);
                commentArr[index].customer_last_name = entities.decode(commentArr[index].customer_last_name);
                if (element.employee_profile_pic && element.employee_profile_pic != '') {
                  let aws_pic_url = await module.exports.__get_signed_url(`${element.employee_profile_pic}`);
                  if (aws_pic_url.status && aws_pic_url.status == 1) {
                    commentArr[index].employee_profile_pic = aws_pic_url.url;
                  }
                }
                let files_arr = await taskModel.select_discussion_comment_specific_files(
                  element.comment_id
                );

                if (files_arr.length > 1) {
                  commentArr[index].multi = process.env.FULL_PATH + '/api/tasks/download_discussion_uploads/' + hashids.encode(element.comment_id);
                } else {
                  commentArr[index].multi = ''
                }

                if (files_arr.length > 0) {
                  for (var j = 0; j < files_arr.length; j++) {
                    if (process.env.NODE_ENV == 'development') {
                      files_arr[j]['url'] = process.env.FULL_PATH + '/client_uploads/' + files_arr[j]['new_file_name'];
                    } else {
                      // s3_bucket_status = await module.exports.__get_signed_url(`${files_arr[j]['new_file_name']}`);
                      // console.log("BUCKET",s3_bucket_status);
                      // if(s3_bucket_status.status === 1)
                      files_arr[j]['url'] = process.env.FULL_PATH + '/api/tasks/get_file/' + cryptr.encrypt(files_arr[j]['upload_id']);
                      // +'/'+encodeURI(cryptr.encrypt(files_arr[j]['actual_file_name']))
                      // else
                      //     files_arr[j]['url'] = process.env.FULL_PATH+':'+process.env.PORT+'/images/VisualEditor_icon_page-not-found-ltr.svg.png';
                      // files_arr[j]['url']=Config.aws.s3staticUrl+files_arr[j]['new_file_name'];
                    }

                  }
                }
                element.uploads = files_arr;
              }

            } else {
              hasComment = false;
            }

          } else {
            hasDiscussion = false;
          }
          let files_arr_discussion = [];
          files_arr_discussion = await taskModel.select_task_discussion_files(task_id);
          var output_arr = [];
          if (files_arr_discussion.length > 0) {
            files_arr_discussion.map(async file => {
              if (file.comment_id) {
                if (process.env.NODE_ENV == 'development') {
                  file.url = process.env.FULL_PATH + '/client_uploads/' + file.new_file_name
                } else {
                  // s3_bucket_status = await module.exports.__get_signed_url(`${file.new_file_name}`);
                  // if(s3_bucket_status.status === 1)
                  file.url = process.env.FULL_PATH + '/api/tasks/get_file/' + cryptr.encrypt(file.upload_id);
                  // file.url = s3_bucket_status.url;
                  // else
                  //     file.url = process.env.FULL_PATH+':'+process.env.PORT+'/images/VisualEditor_icon_page-not-found-ltr.svg.png';
                  // file.url=Config.aws.s3staticUrl+file.new_file_name;
                }
              }

            })

            //SATYAJIT
            files_arr_discussion.map(async file => {
              if (file.comment_id) {
                if (process.env.NODE_ENV == 'development') {
                  file.url = process.env.FULL_PATH + '/client_uploads/' + file.new_file_name
                } else {
                  // s3_bucket_status = await module.exports.__get_signed_url(`${file.new_file_name}`);
                  // if(s3_bucket_status.status === 1)
                  file.url = process.env.FULL_PATH + '/api/tasks/get_file/' + cryptr.encrypt(file.upload_id);
                  // file.url = s3_bucket_status.url;
                  // else
                  //     file.url = process.env.FULL_PATH+':'+process.env.PORT+'/images/VisualEditor_icon_page-not-found-ltr.svg.png';
                  // file.url=Config.aws.s3staticUrl+file.new_file_name;
                }
              }
              //file.url = process.env.FULL_PATH + ':' + process.env.PORT + '/discussion_files/' + file.new_file_name
            })
            var file_comment_arr = [];
            var comment_arr = [];
            var comment_arr = await taskModel.select_comment_discussion(task_id);
            //console.log(comment_arr);

            if (comment_arr) {
              for (var k = 0; k < comment_arr.length; k++) {

                var image_arr = await taskModel.select_comment_discussion_files(comment_arr[k].comment_id);

                if (image_arr.length > 0) {
                  image_arr.map(async file => {
                    if (file.upload_id) {
                      file.customer_first_name = entities.decode(file.customer_first_name);
                      file.customer_last_name = entities.decode(file.customer_last_name)
                      if (process.env.NODE_ENV == 'development') {
                        file.url = process.env.FULL_PATH + '/client_uploads/' + file.new_file_name
                      } else {
                        // s3_bucket_status = await module.exports.__get_signed_url(`${file.new_file_name}`);
                        // if(s3_bucket_status.status === 1)
                        file.url = process.env.FULL_PATH + '/api/tasks/get_file/' + cryptr.encrypt(file.upload_id);
                        // else
                        //     file.url = process.env.FULL_PATH+':'+process.env.PORT+'/images/VisualEditor_icon_page-not-found-ltr.svg.png';
                        // file.url=Config.aws.s3staticUrl+file.new_file_name;
                      }
                    }

                  });
                  let multi = ''
                  if (image_arr.length > 1) {
                    multi = process.env.FULL_PATH + '/api/tasks/download_discussion_files/' + hashids.encode(comment_arr[k].comment_id);
                  }

                  output_arr.push({
                    multi: multi,
                    files: image_arr,
                    first_name: image_arr[0].first_name,
                    last_name: image_arr[0].last_name,
                    date_added: image_arr[0].date_added
                  });
                }

                //image_arr[k]['submitted_by']=image_arr[k].first_name+" "+image_arr[k].last_name;

              }
            }
          }

          // let subTasks         = await taskModel.sub_tasks_new(task_id);
          let subTasks = [];
          // taskDetailsFiles.push({url :process.env.FULL_PATH+'/:'+process.env.PORT+'/task_files/' });
          let taskDetailsFiles = await taskModel.select_task_files(task_id);
          for (var i = 0; i < taskDetailsFiles.length; i++) {
            if (process.env.NODE_ENV == 'development') {
              taskDetailsFiles[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFiles[i]['new_file_name'];
            } else {
              // s3_bucket_status = await module.exports.__get_signed_url(`${taskDetailsFiles[i]['new_file_name']}`);
              // if(s3_bucket_status.status === 1)
              taskDetailsFiles[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_task_file/' + cryptr.encrypt(taskDetailsFiles[i]['upload_id']);
              //taskDetailsFiles[i]['url']  = s3_bucket_status.url;
              // else
              // taskDetailsFiles[i]['url']  = process.env.FULL_PATH+':'+process.env.PORT+'/images/VisualEditor_icon_page-not-found-ltr.svg.png';
              // file.url=Config.aws.s3staticUrl+taskDetailsFiles[i]['new_file_name'];
            }

          }
          // taskDetailsFiles.push({url :process.env.FULL_PATH+'/:'+process.env.PORT+'/task_files/' }); 
          let taskDetailsFilesCoa = [];
          let taskDetailsFilesInvoice = [];
          let taskDetailsFilesPacking = [];
          let shippingInfo = [];
          let paymentInfo = [];
          let shippingTab = false;
          let preShipmentInfo = [];
          let preShipmentTab = false;

          data[0]['SAPDetails'] = [];
          if (data[0].sap_request_id != null && data[0].sap_request_id != '') {
            data[0]['SAPDetails'] = await taskModel.get_sap_all_details(data[0].sap_request_id);
          }
          if (data[0].sales_order_no != null && data[0].sales_order_no != '') {
            shippingTab = true;

            taskDetailsFilesCoa = await taskModel.select_task_files_coa(data[0].sales_order_no);
            for (var i = 0; i < taskDetailsFilesCoa.length; i++) {
              if (process.env.NODE_ENV == 'development') {
                taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFilesCoa[i]['new_file_name'];
              } else {
                taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(taskDetailsFilesCoa[i]['invoice_number'] + '#' + taskDetailsFilesCoa[i]['task_id'] + '#' + taskDetailsFilesCoa[i]['coa_id']);
              }
            }

            // taskDetailsFilesPacking = await taskModel.select_task_files_packing(data[0].sales_order_no);
            // for (var i = 0; i < taskDetailsFilesPacking.length; i++) {
            //   if (process.env.NODE_ENV == 'development') {
            //     taskDetailsFilesPacking[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFilesPacking[i]['new_file_name'];
            //   } else {
            //     taskDetailsFilesPacking[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_packing_task_file/' + cryptr.encrypt(taskDetailsFilesPacking[i]['id']);
            //   }
            // }

            // taskDetailsFilesInvoice = await taskModel.select_task_files_invoice(data[0].sales_order_no);
            // for (var i = 0; i < taskDetailsFilesInvoice.length; i++) {
            //   if (process.env.NODE_ENV == 'development') {
            //     taskDetailsFilesInvoice[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFilesInvoice[i]['new_file_name'];
            //   } else {
            //     taskDetailsFilesInvoice[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_invoice_task_file/' + cryptr.encrypt(taskDetailsFilesInvoice[i]['id']);
            //   }
            // }

            paymentInfo = await taskModel.get_invoice_payment(data[0].sales_order_no, task_id);
            for (let indip = 0; indip < paymentInfo.length; indip++) {
              const element = paymentInfo[indip];
              paymentInfo[indip].ref_name = entities.decode(element.ref_name);
              const invoice_items = await taskModel.get_invoice_payment_items(element.invoice_number);
              console.log(invoice_items)
              if (invoice_items.length > 0) {
                paymentInfo[indip].payment_items = invoice_items;
              } else {
                paymentInfo[indip].payment_items = [];
              }
            }

            shippingInfo = await module.exports.__get_shipping_details(data[0].sales_order_no, task_id);

            // console.log('shippingInfo', shippingInfo);


            preShipmentInfo = await taskModel.get_preship_invoice(data[0].sales_order_no);
            if (preShipmentInfo.length > 0 && req.user.role == 1) {
              preShipmentTab = true;
              for (let indps = 0; indps < preShipmentInfo.length; indps++) {
                const element = preShipmentInfo[indps];

                preShipmentInfo[indps]['file_hash'] = cryptr.encrypt(element.invoice_number + "#" + data[0].task_id);
                /*let preShipmentFilesCoa = await taskModel.select_task_files_coa_by_invoice(element.invoice_number);
                for (var i = 0; i < preShipmentFilesCoa.length; i++) {
                  if (process.env.NODE_ENV == 'development') {
                    preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + preShipmentFilesCoa[i]['coa_file_name'];
                  } else {
                    preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(preShipmentFilesCoa[i]['invoice_number'] + '#' + data[0].task_id + '#' + preShipmentFilesCoa[i]['coa_id']);
                  }
                  preShipmentFilesCoa[i]['coa_id'] = cryptr.encrypt(preShipmentFilesCoa[i]['coa_id'] + "#" + preShipmentFilesCoa[i]['invoice_number']);
                }
                preShipmentInfo[indps]['coa_files'] = preShipmentFilesCoa;*/
                preShipmentInfo[indps]['coa_files'] = [];
              }
            }
          }

          var customer_id = data[0].customer_id;

          let customerDetails = await customerModel.get_user(customer_id);

          var companyId = customerDetails[0].company_id;

          let company_arr = await module.exports.__get_company_list(companyId, req.user.role, req.user.customer_id);

          var ccCutomerList = await customerModel.get_cc_cust_company(company_arr, customer_id);

          var selCCCustList = await customerModel.list_cc_customer(task_id);

          //var trackShipmentList = await taskModel.list_track_shipment(task_id);

          var dataObj = {
            taskDetails: data[0],
            taskDetailsFiles: taskDetailsFiles,
            taskDetailsFilesCoa: taskDetailsFilesCoa,
            taskDetailsFilesInvoice: taskDetailsFilesInvoice,
            taskDetailsFilesPacking: taskDetailsFilesPacking,
            comments: commentArr,
            commentExists: hasComment,
            discussionExists: hasDiscussion,
            discussionFiles: output_arr,
            subTasks: subTasks,
            ccCutomerList: ccCutomerList,
            selCCCustList: selCCCustList,
            preSelectedCCList: [],
            shippingInfo: shippingInfo,
            shippingTab: shippingTab,
            paymentInfo: paymentInfo,
            preShipmentInfo: preShipmentInfo,
            preShipmentTab: preShipmentTab
          };

          if (dataObj.taskDetailsFiles.length > 1) {
            dataObj.downloadAllLink = process.env.FULL_PATH + '/api/tasks/download_files/' + hashids.encode(task_id);
          } else {
            dataObj.downloadAllLink = '';
          }

          res.status(200).json({
            status: 1,
            data: dataObj
          });
        })
        .catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          });
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  temp_task_orders: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;
      await taskModel
        .get_temp_task_orders(task_id)
        .then(async function (data) {
          console.log(data);
          for (let index = 0; index < data.length; index++) {
            if (data[index].quantity && trim(data[index].quantity) != '') {
              let q_arr = data[index].quantity.split(' ');
              if (q_arr[1] == 'KG') {
                q_arr[1] = "Kgs";
              }
              let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].task_language);
              data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
            }

            let t_product_name = await taskModel.get_translated_product_new(data[index].product_id, data[index].task_language);
            data[index].product_name = t_product_name[0].name;

          }

          res.status(200).json({
            status: 1,
            data: data
          });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          });
        });

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  //SVJT
  __get_shipping_details: async (sales_order_no, task_id) => {
    return new Promise(async (resolve, reject) => {
      let date_format = `dd/mm/yyyy`;
      let time_format = `h:MM TT`;
      let last_date = '';
      let all_invoiceDetails = [];

      await taskModel.get_invoice_details(sales_order_no)
        .then(async function (invoiceDetails) {
          if (invoiceDetails.length > 0) {

            let ret_arr = invoiceDetails.map((value, index) => {
              let arr = {};
              let shipping_process = 0;
              let inv_num = value.invoice_number
              arr[inv_num] = {
                "partner": value.logistics_partner,
                "organization": value.sales_org,
                "Invoice Doc": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: '',
                  file_hash: ''
                },
                "Packing List": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: '',
                  file_hash: ''
                },
                "Shipment Picked": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: ''
                },
                "AWB Doc": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: '',
                  file_type: '',
                  file_hash: ''
                },
                "Shipping Progress": {
                  extra_class: '',
                  display_date: ''
                },
                // "HAWB Doc":{
                //   extra_class:'',
                //   display_date:'',
                //   date:'',
                //   time:'',
                //   location:'',
                //   comment:''
                // },
                "Shipment Departed": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: ''
                },
                "Shipment Arrived": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: ''
                },
                "Delivered": {
                  extra_class: '',
                  display_date: '',
                  date: '',
                  time: '',
                  location: '',
                  comment: ''
                }
              };
              let extra_class_used = false;

              if (value.invoice_date != null && trim(value.invoice_date) != '') {
                let doc_time = dateFormat(common.calcInputTime(value.invoice_date), time_format);

                arr[inv_num]["Invoice Doc"]["display_date"] = dateFormat(common.calcInputTime(value.invoice_date), date_format);
                arr[inv_num]["Invoice Doc"]["date"] = dateFormat(common.calcInputTime(value.invoice_date), date_format);
                arr[inv_num]["Invoice Doc"]["time"] = (doc_time != '5:30 AM') ? doc_time : '';
                arr[inv_num]["Invoice Doc"]["location"] = value.invoice_location;
                arr[inv_num]["Invoice Doc"]["comment"] = value.invoice_file_name;
                arr[inv_num]["Invoice Doc"]['file_hash'] = cryptr.encrypt(inv_num + "#" + value.task_id);

                last_date = value.invoice_date;
              }

              if (value.packing_date != null && trim(value.packing_date) != '') {
                let pack_time = dateFormat(common.calcInputTime(value.packing_date), time_format);

                arr[inv_num]["Packing List"]["display_date"] = dateFormat(common.calcInputTime(value.packing_date), date_format);
                arr[inv_num]["Packing List"]["date"] = dateFormat(common.calcInputTime(value.packing_date), date_format);
                arr[inv_num]["Packing List"]["time"] = (pack_time != '5:30 AM') ? pack_time : '';
                arr[inv_num]["Packing List"]["location"] = value.packing_location;
                arr[inv_num]["Packing List"]["comment"] = value.packing_file_name;
                arr[inv_num]["Packing List"]['file_hash'] = cryptr.encrypt(inv_num + "#" + value.task_id);

                last_date = value.packing_date;
              } else {
                if (extra_class_used == false) {
                  extra_class_used = true;
                  arr[inv_num]["Invoice Doc"]["extra_class"] = 'nextActive';
                }
              }

              if (value.shipment_date != null && trim(value.shipment_date) != '') {

                arr[inv_num]["Shipment Picked"]["display_date"] = dateFormat(common.calcInputTime(value.shipment_date), date_format);
                arr[inv_num]["Shipment Picked"]["date"] = dateFormat(common.calcInputTime(value.shipment_date), date_format);
                arr[inv_num]["Shipment Picked"]["time"] = dateFormat(common.calcInputTime(value.shipment_date), time_format);
                arr[inv_num]["Shipment Picked"]["location"] = value.shipment_pickup_location;
                arr[inv_num]["Shipment Picked"]["comment"] = value.enter_invoice_no_shipment_pickup_comments;

                last_date = value.shipment_date;
              } else {
                if (extra_class_used == false) {
                  extra_class_used = true;
                  arr[inv_num]["Packing List"]["extra_class"] = 'nextActive';
                }
              }

              if (value.flight_booked_time != null && trim(value.flight_booked_time) != '') {
                arr[inv_num]["AWB Doc"]["extra_class"] = '';
                arr[inv_num]["AWB Doc"]["display_date"] = dateFormat(common.calcInputTime(value.flight_booked_time), date_format);
                arr[inv_num]["AWB Doc"]["date"] = dateFormat(common.calcInputTime(value.flight_booked_time), date_format);
                arr[inv_num]["AWB Doc"]["time"] = dateFormat(common.calcInputTime(value.flight_booked_time), time_format);
                arr[inv_num]["AWB Doc"]["location"] = value.flight_booked_location;
                arr[inv_num]["AWB Doc"]['file_hash'] = cryptr.encrypt(inv_num + "#" + value.task_id);
                if ((value.flight_booked_doc != '' && value.flight_booked_doc != null)) {
                  arr[inv_num]["AWB Doc"]["comment"] = value.flight_booked + ".pdf";
                  arr[inv_num]['AWB Doc']['file_type'] = 'flight';
                } else if (value.hawb_doc != '' && value.hawb_doc != null) {
                  arr[inv_num]["AWB Doc"]["comment"] = value.hawb + ".pdf";
                  arr[inv_num]['AWB Doc']['file_type'] = 'hawb';
                } else {
                  arr[inv_num]["AWB Doc"]["comment"] = '';
                }

                arr[inv_num]["Shipment Picked"]["extra_class"] = '';
                last_date = value.flight_booked_time;
              } else if (value.hawb_time != null && trim(value.hawb_time) != '') {
                arr[inv_num]["AWB Doc"]["extra_class"] = '';
                arr[inv_num]["AWB Doc"]["display_date"] = dateFormat(common.calcInputTime(value.hawb_time), date_format);
                arr[inv_num]["AWB Doc"]["date"] = dateFormat(common.calcInputTime(value.hawb_time), date_format);
                arr[inv_num]["AWB Doc"]["time"] = dateFormat(common.calcInputTime(value.hawb_time), time_format);
                arr[inv_num]["AWB Doc"]["location"] = value.hawb_location;
                arr[inv_num]["AWB Doc"]['file_hash'] = cryptr.encrypt(inv_num + "#" + value.task_id);
                if ((value.flight_booked_doc != '' && value.flight_booked_doc != null)) {
                  arr[inv_num]["AWB Doc"]["comment"] = value.flight_booked + ".pdf";
                  arr[inv_num]['AWB Doc']['file_type'] = 'flight';
                } else if (value.hawb_doc != '' && value.hawb_doc != null) {
                  arr[inv_num]["AWB Doc"]["comment"] = value.hawb + ".pdf";
                  arr[inv_num]['AWB Doc']['file_type'] = 'hawb';
                } else {
                  arr[inv_num]["AWB Doc"]["comment"] = '';
                }

                arr[inv_num]["Shipment Picked"]["extra_class"] = '';
                last_date = value.hawb_time;
              } else {
                if (extra_class_used == false && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["Shipment Picked"]["extra_class"] = 'nextActive';
                }
              }

              if ((value.approval_for_document_from_consignee_time != null && trim(value.approval_for_document_from_consignee_time) != '')) {
                shipping_process++;
                arr[inv_num]["Shipping Progress"]["extra_class"] = '';
                arr[inv_num]["Shipping Progress"]["display_date"] = dateFormat(common.calcInputTime(value.approval_for_document_from_consignee_time), date_format);

                arr[inv_num]["Shipping Progress"][0] = {
                  date: dateFormat(common.calcInputTime(value.approval_for_document_from_consignee_time), date_format),
                  time: dateFormat(common.calcInputTime(value.approval_for_document_from_consignee_time), time_format),
                  location: value.approval_for_document_from_consignee_location,
                  comment: value.await_approval_for_document_from_consignee
                };

                last_date = value.approval_for_document_from_consignee_time;
              } else {
                if (extra_class_used == false && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["AWB Doc"]["extra_class"] = 'nextActive';
                }
              }

              if ((value.approval_for_haz_clearance_from_consignee_time != null && trim(value.approval_for_haz_clearance_from_consignee_time) != '')) {
                shipping_process++;
                arr[inv_num]["Shipping Progress"]["extra_class"] = '';
                arr[inv_num]["Shipping Progress"]["display_date"] = dateFormat(common.calcInputTime(value.approval_for_haz_clearance_from_consignee_time), date_format);

                arr[inv_num]["Shipping Progress"][1] = {
                  date: dateFormat(common.calcInputTime(value.approval_for_haz_clearance_from_consignee_time), date_format),
                  time: dateFormat(common.calcInputTime(value.approval_for_haz_clearance_from_consignee_time), time_format),
                  location: value.approval_for_haz_clearance_from_consignee_location,
                  comment: value.await_approval_for_haz_clearance_from_consignee
                };

                last_date = value.approval_for_haz_clearance_from_consignee_time;
              } else {
                if (extra_class_used == false && shipping_process == 0 && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["AWB Doc"]["extra_class"] = 'nextActive';
                }
              }

              if ((value.fda_approval_time != null && trim(value.fda_approval_time) != '')) {
                shipping_process++;
                arr[inv_num]["Shipping Progress"]["extra_class"] = '';
                arr[inv_num]["Shipping Progress"]["display_date"] = dateFormat(common.calcInputTime(value.fda_approval_time), date_format);


                arr[inv_num]["Shipping Progress"][2] = {
                  date: dateFormat(common.calcInputTime(value.fda_approval_time), date_format),
                  time: dateFormat(common.calcInputTime(value.fda_approval_time), time_format),
                  location: value.fda_approval_location,
                  comment: value.fda_approval_awaited
                };

                last_date = value.fda_approval_time;
              } else {
                if (extra_class_used == false && shipping_process == 0 && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["AWB Doc"]["extra_class"] = 'nextActive';
                }
              }

              if ((value.customs_clearance_completion_time != null && trim(value.customs_clearance_completion_time) != '')) {
                shipping_process++;
                arr[inv_num]["Shipping Progress"]["extra_class"] = '';
                arr[inv_num]["Shipping Progress"]["display_date"] = dateFormat(common.calcInputTime(value.customs_clearance_completion_time), date_format);


                arr[inv_num]["Shipping Progress"][3] = {
                  date: dateFormat(common.calcInputTime(value.customs_clearance_completion_time), date_format),
                  time: dateFormat(common.calcInputTime(value.customs_clearance_completion_time), time_format),
                  location: value.customs_clearance_completion_location,
                  comment: value.customs_clearance
                };

                last_date = value.customs_clearance_completion_time;
              } else {
                if (extra_class_used == false && shipping_process == 0 && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["AWB Doc"]["extra_class"] = 'nextActive';
                }
              }

              if (shipping_process == 0) {
                delete (arr[inv_num]['Shipping Progress']);
              }

              if (value.shipment_departed != null && trim(value.shipment_departed) != '') {
                arr[inv_num]["Shipment Departed"]["extra_class"] = '';
                arr[inv_num]["Shipment Departed"]["display_date"] = dateFormat(common.calcInputTime(value.shipment_departed), date_format);
                arr[inv_num]["Shipment Departed"]["date"] = dateFormat(common.calcInputTime(value.shipment_departed), date_format);
                arr[inv_num]["Shipment Departed"]["time"] = dateFormat(common.calcInputTime(value.shipment_departed), time_format);
                arr[inv_num]["Shipment Departed"]["location"] = value.shipment_departed_location;

                last_date = value.shipment_departed;
              } else {
                if (extra_class_used == false && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  if (shipping_process == 0) {
                    arr[inv_num]["AWB Doc"]["extra_class"] = 'nextActive';
                  } else {
                    arr[inv_num]["Shipping Progress"]["extra_class"] = 'nextActive';
                  }
                }
              }

              if (value.shipment_arrived_at_destination != null && trim(value.shipment_arrived_at_destination) != '') {
                arr[inv_num]["Shipment Arrived"]["extra_class"] = '';
                arr[inv_num]["Shipment Arrived"]["display_date"] = dateFormat(common.calcInputTime(value.shipment_arrived_at_destination), date_format);
                arr[inv_num]["Shipment Arrived"]["date"] = dateFormat(common.calcInputTime(value.shipment_arrived_at_destination), date_format);
                arr[inv_num]["Shipment Arrived"]["time"] = dateFormat(common.calcInputTime(value.shipment_arrived_at_destination), time_format);
                arr[inv_num]["Shipment Arrived"]["location"] = value.shipment_arrived_location;

                last_date = value.shipment_arrived_at_destination;
              } else {
                if (extra_class_used == false && value.tracking_info_updated == 'false') {
                  extra_class_used = true;
                  arr[inv_num]["Shipment Departed"]["extra_class"] = 'nextActive';
                }
              }

              if (value.delivery_at_customer_destination != null && trim(value.delivery_at_customer_destination) != '') {
                arr[inv_num]["Delivered"]["extra_class"] = '';
                arr[inv_num]["Delivered"]["display_date"] = dateFormat(common.calcInputTime(value.delivery_at_customer_destination), date_format);
                arr[inv_num]["Delivered"]["date"] = dateFormat(common.calcInputTime(value.delivery_at_customer_destination), date_format);
                arr[inv_num]["Delivered"]["time"] = dateFormat(common.calcInputTime(value.delivery_at_customer_destination), time_format);
                arr[inv_num]["Delivered"]["location"] = value.delivery_at_customer_destination_location;

              } else {
                if (value.tracking_info_updated == 'true') {
                  arr[inv_num]["Delivered"]["extra_class"] = '';
                  arr[inv_num]["Delivered"]["display_date"] = dateFormat(common.calcInputTime(last_date), date_format);
                  arr[inv_num]["Delivered"]["date"] = dateFormat(common.calcInputTime(last_date), date_format);
                  arr[inv_num]["Delivered"]["time"] = dateFormat(common.calcInputTime(last_date), time_format);
                  arr[inv_num]["Delivered"]["location"] = "";

                  if (arr[inv_num]["Shipment Departed"]["display_date"] == '' || arr[inv_num]["Shipment Departed"]["display_date"] == null) {
                    arr[inv_num]["Shipment Departed"]["display_date"] = dateFormat(common.calcInputTime(last_date), date_format);
                  }
                  if (arr[inv_num]["Shipment Arrived"]["display_date"] == '' || arr[inv_num]["Shipment Arrived"]["display_date"] == null) {
                    arr[inv_num]["Shipment Arrived"]["display_date"] = dateFormat(common.calcInputTime(last_date), date_format);
                  }
                } else {
                  if (extra_class_used == false) {
                    extra_class_used = true;
                    arr[inv_num]["Shipment Arrived"]["extra_class"] = 'nextActive';
                  }
                }
              }

              return arr;

            });

            all_invoiceDetails = ret_arr;
          }
          await taskModel.getBlueDartShipping(sales_order_no)
            .then(async function (bluedartDetails) {
              if (bluedartDetails.length > 0) {
                let ret_arr = bluedartDetails.map(async (valbd, indbd) => {
                  let arr = {};
                  let inv_num = parseInt(valbd.invoice_number);
                  let waybill_no = valbd.logistics_partner.match(/(\d+)/);
                  if (waybill_no) {

                    arr[inv_num] = {
                      "partner": "bluedart",
                      "organization": valbd.sales_org,
                      "invoice_no": inv_num,
                      "invoice_display_date": dateFormat(common.calcInputTime(valbd.invoice_date), date_format),
                      "invoice_location": valbd.invoice_location,
                      "invoice_file_name": valbd.invoice_file_name,
                      "invoice_file_hash": (valbd.invoice_file_name != '') ? cryptr.encrypt(inv_num + "#" + valbd.task_id) : '',
                      "packing_display_date": dateFormat(common.calcInputTime(valbd.packing_date), date_format),
                      "packing_location": valbd.packing_location,
                      "packing_file_name": valbd.packing_file_name,
                      "packing_file_hash": (valbd.packing_file_name != '') ? cryptr.encrypt(inv_num + "#" + valbd.task_id) : '',
                      "ref_no": '',
                      "origin": '',
                      "destination": '',
                      "pick_up_date": '',
                      "pick_up_time": '',
                      "expected_delivery_date": '',
                      "dynamic_expected_delivery_date": '',
                      "shipping_steps": []
                    };

                    await taskModel.getBlueDartShippingSteps(waybill_no[0])
                      .then(async function (ship_step) {
                        if(ship_step.length>0){
                          let ret_step_arr = ship_step.map(async (valss, indss) => {
                            let arr_step = valss;
                            if(ship_step[indss].scan_code == 'POD'){
                              ship_step[indss].file_path = (ship_step[indss].file_path !='') ?  cryptr.encrypt(inv_num + "#" + valbd.task_id) : '';
                            }
                            return arr_step;
                          });
                          arr[inv_num].ref_no = ship_step[0].ref_no;
                          arr[inv_num].origin = ship_step[0].origin;
                          arr[inv_num].destination = ship_step[0].destination;
                          arr[inv_num].pick_up_date = ship_step[0].pick_up_date;
                          arr[inv_num].pick_up_time = ship_step[0].pick_up_time;
                          arr[inv_num].expected_delivery_date = ship_step[0].expected_delivery_date;
                          arr[inv_num].dynamic_expected_delivery_date = ship_step[0].dynamic_expected_delivery_date;
                          arr[inv_num].shipping_steps = ship_step;
                        }
                      })
                  }
                  return arr;
                });

                Promise.all(ret_arr).then(async function (results) {
                  if (results.length > 0) {
                    for (let inr = 0; inr < results.length; inr++) {
                      const element = results[inr];
                      all_invoiceDetails.push(element);
                    }
                  }
                  resolve(all_invoiceDetails);
                })
              } else {
                resolve(all_invoiceDetails);
              }
            })
            .catch(err => {
              common.logError(err);
            })


        }).catch(err => {
          resolve(all_invoiceDetails);
        })


    });
  },
  update_tour_counter: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_tour_counter(req.user.customer_id);
    } else {
      await customerModel.update_tour_counter(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  update_tour_counter_new: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_tour_counter_new(req.user.customer_id);
    } else {
      await customerModel.update_tour_counter_new(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  update_tour_counter_closed_task: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_tour_counter_closed_task(req.user.customer_id);
    } else {
      await customerModel.update_tour_counter_closed_task(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },

  update_tour_counter_11: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_tour_counter_11(req.user.customer_id);
    } else {
      await customerModel.update_tour_counter_11(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },

  tour_done: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.tour_done(req.user.customer_id);
    } else {
      await customerModel.tour_done(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  tour_done_new: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.tour_done_new(req.user.customer_id);
    } else {
      await customerModel.tour_done_new(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  tour_done_closed_task: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.tour_done_closed_task(req.user.customer_id);
    } else {
      await customerModel.tour_done_closed_task(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  tour_done_11: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.tour_done_11(req.user.customer_id);
    } else {
      await customerModel.tour_done_11(req.user.customer_id);
    }

    res.status(200).json({
      status: 1,
      data: 'updated'
    });
  },
  get_task_attachments: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;

      let task_files = await taskModel.select_task_files_send_mail(task_id);
      let task_discussion_files = await taskModel.select_task_discussion_files_send_mail(task_id);

      res.status(200).json({
        status: 1,
        data: [{ task_files: task_files, task_discussion_files: task_discussion_files }]
      });

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  send_mail_task_details_attachments: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;
      const customer_id = req.user.customer_id;
      const { task_attachments, subject, notes, exclude_task_ref } = req.body;
      let { email_list } = req.body;

      email_list = JSON.parse(email_list);

      let task_attachments_parsed = JSON.parse(task_attachments);

      let task_details = await taskModel.tasks_details(task_id);

      let user_details = [];

      if (req.user.role == 2) {
        user_details = await agentModel.get_user(customer_id);
      } else {
        user_details = await customerModel.get_user(customer_id);
      }

      email_list.push(user_details[0].email);

      //console.log(user_details);
      console.log(task_details);

      let attachments = [];

      if (task_attachments_parsed[0].task_files.length > 0) {
        for (let index = 0; index < task_attachments_parsed[0].task_files.length; index++) {
          const element = task_attachments_parsed[0].task_files[index];

          if (element.chk === true) {
            let s3_key = await taskModel.select_task_files_send_mail_single(element.upload_id);
            let buffer = await module.exports.__get_s3_buffer_base64(s3_key);
            let base_64_buffer = new Buffer(buffer, 'base64');
            attachments.push(
              {
                filename: element.actual_file_name,
                content: base_64_buffer,
                encoding: "base64"
              }
            );
          }

        }
      }

      if (task_attachments_parsed[0].task_discussion_files.length > 0) {
        for (let index = 0; index < task_attachments_parsed[0].task_discussion_files.length; index++) {
          const element = task_attachments_parsed[0].task_discussion_files[index];

          if (element.chk === true) {
            let s3_key = await taskModel.select_task_discussion_files_send_mail_single(element.upload_id);
            let buffer = await module.exports.__get_s3_buffer_base64(s3_key);
            let base_64_buffer = new Buffer(buffer, 'base64');
            attachments.push(
              {
                filename: element.actual_file_name,
                content: base_64_buffer,
                encoding: "base64"
              }
            );
          }

        }
      }

      //MULTER ATTACHMENT AMAN
      if (req.files && req.files.length > 0) {
        for (let index = 0; index < req.files.length; index++) {
          const element = req.files[index];
          //SVJT da ki korbo go ekhane

          let buffer = await module.exports.__get_s3_buffer_base64(element.filename);
          let base_64_buffer = new Buffer(buffer, 'base64');
          attachments.push(
            {
              filename: element.originalname,
              content: base_64_buffer,
              encoding: "base64"
            }
          );
        }
      }
      //console.log(attachments);

      let mail_subject = subject;

      // =========== COUNTRY NAME MULTIPLE ========//
      var country = await taskModel.tasks_details_countries(task_details[0].task_id);

      var quantity_by_lang = task_details[0].quantity;
      if (task_details[0].quantity && trim(task_details[0].quantity) != '' && task_details[0].language != Config.default_language) {
        let q_arr = task_details[0].quantity.split(' ');
        let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
        quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
      }

      //========================================//

      if (task_details[0].language != Config.default_language) {
        //COUNTRY
        let t_country = await taskModel.get_translated_country(task_id, task_details[0].language);
        task_details[0].country_name = t_country[0].country_name;

        if (task_details[0].request_type == 13 && task_details[0].stability_data_type != '') {
          let t_stability_data_type = await taskModel.get_translated_stability_data_type(task_details[0].stability_data_type, task_details[0].language);
          task_details[0].stability_data_type = t_stability_data_type[0].name;
        }

        let t_request_type = await taskModel.get_translated_request_name(task_details[0].request_type, task_details[0].language);
        task_details[0].req_name = t_request_type[0].name;

        if (task_details[0].request_type != 24) {
          let t_product_name = await taskModel.get_translated_product(task_details[0].product_id, task_details[0].language);

          task_details[0].product_name = t_product_name[0].name;
        }

        if (task_details[0].request_type == 38 && task_details[0].apos_document_type != '') {
          let t_apos_document_type = await taskModel.get_translated_apos_document_type(task_details[0].apos_document_type, task_details[0].language);
          task_details[0].apos_document_type = t_apos_document_type;
        }

        if (task_details[0].quantity && trim(task_details[0].quantity) != '') {
          let q_arr = task_details[0].quantity.split(' ');
          let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
          task_details[0].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
        }

        if (task_details[0].impurities_quantity && trim(task_details[0].impurities_quantity) != '') {
          let q_arr = task_details[0].impurities_quantity.split(' ');
          let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
          task_details[0].impurities_quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
        }

        if (task_details[0].working_quantity && trim(task_details[0].working_quantity) != '') {
          let q_arr = task_details[0].working_quantity.split(' ');
          let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
          task_details[0].working_quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
        }


      } else {
        task_details[0].country_name = country[0].country_name;
      }

      let hasDiscussion;
      let commentArr = [];
      let discussionExists = await taskModel.discussion_exists(task_id);
      if (discussionExists.length > 0) {
        hasDiscussion = true;
        commentArr = await taskModel.select_discussion_comment_log(discussionExists[0].discussion_id);
      } else {
        hasDiscussion = false;
      }

      let discussion_html = ``;
      if (hasDiscussion == true && commentArr.length > 0) {
        discussion_html += `<p>${await common.getTranslatedTextMail('Task Discussions', task_details[0].language)}</p><table  style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ccc;" width="700" border="1" cellpadding="10" cellspacing="0">`;
        discussion_html += `<tr><td style="background:#502a91; color:#fff;">${await common.getTranslatedTextMail('Name', task_details[0].language)}</td><td style="background:#502a91; color:#fff;">${await common.getTranslatedTextMail('Date Added', task_details[0].language)}</td><td style="background:#502a91; color:#fff;">${await common.getTranslatedTextMail('Comment', task_details[0].language)}</td></tr>`;
        for (let index = 0; index < commentArr.length; index++) {
          const element = commentArr[index];
          let name = '';
          if (element.added_by_type == "C") {
            name = `${element.customer_first_name} ${element.customer_last_name}`
          } else if (element.added_by_type == "A") {
            name = `${element.agents_first_name} ${element.agents_last_name}`;
          } else if (element.added_by_type == "E") {
            name = `${element.employee_first_name} ${element.employee_last_name}`;
          }

          let comm = ''
          console.log(task_details[0].language, ' === ', element.language);
          if (task_details[0].language == element.language) {
            comm = element.comment;
          } else {
            comm = element.translated_comment;
          }

          discussion_html += `<tr><td>${name}</td><td>${element.date_added}</td><td>${entities.decode(comm)}</td></tr>`;

        }
        discussion_html += `</table>`;
      }

      let request_type = task_details[0].request_type;

      var mail_pharmacopoeia = '';
      var mail_polymorphic_form = '';
      var mail_stability_data_type = '';
      var mail_audit_visit_site_name = '';
      var mail_request_audit_visit_date = '';
      var mail_quantity = '';
      var mail_batch_number = '';
      var mail_nature_of_issue = '';
      var mail_shipping_address = '';
      var mail_samples = '';
      var mail_impurities = '';
      var mail_working_standards = '';
      var mail_rdd = "";
      var mail_apr_url = "";
      var mail_dmf_number = "";
      var mail_apos_document_type = "";
      var mail_notification_number = "";
      var mail_rdfrc = "";

      if (request_type === 1) {
        var req_type_arr = task_details[0].service_request_type.split(
          ","
        );

        if (req_type_arr.indexOf("Samples") !== -1) {
          mail_samples = `<tr><td>${await common.getTranslatedTextMail('Samples', task_details[0].language)}</td><td></td></tr>`;

          if (task_details[0].number_of_batches != null) {
            mail_samples +=
              `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', task_details[0].language)}</td><td>${task_details[0].number_of_batches}</td></tr>`;
          } else {
            mail_samples += `<tr><td>${await common.getTranslatedTextMail('Number Of Batches', task_details[0].language)}</td><td></td></tr>`;
          }

          if (task_details[0].quantity != null) {

            // not required as previously translated in line no 10143 (blocked by Debadrita)
            // if (task_details[0].quantity && trim(task_details[0].quantity) != '') {  
            //   let q_arr = task_details[0].quantity.split(' ');
            //   console.log("Quantity arr : ",q_arr);
            //   let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //   console.log("Translated Quantity arr : ",t_q_arr);
            //   quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            // }

            mail_samples += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td>${quantity_by_lang}</td></tr>`;
          } else {
            mail_samples += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td></td></tr>`;
          }
        } else {
          mail_samples = "";
        }

        if (req_type_arr.indexOf("Working Standards") !== -1) {
          mail_working_standards = `<tr><td>${await common.getTranslatedTextMail('Working Standards', task_details[0].language)}</td><td></td></tr>`;

          if (task_details[0].working_quantity != null) {

            // not required as previously translated in line no 10155 (blocked by Debadrita)
            // if (task_details[0].working_quantity && trim(task_details[0].working_quantity) != '') {
            //   let q_arr = task_details[0].working_quantity.split(' ');
            //   let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //   quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            // }

            mail_working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td>${quantity_by_lang}</td></tr>`;
          } else {
            mail_working_standards += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td></td></tr>`;
          }
        } else {
          mail_working_standards = "";
        }

        if (req_type_arr.indexOf("Impurities") !== -1) {
          mail_impurities = `<tr><td>${await common.getTranslatedTextMail('Impurities', task_details[0].language)}</td><td></td></tr>`;

          if (task_details[0].impurities_quantity != null) {

            // not required as previously translated in line no 10149 (blocked by Debadrita)
            // if (db_task_details[0].impurities_quantity && trim(task_details[0].impurities_quantity) != '') {
            //   let q_arr = task_details[0].impurities_quantity.split(' ');
            //   let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], task_details[0].language);
            //   quantity_by_lang = `${q_arr[0]} ${t_q_arr[0].name}`;
            // }

            mail_impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td>${quantity_by_lang}</td></tr>`;
          } else {
            mail_impurities += `<tr><td>${await common.getTranslatedTextMail('Quantity', task_details[0].language)}</td><td></td></tr>`;
          }

          if (task_details[0].specify_impurity_required != null) {
            mail_impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', task_details[0].language)}</td><td>${task_details[0].specify_impurity_required}</td></tr>`;
          } else {
            mail_impurities += `<tr><td>${await common.getTranslatedTextMail('Specify Impurity Required', task_details[0].language)}</td><td></td></tr>`;
          }
        } else {
          mail_impurities = "";
        }

        mail_shipping_address = `<tr><td>${await common.getTranslatedTextMail('Shipping Address', task_details[0].language)}</td><td>${task_details[0].shipping_address}</td></tr>`;
        mail_pharmacopoeia = `<tr><td>${await common.getTranslatedTextMail('Pharmacopoeia', task_details[0].language)}</td><td>${task_details[0].pharmacopoeia}</td></tr>`;


      } else {
        mail_shipping_address = "";
        mail_samples = "";
        mail_working_standards = "";
        mail_impurities = "";
      }

      if (request_type === 7 || request_type === 23 || request_type === 34 || request_type === 31 || request_type === 41 || request_type === 44) {
        mail_quantity =
          "<tr><td>Quantity</td><td>" +
          task_details[0].quantity +
          "</td></tr>";
      } else {
        mail_quantity = "";
      }

      if (request_type === 23 || request_type === 41) {
        let rdd = (task_details[0].rdd != null) ? common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy') : '';
        mail_rdd = '<tr><td>Requested Date Of Delivery</td><td>' + rdd + '</td></tr>';
      } else {
        mail_rdd = '';
      }

      if (request_type === 7 || request_type === 34) {
        mail_nature_of_issue =
          "<tr><td>Nature Of Issue</td><td>" +
          task_details[0].nature_of_issue +
          "</td></tr>";
        mail_batch_number =
          "<tr><td>Batch No</td><td>" +
          task_details[0].batch_number +
          "</td></tr>";
      } else {
        mail_nature_of_issue = "";
        mail_batch_number = "";
      }

      if (
        request_type == 22 ||
        request_type == 35 ||
        request_type == 36 ||
        request_type == 37 ||
        request_type == 21 ||
        request_type == 19 ||
        request_type == 18 ||
        request_type == 17 ||
        request_type == 2 ||
        request_type == 3 ||
        request_type == 4 ||
        request_type == 10 ||
        request_type == 12 ||
        request_type == 30 ||
        request_type == 32 ||
        request_type == 33 ||
        request_type == 35 ||
        request_type == 36 ||
        request_type == 37 ||
        request_type == 44
      ) {
        mail_pharmacopoeia =
          "<tr><td>Pharmacopoeia</td><td>" +
          task_details[0].pharmacopoeia +
          "</td></tr>";
      } else {
        mail_pharmacopoeia = "";
      }

      if (request_type === 2 || request_type === 3) {
        mail_polymorphic_form =
          "<tr><td>Polymorphic Form</td><td>" +
          task_details[0].polymorphic_form +
          "</td></tr>";
      } else {
        mail_polymorphic_form = "";
      }

      if (request_type === 13) {
        mail_stability_data_type =
          "<tr><td>Stability Data Type</td><td>" +
          task_details[0].stability_data_type +
          "</td></tr>";
      } else {
        mail_stability_data_type = "";
      }

      if (request_type === 9) {
        mail_audit_visit_site_name =
          "<tr><td>Site Name</td><td>" +
          task_details[0].audit_visit_site_name +
          "</td></tr>";

        var visit_date = new Date(
          task_details[0].request_audit_visit_date
        );
        var visit_date_updt = common.changeDateFormat(
          visit_date,
          "/",
          "dd.mm.yyyy"
        );

        mail_request_audit_visit_date =
          "<tr><td>Audit/Visit Date</td><td>" +
          visit_date_updt +
          "</td></tr>";
      } else {
        mail_audit_visit_site_name = "";
        mail_request_audit_visit_date = "";
      }

      if (request_type == 27) {
        mail_dmf_number = '<tr><td>DMF Number</td><td>' + task_details[0].dmf_number + '</td></tr>';
      } else {
        mail_dmf_number = '';
      }

      if (request_type == 29) {
        mail_notification_number = '<tr><td>Notification Number</td><td>' + task_details[0].notification_number + '</td></tr>';
      } else {
        mail_notification_number = '';
      }

      if (request_type == 28) {

        if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
          var rdfrc_date_updt = '';
        } else {
          var rdfrc_date = new Date(task_details[0].rdfrc);
          var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
        }

        mail_rdfrc = '<tr><td>Requested date of response/closure</td><td>' + rdfrc_date_updt + '</td></tr>';
      } else {
        mail_rdfrc = '';
      }

      if (request_type == 38) {
        mail_apos_document_type = '<tr><td>Document Type</td><td>' + task_details[0].apos_document_type + '</td></tr>';
      } else {
        mail_apos_document_type = '';
      }

      let task_ref_html = '';
      if (exclude_task_ref == false) {
        task_ref_html = `<tr>
            <td width="200">${await common.getTranslatedTextMail('Task Ref', task_details[0].language)}</td>
            <td>${task_details[0].task_ref}</td>
        </tr>`;
      }

      let notes_html = ''
      if (notes != '') {
        notes_html = `<p>${notes}</p>`;
      }

      // common.sendMail({
      //   from: Config.webmasterMail,
      //   to: email_list,
      //   subject: mail_subject,
      //   attachments:attachments,
      //   html: `
      //   <!DOCTYPE html>
      //   <html>
      //      <body>
      //         <p>This message has been sent from system on behalf of "${user_details[0].first_name} ${user_details[0].last_name} (${user_details[0].email})" - please reply to this user for further queries.</p>
      //         <p>------------------------------------------------------------------</p>
      //         <p>Hello,</p>
      //         ${notes_html}
      //         <p>Task Details are as below,</p>
      //         <table>
      //           ${task_ref_html}
      //           <tr>
      //               <td>Request Type</td>
      //               <td>${task_details[0].req_name}</td>
      //           </tr>
      //           <tr>
      //               <td>Product</td>
      //               <td>${task_details[0].product_name}</td>
      //           </tr>
      //           <tr>
      //               <td>Market</td>
      //               <td>${task_details[0].country_name}</td>
      //           </tr>
      //           ${mail_pharmacopoeia}
      //           ${mail_polymorphic_form}
      //           ${mail_stability_data_type}
      //           ${mail_audit_visit_site_name}
      //           ${mail_request_audit_visit_date}
      //           ${mail_quantity}
      //           ${mail_batch_number}
      //           ${mail_nature_of_issue}
      //           ${mail_samples}
      //           ${mail_rdd}
      //           ${mail_working_standards}
      //           ${mail_impurities}
      //           ${entities.decode(mail_shipping_address)}
      //           ${mail_dmf_number}
      //           ${mail_notification_number}
      //           ${mail_rdfrc}
      //           ${mail_apos_document_type}
      //           <tr>
      //               <td>Requirement</td>
      //               <td>${entities.decode(task_details[0].content)}</td>
      //           </tr>
      //         </table>

      //         ${discussion_html}

      //      </body>
      //   </html>
      //   `
      // });

      var country_name_by_lang = task_details[0].country_name;
      if (entities.decode(task_details[0].language) != Config.default_language) {
        //COUNTRY
        let t_country = await taskModel.get_translated_country(task_id, entities.decode(task_details[0].language));
        country_name_by_lang = t_country[0].country_name;
      }
      var req_name_lang = task_details[0].req_name;
      var product_name_by_lang = task_details[0].product_name;
      switch (entities.decode(task_details[0].language)) {
        case 'es':
          req_name_lang = task_details[0].req_name_es;
          product_name_by_lang = task_details[0].product_name_es;
          break;
        case 'pt':
          req_name_lang = task_details[0].req_name_pt;
          product_name_by_lang = task_details[0].product_name_pt;
          break;
        case 'ja':
          req_name_lang = task_details[0].req_name_ja;
          product_name_by_lang = task_details[0].product_name_ja;
          break;
        case 'zh':
          req_name_lang = task_details[0].req_name_zh;
          product_name_by_lang = task_details[0].product_name_zh;
          break;
        default:
          req_name_lang = task_details[0].req_name;
          product_name_by_lang = task_details[0].product_name;
          break;
      }
      if (product_name_by_lang == '' || product_name_by_lang == null) {
        product_name_by_lang = task_details[0].product_name;
      }

      console.log("RT : ", request_type);
      if (request_type === 4 || request_type === 10 || request_type === 12 || request_type === 17 || request_type === 18 || request_type === 19 || request_type === 21 || request_type === 22 || request_type == 30 || request_type == 32 || request_type == 33 || request_type == 35 || request_type == 36 || request_type == 37 || request_type === 42) {

        let mailParam = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          pharmacopoeia: task_details[0].pharmacopoeia,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_COMMON_15TYPES', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam, attachments, "c");

      } else if (request_type === 5 || request_type === 6 || request_type === 8 || request_type === 11 || request_type === 14 || request_type === 15 || request_type === 16 || request_type === 20) {

        let mailParam1 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_COMMON_8TYPES', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam1, attachments, "c");

      } else if (request_type === 2 || request_type === 3) {

        let mailParam2 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          pharmacopoeia: task_details[0].pharmacopoeia,
          polymorphic_form: task_details[0].polymorphic_form,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_COA_AND_MOA', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam2, attachments, "c");

      } else if (request_type === 7 || request_type === 34) {

        let mailParam3 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          nature_of_issue: task_details[0].nature_of_issue,
          quantity: quantity_by_lang,
          batch_number: task_details[0].batch_number,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam3, attachments, "c");

      } else if (request_type === 9) {

        var visit_date = new Date(task_details[0].request_audit_visit_date);
        var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');
        let mailParam4 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          audit_visit_site_name: task_details[0].audit_visit_site_name,
          audit_visit_date: visit_date_updt,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam4, attachments, "c");

      } else if (request_type === 13) {

        var stability_data_type_by_lang = task_details[0].stability_data_type;
        if (task_details[0].language != Config.default_language) {
          let t_stability_data_type = await taskModel.get_translated_stability_data_type(task_details[0].stability_data_type, task_details[0].language);
          stability_data_type_by_lang = t_stability_data_type[0].name;
        }

        let mailParam5 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          stability_data_type: stability_data_type_by_lang,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_STABILITY_DATA', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam5, attachments, "c");

      } else if (request_type === 23) {

        let mailParam6 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          quantity: quantity_by_lang,
          rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_NEW_ORDER', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam6, attachments, "c");

      } else if (request_type === 41) {

        let mailParam7 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          quantity: quantity_by_lang,
          rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
          pharmacopoeia: task_details[0].pharmacopoeia,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam7, attachments, "c");

      } else if (request_type === 31) {

        let mailParam8 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          quantity: quantity_by_lang,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_PRICE_QUOTE', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam8, attachments, "c");

      } else if (request_type === 27) {

        let mailParam9 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          dmf_number: task_details[0].dmf_number,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_DMF_QUERY', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam9, attachments, "c");

      } else if (request_type === 28) {

        if (task_details[0].rdfrc == '' || task_details[0].rdfrc == null) {
          var rdfrc_date_updt = '';
        } else {
          var rdfrc_date = new Date(task_details[0].rdfrc);
          var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
        }
        let mailParam10 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          rdfrc_date_updt: rdfrc_date_updt,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_DEFICIENCIES_QUERY', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam10, attachments, "c");

      } else if (request_type === 29) {

        let mailParam11 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          notification_number: task_details[0].notification_number,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_NOTIFICATION_QUERY', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam11, attachments, "c");

      } else if (request_type === 38) {

        var document_type_by_lang = task_details[0].apos_document_type;
        if (task_details[0].language != Config.default_language) {
          let t_apos_document_type = await taskModel.get_translated_apos_document_type(task_details[0].apos_document_type, task_details[0].language);
          document_type_by_lang = t_apos_document_type;
        }
        let mailParam12 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          document_type: document_type_by_lang,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam12, attachments, "c");

      } else if (request_type === 39) {

        let mailParam13 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          dmf_number: task_details[0].dmf_number,
          rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam13, attachments, "c");

      } else if (request_type === 40) {

        let mailParam14 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
          gmp_clearance_id: task_details[0].gmp_clearance_id,
          tga_email_id: task_details[0].tga_email_id,
          applicant_name: task_details[0].applicant_name,
          doc_required: task_details[0].doc_required,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam14, attachments, "c");

      } else if (request_type === 24) {

        let mailParam15 = {
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_FORCAST', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam15, attachments, "c");
      } else if (request_type === 1) {
        let mailParam1 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          pharmacopoeia: mail_pharmacopoeia,
          samples: mail_samples,
          working_standards: mail_working_standards,
          impurities: mail_impurities,
          shipping_address: entities.decode(mail_shipping_address),
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_SAMPLE', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam1, attachments, "c");
      } else if (request_type === 44) {

        let mailParam16 = {
          product_name: product_name_by_lang,
          country_name: country_name_by_lang,
          req_name: req_name_lang,
          task_content: entities.decode(task_details[0].content),
          discussion_html: discussion_html,
          quantity: quantity_by_lang,
          mail_subject: mail_subject,
          task_ref_html: task_ref_html,
          notes_html: notes_html,
          first_name: user_details[0].first_name,
          last_name: user_details[0].last_name,
          email: user_details[0].email,
          pharmacopoeia: task_details[0].pharmacopoeia,
        };

        const mailSent = common.sendMailWithAttachByCodeB2C('CUSTOMER_TASK_DETAILS_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', email_list, Config.contactUsMail, '', entities.decode(task_details[0].language), mailParam16, attachments, "c");

      }



      res.status(200).json({
        status: 1
      });

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  send_mail_escalation: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;
      const customer_id = req.user.customer_id;
      const { content } = req.body;
      const files = req.files;

      console.log('files', files);

      let task_details = await taskModel.tasks_details(task_id);
      let selected_customer = customer_id;

      let user_details = [];

      if (req.user.role == 2) {
        user_details = await agentModel.get_user(customer_id);
        selected_customer = task_details[0].customer_id;
      } else {
        user_details = await customerModel.get_user(customer_id);
      }

      // let manager  = [];
      // let employee = await taskModel.get_allocated_manager(selected_customer);

      // if(employee[0].manager_id > 0){
      //   manager = await taskModel.get_employee_details(employee[0].manager_id);
      // }else{
      //   manager = employee;
      // }

      let owner = task_details[0].owner;
      let current_ownership = task_details[0].current_ownership;

      let to_arr = [owner, current_ownership];

      let owner_manager_arr = await taskModel.get_employee_manager(owner);

      if (owner_manager_arr && owner_manager_arr.length > 0 && owner_manager_arr[0].manager_id > 0) {
        to_arr.push(owner_manager_arr[0].manager_id);
      }

      let owner_current_ownership_arr = await taskModel.get_employee_manager(current_ownership);

      if (owner_current_ownership_arr && owner_current_ownership_arr.length > 0 && owner_current_ownership_arr[0].manager_id > 0) {
        to_arr.push(owner_current_ownership_arr[0].manager_id);
      }

      //GET UNIQUE ARRAY
      let unique_to_arr = to_arr.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
      //GET UNIQUE ARRAY

      let escalation_data = [
        customer_id,
        task_id,
        entities.encode(content),
        common.currentDateTime(),
        req.user.role
      ];

      let escalation = await taskModel.insert_customer_escalation(escalation_data);

      if (files && files.length > 0) {
        for (var j = 0; j < files.length; j++) {
          let file_details = [
            escalation.id,
            files[j].filename,
            files[j].originalname,
            common.currentDateTime()
          ]
          await taskModel.insert_customer_escalation_file(file_details);
        }
      }

      //console.log(user_details);
      //console.log(task_details);

      let attachments = [];

      if (files && files.length > 0) {
        for (let index = 0; index < files.length; index++) {
          const element = files[index];

          let buffer = await module.exports.__get_s3_buffer_base64(element.filename);
          let base_64_buffer = new Buffer(buffer, 'base64');
          attachments.push(
            {
              filename: element.originalname,
              content: base_64_buffer,
              encoding: "base64"
            }
          );

        }
      }

      let mail_subject = `Customer Task Escalation | ${task_details[0].task_ref} | ${task_details[0].req_name}`;

      // console.log('email',manager);

      // common.sendMail({
      //   from: Config.webmasterMail,
      //   to: manager[0].email,
      //   subject: mail_subject,
      //   attachments:attachments,
      //   html: `
      //   <!DOCTYPE html>
      //   <html>
      //      <body>
      //         <p>Hello ${ manager[0].first_name },</p>
      //         <p>The following task has been escalated : ${task_details[0].task_ref} by ${user_details[0].first_name} ${user_details[0].last_name}</p>
      //         <p>Comment:</p>
      //         <p>${content}</p>
      //      </body>
      //   </html>
      //   `
      // });

      let customer_name = `${user_details[0].first_name} ${user_details[0].last_name}`;
      let company_name = (req.user.role == 2) ? '' : `${user_details[0].company_name}`;
      let request_type = `${task_details[0].req_name}`;
      let product_name = `${task_details[0].product_name}`;

      //footer => Regards, Dr.Reddy's XCEED

      for (let index = 0; index < unique_to_arr.length; index++) {
        const element = unique_to_arr[index];

        if (element != null) {
          let user_details_emp = await taskModel.get_employee_details(element);

          var empMailParam = {
            first_name: user_details_emp[0].first_name,
            user_first_name: user_details[0].first_name,
            user_last_name: user_details[0].last_name,
            task_ref: task_details[0].task_ref,
            req_name: task_details[0].req_name,
            company_name: company_name,
            product_name: product_name,
            comment: content.trim(),
            sales_order_no: (task_details[0].request_type == 23 || task_details[0].request_type == 43) ? `${product_name} | ` : null,
          };
          let esclmailSent = common.sendMailWithAttachByCodeB2E('CUSTOMER_ESCALATION_MAIL_TO_EMPLOYEE', user_details_emp[0].email, Config.webmasterMail, '', empMailParam, attachments, "e");
        }
      }

      res.status(200).json({
        status: 1
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  edit_rdd: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;
      const { rdd, temp_id } = req.body;

      let order_details = await taskModel.get_temp_task_orders_by_id(temp_id);

      if (order_details && order_details.length > 0 && order_details[0].status == 0) {

        await taskModel.update_temp_task_orders_by_id(temp_id, rdd);
        let previous_data = await taskModel.get_previous_rdd(task_id);

        let customerData = await customerModel.get_user(previous_data[0].customer_id);
        let customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
        console.log(rdd, order_details[0].rdd);
        let params = {
          task_ref: previous_data[0].task_ref,
          request_type: previous_data[0].req_name,
          customer_name: customername,
          employee_recipient: `from ${dateFormat(common.calcInputTime(order_details[0].rdd), 'dd/mm/yyyy')} to ${dateFormat(common.calcInputTime(rdd), 'dd/mm/yyyy')}`
        }

        console.log(params);
        await taskModel.log_activity(67, task_id, params);


        await taskModel
          .get_temp_task_orders(task_id)
          .then(async function (data) {
            console.log("Data : ", data);
            for (let index = 0; index < data.length; index++) {
              let quantity_en = data[index].quantity;
              if (data[index].quantity && trim(data[index].quantity) != '') {
                let q_arr = data[index].quantity.split(' ');
                if (q_arr[1] == 'KG') {
                  q_arr[1] = "Kgs";
                }
                let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].task_language);
                data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
              }

              let t_product_name = await taskModel.get_translated_product_new(data[index].product_id, data[index].task_language);
              data[index].product_name = t_product_name[0].name;

              if (data[index].temp_id == temp_id) {
                // mail send
                //console.log("mail send for ", temp_id+"=="+rdd);
                let db_task_details = await taskModel.tasks_details(task_id);
                let customer_details = await customerModel.get_user(db_task_details[0].customer_id);
                let emp_details = await taskModel.get_allocated_employee(db_task_details[0].customer_id, 5);
                emp_details = await module.exports.__get_allocated_leave_employee(emp_details, db_task_details[0].customer_id);

                let manager = await taskModel.get_allocated_manager(db_task_details[0].customer_id);
                manager = await module.exports.__get_allocated_leave_employee(manager, db_task_details[0].customer_id);
                let product_name_en = await taskModel.get_translated_product_new(data[index].product_id, 'en');

                const managerMailParam = {
                  emp_first_name: manager[0].first_name,
                  first_name: entities.decode(customer_details[0].first_name),
                  company_name: entities.decode(customer_details[0].company_name),
                  task_ref: db_task_details[0].task_ref,
                  rdd: rdd,
                  product_name: product_name_en[0].name,
                  quantity: quantity_en
                };
                const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_REQUEST_TEMPLATE_FOR_RDD', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

                const employeeMailParam = {
                  emp_first_name: emp_details[0].first_name,
                  first_name: entities.decode(customer_details[0].first_name),
                  company_name: entities.decode(customer_details[0].company_name),
                  task_ref: db_task_details[0].task_ref,
                  rdd: rdd,
                  product_name: product_name_en[0].name,
                  quantity: quantity_en
                };
                const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_REQUEST_TEMPLATE_FOR_RDD', emp_details[0].email, Config.webmasterMail, '', employeeMailParam, "e");
              }

            }

            res.status(200).json({
              status: 1,
              data: data
            });
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            });
          });
      } else {

        let previous_data = await taskModel.get_previous_rdd(task_id);

        let arr = [
          dateFormat(common.calcInputTime(previous_data[0].rdd), 'yyyy-mm-dd'),
          rdd,
          common.currentDateTime(),
          null,
          0,
          0,
          task_id
        ]

        await taskModel.insert_history_rdd(arr);
        let customerData = await customerModel.get_user(previous_data[0].customer_id);
        let customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
        let params = {
          task_ref: previous_data[0].task_ref,
          request_type: previous_data[0].req_name,
          customer_name: customername,
          employee_recipient: `from ${dateFormat(common.calcInputTime(previous_data[0].rdd), 'dd/mm/yyyy')} to ${dateFormat(common.calcInputTime(rdd), 'dd/mm/yyyy')}`
        }

        console.log(params);
        await taskModel.log_activity(65, task_id, params);

        // send mail
        let db_task_details = await taskModel.tasks_details(task_id);

        let quantity_en = db_task_details[0].quantity;
        if (db_task_details[0].quantity && trim(db_task_details[0].quantity) != '') {
          let q_arr = db_task_details[0].quantity.split(' ');
          if (q_arr[1] == 'KG') {
            q_arr[1] = "Kgs";
          }
          let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], db_task_details[0].language);
          db_task_details[0].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;
        }
        let t_product_name = await taskModel.get_translated_product_new(db_task_details[0].product_id, db_task_details[0].language);
        db_task_details[0].product_name = t_product_name[0].name;

        let customer_details = await customerModel.get_user(db_task_details[0].customer_id);
        let emp_details = await taskModel.get_allocated_employee(db_task_details[0].customer_id, 5);
        emp_details = await module.exports.__get_allocated_leave_employee(emp_details, db_task_details[0].customer_id);

        let manager = await taskModel.get_allocated_manager(db_task_details[0].customer_id);
        manager = await module.exports.__get_allocated_leave_employee(manager, db_task_details[0].customer_id);
        let product_name_en = await taskModel.get_translated_product_new(db_task_details[0].product_id, 'en');

        const managerMailParam = {
          emp_first_name: manager[0].first_name,
          first_name: entities.decode(customer_details[0].first_name),
          company_name: entities.decode(customer_details[0].company_name),
          task_ref: db_task_details[0].task_ref,
          rdd: rdd,
          product_name: product_name_en[0].name,
          quantity: quantity_en
        };
        const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_REQUEST_TEMPLATE_FOR_RDD', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

        const employeeMailParam = {
          emp_first_name: emp_details[0].first_name,
          first_name: entities.decode(customer_details[0].first_name),
          company_name: entities.decode(customer_details[0].company_name),
          task_ref: db_task_details[0].task_ref,
          rdd: rdd,
          product_name: product_name_en[0].name,
          quantity: quantity_en
        };
        const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_REQUEST_TEMPLATE_FOR_RDD', emp_details[0].email, Config.webmasterMail, '', employeeMailParam, "e");


        res.status(200).json({
          status: 1
        });
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  __get_s3_buffer_base64: async (key) => {
    AWS.config.update({
      accessKeyId: Config.aws.accessKeyId,
      secretAccessKey: Config.aws.secretAccessKey
    });
    var s3 = new AWS.S3();

    var options = {
      Bucket: Config.aws.bucketName,
      Key: key,
    };
    return new Promise((resolve, reject) => {
      s3.getObject(options, function (err, data) {
        if (err === null) {
          resolve(data.Body);
        } else {
          resolve(null);
        }
      });
    });
  },
  /**
   * Generates a Get CC customer from a company
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Get CC customer from a company used for customer selection
   */
  get_cc: async (req, res, next) => {
    try {


      let company_arr = await module.exports.__get_company_list(req.user.company_id, req.user.role, req.user.customer_id);
      await customerModel.get_cc_cust_company(company_arr, req.user.customer_id)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data,
              pre_selected: []
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Rating Options
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Rating Options used for Rating Options selection
   */
  get_rating_options: async (req, res, next) => {
    try {
      const task_id = req.params.id;
      const posted_by = req.user.customer_id;

      // let language = 'en';
      // if (req.user.role == 2) {
      //   language = await agentModel.get_agent_language(posted_by);
      // }else{
      //   language = await customerModel.get_customer_language(posted_by);
      // }

      //console.log(posted_by);
      let language = await taskModel.get_task_language(task_id);


      let options = await taskModel.get_rating_options(language);
      let questions = await taskModel.get_rating_questions(language);

      res.status(200)
        .json({
          status: 1,
          options: options,
          questions: questions
        });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Task Status
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Task Status used for task selection
   */
  get_task_status: async (req, res, next) => {
    try {
      let lang = req.params.lang;
      let type = req.params.type;
      let id_arr = [];
      if (type == 'C') {
        id_arr = [1, 10, 3, 4];
      } else if (type == 'N') {
        id_arr = [5, 6, 7, 8, 9];
      } else {
        id_arr = [1, 2, 3, 4];
      }


      await taskModel.get_task_status(id_arr, lang)
        .then(function (data) {

          //console.log(data);

          let res_data = []

          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            res_data.push(element.name);
          }

          res.status(200)
            .json({
              status: 1,
              data: res_data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Task Action Status
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Task Action Status used for task action selection
   */
  get_task_action: async (req, res, next) => {
    try {

      let lang = req.params.lang;

      await taskModel.get_task_action(lang)
        .then(function (data) {

          let res_data = []

          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            res_data.push(element.name);
          }

          res.status(200)
            .json({
              status: 1,
              data: res_data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Update cc customer
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Update cc customer used for customer selection
   */
  update_cc: async (req, res, next) => {
    try {
      const task_id = req.body.task_id;
      const ccCust = req.body.cc.map(function (x) {
        return parseInt(x, 10);
      });
      let total_cc_custs = [];
      if (task_id != 0) {
        if (ccCust && ccCust.length > 0) {
          for (let index = 0; index < ccCust.length; index++) {
            const element = parseInt(ccCust[index]);

            var check_cc_id = await customerModel.checkCCCustomer(element, task_id);
            if (check_cc_id > 0) {
              //UDPATE
              await customerModel.udpate_cc_customer(check_cc_id);
            } else {
              //INSERT
              var insert_val = {
                customer_id: element,
                task_id: task_id,
                status: 1
              };
              await customerModel.insert_cc_customer(insert_val);

            }

            //CHECK HIGHLIGHT
            let check = await taskModel.check_highlight_entry(task_id, element, 'C');
            if (check === true) {
              //DO NOTHING
            } else {
              //INSERT HIGHLIGHT ENTRY
              let highlight_arr = [
                task_id,
                element,
                1,
                'C'
              ];
              await taskModel.task_highlight(highlight_arr);
            }
            total_cc_custs.push(element);
          }

          await customerModel.remove_excess_cc_customers(total_cc_custs, task_id);



          //UPDATE HIGHLIGHTER
          let task_details = await taskModel.tasks_details(task_id);
          var customer_id = task_details[0].customer_id;
          if (req.user.role == 2) {

          } else {

            if (task_details[0].submitted_by > 0) {

            } else {
              if (Object.prototype.hasOwnProperty.call(req.body, 'share_with_agent')) {
                await taskModel.update_shared_with_agent(task_id, req.body.share_with_agent);
              }
            }
          }

          total_cc_custs.push(customer_id);

          //console.log('total_cc_custs',total_cc_custs);
          //PRE SELECTED CC LIST 
          // var pre_selected = await module.exports.get_pre_selected_cc_list(customer_id);

          // for (let index = 0; index < pre_selected.length; index++) {
          //   const element = pre_selected[index];
          //   //CHECK HIGHLIGHT HAS ENTRY
          //   let check = await taskModel.check_highlight_entry(task_id,element.customer_id,'C');
          //   if(check === true){
          //     //DO NOTHING
          //   }else{
          //     //INSERT HIGHLIGHT ENTRY
          //     let highlight_arr = [
          //       task_id,
          //       element.customer_id,
          //       1,
          //       'C'
          //     ];
          //     await taskModel.task_highlight(highlight_arr);
          //   }

          //   total_cc_custs.push(element.customer_id);
          // }
          //console.log('total_cc_custs',total_cc_custs);

          await customerModel.remove_excess_customer_highlights(total_cc_custs, task_id);
          await customerModel.remove_excess_customer_notifications(total_cc_custs, task_id);

          //AGENT LIST
          let db_task_details = await taskModel.tasks_details(task_id);
          if (db_task_details[0].share_with_agent == 1) {
            let total_agents = [];
            var customer_details = await customerModel.get_user(customer_id);
            let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            for (let index = 0; index < agent_list.length; index++) {
              const element = agent_list[index];
              //CHECK HIGHLIGHT HAS ENTRY
              let check = await taskModel.check_highlight_entry(task_id, element.agent_id, 'A');
              if (check === true) {
                //DO NOTHING
              } else {
                //INSERT HIGHLIGHT ENTRY

                let highlight_agent_arr = [
                  task_id,
                  element.agent_id,
                  1,
                  'A'
                ];
                await taskModel.task_highlight(highlight_agent_arr);
              }
              total_agents.push(element.agent_id);
            }
            if (total_agents.length > 0) {
              await customerModel.remove_excess_agent_highlights_not_in(total_agents, task_id);
              await customerModel.remove_excess_agent_notifications_not_in(total_agents, task_id);
            }
          } else {
            let total_agents = [];
            var customer_details = await customerModel.get_user(customer_id);
            let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);
            for (let index = 0; index < agent_list.length; index++) {
              const element = agent_list[index];
              total_agents.push(element.agent_id);
            }
            if (total_agents.length > 0) {
              await customerModel.remove_excess_agent_highlights(total_agents, task_id);
              await customerModel.remove_excess_agent_notifications(total_agents, task_id);
            }
          }

          // ACTIVITY LOG - SATYAJIT
          var params = {
            task_ref: db_task_details[0].task_ref,
            request_type: db_task_details[0].req_name
          }
          if (req.user.role == 2) {
            var agentData = await agentModel.get_user(req.user.customer_id);
            params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
            var submitted_details = await customerModel.get_user(customer_id);
            params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`;
            await taskModel.log_activity(5, task_id, params)
          } else {
            var submitted_details = await customerModel.get_user(req.user.customer_id);
            params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`;
            await taskModel.log_activity(4, task_id, params)
          }
          // END LOG

          res.status(200).json({
            status: 1,
            message: "Updated"
          }).end();

        } else {

          await customerModel.udpateAllCCCustomer(task_id);

          let task_details = await taskModel.tasks_details(task_id);
          var customer_id = task_details[0].customer_id;
          if (req.user.role == 2) {

          } else {

            if (task_details[0].submitted_by > 0) {

            } else {

              if (Object.prototype.hasOwnProperty.call(req.body, 'share_with_agent')) {
                await taskModel.update_shared_with_agent(task_id, req.body.share_with_agent);
              }
            }
          }

          total_cc_custs.push(customer_id);

          //PRE SELECTED CC LIST 
          // var pre_selected = await module.exports.get_pre_selected_cc_list(customer_id);

          // for (let index = 0; index < pre_selected.length; index++) {
          //   const element = pre_selected[index];
          //   //CHECK HIGHLIGHT HAS ENTRY
          //   let check = await taskModel.check_highlight_entry(task_id,element.customer_id,'C');
          //   if(check === true){
          //     //DO NOTHING
          //   }else{
          //     //INSERT HIGHLIGHT ENTRY
          //     let highlight_arr = [
          //       task_id,
          //       element.customer_id,
          //       1,
          //       'C'
          //     ];
          //     await taskModel.task_highlight(highlight_arr);
          //   }

          //   total_cc_custs.push(element.customer_id);
          // }

          await customerModel.remove_excess_customer_highlights(total_cc_custs, task_id);
          await customerModel.remove_excess_customer_notifications(total_cc_custs, task_id);

          //AGENT LIST
          let db_task_details = await taskModel.tasks_details(task_id);
          if (db_task_details[0].share_with_agent == 1) {
            let total_agents = [];
            var customer_details = await customerModel.get_user(customer_id);
            let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            for (let index = 0; index < agent_list.length; index++) {
              const element = agent_list[index];
              //CHECK HIGHLIGHT HAS ENTRY
              let check = await taskModel.check_highlight_entry(task_id, element.agent_id, 'A');
              if (check === true) {
                //DO NOTHING
              } else {
                //INSERT HIGHLIGHT ENTRY

                let highlight_agent_arr = [
                  task_id,
                  element.agent_id,
                  1,
                  'A'
                ];
                await taskModel.task_highlight(highlight_agent_arr);
              }
              total_agents.push(element.agent_id);
            }
            if (total_agents.length > 0) {
              await customerModel.remove_excess_agent_highlights_not_in(total_agents, task_id);
              await customerModel.remove_excess_agent_notifications_not_in(total_agents, task_id);
            }
          } else {
            let total_agents = [];
            var customer_details = await customerModel.get_user(customer_id);
            let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);
            for (let index = 0; index < agent_list.length; index++) {
              const element = agent_list[index];
              total_agents.push(element.agent_id);
            }
            if (total_agents.length > 0) {
              await customerModel.remove_excess_agent_highlights(total_agents, task_id);
              await customerModel.remove_excess_agent_notifications(total_agents, task_id);
            }
          }

          // ACTIVITY LOG - SATYAJIT
          var params = {
            task_ref: db_task_details[0].task_ref,
            request_type: db_task_details[0].req_name
          }
          if (req.user.role == 2) {
            var agentData = await agentModel.get_user(req.user.customer_id);
            params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
            var submitted_details = await customerModel.get_user(customer_id);
            params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`;
            await taskModel.log_activity(5, task_id, params)
          } else {
            var submitted_details = await customerModel.get_user(req.user.customer_id);
            params.customer_name = `${submitted_details[0].first_name} ${submitted_details[0].last_name}`;
            await taskModel.log_activity(4, task_id, params)
          }
          // END LOG

          res.status(200).json({
            status: 1,
            message: "Updated"
          }).end();
        }
      } else {
        res.status(400).json({
          status: 2,
          message: "Task id not present"
        }).end();
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Rate Task
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Rate Task used for task selection
   */
  rate_task: async (req, res, next) => {
    try {
      const {
        task_id,
        rating,
        comment,
        options
      } = req.body;

      const posted_by = req.user.customer_id;

      let task_details = await taskModel.tasks_details(task_id);

      let rated_by = posted_by;
      let rated_by_type = req.user.role;
      // if(posted_by != task_details[0].customer_id){
      //   rated_by = posted_by;
      // }

      //console.log('rated_by_type',rated_by_type);

      await taskModel.rate_task([
        task_id,
        rating,
        0,
        common.currentDateTime(),
        options.join(','),
        comment,
        rated_by,
        rated_by_type
      ]);

      res.status(200).json({
        status: 1
      });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Skip Task
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Skip Task used for task selection
   */
  skip_task: async (req, res, next) => {
    try {
      const {
        task_id
      } = req.body;

      await taskModel.skip_task(task_id);

      res.status(200).json({
        status: 1
      });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a selected cc customers
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - selected cc customers used for customer selection
   */
  get_updated_cc_list: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;

      let task_details = await taskModel.tasks_details(task_id);


      var customer_id = task_details[0].customer_id;


      let customerDetails = await customerModel.get_user(customer_id);
      //console.log('customerDetails',customerDetails);
      var companyId = customerDetails[0].company_id;

      let company_arr = await module.exports.__get_company_list(companyId, req.user.role, req.user.customer_id);

      var ccCutomerList = await customerModel.get_cc_cust_company(company_arr, customer_id);

      var selCCCustList = await customerModel.list_cc_customer(task_id);
      var dataObj = {
        ccCutomerList: ccCutomerList,
        selCCCustList: selCCCustList,
        preSelected: []
      };

      res.status(200).json({
        status: 1,
        data: dataObj
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Pending Ratings List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Pending Ratings List used for Ratings selection
   */
  get_all_pending_ratings: async (req, res, next) => {
    try {

      let data = await taskModel.get_all_pending_ratings(req.user.customer_id);

      //console.log('all dashboard', data);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  get_explicit_pending_ratings: async (req, res, next) => {
    try {

      const task_arr = hashids.decode(req.params.ciphered_key);
      const task_id = task_arr[0];
      console.log('task_id', task_id);
      let data = await taskModel.get_explicit_pending_ratings(req.user.customer_id, task_id);

      //console.log('all dashboard', data);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Pending Ratings Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Pending Ratings Count used for Ratings selection
   */
  get_all_pending_ratings_counts: async (req, res, next) => {
    try {

      let data = await taskModel.get_all_pending_ratings_counts(req.user.customer_id);

      res.status(200).json({
        status: 1,
        count: data[0].count
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Other Pending Ratings List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Other Pending Ratings List used for Ratings selection
   */
  get_other_pending_ratings: async (req, res, next) => {
    try {

      let data = await taskModel.get_other_pending_ratings(req.user.customer_id);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Other Pending Ratings Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Other Pending Ratings Count used for Ratings selection
   */
  get_other_pending_ratings_counts: async (req, res, next) => {
    try {

      let data = await taskModel.get_other_pending_ratings_counts(req.user.customer_id);

      res.status(200).json({
        status: 1,
        count: data[0].count
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Complaints Pending Ratings List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Complaints Pending Ratings List used for Ratings selection
   */
  get_complaints_pending_ratings: async (req, res, next) => {
    try {

      let data = await taskModel.get_complaints_pending_ratings(req.user.customer_id);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Complaints Pending Ratings Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Complaints Pending Ratings Count used for Ratings selection
   */
  get_complaints_pending_ratings_counts: async (req, res, next) => {
    try {

      let data = await taskModel.get_complaints_pending_ratings_counts(req.user.customer_id);

      console.log(data);

      res.status(200).json({
        status: 1,
        count: data[0].count
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Orders Pending Ratings List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Orders Pending Ratings List used for Ratings selection
   */
  get_orders_pending_ratings: async (req, res, next) => {
    try {

      let data = await taskModel.get_orders_pending_ratings(req.user.customer_id);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Orders Pending Ratings Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Orders Pending Ratings Count used for Ratings selection
   */
  get_orders_pending_ratings_counts: async (req, res, next) => {
    try {

      let data = await taskModel.get_orders_pending_ratings_counts(req.user.customer_id);

      res.status(200).json({
        status: 1,
        count: data[0].count
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Forecast Pending Ratings List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Forecast Pending Ratings List used for Ratings selection
   */
  get_forecast_pending_ratings: async (req, res, next) => {
    try {

      let data = await taskModel.get_forecast_pending_ratings(req.user.customer_id);

      res.status(200).json({
        status: 1,
        single_entry: data.length,
        data: data
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Forecast Pending Ratings Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Forecast Pending Ratings Count used for Ratings selection
   */
  get_forecast_pending_ratings_counts: async (req, res, next) => {
    try {

      let data = await taskModel.get_forecast_pending_ratings_counts(req.user.customer_id);

      res.status(200).json({
        status: 1,
        count: data[0].count
      });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Update Task Highlight
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Update Task Highlight used for Task selection
   */
  update_task_highlight: async (req, res, next) => {
    try {
      const task_id = req.value.params.id;
      const customer_id = req.user.customer_id;
      let identifier = '';
      if (req.user.role == 2) {
        identifier = 'A';
      } else {
        identifier = 'C';
      }
      await taskModel.update_highlight(task_id, customer_id, identifier, 0);

      res.status(200).json({
        status: 1,
        data: 'Highlight updated'
      });
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Notification to CC Customers
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} task_details - HTTP response argument
   * @param {next} notify_type  - HTTP response argument
   * @return {Json Object} - Notification to CC Customers used for Notification selection
   */
  notify_cc_highlight: async (req, task_details, notify_type = 0) => {
    const posted_by = req.user.customer_id;
    let ret_arr = [];
    let customer_details = await customerModel.get_user(task_details[0].customer_id);
    //console.log('customer_details',customer_details);
    if (customer_details[0].status == 1) {
      let to_details = [];
      let cc_details = [];

      if (customer_details[0].dnd != 1) {
        to_details.push(customer_details[0].email);
      }

      if (task_details[0].share_with_agent == 1) {

        let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

        if (agent_list && agent_list.length > 0) {
          for (let index = 0; index < agent_list.length; index++) {
            const element = agent_list[index];
            let agent_cc_dets = await agentModel.get_user(element.agent_id);
            if (agent_cc_dets[0].dnd != 1) {
              if (task_details[0].submitted_by > 0 && parseInt(task_details[0].submitted_by) == parseInt(element.agent_id)) {
                to_details.push(agent_cc_dets[0].email);
              } else {
                cc_details.push(agent_cc_dets[0].email);
              }
            }
          }
        }
      }


      //NOTIFY CC CUSTOMERS
      let cc_list = await taskModel.task_cc_customer(task_details[0].task_id);
      if (cc_list && cc_list.length > 0) {
        for (let index = 0; index < cc_list.length; index++) {
          const element = cc_list[index];

          let cc_cust_dets = await customerModel.get_user(element.customer_id);
          if (cc_cust_dets && cc_cust_dets.length > 0 && cc_cust_dets[0].status == 1 && cc_cust_dets[0].dnd != 1) {
            cc_details.push(cc_cust_dets[0].email);
          }
        }
      }
      //console.log('NOTIFY CC CUSTOMERS',cc_details);
      //NOTIFY PRE SELECTED CC CUSTOMERS
      // let pre_selected = await module.exports.get_pre_selected_cc_list(task_details[0].customer_id);
      // if(pre_selected && pre_selected.length > 0){
      //   for (let index = 0; index < pre_selected.length; index++) {
      //     const element = pre_selected[index];

      //     let pre_cc_arr = [
      //       notify_type,
      //       task_details[0].task_id,
      //       element.customer_id,
      //       common.currentDateTime(),
      //       0,
      //       'C'
      //     ]
      //     await taskModel.notify(pre_cc_arr);
      //
      //     let pre_cc_cust_dets = await customerModel.get_user(element.customer_id);
      //     if(pre_cc_cust_dets[0].role_type != 0){
      //       cc_details.push(pre_cc_cust_dets[0].email);
      //     }
      //   }
      // }
      //console.log('NOTIFY PRE SELECTED CC CUSTOMERS',cc_details);
      //NOTIFY AGENT SHARED WITH 1

      if (to_details.length == 0 && cc_details.length > 0) {
        to_details = cc_details;
        cc_details = [];
      }

      if (notify_type > 0) {
        //EMPLOYEE
        let task_owner = task_details[0].owner;
        let task_second_assign_arr = await taskModel.get_auto_assigned_task_owner(task_details[0].task_id);
        let task_curr_owner = task_details[0].current_ownership;
        let param_arr = {};

        //console.log('task_owner',task_owner);
        //console.log('task_second_assign',task_second_assign_arr[0].assigned_to);
        //console.log('task_curr_owner',task_curr_owner);

        if (notify_type == 1) {
          let cust_details = await customerModel.get_user(posted_by);
          param_arr = {
            customer_name: cust_details[0].first_name
          };
        } else if (notify_type == 2) {
          let agent_details = await agentModel.get_user(posted_by);
          param_arr = {
            agent_name: `${agent_details[0].first_name} (Agent)`
          };
        }

        let notification_text = await taskModel.get_notification_text(notify_type, param_arr);

        //TASK OWNER
        if (task_owner > 0) {

          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_owner,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }

        let task_second_assign = 0;
        if (task_second_assign_arr && task_second_assign_arr.length > 0 && task_second_assign_arr[0].assigned_to > 0) {
          task_second_assign = task_second_assign_arr[0].assigned_to;
        }

        //TASK SECOND ASSIGNEE
        if (task_second_assign > 0 &&
          !common.inArray(task_second_assign, [task_owner])
        ) {
          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_second_assign,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }

        //TASK CURRENT OWNER
        if (
          task_curr_owner > 0 &&
          !common.inArray(task_curr_owner, [task_owner, task_second_assign])
        ) {
          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_curr_owner,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }
      }


      ret_arr.push({
        to_arr: to_details
      });
      ret_arr.push({
        cc_arr: cc_details
      });
    }

    return ret_arr;
  },
  /**
   * Generates a Selected CC Customers
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} customer_id - HTTP request argument
   * @return {Json Object} - Selected CC Customers used for Customers selection
   */
  get_pre_selected_cc_list: async (customer_id) => {
    //console.log('customer_id',customer_id);
    let ret_arr = [];
    let customer_details = await customerModel.get_user(customer_id);
    //ROLE 0 => Is Admin YES
    if (customer_details[0].role_type == 0) {
      ret_arr = await customerModel.get_super_users(customer_id, customer_details[0].company_id);
      //ROLE TYPE 1 => Is Admin NO, Role Type ADMIN  
    } else if (customer_details[0].role_type == 1) {
      let super_admins = await customerModel.get_super_users(customer_id, customer_details[0].company_id);
      let not_in = [customer_id];
      ret_arr = super_admins;

      for (let index = 0; index < super_admins.length; index++) {
        const element = super_admins[index];
        not_in.push(element.customer_id);
      }

      let cust_role_arr = await customerModel.get_user_role(customer_id);
      let role_arr = [];
      for (let index = 0; index < cust_role_arr.length; index++) {
        const element = cust_role_arr[index];
        role_arr.push(element.role_id);
      }

      let sel_cust_arr = await customerModel.get_department_admins(not_in, role_arr, customer_details[0].company_id);
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element);
      }

      //ROLE TYPE 2 => Is Admin NO, Role Type REGULAR  
    } else if (customer_details[0].role_type == 2) {
      let super_admins = await customerModel.get_super_users(customer_id, customer_details[0].company_id);
      let not_in = [];
      ret_arr = super_admins;

      for (let index = 0; index < super_admins.length; index++) {
        const element = super_admins[index];
        not_in.push(element.customer_id);
      }

      let cust_role_arr = await customerModel.get_user_role(customer_id);
      let role_arr = [];
      for (let index = 0; index < cust_role_arr.length; index++) {
        const element = cust_role_arr[index];
        role_arr.push(element.role_id);
      }
      //console.log('role_arr',role_arr);
      //console.log('not_in',not_in);
      //console.log('company_id',customer_details[0].company_id);
      let sel_cust_arr = await customerModel.get_department_admins(not_in, role_arr, customer_details[0].company_id);
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element);
      }

    }

    return ret_arr;
  },
  /**
   * Generates a S3 signed url
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} key - HTTP request argument
   * @return {Json Object} - S3 signed url used for signed url selection
   */
  __get_signed_url: async (key) => {
    return new Promise((resolve, reject) => {
      // AWS.config.update({ accessKeyId: Config.aws.accessKeyId, secretAccessKey: Config.aws.secretAccessKey });
      // var s3 = new AWS.S3()
      // const s3Config = new AWS.S3({
      //   accessKeyId: Config.aws.accessKeyId,
      //   secretAccessKey: Config.aws.secretAccessKey,
      //   Bucket: Config.aws.bucketName
      // });
      //console.log("KEY",key)
      //console.log("Status");

      params = {
        Bucket: Config.aws.bucketName,
        Key: key,
        Expires: 5000
      }
      //console.log("Params",params);

      AWS.config.update({
        accessKeyId: Config.aws.accessKeyId,
        secretAccessKey: Config.aws.secretAccessKey
      });

      AWS.config.region = Config.aws.regionName;
      var s3 = new AWS.S3();

      // console.log("AWS",AWS.config);

      s3.getSignedUrl('getObject', params, function (err, url) {
        if (err) {
          //console.log('error: ' + err);
          resolve({
            status: 3,
            message: err
          });
        } else {
          //console.log('Signed URL: ' + url);
          //console.log('GET URL: ' + url.split("?")[0]);
          resolve({
            status: 1,
            url: url
          });
        }
        //console.log('Signed URL: ' + url);
      });


      // var head_params = { Bucket: 'ccpattachement', Key: key };
      // var url_params = { Bucket: 'ccpattachement', Key: key, expires: 60 };
      // s3.headObject(head_params, function (err, data) {
      //   if (err) {
      //     console.log("Error", err);
      //     resolve({ status: 3, message: err });
      //     // return res.json{status : 3,message:err};//(err,err.code); // an error occurred
      //   }
      //   else {
      //     s3.getSignedUrl('getObject', url_params, function (err, url) {
      //       if (err) {
      //         console.log('error: ' + err);
      //         resolve({ status: 3, message: err });
      //       } else {
      //         console.log('Signed URL: ' + url);
      //         resolve({ status: 3, url: url });
      //       }
      //     });
      //   }           // successful response
      // });
    })

    // s3.headObject(head_params).on('success', function(response) {
    //   console.log("Error2");
    //   s3.getSignedUrl('getObject', url_params, function (err, url) {
    //       if (err) {
    //         console.log("Error");
    //         return {status : 3,message:'Cannot generate URL'}; 
    //       } else {
    //         console.log("Success");
    //         return {status : 1,url:url};
    //       }
    //   }); 

    //   // console.log("Key was", response.request.params.Key);
    // }).on('error',function(error){
    //   console.log("Error1");
    //   return {status : 3,message:'File does not exist'};
    // });

    //return ret_arr;

  },
  /**
   * Generates a Download Discussion files from S3
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Download Discussion files from S3 used for files download selection
   */
  download_file: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.value.params.ciphered_key);

      await taskModel.select_discussion_comment_specific_files_upload(key)
        .then(async function (data) {

          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = data[0].actual_file_name;
          var new_file_name = data[0].new_file_name;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });
        })
        .catch(err => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value
            })
            .end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Download task files from S3
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Download task files from S3 used for files download selection
   */
  download_task_file: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.value.params.ciphered_key);

      await taskModel.select_task_files_upload(key)
        .then(async function (data) {

          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = data[0].actual_file_name;
          var new_file_name = data[0].new_file_name;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                console.log("actual file--------", actual_file_name);
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });
        })
        .catch(err => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value
            })
            .end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  download_coa_task_file: async (req, res, next) => {
    try {
      // console.log("+++++++++user+++++",req.user);return false;
      const key = cryptr.decrypt(req.value.params.ciphered_key).split('#');
      let coa_id = 0;
      let task_id = 0;
      let invoice_id = 0;
      if (key.length == 3) {
        coa_id = key[2];
        task_id = key[1];
        invoice_id = key[0];
      }

      if (coa_id) {
        await taskModel.select_coa_task_files_upload(coa_id)
          .then(async function (data) {
            await taskModel
              .tasks_details(task_id)
              .then(async function (data_1) {

                if (data_1[0].request_type == 23 || data_1[0].request_type == 43) {
                  let customer_type = '';
                  let customer_id = '';
                  if (req.user.role == 1) {
                    customer_type = 'C';
                    customer_id = req.user.customer_id;
                  } else if (req.user.role == 2) {
                    customer_type = 'A';
                    customer_id = req.user.agent_id
                  }
                  let trackingObj = {
                    task_id: data_1[0].task_id,
                    customer_id: customer_id,
                    customer_type: customer_type,
                    invoice_id: invoice_id,
                    coa_id: coa_id
                  }
                  //console.log();
                  const check_tracking_coa = await taskModel.check_tracking_coa(trackingObj);
                  if (check_tracking_coa && check_tracking_coa.length > 0) {
                    if (check_tracking_coa[0].id > 0) {
                      trackingObj.date_updated = common.currentDateTime();
                      const update_tracking_coa = await taskModel.update_tracking_coa(trackingObj)
                      if (update_tracking_coa && update_tracking_coa[0].id != undefined) {
                        console.log("Tracking data updated .");
                      } else {
                        console.log("Error in Updating Tracking Data");
                      }
                    }
                  } else {
                    trackingObj.date_added = common.currentDateTime();
                    trackingObj.date_updated = common.currentDateTime();
                    const insert_tracking_data_coa = await taskModel.insert_tracking_data_coa(trackingObj)
                    if (insert_tracking_data_coa && insert_tracking_data_coa[0].id != undefined) {
                      console.log("Tracking data Inserted .");
                    } else {
                      console.log("Error in Inserting Tracking Data");
                    }
                  }

                }
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                });
              });

            AWS.config.update({
              accessKeyId: Config.aws.accessKeyId,
              secretAccessKey: Config.aws.secretAccessKey
            });
            var s3 = new AWS.S3();
            var actual_file_name = data[0].actual_file_name;
            var new_file_name = data[0].new_file_name;

            var options = {
              Bucket: Config.aws.bucketName,
              Key: new_file_name,
            };
            return new Promise((resolve, reject) => {
              s3.getObject(options, function (err, data) {
                if (err === null) {
                  res.attachment(actual_file_name); // or whatever your logic needs
                  res.send(data.Body);
                } else {
                  res.status(400)
                    .json({
                      status: 3,
                      message: err
                    })
                }
              });
            });
          })
          .catch(err => {
            common.logError(err);
            res
              .status(400)
              .json({
                status: 3,
                message: Config.errorText.value
              })
              .end();
          });
      } else {
        res
          .status(400)
          .json({
            status: 3,
            message: Config.errorText.value
          })
          .end();
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  download_packing_task_file: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.value.params.ciphered_key);

      await taskModel.select_packing_task_files_upload(key)
        .then(async function (data) {

          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = data[0].actual_file_name;
          var new_file_name = data[0].new_file_name;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });
        })
        .catch(err => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value
            })
            .end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  download_invoice_task_file: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.value.params.ciphered_key);

      await taskModel.select_invoice_task_files_upload(key)
        .then(async function (data) {

          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = data[0].actual_file_name;
          var new_file_name = data[0].new_file_name;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });
        })
        .catch(err => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value
            })
            .end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Employee on leave for allocated task
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Employee on leave for allocated task used for Employee selection
   */
  __get_allocated_leave_employee: async (emp_details, customer_id) => {
    let ret_details;
    if (emp_details && emp_details.length > 0) {
      if (emp_details[0].on_leave == 1) {
        var new_emp_dets_one = await taskModel.get_allocated_leave_employee(emp_details[0].employee_id, customer_id);

        if (new_emp_dets_one && new_emp_dets_one.length > 0 && new_emp_dets_one[0].desig_id == emp_details[0].desig_id) {

          if (new_emp_dets_one[0].on_leave == 1) {
            var new_emp_dets_two = await taskModel.get_allocated_leave_employee(new_emp_dets_one[0].employee_id, customer_id);

            if (new_emp_dets_two && new_emp_dets_two.length > 0 && new_emp_dets_two[0].desig_id == new_emp_dets_one[0].desig_id) {

              if (new_emp_dets_two[0].on_leave == 1) {
                var new_emp_dets_three = await taskModel.get_allocated_leave_employee(new_emp_dets_two[0].employee_id, customer_id);

                if (new_emp_dets_three && new_emp_dets_three.length > 0 && new_emp_dets_three[0].desig_id == new_emp_dets_two[0].desig_id) {

                  if (new_emp_dets_three[0].on_leave == 1) {
                    var new_emp_dets_four = await taskModel.get_allocated_leave_employee(new_emp_dets_three[0].employee_id, customer_id);

                    if (new_emp_dets_four && new_emp_dets_four.length > 0 && new_emp_dets_four[0].desig_id == new_emp_dets_three[0].desig_id) {

                      if (new_emp_dets_four[0].on_leave == 1) {
                        var new_emp_dets_five = await taskModel.get_allocated_leave_employee(new_emp_dets_four[0].employee_id, customer_id);

                        if (new_emp_dets_five && new_emp_dets_five.length > 0 && new_emp_dets_five[0].desig_id == new_emp_dets_four[0].desig_id) {

                          if (new_emp_dets_five[0].on_leave == 1) {
                            var new_emp_dets_six = await taskModel.get_allocated_leave_employee(new_emp_dets_five[0].employee_id, customer_id);

                            if (new_emp_dets_six && new_emp_dets_six.length > 0 && new_emp_dets_six[0].desig_id == new_emp_dets_five[0].desig_id) {
                              ret_details = new_emp_dets_six;
                            } else {
                              ret_details = new_emp_dets_five;
                            }
                          } else {
                            ret_details = new_emp_dets_five;
                          }
                        } else {
                          ret_details = new_emp_dets_four;
                        }
                      } else {
                        ret_details = new_emp_dets_four;
                      }
                    } else {
                      ret_details = new_emp_dets_three;
                    }
                  } else {
                    ret_details = new_emp_dets_three;
                  }
                } else {
                  ret_details = new_emp_dets_two;
                }
              } else {
                ret_details = new_emp_dets_two;
              }
            } else {
              ret_details = new_emp_dets_one;
            }
          } else {
            ret_details = new_emp_dets_one;
          }
        } else {
          ret_details = emp_details;
        }
      } else {
        ret_details = emp_details;
      }
    } else {
      ret_details = [];
    }

    return ret_details;
  },
  /**
   * Generates a Employee spoc leave for allocated task
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Employee spoc leave for allocated task used for Employee selection
   */
  __get_allocated_leave_employee_spoc: async (emp_details, customer_id) => {
    let ret_details;
    if (emp_details && emp_details.length > 0) {
      if (emp_details[0].on_leave == 1) {
        var new_emp_dets_one = await taskModel.get_allocated_leave_employee(emp_details[0].employee_id, customer_id);

        if (new_emp_dets_one && new_emp_dets_one.length > 0 && new_emp_dets_one[0].desig_id == emp_details[0].desig_id) {

          if (new_emp_dets_one[0].on_leave == 1) {
            var new_emp_dets_two = await taskModel.get_allocated_leave_employee(new_emp_dets_one[0].employee_id, customer_id);

            if (new_emp_dets_two && new_emp_dets_two.length > 0 && new_emp_dets_two[0].desig_id == new_emp_dets_one[0].desig_id) {

              if (new_emp_dets_two[0].on_leave == 1) {
                var new_emp_dets_three = await taskModel.get_allocated_leave_employee(new_emp_dets_two[0].employee_id, customer_id);

                if (new_emp_dets_three && new_emp_dets_three.length > 0 && new_emp_dets_three[0].desig_id == new_emp_dets_two[0].desig_id) {

                  if (new_emp_dets_three[0].on_leave == 1) {
                    var new_emp_dets_four = await taskModel.get_allocated_leave_employee(new_emp_dets_three[0].employee_id, customer_id);

                    if (new_emp_dets_four && new_emp_dets_four.length > 0 && new_emp_dets_four[0].desig_id == new_emp_dets_three[0].desig_id) {

                      if (new_emp_dets_four[0].on_leave == 1) {
                        var new_emp_dets_five = await taskModel.get_allocated_leave_employee(new_emp_dets_four[0].employee_id, customer_id);

                        if (new_emp_dets_five && new_emp_dets_five.length > 0 && new_emp_dets_five[0].desig_id == new_emp_dets_four[0].desig_id) {

                          if (new_emp_dets_five[0].on_leave == 1) {
                            var new_emp_dets_six = await taskModel.get_allocated_leave_employee(new_emp_dets_five[0].employee_id, customer_id);

                            if (new_emp_dets_six && new_emp_dets_six.length > 0 && new_emp_dets_six[0].desig_id == new_emp_dets_five[0].desig_id) {
                              ret_details = new_emp_dets_six;
                            } else {
                              ret_details = [];
                            }
                          } else {
                            ret_details = new_emp_dets_five;
                          }
                        } else {
                          ret_details = [];
                        }
                      } else {
                        ret_details = new_emp_dets_four;
                      }
                    } else {
                      ret_details = [];
                    }
                  } else {
                    ret_details = new_emp_dets_three;
                  }
                } else {
                  ret_details = [];
                }
              } else {
                ret_details = new_emp_dets_two;
              }
            } else {
              ret_details = [];
            }
          } else {
            ret_details = new_emp_dets_one;
          }
        } else {
          ret_details = [];
        }
      } else {
        ret_details = emp_details;
      }
    } else {
      ret_details = [];
    }

    return ret_details;
  },
  /**
   * Generates a Company List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Company list used for company selection
   */
  __get_company_list: async (company_id, role, agent_id) => {
    let parent_company_id = await customerModel.get_parent_company(company_id);

    let company_arr = [];
    if (parent_company_id > 0) {
      let associate_company = await customerModel.get_child_company(parent_company_id);
      let in_agent_arr = [];
      if (role == 2) {
        let agent_company_list = await agentModel.list_company(agent_id);
        for (let index = 0; index < agent_company_list.length; index++) {
          const element = agent_company_list[index];
          in_agent_arr.push(element.company_id);
        }
      }
      for (let index = 0; index < associate_company.length; index++) {
        const element = associate_company[index];
        if (role == 2) {
          if (in_agent_arr.length > 0 && common.inArray(element.company_id, in_agent_arr)) {
            company_arr.push(element.company_id);
          }
        } else {
          company_arr.push(element.company_id);
        }
      }
    } else {
      company_arr.push(company_id);
    }
    return company_arr;
  },
  /**
   * Generates a Task Files Download List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Task Files Download list used for Task file selection
   */
  task_files_download_all: async (req, res, next) => {
    try {
      const task_arr = hashids.decode(req.value.params.hash_id);

      if (task_arr && task_arr.length > 0 && task_arr[0] > 0) {
        const task_id = task_arr[0];

        let task_details = await taskModel.tasks_details(task_id);
        let file_name = `${task_details[0].task_ref}_${task_details[0].req_name.split(' ').join('_').split('/').join('_')}.zip`;
        let file_path = `${Config.upload.task_compressed}${file_name}`;

        if (fs.existsSync(file_path)) {
          //DOWNLOAD EXISTING FILE
          console.log('exists');
          res.download(file_path);
        } else {
          console.log('new');
          let filesArr = [];
          let archiveFiles = [];
          let taskDetailsFiles = await taskModel.select_task_files(task_id);
          let folder = '';

          for (let index = 0; index < taskDetailsFiles.length; index++) {
            const element = taskDetailsFiles[index];
            filesArr.push(element.new_file_name);
            archiveFiles.push({
              name: element.actual_file_name
            });
          }

          const AWS = require('aws-sdk');
          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey,
            region: Config.aws.regionName
          });

          const output = fs.createWriteStream(join(Config.upload.task_compressed, file_name));
          let stream = s3Zip.archive({
            region: Config.aws.regionName,
            bucket: Config.aws.bucketName
          }, folder, filesArr, archiveFiles).pipe(output);

          stream.on('finish', () => {
            res.download(file_path);
          });
        }
      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  /**
   * Generates a Discussion Download List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Discussion Download list used for Discussion selection
   */
  discussion_download_all: async (req, res, next) => {
    try {
      const discussion_arr = hashids.decode(req.value.params.hash_id);

      if (discussion_arr && discussion_arr.length > 0 && discussion_arr[0] > 0) {
        const comment_id = discussion_arr[0];

        let task_details = await taskModel.get_task_comment_details(comment_id);
        let file_name = `${task_details[0].task_ref}_${task_details[0].req_name.split(' ').join('_').split('/').join('_')}_discussion.zip`;
        let file_path = `${Config.upload.discussion_compressed}${file_name}`;

        if (fs.existsSync(file_path)) {
          //DOWNLOAD EXISTING FILE
          console.log('exists');
          fs.unlinkSync(file_path);
        }

        let filesArr = [];
        let archiveFiles = [];
        let taskDetailsFiles = await taskModel.select_discussion_comment_specific_files(comment_id);
        let folder = '';

        for (let index = 0; index < taskDetailsFiles.length; index++) {
          const element = taskDetailsFiles[index];
          filesArr.push(element.new_file_name);
          archiveFiles.push({
            name: element.actual_file_name
          });
        }

        const AWS = require('aws-sdk');
        AWS.config.update({
          accessKeyId: Config.aws.accessKeyId,
          secretAccessKey: Config.aws.secretAccessKey,
          region: Config.aws.regionName
        });

        const output = fs.createWriteStream(join(Config.upload.discussion_compressed, file_name));
        let stream = s3Zip.archive({
          region: Config.aws.regionName,
          bucket: Config.aws.bucketName
        }, folder, filesArr, archiveFiles).pipe(output);

        stream.on('finish', () => {
          res.download(file_path);
        });

      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  /**
   * Generates a Discussion File Download List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Discussion File Download list used for Discussion File selection
   */
  discussion_files_download_all: async (req, res, next) => {
    try {
      const discussion_arr = hashids.decode(req.value.params.hash_id);

      if (discussion_arr && discussion_arr.length > 0 && discussion_arr[0] > 0) {
        const comment_id = discussion_arr[0];

        let task_details = await taskModel.get_task_comment_details(comment_id);
        let file_name = `${task_details[0].task_ref}_${task_details[0].req_name.split(' ').join('_').split('/').join('_')}_discussion_files.zip`;
        let file_path = `${Config.upload.discussion_compressed}${file_name}`;

        if (fs.existsSync(file_path)) {
          //DOWNLOAD EXISTING FILE
          console.log('exists');
          //res.download(file_path);
          fs.unlinkSync(file_path);
        }
        let filesArr = [];
        let archiveFiles = [];
        let taskDetailsFiles = await taskModel.select_discussion_comment_specific_files(comment_id);
        let folder = '';

        for (let index = 0; index < taskDetailsFiles.length; index++) {
          const element = taskDetailsFiles[index];
          filesArr.push(element.new_file_name);
          archiveFiles.push({
            name: element.actual_file_name
          });
        }

        const AWS = require('aws-sdk');
        AWS.config.update({
          accessKeyId: Config.aws.accessKeyId,
          secretAccessKey: Config.aws.secretAccessKey,
          region: Config.aws.regionName
        });

        const output = fs.createWriteStream(join(Config.upload.discussion_compressed, file_name));
        let stream = s3Zip.archive({
          region: Config.aws.regionName,
          bucket: Config.aws.bucketName
        }, folder, filesArr, archiveFiles).pipe(output);

        stream.on('finish', () => {
          res.download(file_path);
        });

      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  /**
   * Generates a Sales Force Add Task
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} task_id - HTTP request argument
   * @param {Object} files - HTTP request argument
   * @param {Object} ccp_posted_by - HTTP request argument
   * @return {Json Object} - Sales Force Add Task used for Sales Force selection
   */
  __post_sales_force_add_task: async (task_id, files, ccp_posted_by) => {

    let proceed = true;
    let task_details = await taskModel.get_sales_force_data(task_id);
    let sales_force_task = {
      request_type: `${task_details[0].request_type}`,
      request_name: `${task_details[0].req_name}`,
      task_id: task_details[0].task_id,
      task_ref: task_details[0].task_ref,
      customer_id: task_details[0].cqt_customer_id,
      sales_customer_id: task_details[0].sales_customer_id,
      sales_company_id: (task_details[0].sales_company_id != null) ? task_details[0].sales_company_id : '',
      requirement: task_details[0].content
    };

    if (ccp_posted_by > 0) {
      let emp_details = await employeeModel.get_user(+ccp_posted_by);
      sales_force_task.sales_emp_ref = (emp_details[0].sales_emp_id != null) ? emp_details[0].sales_emp_id : '';
    }

    if (task_details[0].request_type != 24) {
      sales_force_task.cqt_product_id = task_details[0].cqt_product_id;
    }

    if (task_details[0].request_type != 40) {

      let task_countries = await taskModel.get_sales_force_countries(task_id);

      if (task_countries && task_countries[0].task_countries && task_countries[0].task_countries.length > 0) {
        sales_force_task.market_id = task_countries[0].task_countries.map((a) => {
          return entities.decode(a)
        });
      } else {
        sales_force_task.market_id = [];
      }

    }

    let url = '';
    let quantity = '';
    let quantity_unit = '';

    let quantity_impurities = '';
    let quantity_unit_impurities = '';

    let quantity_working = '';
    let quantity_unit_working = '';
    switch (task_details[0].request_type) {

      case 1:
        //url = Config.sales_force.task4;
        url = (Config.sampark.use == true) ? Config.sampark.task4 : Config.sales_force.task4;
        sales_force_task.pharmacopoeia = task_details[0].pharmacopoeia;
        sales_force_task.shipping_address = task_details[0].shipping_address;

        let service_arr = task_details[0].service_request_type.split(',');

        for (let index = 0; index < service_arr.length; index++) {
          const element = service_arr[index];

          if (element == 'Samples') {

            if (trim(task_details[0].quantity) != '') {
              let quant = trim(task_details[0].quantity);
              let quant_trim = quant.split(' ');
              quantity = quant_trim[0];
              quantity_unit = quant_trim[1];
            }

            sales_force_task.sample = {
              batch: task_details[0].number_of_batches,
              quantity: quantity,
              unit: quantity_unit
            };
          }

          if (element == 'Impurities') {

            if (trim(task_details[0].impurities_quantity) != '') {
              let quant = trim(task_details[0].impurities_quantity);
              let quant_trim = quant.split(' ');
              quantity_impurities = quant_trim[0];
              quantity_unit_impurities = quant_trim[1];
            }

            sales_force_task.impurities = {
              specify_impurities: task_details[0].specify_impurity_required,
              quantity: quantity_impurities,
              unit: quantity_unit_impurities
            };
          }

          if (element == 'Working Standards') {

            if (trim(task_details[0].working_quantity) != '') {
              let quant = trim(task_details[0].working_quantity);
              let quant_trim = quant.split(' ');
              quantity_working = quant_trim[0];
              quantity_unit_working = quant_trim[1];
            }

            sales_force_task.working_standards = {
              quantity: quantity_working,
              unit: quantity_unit_working
            };
          }

        }



        break;

      case 22:
      case 35:
      case 4:
      case 10:
      case 12:
      case 21:
      case 36:
      case 19:
      case 18:
      case 17:
      case 30:
      case 32:
      case 33:
      case 37:
      case 42:
        //url = Config.sales_force.task;
        url = (Config.sampark.use == true) ? Config.sampark.task : Config.sales_force.task;
        sales_force_task.pharmacopoeia = task_details[0].pharmacopoeia;
        break;

      case 5:
      case 6:
      case 8:
      case 11:
      case 14:
      case 15:
      case 16:
      case 20:
      case 24:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        break;

      case 2:
      case 3:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.polymorphic_form = task_details[0].polymorphic_form;
        sales_force_task.pharmacopoeia = task_details[0].pharmacopoeia;
        break;

      case 9:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.site_name = task_details[0].audit_visit_site_name;
        sales_force_task.audit_visit_date = dateFormat(task_details[0].request_audit_visit_date, "yyyy-mm-dd");
        break;

      case 13:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.data_type = task_details[0].stability_data_type;
        break;

      case 27:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.dmf_number = (task_details[0].dmf_number == null) ? '' : task_details[0].dmf_number;
        break;

      case 28:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        console.log('task_details[0].rdfrc', task_details[0].rdfrc);
        sales_force_task.response_date = (task_details[0].rdfrc == null || task_details[0].rdfrc == '') ? '' : dateFormat(task_details[0].rdfrc, "yyyy-mm-dd");
        break;

      case 29:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.notification_number = (task_details[0].notification_number == null) ? '' : task_details[0].notification_number;
        break;

      case 38:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.document_type = (task_details[0].apos_document_type == null) ? '' : task_details[0].apos_document_type;
        break;

      case 39:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.dmf_cep = (task_details[0].dmf_number == null) ? '' : task_details[0].dmf_number;
        sales_force_task.requested_deadline = (task_details[0].rdd == null || task_details[0].rdd == '') ? '' : dateFormat(task_details[0].rdd, "yyyy-mm-dd");
        break;

      case 40:
        //url = Config.sales_force.task3;
        url = (Config.sampark.use == true) ? Config.sampark.task3 : Config.sales_force.task3;
        sales_force_task.gmp_clearance_id = task_details[0].gmp_clearance_id;
        sales_force_task.email = task_details[0].tga_email_id;
        sales_force_task.applicant_details = task_details[0].applicant_name;
        sales_force_task.documents_required = task_details[0].doc_required;
        sales_force_task.requested_deadline = (task_details[0].rdd == null || task_details[0].rdd == '') ? '' : dateFormat(task_details[0].rdd, "yyyy-mm-dd");
        break;


      case 7:
      case 34:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        sales_force_task.nature_of_issue = task_details[0].nature_of_issue;
        sales_force_task.batch_no = task_details[0].batch_number;

        if (trim(task_details[0].quantity) != '') {
          let quant = trim(task_details[0].quantity);
          let quant_trim = quant.split(' ');
          quantity = quant_trim[0];
          quantity_unit = quant_trim[1];
        }

        sales_force_task.quantity_unit = quantity_unit;
        sales_force_task.quantity = quantity;
        break;

      case 23:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        if (trim(task_details[0].quantity) != '') {
          console.log('trim quantity', trim(task_details[0].quantity));
          let quant = trim(task_details[0].quantity);
          let quant_trim = quant.split(' ');
          quantity = quant_trim[0];
          quantity_unit = quant_trim[1];
        }

        sales_force_task.quantity_unit = quantity_unit;
        sales_force_task.quantity = quantity;

        sales_force_task.date_of_delivery = (task_details[0].rdd == null || task_details[0].rdd == '') ? '' : dateFormat(task_details[0].rdd, "yyyy-mm-dd");
        break;

      case 31:
      case 41:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        if (trim(task_details[0].quantity) != '') {
          console.log('trim quantity', trim(task_details[0].quantity));
          let quant = trim(task_details[0].quantity);
          let quant_trim = quant.split(' ');
          quantity = quant_trim[0];
          quantity_unit = quant_trim[1];
        }

        sales_force_task.quantity_unit = quantity_unit;
        sales_force_task.quantity = quantity;

        sales_force_task.date_of_delivery = (task_details[0].rdd == null || task_details[0].rdd == '') ? '' : dateFormat(task_details[0].rdd, "yyyy-mm-dd");
        sales_force_task.pharmacopoeia = task_details[0].pharmacopoeia;
        break;

      case 44:
        //url = Config.sales_force.task2;
        url = (Config.sampark.use == true) ? Config.sampark.task2 : Config.sales_force.task2;
        if (trim(task_details[0].quantity) != '') {
          console.log('trim quantity', trim(task_details[0].quantity));
          let quant = trim(task_details[0].quantity);
          let quant_trim = quant.split(' ');
          quantity = quant_trim[0];
          quantity_unit = quant_trim[1];
        }

        sales_force_task.quantity_unit = quantity_unit;
        sales_force_task.quantity = quantity;

        sales_force_task.pharmacopoeia = task_details[0].pharmacopoeia;
        break;

      default:
        break;
    }

    // if(files && files.length > 0){
    //   let sales_force_files = [];
    //   for (let index = 0; index < files.length; index++) {
    //     const element = files[index];
    //     let base_64_stream = await module.exports.__get_base64(element.key,element.originalname);

    //     if(base_64_stream.status == 1){
    //       sales_force_files.push({file_name:element.originalname,file_content:base_64_stream.base64});
    //     }else{
    //       proceed = false;
    //       break;
    //     }


    //   }
    //   sales_force_task.uploaded_files = sales_force_files;
    // }

    if (url != '') {
      if (task_details[0].language != Config.default_language) {
        sales_force_task = await module.exports.get_english_content(sales_force_task);
      }
      //console.log('sales_force_task',sales_force_task);
      let sales_response_task = await outwardCrm.add_task(sales_force_task, url);
      if (sales_response_task.status == 1) {
        if (sales_response_task.data.sales_task_id != '') {
          await taskModel.update_task_details(sales_response_task.data.sales_task_id, task_id);

          if (files && files.length > 0) {
            let sales_force_files = [];
            let file_proceed = true;
            for (let index = 0; index < files.length; index++) {
              const element = files[index];
              let base_64_stream = await module.exports.__get_base64(element.key, element.originalname);

              if (base_64_stream.status == 1) {
                sales_force_files.push({
                  attributes: {
                    type: "ContentVersion",
                    referenceId: element.key
                  },
                  PathOnClient: element.originalname,
                  VersionData: base_64_stream.base64,
                  Description: '',
                  RecId__c: sales_response_task.data.sales_task_id
                });
              } else {
                file_proceed = false;
                break;
              }
            }
            if (file_proceed) {
              let file_sf_body = {
                records: sales_force_files
              };

              let sales_file_response_task = await outwardCrm.upload_file(file_sf_body);
              if (sales_file_response_task.status == 2) {
                common.outwardCrmError(sales_file_response_task.error, `Error Crm While Adding Task Files ${Config.drupal.url}`, `Task ID:${task_id}`);
              }
            }
          }

        }
      } else {
        common.outwardCrmError(sales_response_task.error, `Error Crm While Adding Task ${Config.drupal.url}`, `Task ID:${task_id}`);
      }
    } else {
      common.outwardCrmError(`URL is blank for request Id ${task_details[0].request_type}`, `Error Crm While Adding Task ${Config.drupal.url}`, `Task ID:${task_id}`);
    }


    //console.log('sales_force_task',sales_force_task);


  },
  /**
   * Generates a English Task Content 
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} taskObj - HTTP request argument
   * @return {Json Object} - English Task Content  used for Sales Force selection
   */
  get_english_content: async (taskObj) => {

    let translatedContent = await taskModel.get_translated_task(taskObj.task_id);

    if (taskObj.requirement && taskObj.requirement != '') {
      taskObj.requirement = translatedContent[0].content;
    }

    if (taskObj.pharmacopoeia && taskObj.pharmacopoeia != '') {
      taskObj.pharmacopoeia = translatedContent[0].pharmacopoeia;
    }

    if (taskObj.polymorphic_form && taskObj.polymorphic_form != '') {
      taskObj.polymorphic_form = translatedContent[0].polymorphic_form;
    }

    if (taskObj.site_name && taskObj.site_name != '') {
      taskObj.site_name = translatedContent[0].audit_visit_site_name;
    }

    if (taskObj.batch_no && taskObj.batch_no != '') {
      taskObj.batch_no = translatedContent[0].batch_number;
    }

    if (taskObj.nature_of_issue && taskObj.nature_of_issue != '') {
      taskObj.nature_of_issue = translatedContent[0].nature_of_issue;
    }

    //IMPLURITIES
    if (taskObj.impurities && taskObj.impurities != '' && taskObj.impurities.specify_impurities && taskObj.impurities.specify_impurities != '') {
      taskObj.impurities.specify_impurities = translatedContent[0].specify_impurity_required;
    }

    if (taskObj.shipping_address && taskObj.shipping_address != '') {
      taskObj.shipping_address = translatedContent[0].shipping_address;
    }

    //SAMPLE
    // if (taskObj.sample && taskObj.sample != '' && taskObj.sample.batch && taskObj.sample.batch != '') {
    //   taskObj.batch = translatedContent[0].number_of_batches;
    // }

    if (taskObj.dmf_number && taskObj.dmf_number != '') {
      taskObj.dmf_number = translatedContent[0].dmf_number;
    }

    if (taskObj.notification_number && taskObj.notification_number != '') {
      taskObj.notification_number = translatedContent[0].notification_number;
    }

    if (taskObj.gmp_clearance_id && taskObj.gmp_clearance_id != '') {
      taskObj.gmp_clearance_id = translatedContent[0].gmp_clearance_id;
    }

    if (taskObj.email && taskObj.email != '') {
      taskObj.email = translatedContent[0].tga_email_id;
    }

    if (taskObj.applicant_details && taskObj.applicant_details != '') {
      taskObj.applicant_details = translatedContent[0].applicant_name;
    }

    if (taskObj.documents_required && taskObj.documents_required != '') {
      taskObj.documents_required = translatedContent[0].doc_required;
    }

    return taskObj;

  },
  /**
   * Generates a Sales Force Add Internal Discussions
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} task_id - HTTP request argument
   * @param {Object} files - HTTP request argument
   * @param {Object} discussion - HTTP request argument
   * @param {Object} emp_id - HTTP request argument
   * @return {Json Object} - Sales Force Add Internal Discussions used for Sales Force selection
   */
  __post_sales_force_internal_discussions: async (task_id, files, discussion, emp_id) => {

    let proceed = true;
    let task_details = await taskModel.get_sales_force_data(task_id);
    if (task_details[0].sales_task_id && task_details[0].sales_task_id != null) {
      let employee = await employeeModel.get_user(emp_id);

      let sales_force_discussion = {
        task_id: `${task_details[0].task_id}`,
        sales_task_id: `${task_details[0].sales_task_id}`,
        posted_by: `${employee[0].first_name} ${employee[0].last_name}`,
        content: `${discussion}`,
        date_posted: common.currentDateTime()

      };

      // if(files && files.length > 0){
      //   let sales_force_files = [];
      //   for (let index = 0; index < files.length; index++) {
      //     const element = files[index];
      //     let base_64_stream = await module.exports.__get_base64(element.key,element.originalname);

      //     if(base_64_stream.status == 1){
      //       sales_force_files.push({file_name:element.originalname,file_content:base_64_stream.base64});
      //     }else{
      //       proceed = false;
      //       break;
      //     }


      //   }
      //   sales_force_discussion.uploaded_files = sales_force_files;
      // }


      let sales_response_discussions = await outwardCrm.internal_discussions(sales_force_discussion);
      if (sales_response_discussions.status == 1) {

        if (sales_response_discussions.data.sales_task_id != '' && sales_response_discussions.data.sales_task_id != null && sales_response_discussions.data.sales_idc_id != '' && sales_response_discussions.data.sales_idc_id != null) {
          console.log('ok from sales force');
          if (files && files.length > 0) {
            console.log('files');
            let sales_force_files = [];
            let file_proceed = true;
            for (let index = 0; index < files.length; index++) {
              const element = files[index];
              let base_64_stream = await module.exports.__get_base64(element.key, element.originalname);

              if (base_64_stream.status == 1) {
                sales_force_files.push({
                  attributes: {
                    type: "ContentVersion",
                    referenceId: element.key
                  },
                  PathOnClient: element.originalname,
                  VersionData: base_64_stream.base64,
                  Description: '',
                  RecId__c: `${task_details[0].sales_task_id}`,
                  discussion_id__c: `${sales_response_discussions.data.sales_idc_id}`
                });
              } else {
                file_proceed = false;
                break;
              }
            }
            if (file_proceed) {
              let file_sf_body = {
                records: sales_force_files
              };

              let sales_file_response_task = await outwardCrm.upload_file(file_sf_body);
              if (sales_file_response_task.status == 2) {
                common.outwardCrmError(sales_file_response_task.error, `Error Crm While Adding Files For Internal Discussion ${Config.drupal.url}`, `Task ID:${task_id}`);
              }
            }
          }
        }

      } else {
        common.outwardCrmError(sales_response_discussions.error, `Error Crm While Adding Internal Discussion ${Config.drupal.url}`, `Task ID:${task_id}`);
      }
    }
  },
  /**
   * Generates a Sales Force Add External Discussions
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} task_id - HTTP request argument
   * @param {Object} files - HTTP request argument
   * @param {Object} ccp_posted_by - HTTP request argument
   * @return {Json Object} - Sales Force Add External Discussions used for Sales Force selection
   */
  __post_sales_force_external_discussions: async (task_id, files, discussion) => {
    console.log('discussion ===========>>>>', discussion);
    let proceed = true;
    let task_details = await taskModel.get_sales_force_data(task_id);

    if (task_details[0].sales_task_id && task_details[0].sales_task_id != null) {
      let sales_force_discussion = {
        task_id: `${task_details[0].task_id}`,
        sales_task_id: `${task_details[0].sales_task_id}`,
        posted_by: `${task_details[0].customer_full_name}`,
        posted_by_type: `C`,
        content: `${discussion}`,
        date_posted: common.currentDateTime()

      };

      // if(files && files.length > 0){
      //   let sales_force_files = [];
      //   for (let index = 0; index < files.length; index++) {
      //     const element = files[index];
      //     let base_64_stream = await module.exports.__get_base64(element.key,element.originalname);

      //     if(base_64_stream.status == 1){
      //       sales_force_files.push({file_name:element.originalname,file_content:base_64_stream.base64});
      //     }else{
      //       proceed = false;
      //       break;
      //     }


      //   }
      //   sales_force_discussion.uploaded_files = sales_force_files;
      // }


      let sales_response_discussions = await outwardCrm.external_discussions(sales_force_discussion);
      if (sales_response_discussions.status == 1) {

        if (sales_response_discussions.data.sales_task_id != '' && sales_response_discussions.data.sales_task_id != null && sales_response_discussions.data.sales_edc_id != '' && sales_response_discussions.data.sales_edc_id != null) {
          console.log('ok from sales force');
          if (files && files.length > 0) {
            console.log('files');
            let sales_force_files = [];
            let file_proceed = true;
            for (let index = 0; index < files.length; index++) {
              const element = files[index];
              let base_64_stream = await module.exports.__get_base64(element.key, element.originalname);

              if (base_64_stream.status == 1) {
                sales_force_files.push({
                  attributes: {
                    type: "ContentVersion",
                    referenceId: element.key
                  },
                  PathOnClient: element.originalname,
                  VersionData: base_64_stream.base64,
                  Description: '',
                  RecId__c: `${task_details[0].sales_task_id}`,
                  discussion_id__c: `${sales_response_discussions.data.sales_edc_id}`
                });
              } else {
                file_proceed = false;
                break;
              }
            }
            if (file_proceed) {
              let file_sf_body = {
                records: sales_force_files
              };

              let sales_file_response_task = await outwardCrm.upload_file(file_sf_body);
              if (sales_file_response_task.status == 2) {
                common.outwardCrmError(sales_file_response_task.error, `Error Crm While Adding External Discussion Files ${Config.drupal.url}`, `Task ID:${task_id}`);
              }
            }
          }
        }

      } else {
        common.outwardCrmError(sales_response_discussions.error, `Error Crm While Adding External Discussion ${Config.drupal.url}`, `Task ID:${task_id}`);
      }
    }
  },
  /**
   * Generates a Get Base64 
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} key - HTTP request argument
   * @param {Object} file_name - HTTP request argument
   * @return {Json Object} - Get Base64  used for Decode
   */
  __get_base64: async (key, file_name) => {

    AWS.config.update({
      accessKeyId: Config.aws.accessKeyId,
      secretAccessKey: Config.aws.secretAccessKey
    });
    let s3 = new AWS.S3();
    let options = {
      Bucket: Config.aws.bucketName,
      Key: key
    };
    return new Promise((resolve, reject) => {
      s3.getObject(options, function (err, data) {
        if (err === null) {

          // const fileDataDecoded = Buffer.from(data.Body.toString('base64'),'base64');
          // fs.writeFile(`${Config.upload.task_compressed}${file_name}`, fileDataDecoded, function(err) {
          //     //Handle Error
          //     console.log('=======');
          //     console.log(file_name);
          // });

          resolve({
            status: 1,
            base64: data.Body.toString('base64')
          });
        } else {
          resolve({
            status: 2,
            error: err
          });
        }
      });
    });
  },
  /**
   * Generates a Translate Task Content 
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} taskObj - HTTP request argument
   * @param {Object} task_id - HTTP request argument
   * @param {Object} country_id - HTTP request argument
   * @return {Json Object} - Translate Task Content  used for Sales Force selection
   */
  __translate_language: async (taskObj, task_id, country_id) => {
    let translate_obj = {
      task_id: task_id,
      content: '',
      pharmacopoeia: '',
      polymorphic_form: '',
      stability_data_type: '',
      audit_visit_site_name: '',
      quantity: '',
      batch_number: '',
      nature_of_issue: '',
      working_quantity: '',
      impurities_quantity: '',
      specify_impurity_required: '',
      shipping_address: '',
      number_of_batches: '',
      payment_orders: '',
      payment_status: '',
      payment_pending: '',
      change_category: '',
      notification_action: '',
      notification_type: '',
      dmf_number: '',
      notification_number: '',
      apos_document_type: '',
      cancel_comment: '',
      gmp_clearance_id: '',
      tga_email_id: '',
      applicant_name: '',
      doc_required: '',
      add_date: common.currentDateTime(),
      service_request_type: ''
    };

    let taskOrgLang = taskObj.language;

    if (taskObj.description && taskObj.description != '') {
      translate_obj.content = await common.getConvertedContent(entities.decode(taskObj.description), taskOrgLang);
    }

    if (taskObj.pharmacopoeia && taskObj.pharmacopoeia != '') {
      translate_obj.pharmacopoeia = await common.getConvertedContent(entities.decode(taskObj.pharmacopoeia), taskOrgLang);
    }

    if (taskObj.polymorphic_form && taskObj.polymorphic_form != '') {
      translate_obj.polymorphic_form = await common.getConvertedContent(entities.decode(taskObj.polymorphic_form), taskOrgLang);
    }

    // if(taskObj.stability_data_type && taskObj.stability_data_type != ''){
    //   translate_obj.stability_data_type = await common.getConvertedContent(entities.decode(taskObj.stability_data_type),taskOrgLang);
    // }

    if (taskObj.audit_visit_site_name && taskObj.audit_visit_site_name != '') {
      translate_obj.audit_visit_site_name = await common.getConvertedContent(entities.decode(taskObj.audit_visit_site_name), taskOrgLang);
    }

    // if(taskObj.quantity && taskObj.quantity != ''){
    //   translate_obj.quantity = await common.getConvertedContent(entities.decode(taskObj.quantity),taskOrgLang);
    // }

    if (taskObj.batch_number && taskObj.batch_number != '') {
      translate_obj.batch_number = await common.getConvertedContent(entities.decode(taskObj.batch_number), taskOrgLang);
    }

    if (taskObj.nature_of_issue && taskObj.nature_of_issue != '') {
      translate_obj.nature_of_issue = await common.getConvertedContent(entities.decode(taskObj.nature_of_issue), taskOrgLang);
    }

    // if(taskObj.working_quantity && taskObj.working_quantity != ''){
    //   translate_obj.working_quantity = await common.getConvertedContent(entities.decode(taskObj.working_quantity),taskOrgLang);
    // }

    // if(taskObj.impurities_quantity && taskObj.impurities_quantity != ''){
    //   translate_obj.impurities_quantity = await common.getConvertedContent(entities.decode(taskObj.impurities_quantity),taskOrgLang);
    // }

    if (taskObj.specify_impurity_required && taskObj.specify_impurity_required != '') {
      translate_obj.specify_impurity_required = await common.getConvertedContent(entities.decode(taskObj.specify_impurity_required), taskOrgLang);
    }

    if (taskObj.shipping_address && taskObj.shipping_address != '') {
      translate_obj.shipping_address = await common.getConvertedContent(entities.decode(taskObj.shipping_address), taskOrgLang);
    }

    if (taskObj.number_of_batches && taskObj.number_of_batches != '') {
      translate_obj.number_of_batches = await common.getConvertedContent(taskObj.number_of_batches, taskOrgLang);
    }

    if (taskObj.dmf_number && taskObj.dmf_number != '') {
      translate_obj.dmf_number = await common.getConvertedContent(entities.decode(taskObj.dmf_number), taskOrgLang);
    }

    if (taskObj.notification_number && taskObj.notification_number != '') {
      translate_obj.notification_number = await common.getConvertedContent(entities.decode(taskObj.notification_number), taskOrgLang);
    }

    // if(taskObj.apos_document_type && taskObj.apos_document_type != ''){
    //   translate_obj.apos_document_type = await common.getConvertedContent(taskObj.apos_document_type,taskOrgLang);
    // }

    if (taskObj.gmp_clearance_id && taskObj.gmp_clearance_id != '') {
      translate_obj.gmp_clearance_id = await common.getConvertedContent(entities.decode(taskObj.gmp_clearance_id), taskOrgLang);
    }

    if (taskObj.tga_email_id && taskObj.tga_email_id != '') {
      translate_obj.tga_email_id = await common.getConvertedContent(entities.decode(taskObj.tga_email_id), taskOrgLang);
    }

    if (taskObj.applicant_name && taskObj.applicant_name != '') {
      translate_obj.applicant_name = await common.getConvertedContent(entities.decode(taskObj.applicant_name), taskOrgLang);
    }

    if (taskObj.doc_required && taskObj.doc_required != '') {
      translate_obj.doc_required = await common.getConvertedContent(entities.decode(taskObj.doc_required), taskOrgLang);
    }

    if (taskObj.service_request_type && taskObj.service_request_type != '') {
      translate_obj.service_request_type = await common.getConvertedContent(taskObj.service_request_type, taskOrgLang);
    }

    await taskModel.add_task_translate(translate_obj);

  },
  task_ship_file_download: async (req, res, next) => {
    try {
      const invoice_arr = cryptr.decrypt(req.value.params.id).split('#');
      const type = req.value.params.type;
      //console.log("this is invoice_arr",invoice_arr);
      let invoice_id = (invoice_arr.length > 0) ? invoice_arr[0] : '';
      let task_id = (invoice_arr.length > 0) ? invoice_arr[1] : '';

      if (invoice_id && type) {
        let shipFiles = await taskModel.list_track_shipment(invoice_id.toString());

        //console.log(shipFiles);

        var file_key = ''; var actual_file_name = '';
        if (type == 'invoice' && shipFiles.invoice_file_path != '') {
          file_key = shipFiles.invoice_file_path;
          actual_file_name = shipFiles.invoice_file_name;
          await taskModel
            .tasks_details(task_id)
            .then(async function (data) {
              if (data[0].request_type == 23 || data[0].request_type == 43) {
                let customer_type = '';
                let customer_id = '';
                if (req.user.role == 1) {
                  customer_type = 'C';
                  customer_id = req.user.customer_id;
                } else if (req.user.role == 2) {
                  customer_type = 'A';
                  customer_id = req.user.agent_id
                }
                let trackingObj = {
                  task_id: data[0].task_id,
                  customer_id: customer_id,
                  customer_type: customer_type,
                  invoice_id: invoice_id
                }
                const check_tracking_invoice = await taskModel.check_tracking_invoice(trackingObj);
                if (check_tracking_invoice && check_tracking_invoice.length > 0) {
                  if (check_tracking_invoice[0].id > 0) {
                    trackingObj.date_updated = common.currentDateTime();
                    const update_tracking_invoice = await taskModel.update_tracking_invoice(trackingObj)
                    if (update_tracking_invoice && update_tracking_invoice[0].id != undefined) {
                      console.log("Tracking data updated .");
                    } else {
                      console.log("Error in Updating Tracking Data");
                    }
                  }
                } else {
                  trackingObj.date_added = common.currentDateTime();
                  trackingObj.date_updated = common.currentDateTime();
                  const insert_tracking_data_invoice = await taskModel.insert_tracking_data_invoice(trackingObj)
                  if (insert_tracking_data_invoice && insert_tracking_data_invoice[0].id != undefined) {
                    console.log("Tracking data Inserted .");
                  } else {
                    console.log("Error in Inserting Tracking Data");
                  }
                }

              }
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              });
            });
        } else if (type == 'package' && shipFiles.packing_file_path != '') {
          file_key = shipFiles.packing_file_path;
          actual_file_name = shipFiles.packing_file_name;
          await taskModel
            .tasks_details(task_id)
            .then(async function (data) {
              if (data[0].request_type == 23 || data[0].request_type == 43) {
                let customer_type = '';
                let customer_id = '';
                if (req.user.role == 1) {
                  customer_type = 'C';
                  customer_id = req.user.customer_id;
                } else if (req.user.role == 2) {
                  customer_type = 'A';
                  customer_id = req.user.agent_id
                }
                let trackingObj = {
                  task_id: data[0].task_id,
                  customer_id: customer_id,
                  customer_type: customer_type,
                  invoice_id: invoice_id
                }
                const check_tracking_packing = await taskModel.check_tracking_packing(trackingObj);
                if (check_tracking_packing && check_tracking_packing.length > 0) {
                  if (check_tracking_packing[0].id > 0) {
                    trackingObj.date_updated = common.currentDateTime();
                    const update_tracking_packing = await taskModel.update_tracking_packing(trackingObj)
                    if (update_tracking_packing && update_tracking_packing[0].id != undefined) {
                      console.log("Tracking data updated .");
                    } else {
                      console.log("Error in Updating Tracking Data");
                    }
                  }
                } else {
                  trackingObj.date_added = common.currentDateTime();
                  trackingObj.date_updated = common.currentDateTime();
                  const insert_tracking_data_packing = await taskModel.insert_tracking_data_packing(trackingObj)
                  if (insert_tracking_data_packing && insert_tracking_data_packing[0].id != undefined) {
                    console.log("Tracking data Inserted .");
                  } else {
                    console.log("Error in Inserting Tracking Data");
                  }
                }

              }
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              });
            });
        } else if (type == 'flight' && shipFiles.flight_booked_doc != '') {
          file_key = shipFiles.flight_booked_doc;
          actual_file_name = shipFiles.flight_booked + ".pdf";
          //let awbFiles = await taskModel.get_awb_id(invoice_id.toString());
          await taskModel
            .tasks_details(task_id)
            .then(async function (data) {
              if (data[0].request_type == 23 || data[0].request_type == 43) {
                let customer_type = '';
                let customer_id = '';
                if (req.user.role == 1) {
                  customer_type = 'C';
                  customer_id = req.user.customer_id;
                } else if (req.user.role == 2) {
                  customer_type = 'A';
                  customer_id = req.user.agent_id
                }
                let trackingObj = {
                  task_id: data[0].task_id,
                  customer_id: customer_id,
                  customer_type: customer_type,
                  invoice_id: invoice_id
                }
                const check_tracking_AWB = await taskModel.check_tracking_AWB(trackingObj);
                if (check_tracking_AWB && check_tracking_AWB.length > 0) {
                  if (check_tracking_AWB[0].id > 0) {
                    trackingObj.date_updated = common.currentDateTime();
                    const update_tracking_AWB = await taskModel.update_tracking_AWB(trackingObj)
                    if (update_tracking_AWB && update_tracking_AWB[0].id != undefined) {
                      console.log("Tracking data updated .");
                    } else {
                      console.log("Error in Updating Tracking Data");
                    }
                  }
                } else {
                  trackingObj.date_added = common.currentDateTime();
                  trackingObj.date_updated = common.currentDateTime();
                  const insert_tracking_data_AWB = await taskModel.insert_tracking_data_AWB(trackingObj)
                  if (insert_tracking_data_AWB && insert_tracking_data_AWB[0].id != undefined) {
                    console.log("Tracking data Inserted .");
                  } else {
                    console.log("Error in Inserting Tracking Data");
                  }
                }

              }
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              });
            });

        } else if (type == 'hawb' && shipFiles.hawb_doc != '') {
          file_key = shipFiles.hawb_doc;
          actual_file_name = shipFiles.hawb + ".pdf";
          await taskModel
            .tasks_details(task_id)
            .then(async function (data) {
              if (data[0].request_type == 23 || data[0].request_type == 43) {
                let customer_type = '';
                let customer_id = '';
                if (req.user.role == 1) {
                  customer_type = 'C';
                  customer_id = req.user.customer_id;
                } else if (req.user.role == 2) {
                  customer_type = 'A';
                  customer_id = req.user.agent_id
                }
                let trackingObj = {
                  task_id: data[0].task_id,
                  customer_id: customer_id,
                  customer_type: customer_type,
                  invoice_id: invoice_id
                }
                const check_tracking_AWB = await taskModel.check_tracking_AWB(trackingObj);
                if (check_tracking_AWB && check_tracking_AWB.length > 0) {
                  if (check_tracking_AWB[0].id > 0) {
                    trackingObj.date_updated = common.currentDateTime();
                    const update_tracking_AWB = await taskModel.update_tracking_AWB(trackingObj)
                    if (update_tracking_AWB && update_tracking_AWB[0].id != undefined) {
                      console.log("Tracking data updated .");
                    } else {
                      console.log("Error in Updating Tracking Data");
                    }
                  }
                } else {
                  trackingObj.date_added = common.currentDateTime();
                  trackingObj.date_updated = common.currentDateTime();
                  const insert_tracking_data_AWB = await taskModel.insert_tracking_data_AWB(trackingObj)
                  if (insert_tracking_data_AWB && insert_tracking_data_AWB[0].id != undefined) {
                    console.log("Tracking data Inserted .");
                  } else {
                    console.log("Error in Inserting Tracking Data");
                  }
                }

              }
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              });
            });
        } else if (type == 'bluedart' && shipFiles.logistics_partner != '') {
          
          let waybill_no = shipFiles.logistics_partner.match(/(\d+)/);
          actual_file_name = waybill_no[0] + ".pdf";
          
          await taskModel.getBlueDartPODDoc(waybill_no[0])
            .then(async function (data) {
              file_key = data[0].file_path;
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              });
            });
        }
        //console.log(file_key);
        if (file_key) {
          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = actual_file_name;
          var new_file_name = file_key;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                // console.log("this is data : ",data);
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });

        } else {
          res
            .status(400)
            .json({
              status: 3,
              message: 'No file found'
            })
            .end();
        }

      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  task_po_file_download: async (req, res, next) => {
    try {
      const sap_request_id = req.value.params.sapid;
      if (sap_request_id) {
        let poFiles = await taskModel.get_task_po_file(sap_request_id);

        var file_key = ''; var actual_file_name = '';
        if (poFiles.new_file_name != '') {
          file_key = poFiles.new_file_name;
          actual_file_name = poFiles.actual_file_name;
        }

        if (file_key) {
          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = actual_file_name;
          var new_file_name = file_key;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });

        } else {
          res
            .status(400)
            .json({
              status: 3,
              message: 'No file found'
            })
            .end();
        }
      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  closed_discussion: async (req, res, next) => {

    try {

      let {
        task_id,
        selected_options,
        comment
      } = req.body;

      let posted_files = req.files;
      let prev_files = await taskModel.select_task_files(task_id);

      let submitted_by = 0;
      if (req.user.role == 2) {
        submitted_by = req.user.customer_id;
      }

      let task_details_arr = await addTaskModel.get_task_details_close_comment(task_id);

      let task_details = task_details_arr[0];

      let ccCustId = await taskModel.task_cc_customer(task_id);

      console.log('task_details', task_details);


      var drupal_task_id = 0;
      var drupal_ref_no = '';
      var drupal_request_category = 0;
      var reference_key = '';

      if (task_details.request_type == 23) {
        drupal_request_category = 1;
        reference_key = 'O';
      }
      else if (task_details.request_type == 7) {
        drupal_request_category = 3;
        reference_key = 'C';
      }
      else if (task_details.request_type == 34) {
        drupal_request_category = 3;
        reference_key = 'C';
      }
      else if (task_details.request_type == 24) {
        drupal_request_category = 4;
        reference_key = 'F';
      }
      else if (task_details.request_type == 41) {
        drupal_request_category = 7;
        reference_key = 'O';
      }
      else {
        drupal_request_category = 2;
        reference_key = 'R';
      }


      var today = common.currentDateTime();

      var sla = await taskModel.get_total_sla(task_details.request_type);

      var days = await common.dayCountExcludingWeekends(sla.total_sla);

      var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");

      if (sla.frontend_sla > 0) {
        var front_due_days = await common.dayCountExcludingWeekends(sla.frontend_sla);
        var front_due_date = dateFormat(common.nextDate(front_due_days, "day"), "yyyy-mm-dd HH:MM:ss");
      } else {
        var front_due_date = common.currentDateTime();
      }

      let language = task_details.language;

      var ccp_task_id = 0;
      if (task_details.request_type == 1) {

        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          pharmacopoeia: task_details.pharmacopoeia,

          shipping_address: task_details.shipping_address,

          number_of_batches: task_details.number_of_batches,
          quantity: task_details.quantity,

          specify_impurity_required: task_details.specify_impurity_required,
          impurities_quantity: task_details.impurities_quantity,

          working_quantity: task_details.working_quantity,

          service_request_type: task_details.service_request_type,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }
        }

        ccp_task_id = await addTaskModel.add_task4(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }


      } else if (task_details.request_type == 2 || task_details.request_type == 3) {
        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id         : task_details.country_id,
          pharmacopoeia: task_details.pharmacopoeia,
          polymorphic_form: task_details.polymorphic_form,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language
        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task1(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 13) {
        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id         : task_details.country_id,

          stability_data_type: task_details.stability_data_type,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task5(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 9) {
        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id              : task_details.country_id,

          audit_visit_site_name: task_details.audit_visit_site_name,
          request_audit_visit_date: task_details.request_audit_visit_date,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task6(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 7 || task_details.request_type == 34) {
        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id              : task_details.country_id,

          quantity: task_details.quantity,
          nature_of_issue: task_details.nature_of_issue,
          batch_number: task_details.batch_number,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task8(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 5 || task_details.request_type == 6 || task_details.request_type == 8 || task_details.request_type == 11 || task_details.request_type == 14 || task_details.request_type == 15 || task_details.request_type == 16 || task_details.request_type == 20) {

        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id         : task_details.country_id,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task2(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (
        task_details.request_type == 4 || task_details.request_type == 10 || task_details.request_type == 12 || task_details.request_type == 17 || task_details.request_type == 18 || task_details.request_type == 19 || task_details.request_type == 21 || task_details.request_type == 22 ||
        task_details.request_type == 35 || task_details.request_type == 36 ||
        task_details.request_type == 37 || task_details.request_type == 33 || task_details.request_type == 32 || task_details.request_type == 30 || task_details.request_type == 27 || task_details.request_type == 28 || task_details.request_type == 29 || task_details.request_type == 38 || task_details.request_type == 39 || task_details.request_type == 40 || task_details.request_type == 42) {

        let apos_document_type = '';
        if (task_details.request_type == 38) {
          apos_document_type = task_details.apos_document_type;
        }

        let new_rdd = '';
        if (task_details.request_type == 39 || task_details.request_type == 40) {
          new_rdd = (task_details.rdd != '') ? task_details.rdd : null;
        } else {
          new_rdd = sla_due_date
        }

        const task_data = {

          due_date: sla_due_date,
          rdd: new_rdd,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id         : task_details.country_id,
          pharmacopoeia: task_details.pharmacopoeia,
          dmf_number: task_details.dmf_number,
          apos_document_type: apos_document_type,
          date_of_response: task_details.date_of_response,
          notification_number: task_details.notification_number,
          share_with_agent: task_details.share_with_agent,

          gmp_clearance_id: task_details.gmp_clearance_id,
          tga_email_id: task_details.tga_email_id,
          applicant_name: task_details.applicant_name,
          doc_required: task_details.doc_required,
          ccp_posted_with: '',
          language: language
        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task3(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 23 || task_details.request_type == 31 || task_details.request_type == 41 || task_details.request_type == 44) {
        const task_data = {

          due_date: sla_due_date,
          rdd: (task_details.rdd != '') ? task_details.rdd : null,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          product_id: task_details.product_id,
          //country_id              : task_details.country_id,

          quantity: task_details.quantity,
          share_with_agent: task_details.share_with_agent,
          pharmacopoeia: task_details.pharmacopoeia,
          ccp_posted_with: '',
          language: language
        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task9(task_data);

        let task_countries = await addTaskModel.task_countries(task_id);

        if (task_countries.length > 0) {
          for (let index = 0; index < task_countries.length; index++) {
            await addTaskModel.map_countries(ccp_task_id, task_countries[index].country_id);
          }
        }

      } else if (task_details.request_type == 24) {
        const task_data = {

          due_date: sla_due_date,
          rdd: sla_due_date,
          front_due_date: front_due_date,
          date_added: today,

          status: 1,
          close_status: 0,
          priority: 2,
          parent_id: 0,

          task_ref: drupal_ref_no,
          drupal_task_id: drupal_task_id,
          request_category: drupal_request_category,

          total_sla_processed: 0,
          front_sla_processed: 0,
          back_sla_processed: 0,

          ccp_posted_by: 0,

          request_type: task_details.request_type,
          customer_id: task_details.customer_id,
          share_with_agent: task_details.share_with_agent,
          ccp_posted_with: '',
          language: language

        }

        if (selected_options == 1) {

          task_data.content = entities.encode(comment);

        } else if (selected_options == 2) {

          if (task_details.content == '' || trim(task_details.content) == '') {
            task_data.content = `${entities.encode(comment)}`;
          } else {
            task_data.content = `${entities.encode(comment)} &lt;p&gt;&amp;nbsp;&lt;/p&gt; ------------------------------ &lt;p&gt;&amp;nbsp;&lt;/p&gt; ${task_details.content}`;
          }

        }

        ccp_task_id = await addTaskModel.add_task7(task_data);

      }


      //console.log(tasks);
      if (ccp_task_id > 0) {
        let task_id = ccp_task_id;

        let ref_no = 'PHL-' + reference_key + '-' + task_id;
        await addTaskModel.update_ref_no(ref_no, task_id);

        let db_task_details = await taskModel.tasks_details(task_id);

        if (language != Config.default_language) {
          let latest_task_details = await taskModel.get_task_details_close_comment(task_id);
          await module.exports.__translate_language(latest_task_details[0], task_id, []);
        }

        let customer_details = await customerModel.get_user(db_task_details[0].customer_id);

        var mail_subject = '';
        if (db_task_details[0].request_type == 24) {
          mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${db_task_details[0].task_ref}`;
        } else {
          mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(db_task_details[0].product_name)} | ${db_task_details[0].task_ref}`;
        }

        let manager = await taskModel.get_allocated_manager(task_details.customer_id);

        manager = await module.exports.__get_allocated_leave_employee(manager, task_details.customer_id);

        console.log('manager', manager);

        if (manager.length > 0) {

          // ACTIVITY LOG TASK POSTED BY - SATYAJIT
          customerData = await customerModel.get_user(task_details.customer_id);
          var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
          var params = {
            task_ref: ref_no,
            request_type: db_task_details[0].req_name,
            customer_name: customername
          }
          if (req.user.role == 2) {
            var agentData = await agentModel.get_user(submitted_by);
            params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
            await taskModel.log_activity(2, task_id, params)
          } else {
            await taskModel.log_activity(1, task_id, params)
          }

          // ACTIVITY LOG FROM SYSTEM - SATYAJIT
          var employee_sender = `${manager[0].first_name} ${manager[0].last_name}`;
          var params = {
            task_ref: ref_no,
            request_type: db_task_details[0].req_name,
            employee_sender: 'SYSTEM',
            employee_recipient: employee_sender + ` (${(manager[0].desig_name)})`
          }
          await taskModel.log_activity(8, task_id, params);
          // END LOG

          let employee_notification = 22;
          let param_notification = {};
          let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

          let cc_arr = [
            employee_notification,
            task_id,
            manager[0].employee_id,
            common.currentDateTime(),
            0,
            notification_text,
            employee_notification,
            mail_subject
          ]
          await taskModel.notify_employee(cc_arr);

          await taskModel.update_task_owner(manager[0].employee_id, task_id);
          const assignment = {
            task_id: task_id,
            assigned_to: manager[0].employee_id,
            assigned_by: -1,
            assign_date: today,
            due_date: sla_due_date,
            reopen_date: today
          }
          let parent_assign_id = await taskModel.assign_task(assignment);

          var emp_details = [];
          //CSC TECHNICAL
          if (
            task_details.request_type == 1 || task_details.request_type == 22 || task_details.request_type == 2 || task_details.request_type == 3 || task_details.request_type == 4 || task_details.request_type == 5 || task_details.request_type == 8 || task_details.request_type == 9 || task_details.request_type == 10 || task_details.request_type == 12 || task_details.request_type == 13 || task_details.request_type == 14 || task_details.request_type == 15 || task_details.request_type == 16 || task_details.request_type == 21 || task_details.request_type == 6 || task_details.request_type == 11 || task_details.request_type == 7 ||
            task_details.request_type == 33 || task_details.request_type == 35 ||
            task_details.request_type == 37 || task_details.request_type == 38 || task_details.request_type == 42
          ) {
            emp_details = await taskModel.get_allocated_employee(task_details.customer_id, 2);

          }

          //CSC COMMERCIAL
          if (task_details.request_type == 23 || task_details.request_type == 34 || task_details.request_type == 41 || task_details.request_type == 44) {
            emp_details = await taskModel.get_allocated_employee(task_details.customer_id, 5);
          }

          //RA
          if (
            task_details.request_type == 17 || task_details.request_type == 18 || task_details.request_type == 19 || task_details.request_type == 20 ||
            task_details.request_type == 27 || task_details.request_type == 28 ||
            task_details.request_type == 29 || task_details.request_type == 30 ||
            task_details.request_type == 36 || task_details.request_type == 39 || task_details.request_type == 40
          ) {
            emp_details = await taskModel.get_allocated_employee(task_details.customer_id, 3);
          }

          var bot_assignment_id = 0;

          emp_details = await module.exports.__get_allocated_leave_employee(emp_details, task_details.customer_id);

          //console.log('emp_details',emp_details);
          let comment_arr = '';
          if (emp_details.length > 0) {
            let assignment_val = 2;
            await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
              .then(async function (data) {


                const assign_tasks = {
                  assigned_by: manager[0].employee_id,
                  assigned_to: emp_details[0].employee_id,
                  task_id: task_id,
                  due_date: sla_due_date,
                  assign_date: today,
                  parent_assignment_id: parent_assign_id.assignment_id,
                  comment: '',
                  reopen_date: today
                }

                await taskModel.assign_task(assign_tasks).then(async function (tdata) {

                  // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                  var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                  var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                  var params = {
                    task_ref: ref_no,
                    request_type: db_task_details[0].req_name,
                    employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                    employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                  }
                  await taskModel.log_activity(8, task_id, params)
                  // END LOG

                  let employee_notification = 8;
                  let param_notification = {
                    employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                    assigned_employee: `you`
                  };
                  let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                  let cc_arr = [
                    employee_notification,
                    task_id,
                    emp_details[0].employee_id,
                    common.currentDateTime(),
                    0,
                    notification_text,
                    employee_notification,
                    mail_subject
                  ]
                  await taskModel.notify_employee(cc_arr);

                  bot_assignment_id = tdata.assignment_id;
                  const task_comment = {
                    posted_by: manager[0].employee_id,
                    task_id: task_id,
                    comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
                    assignment_id: bot_assignment_id,
                    post_date: today
                  };
                  await taskModel.insert_comment_log(task_comment);

                  comment_arr = task_comment;

                  //console.log('child assignment',bot_assignment_id);

                }).catch(err => {
                  common.logError(err);
                  res.status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value
                    }).end();
                });

              }).catch(err => {
                common.logError(err);
                res.status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value
                  }).end();
              })
          } else {
            bot_assignment_id = parent_assign_id.assignment_id;

            //console.log('parent assignment',bot_assignment_id);
          }

          if (emp_details.length > 0) {
            await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
          } else {
            await taskModel.update_current_owner(task_id, manager[0].employee_id);
          }
          //CURRENT OWNER

          if (ccCustId && ccCustId.length > 0) {
            for (let index = 0; index < ccCustId.length; index++) {
              //INSERT
              var insert_val = {
                customer_id: ccCustId[index].customer_id,
                task_id: task_id,
                status: 1
              };
              await customerModel.insert_cc_customer(insert_val);
            }
          }

          var db_due_date = new Date(db_task_details[0].due_date);
          var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

          let highlight_arr = [
            task_id,
            db_task_details[0].customer_id,
            1,
            'C'
          ];
          await taskModel.task_highlight(highlight_arr);

          let cc_list = await taskModel.task_cc_customer(db_task_details[0].task_id);
          if (cc_list && cc_list.length > 0) {
            for (let index = 0; index < cc_list.length; index++) {
              const element = cc_list[index];
              let highlight_cc_arr = [
                task_id,
                element.customer_id,
                1,
                'C'
              ];
              await taskModel.task_highlight(highlight_cc_arr);
            }
          }

          if (db_task_details[0].share_with_agent == 1) {
            let customer_details = await customerModel.get_user(db_task_details[0].customer_id);
            let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

            if (agent_list && agent_list.length > 0) {
              for (let index = 0; index < agent_list.length; index++) {
                const element = agent_list[index];
                let highlight_agent_arr = [
                  task_id,
                  element.agent_id,
                  1,
                  'A'
                ];
                await taskModel.task_highlight(highlight_agent_arr);
              }
            }
          }

          let mail_arr = await module.exports.notify_cc_highlight(req, db_task_details, 6);


          var url_status = `${process.env.REACT_API_URL_PORTAL}task-details/${task_id}`;

          // var cust_status_mail = `
          // <!DOCTYPE html>
          // <html>
          //   <body>
          //       <p><em>Thank you for your request</em></p>
          //       <p>Dear ${customer_details[0].first_name} ${customer_details[0].last_name},</p>
          //       <p></p>
          //       <p>Thank you for placing a request -  ${db_task_details[0].req_name}.</p>
          //       <p>We have received your request with the following details:</p>
          //       <table>
          //         <tr>
          //           <td>Task Ref</td>
          //           <td>${db_task_details[0].task_ref}</td>
          //         </tr>
          //         <tr>
          //           <td>User Name</td>
          //           <td>${db_task_details[0].first_name} ${db_task_details[0].last_name}</td>
          //         </tr>
          //         <tr>
          //           <td>Request Type</td>
          //           <td>${db_task_details[0].req_name}</td>
          //         </tr>

          //         ${(db_task_details[0].request_type == 24)?``:`<tr>
          //         <td>Product</td>
          //         <td>${db_task_details[0].product_name}</td>
          //         </tr>`}

          //         ${(db_task_details[0].request_type == 24)?``:`<tr>
          //           <td>Market</td>
          //           <td>${mail_country_name}</td>
          //         </tr>`}
          //         ${mail_pharmacopoeia}
          //         ${mail_polymorphic_form}
          //         ${mail_stability_data_type}
          //         ${mail_audit_visit_site_name}
          //         ${mail_request_audit_visit_date}
          //         ${mail_quantity}
          //         ${mail_rdd}
          //         ${mail_batch_number}
          //         ${mail_nature_of_issue}
          //         ${mail_samples}
          //         ${mail_working_standards}
          //         ${mail_impurities}
          //         ${entities.decode(mail_shipping_address)}
          //         ${mail_dmf_number}
          //         ${mail_notification_number}
          //         ${mail_rdfrc}
          //         ${mail_apos_document_type}
          //         <tr>
          //           <td valign="top">Requirement</td>
          //           <td valign="top">${entities.decode(db_task_details[0].content)}</td>
          //         </tr>
          //         <tr>
          //           <td>&nbsp;</td>
          //           <td></td>
          //         </tr>
          //         <tr>
          //           <td>&nbsp;</td>
          //           <td></td>
          //         </tr>
          //       </table>
          //       <p>Your Dr Reddys team is working on the request and will respond to you shortly with the status. Click <a href="${url_status}" >here</a> to see more details about this query</p>
          //       <p>--  Dr.Reddys API team</p>
          //   </body>
          // </html>
          // `;

          // var mail_options1 = {
          //   from: Config.noReplyMail,
          //   to:  mail_arr[0].to_arr,
          //   subject: mail_subject,
          //   html: cust_status_mail
          // };
          // if(mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0){
          //   mail_options1.cc = mail_arr[1].cc_arr;
          // }
          // common.sendMailB2C(mail_options1);

          var country = await taskModel.tasks_details_countries(task_id);

          if (country && country[0].country_name != '') {
            var mail_country_name = country[0].country_name;
          } else {
            var mail_country_name = '';
          }

          console.log("REQUEST TYPE ", db_task_details[0].request_type);
          var language_extn = 'req_name_' + db_task_details[0].language_code;
          var req_name_by_lang = db_task_details[0].req_name;
          switch (entities.decode(db_task_details[0].language_code)) {
            case 'es': req_name_by_lang = db_task_details[0].req_name_es;
              break;
            case 'pt': req_name_by_lang = db_task_details[0].req_name_pt;
              break;
            case 'ja': req_name_by_lang = db_task_details[0].req_name_ja;
              break;
            case 'zh': req_name_by_lang = db_task_details[0].req_name_zh;
              break;
            default: req_name_by_lang = db_task_details[0].req_name;
              break;
          }

          if (db_task_details[0].request_type === 4 || db_task_details[0].request_type === 10 || db_task_details[0].request_type === 12 || db_task_details[0].request_type === 17 || db_task_details[0].request_type === 18 || db_task_details[0].request_type === 19 || db_task_details[0].request_type === 21 || db_task_details[0].request_type === 22 || db_task_details[0].request_type == 30 || db_task_details[0].request_type == 32 || db_task_details[0].request_type == 33 || db_task_details[0].request_type == 35 || db_task_details[0].request_type == 36 || db_task_details[0].request_type == 37 || db_task_details[0].request_type == 42) {

            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                pharmacopoeia: db_task_details[0].pharmacopoeia,
                req_name: db_task_details[0].req_name
              };

              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_15TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam, "c");
            }

            //Manager Mail
            let managerMailParam = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              pharmacopoeia: db_task_details[0].pharmacopoeia,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                pharmacopoeia: db_task_details[0].pharmacopoeia,
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_15TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam, "e");
            }
          } else if (db_task_details[0].request_type === 5 || db_task_details[0].request_type === 6 || db_task_details[0].request_type === 8 || db_task_details[0].request_type === 11 || db_task_details[0].request_type === 14 || db_task_details[0].request_type === 15 || db_task_details[0].request_type === 16 || db_task_details[0].request_type === 20) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam1 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name
              };

              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COMMON_8TYPES', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam1, "c");
            }

            //Manager Mail
            let managerMailParam1 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', manager[0].email, Config.webmasterMail, '', managerMailParam1, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam1 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COMMON_8TYPES', emp_details[0].email, Config.webmasterMail, '', empMailParam1, "e");
            }
          } else if (db_task_details[0].request_type === 2 || db_task_details[0].request_type === 3) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam2 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                pharmacopoeia: db_task_details[0].pharmacopoeia,
                polymorphic_form: db_task_details[0].polymorphic_form,
                req_name: db_task_details[0].req_name
              };

              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_COA_AND_MOA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam2, "c");
            }

            //Manager Mail
            let managerMailParam2 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              pharmacopoeia: db_task_details[0].pharmacopoeia,
              polymorphic_form: db_task_details[0].polymorphic_form,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', manager[0].email, Config.webmasterMail, '', managerMailParam2, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam2 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                pharmacopoeia: db_task_details[0].pharmacopoeia,
                polymorphic_form: db_task_details[0].polymorphic_form,
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_COA_AND_MOA', emp_details[0].email, Config.webmasterMail, '', empMailParam2, "e");
            }
          } else if (db_task_details[0].request_type === 7 || db_task_details[0].request_type === 34) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam3 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                nature_of_issue: db_task_details[0].nature_of_issue,
                quantity: db_task_details[0].quantity,
                batch_number: db_task_details[0].batch_number,
                req_name: db_task_details[0].req_name
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }
              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam3, "c");
            }

            //Manager Mail
            let managerMailParam3 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              nature_of_issue: db_task_details[0].nature_of_issue,
              quantity: db_task_details[0].quantity,
              batch_number: db_task_details[0].batch_number,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', manager[0].email, Config.webmasterMail, '', managerMailParam3, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam3 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                nature_of_issue: db_task_details[0].nature_of_issue,
                quantity: db_task_details[0].quantity,
                batch_number: db_task_details[0].batch_number,
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_QUALITY_AND_LOGISTIC_COMPLAIN', emp_details[0].email, Config.webmasterMail, '', empMailParam3, "e");
            }
          } else if (db_task_details[0].request_type === 9) {
            var visit_date = new Date(db_task_details[0].request_audit_visit_date);
            var visit_date_updt = common.changeDateFormat(visit_date, '/', 'dd.mm.yyyy');

            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam4 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                audit_visit_site_name: db_task_details[0].audit_visit_site_name,
                audit_visit_date: visit_date_updt,
                req_name: db_task_details[0].req_name
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam4, "c");
            }

            //Manager Mail
            let managerMailParam4 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              audit_visit_site_name: db_task_details[0].audit_visit_site_name,
              audit_visit_date: visit_date_updt,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam4, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam4 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                audit_visit_site_name: db_task_details[0].audit_visit_site_name,
                audit_visit_date: visit_date_updt,
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_AUDIT_AND_VISIT_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam4, "e");
            }
          } else if (db_task_details[0].request_type === 13) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam5 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                stability_data_type: db_task_details[0].stability_data_type,
                req_name: db_task_details[0].req_name
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_STABILITY_DATA', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam5, "c");
            }

            //Manager Mail
            let managerMailParam5 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              stability_data_type: db_task_details[0].stability_data_type,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', manager[0].email, Config.webmasterMail, '', managerMailParam5, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam5 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                stability_data_type: db_task_details[0].stability_data_type,
                req_name: db_task_details[0].req_name,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_STABILITY_DATA', emp_details[0].email, Config.webmasterMail, '', empMailParam5, "e");
            }
          } else if (db_task_details[0].request_type === 23) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam6 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy')
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NEW_ORDER', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam6, "c");
            }

            //Manager Mail
            let managerMailParam6 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              quantity: db_task_details[0].quantity,
              rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', manager[0].email, Config.webmasterMail, '', managerMailParam6, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam6 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NEW_ORDER', emp_details[0].email, Config.webmasterMail, '', empMailParam6, "e");
            }
          } else if (db_task_details[0].request_type === 41) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam7 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                pharmacopoeia: db_task_details[0].pharmacopoeia
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam7, "c");
            }

            //Manager Mail
            let managerMailParam7 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              quantity: db_task_details[0].quantity,
              rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
              pharmacopoeia: db_task_details[0].pharmacopoeia,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', manager[0].email, Config.webmasterMail, '', managerMailParam7, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam7 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                pharmacopoeia: db_task_details[0].pharmacopoeia,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PROFORMA_INVOICE_REQ', emp_details[0].email, Config.webmasterMail, '', empMailParam7, "e");
            }
          } else if (db_task_details[0].request_type === 31) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam8 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_PRICE_QUOTE', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam8, "c");
            }

            //Manager Mail
            let managerMailParam8 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              quantity: db_task_details[0].quantity,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', manager[0].email, Config.webmasterMail, '', managerMailParam8, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam8 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_PRICE_QUOTE', emp_details[0].email, Config.webmasterMail, '', empMailParam8, "e");
            }
          } else if (db_task_details[0].request_type === 27) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam9 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                dmf_number: db_task_details[0].dmf_number,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DMF_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam9, "c");
            }

            //Manager Mail
            let managerMailParam9 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              dmf_number: db_task_details[0].dmf_number,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam9, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam9 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                dmf_number: db_task_details[0].dmf_number,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DMF_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam9, "e");
            }
          } else if (db_task_details[0].request_type === 28) {
            if (db_task_details[0].rdfrc == '' || db_task_details[0].rdfrc == null) {
              var rdfrc_date_updt = '';
            } else {
              var rdfrc_date = new Date(db_task_details[0].rdfrc);
              var rdfrc_date_updt = common.changeDateFormat(rdfrc_date, '/', 'dd.mm.yyyy');
            }
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam91 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                rdfrc_date_updt: rdfrc_date_updt,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam91, "c");
            }

            //Manager Mail
            let managerMailParam91 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              rdfrc_date_updt: rdfrc_date_updt,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam91, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam91 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                rdfrc_date_updt: rdfrc_date_updt,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_DEFICIENCIES_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam91, "e");
            }
          } else if (db_task_details[0].request_type === 29) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail            
              let mailParam10 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                notification_number: db_task_details[0].notification_number,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam10, "c");
            }

            //Manager Mail
            let managerMailParam10 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              notification_number: db_task_details[0].notification_number,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', manager[0].email, Config.webmasterMail, '', managerMailParam10, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam10 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                notification_number: db_task_details[0].notification_number,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_NOTIFICATION_QUERY', emp_details[0].email, Config.webmasterMail, '', empMailParam10, "e");
            }
          } else if (db_task_details[0].request_type === 38) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail                
              let mailParam11 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                document_type: db_task_details[0].apos_document_type,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam11, "c");
            }

            //Manager Mail
            let managerMailParam11 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              document_type: db_task_details[0].apos_document_type,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', manager[0].email, Config.webmasterMail, '', managerMailParam11, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam11 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                document_type: db_task_details[0].apos_document_type,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_APOSTALLATION_OF_DOCUMENTS', emp_details[0].email, Config.webmasterMail, '', empMailParam11, "e");
            }
          } else if (db_task_details[0].request_type === 39) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail 
              let mailParam12 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                dmf_number: db_task_details[0].dmf_number,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy')
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam12, "c");
            }

            //Manager Mail
            let managerMailParam12 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              dmf_number: db_task_details[0].dmf_number,
              rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', manager[0].email, Config.webmasterMail, '', managerMailParam12, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam12 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                dmf_number: db_task_details[0].dmf_number,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_N-NITROSOAMINES_DECLARATIONS', emp_details[0].email, Config.webmasterMail, '', empMailParam12, "e");
            }
          } else if (db_task_details[0].request_type === 40) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail          
              let mailParam13 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                gmp_clearance_id: db_task_details[0].gmp_clearance_id,
                tga_email_id: db_task_details[0].tga_email_id,
                applicant_name: db_task_details[0].applicant_name,
                doc_required: db_task_details[0].doc_required
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }
              console.log("SENDING TASK MAIL 40");
              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam13, "c");
            }

            //Manager Mail
            let managerMailParam13 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
              gmp_clearance_id: db_task_details[0].gmp_clearance_id,
              tga_email_id: db_task_details[0].tga_email_id,
              applicant_name: db_task_details[0].applicant_name,
              doc_required: db_task_details[0].doc_required,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', manager[0].email, Config.webmasterMail, '', managerMailParam13, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam13 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                rdd: common.changeDateFormat(db_task_details[0].rdd, '/', 'dd.mm.yyyy'),
                gmp_clearance_id: db_task_details[0].gmp_clearance_id,
                tga_email_id: db_task_details[0].tga_email_id,
                applicant_name: db_task_details[0].applicant_name,
                doc_required: db_task_details[0].doc_required,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_TGA_GMP_CLEARANCE_DOC', emp_details[0].email, Config.webmasterMail, '', empMailParam13, "e");
            }
          } else if (db_task_details[0].request_type === 24) {
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail 
              let mailParam = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                req_name: db_task_details[0].req_name,
                company_name: entities.decode(customer_details[0].company_name),
              };

              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_FORCAST', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam, "c");
            }

            //Manager Mail
            let managerMailParam = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              format_due_date: format_due_date,
              req_name: db_task_details[0].req_name,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name
            };

            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_FORCAST', manager[0].email, Config.webmasterMail, '', managerMailParam, "e");
          } else if (db_task_details[0].request_type === 44) {
            //console.log(mail_arr);
            if (mail_arr.length > 0 && mail_arr[0].to_arr.length > 0) {
              //customer mail
              let mailParam14 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                task_content: entities.decode(db_task_details[0].content),
                url_status: url_status,
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                pharmacopoeia: db_task_details[0].pharmacopoeia,
              };
              var mailcc = '';
              if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                mailcc = mail_arr[1].cc_arr;
              }

              const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', mail_arr[0].to_arr, Config.contactUsMail, mailcc, entities.decode(db_task_details[0].language), mailParam14, "c");
            }

            //Manager Mail
            let managerMailParam14 = {
              first_name: entities.decode(customer_details[0].first_name),
              last_name: entities.decode(customer_details[0].last_name),
              task_ref: db_task_details[0].task_ref,
              product_name: db_task_details[0].product_name,
              country_name: mail_country_name,
              format_due_date: format_due_date,
              task_content: entities.decode(db_task_details[0].content),
              company_name: entities.decode(customer_details[0].company_name),
              req_name: db_task_details[0].req_name,
              quantity: db_task_details[0].quantity,
              ccpurl: Config.ccpUrl,
              emp_first_name: manager[0].first_name,
              pharmacopoeia: db_task_details[0].pharmacopoeia,
            };
            const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', manager[0].email, Config.webmasterMail, '', managerMailParam14, "e");

            if (emp_details.length > 0) {
              //Employee Mail
              var empMailParam14 = {
                first_name: entities.decode(customer_details[0].first_name),
                last_name: entities.decode(customer_details[0].last_name),
                task_ref: db_task_details[0].task_ref,
                product_name: db_task_details[0].product_name,
                country_name: mail_country_name,
                format_due_date: format_due_date,
                task_content: entities.decode(db_task_details[0].content),
                company_name: entities.decode(customer_details[0].company_name),
                req_name: db_task_details[0].req_name,
                quantity: db_task_details[0].quantity,
                ccpurl: Config.ccpUrl,
                emp_first_name: emp_details[0].first_name,
                pharmacopoeia: db_task_details[0].pharmacopoeia,
              };
              const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_ORDER_GENERAL_REQUEST', emp_details[0].email, Config.webmasterMail, '', empMailParam14, "e");
            }
          }


          if (posted_files && posted_files.length > 0) {

            for (var j = 0; j < posted_files.length; j++) {

              let file_details = {
                task_id: ccp_task_id,
                actual_file_name: posted_files[j].originalname,
                new_file_name: posted_files[j].filename,
                date_added: today
              }

              await taskModel.add_task_file(file_details);
            }
          }

          if (prev_files && prev_files.length > 0) {

            for (var j = 0; j < prev_files.length; j++) {

              let file_details = {
                task_id: ccp_task_id,
                actual_file_name: prev_files[j].actual_file_name,
                new_file_name: prev_files[j].new_file_name,
                date_added: today
              }

              await taskModel.add_task_file(file_details);
            }
          }

          //SALES FORCE TASK
          if (Config.activate_sales_force == true) {
            await module.exports.__post_sales_force_add_task(task_id, req.user.empe);
            if (comment_arr != '') {
              await module.exports.__post_sales_force_internal_discussions(task_id, [], comment_arr.comment, comment_arr.posted_by);
            }
          }

          res.status(200).json({
            status: '1',
            message: 'Task Added Successfully.'
          });

        } else {
          //Unallocated Task Soumyadeep
          var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${task_details.customer_id}`);
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: "Error Adding Task: Customer Has No Allocated BM"
          });
        }
      } else {
        var err = new Error(`Task ID is blank Customer ID: ${task_details.customer_id}`);
        common.logError(err);
        res.status(400).json({
          status: 3,
          message: "Error while adding task"
        });
      }
    }
    catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  download_internal_discussions_all: async (req, res, next) => {
    try {
      const internal_discussion_details_arr = hashids.decode(req.value.params.hash_id);
      console.log(internal_discussion_details_arr);
      if (internal_discussion_details_arr && internal_discussion_details_arr.length > 0 && internal_discussion_details_arr[0] > 0) {
        const comment_id = internal_discussion_details_arr[0];

        let task_details = await taskModel.get_task_internal_discussion_details(comment_id);
        //console.log(task_details);
        let file_name = `${task_details[0].task_ref}_${task_details[0].req_name.split(' ').join('_').split('/').join('_')}_internal_discussion.zip`;
        let file_path = `${Config.upload.internal_discussion_compressed}${file_name}`;

        if (fs.existsSync(file_path)) {
          //DOWNLOAD EXISTING FILE
          console.log('exists');
          //res.download(file_path);
          fs.unlinkSync(file_path);
        }
        let filesArr = [];
        let archiveFiles = [];
        let taskDetailsFiles = await taskModel.select_comment_specific_files(comment_id);
        let folder = '';
        console.log(taskDetailsFiles);
        for (let index = 0; index < taskDetailsFiles.length; index++) {
          const element = taskDetailsFiles[index];
          filesArr.push(element.new_file_name);
          archiveFiles.push({
            name: element.actual_file_name
          });
        }
        console.log(archiveFiles);
        const AWS = require('aws-sdk');
        AWS.config.update({
          accessKeyId: Config.aws.accessKeyId,
          secretAccessKey: Config.aws.secretAccessKey,
          region: Config.aws.regionName
        });

        const output = fs.createWriteStream(join(Config.upload.internal_discussion_compressed, file_name));
        let stream = s3Zip.archive({
          region: Config.aws.regionName,
          bucket: Config.aws.bucketName
        }, folder, filesArr, archiveFiles).pipe(output);

        stream.on('finish', () => {
          res.download(file_path);
        });

      } else {
        let err = new Error('Invalid access');
        common.logError(err);
        res
          .status(400)
          .json({
            status: 3,
            message: 'Invalid access'
          })
          .end();
      }

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },

  approve_invoice: async (req, res, next) => {
    try {
      let customer_id = 0;
      let submitted_by = 0;
      if (req.user.role == 2) {
        submitted_by = req.user.customer_id;
        customer_id = req.body.agent_customer_id;
      } else {
        customer_id = req.user.customer_id;
      }
      let today = common.currentDateTime();
      const {
        task_id,
        type,
        invoice_number,
        reject_comment
      } = req.body;

      if (invoice_number != '' && invoice_number != undefined) {
        await taskModel.is_invoice_exist(invoice_number, task_id).then(async function (tdata) {
          if (tdata.sales_order_no != '') {
            let invoice_detail_info = [];
            if (type == 'approve') {
              await taskModel.updateShipInvoice(invoice_number, 1, customer_id, today);
              invoice_detail_info = await taskModel.getShipInvoiceById(tdata.invoice_id, 1);
            } else {
              await taskModel.updateShipInvoice(invoice_number, 2, customer_id, today, entities.encode(reject_comment));
              invoice_detail_info = await taskModel.getShipInvoiceById(tdata.invoice_id, 2);
            }

            if (invoice_detail_info.length > 0) {
              let emp_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 5);
              let bm_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 1);
              if (emp_details.length > 0) {

                let attachmentArr = [];
                // Mail attached of invoice file.
                if (invoice_detail_info[0].invoice_file_path != '' && invoice_detail_info[0].invoice_file_name != '') {
                  let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].invoice_file_path, invoice_detail_info[0].invoice_file_name);

                  if (base_64_stream.status == 1) {
                    attachmentArr.push({
                      filename: invoice_detail_info[0].invoice_file_name,
                      content: base_64_stream.base64,
                      encoding: "base64"
                    });
                  }
                }

                // Mail attached of package file.   
                if (invoice_detail_info[0].packing_file_path != '' && invoice_detail_info[0].packing_file_name != '') {
                  let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].packing_file_path, invoice_detail_info[0].packing_file_name);

                  if (base_64_stream.status == 1) {
                    attachmentArr.push({
                      filename: invoice_detail_info[0].packing_file_name,
                      content: base_64_stream.base64,
                      encoding: "base64"
                    });
                  }
                }

                let mail_cc = '';
                if (bm_details.length > 0) {
                  mail_cc = bm_details[0].email;
                }
                if (Config.environment != 'production') {
                  mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                }

                let mailToParam = {
                  task_ref: invoice_detail_info[0].task_ref,
                  emp_first_name: entities.decode(emp_details[0].first_name),
                  emp_last_name: entities.decode(emp_details[0].last_name),
                  first_name: entities.decode(invoice_detail_info[0].first_name),
                  last_name: entities.decode(invoice_detail_info[0].last_name),
                  company_name: entities.decode(invoice_detail_info[0].company_name),
                  product_name: entities.decode(invoice_detail_info[0].product_name),
                  rdd: (invoice_detail_info[0].rdd != null && invoice_detail_info[0].rdd != '') ? common.changeDateFormat(invoice_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                  po_number: invoice_detail_info[0].po_no,
                  sales_order_no: invoice_detail_info[0].sales_order_no,
                  status: (type == 'approve') ? 'accepted' : 'rejected',
                  comment: (type != 'approve') ? '<tr><td><strong>Comment</strong></td><td>' + common.nl2br(reject_comment) + '</td></tr>' : null
                };

                const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PROFORMA_INVOICE', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

              }
            }

            preShipmentInfo = await taskModel.get_preship_invoice(tdata.sales_order_no);
            if (preShipmentInfo.length > 0 && req.user.role == 1) {
              preShipmentTab = true;
              for (let indps = 0; indps < preShipmentInfo.length; indps++) {
                const element = preShipmentInfo[indps];

                preShipmentInfo[indps]['file_hash'] = cryptr.encrypt(element.invoice_number + "#" + task_id);
                /*let preShipmentFilesCoa = await taskModel.select_task_files_coa_by_invoice(element.invoice_number);
                for (var i = 0; i < preShipmentFilesCoa.length; i++) {
                  if (process.env.NODE_ENV == 'development') {
                    preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + preShipmentFilesCoa[i]['coa_file_name'];
                  } else {
                    preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(preShipmentFilesCoa[i]['invoice_number'] + '#' + task_id + '#' + preShipmentFilesCoa[i]['coa_id']);
                  }
                  preShipmentFilesCoa[i]['coa_id'] = cryptr.encrypt(preShipmentFilesCoa[i]['coa_id'] + "#" + preShipmentFilesCoa[i]['invoice_number']);
                }
                preShipmentInfo[indps]['coa_files'] = preShipmentFilesCoa;*/
                preShipmentInfo[indps]['coa_files'] = [];
              }
            }

            res.status(200).json({
              status: '1',
              message: 'invoice approved successfully.',
              data: preShipmentInfo
            });
          } else {
            res.status(400)
              .json({
                status: 3,
                message: "No invoice found"
              }).end();
          }

        })
          .catch(err => {
            common.logError(err);
            res.status(400)
              .json({
                status: 3,
                message: Config.errorText.value
              }).end();
          });
      } else {
        res.status(400)
          .json({
            status: 3,
            message: "You are entering wrong Invoice number"
          }).end();
      }

    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },

  approve_coa: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.body.coa).split('#');
      let coa_id = 0;
      let invoice_id = 0;
      let customer_id = 0;
      let submitted_by = 0;
      let today = common.currentDateTime();

      const {
        task_id,
        type,
        reject_comment
      } = req.body;

      if (key.length == 2) {
        coa_id = key[0];
        invoice_id = key[1];
      }
      if (req.user.role == 2) {
        submitted_by = req.user.customer_id;
        customer_id = req.body.agent_customer_id;
      } else {
        customer_id = req.user.customer_id;
      }

      if (coa_id) {
        await taskModel.is_coa_exist(invoice_id, task_id, coa_id)
          .then(async function (tdata) {
            if (tdata.length > 0) {
              let coa_detail_info = [];
              if (type == 'approve') {
                await taskModel.updateShipCOA(coa_id, 1, customer_id, today);
                coa_detail_info = await taskModel.getShipCOAById(coa_id, 1);
              } else {
                await taskModel.updateShipCOA(coa_id, 2, customer_id, today, entities.encode(reject_comment));
                coa_detail_info = await taskModel.getShipCOAById(coa_id, 2);
              }

              if (coa_detail_info.length > 0) {
                let emp_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 5);
                let bm_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 1);
                if (emp_details.length > 0) {

                  let attachmentArr = [];
                  // Mail attached of COA file.
                  if (coa_detail_info[0].coa_file_path != '' && coa_detail_info[0].coa_file != '') {
                    let base_64_stream = await module.exports.__get_base64(coa_detail_info[0].coa_file_path, coa_detail_info[0].coa_file);

                    if (base_64_stream.status == 1) {
                      attachmentArr.push({
                        filename: coa_detail_info[0].coa_file,
                        content: base_64_stream.base64,
                        encoding: "base64"
                      });
                    }
                  }

                  let mail_cc = '';
                  if (bm_details.length > 0) {
                    mail_cc = bm_details[0].email;
                  }
                  if (Config.environment != 'production') {
                    mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                  }

                  let mailToParam = {
                    task_ref: coa_detail_info[0].task_ref,
                    emp_first_name: entities.decode(emp_details[0].first_name),
                    emp_last_name: entities.decode(emp_details[0].last_name),
                    first_name: entities.decode(coa_detail_info[0].first_name),
                    last_name: entities.decode(coa_detail_info[0].last_name),
                    company_name: entities.decode(coa_detail_info[0].company_name),
                    product_name: entities.decode(coa_detail_info[0].product_name),
                    rdd: (coa_detail_info[0].rdd != null && coa_detail_info[0].rdd != '') ? common.changeDateFormat(coa_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                    invoice_number: coa_detail_info[0].invoice_number,
                    po_number: coa_detail_info[0].po_no,
                    sales_order_no: coa_detail_info[0].sales_order_number,
                    status: (type == 'approve') ? 'accepted' : 'rejected',
                    comment: (type != 'approve') ? '<tr><td><strong>Comment</strong></td><td>' + common.nl2br(reject_comment) + '</td></tr>' : null
                  };

                  const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PRESHIPMENT_COA', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

                }
              }

              preShipmentInfo = await taskModel.get_preship_invoice(tdata[0].sales_order_no);
              if (preShipmentInfo.length > 0 && req.user.role == 1) {
                preShipmentTab = true;
                for (let indps = 0; indps < preShipmentInfo.length; indps++) {
                  const element = preShipmentInfo[indps];

                  preShipmentInfo[indps]['file_hash'] = cryptr.encrypt(element.invoice_number + "#" + task_id);
                  /*let preShipmentFilesCoa = await taskModel.select_task_files_coa_by_invoice(element.invoice_number);
                  for (var i = 0; i < preShipmentFilesCoa.length; i++) {
                    if (process.env.NODE_ENV == 'development') {
                      preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + preShipmentFilesCoa[i]['coa_file_name'];
                    } else {
                      preShipmentFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(preShipmentFilesCoa[i]['invoice_number'] + '#' + task_id + '#' + preShipmentFilesCoa[i]['coa_id']);
                    }
                    preShipmentFilesCoa[i]['coa_id'] = cryptr.encrypt(preShipmentFilesCoa[i]['coa_id'] + "#" + preShipmentFilesCoa[i]['invoice_number']);
                  }
                  preShipmentInfo[indps]['coa_files'] = preShipmentFilesCoa;*/
                  preShipmentInfo[indps]['coa_files'] = [];
                }
              }

              res.status(200).json({
                status: '1',
                message: 'COA approved successfully.',
                data: preShipmentInfo
              });
            } else {
              res.status(400)
                .json({
                  status: 3,
                  message: "No COA document found"
                }).end();
            }
          })
          .catch(err => {
            common.logError(err);
            res.status(400)
              .json({
                status: 3,
                message: Config.errorText.value
              }).end();
          });
      } else {
        res.status(400)
          .json({
            status: 3,
            message: "You are entering wrong COA document"
          }).end();
      }
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
}