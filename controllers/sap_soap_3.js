
const fs = require('fs');
const Config     = require('../configuration/config');
const sapModel   = require("../models/sap");
const taskModel  = require('../models/task');
const common     = require('./common');
var auth         = require('basic-auth');

var compare      = require('tsscmp');
var ip = require('ip');
const AWS     = require('aws-sdk');
const uuidv4  = require('uuid/v4');
var path      = require('path');
var request   = require('request').defaults({ encoding: null });
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


const s3Config = new AWS.S3({
  accessKeyId     : Config.aws.accessKeyId,
  secretAccessKey : Config.aws.secretAccessKey

});




module.exports = {

    MyService: {
        MyPort: {
            UpdateCoa:async function(args, cb, headers, req) {
              console.log("hello i am in Coa !!",args)
                
              
            //   var credentials = auth(req);
            //   var valid = true;
              
            //   if (credentials) {
                 
            //     valid = compare(credentials.name, Config.sapAuth.username) && valid
            //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
            //     if(!valid){
            //         throw {
            //             Fault: {
            //               Code: {
            //                 Value: '401'
            //               },
            //               Reason: { Text: 'Authentication failed' },
            //               statusCode: 401
            //             }
            //           };
            //     }                
            //   }else{
            //     throw {
            //             Fault: {
            //               Code: {
            //                 Value: '401'
            //               },
            //               Reason: { Text: 'Authentication failed' },
            //               statusCode: 401
            //             }
            //           };
            //   } 
                
               // console.log('SOAP `reallyDetailedFunction` request from ' + req.connection.remoteAddress);
                let insLogObj = {
                    request : JSON.stringify(args),
                    response : '',
                    method : 'POST',
                    url : '',
                    datetime : common.currentDateTime(),
                    request_type : 'COA_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj); 
                let all_request = args;
                let error = '';
               
                if(all_request.customer_name == '' || all_request.batch_number == '' || all_request.lot_no == '' || all_request.file_path == '' || all_request.customer_name == null || all_request.batch_number == null || all_request.lot_no == null || all_request.file_path == null || all_request.customer_name == '?' || all_request.batch_number == '?' || all_request.lot_no == '?' || all_request.file_path == '?'){
                      // is null
                           
                      if(logResponse){
                        error = new Error('null value is not accepted .');
                        common.logError(error);
                          sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "null value"}');// add reason
                      }
                      return {
                          status : false,
                          message :  "null value is not accepted ."
                      };

                }else{
                    
                 // const get_coa = await sapModel.get_coa(all_request);
                  
                //  if(get_coa.success == false ){

                    // if (all_request != null) { 
                      
                       let file_name =`${all_request.file_path}`;
                       let new_file_name = '';

                        fs.readFile(`${Config.sap_coa.sftp_path}${file_name}`, async (err, buffer)=>{
                            //console.log("this is buffer : ",buffer);
                            if(err){
                                common.logError(err);
                                    res.status(400).json({
                                      status: 3,
                                      message: Config.errorText.value
                                    }).end();

                            }else{
                                try{
                                    if(buffer == '' || buffer == undefined){
                                        common.logError(new Error('file does not exists.'));
                                        //console.log("this is logResponse : ",logResponse);
                                            if(logResponse){
                                                await sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "file does not exists"}');
                                                
                                            };
                                            return {
                                            status: false,
                                            message : "file does not exists"
                                            }
                                                            
                                    }else{
                                        let extention     = path.extname(file_name);
                                        new_file_name = 'qa_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
                                        //console.log(new_file_name);
                                        let params = {
                                            Bucket: Config.aws.bucketName,
                                            Key: new_file_name, 
                                            Body: buffer
                                        };
        
                                        s3Config.putObject(params, function(err, data){
                                            if(err){
                                                common.logError(err);
                                                if(logResponse){
                                                    sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "s3 bucket file path error"}');
                                                }
                                                return {
                                                    status : false
                                                };
                                            }
                                        });
                                        all_request.customer_name = entities.encode(all_request.customer_name);
                                        all_request.batch_number = entities.encode(all_request.batch_number);
                                        all_request.lot_no = entities.encode(all_request.lot_no);
                                        all_request.file_path = entities.encode(all_request.file_path); 
                                        all_request.date_added = common.currentDateTime();
                                        all_request.ip = ip.address();
                                        all_request.coa_file_path = entities.encode(new_file_name); 
                                        all_request.material_id = entities.encode(all_request.material_id)
                                        const add_coa = sapModel.add_coa(all_request);
                                        if(add_coa.coa_id > 0){
                                           sapModel.updateRequestLog(logResponse.id,'{"status" : true}','');
                                           return {
                                               status : true,
                                               message :  "data added successfully"
                   
                                               }; 
                                        }
                                        
                                    } 
                                }catch(err){
                                    common.logError(err);
                                    return {
                                      status: 3,
                                      message: Config.errorText.value
                                    };
                                }
                            }
                            });
                                 return {
                                status : true,
                                message :  "data chenged successfully"
                            };
            
                        
                      
                           
                        // }else{
                        //     error = new Error('plese enter proper data .');
                        //     common.logError(error);
                        //     if(logResponse){
                        //         sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "enter proper data"}');
                        //     }
                        //     return {
                        //         status : false,
                        //         message :  "plese enter proper data ."
                        //     };
            
                        // }

                // }else{
                //     error = new Error('this relation already exists');
                //     common.logError(error);
                      
                //     if(logResponse){
                //         sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "relation exists"}');
                //     }
                //     return {
                //         status : false,
                //         message :  "this relation already exists"
                //     };
    
                // }
              }
            }
        }
    }
}
           
        
    
