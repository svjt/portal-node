
const request = require("request");
const Config = require('../configuration/config');
const sapModel = require("../models/sap");
const taskModel = require('../models/task');
const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp')


module.exports = {

    MyService: {
        MyPort: {
            UpdateTask: async function (args, cb, headers, req) {
                console.log('hello!', args['Reservation_Update']);

                var credentials = auth(req);
                var valid = true;

                if (credentials) {

                    valid = compare(credentials.name, Config.sapAuth.username) && valid
                    valid = compare(credentials.pass, Config.sapAuth.password) && valid
                    if (!valid) {
                        throw {
                            Fault: {
                                Code: {
                                    Value: '401'
                                },
                                Reason: { Text: 'Authentication failed' },
                                statusCode: 401
                            }
                        };
                    }
                } else {
                    throw {
                        Fault: {
                            Code: {
                                Value: '401'
                            },
                            Reason: { Text: 'Authentication failed' },
                            statusCode: 401
                        }
                    };
                }

                //console.log('SOAP `reallyDetailedFunction` request from ' + req.connection.remoteAddress);
                let insLogObj = {
                    request: JSON.stringify(args),
                    response: '',
                    method: 'POST',
                    url: '',
                    datetime: common.currentDateTime(),
                    request_type: 'STOCK_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);
                let all_request = args['Reservation_Update'];
                if (all_request != null) {
                    //console.log(all_request.length);
                    var error = 0;
                    for (let index = 0; index <= (all_request.length - 1); index++) {
                        //console.log("For index : ",all_request[index]);
                        if (all_request[index]['STATUS'].toLowerCase() == 'success') {
                            await sapModel.getParticularRequest(all_request[index]['Main_Req'], all_request[index]['Reservation_Request_No'], all_request[index]['SO_ID'])
                                .then(async function (data) {
                                    // console.log("Task details : ",data );
                                    if (data) {
                                        var update_date = all_request[index]['Date'].slice(0, 4) + "-" + all_request[index]['Date'].slice(4, 6) + "-" + all_request[index]['Date'].slice(6, 8);
                                        var update_date_time = update_date + " " + all_request[index]['Time'];
                                        if (all_request[index]['Req_Type'].toLowerCase() == 'cancel') {
                                            console.log("Task Id To Cancel : ", data.task_id);
                                            await sapModel.cancelTask(data.task_id, all_request[index]['Mode'], update_date_time);
                                        }
                                        if (all_request[index]['Req_Type'].toLowerCase() == 'update') {
                                            console.log("Task Id To SO NO Update : ", data.task_id);
                                            await sapModel.isExistSONumber(data.task_id)
                                                .then(async function (data_exist) {
                                                    if (data_exist[0].sales_order_no == '' || data_exist[0].sales_order_no == 0) {
                                                        await sapModel.updateSONumber(data.task_id, all_request[index]['SAP_SO_Number'], update_date_time);
                                                    } else {
                                                        const toArr = [
                                                            "soumyadeep@indusnet.co.in",
                                                            "debadrita.ghosh@indusnet.co.in"
                                                        ];
                                                        var mailOptions2 = {
                                                            from: Config.webmasterMail, // sender address
                                                            to: toArr, // list of receivers
                                                            subject: `SAP Update WSDL error`, // Subject line
                                                            html: `SO No. not being updated for task ${data.task_id} as exist SO No. is ${data_exist[0].sales_order_no}. Post SAP update data is : ${JSON.stringify({ all_request })}}` // plain text body
                                                        };
                                                        await common.sendMail(mailOptions2);
                                                    }
                                                }).catch((err) => {
                                                    error++;
                                                    common.logError(err);
                                                });

                                        }
                                    } else {
                                        error++;
                                        //common.logError('Not found');
                                    }
                                }).catch((err) => {
                                    error++;
                                    common.logError(err);
                                });
                        } else {
                            error++;
                        }
                    }

                    if (error == all_request.length) {   //it may be error > 0 
                        var updateRequestLog = sapModel.updateRequestLog(logResponse.id, '{"status" : false}', '{"reason" : "error occers"}');
                        return {
                            status: false
                        };
                    } else {
                        var updateRequestLog = sapModel.updateRequestLog(logResponse.id, '{"status" : true}', '');
                        return {
                            status: true
                        };
                    }
                } else {
                    if (logResponse) {
                        var updateRequestLog = sapModel.updateRequestLog(logResponse.id, '{"status" : false}', '{"reason" : "error occers"}');
                    }
                    console.log("FALSE");
                    return {
                        status: false
                    };
                }
            }

        }

    }
}
