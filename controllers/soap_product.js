const Config = require('../configuration/config');
const sapModel = require("../models/soap");

const common = require('./common');
var auth = require('basic-auth');
var compare = require('tsscmp');


const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();



module.exports = {

    MyService: {
        MyPort: {

            AddUpdateMaterial: async function(args, cb, headers, req) {
                // console.log("hello i am in products Add !!",args['Material_Details']);
                // var credentials = auth(req);
                // var valid = true;

                // if (credentials) {

                //     valid = compare(credentials.name, Config.sapAuth.username) && valid
                //     valid = compare(credentials.pass, Config.sapAuth.password) && valid
                //     if (!valid) {
                //         throw {
                //             Fault: {
                //                 Code: {
                //                     Value: '401'
                //                 },
                //                 Reason: {
                //                     Text: 'Authentication failed'
                //                 },
                //                 statusCode: 401
                //             }
                //         };
                //     }
                // } else {
                //     throw {
                //         Fault: {
                //             Code: {
                //                 Value: '401'
                //             },
                //             Reason: {
                //                 Text: 'Authentication failed'
                //             },
                //             statusCode: 401
                //         }
                //     };
                // }


                let insLogObj = {
                    request: JSON.stringify(args),
                    response: '',
                    method: 'POST',
                    url: '',
                    datetime: common.currentDateTime(),
                    request_type: 'PRODUCT_ADD_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);

                let all_request = args['Material_Details'];

                let error   = 0;
                let err_msg = '';
                reason      = '';
                let success_message   = '';
                let material_code_add = [];
                let material_code_update = [];
                let material_code_delete = [];
                let dampped_index     = [];

                if (all_request != null) {

                    for (let index = 0; index <= (all_request.length - 1); index++) {

                        if ( all_request[index]['material_code'] == '' || 
                             all_request[index]['material_description'] == '' || 
                             all_request[index]['sorg'] == '' || 
                             all_request[index]['dchl'] == '' || 
                             all_request[index]['div']  == '' || 
                             all_request[index]['mtyp'] == '' || 
                             all_request[index]['bun']  == '' || 
                             all_request[index]['plant'] == '' || 
                             all_request[index]['material_code'] == '?' || 
                             all_request[index]['material_description'] == '?' || 
                             all_request[index]['sorg'] == '?' || 
                             all_request[index]['dchl'] == '?' || 
                             all_request[index]['div'] == '?'  || 
                             all_request[index]['mtyp'] == '?' || 
                             all_request[index]['bun'] == '?' || 
                             all_request[index]['plant'] == '?' || 
                             all_request[index]['material_code'] == null || 
                             all_request[index]['material_description'] == null || 
                             all_request[index]['sorg'] == null || 
                             all_request[index]['dchl'] == null || 
                             all_request[index]['div'] == null  || 
                             all_request[index]['mtyp'] == null || 
                             all_request[index]['bun'] == null  || 
                             all_request[index]['plant'] == null ||
                             all_request[index]['x_plntmat_status'] == '?' ||
                             all_request[index]['x_distchain_status'] == '?' ||
                             all_request[index]['distchainspecstatus'] == '?' 

                             ) {

                             
                              error++;
                              err_msg = 'Something went wrong, please try again with proper data';
                              dampped_index.push(`All required fields are absent or null for index ${index + 1}`);
                              reason = `{"reason" : '${dampped_index}'}`;

                        } else {

                            if( isNaN(all_request[index]['sorg']) || isNaN(all_request[index]['dchl']) || isNaN(all_request[index]['div']) || isNaN(all_request[index]['material_code']) ){
                                error++;
                                err_msg = 'Non numeric values are not accepted';
                                dampped_index.push(`Non numeric values are not accepted for index ${index + 1}`);
                                reason = `{"reason" : '${dampped_index}'}`;

                            }else{

                                if (all_request[index]['div'] == 30 && all_request[index]['mtyp'] == 'API') {
                                     
                                      

                                    if((all_request[index]['created_date'] != undefined && all_request[index]['created_date'] != '' && isNaN(all_request[index]['created_date'])) || (all_request[index]['last_chg'] != undefined && all_request[index]['last_chg'] != '' && isNaN(all_request[index]['last_chg']))){
                                       
                                        
                                        error++;
                                        err_msg = 'Please enter proper creation date or last change date';
                                        dampped_index.push(`Please enter proper creation date or last change date for index ${index + 1}`);
                                        reason = `{"reason" : '${dampped_index}'}`;
                                    }else{
                                        if((all_request[index]['created_date'] != undefined && all_request[index]['created_date'].length != 8) || (all_request[index]['last_chg'] != undefined && all_request[index]['last_chg'].length != 8)){
                                             
                                                error++;
                                            err_msg = 'Please enter proper creation date or last change date';
                                            dampped_index.push(`Please enter proper creation date or last change date for index ${index + 1}`);
                                            reason = `{"reason" : '${dampped_index}'}`;

                                        }else{
                                            const scarchObj = {
                                                material_code: entities.encode(all_request[index]['material_code'].replace(/^0+/, '')),
                                                sorg: entities.encode(all_request[index]['sorg'].replace(/^0+/, '')), 
                                                dchl: entities.encode(all_request[index]['dchl'].replace(/^0+/, '')) 
                                            }
            
                                            const dataObj = {
            
                                                material_code: entities.encode(all_request[index]['material_code'].replace(/^0+/, '')),
                                                material_description: all_request[index]['material_description'] ? entities.encode(all_request[index]['material_description']) : '',
                                                sorg: entities.encode(all_request[index]['sorg'].replace(/^0+/, '')), 
                                                sorg_desc: all_request[index]['sorg_desc'] ? entities.encode(all_request[index]['sorg_desc']) : '',
                                                dchl: entities.encode(all_request[index]['dchl'].replace(/^0+/, '')), 
                                                dchl_desc: all_request[index]['dchl_desc'] ? entities.encode(all_request[index]['dchl_desc']) : '',
                                                div: all_request[index]['div'],
                                                div_desc: all_request[index]['div_desc'] ? entities.encode(all_request[index]['div_desc']) : '',
                                                x_plntmat_status: all_request[index]['x_plntmat_status'] ? entities.encode(all_request[index]['x_plntmat_status']) : '',
                                                x_distchain_status: all_request[index]['x_distchain_status'] ? entities.encode(all_request[index]['x_distchain_status']) : '',
                                                distchainspecstatus: all_request[index]['distchainspecstatus'] ? entities.encode(all_request[index]['distchainspecstatus']) : '',
                                                mtyp: all_request[index]['mtyp'] ? entities.encode(all_request[index]['mtyp']) : '',
                                                bun: all_request[index]['bun'] ? entities.encode(all_request[index]['bun']) : '',
                                                plant: all_request[index]['plant'] ? entities.encode(all_request[index]['plant'].replace(/^0+/, '')) : '',
                                                date_added: common.currentDateTime(),
                                                date_updated: common.currentDateTime()
                                            }
                                            const get_relation = await sapModel.get_old_product(scarchObj);
            
                                            if (get_relation[0].id == 0 || get_relation[0].id == undefined) {
                                                dataObj.created_date = all_request[index]['created_date'] ? entities.encode(all_request[index]['created_date'].slice(0, 4) + "-" + all_request[index]['created_date'].slice(4, 6) + "-" + all_request[index]['created_date'].slice(6, 8)) : '';
                                                dataObj.created_by = all_request[index]['created_by'] ? entities.encode(all_request[index]['created_by']) : '';
                                                dataObj.last_chg = all_request[index]['last_chg'] ? entities.encode(all_request[index]['last_chg'].slice(0, 4) + "-" + all_request[index]['last_chg'].slice(4, 6) + "-" + all_request[index]['last_chg'].slice(6, 8)) : '';
                                                dataObj.changed_by = all_request[index]['changed_by'] ? entities.encode(all_request[index]['changed_by']) : '';
                                                dataObj.status = 1;
                                                let prod_added = await sapModel.add_product(dataObj);
                                                if (prod_added.id > 0) {
            
                                                    material_code_add.push(all_request[index]['material_code'].replace(/^0+/, ''));
                                                    if (dataObj.x_plntmat_status.toLowerCase()  != '' || dataObj.x_distchain_status.toLowerCase()  != '' || dataObj.distchainspecstatus.toLowerCase()  != '') {
                                                        material_code_delete.push(dataObj.material_code.replace(/^0+/, ''));
                                                        await sapModel.update_product_status(dataObj.material_code.replace(/^0+/, ''), common.currentDateTime())
                                                     }
                                                    success_message = 'Data changed successfully';
            
                                                } else {
                                                    error++;
                                                    err_msg = 'Error occurred in addition of product';
                                                    dampped_index.push(`Error occured in addition of product for index ${index + 1}`);
                                                    reason = `{"reason" : '${dampped_index}'}`;
                                                }
            
                                            } else {
            
                                                
                                                let update_product = await sapModel.update_product(dataObj, get_relation[0].id);
                                                
                                                if (update_product.id > 0) {
                                                    let get_data = await sapModel.get_compare_data(update_product.id);
                                                     if (get_data.length > 0) {
                                                        if (get_data[0].x_plntmat_status.toLowerCase()  != '' || get_data[0].x_distchain_status.toLowerCase()  != '' || get_data[0].distchainspecstatus.toLowerCase()  != '') {
                                                            material_code_delete.push(get_data[0].material_code.replace(/^0+/, ''));
                                                            await sapModel.update_product_status(dataObj.material_code.replace(/^0+/, ''), common.currentDateTime())
            
                                                        } else {
                                                            material_code_update.push(get_data[0].material_code.replace(/^0+/, ''));
                                                        }
                                                    }
                                                    if(all_request[index]['last_chg'] != '' || all_request[index]['changed_by'] != '' ){
                                                       last_chg = all_request[index]['last_chg'] ? entities.encode(all_request[index]['last_chg'].slice(0, 4) + "-" + all_request[index]['last_chg'].slice(4, 6) + "-" + all_request[index]['last_chg'].slice(6, 8)) : '';
                                                        changed_by = all_request[index]['changed_by'] ? entities.encode(all_request[index]['changed_by']) : '';
                                                       await sapModel.update_chenge_data(last_chg,changed_by,get_relation[0].id); 
                                                    }
            
                                                    success_message = 'Data changed successfully';
                                                } else {
                                                    error++;
                                                    err_msg = 'Error occurred in update';
                                                    dampped_index.push(`Error occurred in update for index ${index + 1}`);
                                                    reason = `{"reason" : '${dampped_index}'}`;
                                                }
            
                                            }
                                             
                                        }

                                       

                                    }
                                    
                                    
                                } else {
                                    error++;
                                    err_msg = 'Please enter proper data in div or mtyp';
                                    dampped_index.push(`Plese enter proper data in div or mtyp for index ${index + 1}`);
                                    reason = `{"reason" : '${dampped_index}'}`;
                                }

                            }   
                        }
                    }
                    let mailOptions = {
                        to: Config.sendTo,
                        from: Config.contactUsMail,
                        subject: "Product activity. ",
                        html: `
                        <p>Following updates have been made to the SAP Product table</p>
                        <p> Added material codes   : ${material_code_add}</p>
                        <p> Updated material codes : ${material_code_update} </p>
                        <p> Deleted material codes : ${material_code_delete}</p>
                        <p> Time : ${common.currentDateTime()}</p>
                        <p> From , team XCEED</p>`
                    }
                    if(material_code_add.length>0 || material_code_update.length>0 || material_code_delete.length>0){
                        common.sendMailB2C(mailOptions);
                    }
                  


                } else {

                    error++;
                    err_msg = 'Can not accept null value.';
                    dampped_index.push(`Can not accept null value for index ${index + 1}`);
                    reason = `{"reason" : '${dampped_index}'}`;

                }
                if (error > 0) {

                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : false}', reason);
                    }
                    common.logError(new Error(err_msg))
                    return {
                        status: false,
                        message: err_msg
                    };

                } else {

                    if (logResponse) {
                        sapModel.updateRequestLog(logResponse.id, '{"status" : true}', reason);
                    }
                    return {
                        status: true,
                        message: success_message
                    };
                }

            }

        }
    }
}