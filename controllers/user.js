const JWT = require('jsonwebtoken');
//require('dotenv').config();
const jwtSimple = require('jwt-simple');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const moment = require('moment');
const bcrypt = require('bcryptjs');
const AWS = require('aws-sdk');
const fs = require("fs");
var path = require("path");
const outwardCrm = require('./outward_crm');
const userModel = require('../models/user');
const taskModel = require('../models/task');
const customerModel = require('../models/customer');
const agentModel = require('../models/agents');
const sapModel = require('../models/sap');
const employeeModel = require('../models/employees');
const salesForce = require('../controllers/sales_force');
const Config = require('../configuration/config');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(Config.cryptR.secret);
var Hashids = require('hashids');
var hashids = new Hashids('', 10);
const common = require('./common');
var trim = require('trim');
const globalLimit = 10;
const dateFormat = require('dateformat');
var ip = require('ip');


//var upload = multer({ storage: storage_client }).single('file')
matchPassword = async function (newPassword, existingPassword) {
  try {
    return await bcrypt.compare(newPassword, existingPassword);
  } catch (error) {
    // throw new Error(error);
    console.log({error})
    return false;
  }
}

loginToken = async user_data => {

  let feature_11_popup = await userModel.checkFeature11Popup(cryptr.decrypt(user_data.user_id), user_data.role);

  return JWT.sign({
    iss: 'Reddys',
    sub: user_data.user_id,
    empe: (user_data.empe_id && user_data.empe_id != '') ? user_data.empe_id : '',
    name: user_data.name,
    new_feature: user_data.new_feature,
    new_feature_send_mail: user_data.new_feature_send_mail,
    new_feature_fa: user_data.new_feature_fa,
    new_feature_11: (feature_11_popup) ? user_data.new_feature_11 : 2,
    tour_counter: user_data.tour_counter,
    tour_counter_new: user_data.tour_counter_new,
    tour_counter_closed_task: user_data.tour_counter_closed_task,
    tour_counter_11: (feature_11_popup) ? user_data.tour_counter_11 : 4,
    multi_lang_access: user_data.multi_lang_access,
    show_stock_enquiry: (user_data.show_stock_enquiry != undefined) ? user_data.show_stock_enquiry : 0,
    role_finance: (user_data.role_finance != undefined) ? user_data.role_finance : false,
    user: true,
    email: user_data.email,
    role: user_data.role,
    profile_pic: user_data.profile_pic,
    since: user_data.date_added,
    mobile: user_data.mobile,
    choose: user_data.choose,
    ag: user_data.user_agent,
    pre_activated: user_data.pre_activated != undefined ? user_data.pre_activated: '',
    lang: user_data.lang,
    iat: Math.round((new Date().getTime()) / 1000),
    exp: Math.round((new Date().getTime()) / 1000) + (12 * 60 * 60) //8 hours
  }, Config.jwt.secret);
}


module.exports = {
  /**
   * Generates a Authentication
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Authentication used for Authenticate
   */
  handle_auth: async (req, res, next) => {
    // Number.isInteger(req.user.customer_id) && 

    if (req.user.customer_id > 0) {
      if (req.get('User-Agent') == req.user.ag) {
        next();
      } else {
        let return_err = {
          status: 5,
          message: "Unauthorized"
        };
        return res.status(401).json(return_err);
      }

    } else {
      let return_err = {
        status: 5,
        message: "Unauthorized"
      };
      return res.status(401).json(return_err);
    }
  },
  handle_auth_qa: async (req, res, next) => {
    // Number.isInteger(req.user.customer_id) && 

    //console.log(req.user.user_id);

    if (req.user.user_id > 0) {

      if (req.get('User-Agent') == req.user.user_agent) {
        next();
      } else {
        let return_err = {
          status: 5,
          message: "Unauthorized"
        };
        return res.status(401).json(return_err);
      }

    } else {
      let return_err = {
        status: 5,
        message: "Unauthorized"
      };
      return res.status(401).json(return_err);
    }
  },

  update_feature: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_feature(req.user.customer_id);
    } else {
      await userModel.update_feature(req.user.customer_id);
    }

    return res.status(200)
      .json({
        status: 1
      })
  },

  update_feature_send_mail: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_feature_send_mail(req.user.customer_id);
    } else {
      await userModel.update_feature_send_mail(req.user.customer_id);
    }

    return res.status(200)
      .json({
        status: 1
      })
  },

  update_feature_11: async (req, res, next) => {
    if (req.user.role == 2) {
      await agentModel.update_feature_11(req.user.customer_id);
    } else {
      await userModel.update_feature_11(req.user.customer_id);
    }

    return res.status(200)
      .json({
        status: 1
      })
  },
  /**
   * Generates a Handle Login
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Login used for User login validation
   */
  handle_login: async (req, res, next) => {
    // Number.isInteger(req.user.customer_id) &&
    if (req.user.customer_id > 0) {
      next();
    } else {
      let err_data = {
        password: "Invalid login details"
      };
      return res.status(400).json({
        status: 2,
        errors: err_data
      });
    }

  },
  handle_login_qa: async (req, res, next) => {
    // Number.isInteger(req.user.customer_id) &&
    //console.log('req.user', req.user);
    if (req.user.id > 0) {
      next();
    } else {
      let err_data = {
        password: "Invalid login details"
      };
      return res.status(400).json({
        status: 2,
        errors: err_data
      });
    }

  },
  /**
   * Generates a Login
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Login used for customer or agent Login selection
   */
  login: async (req, res, next) => {
    //console.log('user controller',process.env.NODE_ENV);    
    try {
      // Number.isInteger(req.user.customer_id)
      if (req.user.customer_id > 0) {
        let profile_url = null;
        let choose = 0;
        if (req.user.profile_pic != null) {
          if (process.env.NODE_ENV == 'development') {
            profile_url = process.env.FULL_PATH + '/profile_pic/' + req.user.profile_pic;
          } else {
            s3_bucket_status = await module.exports.__get_signed_url(`${req.user.profile_pic}`);
            if (s3_bucket_status.status === 1)
              profile_url = s3_bucket_status.url;
            else
              profile_url = process.env.FULL_PATH + '/images/default-user.png';
          }

          choose = 0;
        } else {

          profile_url = process.env.FULL_PATH + '/images/default-user.png';
          choose = 1;
        }

        let user_role_finance = await userModel.checkUserRoleFinance(req.user.customer_id);

        let user_data = {
          user_id: cryptr.encrypt(req.user.customer_id),
          lang: req.user.language_code,
          user_agent: cryptr.encrypt(req.get('User-Agent')),
          name: entities.decode(req.user.first_name + " " + req.user.last_name),
          new_feature: req.user.new_feature,
          new_feature_send_mail: req.user.new_feature_send_mail,
          new_feature_fa: req.user.new_feature_fa,
          tour_counter: req.user.tour_counter,
          tour_counter_new: req.user.tour_counter_new,
          tour_counter_closed_task: req.user.tour_counter_closed_task,
          new_feature_11: req.user.new_feature_11,
          tour_counter_11: req.user.tour_counter_11,
          multi_lang_access: req.user.multi_lang_access,
          email: entities.decode(req.user.email),
          mobile: req.user.phone_no,
          date_added: req.user.date_added,
          role: req.user.role,
          profile_pic: profile_url,
          choose: choose,
          show_stock_enquiry: (req.user.show_stock_enquiry != undefined) ? req.user.show_stock_enquiry : 0,
          role_finance: user_role_finance,
          pre_activated: req.user.pre_activated,
        }

        const token = await loginToken(user_data);
        //console.log("token : ",token);
        let login_attempt = {
          customer_id: req.user.customer_id,
          login_date: common.currentDateTime(),
          ip_address: ip.address(),
          customer_type: (req.user.role == 2) ? 'A' : 'C'
        }
        await userModel.login_log(login_attempt);

        if (!req.user.role) {
          common.logError(err);
          return res.status(400).json({
            status: 3,
            message: Config.errorText.value
          })
        } else {
          let show_rating = false
          if (req.user.role == 1) {
            let data = await taskModel.get_all_pending_ratings_counts(req.user.customer_id);
            //console.log(data);
            if (data[0].count > 0) {
              show_rating = true;
            }
          }

          return res.status(200)
            .json({
              status: 1,
              token: token,
              show_rating: show_rating
            })
        }

      } else {
        let err_data = {
          password: "Invalid login details"
        };
        return res.status(400).json({
          status: 2,
          errors: err_data
        });
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  login_qa: async (req, res, next) => {

    try {
      if (req.user.id > 0) {

        let token = JWT.sign({
          iss: 'Reddys',
          sub: cryptr.encrypt(req.user.id),
          ag: cryptr.encrypt(req.get('User-Agent')),
          iat: Math.round((new Date().getTime()) / 1000),
          exp: Math.round((new Date().getTime()) / 1000) + (12 * 60 * 60)
        }, Config.jwt.secret);

        return res.status(200)
          .json({
            status: 1,
            token: token
          })

      } else {
        let err_data = {
          password: "Invalid login details"
        };
        return res.status(400).json({
          status: 2,
          errors: err_data
        });
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  qa_form: async (req, res, next) => {
    try {
      var today = common.currentDateTime();

      const {
        customer_name,
        batch_number,
        lot_no
      } = req.body;

      var files = req.files;

      //console.log(files);

      let data = {
        customer_name: entities.encode(customer_name),
        batch_number: entities.encode(batch_number),
        lot_no: entities.encode(lot_no),
        date_added: today,
        source: 2,
        ip: ip.address(),
        coa_file: files[0].originalname,
        coa_file_path: files[0].key
      };

      let coa_id = await userModel.insert_coa(data);

      res.status(200).json({
        status: 1,
        coa_id: coa_id
      }).end();

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Forgot Password
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Forgot Password used for customer or agent Password selection
   */
  forgotPassword: async (req, res, next) => {
    try {
      const {
        email
      } = req.body;
      const user = {
        email: entities.encode(email),

      }
      let userExist = await userModel.active_email_exists(user.email); // Check if user is a customer
      let agentExist = await agentModel.active_email_exists(user.email); // Check if user is an agent
      let err = {};
      if (agentExist.success) {
        await agentModel.findByEmail(user.email)
          .then(async function (data) {
            var payload = {
              time: new Date().getTime() + (72 * 60 * 60000),
              checksum: data[0].agent_id
            }
            var token = jwtSimple.encode(payload, Config.jwt.secret);
            var url = process.env.REACT_API_URL + `reset_password/` + token;
            const userData = await agentModel.findByUsername(user.email);
            await agentModel.update_token(data[0].agent_id, token);

            // Send reset pass mail with link

            // let mailOptions = {
            //   to: entities.decode(user.email),
            //   from: Config.contactUsMail,
            //   subject: "Finish resetting your Dr.Reddys API password",
            //   html: `<!DOCTYPE html><html>
            //        <body>
            //        <p>Dear ${entities.decode(userData[0].first_name)} ${entities.decode(userData[0].last_name)},</p>

            //         <p>Dr.Reddys API recently received your request for resetting your password for the username - ${entities.decode(user.email)}.</p>

            //         <p>To finish resetting your password, you may now click the below link or copying and pasting it into your browser:</p>

            //         <p><a href="${url}">Click Here</a></p>

            //         <p>This link is valid for 72 hours only and nothing will happen if it's not used.</p>

            //         <p>-- Dr.Reddys API team</p>

            //         <p>Interested in our products - please check our website - api.drreddys.com</p>
            //        </body></html>`
            //   // html : '<!DOCTYPE html><html><head><title>Forget Password Email</title></head><body><div><h3>Dear '+userData[0].first_name+',</h3><p>You requested for a password reset, kindly use this <a href="'+url+'">link</a> to reset your password</p><br><p>Cheers!</p></div></body></html>'
            // }

            // const mailSent = common.sendMailB2C(mailOptions);

            let mailParam = {
              first_name: entities.decode(userData[0].first_name),
              last_name: entities.decode(userData[0].last_name),
              email: entities.decode(user.email),
              url: url
            };
            const mailSent = common.sendMailByCodeB2C('FORGOT_PASSWORD_CUSTOMER', entities.decode(user.email), Config.contactUsMail, '', userData[0].language_code, mailParam, "c");
            if (mailSent) {
              let return_err = {
                'status': 1,
                'errors': "Email sent"
              };
              return res.status(200).json(return_err);
            } else {
              let return_err = {
                'status': 2,
                'errors': "Email has not been sent"
              };
              return res.status(400).json(return_err);
            }

          })
          .catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })


      } else if (userExist.success) {

        await userModel.findByEmail(user.email)
          .then(async function (data) {
            var payload = {
              time: new Date().getTime() + (72 * 60 * 60000),
              checksum: data[0].customer_id
            }
            var token = jwtSimple.encode(payload, Config.jwt.secret);
            var url = process.env.REACT_API_URL + `reset_password/` + token;
            const userData = await userModel.findByUsername(user.email);
            await userModel.update_token(data[0].customer_id, token);

            // let mailOptions = {
            //   to: entities.decode(user.email),
            //   from: Config.contactUsMail,
            //   subject: "Finish resetting your Dr.Reddys API password",
            //   html: `<!DOCTYPE html><html>
            //        <body>
            //        <p>Dear ${entities.decode(userData[0].first_name)} ${entities.decode(userData[0].last_name)},</p>

            //         <p>Dr.Reddys API recently received your request for resetting your password for the username - ${entities.decode(user.email)}.</p>

            //         <p>To finish resetting your password, you may now click the below link or copying and pasting it into your browser:</p>

            //         <p><a href="${url}">Click Here</a></p>

            //         <p>This link is valid for 72 hours only and nothing will happen if it's not used.</p>

            //         <p>-- Dr.Reddys API team</p>

            //         <p>Interested in our products - please check our website - api.drreddys.com</p>
            //        </body></html>`
            // }

            // const mailSent = common.sendMailB2C(mailOptions);
            let mailParam = {
              first_name: entities.decode(userData[0].first_name),
              last_name: entities.decode(userData[0].last_name),
              email: entities.decode(user.email),
              url: url
            };
            const mailSent = common.sendMailByCodeB2C('FORGOT_PASSWORD_CUSTOMER', entities.decode(user.email), Config.contactUsMail, '', userData[0].language_code, mailParam, "c");

            if (mailSent) {
              let return_err = {
                'status': 1,
                'errors': "Email sent"
              };
              return res.status(200).json(return_err);
            } else {
              let return_err = {
                'status': 2,
                'errors': "Email has not been sent"
              };
              return res.status(400).json(return_err);
            }

          })
          .catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })

      } else {
        let return_err = {
          'status': 2,
          'errors': "Email does not exist in the system"
        };
        return res.status(400).json(return_err);
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  forgotPasswordNew: async (req, res, next) => {
    try {
      const {
        email
      } = req.body;
      const user = {
        email: entities.encode(email),

      }
      let userExist = await userModel.active_email_exists_new(user.email); // Check if user is a customer
      let agentExist = await agentModel.active_email_exists(user.email); // Check if user is an agent
      let err = {};
      if (agentExist.success) {
        await agentModel.findByEmail(user.email)
          .then(async function (data) {
            var payload = {
              time: new Date().getTime() + (72 * 60 * 60000),
              checksum: data[0].agent_id
            }
            var token = jwtSimple.encode(payload, Config.jwt.secret);
            var url = process.env.REACT_API_URL + `reset_password/` + token;
            const userData = await agentModel.findByUsername(user.email);
            await agentModel.update_token(data[0].agent_id, token);

            // Send reset pass mail with link

            // let mailOptions = {
            //   to: entities.decode(user.email),
            //   from: Config.contactUsMail,
            //   subject: "Finish resetting your Dr.Reddys API password",
            //   html: `<!DOCTYPE html><html>
            //        <body>
            //        <p>Dear ${entities.decode(userData[0].first_name)} ${entities.decode(userData[0].last_name)},</p>

            //         <p>Dr.Reddys API recently received your request for resetting your password for the username - ${entities.decode(user.email)}.</p>

            //         <p>To finish resetting your password, you may now click the below link or copying and pasting it into your browser:</p>

            //         <p><a href="${url}">Click Here</a></p>

            //         <p>This link is valid for 72 hours only and nothing will happen if it's not used.</p>

            //         <p>-- Dr.Reddys API team</p>

            //         <p>Interested in our products - please check our website - api.drreddys.com</p>
            //        </body></html>`
            //   // html : '<!DOCTYPE html><html><head><title>Forget Password Email</title></head><body><div><h3>Dear '+userData[0].first_name+',</h3><p>You requested for a password reset, kindly use this <a href="'+url+'">link</a> to reset your password</p><br><p>Cheers!</p></div></body></html>'
            // }

            // const mailSent = common.sendMailB2C(mailOptions);

            let mailParam = {
              first_name: entities.decode(userData[0].first_name),
              last_name: entities.decode(userData[0].last_name),
              email: entities.decode(user.email),
              url: url
            };
            const mailSent = common.sendMailByCodeB2C('FORGOT_PASSWORD_CUSTOMER', entities.decode(user.email), Config.contactUsMail, '', userData[0].language_code, mailParam, "c");
            if (mailSent) {
              let return_err = {
                'status': 1,
                'errors': "Email sent"
              };
              return res.status(200).json(return_err);
            } else {
              let return_err = {
                'status': 2,
                'errors': "Email has not been sent"
              };
              return res.status(400).json(return_err);
            }

          })
          .catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })


      } else if (userExist.success) {

        if (userExist.activated == 1) {
          await userModel.findByEmail(user.email)
            .then(async function (data) {
              var payload = {
                time: new Date().getTime() + (72 * 60 * 60000),
                checksum: data[0].customer_id
              }
              var token = jwtSimple.encode(payload, Config.jwt.secret);
              var url = process.env.REACT_API_URL + `reset_password/` + token;
              const userData = await userModel.findByUsername(user.email);
              await userModel.update_token(data[0].customer_id, token);

              let mailParam = {
                first_name: entities.decode(userData[0].first_name),
                last_name: entities.decode(userData[0].last_name),
                email: entities.decode(user.email),
                url: url
              };
              const mailSent = common.sendMailByCodeB2C('FORGOT_PASSWORD_CUSTOMER', entities.decode(user.email), Config.contactUsMail, '', userData[0].language_code, mailParam, "c");

              if (mailSent) {
                let return_err = {
                  'status': 1,
                  'errors': "Email sent"
                };
                return res.status(200).json(return_err);
              } else {
                let return_err = {
                  'status': 2,
                  'errors': "Email has not been sent"
                };
                return res.status(400).json(return_err);
              }

            })
            .catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          var payload = {
            time: new Date().getTime() + (72 * 60 * 60000),
            checksum: userExist.customer_id
          }
          var token = jwtSimple.encode(payload, Config.jwt.secret);
          var url = process.env.REACT_API_URL + `set_password/` + token;
          await userModel.update_token(userExist.customer_id, token);

          var expiry_date = moment(new Date()).add(3, 'days').format("YYYY-MM-DD HH:mm:ss");
          await userModel.activate_customer_mail_expiry_date(userExist.customer_id, expiry_date)

          let mailParam = {
            first_name: entities.decode(userExist.first_name),
            last_name: entities.decode(userExist.last_name),
            url: url,
            react_api_url_portal: process.env.REACT_API_URL,
            email: entities.decode(userExist.email)
          };
          const mailSent = common.sendMailByCodeB2C('REGISTRATION_CUSTOMER_FROM_XCEED_ADMIN', entities.decode(userExist.email), "noreply xceed <" + Config.noReplyMail + ">", '', entities.decode(userExist.language_code), mailParam, "c");

          if (mailSent) {
            let return_err = {
              'status': 1,
              'errors': "Email sent"
            };
            return res.status(200).json(return_err);
          } else {
            let return_err = {
              'status': 2,
              'errors': "Email has not been sent"
            };
            return res.status(400).json(return_err);
          }

        }

      } else {
        let return_err = {
          'status': 2,
          'errors': "Email does not exist or yet to be accepted"
        };
        return res.status(400).json(return_err);
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Reset Password
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Reset Password used for customer or agent Password selection
   */
  resetPassword: async (req, res, next) => {
    try {
      const {
        token,
        password
      } = req.body;
      const user = {
        password: entities.encode(password),
        token: entities.encode(token)
      }
      try {
        var decodedToken = jwtSimple.decode(user.token, Config.jwt.secret);
      } catch (error) {
        return res.status(400).json({
          status: 3,
          message: "Token is invalid"
        });
      }
      let err = {};
      let userExist = await userModel.findByUserId(decodedToken.checksum);
      let agentExist = await agentModel.findByUserId(decodedToken.checksum);
      if (agentExist.length > 0 && agentExist[0].activated == 1) {
        if (agentExist[0].token == token) {
          if (decodedToken.time > new Date().getTime()) {

            let password = user.password;
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(password, salt);
            await agentModel.update_activated(decodedToken.checksum);
            await agentModel.update_pass(passwordHash, decodedToken.checksum)
            await agentModel.update_token(decodedToken.checksum, null)
              .then(function (data) {
                res.status(200).json({
                  status: 1,
                  message: "Password reseted successfully"
                }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })

          } else {
            return res.status(400).json({
              status: 3,
              message: "Token expired please resend link"
            });
          }
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token already used"
          });
        }

      } else if (userExist.length > 0 && userExist[0].activated == 1) {
        if (userExist[0].token == token) {
          if (decodedToken.time > new Date().getTime()) {

            let password = user.password;
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(password, salt);
            await userModel.update_activated(decodedToken.checksum);

            // Pre-Activation handling
            const passData = await userModel.get_pass(decodedToken.checksum);
            const preActivatedData = await userModel.is_pre_activated(decodedToken.checksum);
            if(passData && passData.length > 0 && preActivatedData && preActivatedData.length > 0){
                const isMatch = await matchPassword(user.password, passData[0].password);
                if(!isMatch && preActivatedData[0].pre_activated === 1){
                  await userModel.update_pre_activated(decodedToken.checksum);
                }
            }

            await userModel.update_pass(passwordHash, decodedToken.checksum)
            await userModel.update_token(decodedToken.checksum, null)
              .then(function (data) {
                res.status(200).json({
                  status: 1,
                  message: "Password reseted successfully"
                }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })

          } else {
            return res.status(400).json({
              status: 3,
              message: "Token expired please resend link"
            });
          }
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token already used"
          });
        }

      } else {
        return res.status(400).json({
          status: 3,
          message: "User does not exist or not active"
        });
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Set Password
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Set Password used for customer or agent Password selection
   */
  setPassword: async (req, res, next) => {
    try {
      const {
        token,
        password
      } = req.body;
      const user = {
        password: entities.encode(password),
        token: entities.encode(token)
      }
      try {
        var decodedToken = jwtSimple.decode(user.token, Config.jwt.secret);
      } catch (error) {
        return res.status(400).json({
          status: 3,
          message: "Token is invalid"
        });
      }

      let err = {};
      let userExist = await userModel.findByUserId(decodedToken.checksum); // Check if user exists
      let agentExist = await agentModel.findByUserId(decodedToken.checksum); // Check if agent exists

      if (agentExist.length > 0) {

        if (agentExist[0].token == token) {
          if (decodedToken.time > new Date().getTime()) {

            let password = user.password;
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(password, salt);
            await agentModel.update_activated(decodedToken.checksum);
            await agentModel.update_token(decodedToken.checksum, null);
            await agentModel.update_pass(passwordHash, decodedToken.checksum)
              .then(async function (data) {
                let profile_url = null;
                let choose = 0;
                if (agentExist[0].profile_pic != null) {
                  if (process.env.NODE_ENV == 'development') {
                    profile_url = process.env.FULL_PATH + '/profile_pic/' + agentExist[0].profile_pic;
                  } else {
                    s3_bucket_status = await module.exports.__get_signed_url(`${agentExist[0].profile_pic}`);
                    if (s3_bucket_status.status === 1)
                      profile_url = s3_bucket_status.url;
                    else
                      profile_url = process.env.FULL_PATH + '/images/default-user.png';
                  }

                  choose = 0;
                } else {
                  profile_url = process.env.FULL_PATH + '/images/default-user.png';
                  choose = 1;
                }
                let user_data = {
                  user_id: cryptr.encrypt(decodedToken.checksum),
                  lang: agentExist[0].language_code,
                  user_agent: cryptr.encrypt(req.get('User-Agent')),
                  name: entities.decode(agentExist[0].first_name + " " + agentExist[0].last_name),
                  new_feature: agentExist[0].new_feature,
                  new_feature_fa: agentExist[0].new_feature_fa,
                  new_feature_send_mail: agentExist[0].new_feature_send_mail,
                  tour_counter: agentExist[0].tour_counter,
                  tour_counter_new: agentExist[0].tour_counter_new,
                  tour_counter_closed_task: agentExist[0].tour_counter_closed_task,
                  new_feature_11: agentExist[0].new_feature_11,
                  tour_counter_11: agentExist[0].tour_counter_11,
                  multi_lang_access: agentExist[0].multi_lang_access,
                  email: entities.decode(agentExist[0].email),
                  mobile: agentExist[0].phone_no,
                  date_added: agentExist[0].date_added,
                  profile_pic: profile_url,
                  role: 2,
                  choose: choose
                }

                const token = await loginToken(user_data);
                res.status(200).json({
                  status: 1,
                  token: token
                }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })

          } else {
            return res.status(400).json({
              status: 3,
              message: "Token expired please resend link"
            });
          }
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token already used"
          });
        }

      } else if (userExist.length > 0) {
        if (userExist[0].token == token) {
          if (decodedToken.time > new Date().getTime()) {

            let password = user.password;
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(password, salt);
            await userModel.update_activated(decodedToken.checksum);

            // Pre-Activation handling
            const passData = await userModel.get_pass(decodedToken.checksum);
            const preActivatedData = await userModel.is_pre_activated(decodedToken.checksum);
            if(passData && passData.length > 0 && preActivatedData && preActivatedData.length > 0){
                const isMatch = await matchPassword(user.password, passData[0].password);
                if(!isMatch && preActivatedData[0].pre_activated === 1){
                  await userModel.update_pre_activated(decodedToken.checksum);
                }
            }

            await userModel.update_token(decodedToken.checksum, null);
            await userModel.update_pass(passwordHash, decodedToken.checksum)
              .then(async function (data) {
                let profile_url = null;
                let choose = 0;
                if (userExist[0].profile_pic != null) {
                  if (process.env.NODE_ENV == 'development') {
                    profile_url = process.env.FULL_PATH + '/profile_pic/' + userExist[0].profile_pic;
                  } else {
                    s3_bucket_status = await module.exports.__get_signed_url(`${userExist[0].profile_pic}`);
                    if (s3_bucket_status.status === 1)
                      profile_url = s3_bucket_status.url;
                    else
                      profile_url = process.env.FULL_PATH + '/images/default-user.png';
                  }

                  choose = 0;
                } else {
                  profile_url = process.env.FULL_PATH + '/images/default-user.png';
                  choose = 1;
                }
                if (Config.activate_sales_force == true) {
                  let customerDetails = await customerModel.get_sales_force_customer(decodedToken.checksum);
                  let companyDetailsUpdated = await customerModel.get_company_details(customerDetails[0].company_id);
                  let countryName = await customerModel.country_name(customerDetails[0].country_id);
                  //console.log('customerDetails', customerDetails);
                  const sales_data_cust = {
                    first_name: entities.encode(customerDetails[0].first_name),
                    last_name: entities.encode(customerDetails[0].last_name),
                    country_name: `${countryName}`,
                    company_id: `${companyDetailsUpdated[0].company_id}`,
                    sales_company_id: (companyDetailsUpdated[0].sales_company_id != null) ? `${companyDetailsUpdated[0].sales_company_id}` : '',
                    email: entities.encode(customerDetails[0].email),
                    phone_number: `${entities.encode(customerDetails[0].phone_no)}`,
                    customer_id: `${customerDetails[0].cqt_customer_id}`
                  }

                  sales_data_cust.status = (customerDetails[0].status == 3) ? 0 : customerDetails[0].status;
                  sales_data_cust.activated = customerDetails[0].activated;
                  sales_data_cust.sales_customer_id = customerDetails[0].sales_customer_id;

                  //console.log('sales_data_cust', sales_data_cust);
                  let sales_response_cust = await outwardCrm.update_customer(sales_data_cust);
                  if (sales_response_cust.status == 1) {
                    //DO NOTHING
                  } else {
                    common.outwardCrmError(sales_response_cust.error, `Error Crm While Updating Customer ${Config.drupal.url}`, `Customer ID:${decodedToken.checksum}`);
                  }
                }
                let user_role_finance = await userModel.checkUserRoleFinance(decodedToken.checksum);

                let user_data = {
                  user_id: cryptr.encrypt(decodedToken.checksum),
                  lang: userExist[0].language_code,
                  user_agent: cryptr.encrypt(req.get('User-Agent')),
                  name: entities.decode(userExist[0].first_name + " " + userExist[0].last_name),
                  new_feature: userExist[0].new_feature,
                  new_feature_send_mail: userExist[0].new_feature_send_mail,
                  new_feature_fa: userExist[0].new_feature_fa,
                  tour_counter: userExist[0].tour_counter,
                  tour_counter_new: userExist[0].tour_counter_new,
                  tour_counter_closed_task: userExist[0].tour_counter_closed_task,
                  new_feature_11: userExist[0].new_feature_11,
                  tour_counter_11: userExist[0].tour_counter_11,
                  multi_lang_access: userExist[0].multi_lang_access,
                  email: entities.decode(userExist[0].email),
                  date_added: userExist[0].date_added,
                  mobile: userExist[0].phone_no,
                  profile_pic: profile_url,
                  role: 1,
                  choose: choose,
                  show_stock_enquiry: (userExist[0].show_stock_enquiry != undefined) ? userExist[0].show_stock_enquiry : 0,
                  role_finance: user_role_finance,
                  pre_activated: userExists[0].pre_activated
                }

                const token = await loginToken(user_data);

                res.status(200).json({
                  status: 1,
                  token: token
                }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })

          } else {
            return res.status(400).json({
              status: 3,
              message: "Token expired please resend link"
            });
          }
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token already used"
          });
        }

      } else {
        res.status(400).json({
          status: 3,
          message: Config.errorText.value
        }).end();
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Add User
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Add User used for user registration selection
   */
  add_user: async (req, res, next) => {
    try {
      const {
        first_name,
        last_name,
        mobile,
        email,
        country_id,
        company_name,
        language_code
      } = req.value.body;
      //let dateTime = getCurrentDate();
      let password = "reddy1234";
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(password, salt);


      await userModel.get_company_id(entities.encode(company_name))
        .then(async function (data) {

          let company_id = 0;

          if (data != '') {
            company_id = data[0].company_id;
          } else {
            await userModel.add_company(entities.encode(company_name))
              .then(async function (data) {
                company_id = data.company_id;
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })
          }

          const userobj = {
            password: null,
            email: entities.encode(email),
            first_name: entities.encode(first_name),
            last_name: entities.encode(last_name),
            phone_no: entities.encode(mobile),
            country_id: country_id,
            company_id: company_id,
            //added_by: 0,
            date_added: common.currentDateTime(),
            dnd: 2,
            language_code: language_code
          }
          // Check if its a dummy user
          const dummyData = await userModel.email_exists_dummy(userobj.email);

          if (dummyData.length > 0) {

            await userModel.update_dummy_user(userobj, dummyData[0].customer_id)
              .then(function (data) {
                //Send activation pending mail
                // let mailOptions = {
                //   to: entities.decode(userobj.email),
                //   from: "[Dr Reddys] <" + Config.contactUsMail + ">",
                //   subject: "Your Registration at Dr. Reddy’s XCEED Platform (Pending Activation)",
                //   html: `<!DOCTYPE html><html><head>
                //   <p>Dear ${entities.decode(userobj.first_name)} ${entities.decode(userobj.last_name)},</p>

                //   <p>Thank you for registering with Dr. Reddy’s XCEED Platform. Your account is currently pending activation. You will receive another email shortly with details on your account access and setting up of your password.</p>

                //   <p>-- Dr.Reddys API team</p>

                //   <p>Interested in our products - please check our website - api.drreddys.com</p>
                //   </body></html>`
                // }

                // common.sendMailB2C(mailOptions);

                let mailParam = userobj;
                const mailSent = common.sendMailByCodeB2C('REGISTRATION_CUSTOMER', entities.decode(userobj.email), "[Dr Reddys] <" + Config.contactUsMail + ">", '', language_code, mailParam, "c");

                res.status(200)
                  .json({
                    status: 1
                    // data: data
                  }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              });

          } else {
            await userModel.add_user(userobj)
              .then(async function (data) {
                await userModel.add_role(data.customer_id, 1, 1);
                //ADD TEAM

                let { bm, cscc, csct, ra } = await userModel.get_country_team(country_id);

                let teams = [];

                if (bm != null && bm != '') {
                  teams.push(bm);
                }

                if (csct != null && csct != '') {
                  teams.push(csct);
                }

                if (cscc != null && cscc != '') {
                  teams.push(cscc);
                }

                if (ra != null && ra != '') {
                  teams.push(ra);
                }

                //console.log('teams', teams);
                let team_assign = [];
                if (teams.length > 0) {
                  for (index = 0; index < teams.length; index++) {
                    team_assign[index] = await userModel.add_team(data.customer_id, teams[index]);
                    await userModel.add_customer_sla(data.customer_id, teams[index], 0, 'E');                    
                  }
                  for (index = 0; index < teams.length; index++) {
                    module.exports.__sendOnBoardingEnmployeeEmail(data.customer_id, teams[index], team_assign[index].team_id);
                  }
                }

                //activation pending mail
                // let mailOptions = {
                //   to: entities.decode(userobj.email),
                //   from: "[Dr Reddys] <" + Config.contactUsMail + ">",
                //   subject: "Your Registration at Dr. Reddy's XCEED Platform (Pending Activation)",
                //   html: `<!DOCTYPE html><html><head>
                //   <p>Dear ${entities.decode(userobj.first_name)} ${entities.decode(userobj.last_name)},</p>

                //   <p>Thank you for registering with Dr. Reddy’s XCEED Platform. Your account is currently pending activation. You will receive another email shortly with details on your account access and setting up of your password.</p>

                //   <p>-- Dr.Reddys API team</p>

                //   <p>Interested in our products - please check our website - api.drreddys.com</p>
                //   </body></html>`
                // }

                // common.sendMailB2C(mailOptions);
                let mailParam = userobj;
                const mailSent = common.sendMailByCodeB2C('REGISTRATION_CUSTOMER', entities.decode(userobj.email), "[Dr Reddys] <" + Config.contactUsMail + ">", '', language_code, mailParam, "c");

                res.status(200)
                  .json({
                    status: 1
                    // data: data
                  }).end();
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              });
          }

        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  __generateShadowToken: async (assign, response) => {
    return new Promise((resolve, reject) => {
      let jwttoken = JWT.sign({
        iss: 'Reddys',
        asni: cryptr.encrypt(assign),
        cr: response,
        prxd: '1',
        iat: Math.round((new Date().getTime()) / 1000),
        exp: Math.round((new Date().getTime()) / 1000) + (7 * 24 * 60 * 60),
      }, Config.jwt.secret);

      resolve(jwttoken);
    });
  },
  __sendOnBoardingEnmployeeEmail: async (customer_id, employee_id, assign_id) => {
    //DEBA SEND MAIL
    return new Promise(async (resolve, reject) => {
      let customer_team_arr = await userModel.customer_team(customer_id);
      let customer_team_html = '';
      for (let int = 0; int < customer_team_arr.length; int++) {
        const element = customer_team_arr[int];
        customer_team_html += `<p>${element.first_name} ${element.last_name} (${element.desig_name})</p>`;
      }
      let customer_details = await userModel.get_user_for_mail(customer_id);
      let employee_details = await employeeModel.get_employee_details(employee_id); //console.log('employee',employee_details);


      if (customer_details && employee_details) {
        let hashtoken = hashids.encode(assign_id);
        let appoval_key = await module.exports.__generateShadowToken(assign_id, 1);
        let assign_key = await module.exports.__generateShadowToken(assign_id, 2);

        let mailParam = {
          emp_first_name: employee_details[0].first_name,
          emp_last_name: employee_details[0].last_name,
          first_name: customer_details[0].first_name,
          last_name: customer_details[0].last_name,
          company_name: customer_details[0].company_name,
          user_email: customer_details[0].email,
          user_phone: customer_details[0].phone_no,
          country: customer_details[0].country_name,
          language: customer_details[0].language,
          assign_team: customer_team_html,
          is_admin: (customer_details[0].role_type == 0) ? 'Yes' : 'No',
          approve_url: Config.ccpUrl + '/process_customer_approval/' + appoval_key,
          assign_url: Config.ccpUrl + '/user/dashboard/review_request/' + hashtoken,
          ccpurl: Config.ccpUrl
        };
        //console.log('mail', mailParam);
        const mailSent = common.sendMailByCodeB2E('EMPLOYEE_APPROVAL_TEMPLATE_FOR_CUSTOMER', entities.decode(employee_details[0].email), Config.webmasterMail, '', mailParam, "e");

        resolve(true);
      } else {
        resolve(false);
      }
    });
  },
  /**
   * Generates a Contact
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Contact used for contact selection
   */
  sample_contact: async (req, res, next) => {
    try {

      let salesForceToken = await salesForce.getToken();
      //console.log('This is sales force response',salesForceToken);
      if (salesForceToken.status == 1 && salesForceToken.token.access_token != '') {

        let sfBody = {
          req_name: "Soumyadeep",
          prod_id: "XCEEDPRODUCTID",
          company_id: "XCEEDCOMPANYID",
          description: "Posted by Insdusnet"
        };
        //console.log('sfBody', sfBody);
        let sfURL = salesForceToken.token.instance_url + '/services/apexrest/drreddys/caserequest';
        //console.log('sfURL',sfURL);
        let salesForceResponse = await salesForce.sendSampleDetails(salesForceToken.token.access_token, sfBody, sfURL);
        console.log('sales Force Response', salesForceResponse);
      } else {
        common.logError(err);
        res.status(400).json({
          status: 3,
          message: Config.errorText.value
        }).end();
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Contact Both
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Contact Both used for contact Both selection
   */
  contact_me: async (req, res, next) => {
    try {
      const {
        mobile,
        email,
        country_id,
        message,
        name,
        company_name,
        product_id,
        product_name
      } = req.value.body;
      //let dateTime = getCurrentDate();

      var log_now = common.calcTime();
      var log_date_format = dateFormat(log_now, "yyyy-mm-dd HH:MM:ss");

      var log_err = `\n DateTime: ${log_date_format} \n ${JSON.stringify(req.value.body)} \n`;

      var log_file = `log/contact_request${dateFormat(log_now, "mm_yyyy")}.txt`;

      //fs.appendFile(log_file,log_err, (err) => {
      //if (err) throw err;
      //console.log('The file has been saved!');
      //});

      if (Config.sales_force_contacts.use === true) {
        let salesForceToken = await salesForce.getToken();
        //console.log('This is sales force token',salesForceToken.token.access_token);

        if (salesForceToken.status == 1) {

          var country_name = 'NA';
          if (country_id) {
            await userModel.get_country_name(country_id)
              .then(async function (data) {
                country_name = data[0].country_name ? data[0].country_name : 'NA';
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })

          }

          let cqt_product_id = await userModel.get_cqt_product_id(product_id);

          let sfBody = {
            name: name,
            email: email,
            mobile: mobile,
            country: country_name,
            message: message,
            leadSource: "Website - Contact Us",
            company: company_name,
            product: entities.decode(product_name),
            cqt_product_id: cqt_product_id
          };
          //console.log('sfBody', sfBody);

          let salesForceResponse = await salesForce.sendContactDetails(salesForceToken.token.access_token, sfBody);

          if (salesForceResponse.status == 1) {
            //console.log('This is response from sales force',salesForceResponse.success_token);
            const userobj = {
              email: entities.encode(email),
              name: entities.encode(name),
              mobile: entities.encode(mobile),
              country_id: country_id,
              company_name: entities.encode(company_name),
              message: entities.encode(message),
              date_added: common.currentDateTime(),
              sales_force_token: salesForceResponse.success_token,
              product_id: product_id
            }
            var mobile_no = (userobj.mobile) ? userobj.mobile : 'NA';

            var breakTag = '<br>';
            var messageHtml = 'NA';
            if (userobj.message.length > 0)
              messageHtml = (userobj.message + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            userobj.messagehtml = messageHtml;
            userobj.mobile_no = mobile_no;
            userobj.country_name = country_name;

            await userModel.add_contact(userobj)
              .then(function (data) {

                // let mailOptions = {
                //   to: entities.decode(userobj.email),
                //   from: "[Dr Reddys] <" + Config.contactUsMail + ">",
                //   subject: "Thank you | " + entities.decode(userobj.name),
                //   html: '<!DOCTYPE html><html><head><h3><b>Thank you, we have received your request with following details.</b></h3></head><body><div><p><b>Name : </b>' + userobj.name + '<br><br><b>Email : </b>' + userobj.email + '<br><br><b> Mobile No : </b>' + mobile_no + '<br><br><b>Country : </b>' + country_name + '<br><br><b>Company Name : </b>' + userobj.company_name + '<br><br><b> Message : </b>' + messageHtml + '<br><br><b>Agree to Privacy Policy : </b> Yes</p></div></body></html>'
                // }

                // const mailSent = common.sendMailB2C(mailOptions);
                let mailParam = userobj;
                //console.log(mailParam);
                const mailSent = common.sendMailByCodeB2C('CONTACT_US_CUSTOMER', entities.decode(userobj.email), "[Dr Reddys] <" + Config.contactUsMail + ">", '', 'en', mailParam, "c");

                // let mailOptions1 = {
                //   to     : Config.onlyContactUsEmail,
                //   from   : "[Dr Reddys] <" + Config.contactUsMail + ">",
                //   subject: "Contact Us Query | " + entities.decode(userobj.name),
                //   html   : '<!DOCTYPE html><html><head><h3><b>You have a new query generated.</b></h3></head><body><div><p><b>Name: </b>' + userobj.name + '<br><br><b>Email: </b>' + userobj.email + '<br><br><b> Mobile No: </b>' + mobile_no + '<br><br><b>Country: </b>' + country_name + '<br><br><b>Company Name: </b>' + userobj.company_name + '<br><br><b> Message: </b>' + messageHtml + '<br><br><b>Agree to Privacy Policy: </b> Yes</p></div></body></html>'
                // }

                // const mailSent1 = common.sendMailB2C(mailOptions1);

                const mailSent1 = common.sendMailByCodeB2C('CONTACT_US_EMAILUS', Config.onlyContactUsEmail, "[Dr Reddys] <" + Config.contactUsMail + ">", '', 'en', mailParam, "s");

                if (mailSent1) {
                  let return_err = {
                    'status': 1,
                    'message': "Contacted successfully"
                  };
                  return res.status(200).json(return_err);
                } else {
                  let return_err = {
                    'status': 2,
                    'message': "Error in posting request"
                  };
                  return res.status(400).json(return_err);
                }
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })
          } else {
            common.logError(salesForceResponse.error);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          }
        } else {
          common.logError(salesForceToken.error);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        }
      } else {
        let return_err = {
          'status': 1,
          'message': "Contacted successfully"
        };
        return res.status(200).json(return_err);
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a S3 signed url
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} key - HTTP request argument to the middleware function
   * @return {Json Object} - S3 signed url used for S3 signed url selection
   */
  __get_signed_url: async (key) => {
    // try{

    return new Promise((resolve, reject) => {

      params = {
        Bucket: Config.aws.bucketName,
        Key: key,
        Expires: 43200
      }

      AWS.config.update({
        accessKeyId: Config.aws.accessKeyId,
        secretAccessKey: Config.aws.secretAccessKey
      });

      AWS.config.region = Config.aws.regionName;
      var s3 = new AWS.S3();
      s3.getSignedUrl('getObject', params, function (err, url) {
        if (err) {
          resolve({
            status: 3,
            message: err
          });
        } else {
          resolve({
            status: 1,
            url: url
          });
        }

      });
    });

  },
  /**
   * Generates a Customer Team List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Customer List team used for customer team selection
   */
  list_teams: async (req, res, next) => {
    try {
      await userModel.customer_team(req.user.customer_id)
        .then(async function (data) {
          for (let index = 0; index < data.length; index++) {
            let element = data[index];
            if (element.profile_pic && element.profile_pic != '') {
              let aws_pic_url = await module.exports.__get_signed_url(`${element.profile_pic}`);
              if (aws_pic_url.status && aws_pic_url.status == 1) {
                element.profile_pic = aws_pic_url.url;
              }
            }
            data[index] = element;
          }
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Document Type List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Document type List used for Document type selection
   */
  list_document_type: async (req, res, next) => {
    try {
      await customerModel.list_document_type(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product Unit List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product unit List used for Product unit selection
   */
  list_product_unit: async (req, res, next) => {
    try {
      await customerModel.list_product_unit(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Stability Data List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Stability data List used for Stability data selection
   */
  list_stability_data: async (req, res, next) => {
    try {
      await customerModel.list_stability_data(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Country List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Country List used for Country selection
   */
  list_country: async (req, res, next) => {
    try {
      await customerModel.list_country(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Country List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Country List used for Country selection
   */
  list_country_open: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.type));
      await customerModel.list_country(query_string)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Language List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Language List used for Language selection
   */
  list_language: async (req, res, next) => {
    try {
      await customerModel.list_language(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Language List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Language List used for Language selection
   */
  list_language_open: async (req, res, next) => {
    let query_string = entities.encode(trim(req.params.type));
    try {
      await customerModel.list_language(query_string)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Company List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Company List used for Company selection
   */
  list_company: async (req, res, next) => {
    try {
      await customerModel.list_company()
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Product List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Product List used for Product selection
   */
  list_all_products: async (req, res, next) => {
    try {
      //console.log(req.user)
      await customerModel.list_all_products(req.user.lang)
        .then(function (data) {
          res.status(200)
            .json({
              status: 1,
              data: data
            });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Search Requst List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search requst List used for Search requst selection
   */
  search_my_requests: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.keyword));
      //console.log( 'query_string', query_string );
      if (req.user.role == 2) {

        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.request_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        if (customer_arr.length > 0) {
          await userModel.request_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Search Order List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search order List used for Search order selection
   */
  search_my_order: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.keyword));
      if (req.user.role == 2) {

        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.order_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      } else {
        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        if (customer_arr.length > 0) {
          await userModel.order_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Search Complaint List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search complaint List used for Search complaint selection
   */
  search_my_complaint: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.keyword));
      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.complaint_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      } else {
        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);

        if (customer_arr.length > 0) {
          await userModel.complaint_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Search Forecast List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search forecast List used for Search forecast selection
   */
  search_my_forecast: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.keyword));

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.forecast_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      } else {
        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);

        if (customer_arr.length > 0) {
          await userModel.forecast_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Requst List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Requst list used for requst selection
   */
  list_my_requests: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }

      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      //AGENT
      if (req.user.role == 2) {

        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }
        //console.log(customer_arr);

        if (customer_arr.length > 0) {
          await agentModel.request_list(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_requests = await agentModel.count_request_list(req.user.customer_id, customer_arr, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_requests
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            });
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);

        if (customer_arr.length > 0) {
          await userModel.request_list(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_requests = await userModel.count_request_list(customer_arr, req.user.customer_id, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_requests
              });

            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Order List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Order list used for order selection
   */
  list_my_order: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }


      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }
        if (customer_arr.length > 0) {
          await agentModel.order_list(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

              }

              let count_open_orders = await agentModel.count_order_list(req.user.customer_id, customer_arr, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_orders
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }

      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        //console.log('customer_arr',customer_arr);
        if (customer_arr.length > 0) {
          await userModel.order_list(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_orders = await userModel.count_order_list(customer_arr, req.user.customer_id, query_arr);

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  if (q_arr[1] == 'KG') {
                    q_arr[1] = "Kgs";
                  }
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

              }

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_orders
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  list_my_order_new: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }


      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }
        if (customer_arr.length > 0) {
          await agentModel.order_list_new(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

                if (data[index].order_verified_status === 0) {
                  let product_data = await userModel.get_temp_task_products(data[index].task_id);
                  data[index].unverified_products = product_data.map(val => { return val.product_data }).join(', ');
                }

                data[index].invoicehash = '';
                data[index].ship_status = '';
                data[index].coa_file = [];
                if (data[index].invoice_number != null && data[index].invoice_number != '') {
                  //console.log(data[index].invoice_number+"=="+data[index].soldto)
                  data[index].invoicehash = cryptr.encrypt(data[index].invoice_number + '#' + data[index].task_id);
                  taskDetailsFilesCoa = await userModel.get_coa_files(data[index].invoice_number, data[index].sales_order_no);

                  for (var i = 0; i < taskDetailsFilesCoa.length; i++) {
                    if (process.env.NODE_ENV == 'development') {
                      taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFilesCoa[i]['new_file_name'];
                    } else {
                      taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(taskDetailsFilesCoa[i]['invoice_number'] + '#' + taskDetailsFilesCoa[i]['task_id'] + '#' + taskDetailsFilesCoa[i]['coa_id']);
                    }
                  }
                  data[index].coa_file = taskDetailsFilesCoa;
                  //console.log(data[index].coa_file);

                  taskDetailsShipStatus = await sapModel.get_ship_status(data[index].invoice_number);
                  //console.log(taskDetailsShipStatus);
                  if (taskDetailsShipStatus) {

                    if ((taskDetailsShipStatus.tracking_info_updated == 'true' && taskDetailsShipStatus.tracking_info_updated != null) || (taskDetailsShipStatus.delivery_at_customer_destination != '' && taskDetailsShipStatus.delivery_at_customer_destination != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Delivered', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_arrived_at_destination != '' && taskDetailsShipStatus.shipment_arrived_at_destination != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Arrived', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_departed != '' && taskDetailsShipStatus.shipment_departed != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Departed', data[index].language);
                    } else if ((taskDetailsShipStatus.hawb != '' && taskDetailsShipStatus.hawb_time != '' && taskDetailsShipStatus.hawb != null && taskDetailsShipStatus.hawb_time != null) || (taskDetailsShipStatus.flight_booked != '' && taskDetailsShipStatus.flight_booked_time != '' && taskDetailsShipStatus.flight_booked != null && taskDetailsShipStatus.flight_booked_time != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('AWB Doc Generated', data[index].language);
                    } else if ((taskDetailsShipStatus.await_approval_for_document_from_consignee != '' && taskDetailsShipStatus.approval_for_document_from_consignee_time != '' && taskDetailsShipStatus.await_approval_for_document_from_consignee != null && taskDetailsShipStatus.approval_for_document_from_consignee_time != null) || (taskDetailsShipStatus.await_approval_for_haz_clearance_from_consignee != '' && taskDetailsShipStatus.approval_for_haz_clearance_from_consignee_time != null && taskDetailsShipStatus.await_approval_for_haz_clearance_from_consignee != '' && taskDetailsShipStatus.approval_for_haz_clearance_from_consignee_time != null) || (taskDetailsShipStatus.fda_approval_awaited != '' && taskDetailsShipStatus.fda_approval_time != '' && taskDetailsShipStatus.fda_approval_awaited != null && taskDetailsShipStatus.fda_approval_time != null) || (taskDetailsShipStatus.customs_clearance != '' && taskDetailsShipStatus.customs_clearance_completion_time != '' && taskDetailsShipStatus.customs_clearance != null && taskDetailsShipStatus.customs_clearance_completion_time != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('In Progress', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_date != '' && taskDetailsShipStatus.shipment_date != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Shipment Picked', data[index].language);
                    } else if ((taskDetailsShipStatus.packing_date != '' && taskDetailsShipStatus.packing_date != null) || (taskDetailsShipStatus.packing_file_name != '' && taskDetailsShipStatus.packing_file_name != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Packing List Generated', data[index].language);
                    } else if ((taskDetailsShipStatus.invoice_date != '' && taskDetailsShipStatus.invoice_date != null) || (taskDetailsShipStatus.invoice_file_name != '' && taskDetailsShipStatus.invoice_file_name != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Invoice Doc Generated', data[index].language);
                    } else {
                      data[index].ship_status = '';
                    }
                  }
                }
              }

              let count_open_orders = await agentModel.count_order_list_new(req.user.customer_id, customer_arr, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_orders
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }

      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        //console.log('customer_arr',customer_arr);
        if (customer_arr.length > 0) {
          await userModel.order_list_new(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_orders = await userModel.count_order_list_new(customer_arr, req.user.customer_id, query_arr);

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  if (q_arr[1] == 'KG') {
                    q_arr[1] = "Kgs";
                  }
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

                if (data[index].order_verified_status === 0) {
                  let product_data = await userModel.get_temp_task_products(data[index].task_id);
                  data[index].unverified_products = product_data.map(val => { return val.product_data }).join(', ');
                }

                data[index].invoicehash = '';
                data[index].coa_file = [];
                data[index].ship_status = '';
                if (data[index].invoice_number != null && data[index].invoice_number != '') {
                  //console.log(data[index].invoice_number+"=="+data[index].soldto)
                  data[index].invoicehash = cryptr.encrypt(data[index].invoice_number + '#' + data[index].task_id);
                  taskDetailsFilesCoa = await userModel.get_coa_files(data[index].invoice_number, data[index].sales_order_no);

                  for (var i = 0; i < taskDetailsFilesCoa.length; i++) {
                    if (process.env.NODE_ENV == 'development') {
                      taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/client_uploads/' + taskDetailsFilesCoa[i]['new_file_name'];
                    } else {
                      taskDetailsFilesCoa[i]['url'] = process.env.FULL_PATH + '/api/tasks/get_coa_task_file/' + cryptr.encrypt(taskDetailsFilesCoa[i]['invoice_number'] + '#' + taskDetailsFilesCoa[i]['task_id'] + '#' + taskDetailsFilesCoa[i]['coa_id']);
                    }
                  }
                  data[index].coa_file = taskDetailsFilesCoa;
                  //console.log(data[index].coa_file);

                  taskDetailsShipStatus = await sapModel.get_ship_status(data[index].invoice_number);
                  //console.log(taskDetailsShipStatus);
                  if (taskDetailsShipStatus) {
                    if ((taskDetailsShipStatus.tracking_info_updated == 'true' && taskDetailsShipStatus.tracking_info_updated != null) || (taskDetailsShipStatus.delivery_at_customer_destination != '' && taskDetailsShipStatus.delivery_at_customer_destination != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Delivered', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_arrived_at_destination != '' && taskDetailsShipStatus.shipment_arrived_at_destination != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Arrived', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_departed != '' && taskDetailsShipStatus.shipment_departed != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Departed', data[index].language);
                    } else if ((taskDetailsShipStatus.hawb != '' && taskDetailsShipStatus.hawb_time != '' && taskDetailsShipStatus.hawb != null && taskDetailsShipStatus.hawb_time != null) || (taskDetailsShipStatus.flight_booked != '' && taskDetailsShipStatus.flight_booked_time != '' && taskDetailsShipStatus.flight_booked != null && taskDetailsShipStatus.flight_booked_time != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('AWB Doc Generated', data[index].language);
                    } else if ((taskDetailsShipStatus.await_approval_for_document_from_consignee != '' && taskDetailsShipStatus.approval_for_document_from_consignee_time != '' && taskDetailsShipStatus.await_approval_for_document_from_consignee != null && taskDetailsShipStatus.approval_for_document_from_consignee_time != null) || (taskDetailsShipStatus.await_approval_for_haz_clearance_from_consignee != '' && taskDetailsShipStatus.approval_for_haz_clearance_from_consignee_time != null && taskDetailsShipStatus.await_approval_for_haz_clearance_from_consignee != '' && taskDetailsShipStatus.approval_for_haz_clearance_from_consignee_time != null) || (taskDetailsShipStatus.fda_approval_awaited != '' && taskDetailsShipStatus.fda_approval_time != '' && taskDetailsShipStatus.fda_approval_awaited != null && taskDetailsShipStatus.fda_approval_time != null) || (taskDetailsShipStatus.customs_clearance != '' && taskDetailsShipStatus.customs_clearance_completion_time != '' && taskDetailsShipStatus.customs_clearance != null && taskDetailsShipStatus.customs_clearance_completion_time != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('In Progress', data[index].language);
                    } else if (taskDetailsShipStatus.shipment_date != '' && taskDetailsShipStatus.shipment_date != null) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Shipment Picked', data[index].language);
                    } else if ((taskDetailsShipStatus.packing_date != '' && taskDetailsShipStatus.packing_date != null) || (taskDetailsShipStatus.packing_file_name != '' && taskDetailsShipStatus.packing_file_name != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Packing List Generated', data[index].language);
                    } else if ((taskDetailsShipStatus.invoice_date != '' && taskDetailsShipStatus.invoice_date != null) || (taskDetailsShipStatus.invoice_file_name != '' && taskDetailsShipStatus.invoice_file_name != null)) {
                      data[index].ship_status = await common.getTranslatedTextShipping('Invoice Doc Generated', data[index].language);
                    } else {
                      data[index].ship_status = '';
                    }
                  }
                }


              }

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_orders
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Complaints List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Complaints list used for complaints selection
   */
  list_my_complaints: async (req, res, next) => {
    try {
      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }

      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);
        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);
        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }
        if (customer_arr.length > 0) {
          await agentModel.complaint_list(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_complaints = await agentModel.count_complaint_list(req.user.customer_id, customer_arr, query_arr);

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

              }

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_complaints
              });

            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }

      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        if (customer_arr.length > 0) {
          await userModel.complaint_list(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_complaints = await userModel.count_complaint_list(customer_arr, req.user.customer_id, query_arr);

              for (let index = 0; index < data.length; index++) {

                if (data[index].language != Config.default_language && (data[index].quantity && trim(data[index].quantity) != '')) {

                  let q_arr = data[index].quantity.split(' ');
                  let t_q_arr = await taskModel.get_translated_quantity(q_arr[1], data[index].language);
                  data[index].quantity = `${q_arr[0]} ${t_q_arr[0].name}`;

                }

              }

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_complaints
              });

            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Forecast List
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Forecast list used for forecast selection
   */
  list_my_forecast: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }

      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);
        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);
        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.forecast_list(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_forecast = await agentModel.count_forecast_list(req.user.customer_id, customer_arr, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_forecast
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }

      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);

        if (customer_arr.length > 0) {
          await userModel.forecast_list(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              let count_open_forecast = await userModel.count_forecast_list(customer_arr, req.user.customer_id, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_forecast
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Dashboard
   * @author Monosom Sharma <monosom.sharma@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Dashboard used for Dashboard selection
   */
  overview: async (req, res, next) => {
    try {

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);
        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);
        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.dashboard_overview(req.user.customer_id, customer_arr)
            .then(function (data) {
              res.status(200)
                .json({
                  status: 1,
                  data: data
                });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            });
        } else {
          res.status(200)
            .json({
              status: 1,
              data: {
                product: 0,
                request: 0,
                orders: 0,
                complaints: 0,
                forecasts: 0,
                notification: 0,
                payment: 0,
                discussion: 0
              }
            });
        }
      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        if (customer_arr.length > 0) {
          await userModel.dashboard_overview(customer_arr, req.user.customer_id)
            .then(function (data) {
              //console.log(data);
              res.status(200)
                .json({
                  status: 1,
                  data: data
                });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            });
        } else {
          res.status(200)
            .json({
              status: 1,
              data: {
                product: 0,
                request: 0,
                orders: 0,
                complaints: 0,
                forecasts: 0,
                notification: 0,
                payment: 0,
                discussion: 0
              }
            });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Products List
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Products list used for Products selection
   */
  my_products: async (req, res, next) => {
    try {
      await userModel.my_products(req.user.customer_id)
        .then(function (data) {
          res.status(200).json({
            status: 3,
            data: data
          }).end();
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Update Account
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Update Account used for Update Account selection
   */
  update_account: async (req, res, next) => {
    try {
      const {
        country_id,
        phone_no,
        current_password,
        password,
        file_name,
        dnd,
        language_code
      } = req.body; /*,dnd */
      //console.log('dnd',dnd);
      const customerobj = {
        password: entities.encode(password),
        current_password: entities.encode(current_password),
        phone_no: entities.encode(phone_no),
        country_id: country_id,
        dnd: dnd,
        language_code: language_code
      }
      const {
        role
      } = req.user;


      let is_pre_activated = true;
      let isSamePassword = false;
      let password_entry = '';
      if (role == 2) {
        await agentModel.get_pass(req.user.customer_id)
          .then(async function (data) {
            password_entry = data[0].password;
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      } else {
        await userModel.get_pass(req.user.customer_id)
          .then(async function (data) {
            password_entry = data[0].password;
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })

        await userModel.is_pre_activated(req.user.customer_id)
        .then(async function (data) {
          if(data && data.length > 0){
            if(data[0].is_pre_activated === 0){
              is_pre_activated = false;
            }
          }
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
      }

      let userobj = {};
      if (Object.prototype.hasOwnProperty.call(req.body, 'current_password') && customerobj.current_password != "") {
        const salt = await bcrypt.genSalt(10);
        const oldPasswordHash = await bcrypt.hash(customerobj.current_password, salt);
        const passwordHash = await bcrypt.hash(customerobj.password, salt);
        password_entry = passwordHash;
        isSamePassword = oldPasswordHash === passwordHash; 
      }else{
        isSamePassword = true;
      }



      if (file_name != null && role == 1) {
        userobj = {
          // first_name: entities.encode(first_name),
          // last_name: entities.encode(last_name),
          // phone_no: phone_no ,
          // country_id : country_id,
          // password : password_entry,
          profile_pic: file_name.filename
        }
      } else if (role == 1) {
        userobj = {
          phone_no: customerobj.phone_no,
          country_id: country_id,
          password: password_entry,
          dnd: customerobj.dnd,
          language_code: customerobj.language_code,
          pre_activated: !isSamePassword && is_pre_activated ? 0 : 1
          //profile_pic : null
        }
      }

      if (file_name != null && role == 2) {
        userobj = {
          // first_name: entities.encode(first_name),
          // last_name: entities.encode(last_name),
          // phone_no: phone_no ,
          // country_id : country_id,
          // password : password_entry,
          profile_pic: file_name.filename
        }
      } else if (role == 2) {
        userobj = {
          phone_no: customerobj.phone_no,
          country_id: country_id,
          password: password_entry,
          country_id: country_id,
          dnd: customerobj.dnd,
          language_code: customerobj.language_code
          //profile_pic : null
        }
      }

      // console.log(userobj);
      // return false;
      if (role == 2) {
        await agentModel.update_agent(userobj, req.user.customer_id)
          .then(async function (data) {

            await agentModel.get_user(req.user.customer_id)
              .then(async function (data1) {
                if (data1[0].profile_pic != null && file_name != null) {

                  if (process.env.NODE_ENV == 'production') {
                    let url = '';
                    s3_bucket_status = await module.exports.__get_signed_url(`${data1[0].profile_pic}`);
                    if (s3_bucket_status.status === 1)
                      url = s3_bucket_status.url;
                    else
                      url = process.env.FULL_PATH + '/images/default-user.png';

                    res.status(200)
                      .json({
                        status: 1,
                        url: url

                      });
                  } else {
                    res.status(200)
                      .json({
                        status: 1,
                        url: process.env.FULL_PATH + '/profile_pic/' + data1[0].profile_pic

                      });
                  }

                } else {
                  res.status(200)
                    .json({
                      status: 1,
                    });
                }
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })


          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      } else {
        await userModel.update_customer(userobj, req.user.customer_id)
          .then(async function (data) {

            await userModel.get_user(req.user.customer_id)
              .then(async function (data1) {
                if (Config.activate_sales_force == true) {
                  let customerDetails = await customerModel.get_sales_force_customer(req.user.customer_id);
                  let companyDetailsUpdated = await customerModel.get_company_details(customerDetails[0].company_id);
                  let countryName = await customerModel.country_name(customerDetails[0].country_id);
                  const sales_data_cust = {
                    first_name: entities.encode(customerDetails[0].first_name),
                    last_name: entities.encode(customerDetails[0].last_name),
                    country_name: `${countryName}`,
                    company_id: `${companyDetailsUpdated[0].company_id}`,
                    sales_company_id: (companyDetailsUpdated[0].sales_company_id != null) ? `${companyDetailsUpdated[0].sales_company_id}` : '',
                    email: entities.encode(customerDetails[0].email),
                    phone_number: `${entities.encode(customerDetails[0].phone_no)}`,
                    customer_id: `${customerDetails[0].cqt_customer_id}`
                  }

                  sales_data_cust.status = customerDetails[0].status;
                  sales_data_cust.activated = customerDetails[0].activated;
                  sales_data_cust.sales_customer_id = customerDetails[0].sales_customer_id;

                  //console.log('sales_data_cust', sales_data_cust);
                  let sales_response_cust = await outwardCrm.update_customer(sales_data_cust);
                  if (sales_response_cust.status == 1) {
                    //DO NOTHING
                  } else {
                    common.outwardCrmError(sales_response_cust.error, `Error Crm While Updating Customer ${Config.drupal.url}`, `Customer ID:${req.user.customer_id}`);
                  }
                }

                if (data1[0].profile_pic != null && file_name != null) {
                  if (process.env.NODE_ENV == 'production') {
                    let url = '';
                    s3_bucket_status = await module.exports.__get_signed_url(`${data1[0].profile_pic}`);
                    if (s3_bucket_status.status === 1)
                      url = s3_bucket_status.url;
                    else
                      url = process.env.FULL_PATH + '/images/default-user.png';

                    res.status(200)
                      .json({
                        status: 1,
                        url: url

                      });
                  } else {
                    res.status(200)
                      .json({
                        status: 1,
                        url: process.env.FULL_PATH + '/profile_pic/' + data1[0].profile_pic

                      });
                  }
                } else {
                  res.status(200)
                    .json({
                      status: 1,
                    });
                }
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              })


          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Account
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Account used for Account selection
   */
  my_account: async (req, res, next) => {
    try {

      if (req.user.role == 1) {
        await userModel.get_user(req.user.customer_id)
          .then(function (data) {
            // if(data[0].profile_pic != null)
            // {
            // data[0].url = process.env.FULL_PATH+':'+process.env.PORT+'/profile_pic/'+data[0].profile_pic
            // }
            data[0].first_name = entities.decode(data[0].first_name);
            data[0].last_name = entities.decode(data[0].last_name);
            data[0].email = entities.decode(data[0].email);


            res.status(200)
              .json({
                status: 1,
                data: data
              });
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      } else {
        await agentModel.get_user(req.user.customer_id)
          .then(async function (data) {
            let agent_company = await agentModel.list_company(req.user.customer_id);
            let company_name_arr = [];
            if (agent_company.length > 0) {
              for (let index = 0; index < agent_company.length; index++) {
                company_name_arr.push(agent_company[index].company_name);
              }
            }

            data[0].company_name = company_name_arr;
            // if(data[0].profile_pic != null)
            // {
            // data[0].url = process.env.FULL_PATH+':'+process.env.PORT+'/profile_pic/'+data[0].profile_pic
            // }

            res.status(200)
              .json({
                status: 1,
                data: data
              });
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Delete Profile Picture
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Delete Profile Picture used for Delete Profile Picture selection
   */
  delete_prpfile_pic: async (req, res, next) => {
    try {
      if (req.user.role == 2) {
        await agentModel.remove_profile_pic(req.user.customer_id)
          .then(function (data) {
            res.status(200)
              .json({
                status: 1,
                url: process.env.FULL_PATH + '/images/default-user.png'

              });
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      } else {
        await userModel.remove_profile_pic(req.user.customer_id)
          .then(function (data) {
            res.status(200)
              .json({
                status: 1,
                url: process.env.FULL_PATH + '/images/default-user.png'

              });
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Token for customer / agent
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Generate Token for customer / agent used for Generate Token for customer / agent selection
   */
  generateToken: async (req, res, next) => {
    try {
      if (req.user.role == 2) {
        await agentModel.get_user(req.user.customer_id)
          .then(async function (data) {
            // console.log("Test");
            // return false;
            let profile_url = null;
            let choose = 0;
            if (data[0].profile_pic != null) {
              if (process.env.NODE_ENV == 'development') {
                profile_url = process.env.FULL_PATH + '/profile_pic/' + data[0].profile_pic
              } else {
                s3_bucket_status = await module.exports.__get_signed_url(`${data[0].profile_pic}`);
                if (s3_bucket_status.status === 1)
                  profile_url = s3_bucket_status.url;
                else
                  profile_url = process.env.FULL_PATH + '/images/default-user.png';
              }

              choose = 0;
            } else {
              profile_url = process.env.FULL_PATH + '/images/default-user.png';
              choose = 1;
            }

            let user_data = {
              user_id: cryptr.encrypt(req.user.customer_id),
              lang: data[0].language_code,
              user_agent: cryptr.encrypt(req.get('User-Agent')),
              name: entities.decode(data[0].first_name + " " + data[0].last_name),
              new_feature: data[0].new_feature,
              new_feature_send_mail: data[0].new_feature_send_mail,
              new_feature_fa: data[0].new_feature_fa,
              tour_counter: data[0].tour_counter,
              tour_counter_new: data[0].tour_counter_new,
              tour_counter_closed_task: data[0].tour_counter_closed_task,
              new_feature_11: data[0].new_feature_11,
              tour_counter_11: data[0].tour_counter_11,
              multi_lang_access: data[0].multi_lang_access,
              email: entities.decode(data[0].email),
              mobile: data[0].phone_no,
              date_added: data[0].date_added,
              profile_pic: profile_url,
              role: 2,
              choose: choose
            }

            //console.log(user_data);              
            const token = await loginToken(user_data);
            res.status(200).json({
              status: 1,
              token: token
            }).end();
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      } else {
        await userModel.get_user(req.user.customer_id)
          .then(async function (data) {
            // console.log("Test");
            // return false;
            let profile_url = null;
            let choose = 0;
            if (data[0].profile_pic != null) {
              if (process.env.NODE_ENV == 'development') {
                profile_url = process.env.FULL_PATH + '/profile_pic/' + data[0].profile_pic
              } else {
                //console.log("Profile_pic",data[0].profile_pic);
                s3_bucket_status = await module.exports.__get_signed_url(`${data[0].profile_pic}`);

                if (s3_bucket_status.status === 1)
                  profile_url = s3_bucket_status.url;
                else
                  profile_url = process.env.FULL_PATH + '/images/default-user.png';

              }

              choose = 0;
            } else {
              profile_url = process.env.FULL_PATH + '/images/default-user.png';
              choose = 1;
            }
            let user_role_finance = await userModel.checkUserRoleFinance(req.user.customer_id);
            let user_data = {
              user_id: cryptr.encrypt(req.user.customer_id),
              lang: data[0].language_code,
              user_agent: cryptr.encrypt(req.get('User-Agent')),
              name: entities.decode(data[0].first_name + " " + data[0].last_name),
              new_feature: data[0].new_feature,
              new_feature_send_mail: data[0].new_feature_send_mail,
              new_feature_fa: data[0].new_feature_fa,
              tour_counter: data[0].tour_counter,
              tour_counter_new: data[0].tour_counter_new,
              tour_counter_closed_task: data[0].tour_counter_closed_task,
              new_feature_11: data[0].new_feature_11,
              tour_counter_11: data[0].tour_counter_11,
              multi_lang_access: data[0].multi_lang_access,
              email: entities.decode(data[0].email),
              mobile: data[0].phone_no,
              date_added: data[0].date_added,
              profile_pic: profile_url,
              role: 1,
              choose: choose,
              show_stock_enquiry: (data[0].show_stock_enquiry != undefined) ? data[0].show_stock_enquiry : 0,
              role_finance: user_role_finance,
              pre_activated: data[0].pre_activated,
            }

            console.log(user_data);
            const token = await loginToken(user_data);
            res.status(200).json({
              status: 1,
              token: token
            }).end();
          }).catch(err => {
            common.logError(err);
            res.status(400).json({
              status: 3,
              message: Config.errorText.value
            }).end();
          })
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a change Language
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - change Language used for Language selection
   */
  changeLanguage: async (req, res, next) => {
    try {
      const {
        role
      } = req.user;
      if (role == 2) {
        await agentModel.update_agent_language('en', req.user.customer_id)
      } else {
        await userModel.update_customer_language('en', req.user.customer_id)
      }

      res.status(200)
        .json({
          status: 1
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }

  },
  /**
   * Compare previous password with the new one
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Valid Password used for Password selection
   */
  isValidPassword: async function (newPassword, existingPassword) {
    try {
      return await bcrypt.compare(newPassword, existingPassword);
    } catch (error) {
      throw new Error(error);
    }
  },
  // _generate_token : async(req)=>{
  //   try{
  //     console.log("User",req.user.role);
  //     if(req.user.role == 2)
  //     {
  //       console.log("Agent",req.user);
  //       await agentModel.get_user(req.user.customer_id)
  //     .then(function(data){
  //       // console.log("Test");
  //       // return false;
  //       let profile_url = null;
  //       let choose = 0;
  //       if(data[0].profile_pic != null)
  //       {
  //         profile_url = process.env.FULL_PATH+':'+process.env.PORT+'/profile_pic/'+data[0].profile_pic
  //         choose = 0;
  //       }
  //       else
  //       {
  //         profile_url = process.env.FULL_PATH+':'+process.env.PORT+'/images/default-user.png';
  //         choose = 1;
  //       }

  //       let user_data ={ 
  //                       user_id:cryptr.encrypt(req.user.customer_id),
  //                       user_agent:cryptr.encrypt(req.user.user_agent),
  //                       name:data[0].first_name+" "+data[0].last_name,
  //                       email : data[0].email,
  //                       mobile : data[0].phone_no,
  //                       profile_pic : profile_url,
  //                       role : 2,
  //                       choose : choose
  //                     }

  //       //console.log(user_data);              
  //       const token  = loginToken(user_data);
  //       return token;
  //     }).catch(err=> {
  //       return false;
  //     })
  //     }
  //     else
  //     {

  //       await userModel.get_user(req.user.customer_id)
  //       .then(async function(data){
  //         let profile_url = null;
  //         let choose = 0;
  //         if(data[0].profile_pic != null)
  //         {
  //           profile_url = process.env.FULL_PATH+':'+process.env.PORT+'/profile_pic/'+data[0].profile_pic
  //           choose = 0;
  //         }
  //         else
  //         {
  //           profile_url = process.env.FULL_PATH+':'+process.env.PORT+'/images/default-user.png';
  //           choose = 1;
  //         }

  //         let user_data ={ 
  //                         user_id:cryptr.encrypt(req.user.customer_id),
  //                         user_agent:cryptr.encrypt(req.user.user_agent),
  //                         name:data[0].first_name+" "+data[0].last_name,
  //                         email : data[0].email,
  //                         mobile : data[0].phone_no,
  //                         profile_pic : profile_url,
  //                         role : 1,
  //                         choose : choose
  //                       }

  //         let token  = await loginToken(user_data);
  //         console.log("Token",token); 
  //         return token;


  //       }).catch(err=> {
  //         return false;
  //       })
  //     }

  //   }catch(err){
  //     return false;
  //   }
  // },
  /**
   * Generates a Check Roles
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Check roles used for roles selection
   */
  __check_roles: async (customer_id) => {
    let customer_details = await userModel.get_user(customer_id);
    let ret_arr = [];
    //ROLE 0 => Is Admin YES
    if (customer_details[0].role_type == 0) {

      let company_arr = await module.exports.__get_company_list(customer_details[0].company_id, 1, null);

      let sel_cust_arr = await userModel.get_user_company(company_arr);
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element.customer_id);
      }

      //ROLE TYPE 1 => Is Admin NO, Role Type ADMIN  
    } else if (customer_details[0].role_type == 1) {

      let company_arr = await module.exports.__get_company_list(customer_details[0].company_id, 1, null);

      let sel_roles_arr = await userModel.get_user_role(customer_id);
      let role_arr = [];
      for (let index = 0; index < sel_roles_arr.length; index++) {
        const element = sel_roles_arr[index];
        role_arr.push(element.role_id);
      }

      let sel_cust_arr = await userModel.get_user_company_role(company_arr, role_arr);
      for (let index = 0; index < sel_cust_arr.length; index++) {
        const element = sel_cust_arr[index];
        ret_arr.push(element.customer_id);
      }

      //ROLE TYPE 2 => Is Admin NO, Role Type REGULAR  
    } else if (customer_details[0].role_type == 2) {
      ret_arr.push(customer_id);
    }

    return ret_arr;

  },
  /**
   * Generates a Company List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Company list used for company selection
   */
  __get_company_list: async (company_id, role, agent_id) => {
    //console.log('company_id',company_id);
    let parent_company_id = await userModel.get_parent_company(company_id);

    let company_arr = [];
    if (parent_company_id > 0) {
      let associate_company = await userModel.get_child_company(parent_company_id);
      let in_agent_arr = [];
      if (role == 2) {
        let agent_company_list = await agentModel.list_company(agent_id);
        for (let index = 0; index < agent_company_list.length; index++) {
          const element = agent_company_list[index];
          in_agent_arr.push(element.company_id);
        }
      }
      for (let index = 0; index < associate_company.length; index++) {
        const element = associate_company[index];
        if (role == 2) {
          if (in_agent_arr.length > 0 && common.inArray(element.company_id, in_agent_arr)) {
            company_arr.push(element.company_id);
          }
        } else {
          company_arr.push(element.company_id);
        }
      }
    } else {
      company_arr.push(company_id);
    }
    return company_arr;
  },
  /**
   * Generates a Notification List Count
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Notification List Count used for Notification selection
   */
  count_notification: async (req, res, next) => {
    try {

      const customer_id = req.user.customer_id;
      const role = (req.user.role == 1) ? 'C' : 'A';

      await userModel.countCustomerNotification(customer_id, role)
        .then(async function (data) {

          res.status(200).json({
            status: "1",
            counter: parseInt(data)
          });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Notification List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Notification List used for Notification selection
   */
  list_notification: async (req, res, next) => {
    try {

      const customer_id = req.user.customer_id;
      const role = (req.user.role == 1) ? 'C' : 'A';

      await userModel.fetchCustomerNotification(customer_id, role)
        .then(async function (data) {

          res.status(200).json({
            status: "1",
            data: data
          });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Update Notification
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Update Notification used for Notification selection
   */
  update_notification: async (req, res, next) => {
    try {

      const customer_id = req.user.customer_id;
      const role = req.user.role == 1 ? 'C' : 'A';
      const {
        notification_id,
        task_id
      } = req.body

      await userModel.updateCustomerNotification(customer_id, notification_id, task_id, role)
        .then(async function (data) {

          //let ncounter = await userModel.countCustomerNotification( customer_id, role )
          //let notifications = await userModel.fetchCustomerNotification( customer_id, role )

          res.status(200).json({
            status: "1"
          });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Shadow login
   * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Shadow login used for login selection
   */
  verify_shadow_token: async (req, res, next) => {
    try {
      if (req.user.customer_id > 0) {

        if (req.user.role == 2) {
          await agentModel.get_user(req.user.customer_id)
            .then(async function (data) {
              // console.log("Test");
              // return false;
              let profile_url = null;
              let choose = 0;
              if (data[0].profile_pic != null) {
                if (process.env.NODE_ENV == 'development') {
                  profile_url = process.env.FULL_PATH + '/profile_pic/' + data[0].profile_pic
                } else {
                  s3_bucket_status = await module.exports.__get_signed_url(`${data[0].profile_pic}`);
                  if (s3_bucket_status.status === 1)
                    profile_url = s3_bucket_status.url;
                  else
                    profile_url = process.env.FULL_PATH + '/images/default-user.png';
                }

                choose = 0;
              } else {
                profile_url = process.env.FULL_PATH + '/images/default-user.png';
                choose = 1;
              }

              let user_data = {
                user_id: cryptr.encrypt(req.user.customer_id),
                lang: data[0].language_code,
                empe_id: cryptr.encrypt(req.user.empe),
                user_agent: cryptr.encrypt(req.get('User-Agent')),
                name: entities.decode(data[0].first_name + " " + data[0].last_name),
                new_feature: data[0].new_feature,
                new_feature_send_mail: data[0].new_feature_send_mail,
                new_feature_fa: data[0].new_feature_fa,
                tour_counter: data[0].tour_counter,
                tour_counter_new: data[0].tour_counter_new,
                tour_counter_closed_task: data[0].tour_counter_closed_task,
                new_feature_11: 2,
                tour_counter_11: 4,
                multi_lang_access: data[0].multi_lang_access,
                email: entities.decode(data[0].email),
                mobile: data[0].phone_no,
                date_added: data[0].date_added,
                profile_pic: profile_url,
                role: 2,
                choose: choose
              }

              //console.log(user_data);              
              const token = await loginToken(user_data);
              res.status(200).json({
                status: 1,
                token: token
              }).end();
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          await userModel.get_user(req.user.customer_id)
            .then(async function (data) {
              // console.log("Test");
              // return false;
              let profile_url = null;
              let choose = 0;
              if (data[0].profile_pic != null) {
                if (process.env.NODE_ENV == 'development') {
                  profile_url = process.env.FULL_PATH + '/profile_pic/' + data[0].profile_pic
                } else {
                  //console.log("Profile_pic",data[0].profile_pic);
                  s3_bucket_status = await module.exports.__get_signed_url(`${data[0].profile_pic}`);

                  if (s3_bucket_status.status === 1)
                    profile_url = s3_bucket_status.url;
                  else
                    profile_url = process.env.FULL_PATH + '/images/default-user.png';

                }

                choose = 0;
              } else {
                profile_url = process.env.FULL_PATH + '/images/default-user.png';
                choose = 1;
              }
              let user_role_finance = await userModel.checkUserRoleFinance(req.user.customer_id);
              let user_data = {
                user_id: cryptr.encrypt(req.user.customer_id),
                lang: data[0].language_code,
                empe_id: cryptr.encrypt(req.user.empe),
                user_agent: cryptr.encrypt(req.get('User-Agent')),
                name: entities.decode(data[0].first_name + " " + data[0].last_name),
                new_feature: data[0].new_feature,
                new_feature_send_mail: data[0].new_feature_send_mail,
                new_feature_fa: data[0].new_feature_fa,
                tour_counter: data[0].tour_counter,
                tour_counter_new: data[0].tour_counter_new,
                tour_counter_closed_task: data[0].tour_counter_closed_task,
                new_feature_11: 2,
                tour_counter_11: 4,
                multi_lang_access: data[0].multi_lang_access,
                email: entities.decode(data[0].email),
                mobile: data[0].phone_no,
                date_added: data[0].date_added,
                profile_pic: profile_url,
                role: 1,
                choose: choose,
                show_stock_enquiry: (data[0].show_stock_enquiry != undefined) ? data[0].show_stock_enquiry : 0,
                role_finance: user_role_finance
              }

              //console.log(user_data);              
              const token = await loginToken(user_data);
              console.log("token : ", token);
              res.status(200).json({
                status: 1,
                token: token
              }).end();
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        }

        // let profile_url = null;
        // let choose = 0;
        // if (req.user.profile_pic != null) {
        //   if (process.env.NODE_ENV == 'development') {
        //     profile_url = process.env.FULL_PATH + '/profile_pic/' + req.user.profile_pic;
        //   } else {
        //     s3_bucket_status = await module.exports.__get_signed_url(`${req.user.profile_pic}`);
        //     if (s3_bucket_status.status === 1)
        //       profile_url = s3_bucket_status.url;
        //     else
        //       profile_url = process.env.FULL_PATH + '/images/default-user.png';
        //   }

        //   choose = 0;
        // } else {
        //   profile_url = process.env.FULL_PATH + '/images/default-user.png';
        //   choose = 1;
        // }

        // console.log(req.user.customer_id,req.user.role);

        // let customer_data = await userModel.get_user(req.user.customer_id);
        // let lang = customer_data[0].language_code;
        // let user_data = {
        //   user_id: cryptr.encrypt(req.user.customer_id),
        //   lang: lang,
        //   empe_id: cryptr.encrypt(req.user.empe),
        //   user_agent: cryptr.encrypt(req.get('User-Agent')),
        //   name: entities.decode(req.user.first_name + " " + req.user.last_name),
        //   new_feature: customer_data[0].new_feature,
        //   new_feature_send_mail: customer_data[0].new_feature_send_mail,
        //   new_feature_fa:customer_data[0].new_feature_fa,
        //   tour_counter: customer_data[0].tour_counter,
        //   multi_lang_access: customer_data[0].multi_lang_access,
        //   email: entities.decode(req.user.email),
        //   mobile: req.user.phone_no,
        //   date_added: req.user.date_added,
        //   role: req.user.role,
        //   profile_pic: profile_url,
        //   choose: choose
        // }

        // const token = await loginToken(user_data);
        // if (!req.user.role) {
        //   common.logError(err);
        //   return res.status(400).json({
        //     status: 3,
        //     message: Config.errorText.value
        //   })
        // } else {
        //   return res.status(200)
        //     .json({
        //       status: 1,
        //       token: token
        //     })
        // }
      } else {
        let err_data = {
          password: "Invalid login details"
        };
        return res.status(400).json({
          status: 2,
          errors: err_data
        });
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Search Company
   *@author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search Company used for company selection
   */
  search_my_company: async (req, res, next) => {
    try {
      let query_string = '';
      if (req.params.keyword && req.params.keyword != '') {
        query_string = entities.encode(trim(req.params.keyword));
      }

      await userModel.saerch_user_company(query_string)
        .then(async function (data) {
          let decoded_arr = [];
          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            decoded_arr.push({
              company_id: element.company_id,
              company_name: entities.decode(element.company_name)
            });

          }

          res.status(200).json({
            status: "1",
            data: decoded_arr
          });
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  /**
   * Generates a Check Token
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Check Token used for Token selection
   */
  token_check: async (req, res, next) => {
    const {
      token
    } = req.body;
    const user = {
      token: entities.encode(token)
    }
    try {
      var decodedToken = jwtSimple.decode(user.token, Config.jwt.secret);
    } catch (error) {
      return res.status(400).json({
        status: 3,
        message: "Token is invalid"
      });

      //if(error.message == "Signature verification failed") resolve(false);
    }
    let err = {};
    let userExist = await userModel.findByUserId(decodedToken.checksum);
    let agentExist = await agentModel.findByUserId(decodedToken.checksum);
    if (agentExist.length > 0 && agentExist[0].activated == 1) {
      if (agentExist[0].token == user.token) {
        if (decodedToken.time > new Date().getTime()) {
          return res.status(200).json({
            status: 1,
            message: ""
          });
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token expired please resend link"
          });
        }
      } else {
        return res.status(400).json({
          status: 3,
          message: "Token already used"
        });
      }

    } else if (userExist.length > 0 && userExist[0].activated == 1) {
      if (userExist[0].token == user.token) {
        /*  console.log("Time", decodedToken.time);
         console.log("TimeNOW", new Date().getTime()); */

        if (decodedToken.time > new Date().getTime()) {
          return res.status(200).json({
            status: 1,
            message: ""
          });
        } else {
          return res.status(400).json({
            status: 3,
            message: "Token expired please resend link"
          });
        }
      } else {
        return res.status(400).json({
          status: 3,
          message: "Token already used"
        });
      }

    } else {
      return res.status(400).json({
        status: 3,
        message: "User does not exist or not active"
      });
    }

  },
  /**
   * Generates a Set password using token
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Set password using token used for password selection
   */
  token_check_set_pass: async (req, res, next) => {
    const {
      token
    } = req.body;
    const user = {
      token: entities.encode(token)
    }
    try {
      var decodedToken = jwtSimple.decode(user.token, Config.jwt.secret);
    } catch (error) {
      return res.status(400).json({
        status: 3,
        message: "Token is invalid"
      });
    }
    let err = {};
    let userExist = await userModel.findByUserId(decodedToken.checksum);
    let agentExist = await agentModel.findByUserId(decodedToken.checksum);

    //console.log('userExist',userExist)
    //console.log('agentExist',agentExist)

    if (agentExist.length > 0) {
      if (agentExist[0].token == user.token) {
        if (decodedToken.time > new Date().getTime()) {
          return res.status(200).json({
            status: 1,
            lang: agentExist[0].language_code,
            message: ""
          });
        } else {
          return res.status(400).json({
            status: 3,
            lang: agentExist[0].language_code,
            message: "Token expired please resend link"
          });
        }
      } else {
        return res.status(400).json({
          status: 3,
          lang: agentExist[0].language_code,
          message: "Token already used"
        });
      }

    } else if (userExist.length > 0) {
      if (userExist[0].token == user.token) {
        if (decodedToken.time > new Date().getTime()) {
          return res.status(200).json({
            status: 1,
            lang: userExist[0].language_code,
            message: ""
          });
        } else {
          return res.status(400).json({
            status: 3,
            lang: userExist[0].language_code,
            message: "Token expired please resend link"
          });
        }
      } else {
        return res.status(400).json({
          status: 3,
          lang: userExist[0].language_code,
          message: "Token already used"
        });
      }

    } else {
      return res.status(400).json({
        status: 3,
        message: "User does not exist or not active"
      });
    }

  },

  /**
   * Generates a Payment List
   * @author Debadrita Ghosh
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Payment list used for payment selection
   */

  list_my_payment: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalLimit;
        var offset = (page - 1) * globalLimit;
      } else {
        var limit = globalLimit;
        var offset = 0;
      }

      let query_arr = {};
      if (req.query.s && query_arr.s != '') {
        query_arr.s = entities.encode(trim(req.query.s));
      }

      if (req.query.fdt && req.query.fdt != '') {
        query_arr.date_from = entities.encode(trim(req.query.fdt));
      } else {
        query_arr.date_from = null;
      }

      if (req.query.tdt && req.query.tdt != '') {
        query_arr.date_to = entities.encode(trim(req.query.tdt));
      } else {
        query_arr.date_to = null;
      }

      if (req.user.role == 2) {
        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }
        if (customer_arr.length > 0) {
          await agentModel.payment_list(req.user.customer_id, customer_arr, query_arr, limit, offset)
            .then(async function (data) {
              for (let indp = 0; indp < data.length; indp++) {
                const element = data[indp];
                data[indp].ref_name = entities.decode(element.ref_name);
                const invoice_items = await userModel.get_payment_items_total(element.invoice_number);

                if (invoice_items.length > 0 && invoice_items[0].billed_quantity != null) {
                  data[indp].billed_quantity = parseFloat(invoice_items[0].billed_quantity);
                  data[indp].invoice_value = (parseFloat(invoice_items[0].net_value) + parseFloat(invoice_items[0].tax));
                  data[indp].uom = "-"; //invoice_items[0].uom;
                } else {
                  data[indp].billed_quantity = '';
                  data[indp].invoice_value = 0;
                  data[indp].uom = "";
                }
              }
              let count_open_orders = await agentModel.count_payment_list(req.user.customer_id, customer_arr, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_orders
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }

      } else {

        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        //console.log('customer_arr',customer_arr);
        if (customer_arr.length > 0) {
          await userModel.payment_list(customer_arr, req.user.customer_id, query_arr, limit, offset)
            .then(async function (data) {
              for (let indp = 0; indp < data.length; indp++) {
                const element = data[indp];
                data[indp].ref_name = entities.decode(element.ref_name);
                const invoice_items = await userModel.get_payment_items_total(element.invoice_number);

                if (invoice_items.length > 0 && invoice_items[0].billed_quantity != null) {
                  data[indp].billed_quantity = parseFloat(invoice_items[0].billed_quantity);
                  data[indp].invoice_value = (parseFloat(invoice_items[0].net_value) + parseFloat(invoice_items[0].tax));
                  data[indp].uom = "-"; //invoice_items[0].uom;
                } else {
                  data[indp].billed_quantity = '';
                  data[indp].invoice_value = 0;
                  data[indp].uom = '';
                }
              }

              let count_open_payment = await userModel.count_payment_list(customer_arr, req.user.customer_id, query_arr);

              res.status(200).json({
                status: "1",
                data: data,
                count: count_open_payment
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: [],
            count: 0
          });
        }
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  /**
   * Generates a Search Payment List
   * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Search Payment List used for Search Payment selection
   */
  search_my_payment: async (req, res, next) => {
    try {
      let query_string = entities.encode(trim(req.params.keyword));
      if (req.user.role == 2) {

        var company_id = entities.encode(req.query.cid);

        let company_arr = await module.exports.__get_company_list(company_id, req.user.role, req.user.customer_id);

        let db_cust_arr = await agentModel.get_company_customers(company_arr);
        let customer_arr = [];
        for (let index = 0; index < db_cust_arr.length; index++) {
          const element = db_cust_arr[index];
          customer_arr.push(element.customer_id);
        }

        if (customer_arr.length > 0) {
          await agentModel.payment_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      } else {
        //Customer Arr
        let customer_arr = await module.exports.__check_roles(req.user.customer_id);
        if (customer_arr.length > 0) {
          await userModel.payment_search(customer_arr, req.user.customer_id, query_string)
            .then(async function (data) {

              res.status(200).json({
                status: "1",
                data: data
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else {
          res.status(200).json({
            status: "1",
            data: []
          });
        }
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  /**
   * Generates a Get S3 signed url
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} req - HTTP request argument to the middleware function
   * @param {Object} res - HTTP response argument to the middleware function
   * @param {next} next  - Callback argument to the middleware function
   * @return {Json Object} - Get S3 signed url used for S3 signed url selection
   */
  __get_signed_url: async (key) => {
    // try{

    return new Promise((resolve, reject) => {
      // AWS.config.update({ accessKeyId: Config.aws.accessKeyId, secretAccessKey: Config.aws.secretAccessKey });
      // var s3 = new AWS.S3()
      // const s3Config = new AWS.S3({
      //   accessKeyId: Config.aws.accessKeyId,
      //   secretAccessKey: Config.aws.secretAccessKey,
      //   Bucket: Config.aws.bucketName
      // });
      //12 hours
      params = {
        Bucket: Config.aws.bucketName,
        Key: key,
        Expires: 43200
      }

      AWS.config.update({
        accessKeyId: Config.aws.accessKeyId,
        secretAccessKey: Config.aws.secretAccessKey
      });

      AWS.config.region = Config.aws.regionName;
      var s3 = new AWS.S3();
      s3.getSignedUrl('getObject', params, function (err, url) {
        if (err) {
          resolve({
            status: 3,
            message: err
          });
        } else {
          resolve({
            status: 1,
            url: url
          });
        }
        //console.log('Signed URL: ' + url);
      });


      // var head_params = { Bucket: 'ccpattachement', Key: key };
      // var url_params = { Bucket: 'ccpattachement', Key: key, expires: 60 };
      // s3.headObject(head_params, function (err, data) {
      //   if (err) {
      //     console.log("Error", err);
      //     resolve({ status: 3, message: err });
      //     // return res.json{status : 3,message:err};//(err,err.code); // an error occurred
      //   }
      //   else {
      //     s3.getSignedUrl('getObject', url_params, function (err, url) {
      //       if (err) {
      //         console.log('error: ' + err);
      //         resolve({ status: 3, message: err });
      //       } else {
      //         console.log('Signed URL: ' + url);
      //         resolve({ status: 3, url: url });
      //       }
      //     });
      //   }           // successful response
      // });
    })

    // } catch (err) {
    //   common.logError(err);
    //   res.status(400).json({
    //     status: 2,
    //     message: Config.errorText.value
    //   }).end();
    // }


    // s3.headObject(head_params).on('success', function(response) {
    //   console.log("Error2");
    //   s3.getSignedUrl('getObject', url_params, function (err, url) {
    //       if (err) {
    //         console.log("Error");
    //         return {status : 3,message:'Cannot generate URL'}; 
    //       } else {
    //         console.log("Success");
    //         return {status : 1,url:url};
    //       }
    //   }); 

    //   // console.log("Key was", response.request.params.Key);
    // }).on('error',function(error){
    //   console.log("Error1");
    //   return {status : 3,message:'File does not exist'};
    // });

    //return ret_arr;

  },
  /**
  * Customer Response Approve / Reject
  * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
  * @param {Object} req - HTTP request argument to the middleware function
  * @param {Object} res - HTTP response argument to the middleware function
  * @param {next} next  - Callback argument to the middleware function
  * @return {Json Object} - Success or Error with message
  */

  process_customer_response: async (req, res, next) => {
    try {
      const user = req.user;
      let today = common.currentDateTime();
      if (user && user.customer_id > 0) {
        if (user.payload.cr == 2) {
          const token = JWT.sign({
            iss: user.payload.iss,
            cstm: user.payload.cstm,
            tyds: user.payload.tyds,
            tyti: user.payload.tyti,
            cr: user.payload.cr,
            prcr: user.payload.prcr,
            need_form: 1, // set need form 1 to show form else 0 to complete journey
            iat: user.payload.iat,
            exp: user.payload.exp,
          }, Config.jwt.secret);
          res.status(200).json({
            status: 1,
            need_form: 1, // set need form 1 to show form else 0 to complete journey
            token: token,
            message: "Document Successfully Accessed.",
          });
        } else if (user.payload.cr == 1 && user.payload.tyti) {
          if (user.payload.tyds == 'I') {
            await taskModel.updateShipInvoiceById(user.payload.tyti, 1, user.customer_id, today)
              .then(async function (data) {
                let invoice_detail_info = await taskModel.getShipInvoiceById(user.payload.tyti, 1);
                if (invoice_detail_info.length > 0) {
                  let emp_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 5);
                  let bm_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 1);
                  if (emp_details.length > 0) {

                    let attachmentArr = [];
                    // Mail attached of invoice file.
                    if (invoice_detail_info[0].invoice_file_path != '' && invoice_detail_info[0].invoice_file_name != '') {
                      let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].invoice_file_path, invoice_detail_info[0].invoice_file_name);

                      if (base_64_stream.status == 1) {
                        attachmentArr.push({
                          filename: invoice_detail_info[0].invoice_file_name,
                          content: base_64_stream.base64,
                          encoding: "base64"
                        });
                      }
                    }

                    // Mail attached of package file.   
                    if (invoice_detail_info[0].packing_file_path != '' && invoice_detail_info[0].packing_file_name != '') {
                      let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].packing_file_path, invoice_detail_info[0].packing_file_name);

                      if (base_64_stream.status == 1) {
                        attachmentArr.push({
                          filename: invoice_detail_info[0].packing_file_name,
                          content: base_64_stream.base64,
                          encoding: "base64"
                        });
                      }
                    }
                    let mail_cc = '';
                    if (bm_details.length > 0) {
                      mail_cc = bm_details[0].email;
                    }
                    if (Config.environment != 'production') {
                      mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                    }

                    let mailToParam = {
                      task_ref: invoice_detail_info[0].task_ref,
                      emp_first_name: entities.decode(emp_details[0].first_name),
                      emp_last_name: entities.decode(emp_details[0].last_name),
                      first_name: entities.decode(invoice_detail_info[0].first_name),
                      last_name: entities.decode(invoice_detail_info[0].last_name),
                      company_name: entities.decode(invoice_detail_info[0].company_name),
                      product_name: entities.decode(invoice_detail_info[0].product_name),
                      rdd: (invoice_detail_info[0].rdd != null && invoice_detail_info[0].rdd != '') ? common.changeDateFormat(invoice_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                      po_number: invoice_detail_info[0].po_no,
                      sales_order_no: invoice_detail_info[0].sales_order_no,
                      status: 'accepted'
                    };

                    const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PROFORMA_INVOICE', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

                  }
                }
                res.status(200).json({
                  status: 1,
                  message: "Document Approved Successfully.",
                });
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              });
          } else if (user.payload.tyds == 'C') {
            await taskModel.updateShipCOAById(user.payload.tyti, 1, user.customer_id, today)
              .then(async function (data) {
                let coa_detail_info = await taskModel.getShipCOAById(user.payload.tyti, 1);
                if (coa_detail_info.length > 0) {
                  let emp_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 5);
                  let bm_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 1);
                  if (emp_details.length > 0) {

                    let attachmentArr = [];
                    // Mail attached of COA file.
                    if (coa_detail_info[0].coa_file_path != '' && coa_detail_info[0].coa_file != '') {
                      let base_64_stream = await module.exports.__get_base64(coa_detail_info[0].coa_file_path, coa_detail_info[0].coa_file);

                      if (base_64_stream.status == 1) {
                        attachmentArr.push({
                          filename: coa_detail_info[0].coa_file,
                          content: base_64_stream.base64,
                          encoding: "base64"
                        });
                      }
                    }

                    let mail_cc = '';
                    if (bm_details.length > 0) {
                      mail_cc = bm_details[0].email;
                    }
                    if (Config.environment != 'production') {
                      mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                    }

                    let mailToParam = {
                      task_ref: coa_detail_info[0].task_ref,
                      emp_first_name: entities.decode(emp_details[0].first_name),
                      emp_last_name: entities.decode(emp_details[0].last_name),
                      first_name: entities.decode(coa_detail_info[0].first_name),
                      last_name: entities.decode(coa_detail_info[0].last_name),
                      company_name: entities.decode(coa_detail_info[0].company_name),
                      product_name: entities.decode(coa_detail_info[0].product_name),
                      rdd: (coa_detail_info[0].rdd != null && coa_detail_info[0].rdd != '') ? common.changeDateFormat(coa_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                      invoice_number: coa_detail_info[0].invoice_number,
                      po_number: coa_detail_info[0].po_no,
                      sales_order_no: coa_detail_info[0].sales_order_number,
                      status: 'accepted'
                    };

                    const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PRESHIPMENT_COA', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

                  }
                }
                res.status(200).json({
                  status: 1,
                  message: "Document Approved Successfully.",
                });
              }).catch(err => {
                common.logError(err);
                res.status(400).json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
              });
          } else {
            let return_err = {
              status: 5,
              message: "Invalid Access."
            };
            return res.status(401).json(return_err);
          }

        }
      } else {
        let return_err = {
          status: 5,
          message: "Invalid Access."
        };
        return res.status(401).json(return_err);
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  /**
* Customer Response Rejection
* @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
* @param {Object} req - HTTP request argument to the middleware function
* @param {Object} res - HTTP response argument to the middleware function
* @param {next} next  - Callback argument to the middleware function
* @return {Json Object} - Success or Error with message
*/

  process_customer_rejection: async (req, res, next) => {
    try {
      const user = req.user;
      const {
        reason_of_rejection
      } = req.body;
      let today = common.currentDateTime();

      if (user && user.customer_id > 0 && req.user.payload.tyti) {
        if (user.payload.tyds == 'I') {
          await taskModel.updateShipInvoiceById(req.user.payload.tyti, 2, user.customer_id, today, entities.encode(reason_of_rejection))
            .then(async function (data) {
              let invoice_detail_info = await taskModel.getShipInvoiceById(user.payload.tyti, 2);
              if (invoice_detail_info.length > 0) {
                let emp_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 5);
                let bm_details = await taskModel.get_allocated_employee(invoice_detail_info[0].customer_id, 1);
                if (emp_details.length > 0) {

                  let attachmentArr = [];
                  // Mail attached of invoice file.
                  if (invoice_detail_info[0].invoice_file_path != '' && invoice_detail_info[0].invoice_file_name != '') {
                    let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].invoice_file_path, invoice_detail_info[0].invoice_file_name);

                    if (base_64_stream.status == 1) {
                      attachmentArr.push({
                        filename: invoice_detail_info[0].invoice_file_name,
                        content: base_64_stream.base64,
                        encoding: "base64"
                      });
                    }
                  }

                  // Mail attached of package file.   
                  if (invoice_detail_info[0].packing_file_path != '' && invoice_detail_info[0].packing_file_name != '') {
                    let base_64_stream = await module.exports.__get_base64(invoice_detail_info[0].packing_file_path, invoice_detail_info[0].packing_file_name);

                    if (base_64_stream.status == 1) {
                      attachmentArr.push({
                        filename: invoice_detail_info[0].packing_file_name,
                        content: base_64_stream.base64,
                        encoding: "base64"
                      });
                    }
                  }

                  let mail_cc = '';
                  if (bm_details.length > 0) {
                    mail_cc = bm_details[0].email;
                  }
                  if (Config.environment != 'production') {
                    mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                  }

                  let mailToParam = {
                    task_ref: invoice_detail_info[0].task_ref,
                    emp_first_name: entities.decode(emp_details[0].first_name),
                    emp_last_name: entities.decode(emp_details[0].last_name),
                    first_name: entities.decode(invoice_detail_info[0].first_name),
                    last_name: entities.decode(invoice_detail_info[0].last_name),
                    company_name: entities.decode(invoice_detail_info[0].company_name),
                    product_name: entities.decode(invoice_detail_info[0].product_name),
                    rdd: (invoice_detail_info[0].rdd != null && invoice_detail_info[0].rdd != '') ? common.changeDateFormat(invoice_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                    po_number: invoice_detail_info[0].po_no,
                    sales_order_no: invoice_detail_info[0].sales_order_no,
                    status: 'rejected',
                    comment: '<tr><td><strong>Comment</strong></td><td>' + common.nl2br(reason_of_rejection) + '</td></tr>'
                  };

                  const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PROFORMA_INVOICE', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

                }
              }
              res.status(200).json({
                status: 1,
                message: "Document Rejected Successfully.",
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            })
        } else if (user.payload.tyds == 'C') {
          await taskModel.updateShipCOAById(user.payload.tyti, 2, user.customer_id, today, entities.encode(reason_of_rejection))
            .then(async function (data) {
              let coa_detail_info = await taskModel.getShipCOAById(user.payload.tyti, 2);
              if (coa_detail_info.length > 0) {
                let emp_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 5);
                let bm_details = await taskModel.get_allocated_employee(coa_detail_info[0].customer, 1);
                if (emp_details.length > 0) {

                  let attachmentArr = [];
                  // Mail attached of COA file.
                  if (coa_detail_info[0].coa_file_path != '' && coa_detail_info[0].coa_file != '') {
                    let base_64_stream = await module.exports.__get_base64(coa_detail_info[0].coa_file_path, coa_detail_info[0].coa_file);

                    if (base_64_stream.status == 1) {
                      attachmentArr.push({
                        filename: coa_detail_info[0].coa_file,
                        content: base_64_stream.base64,
                        encoding: "base64"
                      });
                    }
                  }

                  let mail_cc = '';
                  if (bm_details.length > 0) {
                    mail_cc = bm_details[0].email;
                  }
                  if (Config.environment != 'production') {
                    mail_cc = (mail_cc != '') ? mail_cc + ',debadrita.ghosh@indusnet.co.in' : 'debadrita.ghosh@indusnet.co.in';
                  }

                  let mailToParam = {
                    task_ref: coa_detail_info[0].task_ref,
                    emp_first_name: entities.decode(emp_details[0].first_name),
                    emp_last_name: entities.decode(emp_details[0].last_name),
                    first_name: entities.decode(coa_detail_info[0].first_name),
                    last_name: entities.decode(coa_detail_info[0].last_name),
                    company_name: entities.decode(coa_detail_info[0].company_name),
                    product_name: entities.decode(coa_detail_info[0].product_name),
                    rdd: (coa_detail_info[0].rdd != null && coa_detail_info[0].rdd != '') ? common.changeDateFormat(coa_detail_info[0].rdd, '/', 'dd.mm.yyyy') : '-',
                    invoice_number: coa_detail_info[0].invoice_number,
                    po_number: coa_detail_info[0].po_no,
                    sales_order_no: coa_detail_info[0].sales_order_number,
                    status: 'rejected',
                    comment: '<tr><td><strong>Comment</strong></td><td>' + common.nl2br(reason_of_rejection) + '</td></tr>'
                  };

                  const tomailSent = common.sendMailWithAttachByCodeB2E('EMPLOYEE_NOTIFICATION_TEMPLATE_FOR_PRESHIPMENT_COA', emp_details[0].email, Config.noReplyMail, mail_cc, mailToParam, attachmentArr, "e");

                }
              }
              res.status(200).json({
                status: 1,
                message: "Document Rejected Successfully.",
              });
            }).catch(err => {
              common.logError(err);
              res.status(400).json({
                status: 3,
                message: Config.errorText.value
              }).end();
            });
        } else {
          let return_err = {
            status: 5,
            message: "Invalid Access."
          };
          return res.status(401).json(return_err);
        }
      } else {
        let return_err = {
          status: 5,
          message: "Invalid Access."
        };
        return res.status(401).json(return_err);
      }

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  /**
  * Customer Validate Token
  * @author Debadrita Ghosh <debadrita.ghosh@indusnet.co.in>
  * @param {Object} req - HTTP request argument to the middleware function
  * @param {Object} res - HTTP response argument to the middleware function
  * @param {next} next  - Callback argument to the middleware function
  * @return {Json Object} - Success or Error with message
  */

  process_validate_token: async (req, res, next) => {
    try {
      const user = req.user;
      let today = common.currentDateTime();
      if (user && user.customer_id > 0) {
        res.status(200).json({
          status: 1,
          type: 1,
          token: '',
          customer_details: {},
        });
      } else {
        let return_err = {
          status: 5,
          message: "Invalid Access."
        };
        return res.status(401).json(return_err);
      }
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  /**
   * Generates a Get Base64 
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} key - HTTP request argument
   * @param {Object} file_name - HTTP request argument
   * @return {Json Object} - Get Base64  used for Decode
   */
  __get_base64: async (key, file_name) => {

    AWS.config.update({
      accessKeyId: Config.aws.accessKeyId,
      secretAccessKey: Config.aws.secretAccessKey
    });
    let s3 = new AWS.S3();
    let options = {
      Bucket: Config.aws.bucketName,
      Key: key
    };
    return new Promise((resolve, reject) => {
      s3.getObject(options, function (err, data) {
        if (err === null) {

          // const fileDataDecoded = Buffer.from(data.Body.toString('base64'),'base64');
          // fs.writeFile(`${Config.upload.task_compressed}${file_name}`, fileDataDecoded, function(err) {
          //     //Handle Error
          //     console.log('=======');
          //     console.log(file_name);
          // });

          resolve({
            status: 1,
            base64: data.Body.toString('base64')
          });
        } else {
          resolve({
            status: 2,
            error: err
          });
        }
      });
    });
  },
}