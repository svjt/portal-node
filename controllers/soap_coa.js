
const fs = require('fs');
const Config     = require('../configuration/config');
const sapModel   = require("../models/soap");

const common     = require('./common');
var auth         = require('basic-auth');

var compare      = require('tsscmp');
var ip = require('ip');
const AWS     = require('aws-sdk');
const uuidv4  = require('uuid/v4');
var path      = require('path');
var request   = require('request').defaults({ encoding: null });
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


const s3Config = new AWS.S3({
  accessKeyId     : Config.aws.accessKeyId,
  secretAccessKey : Config.aws.secretAccessKey

});




module.exports = {
    __fs_readfile: async (filepath) => {
		return new Promise(function (resolve, reject) {
            fs.readFile(filepath, function(err, data){
                if(err){
                   console.log(err);
                   resolve("");    
                }else{
                   resolve(data);
                }
            });
		});
	},

    MyService: {
        MyPort: {
            UpdateCoa:async function(args, cb, headers, req) {
              console.log("hello i am in Coa !!",args)
                
              
              // var credentials = auth(req);
              // var valid = true;
              
              // if (credentials) {
                 
              //   valid = compare(credentials.name, Config.sapAuth.username) && valid
              //   valid = compare(credentials.pass, Config.sapAuth.password) && valid
              //   if(!valid){
              //       throw {
              //           Fault: {
              //             Code: {
              //               Value: '401'
              //             },
              //             Reason: { Text: 'Authentication failed' },
              //             statusCode: 401
              //           }
              //         };
              //   }                
              // }else{
              //   throw {
              //           Fault: {
              //             Code: {
              //               Value: '401'
              //             },
              //             Reason: { Text: 'Authentication failed' },
              //             statusCode: 401
              //           }
              //         };
              // } 
                
               // console.log('SOAP `reallyDetailedFunction` request from ' + req.connection.remoteAddress);
                let insLogObj = {
                    request : JSON.stringify(args),
                    response : '',
                    method : 'POST',
                    url : '',
                    datetime : common.currentDateTime(),
                    request_type : 'COA_UPDATE'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj); 
                let all_request = args;
                let error = '';
               
                if(all_request.customer_name == '' || 
                  all_request.batch_number == '' || 
                  all_request.lot_no == '' || 
                  all_request.file_path == '' || 
                  all_request.customer_name == null || 
                  all_request.batch_number == null || 
                  all_request.lot_no == null || 
                  all_request.file_path == null || 
                  all_request.customer_name == '?' || 
                  all_request.batch_number == '?' || 
                  all_request.lot_no == '?' || 
                  all_request.file_path == '?' ||
                  all_request.customer_id  == null ||
                  all_request.customer_id  == '' ||
                  all_request.customer_id  == '?' ||
                  all_request.material_id  == null ||
                  all_request.material_id  == '' ||
                  all_request.material_id  == '?'
                  ){

                  
                      // is null
                      //check customer and material id is numeric or not  

                      if(logResponse){
                        error = new Error('Null value is not accepted .');
                        common.logError(error);
                          sapModel.updateRequestLog(logResponse.id,'{"status" : false}', '{"reason" : "null value"}');// add reason
                      }
                      return {
                          status : false,
                          message :  "Null value is not accepted ."
                      };

                }else{


                    if(isNaN(all_request.customer_id) || isNaN(all_request.material_id) ){  //string to number checking []

                        common.logError(new Error("Non numeric value posted"));
                        sapModel.updateRequestLog(logResponse.id,'{"status" : false}','{"reason" : "Non numeric value posted"}');
                        return {
                            status : false,
                            message :  "Error occured while adding."
                        };

                    }
                    
                
                    all_request.customer_name = entities.encode(all_request.customer_name);
                    all_request.batch_number = entities.encode(all_request.batch_number.replace(/^0+/, ''));
                    all_request.lot_no = entities.encode(all_request.lot_no.replace(/^0+/, ''));
                    all_request.file_path = entities.encode(all_request.file_path); 
                    all_request.date_added = common.currentDateTime();
                    all_request.ip = ip.address();
                    all_request.customer_id = entities.encode(all_request.customer_id) ? entities.encode(all_request.customer_id.replace(/^0+/, '')) : '' ; 
                    all_request.material_id = entities.encode(all_request.material_id.replace(/^0+/, '')); ///00
                    all_request.processed = 0;
                    const add_coa = await sapModel.add_coa(all_request)

                       if(add_coa && add_coa.coa_id > 0){
                          sapModel.updateRequestLog(logResponse.id,'{"status" : true}','');
                          return {
                              status : true,
                              message :  "data added successfully"

                              }; 
                       }else{
                           logError(new Error("Error occured while adding"))
                          sapModel.updateRequestLog(logResponse.id,'{"status" : true}','{"reason" : "Error occured while adding."}');
                          return {
                              status : false,
                              message :  "Error occured while adding."
                          };
                       }                        
                }
            }
        }
    }
}
           
        
    
