const faModel = require('../models/fa');
const Config = require('../configuration/config');
const common = require('./common');
const globalAdminLimit = 10;
const Excel = require('exceljs');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const Cryptr = require('cryptr');
const cryptr = new Cryptr(Config.cryptR.secret);

const AWS = require('aws-sdk');
const uuidv4        = require('uuid/v4');
var path            = require('path');

const dateFormat    = require('dateformat');
var striptags       = require('striptags');
const fs             = require('fs'); 

const s3Config = new AWS.S3({
  accessKeyId: Config.aws.accessKeyId,
  secretAccessKey: Config.aws.secretAccessKey
});

module.exports = {


  get_merged_products: async (req, res, next) => {

    let us_products = await faModel.product_us();
    let au_products = await faModel.product_au();
    let ema_products = await faModel.product_ema();

    let products_1 = au_products.concat(us_products);
    let products_2 = ema_products.concat(products_1);

    return res.status(200)
      .json({
        status: 1,
        data: products_2
      })
  },

  suggest: async (req, res, next) => {
    try {
      const { id, txt } = req.body;

      let search_text = entities.encode(txt);

      await faModel.suggest_all(search_text)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data,
          }).end();
        })
        .catch((err) => {
          console.log(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    } catch (err) {
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },

  // suggest: async (req, res, next) => {
  //   try {
  //     const { id, txt } = req.body;

  //     if (id == 1 || id == 6) {
  //       await faModel.suggest_products_us(txt)
  //         .then(async function (data) {
  //           // var arrOfVals = [];
  //           // for( let i=0; i<data.length; i++){
  //           //   arrOfVals.push( {label:data[i].product_name} ); //Modified By SVJT
  //           //   //arrOfVals.push( data[i].product_name );
  //           // }           
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     } else if (id == 3) { // Modified By SVJT
  //       await faModel.suggest_products_ema(txt)
  //         .then(async function (data) {
  //           // var arrOfVals = [];
  //           // for( let i=0; i<data.length; i++){
  //           //   arrOfVals.push( {label:data[i].product_name} ); //Modified By SVJT
  //           //   //arrOfVals.push( data[i].product_name );
  //           // }          
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     } else if (id == 2) { // Modified By SVJT
  //       await faModel.suggest_products_au(txt)
  //         .then(async function (data) {
  //           // var arrOfVals = [];
  //           // for( let i=0; i<data.length; i++){
  //           //   arrOfVals.push( {label:data[i].product_name} ); //Modified By SVJT
  //           //   //arrOfVals.push( data[i].product_name );
  //           // }          
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     } else if (id == 4) {
  //       await faModel.suggest_products_drugbank(txt)
  //         .then(async function (data) {
  //           // var arrOfVals = [];
  //           // for( let i=0; i<data.length; i++){
  //           //   arrOfVals.push( {label:data[i].product_name} ); //Modified By SVJT
  //           //   //arrOfVals.push( data[i].product_name );
  //           // }          
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     } else if (id == 5) {
  //       await faModel.suggest_products_patent(txt)
  //         .then(async function (data) {
  //           var arrOfVals = [];
  //           for (let i = 0; i < data.length; i++) {
  //             arrOfVals.push({ label: data[i].product_name }); //Modified By SVJT
  //             //arrOfVals.push( data[i].product_name );
  //           }
  //           res.status(200).json({
  //             status: 1,
  //             data: arrOfVals,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     }
  //   } catch (err) {
  //     res
  //       .status(400)
  //       .json({
  //         status: 3,
  //         message: Config.errorText.value,
  //       })
  //       .end();
  //   }

  // },


  search: async (req, res, next) => {
    try {
      let { id, txt, ndc_code, organization,functional_category,unni,similarity_index } = req.body;

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalAdminLimit;
        var offset = ((page - 1) * globalAdminLimit);
      } else {
        var limit = globalAdminLimit;
        var offset = 0
      }

      let search_text = entities.encode(txt);

      let ndc_codes = await faModel.get_ndc_codes(search_text);

      if(ndc_codes !== ''){
        let remove_white_space_ndc = ndc_codes.replace(/ /g,'');
        let ndc_arr = remove_white_space_ndc.split(',');
        if (id == 1) {
          await faModel.find_product_us_ndc(ndc_arr, limit, offset)
            .then(async function (data) {
              const total = await faModel.count_product_us_ndc(ndc_arr);
              res.status(200).json({
                status: 1,
                data: data,
                cnt: total
              }).end();
            })
            .catch((err) => {
              console.log(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
  
        } else if (id == 2) {//Modified By SVJT
          await faModel.find_product_au_ndc(ndc_arr, limit, offset)
            .then(async function (data) {
              const total = await faModel.count_product_au_ndc(ndc_arr);
              res.status(200).json({
                status: 1,
                cnt: total,
                data: data
              }).end();
            })
            .catch((err) => {
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
  
        } else if (id == 3) {//Modified By SVJT
          await faModel.find_product_ema_ndc(ndc_arr, limit, offset)
            .then(async function (data) {
              const total = await faModel.count_product_ema_ndc(ndc_arr);
              res.status(200).json({
                status: 1,
                data: data,
                cnt: total,
              }).end();
            })
            .catch((err) => {
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
  
        } else if (id == 4) {
          await faModel.find_product_drugbank_ndc(ndc_arr)
            .then(async function (data) {
              res.status(200).json({
                status: 1,
                cnt: 0,
                data: data
              }).end();
            })
            .catch((err) => {
              console.log(err)
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
  
        } else if (id == 5) {
          await faModel.find_product_patent_ndc(ndc_arr, limit, offset)
            .then(async function (data) {
              const total = await faModel.count_product_patent_ndc(ndc_arr);
              console.log(total);
              res.status(200).json({
                status: 1,
                cnt: total,
                data: data
              }).end();
            })
            .catch((err) => {
              console.log(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
  
        } else if (id == 6) {
          let ndc_arr2 = [];
          if (ndc_code && ndc_code.length > 0) {
            ndc_arr2 = ndc_code;
          }else{
            ndc_arr2 = ndc_arr;
          }
          if (organization == undefined) {
            organization = [];
          }
          await faModel.find_product_us_excipient_ndc(ndc_arr2, organization, limit, offset)
            .then(async function (data) {
              const total = await faModel.count_product_us_excipient_ndc(ndc_arr2, organization);

              let ndc_code_arr = [];
              if (ndc_arr.length > 0) {
                for (let i = 0; i < ndc_arr.length; i++) {
                  ndc_code_arr.push({ label: ndc_arr[i],value:ndc_arr[i] });
                }
              }
              
              const organization_result = await faModel.find_product_us_excipient_organization_list_ndc(ndc_arr);
              let organization_arr = [];
              if (organization_result.length > 0) {
                for (let i = 0; i < organization_result.length; i++) {
                  organization_arr.push({ label: organization_result[i].organization,value:organization_result[i].organization });
                }
              }
              res.status(200).json({
                status: 1,
                data: data,
                ndc_code: ndc_code_arr,
                organization: organization_arr,
                cnt: total,
              }).end();
            })
            .catch((err) => {
              console.log('err',err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
        } else if (id == 7) {
          let ndc_arr2 = [];
          if (ndc_code && ndc_code.length > 0) {
            ndc_arr2 = ndc_code;
          }

          let ndc_code_arr = [];
          if (ndc_arr.length > 0) {
            for (let i = 0; i < ndc_arr.length; i++) {
              ndc_code_arr.push({ label: ndc_arr[i],value:ndc_arr[i] });
            }
          }

          if(ndc_arr2.length > 0 && functional_category && functional_category != '' && unni &&  unni != ''){

            let get_all_expients = await faModel.get_alternate_excipients_list(ndc_arr2[0]);

            await faModel.find_product_alternate_excipient_ndc(ndc_arr2[0],functional_category,unni,limit,offset)
            .then(async function (data) {

              console.log(data);

              const total = await faModel.count_product_alternate_excipient_ndc(ndc_arr2[0],functional_category,unni);

              let sim_index = 0;
              if(similarity_index > 0){
                sim_index = similarity_index;
              }else{
                sim_index = data[0].similarity_index
              }

              let similarity_data = await faModel.find_cosine(sim_index);

              let new_similarity_data = similarity_data.filter((element)=>{
                return element.index > 0.1
              });

              console.log(new_similarity_data.length,total);

              //let arr3 = arr1.map((item, i) => Object.assign({}, item, arr2[i]));

              res.status(200).json({
                status: 1,
                data: data,
                ndc_code: ndc_code_arr,
                alternate_expients_list:get_all_expients,
                cnt: total,
                similarity_index:sim_index
              }).end();
              
            })
            .catch((err) => {
              console.log('err',err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
          }else{

            if(ndc_arr2.length > 0){

              console.log('here');

              let get_all_expients = await faModel.get_alternate_excipients_list(ndc_arr2[0]);

              res.status(200).json({
                status: 1,
                ndc_code: ndc_code_arr,
                alternate_expients_list:get_all_expients,
                data: [],
                cnt: 0
              }).end();
            }else{
              res.status(200).json({
                status: 1,
                ndc_code: ndc_code_arr,
                alternate_expients_list:[],
                data: [],
                cnt: 0
              }).end();
            }
          }
        }
      }else{
        res.status(200).json({
          status: 1,
          data: [],
          cnt: 0,
        }).end();
      }
    } catch (err) {
      console.log(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },

  // search: async (req, res, next) => {
  //   try {
  //     let { id, txt, ndc_code, organization } = req.body;

  //     if (req.query.page && req.query.page > 0) {
  //       var page = req.query.page;
  //       var limit = globalAdminLimit;
  //       var offset = ((page - 1) * globalAdminLimit);
  //     } else {
  //       var limit = globalAdminLimit;
  //       var offset = 0
  //     }
  //     if (id == 1) {
  //       await faModel.find_product_us(txt, limit, offset)
  //         .then(async function (data) {
  //           const total = await faModel.count_product_us(txt);
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //             cnt: total
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 3) {//Modified By SVJT
  //       await faModel.find_product_ema(txt, limit, offset)
  //         .then(async function (data) {
  //           const total = await faModel.count_product_ema(txt);
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //             cnt: total[0].count,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 2) {//Modified By SVJT
  //       await faModel.find_product_au(txt, limit, offset)
  //         .then(async function (data) {
  //           const total = await faModel.count_product_au(txt);
  //           res.status(200).json({
  //             status: 1,
  //             cnt: total[0].count,
  //             data: data
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 4) {
  //       await faModel.find_product_drugbank(txt, limit, offset)
  //         .then(async function (data) {
  //           //const total = await faModel.count_product_drugbank(txt);
  //           res.status(200).json({
  //             status: 1,
  //             cnt: 0,
  //             data: data
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 5) {
  //       await faModel.find_product_patent(txt, limit, offset)
  //         .then(async function (data) {
  //           const total = await faModel.count_product_patent(txt);
  //           console.log(total);
  //           res.status(200).json({
  //             status: 1,
  //             cnt: total,
  //             data: data
  //           }).end();
  //         })
  //         .catch((err) => {
  //           console.log(err);
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 6) {
  //       if (ndc_code == undefined) {
  //         ndc_code = [];
  //       }
  //       if (organization == undefined) {
  //         organization = [];
  //       }
  //       await faModel.find_product_us_excipient(txt, ndc_code, organization, limit, offset)
  //         .then(async function (data) {
  //           const total = await faModel.count_product_us_excipient(txt, ndc_code, organization);
  //           // send unique organization and ndc code list based on  txt    
  //           const ndc_code_result = await faModel.find_product_us_excipient_ndc_list(txt);
  //           var ndc_code_arr = [];
  //           if (ndc_code_result.length > 0) {
  //             for (let i = 0; i < ndc_code_result.length; i++) {
  //               ndc_code_arr.push({ label: ndc_code_result[i].ndc_code,value:ndc_code_result[i].ndc_code });
  //             }
  //           }

  //           const organization_result = await faModel.find_product_us_excipient_organization_list(txt);
  //           var organization_arr = [];
  //           if (organization_result.length > 0) {
  //             for (let i = 0; i < organization_result.length; i++) {
  //               organization_arr.push({ label: organization_result[i].organization,value:organization_result[i].organization });
  //             }
  //           }
  //           res.status(200).json({
  //             status: 1,
  //             data: data,
  //             ndc_code: ndc_code_arr,
  //             organization: organization_arr,
  //             cnt: total,
  //           }).end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     }
  //   } catch (err) {
  //     console.log(err);
  //     res
  //       .status(400)
  //       .json({
  //         status: 3,
  //         message: Config.errorText.value,
  //       })
  //       .end();
  //   }

  // },

  search_download: async (req, res, next) => {
    try {
      let { id, txt, ndc_code, organization } = req.body;

      let search_text = entities.encode(txt);

      let ndc_codes = await faModel.get_ndc_codes(search_text);

      if(ndc_codes !== ''){
          let remove_white_space_ndc = ndc_codes.replace(/ /g,'');
          let ndc_arr = remove_white_space_ndc.split(',');

          if (id == 1) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('Orange Book details');
    
            await faModel.find_product_us_download_ndc(ndc_arr)
              .then(async function (data) {
    
                worksheet.columns = [{
                  header: 'NCD Code',
                  key: 'ndc_code',
                  width: 30
                },
                {
                  header: 'Application Number',
                  key: 'applicationnumber',
                  width: 30
                },
                {
                  header: 'Organization',
                  key: 'organization',
                  width: 40
                }
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'ndc_code': `${element.ndc_code}`,
                    'applicationnumber': `${element.applicationnumber}`,
                    'organization': `${element.organization}`
                  };
                  rows_arr.push(row);
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'orange_book_details.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
    
          } else if (id == 3) {//Modified By SVJT
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('TGA details');
            await faModel.find_product_ema_download_ndc(ndc_arr)
              .then(async function (data) {
                worksheet.columns = [{
                  header: 'Product Name',
                  key: 'productname',
                  width: 30
                },
                {
                  header: 'NCD Code',
                  key: 'ndc_code',
                  width: 30
                },
                {
                  header: 'Product Number',
                  key: 'product_number',
                  width: 30
                },
                {
                  header: 'Authorisation Status',
                  key: 'authorisation_status',
                  width: 30
                },
                {
                  header: 'Therapeutic Area',
                  key: 'therapeutic_area',
                  width: 30
                },
                {
                  header: 'Generic',
                  key: 'generic',
                  width: 30
                },
                {
                  header: 'Biosimilar',
                  key: 'biosimilar',
                  width: 30
                },
                {
                  header: 'Additional Monitoring',
                  key: 'additional_monitoring',
                  width: 30
                },
                {
                  header: 'Orphan Drug',
                  key: 'orphan_drug',
                  width: 30
                },
                {
                  header: 'Marketing Date',
                  key: 'marketing_date',
                  width: 30
                },
                {
                  header: 'Pharma Class',
                  key: 'pharma_class',
                  width: 30
                },
                {
                  header: 'Company Name',
                  key: 'company_name',
                  width: 30
                }
    
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'productname': `${element.productname}`,
                    'ndc_code': `${element.ndc_code}`,
                    'product_number': `${element.product_number}`,
                    'authorisation_status': `${element.authorisation_status}`,
                    'therapeutic_area': `${element.therapeutic_area}`,
                    'generic': `${element.generic}`,
                    'biosimilar': `${element.biosimilar}`,
                    'additional_monitoring': `${element.additional_monitoring}`,
                    'orphan_drug': `${element.orphan_drug}`,
                    'marketing_date': `${element.marketing_date}`,
                    'pharma_class': `${element.pharma_class}`,
                    'company_name': `${element.company_name}`
                  };
                  rows_arr.push(row);
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'tga_details.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
    
          } else if (id == 2) {//Modified By SVJT
    
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('EMA details');
            await faModel.find_product_au_download_ndc(ndc_arr)
              .then(async function (data) {
                worksheet.columns = [{
                  header: 'Product Name',
                  key: 'product_name',
                  width: 30
                },
                {
                  header: 'NCD Code',
                  key: 'ndc_code',
                  width: 30
                },
                {
                  header: 'NCD Name',
                  key: 'ndc_name',
                  width: 30
                },
                {
                  header: 'Submission Number',
                  key: 'submission_number',
                  width: 30
                },
                {
                  header: 'Sponsor',
                  key: 'sponsor',
                  width: 30
                },
                {
                  header: 'Submission Type',
                  key: 'submission_type',
                  width: 30
                },
                {
                  header: 'Authorization Status',
                  key: 'authorization_status',
                  width: 30
                },
                {
                  header: 'Auspar Date',
                  key: 'auspar_date',
                  width: 30
                },
                {
                  header: 'Publication Date',
                  key: 'publication_date',
                  width: 30
                },
    
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'product_name': `${element.product_name}`,
                    'ndc_code': `${element.ndc_code}`,
                    'ndc_name': `${element.ndc_name}`,
                    'submission_number': `${element.submission_number}`,
                    'sponsor': `${element.sponsor}`,
                    'submission_type': `${element.submission_type}`,
                    'authorization_status': `${element.authorization_status}`,
                    'auspar_date': `${element.auspar_date}`,
                    'publication_date': `${element.publication_date}`,
                  };
                  rows_arr.push(row);
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'ema_details.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
    
          } else if (id == 4) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('API Details');
            await faModel.find_product_drugbank_download_ndc(ndc_arr)
              .then(async function (data) {
                worksheet.columns = [{
                  header: 'Dc Name',
                  key: 'dcname',
                  width: 30
                },
                {
                  header: 'NCD Code',
                  key: 'ndccode',
                  width: 30
                },
                {
                  header: 'Drug Bank ID',
                  key: 'drugbank_id',
                  width: 30
                },
                {
                  header: 'Product Name',
                  key: 'productname',
                  width: 30
                },
                {
                  header: 'Water Sol X',
                  key: 'water_sol_x',
                  width: 30
                },
                {
                  header: 'LogP',
                  key: 'logp_x',
                  width: 30
                },
                {
                  header: 'PKA',
                  key: 'pka',
                  width: 30
                },
                {
                  header: 'Smiles',
                  key: 'smiles',
                  width: 30
                },
                {
                  header: 'CAS Number',
                  key: 'casnum',
                  width: 30
                },
                {
                  header: 'Boiling Point',
                  key: 'boiling_point',
                  width: 30
                },
                {
                  header: 'Hydrophobicity',
                  key: 'hydrophobicity',
                  width: 30
                },
                {
                  header: 'Isoelectric Point',
                  key: 'isoelectric_point',
                  width: 30
                },
                {
                  header: 'Melting Point',
                  key: 'melting_point',
                  width: 30
                },
                {
                  header: 'Molecular Formula X',
                  key: 'molecular_formula_x',
                  width: 30
                },
                {
                  header: 'Molecular Weight',
                  key: 'molecular_weight_x',
                  width: 30
                },
                {
                  header: 'Radioactivity',
                  key: 'radioactivity',
                  width: 30
                },
                {
                  header: 'Caco2 Permeability',
                  key: 'caco2_permeability',
                  width: 30
                },
                {
                  header: 'Logs',
                  key: 'logs_x',
                  width: 30
                },
                {
                  header: 'Description',
                  key: 'description',
                  width: 30
                },
                {
                  header: 'Indication',
                  key: 'indication',
                  width: 30
                },
                {
                  header: 'Average Mass',
                  key: 'average_mass',
                  width: 30
                },
                {
                  header: 'Monoisotopic Mass',
                  key: 'monoisotopic_mass',
                  width: 30
                },
                {
                  header: 'State',
                  key: 'state',
                  width: 30
                },
                {
                  header: 'Pharmacodynamics',
                  key: 'pharmacodynamics',
                  width: 30
                },
                {
                  header: 'Clearance',
                  key: 'clearance',
                  width: 30
                },
                {
                  header: 'Mechanism Of Action',
                  key: 'mechanism_of_action',
                  width: 30
                },
                {
                  header: 'Toxicity',
                  key: 'toxicity',
                  width: 30
                },
                {
                  header: 'Metabolism',
                  key: 'metabolism',
                  width: 30
                },
                {
                  header: 'Absorption',
                  key: 'absorption',
                  width: 30
                },
                {
                  header: 'Half Life',
                  key: 'half_life',
                  width: 30
                },
                {
                  header: 'Protein Binding',
                  key: 'protein_binding',
                  width: 30
                },
                {
                  header: 'Route Of Elimination',
                  key: 'route_of_elimination',
                  width: 30
                },
                {
                  header: 'Volume Of Distribution',
                  key: 'volume_of_distribution',
                  width: 30
                },
                {
                  header: 'Bond Acceptor Count',
                  key: 'h_bond_acceptor_count',
                  width: 30
                },
                {
                  header: 'H Bond Donor CH Bond Donor Countount',
                  key: 'h_bond_donor_ch_bond_donor_countount',
                  width: 30
                },
                {
                  header: 'IUPAC Name',
                  key: 'iupac_name',
                  width: 30
                },
                {
                  header: 'Molecular Formula Y',
                  key: 'molecular_formula_y',
                  width: 30
                },
                {
                  header: 'Physiological Charge',
                  key: 'physiological_charge',
                  width: 30
                },
                {
                  header: 'Polar Surface Area',
                  key: 'polar_surface_area_psa',
                  width: 30
                },
                {
                  header: 'Polarizability',
                  key: 'polarizability',
                  width: 30
                },
                {
                  header: 'Refractivity',
                  key: 'refractivity',
                  width: 30
                },
                {
                  header: 'Calculated Water Solubility',
                  key: 'water_solubility_y',
                  width: 30
                },
                {
                  header: 'Logp Y',
                  key: 'logp_y',
                  width: 30
                },
                {
                  header: 'Logs Y',
                  key: 'logs_y',
                  width: 30
                },
                {
                  header: 'PKA Strongest Acidic',
                  key: 'pka_strongest_acidic',
                  width: 30
                },
                {
                  header: 'PKA Strongest Basic',
                  key: 'pka_strongest_basic',
                  width: 30
                },
                {
                  header: 'Direct Parent',
                  key: 'dc_direct_parent',
                  width: 30
                },
                {
                  header: 'Pharm Classes',
                  key: 'pharm_classes',
                  width: 30
                },
    
    
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'dcname': `${element.dcname}`,
                    'ndccode': `${element.ndccode}`,
                    'drugbank_id': `${element.drugbank_id}`,
                    'productname': `${element.productname}`,
                    'water_sol_x': `${element.water_sol_x}`,
                    'pka': `${element.pka}`,
                    'smiles': `${element.smiles}`,
                    'casnum': `${element.casnum}`,
                    'boiling_point': `${element.boiling_point}`,
                    'hydrophobicity': `${element.hydrophobicity}`,
                    'isoelectric_point': `${element.isoelectric_point}`,
                    'melting_point': `${element.melting_point}`,
                    'molecular_formula_x': `${element.molecular_formula_x}`,
                    'molecular_weight_x': `${element.molecular_weight_x}`,
                    'radioactivity': `${element.radioactivity}`,
                    'caco2_permeability': `${element.caco2_permeability}`,
                    'logs_x': `${element.logs_x}`,
                    'description': `${element.description}`,
                    'indication': `${element.indication}`,
                    'average_mass': `${element.average_mass}`,
                    'monoisotopic_mass': `${element.monoisotopic_mass}`,
                    'state': `${element.state}`,
                    'pharmacodynamics': `${element.pharmacodynamics}`,
                    'clearance': `${element.clearance}`,
                    'mechanism_of_action': `${element.mechanism_of_action}`,
                    'toxicity': `${element.toxicity}`,
                    'metabolism': `${element.metabolism}`,
                    'absorption': `${element.absorption}`,
                    'half_life': `${element.half_life}`,
                    'protein_binding': `${element.protein_binding}`,
                    'route_of_elimination': `${element.route_of_elimination}`,
                    'volume_of_distribution': `${element.volume_of_distribution}`,
                    'h_bond_acceptor_count': `${element.h_bond_acceptor_count}`,
                    'h_bond_donor_ch_bond_donor_countount': `${element.h_bond_donor_ch_bond_donor_countount}`,
                    'iupac_name': `${element.iupac_name}`,
                    'molecular_formula_y': `${element.molecular_formula_y}`,
                    'physiological_charge': `${element.physiological_charge}`,
                    'polar_surface_area_psa': `${element.polar_surface_area_psa}`,
                    'polarizability': `${element.polarizability}`,
                    'refractivity': `${element.refractivity}`,
                    'water_solubility_y': `${element.water_solubility_y}`,
                    'logp_y': `${element.logp_y}`,
                    'logs_y': `${element.logs_y}`,
                    'pka_strongest_acidic': `${element.pka_strongest_acidic}`,
                    'pka_strongest_basic': `${element.pka_strongest_basic}`,
                    'dc_direct_parent': `${element.dc_direct_parent}`,
                    'pharm_classes': `${element.pharm_classes}`,
                  };
                  rows_arr.push(row);
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'api_etails.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
    
          } else if (id == 5) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('Patents Associated');
            await faModel.find_product_patent_download_ndc(ndc_arr)
              .then(async function (data) {
                worksheet.columns = [{
                  header: 'Patent Number',
                  key: 'patent_number',
                  width: 30
                },
                {
                  header: 'Product Name',
                  key: 'productname',
                  width: 30
                },
                {
                  header: 'Exclusivity Information',
                  key: 'exclusivity_information',
                  width: 30
                },
                {
                  header: 'Submission Date',
                  key: 'submission_date',
                  width: 30
                },
                {
                  header: 'Patent Expiry',
                  key: 'patentexpiry',
                  width: 30
                },
                {
                  header: 'DP	',
                  key: 'dp',
                  width: 30
                },
                {
                  header: 'DS',
                  key: 'ds',
                  width: 30
                },
                {
                  header: 'Current Assignee',
                  key: 'current_assignee',
                  width: 30
                },
                {
                  header: 'Patent Title',
                  key: 'patent_title',
                  width: 30
                },
                {
                  header: 'Key Words',
                  key: 'key_words',
                  width: 30
                },
                {
                  header: 'Global Dossier',
                  key: 'global_dossier',
                  width: 30
                },
    
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'patent_number': `${element.patent_number}`,
                    'productname': `${element.productname}`,
                    'exclusivity_information': `${element.exclusivity_information}`,
                    'submission_date': `${element.submission_date}`,
                    'patentexpiry': `${element.patentexpiry}`,
                    'dp': `${element.dp}`,
                    'ds': `${element.ds}`,
                    'current_assignee': `${element.current_assignee}`,
                    'patent_title': `${element.patent_title}`,
                    'key_words': `${element.key_words}`,
                    'global_dossier': `${element.global_dossier}`,
                  };
                  rows_arr.push(row);
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'patents_associated.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
    
          } else if (id == 6) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet('Excipients Used');
    
            let ndc_arr2 = [];
            if (ndc_code && ndc_code.length > 0) {
              ndc_arr2 = ndc_code;
            }else{
              ndc_arr2 = ndc_arr;
            }
            if (organization == undefined) {
              organization = [];
            }
            await faModel.find_product_us_excipient_download_ndc(ndc_arr2, organization)
              .then(async function (data) {
                worksheet.columns = [{
                  header: 'NDC Code',
                  key: 'ndc_code',
                  width: 30
                },
                {
                  header: 'Application Number',
                  key: 'applicationnumber',
                  width: 30
                },
                {
                  header: 'Organization',
                  key: 'organization',
                  width: 30
                },
                {
                  header: 'No Of Excepients',
                  key: 'no_of_excepients',
                  width: 30
                },
                {
                  header: 'Dosage Form',
                  key: 'dosage_form',
                  width: 30
                },
                {
                  header: 'Route	',
                  key: 'route',
                  width: 30
                },
                {
                  header: 'Color',
                  key: 'color',
                  width: 30
                },
                {
                  header: 'Shape',
                  key: 'shape',
                  width: 30
                },
                {
                  header: 'Sizes',
                  key: 'sizes',
                  width: 30
                },
                {
                  header: 'Strength',
                  key: 'strength',
                  width: 30
                },
                {
                  header: 'RLD',
                  key: 'rld',
                  width: 30
                },
    
                ];
    
                let rows_arr = [];
                for (let index = 0; index < data.length; index++) {
                  const element = data[index];
    
                  let row = {
                    'ndc_code': `${element.ndc_code}`,
                    'applicationnumber': `${element.applicationnumber}`,
                    'organization': `${element.organization}`,
                    'no_of_excepients': `${element.no_of_excepients}`,
                    'dosage_form': `${element.dosage_form}`,
                    'route': `${element.route}`,
                    'color': `${element.color}`,
                    'shape': `${element.shape}`,
                    'sizes': `${element.sizes}`,
                    'strength': `${element.strength}`,
                    'rld': `${element.rld}`,
                  };
                  rows_arr.push(row);
    
                  let ndc_code_data = await faModel.get_excipients_list_all(element.ndc_code);
    
                  if(ndc_code_data.length > 0){
                      let worksheet_no_excipents = workbook.addWorksheet(`No Of Excepients ${element.ndc_code}`);
    
                      worksheet_no_excipents.columns = [{
                        header: 'NDC Code',
                        key: 'ndc_code',
                        width: 30
                      },
                      {
                        header: 'UNII',
                        key: 'unii',
                        width: 30
                      },
                      {
                        header: 'Product Name',
                        key: 'product_name',
                        width: 30
                      },
                      {
                        header: 'Excipient Name',
                        key: 'excipientname',
                        width: 30
                      },
                      {
                        header: 'Daily Dosage Limit',
                        key: 'dailydosagelimit',
                        width: 30
                      },
                      {
                        header: 'Alternate Excipients',
                        key: 'alternate_excipients',
                        width: 30
                      },
                      {
                        header: 'Functional Category',
                        key: 'functional_category',
                        width: 30
                      },
                      {
                        header: 'Incompatibilities',
                        key: 'incompatibilities',
                        width: 30
                      }
          
                      ];
    
                      let rows_arr_ndc = [];
                      for (let index = 0; index < ndc_code_data.length; index++) {
                        const element_ndc = ndc_code_data[index];
          
                        let row = {
                          'ndc_code': `${element_ndc.ndc_code}`,
                          'unii': `${element_ndc.unii}`,
                          'product_name': `${element_ndc.product_name}`,
                          'excipientname': `${element_ndc.excipientname}`,
                          'dailydosagelimit': `${element_ndc.dailydosagelimit}`,
                          'alternate_excipients': `${element_ndc.alternate_excipients}`,
                          'functional_category': `${element_ndc.functional_category}`,
                          'incompatibilities': `${element_ndc.incompatibilities}`
                        };
                        rows_arr_ndc.push(row);
                      }
                      worksheet_no_excipents.addRows(rows_arr_ndc);
                  }
                }
    
                worksheet.addRows(rows_arr);
                var fileName = 'excipients_used.xlsx';
    
                res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                await workbook.xlsx.write(res);
                res.end();
              })
              .catch((err) => {
                console.log(err);
                res
                  .status(400)
                  .json({
                    status: 3,
                    message: Config.errorText.value,
                  })
                  .end();
              });
          }

      }else{
          res.status(200).json({
              status: 1,
              data: [],
              cnt: 0,
          }).end();
      }
    } catch (err) {
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },
  
  // search_download: async (req, res, next) => {
  //   try {
  //     let { id, txt, ndc_code, organization } = req.body;

  //     if (id == 1) {
  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('Orange Book details');

  //       await faModel.find_product_us_download(txt)
  //         .then(async function (data) {

  //           worksheet.columns = [{
  //             header: 'NCD Code',
  //             key: 'ndc_code',
  //             width: 30
  //           },
  //           {
  //             header: 'Application Number',
  //             key: 'applicationnumber',
  //             width: 30
  //           },
  //           {
  //             header: 'Organization',
  //             key: 'organization',
  //             width: 40
  //           }
  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'ndc_code': `${element.ndc_code}`,
  //               'applicationnumber': `${element.applicationnumber}`,
  //               'organization': `${element.organization}`
  //             };
  //             rows_arr.push(row);
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'orange_book_details.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 3) {//Modified By SVJT
  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('TGA details');
  //       await faModel.find_product_ema_download(txt)
  //         .then(async function (data) {
  //           worksheet.columns = [{
  //             header: 'Product Name',
  //             key: 'productname',
  //             width: 30
  //           },
  //           {
  //             header: 'NCD Code',
  //             key: 'ndc_code',
  //             width: 30
  //           },
  //           {
  //             header: 'Product Number',
  //             key: 'product_number',
  //             width: 30
  //           },
  //           {
  //             header: 'Authorisation Status',
  //             key: 'authorisation_status',
  //             width: 30
  //           },
  //           {
  //             header: 'Therapeutic Area',
  //             key: 'therapeutic_area',
  //             width: 30
  //           },
  //           {
  //             header: 'Generic',
  //             key: 'generic',
  //             width: 30
  //           },
  //           {
  //             header: 'Biosimilar',
  //             key: 'biosimilar',
  //             width: 30
  //           },
  //           {
  //             header: 'Additional Monitoring',
  //             key: 'additional_monitoring',
  //             width: 30
  //           },
  //           {
  //             header: 'Orphan Drug',
  //             key: 'orphan_drug',
  //             width: 30
  //           },
  //           {
  //             header: 'Marketing Date',
  //             key: 'marketing_date',
  //             width: 30
  //           },
  //           {
  //             header: 'Pharma Class',
  //             key: 'pharma_class',
  //             width: 30
  //           },
  //           {
  //             header: 'Company Name',
  //             key: 'company_name',
  //             width: 30
  //           }

  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'productname': `${element.productname}`,
  //               'ndc_code': `${element.ndc_code}`,
  //               'product_number': `${element.product_number}`,
  //               'authorisation_status': `${element.authorisation_status}`,
  //               'therapeutic_area': `${element.therapeutic_area}`,
  //               'generic': `${element.generic}`,
  //               'biosimilar': `${element.biosimilar}`,
  //               'additional_monitoring': `${element.additional_monitoring}`,
  //               'orphan_drug': `${element.orphan_drug}`,
  //               'marketing_date': `${element.marketing_date}`,
  //               'pharma_class': `${element.pharma_class}`,
  //               'company_name': `${element.company_name}`
  //             };
  //             rows_arr.push(row);
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'tga_details.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 2) {//Modified By SVJT

  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('EMA details');
  //       await faModel.find_product_au_download(txt)
  //         .then(async function (data) {
  //           worksheet.columns = [{
  //             header: 'Product Name',
  //             key: 'product_name',
  //             width: 30
  //           },
  //           {
  //             header: 'NCD Code',
  //             key: 'ndc_code',
  //             width: 30
  //           },
  //           {
  //             header: 'NCD Name',
  //             key: 'ndc_name',
  //             width: 30
  //           },
  //           {
  //             header: 'Submission Number',
  //             key: 'submission_number',
  //             width: 30
  //           },
  //           {
  //             header: 'Sponsor',
  //             key: 'sponsor',
  //             width: 30
  //           },
  //           {
  //             header: 'Submission Type',
  //             key: 'submission_type',
  //             width: 30
  //           },
  //           {
  //             header: 'Authorization Status',
  //             key: 'authorization_status',
  //             width: 30
  //           },
  //           {
  //             header: 'Auspar Date',
  //             key: 'auspar_date',
  //             width: 30
  //           },
  //           {
  //             header: 'Publication Date',
  //             key: 'publication_date',
  //             width: 30
  //           },

  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'product_name': `${element.product_name}`,
  //               'ndc_code': `${element.ndc_code}`,
  //               'ndc_name': `${element.ndc_name}`,
  //               'submission_number': `${element.submission_number}`,
  //               'sponsor': `${element.sponsor}`,
  //               'submission_type': `${element.submission_type}`,
  //               'authorization_status': `${element.authorization_status}`,
  //               'auspar_date': `${element.auspar_date}`,
  //               'publication_date': `${element.publication_date}`,
  //             };
  //             rows_arr.push(row);
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'ema_details.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 4) {
  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('API Details');
  //       await faModel.find_product_drugbank_download(txt)
  //         .then(async function (data) {
  //           worksheet.columns = [{
  //             header: 'Dc Name',
  //             key: 'dcname',
  //             width: 30
  //           },
  //           {
  //             header: 'NCD Code',
  //             key: 'ndccode',
  //             width: 30
  //           },
  //           {
  //             header: 'Drug Bank ID',
  //             key: 'drugbank_id',
  //             width: 30
  //           },
  //           {
  //             header: 'Product Name',
  //             key: 'productname',
  //             width: 30
  //           },
  //           {
  //             header: 'Water Sol X',
  //             key: 'water_sol_x',
  //             width: 30
  //           },
  //           {
  //             header: 'LogP',
  //             key: 'logp_x',
  //             width: 30
  //           },
  //           {
  //             header: 'PKA',
  //             key: 'pka',
  //             width: 30
  //           },
  //           {
  //             header: 'Smiles',
  //             key: 'smiles',
  //             width: 30
  //           },
  //           {
  //             header: 'CAS Number',
  //             key: 'casnum',
  //             width: 30
  //           },
  //           {
  //             header: 'Boiling Point',
  //             key: 'boiling_point',
  //             width: 30
  //           },
  //           {
  //             header: 'Hydrophobicity',
  //             key: 'hydrophobicity',
  //             width: 30
  //           },
  //           {
  //             header: 'Isoelectric Point',
  //             key: 'isoelectric_point',
  //             width: 30
  //           },
  //           {
  //             header: 'Melting Point',
  //             key: 'melting_point',
  //             width: 30
  //           },
  //           {
  //             header: 'Molecular Formula X',
  //             key: 'molecular_formula_x',
  //             width: 30
  //           },
  //           {
  //             header: 'Molecular Weight',
  //             key: 'molecular_weight_x',
  //             width: 30
  //           },
  //           {
  //             header: 'Radioactivity',
  //             key: 'radioactivity',
  //             width: 30
  //           },
  //           {
  //             header: 'Caco2 Permeability',
  //             key: 'caco2_permeability',
  //             width: 30
  //           },
  //           {
  //             header: 'Logs',
  //             key: 'logs_x',
  //             width: 30
  //           },
  //           {
  //             header: 'Description',
  //             key: 'description',
  //             width: 30
  //           },
  //           {
  //             header: 'Indication',
  //             key: 'indication',
  //             width: 30
  //           },
  //           {
  //             header: 'Average Mass',
  //             key: 'average_mass',
  //             width: 30
  //           },
  //           {
  //             header: 'Monoisotopic Mass',
  //             key: 'monoisotopic_mass',
  //             width: 30
  //           },
  //           {
  //             header: 'State',
  //             key: 'state',
  //             width: 30
  //           },
  //           {
  //             header: 'Pharmacodynamics',
  //             key: 'pharmacodynamics',
  //             width: 30
  //           },
  //           {
  //             header: 'Clearance',
  //             key: 'clearance',
  //             width: 30
  //           },
  //           {
  //             header: 'Mechanism Of Action',
  //             key: 'mechanism_of_action',
  //             width: 30
  //           },
  //           {
  //             header: 'Toxicity',
  //             key: 'toxicity',
  //             width: 30
  //           },
  //           {
  //             header: 'Metabolism',
  //             key: 'metabolism',
  //             width: 30
  //           },
  //           {
  //             header: 'Absorption',
  //             key: 'absorption',
  //             width: 30
  //           },
  //           {
  //             header: 'Half Life',
  //             key: 'half_life',
  //             width: 30
  //           },
  //           {
  //             header: 'Protein Binding',
  //             key: 'protein_binding',
  //             width: 30
  //           },
  //           {
  //             header: 'Route Of Elimination',
  //             key: 'route_of_elimination',
  //             width: 30
  //           },
  //           {
  //             header: 'Volume Of Distribution',
  //             key: 'volume_of_distribution',
  //             width: 30
  //           },
  //           {
  //             header: 'Bond Acceptor Count',
  //             key: 'h_bond_acceptor_count',
  //             width: 30
  //           },
  //           {
  //             header: 'H Bond Donor CH Bond Donor Countount',
  //             key: 'h_bond_donor_ch_bond_donor_countount',
  //             width: 30
  //           },
  //           {
  //             header: 'IUPAC Name',
  //             key: 'iupac_name',
  //             width: 30
  //           },
  //           {
  //             header: 'Molecular Formula Y',
  //             key: 'molecular_formula_y',
  //             width: 30
  //           },
  //           {
  //             header: 'Physiological Charge',
  //             key: 'physiological_charge',
  //             width: 30
  //           },
  //           {
  //             header: 'Polar Surface Area',
  //             key: 'polar_surface_area_psa',
  //             width: 30
  //           },
  //           {
  //             header: 'Polarizability',
  //             key: 'polarizability',
  //             width: 30
  //           },
  //           {
  //             header: 'Refractivity',
  //             key: 'refractivity',
  //             width: 30
  //           },
  //           {
  //             header: 'Calculated Water Solubility',
  //             key: 'water_solubility_y',
  //             width: 30
  //           },
  //           {
  //             header: 'Logp Y',
  //             key: 'logp_y',
  //             width: 30
  //           },
  //           {
  //             header: 'Logs Y',
  //             key: 'logs_y',
  //             width: 30
  //           },
  //           {
  //             header: 'PKA Strongest Acidic',
  //             key: 'pka_strongest_acidic',
  //             width: 30
  //           },
  //           {
  //             header: 'PKA Strongest Basic',
  //             key: 'pka_strongest_basic',
  //             width: 30
  //           },
  //           {
  //             header: 'Direct Parent',
  //             key: 'dc_direct_parent',
  //             width: 30
  //           },
  //           {
  //             header: 'Pharm Classes',
  //             key: 'pharm_classes',
  //             width: 30
  //           },


  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'dcname': `${element.dcname}`,
  //               'ndccode': `${element.ndccode}`,
  //               'drugbank_id': `${element.drugbank_id}`,
  //               'productname': `${element.productname}`,
  //               'water_sol_x': `${element.water_sol_x}`,
  //               'pka': `${element.pka}`,
  //               'smiles': `${element.smiles}`,
  //               'casnum': `${element.casnum}`,
  //               'boiling_point': `${element.boiling_point}`,
  //               'hydrophobicity': `${element.hydrophobicity}`,
  //               'isoelectric_point': `${element.isoelectric_point}`,
  //               'melting_point': `${element.melting_point}`,
  //               'molecular_formula_x': `${element.molecular_formula_x}`,
  //               'molecular_weight_x': `${element.molecular_weight_x}`,
  //               'radioactivity': `${element.radioactivity}`,
  //               'caco2_permeability': `${element.caco2_permeability}`,
  //               'logs_x': `${element.logs_x}`,
  //               'description': `${element.description}`,
  //               'indication': `${element.indication}`,
  //               'average_mass': `${element.average_mass}`,
  //               'monoisotopic_mass': `${element.monoisotopic_mass}`,
  //               'state': `${element.state}`,
  //               'pharmacodynamics': `${element.pharmacodynamics}`,
  //               'clearance': `${element.clearance}`,
  //               'mechanism_of_action': `${element.mechanism_of_action}`,
  //               'toxicity': `${element.toxicity}`,
  //               'metabolism': `${element.metabolism}`,
  //               'absorption': `${element.absorption}`,
  //               'half_life': `${element.half_life}`,
  //               'protein_binding': `${element.protein_binding}`,
  //               'route_of_elimination': `${element.route_of_elimination}`,
  //               'volume_of_distribution': `${element.volume_of_distribution}`,
  //               'h_bond_acceptor_count': `${element.h_bond_acceptor_count}`,
  //               'h_bond_donor_ch_bond_donor_countount': `${element.h_bond_donor_ch_bond_donor_countount}`,
  //               'iupac_name': `${element.iupac_name}`,
  //               'molecular_formula_y': `${element.molecular_formula_y}`,
  //               'physiological_charge': `${element.physiological_charge}`,
  //               'polar_surface_area_psa': `${element.polar_surface_area_psa}`,
  //               'polarizability': `${element.polarizability}`,
  //               'refractivity': `${element.refractivity}`,
  //               'water_solubility_y': `${element.water_solubility_y}`,
  //               'logp_y': `${element.logp_y}`,
  //               'logs_y': `${element.logs_y}`,
  //               'pka_strongest_acidic': `${element.pka_strongest_acidic}`,
  //               'pka_strongest_basic': `${element.pka_strongest_basic}`,
  //               'dc_direct_parent': `${element.dc_direct_parent}`,
  //               'pharm_classes': `${element.pharm_classes}`,
  //             };
  //             rows_arr.push(row);
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'api_etails.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 5) {
  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('Patents Associated');
  //       await faModel.find_product_patent_download(txt)
  //         .then(async function (data) {
  //           worksheet.columns = [{
  //             header: 'Patent Number',
  //             key: 'patent_number',
  //             width: 30
  //           },
  //           {
  //             header: 'Product Name',
  //             key: 'productname',
  //             width: 30
  //           },
  //           {
  //             header: 'Exclusivity Information',
  //             key: 'exclusivity_information',
  //             width: 30
  //           },
  //           {
  //             header: 'Submission Date',
  //             key: 'submission_date',
  //             width: 30
  //           },
  //           {
  //             header: 'Patent Expiry',
  //             key: 'patentexpiry',
  //             width: 30
  //           },
  //           {
  //             header: 'DP	',
  //             key: 'dp',
  //             width: 30
  //           },
  //           {
  //             header: 'DS',
  //             key: 'ds',
  //             width: 30
  //           },
  //           {
  //             header: 'Current Assignee',
  //             key: 'current_assignee',
  //             width: 30
  //           },
  //           {
  //             header: 'Patent Title',
  //             key: 'patent_title',
  //             width: 30
  //           },
  //           {
  //             header: 'Key Words',
  //             key: 'key_words',
  //             width: 30
  //           },
  //           {
  //             header: 'Global Dossier',
  //             key: 'global_dossier',
  //             width: 30
  //           },

  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'patent_number': `${element.patent_number}`,
  //               'productname': `${element.productname}`,
  //               'exclusivity_information': `${element.exclusivity_information}`,
  //               'submission_date': `${element.submission_date}`,
  //               'patentexpiry': `${element.patentexpiry}`,
  //               'dp': `${element.dp}`,
  //               'ds': `${element.ds}`,
  //               'current_assignee': `${element.current_assignee}`,
  //               'patent_title': `${element.patent_title}`,
  //               'key_words': `${element.key_words}`,
  //               'global_dossier': `${element.global_dossier}`,
  //             };
  //             rows_arr.push(row);
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'patents_associated.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });

  //     } else if (id == 6) {
  //       var workbook = new Excel.Workbook();
  //       var worksheet = workbook.addWorksheet('Excipients Used');

  //       if (ndc_code == undefined) {
  //         ndc_code = [];
  //       }
  //       if (organization == undefined) {
  //         organization = [];
  //       }
  //       await faModel.find_product_us_excipient_download(txt, ndc_code, organization)
  //         .then(async function (data) {
  //           worksheet.columns = [{
  //             header: 'NDC Code',
  //             key: 'ndc_code',
  //             width: 30
  //           },
  //           {
  //             header: 'Application Number',
  //             key: 'applicationnumber',
  //             width: 30
  //           },
  //           {
  //             header: 'Organization',
  //             key: 'organization',
  //             width: 30
  //           },
  //           {
  //             header: 'No Of Excepients',
  //             key: 'no_of_excepients',
  //             width: 30
  //           },
  //           {
  //             header: 'Dosage Form',
  //             key: 'dosage_form',
  //             width: 30
  //           },
  //           {
  //             header: 'Route	',
  //             key: 'route',
  //             width: 30
  //           },
  //           {
  //             header: 'Color',
  //             key: 'color',
  //             width: 30
  //           },
  //           {
  //             header: 'Shape',
  //             key: 'shape',
  //             width: 30
  //           },
  //           {
  //             header: 'Sizes',
  //             key: 'sizes',
  //             width: 30
  //           },
  //           {
  //             header: 'Strength',
  //             key: 'strength',
  //             width: 30
  //           },
  //           {
  //             header: 'RLD',
  //             key: 'rld',
  //             width: 30
  //           },

  //           ];

  //           let rows_arr = [];
  //           for (let index = 0; index < data.length; index++) {
  //             const element = data[index];

  //             let row = {
  //               'ndc_code': `${element.ndc_code}`,
  //               'applicationnumber': `${element.applicationnumber}`,
  //               'organization': `${element.organization}`,
  //               'no_of_excepients': `${element.no_of_excepients}`,
  //               'dosage_form': `${element.dosage_form}`,
  //               'route': `${element.route}`,
  //               'color': `${element.color}`,
  //               'shape': `${element.shape}`,
  //               'sizes': `${element.sizes}`,
  //               'strength': `${element.strength}`,
  //               'rld': `${element.rld}`,
  //             };
  //             rows_arr.push(row);

  //             let ndc_code_data = await faModel.get_excipients_list_all(element.ndc_code);

  //             if(ndc_code_data.length > 0){
  //                 let worksheet_no_excipents = workbook.addWorksheet(`No Of Excepients ${element.ndc_code}`);

  //                 worksheet_no_excipents.columns = [{
  //                   header: 'NDC Code',
  //                   key: 'ndc_code',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'UNII',
  //                   key: 'unii',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Product Name',
  //                   key: 'product_name',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Excipient Name',
  //                   key: 'excipientname',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Daily Dosage Limit',
  //                   key: 'dailydosagelimit',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Alternate Excipients',
  //                   key: 'alternate_excipients',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Functional Category',
  //                   key: 'functional_category',
  //                   width: 30
  //                 },
  //                 {
  //                   header: 'Incompatibilities',
  //                   key: 'incompatibilities',
  //                   width: 30
  //                 }
      
  //                 ];

  //                 let rows_arr_ndc = [];
  //                 for (let index = 0; index < ndc_code_data.length; index++) {
  //                   const element_ndc = ndc_code_data[index];
      
  //                   let row = {
  //                     'ndc_code': `${element_ndc.ndc_code}`,
  //                     'unii': `${element_ndc.unii}`,
  //                     'product_name': `${element_ndc.product_name}`,
  //                     'excipientname': `${element_ndc.excipientname}`,
  //                     'dailydosagelimit': `${element_ndc.dailydosagelimit}`,
  //                     'alternate_excipients': `${element_ndc.alternate_excipients}`,
  //                     'functional_category': `${element_ndc.functional_category}`,
  //                     'incompatibilities': `${element_ndc.incompatibilities}`
  //                   };
  //                   rows_arr_ndc.push(row);
  //                 }
  //                 worksheet_no_excipents.addRows(rows_arr_ndc);
  //             }
  //           }

  //           worksheet.addRows(rows_arr);
  //           var fileName = 'excipients_used.xlsx';

  //           res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
  //           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //           await workbook.xlsx.write(res);
  //           res.end();
  //         })
  //         .catch((err) => {
  //           res
  //             .status(400)
  //             .json({
  //               status: 3,
  //               message: Config.errorText.value,
  //             })
  //             .end();
  //         });
  //     }
  //   } catch (err) {
  //     res
  //       .status(400)
  //       .json({
  //         status: 3,
  //         message: Config.errorText.value,
  //       })
  //       .end();
  //   }

  // },

  task_enquiry: async (req, res, next) => {
    try {

      let customer_id = req.user.customer_id;

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalAdminLimit;
        var offset = ((page - 1) * globalAdminLimit);
      } else {
        var limit = globalAdminLimit;
        var offset = 0
      }

      await faModel.get_task_enquiry_list(customer_id, limit, offset)
        .then(async function (data) {
          const total = await faModel.count_task_enquiry(customer_id);
          res.status(200).json({
            status: 1,
            data: data,
            cnt: total[0].count,
          }).end();
        })
        .catch((err) => {
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    } catch (err) {
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },

  task_enquiry_details: async (req, res, next) => {
    try {

      let enquiry_id = req.body.id;

      await faModel.get_task_enquiry_details(enquiry_id)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data,
          }).end();
        })
        .catch((err) => {
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    } catch (err) {
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },

  get_excipients_list: async (req, res, next) => {
    try {

      if (req.query.page && req.query.page > 0) {
        var page = req.query.page;
        var limit = globalAdminLimit;
        var offset = ((page - 1) * globalAdminLimit);
      } else {
        var limit = globalAdminLimit;
        var offset = 0
      }

      var ndc_code = req.body.ndc_code;

      await faModel.get_excipients_list(ndc_code, limit, offset)
        .then(async function (data) {
          const total = await faModel.get_excipients_count(ndc_code);
          res.status(200).json({
            status: 1,
            data: data,
            cnt: total
          }).end();
        })
        .catch((err) => {
          console.log(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    } catch (err) {
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }

  },
  request_more_info: async (req, res, next) => {
    try {
      const { enquiry_type, product_name, comment } = req.value.body;

      //var dt = new Date();
      //dt.setHours(dt.getHours() + 2);
      //var d_date = dateFormat(dt, "yyyy-mm-dd HH:MM:ss");

      let d_date = dateFormat(common.nextDate(1,'minutes'), "yyyy-mm-dd HH:MM:ss");

      const Obj = {
        enquiry_type: enquiry_type,
        product_name: entities.encode(product_name),
        comment: entities.encode(comment),
        customer_id: req.user.customer_id,
        date_added: common.currentDateTime(),
        due_date: d_date,
        processed: '0'
      }

      await faModel.request_more_info(Obj)
        .then(async function (insertId) {

          let search_text = entities.encode(product_name);

          let ndc_codes = await faModel.get_ndc_codes(search_text);

          if(ndc_codes !== ''){
            let remove_white_space_ndc = ndc_codes.replace(/ /g,'');
            let ndc_arr = remove_white_space_ndc.split(',');

            if (enquiry_type == 1) {
              var workbook = new Excel.Workbook();
              var worksheet = workbook.addWorksheet('Orange Book details');
  
              await faModel.find_product_us_download_request_more_ndc(ndc_arr)
                .then(async function (data) {
  
                  worksheet.columns = [{
                    header: 'NDC Code',
                    key: 'ndc_code',
                    width: 30
                  },
                  {
                    header: 'Application Number',
                    key: 'applicationnumber',
                    width: 30
                  },
                  {
                    header: 'Organization',
                    key: 'organization',
                    width: 30
                  },
                  {
                    header: 'No Of Excepients',
                    key: 'no_of_excepients',
                    width: 30
                  },
                  {
                    header: 'Dosage Form',
                    key: 'dosage_form',
                    width: 30
                  },
                  {
                    header: 'Route	',
                    key: 'route',
                    width: 30
                  },
                  {
                    header: 'Color',
                    key: 'color',
                    width: 30
                  },
                  {
                    header: 'Shape',
                    key: 'shape',
                    width: 30
                  },
                  {
                    header: 'Sizes',
                    key: 'sizes',
                    width: 30
                  },
                  {
                    header: 'Strength',
                    key: 'strength',
                    width: 30
                  },
                  {
                    header: 'RLD',
                    key: 'rld',
                    width: 30
                  },
      
                  ];
      
                  let rows_arr = [];
                  for (let index = 0; index < data.length; index++) {
                    const element = data[index];
      
                    let row = {
                      'ndc_code': `${element.ndc_code}`,
                      'applicationnumber': `${element.applicationnumber}`,
                      'organization': `${element.organization}`,
                      'no_of_excepients': `${element.no_of_excepients}`,
                      'dosage_form': `${element.dosage_form}`,
                      'route': `${element.route}`,
                      'color': `${element.color}`,
                      'shape': `${element.shape}`,
                      'sizes': `${element.sizes}`,
                      'strength': `${element.strength}`,
                      'rld': `${element.rld}`,
                    };
                    rows_arr.push(row);
                  }
      
                  worksheet.addRows(rows_arr);
                  let fileName = `Orange-Book-details-${product_name}.xlsx`
                  var filePath = `${Config.upload.fa}${fileName}`;
                  await workbook.xlsx.writeFile(filePath).then(async () => {
  
                    var extention  = path.extname(fileName);
                    var new_file_name = 'fa_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
                    await module.exports.__upload_to_s3_bucket(new_file_name,fs.readFileSync(filePath));
                    let arr = [
                      fileName,
                      new_file_name,
                      insertId
                    ]
                    await faModel.update_request_more_info(arr);
                    fs.unlink(filePath,()=>{
                      console.log('File Unlinked');
                    });
                  });
  
                  //var fileName = 'family.xlsx';
  
                  //res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                  //res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                  //await workbook.xlsx.write(res);
                  
                  //res.end();
  
                  res.status(200)
                  .json({
                    status: 1,
                    message: `Added Successfully`
                  })
                  .end();
  
                })
                .catch((err) => {
                  console.log('err',err);
                  res
                    .status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value,
                    })
                    .end();
                });
            } else if (enquiry_type == 3) {
              var workbook = new Excel.Workbook();
              var worksheet = workbook.addWorksheet('EMA details');
              await faModel.find_product_ema_download_request_more_ndc(ndc_arr)
                .then(async function (data) {
                  worksheet.columns = [{
                    header: 'Product Name',
                    key: 'productname',
                    width: 30
                  },
                  {
                    header: 'NCD Code',
                    key: 'ndc_code',
                    width: 30
                  },
                  {
                    header: 'Product Number',
                    key: 'product_number',
                    width: 30
                  },
                  {
                    header: 'Authorisation Status',
                    key: 'authorisation_status',
                    width: 30
                  },
                  {
                    header: 'Therapeutic Area',
                    key: 'therapeutic_area',
                    width: 30
                  },
                  {
                    header: 'Generic',
                    key: 'generic',
                    width: 30
                  },
                  {
                    header: 'Biosimilar',
                    key: 'biosimilar',
                    width: 30
                  },
                  {
                    header: 'Additional Monitoring',
                    key: 'additional_monitoring',
                    width: 30
                  },
                  {
                    header: 'Orphan Drug',
                    key: 'orphan_drug',
                    width: 30
                  },
                  {
                    header: 'Marketing Date',
                    key: 'marketing_date',
                    width: 30
                  },
                  {
                    header: 'Pharma Class',
                    key: 'pharma_class',
                    width: 30
                  },
                  {
                    header: 'Company Name',
                    key: 'company_name',
                    width: 30
                  },
                  {
                    header: 'Published Date',
                    key: 'published_date',
                    width: 30
                  },
                  {
                    header: 'Indication',
                    key: 'indication',
                    width: 30
                  }
  
                  ];
  
                  let rows_arr = [];
                  for (let index = 0; index < data.length; index++) {
                    const element = data[index];
  
                    let row = {
                      'productname': `${element.productname}`,
                      'ndc_code': `${element.ndc_code}`,
                      'product_number': `${element.product_number}`,
                      'authorisation_status': `${element.authorisation_status}`,
                      'therapeutic_area': `${element.therapeutic_area}`,
                      'generic': `${element.generic}`,
                      'biosimilar': `${element.biosimilar}`,
                      'additional_monitoring': `${element.additional_monitoring}`,
                      'orphan_drug': `${element.orphan_drug}`,
                      'marketing_date': `${element.marketing_date}`,
                      'pharma_class': `${element.pharma_class}`,
                      'company_name': `${element.company_name}`,
                      'published_date': `${element.published_date}`,
                      'indication': `${element.indication}`
                    };
                    rows_arr.push(row);
                  }
  
                  worksheet.addRows(rows_arr);
                  let fileName = `EMA-details-${product_name}.xlsx`
                  var filePath = `${Config.upload.fa}${fileName}`;
                  await workbook.xlsx.writeFile(filePath).then(async () => {
  
                    var extention  = path.extname(fileName);
                    var new_file_name = 'fa_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
                    await module.exports.__upload_to_s3_bucket(new_file_name,fs.readFileSync(filePath));
                    let arr = [
                      fileName,
                      new_file_name,
                      insertId
                    ]
                    await faModel.update_request_more_info(arr);
                    fs.unlink(filePath,()=>{
                      console.log('File Unlinked');
                    });
                  });
  
                  res.status(200)
                  .json({
                    status: 1,
                    message: `Added Successfully`
                  })
                  .end();
  
                })
                .catch((err) => {
                  console.log('err',err);
                  res
                    .status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value,
                    })
                    .end();
                });
            } else if (enquiry_type == 5) {
              var workbook = new Excel.Workbook();
              var worksheet = workbook.addWorksheet('Patents Associated');
              await faModel.find_product_patent_download_request_more_ndc(ndc_arr)
                .then(async function (data) {
                  worksheet.columns = [
                  {
                    header: 'Product Name',
                    key: 'productname',
                    width: 30
                  },
                  {
                    header: 'Exclusivity Information',
                    key: 'exclusivity_information',
                    width: 30
                  },
                  {
                    header: 'Submission Date',
                    key: 'submission_date',
                    width: 30
                  },
                  {
                    header: 'Patent Expiry',
                    key: 'patentexpiry',
                    width: 30
                  },
                  {
                    header: 'DP	',
                    key: 'dp',
                    width: 30
                  },
                  {
                    header: 'DS',
                    key: 'ds',
                    width: 30
                  },
                  {
                    header: 'Current Assignee',
                    key: 'current_assignee',
                    width: 30
                  },
                  {
                    header: 'Patent Title',
                    key: 'patent_title',
                    width: 30
                  },
                  {
                    header: 'Global Dossier',
                    key: 'global_dossier',
                    width: 30
                  },
                  {
                    header: 'Patent Claim',
                    key: 'patent_claim',
                    width: 30
                  }
  
                  ];
  
                  let rows_arr = [];
                  for (let index = 0; index < data.length; index++) {
                    const element = data[index];
  
                    let row = {
                      'patent_number': `${element.patent_number}`,
                      'productname': `${element.productname}`,
                      'exclusivity_information': `${element.exclusivity_information}`,
                      'submission_date': `${element.submission_date}`,
                      'patentexpiry': `${element.patentexpiry}`,
                      'dp': `${element.dp}`,
                      'ds': `${element.ds}`,
                      'current_assignee': `${element.current_assignee}`,
                      'patent_title': `${element.patent_title}`,
                      'global_dossier': `${element.global_dossier}`,
                      'patent_claim': `${element.patent_claim}`
                    };
                    rows_arr.push(row);
                  }
  
                  worksheet.addRows(rows_arr);
                  let fileName = `Patents-Associated-${product_name}.xlsx`
                  var filePath = `${Config.upload.fa}${fileName}`;
                  await workbook.xlsx.writeFile(filePath).then(async () => {
  
                    var extention  = path.extname(fileName);
                    var new_file_name = 'fa_uploads/'+(Math.floor(Date.now() / 1000))+'-'+uuidv4()+extention;
                    await module.exports.__upload_to_s3_bucket(new_file_name,fs.readFileSync(filePath));
                    let arr = [
                      fileName,
                      new_file_name,
                      insertId
                    ]
                    await faModel.update_request_more_info(arr);
                    fs.unlink(filePath,()=>{
                      console.log('File Unlinked');
                    });
                  });
  
                  res.status(200)
                  .json({
                    status: 1,
                    message: `Added Successfully`
                  })
                  .end();
  
                })
                .catch((err) => {
                  console.log('err',err);
                  res
                    .status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value,
                    })
                    .end();
                });
            }

          }
          /* res.status(200)
            .json({
              status: 1,
              data: data
            }).end(); */
        }).catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          }).end();
        })
    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  enquiry_details: async (req, res, next) => {
    try {
      const enqiry_id = req.params.id;
      await faModel
        .enquiry_details(enqiry_id)
        .then(async function (data) {

          data[0].download_url = process.env.FULL_PATH_FA + '/api/fa/get_file/' + cryptr.encrypt(enqiry_id);

          res.status(200).json({
            status: 1,
            data: data
          });
        })
        .catch(err => {
          common.logError(err);
          res.status(400).json({
            status: 3,
            message: Config.errorText.value
          });
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value
        })
        .end();
    }
  },
  download_file: async (req, res, next) => {
    try {
      const key = cryptr.decrypt(req.value.params.ciphered_key);

      await faModel.enquiry_details_file(key)
        .then(async function (data) {

          AWS.config.update({
            accessKeyId: Config.aws.accessKeyId,
            secretAccessKey: Config.aws.secretAccessKey
          });
          var s3 = new AWS.S3();
          var actual_file_name = data[0].actual_file_name;
          var new_file_name = data[0].new_file_name;

          var options = {
            Bucket: Config.aws.bucketName,
            Key: new_file_name,
          };
          return new Promise((resolve, reject) => {
            s3.getObject(options, function (err, data) {
              if (err === null) {
                res.attachment(actual_file_name); // or whatever your logic needs
                res.send(data.Body);
              } else {
                res.status(400)
                  .json({
                    status: 3,
                    message: err
                  })
              }
            });
          });
        })
        .catch(err => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value
            })
            .end();
        });

    } catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 2,
        message: Config.errorText.value
      }).end();
    }
  },
  __upload_to_s3_bucket: async (file_name,file_content) => {
    return new Promise(function(resolve, reject) {
      let extention  = path.extname(file_name);
      let mime       = common.get_mime_type(extention);
      let buff = new Buffer(file_content, 'base64');
      let params = {
        Bucket: Config.aws.bucketName,
        Key: file_name, 
        Body: buff,
        ContentEncoding: 'base64',
        ContentType:`${mime}`
      };
      console.log(params);
      s3Config.putObject(params, function(err, data){
          if(err){ 
            reject(err);
          }else{
            resolve(data);
          }
      });
		});
  }

}