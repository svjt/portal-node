const JWT        = require('jsonwebtoken');
const request 	 = require("request");
const Config     = require('../configuration/config');
const sapModel   = require("../models/sap");
const taskModel  = require('../models/task');
const customerModel = require('../models/user');
const agentModel = require('../models/agents');
const employeeModel = require('../models/employees');
const common     = require('./common');
const xmlParser  = require('xml2json');
const dateFormat = require('dateformat');
const Entities   = require('html-entities').AllHtmlEntities;
const entities   = new Entities();
const md5        = require('md5') 
var fs           = require('fs');
const soapRequest = require('easy-soap-request');
var Hashids = require('hashids');
var hashids = new Hashids('', 7);
const globalLimit = 10;

module.exports = {

  /**
	 * Generates a JWT Token
	 * @author Soumyadeep Adhikary <soumyadeep@indusnet.co.in>
	 * @param {Object} req - HTTP request argument to the middleware function
	 * @param {Object} res - HTTP response argument to the middleware function
	 * @param {next} next  - Callback argument to the middleware function
	 * @return {Json Object}
	 */
	get_token: async (req, res, next) => {
      try{  //console.log(req.body);

      const {email,password} = req.body;  

      if(Config.sapAuth.email == email && Config.sapAuth.password == password ){
        let token = JWT.sign({
            iss  : 'Reddys',
            sap : '1'
          }, Config.jwt.secret);

        res.status(200).json({
          status     : 1,
          token      : token
          
        }).end();
      }else{
        let return_err = {status:2,message:"Invalid login details"};
        res.status(400).json(return_err).end();
      }
      
    }catch(err){
          common.logError(err);
          res.status(400).json({
        status  : 2,
        message :"Error in posting data."
      }).end();
    }
  },

  list_products: async (req, res, next) => {
    try {      
      let customer_id = req.user.customer_id;
      await sapModel.list_products(customer_id)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  list_products_code: async (req, res, next) => {
    try {      
      let product_id = req.value.params.id;
      await sapModel.list_products_code(product_id)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  list_shipto: async (req, res, next) => {
    try {
      let customer_id =  0;
      if (req.user.role == 2) {
        
      }else{
        customer_id = req.user.customer_id;
      }
      await sapModel.list_shipto(customer_id)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  list_soldto: async (req, res, next) => {
    try {      
      let customer_id = req.user.customer_id;
      await sapModel.list_soldto(customer_id)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  stock_task_details: async (req, res, next) => {
    try {
      await sapModel.stock_task_details(req.value.params.id)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  list_enquiries: async (req, res, next) => {
    try {
      if (req.query.page && req.query.page > 0) {
        var page = req.query.page; 
        var limit = globalLimit; // Total data to be shown.
        var offset = (page - 1) * globalLimit; // Works as SKIP
      } else {
        var limit = globalLimit;
        var offset = 0;
      }
      if(req.query.status){
        status = req.query.status; 
      }else{
        status = '';
      }

      let customer_id = 0;
      let submitted_by = 0;	//console.log(req.user);
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }

      await sapModel.list_enquiries(customer_id,status,limit, offset)
        .then(async function (data) {
          let count_enquiries = await sapModel.count_enquiries(customer_id,status);
          res.status(200).json({
            status: 1,
            data: data,
            count: count_enquiries
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  /**
    * @desc Send request to sap for availability check
    * @param 
    * @return json
  */
  check_stock_availability: async (req, res, next) => {
    try {
      const {
        product_name,
        product_code,
        market,
        quantity,
        unit,
        rdd,
        ship_to_party,
        additional_comment,
        as_admin
      } = req.body;      
      
      let customer_id = 0;
      let submitted_by = 0;
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }

      var now = common.currentDateTime();
      now = now.replace(/[^a-zA-Z0-9]/g, "");
      var rand = Math.floor((Math.random() * 1000) + 1);

      var ref_no = customer_id+now+rand;
      var sap_request_id = 0;
      //console.log(ref_no);

      if(ref_no!=''){
        rdd_req = rdd.replace(/[^a-zA-Z0-9]/g, "");
        var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:stoc="http://S4HANA/XCEED/StockAvailability"> <soapenv:Header/> <soapenv:Body> <stoc:MT_XCEED_StockAvailability_Req> <StockDetails> <Reference_Number>'+ref_no+'</Reference_Number> <Material_Number>'+product_code+'</Material_Number> <Quantity>'+quantity.toFixed(1)+'</Quantity> <Unit_Of_Measure>'+unit+'</Unit_Of_Measure> <RDD>'+rdd_req+'</RDD> <Ship_to_party>'+ship_to_party+'</Ship_to_party> <Market>'+market+'</Market> <Additional_Specs>';
        if(additional_comment!='' && additional_comment!=undefined){
          xml_Request += 'X';
        }
        xml_Request += '</Additional_Specs> <User_Role>';
        if(as_admin){//if(submitted_by!=0){
          xml_Request += 'X';
        }
        xml_Request += '</User_Role> </StockDetails> </stoc:MT_XCEED_StockAvailability_Req> </soapenv:Body> </soapenv:Envelope>';
        console.log("Sending XML : ", xml_Request);
        var headers, body, statusCode;
        
        if(xml_Request){
          const insObj = {
            request_id : ref_no,
            user_id : customer_id,
            rdd : rdd_req,
            product_sku : product_code,
            ship_to_party : ship_to_party,
            quantity : quantity,
            unit: unit,
            market : market,
            request_date : common.currentDateTime(),
            additional_specification:additional_comment,
            reservation_no : 1,
            is_admin : as_admin
          };
          sap_request_id = await sapModel.insertRequest(insObj);
          // console.log(sap_request_id);
        }
    console.log("Config.sapEnv : ", Config.sapEnv) ;
        if(Config.sapEnv=='qa'){
          const sampleHeaders = {
            'Content-Type': 'application/soap+xml;charset=UTF-8',
            'soapAction': Config.sap_stock_soap.soapAction,
            'Authorization': 'Basic '+Config.sap_stock_soap.authorization
          }; console.log("request---",xml_Request);
      console.log("sap url---",Config.sapSoapUrl);
          const { response } = await soapRequest({ url: Config.sapSoapUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000});
      console.log(response)
          var { headers, body, statusCode } = response;
      console.log("statusCode ----: ",statusCode);
          
        }
        if(Config.sapEnv=='local'){          
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>FAILED</Status><Message>RDD is in past</Message><RDD>20200821</RDD></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20200929</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20201026</EPDD><RDD>20200929</RDD><Total_Available_Quantity>500.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886544</FBPO_Number_RLT><EPDD>20201030</EPDD><RDD>20201121</RDD><Total_Available_Quantity>660.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>2</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20211130</EPDD><RDD>20201121</RDD><Total_Available_Quantity>620.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header /><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201016</Message><Itm_ID>1 </Itm_ID><FBPO_Number_RLT>0018916707</FBPO_Number_RLT><EPDD>20201031</EPDD><RDD>20201016</RDD><Total_Available_Quantity>5.000 </Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF><User_Role_Flag>X</User_Role_Flag></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>1 </Itm_ID_Additional><FBPO_Number_Additional>0018916707</FBPO_Number_Additional><EPDD_Additional>20201031</EPDD_Additional><Total_Available_Quantity_Additional>560.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>2 </Itm_ID_Additional><FBPO_Number_Additional>0018916708</FBPO_Number_Additional><EPDD_Additional>20201104</EPDD_Additional><Total_Available_Quantity_Additional>640.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>3 </Itm_ID_Additional><FBPO_Number_Additional>0018916709</FBPO_Number_Additional><EPDD_Additional>20201103</EPDD_Additional><Total_Available_Quantity_Additional>660.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          var statusCode = 200;
        }       

        let insLogObj = {
          request : xml_Request,
          response : body,
          method : 'POST',
          url : Config.sapSoapUrl,
          datetime : common.currentDateTime(),
          request_type : 'STOCK_ENQUIRY'
        };
        let logResponse = await sapModel.insertRequestLog(insLogObj);

        var body_data = xmlParser.toJson( body);
        var stack_details_arr = [];
        var response_arr = {};

        if(statusCode==200){
          body_data = JSON.parse(body_data,true);
          //console.log('JSON output', body_data);

          if(body_data !='' && body_data['SOAP:Envelope']!=''){
            stack_details_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_StockAvailability_Resp']['StockDetails'];
            response_arr.product_code = product_code;
            response_arr.market = market;
            response_arr.quantity = quantity.toFixed(1);
            response_arr.unit = unit;
            response_arr.rdd = common.formatDate(rdd,"dd-mm-yyyy");
            response_arr.ship_to_party = ship_to_party;
            response_arr.reference_no = ref_no;
            response_arr.display_full_quantity = false;
            response_arr.disabled = false;            
            response_arr.reservation_no = 1;


            let total_quantity = 0;
            var available_stock =[];
            var information_stock = [];
            var error_message = '';
            // console.log('Stack output',stack_details_arr);
            
            if(stack_details_arr['Status']=='FAILED'){
              console.log("error block");                            
              res.status(200).json({
                status: 2,
                message: stack_details_arr['Message']
              }).end();
            }else{    
              if(stack_details_arr['Status']=='SUCCESS'){   // for single StockDetails response
                var temp_arr = stack_details_arr;
                stack_details_arr = [];
                stack_details_arr.push(temp_arr);
              }
              
              for (let index = 0; index <= (stack_details_arr.length - 1); index++) {
                const element = stack_details_arr[index];
                var temp_element = {};  //console.log('Stack output',stack_details_arr[0]['Status']);
                //console.log(element);
                if(element['Status']=='FAILED'){
                  error_message = element['Message'];
                }else{
                  //if(submitted_by!=0){
                    if(element['FBPO_Number_Additional'] !=undefined && element['Total_Available_Quantity_Additional']!=undefined && as_admin){
                      //console.log("Additional")
                      temp_element['item_number'] = element['Itm_ID_Additional'];
                      temp_element['fbpo_number'] = element['FBPO_Number_Additional'];
                      temp_element['expected_date'] = element['EPDD_Additional'];
                      temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity_Additional']);
                      temp_element['quantity_unit'] = element['Unit_Of_Measure_Additional'];                      
                      information_stock.push(temp_element);                                       
                    }
                  //}else{
                    if(element['Total_Available_Quantity']!=undefined){
                      //console.log("Original")
                      temp_element['item_number'] = element['Itm_ID'];                      
                      temp_element['fbpo_number'] = element['FBPO_Number_RLT'];                      
                      temp_element['expected_date'] = element['EPDD'];
                      temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity']);
                      temp_element['quantity_unit'] = element['Unit_Of_Measure'];
                      temp_element['confirm'] = false;
                      temp_element['disabled'] = false;
                      temp_element['rtl_flag'] = (element['RLT_Flag']) ? true : false;
                      total_quantity += parseInt(element['Total_Available_Quantity']);
                      console.log(element['EPDD']+"=="+rdd_req);

                      temp_element['epdd_flag'] = true;
                      if(element['EPDD'] < rdd_req){                        
                        temp_element['rdd_flag'] = false;
                      }else{
                        temp_element['rdd_flag'] = true;
                      }  
                      
                      //console.log(temp_element);
                      let insObj1 = {
                        sap_request_id : sap_request_id.id,
                        fbpo_number : temp_element['fbpo_number'],
                        itm_ID : element['Itm_ID'],
                        epdd : element['EPDD'],
                        total_available_quantity : parseInt(element['Total_Available_Quantity']),
                        unit_of_measure: element['Unit_Of_Measure'],
                        rtl_flag : (element['RLT_Flag']) ? 1 : 0
                      };
                      let responseId = await sapModel.insertResponse(insObj1);   
                      
                      if(responseId){
                        temp_element['response_id'] = hashids.encode(responseId.id); //md5(responseId.id);
                        temp_element['act_response_id'] = responseId.id;
                      }else{
                        temp_element['response_id'] = 'RESPONSEID';
                        temp_element['act_response_id'] = 0;
                      }
                      available_stock.push(temp_element); 
                    }
                  //}
                }                                 
              }

              if(error_message!=''){
                res.status(200).json({
                  status: 2,
                  message: error_message
                }).end();
              }else{
                //console.log(available_stock);
                available_stock.sort((a, b) => parseFloat(a['expected_date']) - parseFloat(b['expected_date']));
                //information_stock.sort((c, d) => parseFloat(c['expected_date']) - parseFloat(d['expected_date']));
                //console.log(available_stock);
                response_arr.total_quantity = total_quantity;
                response_arr.available_stock = available_stock;
                response_arr.information_stock = information_stock;
                return res.status(200)
                .json({
                  status: 1,
                  stock_list: response_arr,
                })
              }
            }
          }else{
            res.status(200).json({
              status: 2,
              message: "Stock availability check failed."
            }).end();
          }
        }else{
          res.status(statusCode).json({
            status: 2,
            message: "Stock availability response code "+statusCode
          }).end();
        }  

      }else{       
        res.status(400).json({
          status: 2,
          message: "Unable to generate Reference Number"
        }).end();
      }
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }    
    
  },

  reserve_stock: async (req, res, next) => {
    try {
      const {
        request_id,
        supply_type,
        line_items,
        line_items_date
      } = req.body;

      let customer_id = 0;
      let product_id = 1;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }
      console.log("supply_type : " , supply_type);

      if(supply_type==9){
              
        await sapModel.getLastRequest(request_id)
          .then(async function (all_reserve) {
            console.log("Previous : ",all_reserve);
            if(all_reserve.success){
                return res.status(200)
                    .json({
                      status: 1,
                      redircted : 1,             
                  })
            }else{
              return res.status(200)
                    .json({
                      status: 1,
                      redircted : 0,             
                  });
            }
          }).catch((err) => {
            res.status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
          .end();
          });
      }else{
        await sapModel.getResevation(request_id, customer_id)
          .then(async function (data) { //console.log("DATA : ",data);
            if(data.success){ //console.log("DATA : ",data.data[0].product_sku + "=====" +data.data[0].user_id)
              let get_product_arr = await sapModel.getproductDetailsByCode(data.data[0].product_sku, data.data[0].user_id);
              if(get_product_arr.success){
                product_id = get_product_arr.data[0].id;
              }
              
              if(supply_type==1){
                let all_availables = await sapModel.getAllResevationList(data.data[0]);
                //console.log("Single : ",all_availables);

                let total_available = 0;  max_epdd = 0;
                for (let index = 0; index <= (all_availables.length - 1); index++) {                
                  total_available = parseInt(total_available) + parseInt(all_availables[index].total_available_quantity);   
                  if(max_epdd<all_availables[index].epdd){
                    max_epdd = all_availables[index].epdd;
                  }             
                }
                  for (let index = 0; index <= (all_availables.length - 1); index++) {                 
                    
                    let rdd_date_name = "rdd_check_"+all_availables[index].id;
                    let epdd_date_name = "expected_date_check_"+all_availables[index].id;

                    //console.log(rdd_date_name + "===="+ epdd_date_name);
                    if(line_items_date.indexOf(rdd_date_name) !== -1){
                      max_epdd = all_availables[index].rdd;
                      await sapModel.updatePreferedDateForSingle(all_availables[index].sap_request_id, 'rdd');
                      break;
                    }

                    if(line_items_date.indexOf(epdd_date_name) !== -1){
                      max_epdd = all_availables[index].epdd;
                      await sapModel.updatePreferedDateForSingle(all_availables[index].sap_request_id, 'epdd');
                      break;
                    }
                  }                
                
                  var headers, body, statusCode;
                  var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><!--Zero or more repetitions:-->';
          
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    
                    xml_Request += '<ReservationDetails><Main_Req>'+all_availables[index].request_id+'</Main_Req><Reservation_Request_No>'+all_availables[index].reservation_no+'</Reservation_Request_No><SO_ID>1</SO_ID><Identifier>CREATE</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number>'+all_availables[index].ship_to_party+'</Ship_to_party_Number><Material_Number>'+all_availables[index].product_sku+'</Material_Number><Market>'+all_availables[index].market+'</Market><Total_Order_Quantity>'+total_available+'</Total_Order_Quantity><Unit_Of_Measure>'+all_availables[index].req_unit+'</Unit_Of_Measure><Full_Qty_Single>X</Full_Qty_Single><Itm_ID>'+all_availables[index].itm_id+'</Itm_ID>';
                    if(all_availables[index].rtl_flag == 0 && all_availables[index].fbpo_number != 'RLT'){
                      xml_Request += '<FBPO_Number>'+all_availables[index].fbpo_number+'</FBPO_Number>';
                    }else{
                      xml_Request += '<FBPO_Number></FBPO_Number>';
                    }
                    xml_Request += '<EPDD>'+all_availables[index].epdd+'</EPDD><Request_Del_Date>'+max_epdd+'</Request_Del_Date><TotalAvailableQuantity>'+all_availables[index].total_available_quantity+'</TotalAvailableQuantity><UnitOfMeasure>'+all_availables[index].unit_of_measure+'</UnitOfMeasure>';
                    if(all_availables[index].rtl_flag == 1 && all_availables[index].additional_specification != ''){
                      xml_Request += '<RLT_Flag>Y</RLT_Flag>';
                    }else if(all_availables[index].rtl_flag == 1){
                      xml_Request += '<RLT_Flag>X</RLT_Flag>';
                    }else{
                      xml_Request += '<RLT_Flag></RLT_Flag>';
                    }                   
                    xml_Request += '</ReservationDetails>';
                  }
                  xml_Request += ' </res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
                  //console.log('Reservation XML : ',xml_Request);

                  if(Config.sapEnv=='qa'){
                    const sampleHeaders = {
                      'Content-Type': 'application/soap+xml;charset=UTF-8',
                      'soapAction': Config.sap_stock_soap.soapAction,
                      'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                    };
                    const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                    var { headers, body, statusCode } = response;
                    console.log(headers);
                    console.log(body);
                    console.log(statusCode);
                  }

                  if(Config.sapEnv=='local'){
                    var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
                    var statusCode = 200;
                  }

                  let insLogObj = {
                    request : xml_Request,
                    response : body,
                    method : 'POST',
                    url : Config.sapReservationUrl,
                    datetime : common.currentDateTime(),
                    request_type : 'STOCK_RESERVATION'
                  };
                  let logResponse = await sapModel.insertRequestLog(insLogObj);

                  var body_data = xmlParser.toJson(body);
                  var stack_reservation_arr = [];
                  var is_error = 0;                

                  if(statusCode==200){
                    body_data = JSON.parse(body_data,true);

                    if(body_data !='' && body_data['SOAP:Envelope']!=''){
                      stack_reservation_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
                      //console.log(stack_reservation_arr);

                      for (let index = 0; index <= (stack_reservation_arr.length - 1); index++) {
                        if(stack_reservation_arr[index]['STATUS']!='Success'){
                          is_error++;
                        }
                      }

                      if(is_error==0){
                        let updateRequest = await sapModel.updateAllRequestForReservation(data.data[0], total_available).catch(err => {
                          common.logError(err);
                          res.status(400)
                            .json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                        });;

                        var sla = await taskModel.get_total_sla(43);
                        var days = await common.dayCountExcludingWeekends(sla.total_sla);
                        var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
                        var temp_rdd = data.data[0].rdd;
                        var rdd = max_epdd.slice(0,4)+"-"+max_epdd.slice(4,6)+"-"+max_epdd.slice(6,8);
                        let taskObj = {
                          customer_id: data.data[0].user_id,
                          product_id: product_id,
                          country_id: 0,
                          parent_id: 0,
                          due_date: sla_due_date,
                          quantity: entities.encode(total_available+' '+data.data[0].unit),
                          current_date: today,
                          content: entities.encode(data.data[0].additional_specification),
                          request_type: 43,
                          submitted_by: 0,
                          rdd: rdd != '' && rdd != undefined ? entities.encode(rdd) : null,
                          sap_request_id: data.data[0].id,
                          so_id : 1,
                          language : 'en'
                        };
                        await sapModel.addTaskRequest(taskObj)
                          .then(async function (dataTask) {                          
                            let ref_no = 'PHL-E-' + dataTask.task_id;
                            var updateTaskRef = taskModel.update_ref_no(ref_no, dataTask.task_id).catch(err => {
                              common.logError(err);
                              res.status(400)
                                .json({
                                  status: 3,
                                  message: Config.errorText.value
                                }).end();
                            });
                            let CRObj = {
                              task_id : dataTask.task_id,
                              customer_id : data.data[0].user_id,
                              action_req : 'Document Required ',
                              status : 'PO Pending',
                              expected_closure_date : dateFormat(rdd, "yyyy-mm-dd HH:MM:ss"),
                              date_added : today
                            };
                            var insertCustomerResponse = sapModel.addCustomerResponse(CRObj).catch(err => {
                              common.logError(err);
                              res.status(400)
                                .json({
                                  status: 3,
                                  message: Config.errorText.value
                                }).end();
                            });
                            return res.status(200)
                            .json({
                              status: 1,
                              redircted: 1,                
                            })
                          }).catch(err => {
                            common.logError(err);
                            res.status(400).json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                          })
                      }else{
                        res.status(200).json({
                          status: 2,
                          message: "Stock reservation failed."
                        }).end();
                      }
                    }else{
                      res.status(200).json({
                        status: 2,
                        message: "Stock reservation failed."
                      }).end();
                    }

                  }else{
                    res.status(200).json({
                      status: 2,
                      message: "Stock reservation failed."
                    }).end();
                  }              
              }else if(supply_type==2){
                var line_items_arr = [];
                for (let ind = 0; ind <= (line_items.length - 1); ind++) {
                  line_items_arr.push(parseInt(line_items[ind]));
                }
                let all_availables = await sapModel.getSpecificResevationList(data.data[0],line_items_arr);
                if(all_availables.length > 0){
                  //console.log("Multiple : ",all_availables);
                  let total_available = 0;
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    total_available = total_available+ parseInt(all_availables[index].total_available_quantity);
                  }                
                
                  var headers, body, statusCode;
                  var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><!--Zero or more repetitions:-->';
          
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    all_availables[index].so_id =  index+1;

                    let rdd_date_name = "rdd_check_"+all_availables[index].id;
                    let epdd_date_name = "expected_date_check_"+all_availables[index].id;
                    let request_del_date = 0;
                    //console.log(rdd_date_name + "===="+ epdd_date_name);
                    if(line_items_date.indexOf(rdd_date_name) !== -1){
                      request_del_date = all_availables[index].rdd;
                      await sapModel.updatePreferedDateForMultiple(all_availables[index].id, 'rdd');
                    }

                    if(line_items_date.indexOf(epdd_date_name) !== -1){
                      request_del_date = all_availables[index].epdd;
                      await sapModel.updatePreferedDateForMultiple(all_availables[index].id, 'epdd');                
                    }
                    //console.log("Request_Del_Date : ",request_del_date);

                    xml_Request += '<ReservationDetails><Main_Req>'+all_availables[index].request_id+'</Main_Req><Reservation_Request_No>'+all_availables[index].reservation_no+'</Reservation_Request_No><SO_ID>'+all_availables[index].so_id+'</SO_ID><Identifier>CREATE</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number>'+all_availables[index].ship_to_party+'</Ship_to_party_Number><Material_Number>'+all_availables[index].product_sku+'</Material_Number><Market>'+all_availables[index].market+'</Market><Total_Order_Quantity>'+total_available+'</Total_Order_Quantity><Unit_Of_Measure>'+all_availables[index].req_unit+'</Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID>1</Itm_ID>';
                    if(all_availables[index].rtl_flag == 0 && all_availables[index].fbpo_number != 'RTL'){
                      xml_Request += '<FBPO_Number>'+all_availables[index].fbpo_number+'</FBPO_Number>';
                    }else{
                      xml_Request += '<FBPO_Number></FBPO_Number>';
                    }
                    xml_Request += '<EPDD>'+all_availables[index].epdd+'</EPDD><Request_Del_Date>'+request_del_date+'</Request_Del_Date><TotalAvailableQuantity>'+all_availables[index].total_available_quantity+'</TotalAvailableQuantity><UnitOfMeasure>'+all_availables[index].unit_of_measure+'</UnitOfMeasure>';                  
                    if(all_availables[index].rtl_flag == 1 && all_availables[index].additional_specification != ''){
                      xml_Request += '<RLT_Flag>Y</RLT_Flag>';
                    }else if(all_availables[index].rtl_flag == 1){
                      xml_Request += '<RLT_Flag>X</RLT_Flag>';
                    }else{
                      xml_Request += '<RLT_Flag></RLT_Flag>';
                    }                   
                    xml_Request += '</ReservationDetails>';
                  }

                  xml_Request += ' </res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';

                  //console.log('Reservation XML : ',xml_Request);

                  if(Config.sapEnv=='qa'){
                    const sampleHeaders = {
                      'Content-Type': 'application/soap+xml;charset=UTF-8',
                      'soapAction': Config.sap_stock_soap.soapAction,
                      'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                    };
                    const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                    var { headers, body, statusCode } = response;
                    console.log(headers);
                    console.log(body);
                    console.log(statusCode);
                  }

                  if(Config.sapEnv=='local'){
                    var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
                    var statusCode = 200;
                  }

                  var body_data = xmlParser.toJson(body);
                  var stack_reservation_arr = [];
                  var is_error = 0;

                  let insLogObj = {
                    request : xml_Request,
                    response : body,
                    method : 'POST',
                    url : Config.sapReservationUrl,
                    datetime : common.currentDateTime(),
                    request_type : 'STOCK_RESERVATION'
                  };
                  let logResponse = await sapModel.insertRequestLog(insLogObj);

                  if(statusCode==200){
                    body_data = JSON.parse(body_data,true);

                    if(body_data !='' && body_data['SOAP:Envelope']!=''){
                      stack_reservation_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
                      //console.log(stack_reservation_arr);

                      for (let index = 0; index <= (stack_reservation_arr.length - 1); index++) {
                        if(stack_reservation_arr[index]['STATUS']!='Success'){
                          is_error++;
                        }
                      }

                      if(is_error==0){
                        let updateRequest = await sapModel.updateSpecificRequestForReservation(data.data[0], line_items_arr, total_available).catch(err => {
                          common.logError(err);
                          res.status(400)
                            .json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                        });;

                        for (let index = 0; index <= (all_availables.length - 1); index++) {
                          var sla = await taskModel.get_total_sla(43);
                          var days = await common.dayCountExcludingWeekends(sla.total_sla);
                          var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
                          var temp_rdd = all_availables[index].epdd;
                          var rdd = temp_rdd.slice(0,4)+"-"+temp_rdd.slice(4,6)+"-"+temp_rdd.slice(6,8);
                          let taskObj = {
                            customer_id: all_availables[index].user_id,
                            product_id: product_id,
                            country_id: 0,
                            parent_id: 0,
                            due_date: sla_due_date,
                            quantity: entities.encode(all_availables[index].total_available_quantity+' '+all_availables[index].req_unit),
                            current_date: today,
                            content: entities.encode(all_availables[index].additional_specification),
                            request_type: 43,
                            submitted_by: 0,
                            rdd: rdd != '' && rdd != undefined ? entities.encode(rdd) : null,
                            sap_request_id: all_availables[index].sap_request_id,
                            so_id: all_availables[index].so_id,
                            language : 'en'
                          };
                          await sapModel.addTaskRequest(taskObj)
                            .then(async function (dataTask) {                          
                              let ref_no = 'PHL-E-' + dataTask.task_id;
                              var updateTaskRef = taskModel.update_ref_no(ref_no, dataTask.task_id).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });;
                              let CRObj = {
                                task_id : dataTask.task_id,
                                customer_id : all_availables[index].user_id,
                                action_req : 'Document Required ',
                                status : 'PO Pending',
                                expected_closure_date : dateFormat(rdd, "yyyy-mm-dd HH:MM:ss"),
                                date_added : today
                              };
                              var insertCustomerResponse = sapModel.addCustomerResponse(CRObj).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });
                              var updateResponseSO = sapModel.updateResponseSO(all_availables[index].id, all_availables[index].so_id).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });;                             
                            }).catch(err => {
                              common.logError(err);
                              res.status(400).json({
                                status: 3,
                                message: Config.errorText.value
                              }).end();
                            })
                        }
                        return res.status(200)
                          .json({
                            status: 1,
                            redircted : 1,                
                          })
                      }else{
                        res.status(200).json({
                          status: 2,
                          message: "Stock reservation failed."
                        }).end();
                      }
                    }else{
                      res.status(200).json({
                        status: 2,
                        message: "Stock reservation failed."
                      }).end();
                    }

                  }else{
                    res.status(200).json({
                      status: 2,
                      message: "Stock reservation failed."
                    }).end();
                  }  
                  
                  
                }else{
                  
                  res.status(400)
                    .json({
                      status: 3,
                      message: 'Available stock not found.',
                    })
                  .end();
                }
              }else{              
                res.status(400)
                  .json({
                    status: 3,
                    message: 'Reservation type not mentioned.',
                  })
                .end();
              }
            }else{ 
                res.status(400)
                  .json({
                    status: 3,
                    message: data.message,
                  })
                .end();
            }
          })
          .catch((err) => {
            common.logError(err);
            res
              .status(400)
              .json({
                status: 3,
                message: Config.errorText.value,
              })
              .end();
          });
      }
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },


  
  /**
		* @desc Send request to sap for new availability check
		* @param 
		* @return json
	*/
  check_stock_availability_new: async (req, res, next) => {
    try {
      const {
        product_name,
        product_code,
        market,
        quantity,
        productunit,
        rdd,
        ship_to_party,
        additional_comment,
        as_admin,
        cc_customers,
        share_with_agent
      } = req.body;      //console.log(req.body);
      
      let customer_id = 0;
      let submitted_by = 0;
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {  //console.log(req.user);
        if(req.user.role == 2){
          customer_id = req.body.agent_customer_id;
        }else{
          customer_id = req.user.customer_id;
        }        
      }

      var now = common.currentDateTime();
      now = now.replace(/[^a-zA-Z0-9]/g, "");
      var rand = Math.floor((Math.random() * 1000) + 1);

      var ref_no = customer_id+now+rand;
      var sap_request_id = 0;

      let country_details = await sapModel.get_country_by_id(market);
      let country_name = "";
      if(country_details.length > 0){
        country_name = country_details[0].country_name;
      }
      //console.log(ref_no);
      let unit = '';
      let cc_cust = [];

      if (cc_customers) {
        let cc_cust_arr = JSON.parse(cc_customers);
        //INSERT CC CUSTOMERS
        if (cc_cust_arr.length > 0) {
          for (let index = 0; index < cc_cust_arr.length; index++) {
            const element = cc_cust_arr[index].customer_id;
            cc_cust.push(element);            
          }
        }
      }

      switch (productunit.toLowerCase()) {
        case 'kgs': unit = 'KG'; break;
        case 'gms': unit = 'G'; break;
        case 'mgs': unit = 'MG'; break;
        case 'ea': unit = 'EA'; break;
        case 'th': unit = 'TH'; break;
        case 'l': unit = 'L'; break;
        case 'su': unit = 'SU'; break;
        default: unit = 'KG';
      }


      if(ref_no!=''){
        rdd_req = rdd.replace(/[^a-zA-Z0-9]/g, "");
        var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:stoc="http://S4HANA/XCEED/StockAvailability"> <soapenv:Header/> <soapenv:Body> <stoc:MT_XCEED_StockAvailability_Req> <StockDetails> <Reference_Number>'+ref_no+'</Reference_Number> <Material_Number>'+product_code+'</Material_Number> <Quantity>'+quantity.toFixed(1)+'</Quantity> <Unit_Of_Measure>'+unit+'</Unit_Of_Measure> <RDD>'+rdd_req+'</RDD> <Ship_to_party>'+ship_to_party+'</Ship_to_party> <Market>'+entities.decode(country_name)+'</Market> <Additional_Specs>';
        if(additional_comment!='' && additional_comment!=undefined){
          xml_Request += 'X';
        }
        xml_Request += '</Additional_Specs> <User_Role>';
        if(as_admin || submitted_by!=0){//if(submitted_by!=0){
          xml_Request += 'X';
        }
        xml_Request += '</User_Role> </StockDetails> </stoc:MT_XCEED_StockAvailability_Req> </soapenv:Body> </soapenv:Envelope>';
        console.log("Request XML : ", xml_Request);
        var headers, body, statusCode;
        
        if(xml_Request){
          const insObj = {
            request_id : ref_no,
            user_id : customer_id,
            rdd : rdd_req,
            product_sku : product_code,
            ship_to_party : ship_to_party,
            quantity : quantity,
            unit: unit,
            market : country_name,
            request_date : common.currentDateTime(),
            additional_specification:additional_comment,
            reservation_no : 1,
            is_admin : (as_admin || submitted_by!=0) ? 1 : 0,
            share_with_agent: (share_with_agent!='' && share_with_agent!=undefined) ? share_with_agent : 1 ,
            cc_customers : (cc_cust.length > 0) ? JSON.stringify(cc_cust) : ''
          };
          sap_request_id = await sapModel.insertRequest(insObj);
          console.log("SAP Request Id : ", sap_request_id);
        }

        if(Config.sapEnv=='qa'){
          const sampleHeaders = {
            'Content-Type': 'application/soap+xml;charset=UTF-8',
            'soapAction': Config.sap_stock_soap.soapAction,
            'Authorization': 'Basic '+Config.sap_stock_soap.authorization
          };
          const { response } = await soapRequest({ url: Config.sapSoapUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000});
          var { headers, body, statusCode } = response;
          
        }
        if(Config.sapEnv=='local'){          
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>FAILED</Status><Message>RDD is in past</Message><RDD>20200821</RDD></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20200929</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20201026</EPDD><RDD>20200929</RDD><Total_Available_Quantity>500.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886544</FBPO_Number_RLT><EPDD>20201030</EPDD><RDD>20201121</RDD><Total_Available_Quantity>660.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>2</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20211130</EPDD><RDD>20201121</RDD><Total_Available_Quantity>620.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header /><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201016</Message><Itm_ID>1 </Itm_ID><FBPO_Number_RLT>0018916707</FBPO_Number_RLT><EPDD>20201031</EPDD><RDD>20201016</RDD><Total_Available_Quantity>5.000 </Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF><User_Role_Flag>X</User_Role_Flag></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>1 </Itm_ID_Additional><FBPO_Number_Additional>0018916707</FBPO_Number_Additional><EPDD_Additional>20201031</EPDD_Additional><Total_Available_Quantity_Additional>560.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>2 </Itm_ID_Additional><FBPO_Number_Additional>0018916708</FBPO_Number_Additional><EPDD_Additional>20201104</EPDD_Additional><Total_Available_Quantity_Additional>640.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>3 </Itm_ID_Additional><FBPO_Number_Additional>0018916709</FBPO_Number_Additional><EPDD_Additional>20201103</EPDD_Additional><Total_Available_Quantity_Additional>660.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
          var statusCode = 200;
        }       

        let insLogObj = {
          request : xml_Request,
          response : body,
          method : 'POST',
          url : Config.sapSoapUrl,
          datetime : common.currentDateTime(),
          request_type : 'STOCK_ENQUIRY_NEW'
        };
        let logResponse = await sapModel.insertRequestLog(insLogObj);

        var body_data = xmlParser.toJson( body);
        var stack_details_arr = [];
        var response_arr = {};

        if(statusCode==200){
          body_data = JSON.parse(body_data,true);
          //console.log('JSON output', body_data);

          if(body_data !='' && body_data['SOAP:Envelope']!=''){
            stack_details_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_StockAvailability_Resp']['StockDetails'];
            response_arr.product_code = product_code;
            response_arr.market = market;
            response_arr.quantity = quantity.toFixed(1);
            response_arr.unit = productunit;
            response_arr.rdd = common.formatDate(rdd,"dd-mm-yyyy");
            response_arr.ship_to_party = ship_to_party;
            response_arr.reference_no = ref_no;
            response_arr.display_full_quantity = false;
            response_arr.disabled = false;            
            response_arr.reservation_no = 1;
            


            let total_quantity = 0;
            var available_stock =[];
            var information_stock = [];
            var error_message = '';
            // console.log('Stack output',stack_details_arr);
            
            if(stack_details_arr['Status']=='FAILED'){
              console.log("error block");                            
              res.status(200).json({
                status: 2,
                message: stack_details_arr['Message']
              }).end();
            }else{    
              if(stack_details_arr['Status']=='SUCCESS'){   // for single StockDetails response
                var temp_arr = stack_details_arr;
                stack_details_arr = [];
                stack_details_arr.push(temp_arr);
              }
              
              for (let index = 0; index <= (stack_details_arr.length - 1); index++) {
                const element = stack_details_arr[index];
                var temp_element = {};  //console.log('Stack output',stack_details_arr[0]['Status']);
                //console.log(element);
                if(element['Status']=='FAILED'){
                  error_message = element['Message'];
                }else{
                  //if(submitted_by!=0){
                    if(element['FBPO_Number_Additional'] !=undefined && element['Total_Available_Quantity_Additional']!=undefined && (as_admin || submitted_by!=0)){
                      //console.log("Additional")
                      temp_element['item_number'] = element['Itm_ID_Additional'];
                      temp_element['fbpo_number'] = element['FBPO_Number_Additional'];
                      temp_element['expected_date'] = element['EPDD_Additional'];
                      temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity_Additional']);
                      //temp_element['quantity_unit'] = element['Unit_Of_Measure_Additional'];
                      
                      switch (element['Unit_Of_Measure_Additional'].toLowerCase()) {
                        case 'kg': temp_element['quantity_unit'] = 'Kgs'; break;
                        case 'g': temp_element['quantity_unit'] = 'Gms'; break;
                        case 'mg': temp_element['quantity_unit'] = 'Mgs'; break;
                        case 'ea': temp_element['quantity_unit'] = 'EA'; break;
                        case 'th': temp_element['quantity_unit'] = 'TH'; break;
                        case 'l': temp_element['quantity_unit'] = 'L'; break;
                        case 'su': temp_element['quantity_unit'] = 'SU'; break;
                        default: temp_element['quantity_unit'] = '';
                      }
                      information_stock.push(temp_element);                                       
                    }
                  //}else{
                    if(element['Total_Available_Quantity']!=undefined){
                      //console.log("Original")
                      temp_element['item_number'] = element['Itm_ID'];                      
                      temp_element['fbpo_number'] = element['FBPO_Number_RLT'];                      
                      temp_element['expected_date'] = element['EPDD'];
                      temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity']);
                      //temp_element['quantity_unit'] = element['Unit_Of_Measure'];
                      temp_element['confirm'] = false;
                      temp_element['disabled'] = false;
                      temp_element['rtl_flag'] = (element['RLT_Flag']) ? true : false;
                      total_quantity += parseInt(element['Total_Available_Quantity']);
                      console.log(element['EPDD']+"=="+rdd_req);

                      switch (element['Unit_Of_Measure'].toLowerCase()) {
                        case 'kg': temp_element['quantity_unit'] = 'Kgs'; break;
                        case 'g': temp_element['quantity_unit'] = 'Gms'; break;
                        case 'mg': temp_element['quantity_unit'] = 'Mgs'; break;
                        case 'ea': temp_element['quantity_unit'] = 'EA'; break;
                        case 'th': temp_element['quantity_unit'] = 'TH'; break;
                        case 'l': temp_element['quantity_unit'] = 'L'; break;
                        case 'su': temp_element['quantity_unit'] = 'SU'; break;
                        default: temp_element['quantity_unit'] = '';
                      }

                      temp_element['epdd_flag'] = true;
                      if(element['EPDD'] < rdd_req){                        
                        temp_element['rdd_flag'] = false;
                      }else{
                        temp_element['rdd_flag'] = true;
                      }  
                      
                      //console.log(temp_element);
                      let insObj1 = {
                        sap_request_id : sap_request_id.id,
                        fbpo_number : temp_element['fbpo_number'],
                        itm_ID : element['Itm_ID'],
                        epdd : element['EPDD'],
                        total_available_quantity : parseInt(element['Total_Available_Quantity']),
                        unit_of_measure: element['Unit_Of_Measure'],
                        rtl_flag : (element['RLT_Flag']) ? 1 : 0
                      };
                      let responseId = await sapModel.insertResponse(insObj1);   
                      
                      if(responseId){
                        temp_element['response_id'] = hashids.encode(responseId.id); //md5(responseId.id);
                        temp_element['act_response_id'] = responseId.id;
                      }else{
                        temp_element['response_id'] = 'RESPONSEID';
                        temp_element['act_response_id'] = 0;
                      }
                      available_stock.push(temp_element); 
                    }
                  //}
                }                                 
              }

              if(error_message!=''){
                res.status(200).json({
                  status: 2,
                  message: error_message
                }).end();
              }else{
                //console.log(available_stock);
                available_stock.sort((a, b) => parseFloat(a['expected_date']) - parseFloat(b['expected_date']));
                //information_stock.sort((c, d) => parseFloat(c['expected_date']) - parseFloat(d['expected_date']));
                //console.log(available_stock);
                response_arr.total_quantity = total_quantity;
                response_arr.available_stock = available_stock;
                response_arr.information_stock = information_stock;
                return res.status(200)
                .json({
                  status: 1,
                  stock_list: response_arr,
                })
              }
            }
          }else{
            res.status(200).json({
              status: 2,
              message: "Stock availability check failed."
            }).end();
          }
        }else{
          res.status(statusCode).json({
            status: 2,
            message: "Stock availability response code "+statusCode
          }).end();
        }  

      }else{       
        res.status(400).json({
          status: 2,
          message: "Unable to generate Reference Number"
        }).end();
      }
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }    
    
  },

  reserve_stock_new: async (req, res, next) => {
    try {
      const {
        request_id,
        supply_type,
        line_items,
        line_items_date
      } = req.body;

      let customer_id = 0;
      let product_id = 1;
      let submitted_by = 0;
      let request_type_id = 43;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        if(req.user.role == 2){
          customer_id = req.body.agent_customer_id;
        }else{
          customer_id = req.user.customer_id;
        }
      }

      let language = '';
      if (req.user.empe > 0) {
        language = await customerModel.get_customer_language(customer_id);
      } else {
        language = await customerModel.get_customer_language(customer_id);
      }

      console.log("language:" , language);

      if(supply_type==9){
              
        await sapModel.getLastRequest(request_id)
          .then(async function (all_reserve) {
            console.log("Previous : ",all_reserve);
            if(all_reserve.success){
                return res.status(200)
                    .json({
                      status: 1,
                      redircted : 1,             
                  })
            }else{
              return res.status(200)
                    .json({
                      status: 1,
                      redircted : 0,             
                  });
            }
          }).catch((err) => {
            res.status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
          .end();
          });
      }else{
        await sapModel.getResevation(request_id, customer_id)
          .then(async function (data) { //console.log("DATA : ",data);
            if(data.success){ console.log("DATA : ",data.data[0].product_sku + "=====" +data.data[0].user_id)
              let get_product_arr = await sapModel.getmasterproductByCode(data.data[0].product_sku);
              if(get_product_arr.success){
                product_id = get_product_arr.data[0].id;
              }
              let country_name = "";
              
              if(supply_type==1){
                let all_availables = await sapModel.getAllResevationList(data.data[0]);
                //console.log("Single : ",all_availables);

                let total_available = 0;  max_epdd = 0;
                for (let index = 0; index <= (all_availables.length - 1); index++) {                
                  total_available = parseInt(total_available) + parseInt(all_availables[index].total_available_quantity);   
                  if(max_epdd<all_availables[index].epdd){
                    max_epdd = all_availables[index].epdd;
                  }             
                }
                  for (let index = 0; index <= (all_availables.length - 1); index++) {                 
                    
                    let rdd_date_name = "rdd_check_"+all_availables[index].id;
                    let epdd_date_name = "expected_date_check_"+all_availables[index].id;
                    country_name = all_availables[index].market;

                    //console.log(rdd_date_name + "===="+ epdd_date_name);
                    if(line_items_date.indexOf(rdd_date_name) !== -1){
                      max_epdd = all_availables[index].rdd;
                      await sapModel.updatePreferedDateForSingle(all_availables[index].sap_request_id, 'rdd');
                      break;
                    }

                    if(line_items_date.indexOf(epdd_date_name) !== -1){
                      max_epdd = all_availables[index].epdd;
                      await sapModel.updatePreferedDateForSingle(all_availables[index].sap_request_id, 'epdd');
                      break;
                    }
                  }                
                
                  var headers, body, statusCode;
                  var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><!--Zero or more repetitions:-->';
          
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    
                    xml_Request += '<ReservationDetails><Main_Req>'+all_availables[index].request_id+'</Main_Req><Reservation_Request_No>'+all_availables[index].reservation_no+'</Reservation_Request_No><SO_ID>1</SO_ID><Identifier>CREATE</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number>'+all_availables[index].ship_to_party+'</Ship_to_party_Number><Material_Number>'+all_availables[index].product_sku+'</Material_Number><Market>'+entities.decode(all_availables[index].market)+'</Market><Total_Order_Quantity>'+total_available+'</Total_Order_Quantity><Unit_Of_Measure>'+all_availables[index].req_unit+'</Unit_Of_Measure><Full_Qty_Single>X</Full_Qty_Single><Itm_ID>'+all_availables[index].itm_id+'</Itm_ID>';
                    if(all_availables[index].rtl_flag == 0 && all_availables[index].fbpo_number != 'RLT'){
                      xml_Request += '<FBPO_Number>'+all_availables[index].fbpo_number+'</FBPO_Number>';
                    }else{
                      xml_Request += '<FBPO_Number></FBPO_Number>';
                    }
                    xml_Request += '<EPDD>'+all_availables[index].epdd+'</EPDD><Request_Del_Date>'+max_epdd+'</Request_Del_Date><TotalAvailableQuantity>'+all_availables[index].total_available_quantity+'</TotalAvailableQuantity><UnitOfMeasure>'+all_availables[index].unit_of_measure+'</UnitOfMeasure>';
                    if(all_availables[index].rtl_flag == 1 && all_availables[index].additional_specification != ''){
                      xml_Request += '<RLT_Flag>Y</RLT_Flag>';
                    }else if(all_availables[index].rtl_flag == 1){
                      xml_Request += '<RLT_Flag>X</RLT_Flag>';
                    }else{
                      xml_Request += '<RLT_Flag></RLT_Flag>';
                    }                   
                    xml_Request += '</ReservationDetails>';
                  }
                  xml_Request += ' </res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
                  //console.log('Reservation XML : ',xml_Request);

                  if(Config.sapEnv=='qa'){
                    const sampleHeaders = {
                      'Content-Type': 'application/soap+xml;charset=UTF-8',
                      'soapAction': Config.sap_stock_soap.soapAction,
                      'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                    };
                    const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                    var { headers, body, statusCode } = response;
                    console.log(headers);
                    console.log(body);
                    console.log(statusCode);
                  }

                  if(Config.sapEnv=='local'){
                    var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
                    var statusCode = 200;
                  }

                  let insLogObj = {
                    request : xml_Request,
                    response : body,
                    method : 'POST',
                    url : Config.sapReservationUrl,
                    datetime : common.currentDateTime(),
                    request_type : 'STOCK_RESERVATION_NEW'
                  };
                  let logResponse = await sapModel.insertRequestLog(insLogObj);

                  var body_data = xmlParser.toJson(body);
                  var stack_reservation_arr = [];
                  var is_error = 0;                

                  if(statusCode==200){
                    body_data = JSON.parse(body_data,true);

                    if(body_data !='' && body_data['SOAP:Envelope']!=''){
                      stack_reservation_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
                      //console.log(stack_reservation_arr);

                      for (let index = 0; index <= (stack_reservation_arr.length - 1); index++) {
                        if(stack_reservation_arr[index]['STATUS']!='Success'){
                          is_error++;
                        }
                      }

                      if(is_error==0){
                        let updateRequest = await sapModel.updateAllRequestForReservation(data.data[0], total_available).catch(err => {
                          common.logError(err);
                          res.status(400)
                            .json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                        });;

                        var sla = await taskModel.get_total_sla(43);
                        var days = await common.dayCountExcludingWeekends(sla.total_sla);
                        var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
                        var temp_rdd = data.data[0].rdd;
                        var rdd = max_epdd.slice(0,4)+"-"+max_epdd.slice(4,6)+"-"+max_epdd.slice(6,8);
                        let data_unit = '';
                        switch(data.data[0].unit.toLowerCase()){
                          case 'kg' : data_unit = 'Kgs';	break;
                          case 'g'  : data_unit = 'Gms';	break;
                          case 'mg' : data_unit = 'Mgs';	break;
                          default   : data_unit = '';
                        }
                        let taskObj = {
                          customer_id: data.data[0].user_id,
                          product_id: product_id,
                          country_id: 0,
                          parent_id: 0,
                          due_date: sla_due_date,
                          quantity: entities.encode(total_available+' '+data_unit),
                          current_date: today,
                          content: entities.encode(data.data[0].additional_specification),
                          request_type: 43,
                          submitted_by: 0,
                          rdd: rdd != '' && rdd != undefined ? entities.encode(rdd) : null,
                          sap_request_id: data.data[0].id,
                          so_id : 1,
                          language:language,
                          share_with_agent: data.data[0].share_with_agent,
                        };
                        if(req.user.role == 2){
                          taskObj.submitted_by = req.user.customer_id;
                        }
                        await sapModel.addTaskRequest(taskObj)
                          .then(async (dataTask) => {    
                            let task_id = dataTask.task_id;
                            let task_details = await taskModel.tasks_details(task_id);

                            

                            if (language != Config.default_language) {
                              await module.exports.__translate_language(taskObj, task_id);
                            }

                            let manager = await taskModel.get_allocated_manager(taskObj.customer_id).catch(err => {
                              common.logError(err);
                              res.status(400)
                                .json({
                                  status: 3,
                                  message: Config.errorText.value
                                }).end();
                            });

                            let ref_no = 'PHL-O-' + task_id;
                            let customerData = [];
                            await taskModel.update_ref_no(ref_no, task_id).then(async (dataRef) =>{
                              // ACTIVITY LOG - SATYAJIT
                              customerData = await customerModel.get_user(taskObj.customer_id);
                              var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
                              var params = {
                                task_ref: ref_no,
                                request_type: task_details[0].req_name,
                                customer_name: customername
                              }
                              if (req.user.role == 2) {
                                var agentData = await agentModel.get_user(taskObj.submitted_by);
                                params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;console.log(params);
                                await taskModel.log_activity(2, task_id, params)
                              } else {
                                await taskModel.log_activity(1, task_id, params)
                              }
                            })
                            .catch(async (err) => {
                              common.logError(err);
                              res.status(400)
                                .json({
                                  status: 3,
                                  message: Config.errorText.value
                                }).end();
                            });
                            
                            if (data.data[0].cc_customers) {
                              let cc_cust_arr = JSON.parse(data.data[0].cc_customers);
                              //INSERT CC CUSTOMERS
                              if (cc_cust_arr.length > 0) {
                                for (let index = 0; index < cc_cust_arr.length; index++) {
                                  const element = cc_cust_arr[index];
                                  let insrt_cc = {
                                    customer_id: element,
                                    task_id: task_id,
                                    status: 1
                                  };
                                  await customerModel.insert_cc_customer(insrt_cc);
                                }
                              }
                            }

                            if (data.data[0].share_with_agent == 1) {
                              let customer_details = await customerModel.get_user(data.data[0].user_id);
                              let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);
              
                              if (agent_list && agent_list.length > 0) {
                                for (let index = 0; index < agent_list.length; index++) {
                                  const element = agent_list[index];
                                  let highlight_agent_arr = [
                                    task_id,
                                    element.agent_id,
                                    1,
                                    'A'
                                  ];
                                  await taskModel.task_highlight(highlight_agent_arr);
                                }
                              }
                            }

                            let comment_arr = [];
                            task_details = await taskModel.tasks_details(task_id);
                            manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

                            if(manager.length > 0){

                              var customer_details = await customerModel.get_user(task_details[0].customer_id);

                              var mail_subject = '';
                              if (task_details[0].request_type == 24) {
                                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
                              } else {
                                mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
                              }

                              var emp_details = [];
                              await taskModel.update_task_owner(manager[0].employee_id, task_id).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });

                              await sapModel.get_country(all_availables[0].market).then(async function(country_details){
                                if(country_details.length){
                                  taskModel.map_countries(dataTask.task_id, country_details[0].id)
                                }
                              }).catch(err => {
                                common.logError(err);
                                res.status(400).json({
                                  status: 3,
                                  message: Config.errorText.value
                                }).end();
                              })

                              const assignment = {
                                task_id: task_id,
                                assigned_to: manager[0].employee_id,
                                assigned_by: -1,
                                assign_date: today,
                                due_date: sla_due_date,
                                reopen_date: today,
                                parent_assignment_id: 0
                              }
                
                              var parent_assign_id = await taskModel.assign_task(assignment).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });

                              // ACTIVITY LOG FROM SYSTEM - SATYAJIT
                              var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
                              var params = {
                                task_ref: ref_no,
                                request_type: task_details[0].req_name,
                                employee_sender: 'SYSTEM',
                                employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
                              }
                              await taskModel.log_activity(8, task_id, params);
                              // END LOG

                              let employee_notification = 22;
                              let param_notification = {};
                              let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                              let cc_arr = [
                                employee_notification,
                                task_id,
                                manager[0].employee_id,
                                common.currentDateTime(),
                                0,
                                notification_text,
                                employee_notification,
                                mail_subject
                              ]
                              await taskModel.notify_employee(cc_arr);

                              //ASSIGN TO COMMERCIAL
                              emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);

                              emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

                              if (emp_details.length > 0) {
                                let assignment_val = 2;
                                await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                                .then(async function (data) {

                                  const assign_tasks = {
                                    assigned_by: manager[0].employee_id,
                                    assigned_to: emp_details[0].employee_id,
                                    task_id: task_id,
                                    due_date: sla_due_date,
                                    assign_date: today,
                                    parent_assignment_id: parent_assign_id.assignment_id,
                                    comment: '',
                                    reopen_date: today
                                  }

                                  await taskModel.assign_task(assign_tasks)
                                    .then(async function (tdata) {

                                      // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                                      var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                                      var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                                      var params = {
                                        task_ref: ref_no,
                                        request_type: task_details[0].req_name,
                                        employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                                        employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                                      }
                                      await taskModel.log_activity(8, task_id, params)
                                      // END LOG

                                      bot_assignment_id = tdata.assignment_id;

                                      let employee_notification = 8;
                                      let param_notification = {
                                        employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                                        assigned_employee: `you`
                                      };
                                      let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                                      let cc_arr = [
                                        employee_notification,
                                        task_id,
                                        emp_details[0].employee_id,
                                        common.currentDateTime(),
                                        0,
                                        notification_text,
                                        employee_notification,
                                        mail_subject
                                      ]
                                      await taskModel.notify_employee(cc_arr);

                                      const task_comment = {
                                        posted_by: manager[0].employee_id,
                                        task_id: task_id,
                                        comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
                                        assignment_id: bot_assignment_id,
                                        post_date: today
                                      };

                                      await taskModel.insert_comment_log(task_comment);

                                      comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

                                      //console.log('child assignment',bot_assignment_id);

                                    }).catch(err => {
                                      common.logError(err);
                                      res.status(400)
                                        .json({
                                          status: 3,
                                          message: Config.errorText.value
                                        }).end();
                                    });

                                }).catch(err => {
                                  common.logError(err);
                                  res.status(400)
                                    .json({
                                      status: 3,
                                      message: Config.errorText.value
                                    }).end();
                                })
                              }


                              //CURRENT OWNER
                              if (emp_details.length > 0) {
                                await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
                              } else {
                                await taskModel.update_current_owner(task_id, manager[0].employee_id).catch(err => {
                                  common.logError(err);
                                  res.status(400)
                                    .json({
                                      status: 3,
                                      message: Config.errorText.value
                                    }).end();
                                });
                              }

                              //HIGHLIGHT NEW TASK
                              let customer_id = taskObj.customer_id;

                              let highlight_arr = [
                                task_id,
                                customer_id,
                                1,
                                'C'
                              ];
                              await taskModel.task_highlight(highlight_arr);


                              var country = await taskModel.tasks_details_countries(task_id);

                              if (country && country[0].country_name != '') {
                                var country_name = country[0].country_name;
                              } else {
                                var country_name = '';
                              }

                              var country_name_by_lang = country_name;
                              if (language != Config.default_language) {
                                //COUNTRY
                                let t_country = await taskModel.get_translated_country(task_id, language);
                                country_name_by_lang = t_country[0].country_name;
                              }

                              var db_due_date = new Date(task_details[0].due_date);
                              var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

                              let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

                              var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

                              /* sending email */
                              //console.log(mail_arr);
                              if(mail_arr.length > 0 && mail_arr[0].to_arr.length > 0){
                                //customer mail
                                let mailParam = {
                                  first_name: entities.decode(customer_details[0].first_name),
                                  last_name: entities.decode(customer_details[0].last_name),                
                                  task_ref: task_details[0].task_ref,
                                  product_name: task_details[0].product_name,
                                  country_name: country_name_by_lang,
                                  task_content: entities.decode(task_details[0].content),
                                  url_status: url_status,
                                  company_name: entities.decode(customer_details[0].company_name),
                                  req_name: task_details[0].req_name,
                                  quantity: task_details[0].quantity,
                                  rdd:  common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy')                
                                };                           
                                var mailcc = '';
                                if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                                  mailcc = mail_arr[1].cc_arr;
                                }
                                
                                const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_SAP_ORDER',mail_arr[0].to_arr,Config.contactUsMail,mailcc,entities.decode(task_details[0].language),mailParam,"c");
                              }
                
                              //Manager Mail
                              let managerMailParam = {
                                first_name: entities.decode(customer_details[0].first_name),
                                last_name: entities.decode(customer_details[0].last_name),
                                task_ref: task_details[0].task_ref,
                                product_name: task_details[0].product_name,
                                country_name: country_name_by_lang,
                                format_due_date: format_due_date,
                                task_content: entities.decode(task_details[0].content),
                                company_name: entities.decode(customer_details[0].company_name),
                                req_name: task_details[0].req_name,
                                quantity: task_details[0].quantity,
                                rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                                ccpurl: Config.ccpUrl,
                                emp_first_name: manager[0].first_name
                              };
                              const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAP_ORDER',manager[0].email,Config.webmasterMail,'',managerMailParam,"e");
                
                              if (emp_details.length > 0) {
                                //Employee Mail
                                var empMailParam = {
                                  first_name: entities.decode(customer_details[0].first_name),
                                  last_name: entities.decode(customer_details[0].last_name),
                                  task_ref: task_details[0].task_ref,
                                  product_name: task_details[0].product_name,
                                  country_name: country_name_by_lang,
                                  format_due_date: format_due_date,
                                  task_content: entities.decode(task_details[0].content),
                                  company_name: entities.decode(customer_details[0].company_name),
                                  req_name: task_details[0].req_name,
                                  quantity: task_details[0].quantity,
                                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                                  ccpurl: Config.ccpUrl,
                                  emp_first_name: emp_details[0].first_name
                                };
                                const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAP_ORDER',emp_details[0].email,Config.webmasterMail,'',empMailParam,"e");
                              }
                            }else{
                              //Unallocated Task Soumyadeep
                              var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
                              common.logError(err);
                            }
                           
                            
                            let CRObj = {
                              task_id : dataTask.task_id,
                              customer_id : data.data[0].user_id,
                              action_req : 'Document Required',
                              status : 'PO Pending',
                              expected_closure_date : (rdd!=null) ? dateFormat(rdd, "yyyy-mm-dd HH:MM:ss") : today,
                              date_added : today
                            };
                            var insertCustomerResponse = sapModel.addCustomerResponse(CRObj);
                            return res.status(200)
                            .json({
                              status: 1,
                              redircted: 1,                
                            })
                          }).catch(err => {
                            common.logError(err);
                            res.status(400).json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                          })
                      }else{
                        res.status(200).json({
                          status: 2,
                          message: "Stock reservation failed."
                        }).end();
                      }
                    }else{
                      res.status(200).json({
                        status: 2,
                        message: "Stock reservation failed."
                      }).end();
                    }

                  }else{
                    res.status(200).json({
                      status: 2,
                      message: "Stock reservation failed."
                    }).end();
                  }              
              }else if(supply_type==2){
                var line_items_arr = [];
                for (let ind = 0; ind <= (line_items.length - 1); ind++) {
                  line_items_arr.push(parseInt(line_items[ind]));
                }
                let all_availables = await sapModel.getSpecificResevationList(data.data[0],line_items_arr);
                if(all_availables.length > 0){
                  //console.log("Multiple : ",all_availables);
                  let total_available = 0;
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    total_available = total_available+ parseInt(all_availables[index].total_available_quantity);
                  }                
                
                  var headers, body, statusCode;
                  var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><!--Zero or more repetitions:-->';
          
                  for (let index = 0; index <= (all_availables.length - 1); index++) {
                    all_availables[index].so_id =  index+1;

                    let rdd_date_name = "rdd_check_"+all_availables[index].id;
                    let epdd_date_name = "expected_date_check_"+all_availables[index].id;
                    let request_del_date = 0;
                    //console.log(rdd_date_name + "===="+ epdd_date_name);
                    if(line_items_date.indexOf(rdd_date_name) !== -1){
                      request_del_date = all_availables[index].rdd;
                      await sapModel.updatePreferedDateForMultiple(all_availables[index].id, 'rdd');
                    }

                    if(line_items_date.indexOf(epdd_date_name) !== -1){
                      request_del_date = all_availables[index].epdd;
                      await sapModel.updatePreferedDateForMultiple(all_availables[index].id, 'epdd');                
                    }
                    //console.log("Request_Del_Date : ",request_del_date);

                    xml_Request += '<ReservationDetails><Main_Req>'+all_availables[index].request_id+'</Main_Req><Reservation_Request_No>'+all_availables[index].reservation_no+'</Reservation_Request_No><SO_ID>'+all_availables[index].so_id+'</SO_ID><Identifier>CREATE</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number>'+all_availables[index].ship_to_party+'</Ship_to_party_Number><Material_Number>'+all_availables[index].product_sku+'</Material_Number><Market>'+entities.decode(all_availables[index].market)+'</Market><Total_Order_Quantity>'+total_available+'</Total_Order_Quantity><Unit_Of_Measure>'+all_availables[index].req_unit+'</Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID>1</Itm_ID>';
                    if(all_availables[index].rtl_flag == 0 && all_availables[index].fbpo_number != 'RTL'){
                      xml_Request += '<FBPO_Number>'+all_availables[index].fbpo_number+'</FBPO_Number>';
                    }else{
                      xml_Request += '<FBPO_Number></FBPO_Number>';
                    }
                    xml_Request += '<EPDD>'+all_availables[index].epdd+'</EPDD><Request_Del_Date>'+request_del_date+'</Request_Del_Date><TotalAvailableQuantity>'+all_availables[index].total_available_quantity+'</TotalAvailableQuantity><UnitOfMeasure>'+all_availables[index].unit_of_measure+'</UnitOfMeasure>';                  
                    if(all_availables[index].rtl_flag == 1 && all_availables[index].additional_specification != ''){
                      xml_Request += '<RLT_Flag>Y</RLT_Flag>';
                    }else if(all_availables[index].rtl_flag == 1){
                      xml_Request += '<RLT_Flag>X</RLT_Flag>';
                    }else{
                      xml_Request += '<RLT_Flag></RLT_Flag>';
                    }                   
                    xml_Request += '</ReservationDetails>';
                  }

                  xml_Request += ' </res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';

                  //console.log('Reservation XML : ',xml_Request);

                  if(Config.sapEnv=='qa'){
                    const sampleHeaders = {
                      'Content-Type': 'application/soap+xml;charset=UTF-8',
                      'soapAction': Config.sap_stock_soap.soapAction,
                      'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                    };
                    const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                    var { headers, body, statusCode } = response;
                    console.log(headers);
                    console.log(body);
                    console.log(statusCode);
                  }

                  if(Config.sapEnv=='local'){
                    var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>CREATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
                    var statusCode = 200;
                  }

                  var body_data = xmlParser.toJson(body);
                  var stack_reservation_arr = [];
                  var is_error = 0;

                  let insLogObj = {
                    request : xml_Request,
                    response : body,
                    method : 'POST',
                    url : Config.sapReservationUrl,
                    datetime : common.currentDateTime(),
                    request_type : 'STOCK_RESERVATION'
                  };
                  let logResponse = await sapModel.insertRequestLog(insLogObj);

                  if(statusCode==200){
                    body_data = JSON.parse(body_data,true);

                    if(body_data !='' && body_data['SOAP:Envelope']!=''){
                      stack_reservation_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
                      //console.log(stack_reservation_arr);

                      for (let index = 0; index <= (stack_reservation_arr.length - 1); index++) {
                        if(stack_reservation_arr[index]['STATUS']!='Success'){
                          is_error++;
                        }
                      }

                      if(is_error==0){
                        let updateRequest = await sapModel.updateSpecificRequestForReservation(data.data[0], line_items_arr, total_available).catch(err => {
                          common.logError(err);
                          res.status(400)
                            .json({
                              status: 3,
                              message: Config.errorText.value
                            }).end();
                        });;

                        for (let index = 0; index <= (all_availables.length - 1); index++) {
                          var sla = await taskModel.get_total_sla(43);
                          var days = await common.dayCountExcludingWeekends(sla.total_sla);
                          var sla_due_date = dateFormat(common.nextDate(days, "day"), "yyyy-mm-dd HH:MM:ss");
                          var temp_rdd = all_availables[index].epdd;
                          var rdd = temp_rdd.slice(0,4)+"-"+temp_rdd.slice(4,6)+"-"+temp_rdd.slice(6,8);
                          let data_unit = '';
                          switch(all_availables[index].req_unit.toLowerCase()){
                            case 'kg' : data_unit = 'Kgs';	break;
                            case 'g'  : data_unit = 'Gms';	break;
                            case 'mg' : data_unit = 'Mgs';	break;
                            default   : data_unit = '';
                          }
                          let taskObj = {
                            customer_id: all_availables[index].user_id,
                            product_id: product_id,
                            country_id: 0,
                            parent_id: 0,
                            due_date: sla_due_date,
                            quantity: entities.encode(all_availables[index].total_available_quantity+' '+data_unit),
                            current_date: today,
                            content: entities.encode(all_availables[index].additional_specification),
                            request_type: 43,
                            submitted_by: 0,
                            rdd: rdd != '' && rdd != undefined ? entities.encode(rdd) : null,
                            sap_request_id: all_availables[index].sap_request_id,
                            so_id: all_availables[index].so_id,
                            language:language,
                            share_with_agent: data.data[0].share_with_agent
                          };
                          if(req.user.role == 2){
                            taskObj.submitted_by = req.user.customer_id;
                          }
                          await sapModel.addTaskRequest(taskObj)
                            .then(async function (dataTask) {  
                              let task_id = dataTask.task_id;
                              let task_details = await taskModel.tasks_details(task_id);  

                              if (language != Config.default_language) {
                                await module.exports.__translate_language(taskObj, task_id);
                              }

                              let manager = await taskModel.get_allocated_manager(taskObj.customer_id).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });
                              
                              let ref_no = 'PHL-O-' + task_id;
                              let customerData = [];
                              await taskModel.update_ref_no(ref_no, task_id).then(async (dataRef) =>{
                                // ACTIVITY LOG - SATYAJIT
                                customerData = await customerModel.get_user(taskObj.customer_id);
                                var customername = `${customerData[0].first_name} ${customerData[0].last_name}`;
                                
                                var params = {
                                  task_ref: ref_no,
                                  request_type: task_details[0].req_name,
                                  customer_name: customername
                                }
                                if (req.user.role == 2) {
                                  var agentData = await agentModel.get_user(taskObj.submitted_by);
                                  params.agent_name = `${agentData[0].first_name} ${agentData[0].last_name} (Agent)`;
                                  console.log(params);
                                  await taskModel.log_activity(2, task_id, params)
                                } else {
                                  await taskModel.log_activity(1, task_id, params)
                                }
                              })
                              .catch(async (err) => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });

                              if (data.data[0].cc_customers) {
                                let cc_cust_arr = JSON.parse(data.data[0].cc_customers);
                                //INSERT CC CUSTOMERS
                                if (cc_cust_arr.length > 0) {
                                  for (let index = 0; index < cc_cust_arr.length; index++) {
                                    const element = cc_cust_arr[index];
                                    let insrt_cc = {
                                      customer_id: element,
                                      task_id: task_id,
                                      status: 1
                                    };
                                    await customerModel.insert_cc_customer(insrt_cc);
                                  }
                                }
                              }

                              if (data.data[0].share_with_agent == 1) {
                                let customer_details = await customerModel.get_user(data.data[0].user_id);
                                let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);
                
                                if (agent_list && agent_list.length > 0) {
                                  for (let index = 0; index < agent_list.length; index++) {
                                    const element = agent_list[index];
                                    let highlight_agent_arr = [
                                      task_id,
                                      element.agent_id,
                                      1,
                                      'A'
                                    ];
                                    await taskModel.task_highlight(highlight_agent_arr);
                                  }
                                }
                              }

                              let comment_arr = [];
                              task_details = await taskModel.tasks_details(task_id);  
                              manager = await module.exports.__get_allocated_leave_employee(manager, taskObj.customer_id);

                              if(manager.length > 0){
                                

                                var customer_details = await customerModel.get_user(task_details[0].customer_id);

                                var mail_subject = '';
                                if (task_details[0].request_type == 24) {
                                  mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${task_details[0].task_ref}`;
                                } else {
                                  mail_subject = `${entities.decode(customer_details[0].company_name)} | ${entities.decode(customer_details[0].first_name)} | ${entities.decode(task_details[0].product_name)} | ${task_details[0].task_ref}`;
                                }

                                var emp_details = [];
                                await taskModel.update_task_owner(manager[0].employee_id, task_id).catch(err => {
                                  common.logError(err);
                                  res.status(400)
                                    .json({
                                      status: 3,
                                      message: Config.errorText.value
                                    }).end();
                                });

                                const assignment = {
                                  task_id: task_id,
                                  assigned_to: manager[0].employee_id,
                                  assigned_by: -1,
                                  assign_date: today,
                                  due_date: sla_due_date,
                                  reopen_date: today,
                                  parent_assignment_id: 0
                                }
                  
                                var parent_assign_id = await taskModel.assign_task(assignment).catch(err => {
                                  common.logError(err);
                                  res.status(400)
                                    .json({
                                      status: 3,
                                      message: Config.errorText.value
                                    }).end();
                                });

                                // ACTIVITY LOG FROM SYSTEM - SATYAJIT
                                var employee_recipient = `${manager[0].first_name} ${manager[0].last_name}`;
                                var params = {
                                  task_ref: ref_no,
                                  request_type: task_details[0].req_name,
                                  employee_sender: 'SYSTEM',
                                  employee_recipient: employee_recipient + ` (${(manager[0].desig_name)})`
                                }
                                await taskModel.log_activity(8, task_id, params);
                                // END LOG

                                let employee_notification = 22;
                                let param_notification = {};
                                let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                                let cc_arr = [
                                  employee_notification,
                                  task_id,
                                  manager[0].employee_id,
                                  common.currentDateTime(),
                                  0,
                                  notification_text,
                                  employee_notification,
                                  mail_subject
                                ]
                                await taskModel.notify_employee(cc_arr);

                                await sapModel.get_country(all_availables[0].market).then(async function(country_details){
                                  if(country_details.length){
                                    taskModel.map_countries(dataTask.task_id, country_details[0].id)
                                  }
                                }).catch(err => {
                                  common.logError(err);
                                  res.status(400).json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                                })

                                //ASSIGN TO COMMERCIAL
                                emp_details = await taskModel.get_allocated_employee(taskObj.customer_id, 5);

                                emp_details = await module.exports.__get_allocated_leave_employee(emp_details, taskObj.customer_id);

                                if (emp_details.length > 0) {
                                  let assignment_val = 2;
                                  await taskModel.update_task_assignment(task_id, manager[0].employee_id, assignment_val)
                                  .then(async function (data) {

                                    const assign_tasks = {
                                      assigned_by: manager[0].employee_id,
                                      assigned_to: emp_details[0].employee_id,
                                      task_id: task_id,
                                      due_date: sla_due_date,
                                      assign_date: today,
                                      parent_assignment_id: parent_assign_id.assignment_id,
                                      comment: '',
                                      reopen_date: today
                                    }

                                    await taskModel.assign_task(assign_tasks)
                                      .then(async function (tdata) {

                                        // ACTIVITY LOG FROM ASSIGNER TO ASSIGNEE - SATYAJIT
                                        var emp_sender = `${manager[0].first_name} ${manager[0].last_name}`;
                                        var emp_recipient = `${emp_details[0].first_name} ${emp_details[0].last_name}`;
                                        var params = {
                                          task_ref: ref_no,
                                          request_type: task_details[0].req_name,
                                          employee_sender: emp_sender + ` (${(manager[0].desig_name)})`,
                                          employee_recipient: emp_recipient + ` (${(emp_details[0].desig_name)})`
                                        }
                                        await taskModel.log_activity(8, task_id, params)
                                        // END LOG

                                        bot_assignment_id = tdata.assignment_id;

                                        let employee_notification = 8;
                                        let param_notification = {
                                          employee_name: `${manager[0].first_name} (${(manager[0].desig_name)})`,
                                          assigned_employee: `you`
                                        };
                                        let notification_text = await taskModel.get_notification_text(employee_notification, param_notification);

                                        let cc_arr = [
                                          employee_notification,
                                          task_id,
                                          emp_details[0].employee_id,
                                          common.currentDateTime(),
                                          0,
                                          notification_text,
                                          employee_notification,
                                          mail_subject
                                        ]
                                        await taskModel.notify_employee(cc_arr);

                                        const task_comment = {
                                          posted_by: manager[0].employee_id,
                                          task_id: task_id,
                                          comment: `This task has been auto assigned to ${emp_recipient} (${emp_details[0].desig_name}) by the system on my behalf.`,
                                          assignment_id: bot_assignment_id,
                                          post_date: today
                                        };

                                        await taskModel.insert_comment_log(task_comment);

                                        comment_arr.push({comment:task_comment.comment,posted_by:task_comment.posted_by});

                                        //console.log('child assignment',bot_assignment_id);

                                      }).catch(err => {
                                        common.logError(err);
                                        res.status(400)
                                          .json({
                                            status: 3,
                                            message: Config.errorText.value
                                          }).end();
                                      });

                                  }).catch(err => {
                                    common.logError(err);
                                    res.status(400)
                                      .json({
                                        status: 3,
                                        message: Config.errorText.value
                                      }).end();
                                  })
                                }


                                //CURRENT OWNER
                                if (emp_details.length > 0) {
                                  await taskModel.update_current_owner(task_id, emp_details[0].employee_id);
                                } else {
                                  await taskModel.update_current_owner(task_id, manager[0].employee_id).catch(err => {
                                    common.logError(err);
                                    res.status(400)
                                      .json({
                                        status: 3,
                                        message: Config.errorText.value
                                      }).end();
                                  });
                                }

                                //HIGHLIGHT NEW TASK
                                let customer_id = taskObj.customer_id;

                                let highlight_arr = [
                                  task_id,
                                  customer_id,
                                  1,
                                  'C'
                                ];
                                await taskModel.task_highlight(highlight_arr);


                                var country = await taskModel.tasks_details_countries(task_id);
                                //console.log(country);
                                if (country && country[0].country_name != '') {
                                  var country_name = country[0].country_name;
                                } else {
                                  var country_name = '';
                                }

                                var country_name_by_lang = country_name;
                                if (language != Config.default_language) {
                                  //COUNTRY
                                  let t_country = await taskModel.get_translated_country(task_id, language);
                                  country_name_by_lang = t_country[0].country_name;
                                }

                                var db_due_date = new Date(task_details[0].due_date);
                                var format_due_date = common.changeDateFormat(db_due_date, '/', 'dd.mm.yyyy');

                                let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

                                var url_status = `${process.env.REACT_API_URL}task-details/${task_id}`;

                                //console.log(mail_arr);
                              if(mail_arr.length > 0 && mail_arr[0].to_arr.length > 0){
                                //customer mail
                                let mailParam = {
                                  first_name: entities.decode(customer_details[0].first_name),
                                  last_name: entities.decode(customer_details[0].last_name),                
                                  task_ref: task_details[0].task_ref,
                                  product_name: task_details[0].product_name,
                                  country_name: country_name_by_lang,
                                  task_content: entities.decode(task_details[0].content),
                                  url_status: url_status,
                                  company_name: entities.decode(customer_details[0].company_name),
                                  req_name: task_details[0].req_name,
                                  quantity: task_details[0].quantity,
                                  rdd:  common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy')                
                                };                           
                                var mailcc = '';
                                if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                                  mailcc = mail_arr[1].cc_arr;
                                }
                                //console.log(mailParam);
                                const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_SAP_ORDER',mail_arr[0].to_arr,Config.contactUsMail,mailcc,entities.decode(task_details[0].language),mailParam,"c");
                              }
                
                              //Manager Mail
                              let managerMailParam = {
                                first_name: entities.decode(customer_details[0].first_name),
                                last_name: entities.decode(customer_details[0].last_name),
                                task_ref: task_details[0].task_ref,
                                product_name: task_details[0].product_name,
                                country_name: country_name_by_lang,
                                format_due_date: format_due_date,
                                task_content: entities.decode(task_details[0].content),
                                company_name: entities.decode(customer_details[0].company_name),
                                req_name: task_details[0].req_name,
                                quantity: task_details[0].quantity,
                                rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                                ccpurl: Config.ccpUrl,
                                emp_first_name: manager[0].first_name
                              };//console.log(managerMailParam);
                              const managermailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAP_ORDER',manager[0].email,Config.webmasterMail,'',managerMailParam,"e");
                
                              if (emp_details.length > 0) {
                                //Employee Mail
                                var empMailParam = {
                                  first_name: entities.decode(customer_details[0].first_name),
                                  last_name: entities.decode(customer_details[0].last_name),
                                  task_ref: task_details[0].task_ref,
                                  product_name: task_details[0].product_name,
                                  country_name: country_name_by_lang,
                                  format_due_date: format_due_date,
                                  task_content: entities.decode(task_details[0].content),
                                  company_name: entities.decode(customer_details[0].company_name),
                                  req_name: task_details[0].req_name,
                                  quantity: task_details[0].quantity,
                                  rdd: common.changeDateFormat(task_details[0].rdd, '/', 'dd.mm.yyyy'),
                                  ccpurl: Config.ccpUrl,
                                  emp_first_name: emp_details[0].first_name
                                };
                                //console.log(empMailParam);
                                
                                const empmailSent = common.sendMailByCodeB2E('EMPLOYEE_TASK_TEMPLATE_FOR_SAP_ORDER',emp_details[0].email,Config.webmasterMail,'',empMailParam,"e");
                              }

                              }else{
                                //Unallocated Task Soumyadeep
                                var err = new Error(`Customer has no allocated manager Task ID: ${task_id} Customer ID: ${taskObj.customer_id}`);
                                common.logError(err);
                              }
                              

                              let CRObj = {
                                task_id : dataTask.task_id,
                                customer_id : all_availables[index].user_id,
                                action_req : 'Document Required	',
                                status : 'PO Pending',
                                expected_closure_date : (rdd!=null) ? dateFormat(rdd, "yyyy-mm-dd HH:MM:ss") : today,
                                date_added : today
                              };
                              var insertCustomerResponse = sapModel.addCustomerResponse(CRObj);
                              var updateResponseSO = sapModel.updateResponseSO(all_availables[index].id, all_availables[index].so_id).catch(err => {
                                common.logError(err);
                                res.status(400)
                                  .json({
                                    status: 3,
                                    message: Config.errorText.value
                                  }).end();
                              });;                             
                            }).catch(err => {
                              common.logError(err);
                              res.status(400).json({
                                status: 3,
                                message: Config.errorText.value
                              }).end();
                            })
                        }
                        return res.status(200)
                          .json({
                            status: 1,
                            redircted : 1,                
                          })
                      }else{
                        res.status(200).json({
                          status: 2,
                          message: "Stock reservation failed."
                        }).end();
                      }
                    }else{
                      res.status(200).json({
                        status: 2,
                        message: "Stock reservation failed."
                      }).end();
                    }

                  }else{
                    res.status(200).json({
                      status: 2,
                      message: "Stock reservation failed."
                    }).end();
                  }  
                  
                  
                }else{
                  
                  res.status(400)
                    .json({
                      status: 3,
                      message: 'Available stock not found.',
                    })
                  .end();
                }
              }else{              
                res.status(400)
                  .json({
                    status: 3,
                    message: 'Reservation type not mentioned.',
                  })
                .end();
              }
            }else{ 
                res.status(400)
                  .json({
                    status: 3,
                    message: data.message,
                  })
                .end();
            }
          })
          .catch((err) => {
            common.logError(err);
            res
              .status(400)
              .json({
                status: 3,
                message: Config.errorText.value,
              })
              .end();
          });
      }
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  __get_allocated_leave_employee: async (emp_details, customer_id) => {
    let ret_details;
    if (emp_details && emp_details.length > 0) {
      if (emp_details[0].on_leave == 1) {
        var new_emp_dets_one = await taskModel.get_allocated_leave_employee(emp_details[0].employee_id, customer_id);

        if (new_emp_dets_one && new_emp_dets_one.length > 0 && new_emp_dets_one[0].desig_id == emp_details[0].desig_id) {

          if (new_emp_dets_one[0].on_leave == 1) {
            var new_emp_dets_two = await taskModel.get_allocated_leave_employee(new_emp_dets_one[0].employee_id, customer_id);

            if (new_emp_dets_two && new_emp_dets_two.length > 0 && new_emp_dets_two[0].desig_id == new_emp_dets_one[0].desig_id) {

              if (new_emp_dets_two[0].on_leave == 1) {
                var new_emp_dets_three = await taskModel.get_allocated_leave_employee(new_emp_dets_two[0].employee_id, customer_id);

                if (new_emp_dets_three && new_emp_dets_three.length > 0 && new_emp_dets_three[0].desig_id == new_emp_dets_two[0].desig_id) {

                  if (new_emp_dets_three[0].on_leave == 1) {
                    var new_emp_dets_four = await taskModel.get_allocated_leave_employee(new_emp_dets_three[0].employee_id, customer_id);

                    if (new_emp_dets_four && new_emp_dets_four.length > 0 && new_emp_dets_four[0].desig_id == new_emp_dets_three[0].desig_id) {

                      if (new_emp_dets_four[0].on_leave == 1) {
                        var new_emp_dets_five = await taskModel.get_allocated_leave_employee(new_emp_dets_four[0].employee_id, customer_id);

                        if (new_emp_dets_five && new_emp_dets_five.length > 0 && new_emp_dets_five[0].desig_id == new_emp_dets_four[0].desig_id) {

                          if (new_emp_dets_five[0].on_leave == 1) {
                            var new_emp_dets_six = await taskModel.get_allocated_leave_employee(new_emp_dets_five[0].employee_id, customer_id);

                            if (new_emp_dets_six && new_emp_dets_six.length > 0 && new_emp_dets_six[0].desig_id == new_emp_dets_five[0].desig_id) {
                              ret_details = new_emp_dets_six;
                            } else {
                              ret_details = new_emp_dets_five;
                            }
                          } else {
                            ret_details = new_emp_dets_five;
                          }
                        } else {
                          ret_details = new_emp_dets_four;
                        }
                      } else {
                        ret_details = new_emp_dets_four;
                      }
                    } else {
                      ret_details = new_emp_dets_three;
                    }
                  } else {
                    ret_details = new_emp_dets_three;
                  }
                } else {
                  ret_details = new_emp_dets_two;
                }
              } else {
                ret_details = new_emp_dets_two;
              }
            } else {
              ret_details = new_emp_dets_one;
            }
          } else {
            ret_details = new_emp_dets_one;
          }
        } else {
          ret_details = emp_details;
        }
      } else {
        ret_details = emp_details;
      }
    } else {
      ret_details = [];
    }

    return ret_details;
  },
  __translate_language: async (taskObj, task_id) => {
    let translate_obj = {
      task_id: task_id,
      content: '',
      pharmacopoeia: '',
      polymorphic_form: '',
      stability_data_type: '',
      audit_visit_site_name: '',
      quantity: '',
      batch_number: '',
      nature_of_issue: '',
      working_quantity: '',
      impurities_quantity: '',
      specify_impurity_required: '',
      shipping_address: '',
      number_of_batches: '',
      payment_orders: '',
      payment_status: '',
      payment_pending: '',
      change_category: '',
      notification_action: '',
      notification_type: '',
      dmf_number: '',
      notification_number: '',
      apos_document_type: '',
      cancel_comment: '',
      gmp_clearance_id: '',
      tga_email_id: '',
      applicant_name: '',
      doc_required: '',
      add_date: common.currentDateTime(),
      service_request_type: ''
    };

    let taskOrgLang = taskObj.language;

    if (taskObj.content && taskObj.content != '') {
      translate_obj.content = await common.getConvertedContent(entities.decode(taskObj.content), taskOrgLang);
    }

    await taskModel.add_task_translate(translate_obj);

  },

  approve_po: async (req, res, next) => {
    try {
      const {
        request_id,
        sold_to_party
      } = req.body;

      let customer_id = 0;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }      

     
      var headers, body, statusCode;
      var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><ReservationDetails><Main_Req>'+request_id+'</Main_Req><Reservation_Request_No></Reservation_Request_No><SO_ID></SO_ID><Identifier>UPDATE</Identifier><Sold_To_Party>'+sold_to_party+'</Sold_To_Party><PO_Update_Status>X</PO_Update_Status><Ship_to_party_Number></Ship_to_party_Number><Material_Number></Material_Number><Market></Market><Total_Order_Quantity></Total_Order_Quantity><Unit_Of_Measure></Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID></Itm_ID><FBPO_Number></FBPO_Number><EPDD></EPDD><Request_Del_Date></Request_Del_Date><TotalAvailableQuantity></TotalAvailableQuantity><UnitOfMeasure></UnitOfMeasure><RLT_Flag></RLT_Flag></ReservationDetails></res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
      
      //console.log('PO Update XML : ',xml_Request);

      if(Config.sapEnv=='qa'){
        const sampleHeaders = {
          'Content-Type': 'application/soap+xml;charset=UTF-8',
          'soapAction': Config.sap_stock_soap.soapAction,
          'Authorization': 'Basic '+Config.sap_stock_soap.authorization
        };
        const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
        var { headers, body, statusCode } = response;
        console.log(headers);
        console.log(body);
        console.log(statusCode);
      }

      if(Config.sapEnv=='local'){
        var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>UPDATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
        var statusCode = 200;
      }

      var body_data = xmlParser.toJson(body);
      var po_update_arr = [];

      var is_error = 0;

      let insLogObj = {
        request : xml_Request,
        response : body,
        method : 'POST',
        url : Config.sapReservationUrl,
        datetime : common.currentDateTime(),
        request_type : 'PO_UPDATE'
      };
      let logResponse = await sapModel.insertRequestLog(insLogObj);

      if(statusCode==200){
        body_data = JSON.parse(body_data,true);

        if(body_data !='' && body_data['SOAP:Envelope']!=''){
          po_update_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
  
          if(po_update_arr['STATUS']!='Success'){
            is_error++;
          }
          
          if(is_error==0){
            await sapModel.updateSoldToParty(request_id, sold_to_party)
            .then(async function (data) {
              if(data.success){
    
                // var files = req.files;
                // if (files && files.length > 0) {
    
                //   for (var j = 0; j < files.length; j++) {
    
                //     let file_details = {
                //       request_id: request_id,
                //       actual_file_name: files[j].originalname,
                //       new_file_name: files[j].filename,
                //       date_added: today
                //     }
    
                //     //await taskModel.addSapPOFile(file_details);
                    
                //   }
                // }
                let update_details = {
                  request_id: request_id,
                  action_req : 'No Action Required',
                  status : 'PO Accepted',
                }
    
                var UpdateCustomerResponse = sapModel.UpdateCustomerResponse(update_details).catch(err => {
                  common.logError(err);
                  res.status(400)
                    .json({
                      status: 3,
                      message: Config.errorText.value
                    }).end();
                });;  

                return res.status(200)
                .json({
                  status: 1,                
                })

              }else{    
                res.status(400)
                  .json({
                    status: 3,
                    message: data.message,
                  })
                .end();
              }
            })
            .catch((err) => {
              common.logError(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
          }else{
            res.status(200).json({
              status: 2,
              message: "PO update failed."
            }).end();
          }
        }else{
          res.status(200).json({
            status: 2,
            message: "PO update response failed."
          }).end();
        }
      }else{        
        res.status(400)
          .json({
            status: 3,
            message: 'SOAP PO update failed.',
          })
        .end();
      } 
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  delay_po: async (req, res, next) => {
    try {
      const {
        request_id
      } = req.body;           

      await sapModel.updateDelayPO(request_id)
        .then(async function (data) {
          if(data.success){         
            return res.status(200)
              .json({
                status: 1,                
              })
          }else{    
            res.status(400)
              .json({
                status: 3,
                message: data.message,
              })
            .end();
          }
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },



   /**
		* @desc Send request to sap for more availability check
		* @param 
		* @return json
	*/
  more_stock_availability: async (req, res, next) => {
    try {
      const {
        request_id,
        quantity,
        productunit,
        rdd
      } = req.body; 
      
      let sap_request_id = 0;
      let customer_id = 0;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        //customer_id = req.user.customer_id;
        if(req.user.role == 2){
          customer_id = req.body.agent_customer_id;
        }else{
          customer_id = req.user.customer_id;
        }
      }

      let unit = '';

      switch (productunit.toLowerCase()) {
        case 'kgs': unit = 'KG'; break;
        case 'gms': unit = 'G'; break;
        case 'mgs': unit = 'MG'; break;
        case 'ea': unit = 'EA'; break;
        case 'th': unit = 'TH'; break;
        case 'l': unit = 'L'; break;
        case 'su': unit = 'SU'; break;
        default: unit = 'KG';
      }

      var headers, body, statusCode;
      await sapModel.getLastRequest(request_id)
        .then(async function (dataPrevious) {
            if(dataPrevious.success){
              var ref_no = dataPrevious.data[0].request_id;
              var reservation_no = parseInt(dataPrevious.data[0].reservation_no) + 1;
              rdd_req = rdd.replace(/[^a-zA-Z0-9]/g, "");
              var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:stoc="http://S4HANA/XCEED/StockAvailability"> <soapenv:Header/> <soapenv:Body> <stoc:MT_XCEED_StockAvailability_Req> <StockDetails> <Reference_Number>'+ref_no+'</Reference_Number> <Material_Number>'+dataPrevious.data[0].product_sku+'</Material_Number> <Quantity>'+quantity.toFixed(1)+'</Quantity> <Unit_Of_Measure>'+unit+'</Unit_Of_Measure> <RDD>'+rdd_req+'</RDD> <Ship_to_party>'+dataPrevious.data[0].ship_to_party+'</Ship_to_party> <Market>'+entities.decode(dataPrevious.data[0].market)+'</Market> <Additional_Specs>';  //
              if(dataPrevious.data[0].additional_specification!='' && dataPrevious.data[0].additional_specification!=undefined){
                xml_Request += 'X';
              }
              xml_Request += '</Additional_Specs> <User_Role>';
              if(dataPrevious.data[0].is_admin){//if(submitted_by!=0){
                xml_Request += 'X';
              }
              xml_Request += '</User_Role> </StockDetails> </stoc:MT_XCEED_StockAvailability_Req> </soapenv:Body> </soapenv:Envelope>';
              //console.log(xml_Request);
              if(xml_Request){
                const insObj = {
                  request_id : ref_no,
                  user_id : dataPrevious.data[0].user_id,
                  rdd : rdd_req,
                  product_sku : dataPrevious.data[0].product_sku,
                  ship_to_party : dataPrevious.data[0].ship_to_party,
                  quantity : quantity,
                  unit: unit,
                  market : dataPrevious.data[0].market,
                  request_date : today,
                  additional_specification: dataPrevious.data[0].additional_specification,
                  reservation_no : reservation_no,
                  is_admin: dataPrevious.data[0].is_admin,
                  share_with_agent: dataPrevious.data[0].share_with_agent,
                  cc_customers : dataPrevious.data[0].cc_customers
                };
                sap_request_id = await sapModel.insertRequest(insObj);
                console.log("sap_request_id",sap_request_id);
              }

              if(Config.sapEnv=='qa'){
                const sampleHeaders = {
                  'Content-Type': 'application/soap+xml;charset=UTF-8',
                  'soapAction': Config.sap_stock_soap.soapAction,
                  'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                };
                const { response } = await soapRequest({ url: Config.sapSoapUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                var { headers, body, statusCode } = response;
                console.log(headers);
                console.log(body);
                console.log(statusCode);
              }

              if(Config.sapEnv=='local'){
                //var body = "<SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'><SOAP:Header/><SOAP:Body xmlns:stoc='http://S4HANA/XCEED/StockAvailability'><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0='http://S4HANA/XCEED/StockAvailability'><StockDetails><Uniqure_Reference_No>40220210119132155138</Uniqure_Reference_No><Ship_to_party>156833</Ship_to_party><Material_Number>350000003</Material_Number><Total_Order_Quantity>1.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>FAILED</Status><Message>RRD2-20210119</Message><Itm_ID>1 </Itm_ID><FBPO_Number_RLT>0019852196</FBPO_Number_RLT><EPDD>20210321</EPDD><RDD>20210123</RDD><Total_Available_Quantity>1.000 </Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>";
                //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>FAILED</Status><Message>RDD is in past</Message><RDD>20200821</RDD></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
                //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>92120200915192905108</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>500</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20200929</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20200926</EPDD><RDD>20200929</RDD><Total_Available_Quantity>500.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
                var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>1</Itm_ID><FBPO_Number_RLT>0018886544</FBPO_Number_RLT><EPDD>20201030</EPDD><RDD>20201121</RDD><Total_Available_Quantity>660.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails><StockDetails><Uniqure_Reference_No>1000000009</Uniqure_Reference_No><Ship_to_party>108549</Ship_to_party><Material_Number>350000037</Material_Number><Total_Order_Quantity>1280</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201121</Message><Itm_ID>2</Itm_ID><FBPO_Number_RLT>0018886559</FBPO_Number_RLT><EPDD>20201030</EPDD><RDD>20201121</RDD><Total_Available_Quantity>620.000</Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
                //var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header /><SOAP:Body xmlns:stoc="http://S4HANA/XCEED/StockAvailability"><ns0:MT_XCEED_StockAvailability_Resp xmlns:ns0="http://S4HANA/XCEED/StockAvailability"><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><Status>SUCCESS</Status><Message>RRD2-20201016</Message><Itm_ID>1 </Itm_ID><FBPO_Number_RLT>0018916717</FBPO_Number_RLT><EPDD>20201031</EPDD><RDD>20201016</RDD><Total_Available_Quantity>5.000 </Total_Available_Quantity><Unit_Of_Measure_PLAF>KG</Unit_Of_Measure_PLAF><User_Role_Flag>X</User_Role_Flag></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>1 </Itm_ID_Additional><FBPO_Number_Additional>0018916717</FBPO_Number_Additional><EPDD_Additional>20201031</EPDD_Additional><Total_Available_Quantity_Additional>560.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>2 </Itm_ID_Additional><FBPO_Number_Additional>0018916718</FBPO_Number_Additional><EPDD_Additional>20201104</EPDD_Additional><Total_Available_Quantity_Additional>640.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails><StockDetails><Uniqure_Reference_No>38120201016165806480</Uniqure_Reference_No><Ship_to_party>158565</Ship_to_party><Material_Number>350000139</Material_Number><Total_Order_Quantity>5.0</Total_Order_Quantity><Unit_Of_Measure>KG</Unit_Of_Measure><RDD>20201016</RDD><User_Role_Flag>X</User_Role_Flag><Itm_ID_Additional>3 </Itm_ID_Additional><FBPO_Number_Additional>0018916719</FBPO_Number_Additional><EPDD_Additional>20201103</EPDD_Additional><Total_Available_Quantity_Additional>660.000 </Total_Available_Quantity_Additional><Unit_Of_Measure_Additional>KG</Unit_Of_Measure_Additional></StockDetails></ns0:MT_XCEED_StockAvailability_Resp></SOAP:Body></SOAP:Envelope>';
                var statusCode = 200;
              }

              var body_data = xmlParser.toJson(body);
              var stack_details_arr = [];
              var response_arr = {};

              let insLogObj = {
                request : xml_Request,
                response : body,
                method : 'POST',
                url : Config.sapSoapUrl,
                datetime : common.currentDateTime(),
                request_type : 'STOCK_ENQUIRY'
              };
              let logResponse = await sapModel.insertRequestLog(insLogObj);

              if(statusCode==200){
                body_data = JSON.parse(body_data,true);
                //console.log('JSON output', body_data);

                if(body_data !='' && body_data['SOAP:Envelope']!=''){
                  stack_details_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_StockAvailability_Resp']['StockDetails'];
                  response_arr.product_code = dataPrevious.data[0].product_code;
                  response_arr.market = dataPrevious.data[0].market;
                  response_arr.quantity = quantity;
                  response_arr.unit = productunit;
                  response_arr.rdd = common.formatDate(rdd,"dd-mm-yyyy");
                  response_arr.ship_to_party = dataPrevious.data[0].ship_to_party;
                  response_arr.reference_no = ref_no;
                  response_arr.reservation_no = reservation_no;
                  response_arr.display_full_quantity = false;
                  response_arr.disabled = false;

                  var available_stock =[];
                  let total_quantity = 0;
                  var information_stock = [];
                  var error_message = '';
                  // console.log('Stack output',stack_details_arr);
                  
                  if(stack_details_arr['Status']=='FAILED'){
                    console.log("error block")
                    var cancelTask = sapModel.cancelAvailableRequest(sap_request_id);
                    res.status(200).json({
                      status: 2,
                      message: stack_details_arr['Message']
                    }).end();
                  }else{    
                    if(stack_details_arr['Status']=='SUCCESS'){   // for single StockDetails response
                      var temp_arr = stack_details_arr;
                      stack_details_arr = [];
                      stack_details_arr.push(temp_arr);
                    }
                    
                    for (let index = 0; index <= (stack_details_arr.length - 1); index++) {
                      const element = stack_details_arr[index];
                      var temp_element = {};  //console.log('Stack output',stack_details_arr[0]['Status']);
                      //console.log(element);
                      if(element['Status']=='FAILED'){
                        error_message = element['Message'];
                      }else{
                        //if(submitted_by!=0){
                          if(element['FBPO_Number_Additional'] !=undefined && element['Total_Available_Quantity_Additional']!=undefined && dataPrevious.data[0].is_admin){
                            temp_element['item_number'] = element['Itm_ID_Additional'];
                            temp_element['fbpo_number'] = element['FBPO_Number_Additional'];
                            temp_element['expected_date'] = element['EPDD_Additional'];
                            temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity_Additional']);
                            //temp_element['quantity_unit'] = element['Unit_Of_Measure_Additional'];
                            switch (element['Unit_Of_Measure_Additional'].toLowerCase()) {
                              case 'kg': temp_element['quantity_unit'] = 'Kgs'; break;
                              case 'g': temp_element['quantity_unit'] = 'Gms'; break;
                              case 'mg': temp_element['quantity_unit'] = 'Mgs'; break;
                              case 'ea': temp_element['quantity_unit'] = 'EA'; break;
                              case 'th': temp_element['quantity_unit'] = 'TH'; break;
                              case 'l': temp_element['quantity_unit'] = 'L'; break;
                              case 'su': temp_element['quantity_unit'] = 'SU'; break;
                              default: temp_element['quantity_unit'] = '';
                            }
                            information_stock.push(temp_element); 
                          }
                        //}else{
                          if(element['Total_Available_Quantity']!=undefined){
                            temp_element['item_number'] = element['Itm_ID'];                      
                            temp_element['fbpo_number'] = element['FBPO_Number_RLT'];                      
                            temp_element['expected_date'] = element['EPDD'];
                            temp_element['available_quantity'] = parseInt(element['Total_Available_Quantity']);
                            //temp_element['quantity_unit'] = element['Unit_Of_Measure'];
                            temp_element['confirm'] = false;
                            temp_element['disabled'] = false;
                            temp_element['rtl_flag'] = (element['RLT_Flag']) ? true : false;
                            total_quantity += parseInt(element['Total_Available_Quantity']);  
                            
                            console.log(element['EPDD']+"=="+rdd_req);
                            switch (element['Unit_Of_Measure'].toLowerCase()) {
                              case 'kg': temp_element['quantity_unit'] = 'Kgs'; break;
                              case 'g': temp_element['quantity_unit'] = 'Gms'; break;
                              case 'mg': temp_element['quantity_unit'] = 'Mgs'; break;
                              case 'ea': temp_element['quantity_unit'] = 'EA'; break;
                              case 'th': temp_element['quantity_unit'] = 'TH'; break;
                              case 'l': temp_element['quantity_unit'] = 'L'; break;
                              case 'su': temp_element['quantity_unit'] = 'SU'; break;
                              default: temp_element['quantity_unit'] = '';
                            }
                            temp_element['epdd_flag'] = true;
                            if(element['EPDD'] < rdd_req){                              
                              temp_element['rdd_flag'] = false;
                            }else{
                              temp_element['rdd_flag'] = true;
                            }
                            //console.log(temp_element);
                            let insObj1 = {
                              sap_request_id : sap_request_id.id,
                              fbpo_number : temp_element['fbpo_number'],
                              itm_ID : element['Itm_ID'],
                              epdd : element['EPDD'],
                              total_available_quantity : parseInt(element['Total_Available_Quantity']),
                              unit_of_measure: element['Unit_Of_Measure'],
                              rtl_flag : (element['RLT_Flag']) ? 1 : 0
                            };
                            let responseId = await sapModel.insertResponse(insObj1);   
                            
                            if(responseId){
                              temp_element['response_id'] = hashids.encode(responseId.id); //md5(responseId.id);
                              temp_element['act_response_id'] = responseId.id;
                            }else{
                              temp_element['response_id'] = 'RESPONSEID';
                              temp_element['act_response_id'] = 0;
                            }
                            available_stock.push(temp_element); 
                          }
                        //}
                      }                                 
                    }
                    if(error_message!=''){
                      console.log("sap_request_id",sap_request_id.id);
                      var cancelTask = sapModel.cancelAvailableRequest(sap_request_id);
                      res.status(200).json({
                        status: 2,
                        message: error_message
                      }).end();
                    }else{
                      //console.log(available_stock);
                      available_stock.sort((a, b) => parseFloat(a['expected_date']) - parseFloat(b['expected_date']));
                      //console.log(available_stock);
                      response_arr.available_stock = available_stock;
                      response_arr.information_stock = information_stock;
                      response_arr.total_quantity = total_quantity;
                      return res.status(200)
                      .json({
                        status: 1,
                        stock_list: response_arr,
                      })
                    }
                  }
                }else{
                  res.status(200).json({
                    status: 2,
                    message: "Stock availability check failed."
                  }).end();
                }
              }else{
                res.status(statusCode).json({
                  status: 2,
                  message: "Stock response"
                }).end();
              }  

            }else{       
              res.status(400).json({
                status: 2,
                message: "Unable to generate Reference Number"
              }).end();
            }
        }).catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }    
   
  },

  enquiry_cancel: async (req, res, next) => {
    try {
      const {
        task_id,
      } = req.body; 
      
      let customer_id = 0;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        customer_id = req.user.customer_id;
      }
      await sapModel.get_stock_task_details(task_id)
        .then(async function (data) {

          if(data && data.request_id!='' && data.request_id!= undefined){
            var headers, body, statusCode;
            var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><ReservationDetails><Main_Req>'+data.request_id+'</Main_Req><Reservation_Request_No>'+data.reservation_no+'</Reservation_Request_No><SO_ID>'+data.so_id+'</SO_ID><Identifier>CANCEL</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number></Ship_to_party_Number><Material_Number></Material_Number><Market></Market><Total_Order_Quantity></Total_Order_Quantity><Unit_Of_Measure></Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID></Itm_ID><FBPO_Number></FBPO_Number><EPDD></EPDD><Request_Del_Date></Request_Del_Date><TotalAvailableQuantity></TotalAvailableQuantity><UnitOfMeasure></UnitOfMeasure><RLT_Flag></RLT_Flag></ReservationDetails></res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
        
            //console.log('STOCK Cancel XML : ',xml_Request);
      
            if(Config.sapEnv=='qa'){
              const sampleHeaders = {
                'Content-Type': 'application/soap+xml;charset=UTF-8',
                'soapAction': Config.sap_stock_soap.soapAction,
                'Authorization': 'Basic '+Config.sap_stock_soap.authorization
              };
              const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
              var { headers, body, statusCode } = response;
              console.log(headers);
              console.log(body);
              console.log(statusCode);
            }
      
            if(Config.sapEnv=='local'){
              var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>UPDATE</Req_Type><STATUS>Success</STATUS><Mode>Customer Cancellation</Mode><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
              var statusCode = 200;
            }
      
            var body_data = xmlParser.toJson(body);
            var cancel_arr = [];
      
            var is_error = 0; var cancel_message = '';
      
            let insLogObj = {
              request : xml_Request,
              response : body,
              method : 'POST',
              url : Config.sapReservationUrl,
              datetime : common.currentDateTime(),
              request_type : 'CANCEL'
            };
            let logResponse = await sapModel.insertRequestLog(insLogObj);
            
            if(statusCode==200){
              body_data = JSON.parse(body_data,true);
      
              if(body_data !='' && body_data['SOAP:Envelope']!=''){
                cancel_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
                
                if(cancel_arr.length>1){
                  for (let index = 0; index <= (cancel_arr.length - 1); index++) {
                    if(cancel_arr[index]['STATUS']!='Success'){
                      $is_error++;                    
                    }else{
                      cancel_message = cancel_arr[index]['Mode'];
                    }
                  }
                }else{
                  if(cancel_arr['STATUS']!='Success'){
                    is_error++;                
                  }else{
                    cancel_message = cancel_arr['Mode'];
                  }
                }              
                
                if(is_error==0){
                  var cancelTask = sapModel.cancelTask(task_id,cancel_message).catch(err => {
                    common.logError(err);
                    res.status(400)
                      .json({
                        status: 3,
                        message: Config.errorText.value
                      }).end();
                  });
                  
                  return res.status(200)
                    .json({
                      status: 1,                
                  })
                }else{
                  res.status(200).json({
                    status: 2,
                    message: "Cancel failed."
                  }).end();
                }
              }else{
                res.status(200).json({
                  status: 2,
                  message: "Cancel response not found."
                }).end();
              }
            }else{        
              res.status(400)
                .json({
                  status: 3,
                  message: 'SOAP cancel failed.',
                })
              .end();
            }
          }else{        
            res.status(400)
              .json({
                status: 3,
                message: 'Unable to cancel this task.',
              })
            .end();
          }

        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  remove_reservation:async (req, res, next) => {
    try {
      const {
        request_id,
        reservation_no
      } = req.body;

      let customer_id = 0;
      let submitted_by = 0;
      var today = common.currentDateTime();
      if (req.user.empe != 0) {
        submitted_by = req.user.empe;
        customer_id = req.user.customer_id;
      } else {
        //customer_id = req.user.customer_id;
        if(req.user.role == 2){
          customer_id = req.body.agent_customer_id;
        }else{
          customer_id = req.user.customer_id;
        }
      }

      await sapModel.getParticularReservation(request_id,reservation_no,customer_id)
        .then(async function (dataReserv) {
          if(dataReserv.status==0){
            var cancelTask = sapModel.cancelAvailable(dataReserv).catch(err => {
              common.logError(err);
              res.status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value
                }).end();
            });

            await sapModel.getLastRequest(request_id)
            .then(async function (dataPrevious) {
                if(dataPrevious.success){
                  return res.status(200)
                    .json({
                      status: 1,
                      redircted : 0,           
                  })
                }else{
                  return res.status(200)
                    .json({
                      status: 1,
                      redircted : 1,             
                  })
                }
            }).catch((err) => {
              common.logError(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
          }else if(dataReserv.status==1){
            
            await sapModel.getAllReservedResponse(request_id, dataReserv.user_id)
            .then(async function (dataRev) {
              if(dataRev.success){
                
                var headers, body, statusCode;
                var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req>';

                for(let index = 0; index <= (dataRev.data.length - 1); index++) {
                
                  xml_Request += '<ReservationDetails><Main_Req>'+dataRev.data[index].request_id+'</Main_Req><Reservation_Request_No>'+dataRev.data[index].reservation_no+'</Reservation_Request_No><SO_ID>'+dataRev.data[index].so_id+'</SO_ID><Identifier>CANCEL</Identifier><Sold_To_Party></Sold_To_Party><PO_Update_Status></PO_Update_Status><Ship_to_party_Number></Ship_to_party_Number><Material_Number></Material_Number><Market></Market><Total_Order_Quantity></Total_Order_Quantity><Unit_Of_Measure></Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID></Itm_ID><FBPO_Number></FBPO_Number><EPDD></EPDD><Request_Del_Date></Request_Del_Date><TotalAvailableQuantity></TotalAvailableQuantity><UnitOfMeasure></UnitOfMeasure><RLT_Flag></RLT_Flag></ReservationDetails>';
                }
                
                xml_Request += '</res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
            
                console.log('STOCK Cancel XML : ',xml_Request);
          
                if(Config.sapEnv=='qa'){
                  const sampleHeaders = {
                    'Content-Type': 'application/soap+xml;charset=UTF-8',
                    'soapAction': Config.sap_stock_soap.soapAction,
                    'Authorization': 'Basic '+Config.sap_stock_soap.authorization
                  };
                  const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
                  var { headers, body, statusCode } = response;
                  console.log(headers);
                  console.log(body);
                  console.log(statusCode);
                }
          
                if(Config.sapEnv=='local'){
                  var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>UPDATE</Req_Type><STATUS>Success</STATUS><Info_Message>This is cancelled from Reservation page</Info_Message><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
                  var statusCode = 200;
                }
          
                var body_data = xmlParser.toJson(body);
                var cancel_arr = [];    
                var is_error = 0;
                var cancel_text = '';
          
                let insLogObj = {
                  request : xml_Request,
                  response : body,
                  method : 'POST',
                  url : Config.sapReservationUrl,
                  datetime : common.currentDateTime(),
                  request_type : 'CANCEL'
                };
                let logResponse = await sapModel.insertRequestLog(insLogObj);
                
                if(statusCode==200){
                  body_data = JSON.parse(body_data,true);
          
                  if(body_data !='' && body_data['SOAP:Envelope']!=''){
                    cancel_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
            
                    if(cancel_arr.length>1){
                      for (let index = 0; index <= (cancel_arr.length - 1); index++) {
                        if(cancel_arr[index]['STATUS']!='Success'){
                          $is_error++;
                        }
                        cancel_text= cancel_arr[index]['Info_Message'];
                      }
                    }else if(cancel_arr.length==1 && cancel_arr['STATUS']!='Success'){
                      is_error++;
                      cancel_text= cancel_arr['Info_Message'];
                    }
                    
                    if(is_error==0){
                      for(let index = 0; index <= (dataRev.data.length - 1); index++) {
                        sapModel.cancelTaskByReqid(dataRev.data[index].id,cancel_text);
                      }
                      
                      return res.status(200)
                        .json({
                          status: 1,
                          redircted: 1                
                      })
                    }else{
                      res.status(200).json({
                        status: 2,
                        message: "Cancel failed."
                      }).end();
                    }
                  }else{
                    res.status(200).json({
                      status: 2,
                      message: "Cancel response not found."
                    }).end();
                  }
                }else{        
                  res.status(400)
                    .json({
                      status: 3,
                      message: 'SOAP cancel failed.',
                    })
                  .end();
                }

              }else{
                res
                .status(400)
                .json({
                  status: 2,
                  message: dataRev.message,
                })
                .end();
              }
            }).catch((err) => {
              common.logError(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
          }else{
            return res.status(200)
                    .json({
                      status: 1,
                      redircted : 0,           
                  })
          }
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });

    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  
  },

  fetch_invoice_sap:async function(req, res, next) {
    try {
      console.log('SAP request: ' + req);

      const {
        so_number,
        invoice_no
      } = req.body;

      if(invoice_no && so_number){
        res.status(200).json({
          status: true,        
        }).end();
      }else{
        res.status(200).json({
          status: false,        
        }).end();
      }

    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: false,        
      }).end();
    }
  },

  fetch_invoice_file_sap:async function(req, res, next) {
    try {
      let file_name = `${uuidv4()}.pdf`;
      let file_path = `${Config.upload.temp_files}${file_name}`;
      let url = 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf';
      
      // const response = await Axios({
      //   url,
      //   method: 'GET',
      //   responseType: 'stream',        
      // });

      // fs.writeFile(file_path, response, async function(err) {
      //   if(err){
      //     console.log("Error : ",err);
      //   }else{
      //     console.log('file save in ', file_path);
      //   }
      // });


    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: false,        
      }).end();
    }
  },

  // update_reservation:async (req, res, next) => {
  //   try {
  //     console.log("SOAP UPDATE Reservation Start");
  //     //console.log("Checking : ",req.body['soapenv:envelope']['soapenv:body']['res:mt_sap_reservation_creation_update']);

  //     let insLogObj = {
  //       request : JSON.stringify(req.body['soapenv:envelope']),
  //       response : '',
  //       method : 'POST',
  //       url : '',
  //       datetime : common.currentDateTime(),
  //       request_type : 'STOCK_UPDATE'
  //     };
  //     let logResponse = await sapModel.insertRequestLog(insLogObj);      

  //     if (typeof req.body['soapenv:envelope']['soapenv:body']['res:mt_sap_reservation_creation_update']['reservationcreationupdate'] !== 'undefined') {
  //       let all_request = req.body['soapenv:envelope']['soapenv:body']['res:mt_sap_reservation_creation_update']['reservationcreationupdate'];
        
  //       if(all_request.length>1){
  //         console.log('multiple post');
  //         var error=0;
  //         for(let index = 0; index <= (all_request.length - 1); index++) {
  //           if(all_request[index].status=='SUCCESS'){
  //             await sapModel.getParticularRequest(all_request[index].main_ereq, all_request[index].reservation_request_no,all_request[index].so_id)
  //                 .then(async function (data) {
  //                     //console.log("Task details : ",data );
  //                     if(data){
  //                       var update_date = all_request[index].date.slice(0,4)+"-"+all_request[index].date.slice(4,6)+"-"+all_request[index].date.slice(6,8);
  //                       var update_date_time = update_date+" "+all_request[index].time;
  //                       if(all_request[index].req_type=='CANCEL'){                      
  //                         console.log("Task Id To Cancel : ",data.task_id );
  //                         var cancelTask = sapModel.cancelTask(data.task_id, all_request[index].info_message,update_date_time);
  //                       }
  //                       if(all_request[index].req_type=='UPDATE'){
  //                         console.log("Task Id To PO NO Update : ",data.task_id );
  //                         var updatePONumber = sapModel.updatePONumber(data.task_id, all_request[index].sap_so_number,update_date_time);
  //                       }
  //                     }
  //                 }).catch((err) => {
  //                   error++;
  //                   common.logError(err);                  
  //                 });
  //           }  
  //         }
  //         var updateRequestLog = sapModel.updateRequestLog(logResponse.id,'{"status" : 200}');
  //         res.status(200).end();
  //       }else{
  //         console.log('single post')
  //         if(all_request.status=='SUCCESS'){
  //           await sapModel.getParticularRequest(all_request.main_ereq, all_request.reservation_request_no,all_request.so_id)
  //               .then(async function (data) {
  //                   //console.log("Task details : ",data );
  //                   if(data){
  //                     var update_date = all_request.date.slice(0,4)+"-"+all_request.date.slice(4,6)+"-"+all_request.date.slice(6,8);
  //                     var update_date_time = update_date+" "+all_request.time;
  //                     if(all_request.req_type=='CANCEL'){                      
  //                       console.log("Task Id To Cancel : ",data.task_id );
  //                       var cancelTask = sapModel.cancelTask(data.task_id, all_request.info_message,update_date_time);
  //                     }
  //                     if(all_request.req_type=='UPDATE'){
  //                       console.log("Task Id To PO NO Update : ",data.task_id );
  //                       var updatePONumber = sapModel.updatePONumber(data.task_id, all_request.sap_so_number,update_date_time);
  //                     }
  //                   }
  //               }).catch((err) => {
  //                 common.logError(err);                  
  //               });
  //         }          
  //       }
  //       var updateRequestLog = sapModel.updateRequestLog(logResponse.id,'{"status" : 200}');
  //       res.status(200).end();
  //     }else{
  //       if(logResponse){
  //         var updateRequestLog = sapModel.updateRequestLog(logResponse.id,'{"status" : 400}');
  //       }
  //       res.status(400).end();
  //     }
  //   }catch (err) {
  //     common.logError(err);
  //     res.status(400).end();
  //   }
  // },

  list_customer_shipto: async (req, res, next) => {
    try {      
      let customer_id = 0; 
      if(req.user.role == 2){
        customer_id = req.body.customer_id;
      }else{
        customer_id = req.user.customer_id;
      }
      //console.log(customer_id)
      await sapModel.list_customer_shipto(customer_id)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  list_customer_products: async (req, res, next) => {
    try {      
      let shipto_id = req.value.params.shipto_id;
      await sapModel.list_customer_products(shipto_id, req.user.lang)
        .then(async function (data) {          
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  list_customer_products_code: async (req, res, next) => {
    try {      
      let product_id = req.value.params.id;
      await sapModel.get_products_code(product_id)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data,            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  get_customer_shipto_country: async (req, res, next) => {
    try {      
      let shipto_id = req.value.params.shipto_id;
      await sapModel.get_customer_shipto_details(shipto_id)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data[0],            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },
  get_customer_shipto_soldto: async (req, res, next) => {
    try {      
      let shipto_id = req.value.params.shipto_id;
      await sapModel.get_customer_soldto_details(shipto_id)
        .then(async function (data) {
          res.status(200).json({
            status: 1,
            data: data[0],            
          });
        })
        .catch((err) => {
          common.logError(err);
          res
            .status(400)
            .json({
              status: 3,
              message: Config.errorText.value,
            })
            .end();
        });
    } catch (err) {
      common.logError(err);
      res
        .status(400)
        .json({
          status: 3,
          message: Config.errorText.value,
        })
        .end();
    }
  },

  customer_approve_po: async (req, res, next) => {
    try {
      const request_id = req.body.request_id;
      let customer_id = req.user.customer_id;
      let sold_to_party = 0;

      let submitted_by = 0;
      var today = common.currentDateTime();
      if(req.user.role == 2){
        customer_id = req.body.agent_customer_id;
      }else{
        if (req.user.empe != 0) {
          submitted_by = req.user.empe;
          customer_id = req.user.customer_id;
        } else {
          customer_id = req.user.customer_id;
        }
      }
     

      let soldto_details = await sapModel.list_customer_shipto(customer_id);	
      //console.log(soldto_details);

      if(soldto_details.length!=0){
      	sold_to_party = soldto_details[0].soldto_id;
     
	      var headers, body, statusCode;
	      var xml_Request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://S4HANA/XCEED/Reservation"><soapenv:Header/><soapenv:Body><res:MT_XCEED_Reservation_Req><ReservationDetails><Main_Req>'+request_id+'</Main_Req><Reservation_Request_No></Reservation_Request_No><SO_ID></SO_ID><Identifier>UPDATE</Identifier><Sold_To_Party>'+sold_to_party+'</Sold_To_Party><PO_Update_Status>X</PO_Update_Status><Ship_to_party_Number></Ship_to_party_Number><Material_Number></Material_Number><Market></Market><Total_Order_Quantity></Total_Order_Quantity><Unit_Of_Measure></Unit_Of_Measure><Full_Qty_Single></Full_Qty_Single><Itm_ID></Itm_ID><FBPO_Number></FBPO_Number><EPDD></EPDD><Request_Del_Date></Request_Del_Date><TotalAvailableQuantity></TotalAvailableQuantity><UnitOfMeasure></UnitOfMeasure><RLT_Flag></RLT_Flag></ReservationDetails></res:MT_XCEED_Reservation_Req></soapenv:Body></soapenv:Envelope>';
	      
	      console.log('Customer PO Update XML : ',xml_Request);

	      if(Config.sapEnv=='qa'){
	        const sampleHeaders = {
	          'Content-Type': 'application/soap+xml;charset=UTF-8',
	          'soapAction': Config.sap_stock_soap.soapAction,
	          'Authorization': 'Basic '+Config.sap_stock_soap.authorization
	        };
	        const { response } = await soapRequest({ url: Config.sapReservationUrl, headers: sampleHeaders, xml: xml_Request, timeout: 20000 });
	        var { headers, body, statusCode } = response;
	        console.log(headers);
	        console.log(body);
	        console.log(statusCode);
	      }

	      if(Config.sapEnv=='local'){
	        var body = '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"><SOAP:Header/><SOAP:Body xmlns:res="http://S4HANA/XCEED/Reservation"><ns0:MT_XCEED_Reservation_Resp xmlns:ns0="http://S4HANA/XCEED/Reservation"><ReservationDetails><Main_Req>20200929REQUEST1</Main_Req><Reservation_Request>01</Reservation_Request><SO_ID>01</SO_ID><Req_Type>UPDATE</Req_Type><STATUS>Success</STATUS><Date>20200929</Date><Time>16:05:00</Time></ReservationDetails></ns0:MT_XCEED_Reservation_Resp></SOAP:Body></SOAP:Envelope>';
	        var statusCode = 200;
	      }

	      var body_data = xmlParser.toJson(body);
	      var po_update_arr = [];

	      var is_error = 0;

	      let insLogObj = {
	        request : xml_Request,
	        response : body,
	        method : 'POST',
	        url : Config.sapReservationUrl,
	        datetime : common.currentDateTime(),
	        request_type : 'PO_UPDATE'
	      };
	      let logResponse = await sapModel.insertRequestLog(insLogObj);

	      if(statusCode==200){
	        body_data = JSON.parse(body_data,true);

	        if(body_data !='' && body_data['SOAP:Envelope']!=''){
	          po_update_arr = body_data['SOAP:Envelope']['SOAP:Body']['ns0:MT_XCEED_Reservation_Resp']['ReservationDetails'];
	  
	          if(po_update_arr['STATUS']!='Success'){
	            is_error++;
	          }
	          
	          if(is_error==0){
	            await sapModel.updateSoldToParty(request_id, sold_to_party)
	            .then(async function (data) {
	              if(data.success){
	    
	                var files = req.files;
	                if (files && files.length > 0) {
	    
	                  for (var j = 0; j < files.length; j++) {
	    
	                    let file_details = {
	                      request_id: request_id,
	                      actual_file_name: files[j].originalname,
	                      new_file_name: files[j].filename,
	                      date_added: today
	                    }
	    
	                    await sapModel.addSapPOFile(file_details);
	                    
	                  }
	                }
	                let update_details = {
	                  request_id: request_id,
	                  action_req : 'No Action Required',
	                  status : 'PO Acknowledged',
                    po_no: req.body.po_no
	                }
	    
	                var UpdateCustomerResponse = await sapModel.UpdateCustomerResponse(update_details).catch(err => {
	                  common.logError(err);
	                  res.status(400)
	                    .json({
	                      status: 3,
	                      message: Config.errorText.value
	                    }).end();
	                });;  

                  await sapModel.getTaskListByRequest(request_id)
	                  .then(async function (data1) {console.log(data1);

                      if(data1.length!=0){
                        for (let index = 0; index <= (data1.length - 1); index++) {
                          let task_details = await taskModel.tasks_details(data1[index].task_id);
                          let customer_details = await customerModel.get_user(task_details[0].customer_id);
                  
                          let mail_arr = await module.exports.notify_cc_highlight(req, task_details);

                          var url_status = `${process.env.REACT_API_URL}task-details/${data1[index].task_id}`;

                          if(mail_arr.length > 0 && mail_arr[0].to_arr.length > 0){
                            //customer mail
                            let mailParam = {
                              first_name: entities.decode(customer_details[0].first_name),
                              last_name: entities.decode(customer_details[0].last_name),                
                              task_ref: task_details[0].task_ref,
                              product_name: task_details[0].product_name,
                              po_no: task_details[0].po_no,
                              url_status: url_status,
                              company_name: entities.decode(customer_details[0].company_name),
                              req_name: task_details[0].req_name,
                            };                           
                            var mailcc = '';
                            if (mail_arr[1].cc_arr && mail_arr[1].cc_arr.length > 0) {
                              mailcc = mail_arr[1].cc_arr;
                            }
                            
                            const mailSent = common.sendMailByCodeB2C('CUSTOMER_TASK_TEMPLATE_FOR_SAP_PO_UPLOAD',mail_arr[0].to_arr,Config.contactUsMail,mailcc,entities.decode(task_details[0].language),mailParam,"c");
                          }

                          var cc_exclude_arr = [];
                          //==== OWNER BM
                          let employee_details = await employeeModel.get_user(task_details[0].owner);
                          if (employee_details && employee_details.length > 0) {
                            let assignment_owner_id = await taskModel.get_assignment_id(task_details[0].owner, data1[index].task_id);                                
  
                            let mailParam1 = {
                              first_name: employee_details[0].first_name,
                              po_no: task_details[0].po_no,
                              ccpurl: Config.ccpUrl,
                              task_id: data1[index].task_id,
                              assignment_owner_id: assignment_owner_id,
                              task_ref: task_details[0].task_ref,
                              company_name: entities.decode(customer_details[0].company_name),
                              customer_name: entities.decode(customer_details[0].first_name),
                              product_name: entities.decode(task_details[0].product_name)
                            };
  
                            const mailSent = common.sendMailByCodeB2E('TASK_TEMPLATE_FOR_SAP_PO_UPLOAD_BM', employee_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam1, "e");
  
                            //==== OWNER BM
                            cc_exclude_arr.push(task_details[0].owner);
                            if (task_details[0].owner != task_details[0].current_ownership) {
                              //==== ASSIGNED EMPLOYEES
                              let assignment_assign_to_id = await taskModel.get_assignment_id(task_details[0].current_ownership, data1[index].task_id);
                              let a_emp_details = await employeeModel.get_user(task_details[0].current_ownership);
  
                              let mailParam2 = {
                                first_name: a_emp_details[0].first_name,
                                po_no: task_details[0].po_no,
                                ccpurl: Config.ccpUrl,
                                task_id: data1[index].task_id,
                                assignment_assign_to_id: assignment_assign_to_id,
                                task_ref: task_details[0].task_ref,
                                company_name: entities.decode(customer_details[0].company_name),
                                customer_name: entities.decode(customer_details[0].first_name),
                                product_name: entities.decode(task_details[0].product_name)
                              };
                              
  
                              const mailSent = common.sendMailByCodeB2E('TASK_TEMPLATE_FOR_SAP_PO_UPLOAD_ASSIGNED_EMPLOYEES', a_emp_details[0].email, "[Dr Reddys] <" + Config.webmasterMail + ">", '', mailParam2, "e");
  
                              //==== ASSIGNED EMPLOYEES
                              cc_exclude_arr.push(task_details[0].current_ownership);
                            }
                          }
                        }
                      }

                  }).catch(err => {
	                  common.logError(err);
	                  res.status(400)
	                    .json({
	                      status: 3,
	                      message: Config.errorText.value
	                    }).end();
	                });;  

	                return res.status(200)
	                .json({
	                  status: 1,                
	                })

	              }else{    
	                res.status(400)
	                  .json({
	                    status: 3,
	                    message: data.message,
	                  })
	                .end();
	              }
	            })
	            .catch((err) => {
	              common.logError(err);
	              res
	                .status(400)
	                .json({
	                  status: 3,
	                  message: Config.errorText.value,
	                })
	                .end();
	            });
	          }else{
	            res.status(200).json({
	              status: 2,
	              message: "PO update failed."
	            }).end();
	          }
	        }else{
	          res.status(200).json({
	            status: 2,
	            message: "PO update response failed."
	          }).end();
	        }
	      }else{        
	        res.status(400)
	          .json({
	            status: 3,
	            message: 'SOAP PO update failed.',
	          })
	        .end();
	      }
	    }else{
	    	res.status(400)
	          .json({
	            status: 3,
	            message: 'Unable To find soldTo party information.',
	          })
	        .end();
	    } 
    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },

  customer_delay_po: async (req, res, next) => {
    try {
      	const request_id = req.body.request_id;	//console.log('request_id',request_id);        
      	let customer_id = req.user.customer_id;	
        if(req.user.role == 2){
          customer_id = req.body.agent_customer_id;
        }
        //console.log('customer_id',customer_id);
      	let sold_to_party = 0;      

      	let soldto_details = await sapModel.list_customer_shipto(customer_id);    //console.log(soldto_details);      

      	if(soldto_details.length!=0){
      		sold_to_party = soldto_details[0].soldto_id;
          await sapModel.updateCustomerDelayPO(request_id, sold_to_party)
            .then(async function (data) {
              if(data.success){         
                return res.status(200)
                  .json({
                    status: 1,                
                  })
              }else{    
                res.status(400)
                  .json({
                    status: 3,
                    message: data.message,
                  })
                .end();
              }
            })
            .catch((err) => {
              common.logError(err);
              res
                .status(400)
                .json({
                  status: 3,
                  message: Config.errorText.value,
                })
                .end();
            });
	      }else{
          res.status(400)
              .json({
                status: 3,
                message: 'Unable To find soldTo party information.',
              })
            .end();
        }

    }catch (err) {
      common.logError(err);
      res.status(400).json({
        status: 3,
        message: Config.errorText.value
      }).end();
    }
  },
  notify_cc_highlight: async (req, task_details, notify_type = 0) => {
    const posted_by = task_details[0].customer_id;
    let ret_arr = [];
    let customer_details = await customerModel.get_user(task_details[0].customer_id);
    if (customer_details[0].status == 1) {
      let to_details = [];
      let cc_details = [];

      if (customer_details[0].dnd != 1) {
        to_details.push(customer_details[0].email);
      }

      if (task_details[0].share_with_agent == 1) {

        let agent_list = await customerModel.get_agent_company(customer_details[0].company_id);

        if (agent_list && agent_list.length > 0) {
          for (let index = 0; index < agent_list.length; index++) {
            const element = agent_list[index];
            let agent_cc_dets = await agentModel.get_user(element.agent_id);
            if (agent_cc_dets[0].dnd != 1) {
              if (task_details[0].submitted_by > 0 && parseInt(task_details[0].submitted_by) == parseInt(element.agent_id)) {
                to_details.push(agent_cc_dets[0].email);
              } else {
                cc_details.push(agent_cc_dets[0].email);
              }
            }
          }
        }
      }


      //NOTIFY CC CUSTOMERS
      let cc_list = await taskModel.task_cc_customer(task_details[0].task_id);
      if (cc_list && cc_list.length > 0) {
        for (let index = 0; index < cc_list.length; index++) {
          const element = cc_list[index];

          let cc_cust_dets = await customerModel.get_user(element.customer_id);
          if (cc_cust_dets && cc_cust_dets.length > 0 && cc_cust_dets[0].status == 1 && cc_cust_dets[0].dnd != 1) {
            cc_details.push(cc_cust_dets[0].email);
          }
        }
      }

      if (to_details.length == 0 && cc_details.length > 0) {
        to_details = cc_details;
        cc_details = [];
      }

      if (notify_type > 0) {
        //EMPLOYEE
        let task_owner = task_details[0].owner;
        let task_second_assign_arr = await taskModel.get_auto_assigned_task_owner(task_details[0].task_id);
        let task_curr_owner = task_details[0].current_ownership;
        let param_arr = {};

        //console.log('task_owner',task_owner);
        //console.log('task_second_assign',task_second_assign_arr[0].assigned_to);
        //console.log('task_curr_owner',task_curr_owner);

        if (notify_type == 1) {
          let cust_details = await customerModel.get_user(posted_by);
          param_arr = {
            customer_name: cust_details[0].first_name
          };
        } else if (notify_type == 2) {
          let agent_details = await agentModel.get_user(posted_by);
          param_arr = {
            agent_name: `${agent_details[0].first_name} (Agent)`
          };
        }

        let notification_text = await taskModel.get_notification_text(notify_type, param_arr);

        //TASK OWNER
        if (task_owner > 0) {

          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_owner,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }

        let task_second_assign = 0;
        if (task_second_assign_arr && task_second_assign_arr.length > 0 && task_second_assign_arr[0].assigned_to > 0) {
          task_second_assign = task_second_assign_arr[0].assigned_to;
        }

        //TASK SECOND ASSIGNEE
        if (task_second_assign > 0 &&
          !common.inArray(task_second_assign, [task_owner])
        ) {
          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_second_assign,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }

        //TASK CURRENT OWNER
        if (
          task_curr_owner > 0 &&
          !common.inArray(task_curr_owner, [task_owner, task_second_assign])
        ) {
          let cc_arr = [
            notify_type,
            task_details[0].task_id,
            task_curr_owner,
            common.currentDateTime(),
            0,
            notification_text,
            notify_type,
            module.exports.subject_line
          ]
          await taskModel.notify_employee(cc_arr);
        }
      }


      ret_arr.push({
        to_arr: to_details
      });
      ret_arr.push({
        cc_arr: cc_details
      });
    }

    return ret_arr;
  }
}