const request = require("request");
const Config = require('../configuration/config');

module.exports = {
  /**
   * Generates a Sales Force Token
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @return {Json Object} - Sales Force Token used for token selection
   */
  getToken: async () => {

    return new Promise((resolve, reject) => {
      var options = {
        method: 'POST',
        url: Config.sales_force_contacts.token_url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };
      //console.log("options===========>", options);
      request(options, async function (error, response, body) {
        console.log(response.body);
        if (error) {
          resolve({
            status: 2,
            error: error
          });
        } else {
          resolve({
            status: 1,
            token: JSON.parse(response.body)
          });
        }
      });
    });
  },
  /**
   * Generates a Send Contact Details
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} token - HTTP request argument to the middleware function
   * @param {Object} sfBody - HTTP request argument to the middleware function
   * @return {Json Object} - Send contact details used for contact selection
   */
  sendContactDetails: async (token, sfBody) => {
    return new Promise((resolve, reject) => {
      var options = {
        method: 'POST',
        url: Config.sales_force_contacts.contact_url,
        headers: {
          'Content-type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(sfBody)
      };

      request(options, async function (error, response, body) {
        if (error) {
          resolve({
            status: 2,
            error: error
          });
        } else {
          resolve({
            status: 1,
            success_token: JSON.parse(response.body)
          });
        }
      });
    });
  },
  /**
   * Generates a Send Sample Details
   * @author Suvojit Biswas <suvojit.biswas@indusnet.co.in>
   * @param {Object} token - HTTP request argument to the middleware function
   * @param {Object} sfBody - HTTP request argument to the middleware function
   * @param {Object} sfURL - HTTP request argument to the middleware function
   * @return {Json Object} - Send sample details used for sample selection
   */
  sendSampleDetails: async (token, sfBody, sfURL) => {
    return new Promise((resolve, reject) => {
      var options = {
        method: 'POST',
        url: sfURL,
        headers: {
          'Content-type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(sfBody)
      };

      request(options, async function (error, response, body) {
        if (error) {
          resolve({
            status: 2,
            error: error
          });
        } else {
          resolve({
            status: 1,
            success_token: JSON.parse(response.body)
          });
        }
      });
    });
  },
}